
export const environment = {
  production: false,
  abreviation:'L',
  services: {
    url: {

      auth: 'https://ams.ws.iordanov.info/',
      admin: 'https://ams.ws.iordanov.info/admin/',
      country: 'https://ams.ws.iordanov.info/admin/country/',
      airport: 'https://ams.ws.iordanov.info/admin/airport/',
      aircraft: 'https://ams.ws.iordanov.info/admin/aircraft/',
      airline: 'https://ams.ws.iordanov.info/airline/',
      flight: 'https://ams.ws.iordanov.info/flight/',
      company: 'https://sx.ws.iordanov.info/company/',
      pricefile: 'https://sx.ws.iordanov.info/pricefile/',
      project: 'https://sx.ws.iordanov.info/project/',
      commodity: 'https://sx.ws.iordanov.info/mfr/',
    },
    domains: {
      iordanovInfo: 'sx.ws.iordanov.info',
      amsWs:'ams.ws.iordanov.info',
      ams:'ams.iordanov.info'
    },
  },
  googleanalitics: {
    gaTagId: 'GTM-WTZFR6K2',
    gaTag: 'WTZFR6K2',
    gaTagIdOld: 'G-KXR6TFBBQX',
    gaTagOld: 'KXR6TFBBQX',
  },
  adsense: {
    adClientId: 'client=ca-pub-9836419746300792',
    show: true,
  },
  yllix: {
    clientId: 455632,
  },
  aads: {
    clientId: 448273,
  },
  adsprovider: 'aads',
};
