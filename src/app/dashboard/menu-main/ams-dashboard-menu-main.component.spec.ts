import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmsDashboardMenuMainComponent } from './ams-dashboard-menu-main.component';

describe('AmsDashboardMenuMainComponent', () => {
  let component: AmsDashboardMenuMainComponent;
  let fixture: ComponentFixture<AmsDashboardMenuMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsDashboardMenuMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsDashboardMenuMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
