import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmsDashboardComponent } from './ams-dashboard.component';

describe('AmsDashboardComponent', () => {
  let component: AmsDashboardComponent;
  let fixture: ComponentFixture<AmsDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
