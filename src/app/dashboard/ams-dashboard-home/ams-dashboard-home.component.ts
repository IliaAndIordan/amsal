import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AMS_COMMON, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsTransactionsSum, AmsTransactionsSumLastWeekData } from 'src/app/@core/services/api/airline/transaction';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { AirlineService } from 'src/app/airline/airline.service';

@Component({
  selector: 'ams-dashboard-home',
  templateUrl: './ams-dashboard-home.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class AmsDashboardHomeComponent implements OnInit, OnDestroy {

  roots = AppRoutes;
  tileImgUrl: string = URL_COMMON_IMAGE_TILE;
  acTypeImg = URL_COMMON_IMAGE_AIRCRAFT + 'aircraft_0_tableimg.png';
  bankImg = URL_COMMON_IMAGE_AMS_COMMON + 'Bank.png';
  airportImg = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';

  user: UserModel;
  userChanged: Subscription;
  avatarUrl: string;


  airline: AmsAirline;
  logoUrl: string;
  liveryUrl: string;
  hqAirport: AmsAirport;

  trSumLastWeek: AmsTransactionsSumLastWeekData;
  trSumLastWeekChanged: Subscription;
  loadingTrSumLastWeek$: Observable<boolean>;

  timerMinOne: Subscription;

  constructor(
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private alService: AirlineService,) {
  }

  ngOnInit(): void {

    this.timerMinOne = this.cus.timerMinOneSubj.subscribe(min => {
      this.refreshAirline();
    });
    this.userChanged = this.cus.userChanged.subscribe(user => {
      this.initFields();
    });
    this.trSumLastWeekChanged = this.trService.trSumLastWeekChanged.subscribe(data => {
      this.initFields();
    });
    this.initFields();
    this.loadTransactionsSumLastWeek();
    this.refreshAirline();
  }

  ngOnDestroy(): void {
    if (this.timerMinOne) { this.timerMinOne.unsubscribe(); }
    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.trSumLastWeekChanged) { this.trSumLastWeekChanged.unsubscribe(); }
  }

  initFields() {
    this.user = this.cus.user;
    this.airline = this.cus.airline;
    this.trSumLastWeek = this.trService.trSumLastWeek;
    //console.log('initFields -> trSumLastWeek:', this.trSumLastWeek);
    this.loadingTrSumLastWeek$ = this.trService.loadingTrSumLastWeek$;
    this.avatarUrl = this.cus.avatarUrl;
    this.hqAirport = this.cus.hqAirport;
    this.logoUrl = this.airline?.logo ? this.airline.logo : this.airline?.amsLogoUrl;
    this.liveryUrl = this.airline?.amsLiveryUrl;

  }

  //#region Data

  loadTransactionsSumLastWeek(): void {

    this.trService.loadTrSumLastWeek(this.airline.alId)
      .subscribe((res: AmsTransactionsSum[]) => {
        this.initFields();
      },
        err => {
          console.log('loadTransactionsSumLastWeek-> err: ', err);
        });

  }

  refreshAirline(): void {
    if (this.airline) {
      this.alService.loadAirline(this.airline.alId)
        .subscribe((res: AmsAirline) => {
          this.cus.airline = res;
          this.initFields();
        });
    }
  }

  spmAircraftOpen(ac: AmsAircraft): void {
    if (ac && ac.acId) {
      this.cus.spmAircraftPanelOpen.next(ac.acId);
    }
  }

  spmFlqOpenClick(flq: AmsFlight) {
    if (flq && flq.flId) {
      this.cus.spmAircraftPanelOpen.next(flq.flId);
    }
  }

  //#endregion

}
