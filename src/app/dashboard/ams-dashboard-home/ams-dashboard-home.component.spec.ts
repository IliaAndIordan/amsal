import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmsDashboardHomeComponent } from './ams-dashboard-home.component';

describe('AmsDashboardHomeComponent', () => {
  let component: AmsDashboardHomeComponent;
  let fixture: ComponentFixture<AmsDashboardHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsDashboardHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsDashboardHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
