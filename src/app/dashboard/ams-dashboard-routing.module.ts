import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '../@core/const/app-routes.const';
import { AmsDashboardHomeComponent } from './ams-dashboard-home/ams-dashboard-home.component';
import { AmsDashboardComponent } from './ams-dashboard.component';

const routes: Routes = [
  { 
    path: AppRoutes.Root, component: AmsDashboardComponent,
    children: [
      { path: AppRoutes.Root, component: AmsDashboardHomeComponent,
        
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmsDashboardRoutingModule { }

export const routedComponents = [
  AmsDashboardComponent, 
  AmsDashboardHomeComponent,
];
