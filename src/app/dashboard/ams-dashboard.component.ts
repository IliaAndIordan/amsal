import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subscription } from 'rxjs';
import { Animate } from '../@core/const/animation.const';
import { COMMON_IMG_LOGO_RED } from '../@core/const/app-storage.const';
import { AmsAircraft } from '../@core/services/api/aircraft/dto';
import { AmsAirline } from '../@core/services/api/airline/dto';
import { AmsTransactionsSum, AmsTransactionTypeSum } from '../@core/services/api/airline/transaction';
import { AmsAirportCacheService } from '../@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from '../@core/services/api/airport/dto';
import { AmsFlight } from '../@core/services/api/flight/dto';
import { ApiAuthClient } from '../@core/services/auth/api/api-auth.client';
import { UserModel } from '../@core/services/auth/api/dto';
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { AmsCommTransactionService } from '../@core/services/common/ams-comm-transaction.service';
import { SpinnerService } from '../@core/services/spinner.service';
import { AmsSidePanelModalComponent } from '../@share/components/common/side-panel-modal/side-panel-modal';
import { SxSidebarNavComponent } from '../@share/components/common/sidebar/nav/sx-sidebar-nav.component';
import { IInfoMessage, InfoMessageDialog } from '../@share/components/dialogs/info-message/info-message.dialog';

@Component({
  selector: 'ams-al-dashboard',
  templateUrl: './ams-dashboard.component.html',
})
export class AmsDashboardComponent implements OnInit, OnDestroy {

  @ViewChild('spmAircraftPanel') spmAircraftPanel: AmsSidePanelModalComponent;
  @ViewChild('spmFlq') spmFlq: AmsSidePanelModalComponent;
  @ViewChild('snav') snav: MatSidenav;
  @ViewChild('sxSide') sxSide: SxSidebarNavComponent;

  snavmode: MatDrawerMode = 'side';


  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  airline: AmsAirline;

  user: UserModel;
  userChanged: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    public dialog: MatDialog,
    private authClient: ApiAuthClient,
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private apCache: AmsAirportCacheService,) { }

  ngOnInit(): void {
    this.userChanged = this.cus.userChanged.subscribe(user => {
      this.initFields();
    });
    this.authClient.getUserSettings(this.cus.user.userId).subscribe(res => {
      //console.log('getUserSettings -> res:', res);
    });
    

    this.initFields();
    this.loadAirport();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
  }

  initFields() {
    this.user = this.cus.user;
    this.airline = this.cus.airline;

  }

  //#region Sidebar


  showMessage(data: IInfoMessage) {
    console.log('showMessage-> data:', data);

    const dialogRef = this.dialog.open(InfoMessageDialog, {
      width: '721px',
      height: '320px',
      data: data
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log('showMessage-> res:', res);
      if (res) {

      }
    });

  }

  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  //#endregion

  //#region Data

  refreshTransactions(airline: AmsAirline) {
    this.loadTransactionsSumLastWeek(airline);
    this.loadTransactionsTypeSumToday(airline);
  }

  loadTransactionsSumLastWeek(airline: AmsAirline): void {
    if (airline) {
      this.spinerService.show();
      this.trService.loadTrSumLastWeek(this.airline.alId)
        .subscribe((res: AmsTransactionsSum[]) => {
          console.log('loadTransactionsSumLastWeek-> res: ', res);
          this.spinerService.hide();
          this.initFields();
        },
          err => {
            this.spinerService.hide();
            console.log('autenticate-> err: ', err);
          });
    }
  }

  loadTransactionsTypeSumToday(airline: AmsAirline): void {
    if (airline) {
      this.spinerService.show();
      this.trService.loadTrTypeSumToday(airline.alId)
        .subscribe((res: AmsTransactionTypeSum[]) => {
          console.log('loadTransactionsTypeSumToday-> loadTrTypeSumToday: ', res);
          this.spinerService.hide();
          this.initFields();
        },
          err => {
            this.spinerService.hide();
            console.log('autenticate-> err: ', err);
          });
    }
  }

  airport$: Observable<AmsAirport>;
  loadAirport() {
    if (this.airline) {
      this.airport$ = this.apCache.getAirport(this.airline.apId);
      this.airport$.subscribe(airport => {
        this.initFields();
      });
    }
  }

  //#endregion

  //#region  Expand/Close Menu


  snavToggleClick() {
    if (this.sxSide) {
      this.sxSide.toggle();
    }
  }

  //#endregion

  
}
