import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmsDashboardRoutingModule, routedComponents } from './ams-dashboard-routing.module';
import { AmsDashboardMenuMainComponent } from './menu-main/ams-dashboard-menu-main.component';
import { SxShareModule } from '../@share/@share.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { AmsDashboardHomeComponent } from './ams-dashboard-home/ams-dashboard-home.component';


@NgModule({
  imports: [
    CommonModule,
    SxShareModule,
    SxMaterialModule,
    SxPipesModule,
    //
    AmsDashboardRoutingModule,
  ],
  declarations: [
    routedComponents,
    AmsDashboardMenuMainComponent,
    AmsDashboardHomeComponent,
  ],
  
})
export class AmsDashboardModule { }
