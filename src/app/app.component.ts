import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { NavigationEnd, RouteConfigLoadEnd, RouteConfigLoadStart, Router } from '@angular/router';
import localeFr from '@angular/common/locales/fr';
import { AmsAirportCacheService } from './@core/services/api/airport/ams-airport-cache.service';
import { CurrentUserService } from './@core/services/auth/current-user.service';
import { SpinnerService } from './@core/services/spinner.service';
import { filter, Subscription } from 'rxjs';
import { MapLeafletSpmService } from './@share/maps/map-liflet-spm.service';
import { AmsSidePanelModalComponent } from './@share/components/common/side-panel-modal/side-panel-modal';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from './@core/const/animations-triggers';
import { AmsAircraft, AmsMac } from './@core/services/api/aircraft/dto';
import { AmsAirlineAircraftCacheService } from './@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { AmsAirport } from './@core/services/api/airport/dto';
import { AmsAirline } from './@core/services/api/airline/dto';
import { AmsAirlineClient } from './@core/services/api/airline/api-client';
import { AmsFlight } from './@core/services/api/flight/dto';
import { AmsFlightClient } from './@core/services/api/flight/ams-flight.client';
import packageJson from '../../package.json';
import { ToastrService } from 'ngx-toastr';
import { AmsFlightPlanPayload } from './@core/services/api/airline/al-flp-shedule';
import { CookiesConsentModel } from './@core/services/auth/api/dto';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';
import { ajax, css } from "jquery";
import * as Leaflet from 'leaflet';
import * as L from 'leaflet-arc';
declare let L;

Leaflet.Icon.Default.imagePath = 'assets/';
registerLocaleData(localeFr);
//declare var dataLayer: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AppComponent implements OnInit, OnDestroy {

  @ViewChild('spmMap') spmMap: AmsSidePanelModalComponent;
  @ViewChild('spmAircraft') spmAircraft: AmsSidePanelModalComponent;
  @ViewChild('spmMac') spmMac: AmsSidePanelModalComponent;
  
  @ViewChild('spmAirport') spmAirport: AmsSidePanelModalComponent;
  @ViewChild('spmAirline') spmAirline: AmsSidePanelModalComponent;
  @ViewChild('spmFlight') spmFlight: AmsSidePanelModalComponent;
  @ViewChild('spmFlightQueue') spmFlightQueue: AmsSidePanelModalComponent;
  @ViewChild('spmFlpPanel') spmFlpPanel: AmsSidePanelModalComponent;

  title = 'psm';
  version: string = packageJson.version;
  spmMapPanelOpen: Subscription;
  spmAircraftPanelOpen: Subscription;
  spmAirportPanelOpen: Subscription;
  spmAirlinePanelOpen: Subscription;
  spmFlightPanelOpen: Subscription;
  spmFlightQueuePanelOpen: Subscription;
  spmFlpPanelOpen: Subscription;

  consent: CookiesConsentModel | undefined;
  consentChanged: Subscription | undefined;

  constructor(
    private router: Router,
    private spinner: SpinnerService,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private alClient: AmsAirlineClient,
    private flClient: AmsFlightClient,
    private apCache: AmsAirportCacheService,
    private acCache: AmsAirlineAircraftCacheService,
    private spmMapService: MapLeafletSpmService) {
    const navEndEvent$ = router.events.pipe(
      filter(e => e instanceof NavigationEnd)
    );
    navEndEvent$.subscribe((event: any) => {
      const location = document.location.origin + event.urlAfterRedirects;
      const title = document.title + ' ' + event?.urlAfterRedirects?.substring(1);
      this.consent = this.cus.cookiesConsent;
      const key: string = `ga-disable-${environment.googleanalitics.gaTagId}`;
      (window as { [key: string]: any })[key] = !this.consent?.consentUseStatistics || false;
      if (this.consent?.consentUseStatistics) {
        /*
        dataLayer.push({
          'event': 'Pageview',
          'page_location': location,
          'pagePath': location,
          'pageTitle': title,
          'visitorType': 'customer',
          cookie_flags: 'max-age=7200;secure;samesite=none',
        });*/
      }
    });
  }

  ngOnInit(): void {
    this.spmFlpPanelOpen = this.cus.spmFlpPanelOpen.subscribe((data: AmsFlightPlanPayload) => {
      if (data) {
        this.spmFlp = data;
        this.spmFlpOpen(data);
      }
    })
    this.spmMapPanelOpen = this.spmMapService.spmMapPanelOpen.subscribe((panelIn: boolean) => {
      if (panelIn) { this.spmMapOpen(); }
    });
    this.spmAircraftPanelOpen = this.cus.spmAircraftPanelOpen.subscribe((acId: number) => {
      if (acId) {
        this.spinner.show();
        const aircraft$ = this.acCache.getAircraft(acId);
        aircraft$.subscribe(ac => {
          this.spinner.hide();
          this.spmAircraftOpen(ac);
        });
      }
    });

    this.spmAirportPanelOpen = this.cus.spmAirportPanelOpen.subscribe((apId: number) => {
      if (apId) {
        this.spinner.show();
        const ap$ = this.apCache.getAirport(apId);
        ap$.subscribe(ap => {
          if (ap) {
            this.spinner.hide();
            this.spmAirportOpen(ap);
          } else {
            this.apCache.clearCache();
            this.toastr.info(`Airport #${apId} not found. <br/><i>Please, try again. Airport cache was cleared.</i>`, 'Airports', { enableHtml: true })
          }

        });
      }
    });
    this.spmAirlinePanelOpen = this.cus.spmAirlinePanelOpen.subscribe((alId: number) => {
      if (alId) {
        this.spinner.show();
        const al$ = this.alClient.airlineGet(alId);
        al$.subscribe(al => {
          this.spinner.hide();
          this.spmAirlineOpen(al);
        });
      }
    });
    this.spmFlightPanelOpen = this.cus.spmFlightPanelOpen.subscribe((flId: number) => {
      if (flId) {
        this.spinner.show();
        const al$ = this.flClient.flightGet(flId);
        al$.subscribe(flight => {
          console.log('spmFlightPanelOpen -> flight:', flight);
          this.spinner.hide();
          this.spmFlightOpen(flight);
        });
      }
      else {
        this.spmFlightClose();
      }
    });
    this.spmFlightQueuePanelOpen = this.cus.spmFlightQueuePanelOpen.subscribe((flId: number) => {
      //console.log('spmFlightQueuePanelOpen -> flId:', flId);
      if (flId) {
        this.spinner.show();
        const al$ = this.flClient.flightQueueGet(flId);
        al$.subscribe(flight => {
          this.spinner.hide();
          this.spmFlightQueueOpen(flight);
        });
      }
      else {
        this.spmFlightClose();
      }
    });
    this.spmFlightClose();
  }

  ngOnDestroy(): void {
    if (this.spmMapPanelOpen) { this.spmMapPanelOpen.unsubscribe(); }
    if (this.spmAircraftPanelOpen) { this.spmAircraftPanelOpen.unsubscribe(); }
    if (this.spmAirportPanelOpen) { this.spmAirportPanelOpen.unsubscribe(); }
    if (this.spmAirlinePanelOpen) { this.spmAirlinePanelOpen.unsubscribe(); }
    if (this.spmFlightPanelOpen) { this.spmFlightPanelOpen.unsubscribe(); }
  }

  preloadCacheData(): void {
    this.apCache.airports.subscribe(res => {
      console.log('preloadCacheData-> airports:', res);
    })
  }

  //#region SPM

  spmMapClose(): void {
    if (this.spmMap) {
      this.spmMapService.spmMapPanelOpen.next(false);
      this.spmMap.closePanel();
    }
  }

  spmMapOpen() {
    if (this.spmMap) {
      this.spmMap.expandPanel();
      /*
      this.spinner.show();
      this.apCache.loadAirports().then(airports => {
        this.spinner.hide();
        if (airports) {
          this.spmMap.expandPanel();
        }
      });*/
    }
  }

  spmMacDto:AmsMac;
  spmMacClose(): void {
    if (this.spmAircraft) {
      this.spmAircraft.closePanel();
    }
  }

  spmMacOpen(value: AmsMac) {
    this.spmMacDto = value;
    if (this.spmMac && this.spmMacDto) {
      this.spmMac.expandPanel();
    }
  }
  

  spmAc: AmsAircraft;

  spmAircraftClose(): void {
    if (this.spmAircraft) {
      this.spmAircraft.closePanel();
    }
  }

  spmAircraftOpen(ac: AmsAircraft) {
    this.spmAc = ac;
    if (this.spmAc && this.spmAircraft) {
      this.spmAircraft.expandPanel();
    }
  }

  spmAp: AmsAirport;

  spmAirportClose(): void {
    if (this.spmAirport) {
      this.spmAirport.closePanel();
    }
  }

  spmAirportOpen(ap: AmsAirport) {
    this.spmAp = ap;
    if (this.spmAp && this.spmAirport) {
      this.spmAirport.expandPanel();
    }
  }

  spmAl: AmsAirline;

  spmAirlineClose(): void {
    if (this.spmAirline) {
      this.spmAirline.closePanel();
    }
  }

  spmAirlineOpen(al: AmsAirline) {
    this.spmAl = al;
    if (this.spmAirline && this.spmAl) {
      this.spmAirline.expandPanel();
    }
  }

  spmFl: AmsFlight;

  spmFlightClose(): void {
    if (this.spmFlight) {
      this.spmFlight.closePanel();
    }
  }

  spmFlightOpen(fl: AmsFlight) {
    this.spmFl = fl;
    if (this.spmFlight && this.spmFl) {
      this.spmFlight.expandPanel();
    }
  }

  spmFlq: AmsFlight;

  spmFlightQueueClose(): void {
    if (this.spmFlightQueue) {
      this.spmFlightQueue.closePanel();
    }
  }

  spmFlightQueueOpen(fl: AmsFlight) {
    this.spmFlq = fl;
    if (this.spmFlightQueue && this.spmFlq) {
      this.spmFlightQueue.expandPanel();
    }
  }

  spmFlp: AmsFlightPlanPayload;
  spmFlpClose(): void {
    if (this.spmFlpPanel) {
      this.spmFlpPanel.closePanel();
    }
  }

  spmFlpOpen(value: AmsFlightPlanPayload) {
    this.spmFlp = value;
    if (this.spmFlpPanel && this.spmFlp) {
      this.spmFlpPanel.expandPanel();
    }
  }
  //#endregion

}
