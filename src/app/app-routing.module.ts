import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from './@core/const/app-routes.const';
import { AdminGuard } from './@core/guards/admin.guard';
import { AuthGuard } from './@core/guards/auth.guard';
import { PreloadSelectedModulesList } from './@core/guards/preload-strategy';

const routes: Routes = [
  { path: AppRoutes.Root, pathMatch: 'full', redirectTo: AppRoutes.public },
  {
    path: AppRoutes.public,
    loadChildren: () => import('./public/ams-public.module').then(m => m.AmsPublicModule), data: { preload: false }
  },
  {
    path: AppRoutes.airline,
    loadChildren: () => import('./airline/airline.module').then(m => m.AirlineModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutes.fleet,
    loadChildren: () => import('./fleet/al-fleet.module').then(m => m.AmsAlFleetModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutes.acMarket,
    loadChildren: () => import('./ac-market/ac-market.module').then(m => m.AcMarketModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutes.admin,
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule), data: { preload: false },
    canActivate: [AdminGuard]
  },
  {
    path: AppRoutes.wad,
    loadChildren: () => import('./wad/wad.module').then(m => m.AmsWadModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutes.dashboard,
    loadChildren: () => import('./dashboard/ams-dashboard.module').then(m => m.AmsDashboardModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  {
    path: AppRoutes.flightPlan,
    loadChildren: () => import('./flight-plan/flight-plan.module').then(m => m.FlightPlanModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  /*
  { path: AppRoutes.wad+'/'+AppRoutes.subregion,
    loadChildren: () => import('./subregion/subregion.module').then(m => m.AmsSubregionModule), data: { preload: false },
    canActivate: [AuthGuard]
  },
  { path: AppRoutes.wad+'/'+AppRoutes.country,
  loadChildren: () => import('./wad-country/wad-country.module').then(m => m.AmsWadCountryModule), data: { preload: false },
  canActivate: [AuthGuard]
},*/

];

@NgModule({
  imports: [
   
    RouterModule.forRoot(routes,
      {
    preloadingStrategy: PreloadSelectedModulesList,
    paramsInheritanceStrategy: 'always', onSameUrlNavigation: 'reload'
})
  ],
  exports: [RouterModule],
  providers: [
    PreloadSelectedModulesList
  ]
})
export class AppRoutingModule { }

export const routableComponents = [];
