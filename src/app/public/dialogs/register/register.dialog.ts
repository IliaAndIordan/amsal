import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators, FormControl } from '@angular/forms';
// Services
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { LatLng, ResponseAuthenticate } from 'src/app/@core/services/auth/api/dto';
// Constants

export interface LoginData {
  email: string;
  password: string;
}

@Component({
  templateUrl: './register.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class RegisterModal implements OnInit {

  formGrp: UntypedFormGroup;

  email: string;
  password: string;
  hidePassword:boolean = true;
  latlon: LatLng;

  hasSpinner = false;
  errorMessage: string;

  constructor(
    private fb: UntypedFormBuilder,
    private router: Router,
    private authClient: ApiAuthClient,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    public dialogRef: MatDialogRef<RegisterModal>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData) {
    this.email = data.email;
    this.password = data.password;
  }

  
  get ctr_email():FormControl { return this.formGrp.get('ctr_email') as FormControl; }
  get ctr_password():FormControl { return this.formGrp.get('ctr_password') as FormControl; }

  get errorEMail(): string {
    return this.ctr_email.hasError('required') ? 'E-Mail is required.' :
      this.ctr_email.hasError('email') ? 'Please enter a valid email address' : '';
  }

  get errorPassword(): string {
    return this.ctr_password.hasError('required') ? 'Password is required.' :
      this.ctr_password.hasError('minlength') ? 'Password must be et least 3 characters lon' : '';
  }

  ngOnInit(): void {

    this.errorMessage = undefined;
    this.hasSpinner = false;

    this.formGrp = this.fb.group({
      ctr_email: new UntypedFormControl(this.email, [Validators.required, Validators.email]),
      ctr_password: new UntypedFormControl(this.password, [Validators.required, Validators.minLength(3)]),
    });

    this.cus.getLocation(true).subscribe(location => {
      console.log('Location:', location);
      this.latlon = location;
    });
  }


  cansel(): void {
    this.dialogRef.close();
  }

  onSubmit() {

    if (this.formGrp.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;

      this.email = this.ctr_email.value;
      this.password = this.ctr_password.value;

      this.authClient.register(this.email, this.password, this.latlon)
        .subscribe((resp: ResponseAuthenticate) => {
          console.log('register-> resp:', resp);
          if (resp && resp.success) {
            const user = this.cus.user;
            console.log('register-> resp user', user);
            if (user) {
              // this.spinerService.display(false);
              this.hasSpinner = false;
              const name = user.userName?user.userName:user.userEmail;
              this.toastr.success('Welcome ' + name + '!', 'Registration success');
              setTimeout((router: Router) => {
                this.router.navigate([AppRoutes.Root, AppRoutes.airline]);
                this.dialogRef.close(user);
              }, 1000);
            } else {
              this.hasSpinner = false;
              this.toastr.error('Registration Failed', 'User registration failed.');
            }
          } else{
            console.error('register-> got an error: ' + resp);
            this.errorMessage = resp && resp.message?resp.message:'   Please enter valid values for fields';
            this.toastr.error(`User registration failed.<br/> ${this.errorMessage}`, 'Registration Failed',{enableHtml:true});
            this.hasSpinner = false;
          }

        },
          err => {
            console.error('register-> got an error: ' + err);
            this.errorMessage = 'Please enter valid values for fields';
            this.hasSpinner = false;
          },
          () => {
            console.log('autenticate-> got a complete notification');
          });
    } else {
      this.errorMessage = 'Please enter valid values for fields';
      this.hasSpinner = false;
    }
  }


}
