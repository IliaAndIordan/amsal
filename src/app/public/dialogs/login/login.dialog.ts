import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// Services
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { LatLng, ResponseAuthenticate } from 'src/app/@core/services/auth/api/dto';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/@core/services/spinner.service';


export interface LoginData {
  email: string;
  password: string;
}

@Component({
  templateUrl: './login.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class LoginModal implements OnInit {

  formGrp: FormGroup;

  email: string;
  password: string;
  latlon: LatLng;

  hasSpinner = false;
  errorMessage: string;
  redirectTo: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: SpinnerService,
    private authClient: ApiAuthClient,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    public dialogRef: MatDialogRef<LoginModal>,
    @Inject(MAT_DIALOG_DATA) public data: LoginData) {
    this.email = data.email;
    this.password = data.password;
  }

  get ctr_email(): FormControl { return this.formGrp.get('ctr_email') as FormControl; }
  get ctr_password(): FormControl { return this.formGrp.get('ctr_password') as FormControl; }

  errorEMail(): string {
    return this.ctr_email.hasError('required') ? 'E-Mail is required.' :
      this.ctr_email.hasError('email') ? 'Please enter a valid email address' : '';
  }

  errorPassword(): string {
    return this.ctr_password.hasError('required') ? 'Password is required.' :
      this.ctr_password.hasError('minlength') ? 'Password must be et least 3 characters lon' : '';
  }

  ngOnInit(): void {

    this.errorMessage = undefined;
    this.hasSpinner = false;

    this.route.queryParams
      .subscribe(params => this.redirectTo = params['redirectTo'] || AppRoutes.airline);

    this.formGrp = this.fb.group({
      ctr_email: new FormControl<string | null>(this.email || '', [Validators.required, Validators.email]),
      ctr_password: new FormControl<string | null>(this.password || '', [Validators.required, Validators.minLength(3)]),
    });
    this.cus.getLocation(true).subscribe(location => {
      this.latlon = location;
    });
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  onLoginClick() {

    if (this.formGrp.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;
      this.spinner.show();

      this.email = this.ctr_email.value;
      this.password = this.ctr_password.value;

      this.authClient.autenticate(this.email, this.password, this.latlon)
        .subscribe((resp: ResponseAuthenticate) => {
          const user = this.cus.user;
          this.spinner.hide();
          console.log('autenticate-> resp user', user);
          if (user) {
            // this.spinerService.display(false);
            this.hasSpinner = false;
            const name = user.userName ? user.userName : user.userEmail;
            this.toastr.success('Welcome ' + name + '!', 'Login success');

            if (this.redirectTo) {
              this.router.navigate([this.redirectTo]);
            } else {
              this.router.navigate([AppRoutes.Root, AppRoutes.airline]);
            }
            this.dialogRef.close(user);
            /*
            setTimeout((router: Router=this.router, container=this) => {
              console.log('container.redirectTo', container.redirectTo);
              if(container.redirectTo){
                //router.navigate(['/'+this.redirectTo]);
                router.navigate([AppRoutes.Root, AppRoutes.airline]);
              } else{
                router.navigate([AppRoutes.Root, AppRoutes.airline]);
              }
              
              container.dialogRef.close(user);
            }, 1000);*/
          } else {
            this.hasSpinner = false;
            this.toastr.error('Login Failed', 'Login Failed');
          }
        },
          err => {
            console.error('autenticate-> got an error: ' + err);
            this.errorMessage = 'Please enter valid values for fields';
            this.hasSpinner = false;
            this.spinner.hide();
          },
          () => {
            console.log('autenticate-> got a complete notification');
          });
    } else {
      this.errorMessage = 'Please enter valid values for fields';
      this.hasSpinner = false;
      this.spinner.hide();
    }
  }


}
