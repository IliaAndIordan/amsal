import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/const/animation.const';
// --- Models
import { AppRoutes } from '../@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED, URL_IMG_STORYLINE } from '../@core/const/app-storage.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger } from '../@core/const/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { LoginModal } from './dialogs/login/login.dialog';
import { RegisterModal } from './dialogs/register/register.dialog';
// import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab]
})

export class AmsPublicHomeComponent implements OnInit, OnDestroy {

  /**
   * Fields
   */
  sxLogo = COMMON_IMG_LOGO_RED;
  imgUrlStoryline = URL_IMG_STORYLINE;
  // --- Side Tab
  expandTabVar: string = Animate.in;
  showTabContentsVar: string = Animate.hide;
  total: number = 0;
  aggressiveTotal: number = 0;
  subtittle:string;

  // public dialogService: MatDialog
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    public dialogService: MatDialog) { }


  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
  }

  exapandTabClick() {
    this.expandTabVar = this.expandTabVar === Animate.in ? Animate.out : Animate.in;
    this.showTabContentsVar = this.showTabContentsVar === Animate.show ? Animate.hide : Animate.show;
  }

  public loginDialogShow(): void {
    
    const dialogRef = this.dialogService.open(LoginModal, {
      width: '500px',
      /*height: '340px',*/
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });
    
  }

  public registerDialogShow(): void {
    
    const dialogRef = this.dialogService.open(RegisterModal, {
      width: '500px',
      //height: '340px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);
      // this.password = result.password;
    });
    
  }

  gotoStoryline():void{
    const url = 'https://bwt.iordanov.info/public/published/roadmap/275530ab-07fd-11ef-887e-5254008c3f93';
    window.open(url, '_blank');
  }

}
