import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: './welcome-component.html',
})

export class WelcomeComponent implements OnInit, OnDestroy {

    constructor(private router: Router,
                private route: ActivatedRoute ) { }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

}
