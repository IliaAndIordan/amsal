import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// --- Models
import { AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS, URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { RwTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMac, AmsMacCabin, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMacCabinEditDialog } from 'src/app/@share/components/dialogs/mac-cabin-edit/mac-cabin-edit.dialog';
import { AcMarketService } from '../../ac-market.service';
import { AmsGridAcMarketAircraftTableDataSource } from './grid-ac-market-aircrafts.datasource';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';






@Component({
    selector: 'ams-grid-ac-market-aircrafts',
    templateUrl: './grid-ac-market-aircrafts.component.html',
})
export class AmsGridAcMarketAircraftsComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<AmsState>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    seatsFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_f.png';
    seatsBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_b.png';
    seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_e.png';
    typeOpt = RwTypeOpt;
    selaircraft: AmsAircraft;
    airport: AmsAirport;
    manufacturer: AmsManufacturer;

    mac: AmsMac;
    macChanged: Subscription;

    criteria: AmsAircraftTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    //'homeApId', 'homeAp',  'distanceKm', 'state', 'price'
    displayedColumns = ['action', 'image', 'acId', 'registration', 'currApId',  'price', 'cabinId', 'flightHours', 'distanceKm', 'acStatusId', 'state'];
    canEdit: boolean;
    airline: AmsAirline;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private acmService: AcMarketService,
        private client: AmsAircraftClient,
        public tableds: AmsGridAcMarketAircraftTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {


        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsAircraft>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsAircraftTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });


        this.macChanged = this.acmService.acMarketMacChanged.subscribe((mac: AmsMac) => {
            //console.log(' manufacturerChanged -> = mfr ', mfr);
            this.criteria = this.tableds.criteria;
            this.criteria.macId = mac.macId;
            this.tableds.criteria = this.criteria;

        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.macChanged) { this.macChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ?  'desc':'asc';

        if (this.criteria.macId !== this.acmService.acMarketMac.macId) {
            this.criteria.macId = this.acmService.acMarketMac.macId;
            this.tableds.criteria = this.criteria;
        }

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    buyAircraft(aicraft:AmsAircraft):void{
        if(aicraft && this.cus.airline){
            this.preloader.show();
            this.client.acBuy(aicraft.acId, this.cus.airline.alId)
            .subscribe(res => {
                this.preloader.hide();
                console.log('buyAircraft -> res:', res);
                this.refreshClick();
            }, msg => {
                console.log('loadData -> ' + msg);
                this.preloader.hide();
                this.refreshClick();
            });
        }
    }

    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if(airport && airport.apId){
            this.cus.spmAirportPanelOpen.next(airport.apId);
          }
    }

    spmAircraftOpen(value: AmsAircraft) {
        if (value) {
            this.acmService.spmAircraftPanelOpen.next(value);
        }
    }


    ediMacCabin(data: AmsMacCabin) {
        console.log('ediMacCabin-> data:', data);
        //console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            const dialogRef = this.dialogService.open(AmsMacCabinEditDialog, {
                width: '721px',
                height: '620px',
                data: {
                    cabin: data,
                    mac: this.acmService.acMarketMac
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediMac-> res:', res);
                if (res) {
                    //this.table.renderRows();
                    this.tableds.loadData()
                        .subscribe(res => {
                            this.initFields();
                        });
                }
                else {
                    this.initFields();
                }


            });

        }
        else {
            this.toastr.info('Please select manufacturer acrcraft model first.', 'Edit Cabin Configuration');
        }


    }

    addCabin() {
        // console.log('inviteUser-> ');
        let dto = new AmsMacCabin();
        dto.macId = this.acmService.acMarketMac.macId;
        this.ediMacCabin(dto);
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);

        if (data) {
            this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    //#region Data
    /*
        public loadAirport(apId: number): Observable<AmsAirport> {
            
            this.preloader.show();
            return new Observable<AmsAirport>(subscriber => {
    
                this.aService.loadAirport(apId)
                    .subscribe((resp: AmsAirport) => {
                        //console.log('loadAirport-> resp', resp);
                        subscriber.next(resp);
                        this.preloader.hide();
                    },
                        err => {
                            this.preloader.hide();
                            throw err;
                        });
            });
            
        }
    */
    //#endregion


    /*
    gotoAirport(data: AmsAirport) {
       

        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }

    }*/

    //#endregion
}

