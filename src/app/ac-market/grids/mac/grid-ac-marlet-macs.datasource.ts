import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMac, AmsMacTableCriteria, ResponseAmsAircraftTable, ResponseAmsMacTableData } from 'src/app/@core/services/api/aircraft/dto';
import { AcMarketService } from '../../ac-market.service';

export const KEY_CRITERIA = 'ams-grid-ac-marlet-macs_criteria';
@Injectable()
export class AmsGridAcMarketMacTableDataSource extends DataSource<AmsMac> {

    selrw: AmsMac;

    private _data: Array<AmsMac>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsMac[]> = new BehaviorSubject<AmsMac[]>([]);
    listSubject = new BehaviorSubject<AmsMac[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsMac> {
        return this._data;
    }

    set data(value: Array<AmsMac>) {
        this._data = value;
        this.listSubject.next(this._data as AmsMac[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private acmService: AcMarketService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsAircraftTableCriteria>();

    public get criteria(): AmsAircraftTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsMacTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsAircraftTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsAircraftTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsAircraftTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsMac[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsMac>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
     
        return new Observable<Array<AmsMac>>(subscriber => {

            this.client.acMarketTable(this.criteria)
                .subscribe((resp: ResponseAmsMacTableData) => {
                    this.data = new Array<AmsMac>();
                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.macs && resp.data.macs.length > 0) {
                            resp.data.macs.forEach(iu => {
                                const obj = AmsMac.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }
                       
                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsMac>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
