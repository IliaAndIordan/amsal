import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import { COMMON_IMG_LOGO_RED } from "src/app/@core/const/app-storage.const";
import { UserModel } from "src/app/@core/services/auth/api/dto";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { AmsUserNameEditDialog } from "src/app/@share/components/dialogs/user-edit/user-name-edit.dialog";
import { environment } from "src/environments/environment";

@Component({
    selector: 'ams-acm-menu-main',
    templateUrl: './acm-menu-main.component.html',
    // animations: [PageTransition]
})
export class AcMarketMenuMainComponent implements OnInit, OnDestroy {

    @Output() sidebarToggle: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Fields
     */
    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    roots = AppRoutes;
    user: UserModel;
    userName: string;
    userChanged: Subscription;
    avatarUrl: string;
    env: string;

    constructor(private router: Router,
        public dialogService: MatDialog,
        private cus: CurrentUserService, ) { }

    ngOnInit(): void {

        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.env = environment.abreviation;
        this.avatarUrl = this.cus.avatarUrl;
        let name = 'Guest';
        if(this.cus.user){
            name = this.cus.user.userName?this.cus.user.userName:this.cus.user.userEmail;
        }
        this.userName = name;
        // console.log('initFields -> cus.user:', this.cus.user);
    }

    
    edituser(data: UserModel) {
        
        const dialogRef = this.dialogService.open(AmsUserNameEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                user: data,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('edituser-> res:', res);
            if (res) {
                
            }
            this.initFields();
        });
        
    }
}
