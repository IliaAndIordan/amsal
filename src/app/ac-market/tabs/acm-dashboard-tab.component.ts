import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { PieMiniChartViewModel } from 'src/app/@core/models/common/chart/pie-chart.model';
import { AirlineService } from 'src/app/airline/airline.service';
import { AcMarketService } from '../ac-market.service';


@Component({
    selector: 'ams-acm-dashboard-tab',
    templateUrl: './acm-dashboard-tab.component.html',
})
export class AmsAcmDashboardTabComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Input() tabindex: string;
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();

    acImg = URL_COMMON_IMAGE_AMS_COMMON + 'airline.png';
    /**
     * Fields
     */
    panelIChanged: Subscription;
    region: AmsRegion;
    subregion: AmsSubregion;
    subregionChanged: Subscription;
    country: AmsCountry;
    state:AmsState;
    airport: AmsAirport;
    manufacturers: AmsManufacturer[];
    manufacturer:AmsManufacturer;
    manufacturerChanged: Subscription;
    mac:AmsMac;
    macChanged: Subscription;
    
    selTabIdx:number;
    tabIdxChanged: Subscription;
    productionPieChart:PieMiniChartViewModel;

    get panelIn(): boolean {
        return this.acmService.panelIn;
    }

    set panelIn(value: boolean) {
        this.acmService.panelIn = value;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private alService: AirlineService,
        private acmService: AcMarketService) {

    }


    ngOnInit(): void {
        

        this.panelIChanged = this.acmService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.acmService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });
       
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.acmService.tabIdx;
        this.manufacturers = this.acmService.manufacturers;
    }

    //#region Mobile dialog methods

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> usecase:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.acmService.tabIdx = this.selTabIdx;
    }

    panelInOpen() {
        this.acmService.panelIn = true;
    }

    gotoManufacturer(mfr:AmsManufacturer){

    }
    //#endregion

    //#region SPM Methods
   
    spmProjectOpenClick() {

        // console.log('spmProjectOpen -> spmProjectObj=', this.project);
        /*
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }*/

    }

    //#endregion

}
