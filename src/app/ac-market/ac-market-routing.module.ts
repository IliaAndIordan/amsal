import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { AcMarketHomeComponent } from './home.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { AcMarketComponent } from './ac-market.component';
import { AmsAcMarketHomeTabsComponent } from './tabs/acm-home-tabs.component';
import { AmsAcmAircraftListComponent } from './tabs/acm-aircraft-list.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: AcMarketComponent,
    children: [
      { path: AppRoutes.Root, component: AcMarketHomeComponent,
        
        children: [
          { path: AppRoutes.Root,   pathMatch: 'full', component: AmsAcMarketHomeTabsComponent, canActivate: [AuthGuard]},
          { path: AppRoutes.aircafts,  component: AmsAcmAircraftListComponent },
           /*
          { path: AppRoutes.profile, component: UserProfileComponent },
          { path: AppRoutes.create, component: AirlineCreateFormComponent },
          { path: AppRoutes.info, component: AirlineInfoComponent,  
            children: [
            { path: AppRoutes.Root,   pathMatch: 'full', component: AirlineInfoDashboardComponent },
            // { path: AppRoutes.Dashboard, component: DashboardComponent },
          ]},
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
          */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AcMarketRoutingModule { }

export const routedComponents = [
  AcMarketComponent, 
  AcMarketHomeComponent,
  AmsAcMarketHomeTabsComponent,
  AmsAcmAircraftListComponent,
];
