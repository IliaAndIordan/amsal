import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { AcMarketService } from './ac-market.service';
import { AcMarketRoutingModule, routedComponents } from './ac-market-routing.module';
import { AcMarketMenuMainComponent } from './menu-main/acm-menu-main.component';
import { AcMarketFilterPanelBodyComponent } from './filter-panel/acm-filter-panel.component';
import { AmsAcmDashboardTabComponent } from './tabs/acm-dashboard-tab.component';
import { AmsGridAcMarketMacTableDataSource } from './grids/mac/grid-ac-marlet-macs.datasource';
import { AmsGridAcMarketMacComponent } from './grids/mac/grid-ac-marlet-macs.component';
import { AmsGridAcMarketAircraftTableDataSource } from './grids/aircrafts/grid-ac-market-aircrafts.datasource';
import { AmsGridAcMarketAircraftsComponent } from './grids/aircrafts/grid-ac-market-aircrafts.component';


@NgModule({
    imports: [
        CommonModule,

        SxShareModule,
        SxMaterialModule,
        SxPipesModule,
        //
        AcMarketRoutingModule,
    ],
    declarations: [
        routedComponents,
        AcMarketMenuMainComponent,
        AcMarketFilterPanelBodyComponent,
        //tabs
        AmsAcmDashboardTabComponent,
        //Grids
        AmsGridAcMarketMacComponent,
        AmsGridAcMarketAircraftsComponent,
        //-Dialogs
        //
    ],
    exports: [],
    providers: [
        AcMarketService,
        AmsGridAcMarketMacTableDataSource,
        AmsGridAcMarketAircraftTableDataSource,
    ]
})
export class AcMarketModule { }
