import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/const/animation.const';
// --- Models
import { AppRoutes } from '../@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from '../@core/const/app-storage.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from '../@core/const/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AmsSidePanelModalComponent } from '../@share/components/common/side-panel-modal/side-panel-modal';
import { AmsAirport } from '../@core/services/api/airport/dto';
import { AmsCountry, AmsState } from '../@core/services/api/country/dto';
import { IInfoMessage, InfoMessageDialog } from '../@share/components/dialogs/info-message/info-message.dialog';
import { AmsAirline } from '../@core/services/api/airline/dto';
import { AcMarketService } from './ac-market.service';
import { UserModel } from '../@core/services/auth/api/dto';
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { SpinnerService } from '../@core/services/spinner.service';
import { AmsAircraft, AmsMac, IAmsAircraft, IAmsMac, ResponseAmsAircraftTable, ResponseAmsMacTableData } from '../@core/services/api/aircraft/dto';
import { SxSidebarNavComponent } from '../@share/components/common/sidebar/nav/sx-sidebar-nav.component';
import { MatDrawerMode } from '@angular/material/sidenav';
// import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AcMarketHomeComponent implements OnInit, OnDestroy {

  @ViewChild('spmAirport') spmAirport: AmsSidePanelModalComponent;
  @ViewChild('spmAirline') spmAirline: AmsSidePanelModalComponent;
  @ViewChild('spmAcmMac') spmAcmMac: AmsSidePanelModalComponent;
  @ViewChild('spmAircraft') spmAircraft: AmsSidePanelModalComponent;
  @ViewChild('sxSide') sxSide: SxSidebarNavComponent;

  snavmode: MatDrawerMode = 'side';
  in = Animate.in;

  country: AmsCountry;
  state: AmsState;

  hqAirport: AmsAirport;
  spmAp: AmsAirport;
  airportPanelOpen: Subscription;
  spmAl: AmsAirline;
  spmAlPanelOpen: Subscription;
  spmAc: AmsAircraft;
  spmAircraftPanelOpen: Subscription;

  spmMac: AmsMac;
  spmAcmMacChange: Subscription;

  user: UserModel;
  userChanged: Subscription;
  avatarUrl: string;

  airline: AmsAirline;
  airlineChanged: Subscription;






  sxLogo = COMMON_IMG_LOGO_RED;
  // --- Side Tab
  expandTabVar: string = Animate.in;
  showTabContentsVar: string = Animate.hide;
  total: number = 0;
  aggressiveTotal: number = 0;

  panelInVar = Animate.in;
  panelInChanged: Subscription;
  sidebarClass = 'sidebar-closed';

  acMarketAllResp: ResponseAmsMacTableData;
  acMacs: IAmsMac[];

  get panelIn(): boolean {
    let rv = this.acmService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.acmService.panelIn;
  }

  set panelIn(value: boolean) {
    this.acmService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  // public dialogService: MatDialog
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    public dialog: MatDialog,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private acmService: AcMarketService) { }


  ngOnInit(): void {

    this.user = this.cus.user;
    this.avatarUrl = this.cus.avatarUrl;
    this.airline = this.cus.airline;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    this.airportPanelOpen = this.acmService.spmAirportPanelOpen.subscribe((airport: AmsAirport) => {
      this.spmAirportOpen(airport);
    });
    this.spmAlPanelOpen = this.acmService.spmAirlinePanelOpen.subscribe((al: AmsAirline) => {
      this.spmAirlineOpen(al);
    });
    this.spmAcmMacChange = this.acmService.spmAcmMacOpen.subscribe((mac: AmsMac) => {
      this.spmAcmMacOpen(mac);
    });

    this.spmAircraftPanelOpen = this.acmService.spmAircraftPanelOpen.subscribe((ac: AmsAircraft) => {
      this.spmAircraftOpen(ac);
    });

    this.panelInChanged = this.acmService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
      this.sidebarToggle()
    });
    this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
      this.initFields();
    });

    this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
      this.initFields();
    });
    this.loadAcMacs();
  }

  ngOnDestroy(): void {
    if (this.airportPanelOpen) { this.airportPanelOpen.unsubscribe(); }
    if (this.spmAlPanelOpen) { this.spmAlPanelOpen.unsubscribe(); }

    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields() {
    const tmp = this.panelIn;
    this.user = this.cus.user;
    this.avatarUrl = this.cus.avatarUrl;
    this.airline = this.cus.airline;

    this.hqAirport = this.acmService.hqAirport;
    this.acMarketAllResp = this.acmService.acMarketAllResp;
    if (this.acMarketAllResp &&
      this.acMarketAllResp.data && this.acMarketAllResp.data.macs) {
      this.acMacs = this.acMarketAllResp.data.macs;
    }

  }


  exapandTabClick() {
    this.expandTabVar = this.expandTabVar === Animate.in ? Animate.out : Animate.in;
    this.showTabContentsVar = this.showTabContentsVar === Animate.show ? Animate.hide : Animate.show;
  }

  //#region Data


  public loadAcMacs(): void {
    this.spinerService.show();
    const acMarket$ = this.acmService.acMarketTableAll();

    //this.acmService.acMarketTableAll()
    acMarket$.subscribe((resp: ResponseAmsMacTableData) => {
      this.spinerService.hide();
      this.initFields();
    },
      err => {
        this.spinerService.hide();
        console.log('loadRegions-> err: ', err);
      });

  }

  //#endregion

  //#region  Expand/Close Menu


  snavToggleClick() {
    console.log('snavToggleClick-> sxSide', this.sxSide);
    if (this.sxSide) {
      this.sxSide.toggle();
    }
  }

  //#endregion

  //#region SPM

  showMessage(data: IInfoMessage) {
    console.log('showMessage-> data:', data);

    const dialogRef = this.dialog.open(InfoMessageDialog, {
      width: '721px',
      height: '320px',
      data: data
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log('showMessage-> res:', res);
      if (res) {

      }
    });

  }

  spmAirportClose(): void {
    if (this.spmAirport) {
      this.spmAirport.closePanel();
    }
  }

  spmAirportOpen(airport: AmsAirport) {
    this.spmAp = airport;
    if (this.spmAp && this.spmAirport) {
      this.spmAirport.expandPanel();
    }
    else {
      this.spmAirportClose();
    }

  }

  spmAirlineClose(): void {
    if (this.spmAirline) {
      this.spmAirline.closePanel();
    }
  }

  spmAirlineOpen(al: AmsAirline) {
    this.spmAl = al;

    if (this.spmAl && this.spmAirline) {
      this.showMessage({ title: 'Not Implemented', message: 'SPM component not implemented' });
      //this.spmAirline.expandPanel();
    }
    else {
      //this.spmAirlineClose();

    }

  }

  spmAcmMacClose(): void {
    if (this.spmAcmMac) {
      this.spmAcmMac.closePanel();
    }
  }

  spmAcmMacOpen(mac: AmsMac) {
    this.spmMac = mac;

    if (this.spmMac && this.spmAcmMac) {
      //this.showMessage({ title: 'Not Implemented', message: 'SPM component not implemented' });
      this.spmAcmMac.expandPanel();
    }
    else {
      //this.spmAcmMacClose();

    }

  }

  spmAircraftClose(): void {
    if (this.spmAircraft) {
      this.spmAircraft.closePanel();
    }
  }

  spmAircraftOpen(ac: AmsAircraft) {
    this.spmAc = ac;
    if (this.spmAc && this.spmAircraft) {
      this.spmAircraft.expandPanel();
    }
  }

  //#endregion

  //#region Tree Panel

  sidebarToggle() {
    this.sidebarClass = this.panelIn ? 'sidebar-open' : 'sidebar-closed';
  }

  public closePanelLeftClick(): void {
    this.panelIn = false;
    this.sidebarToggle();

  }

  public expandPanelLeftClick(): void {
    this.panelIn = true;
    this.sidebarToggle();
  }

  //#endregion


}
