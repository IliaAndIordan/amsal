import { Component, OnInit } from '@angular/core';

@Component({
  template: '<div> <router-outlet></router-outlet> </div>'
})
export class AcMarketComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
