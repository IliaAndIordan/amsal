import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS_COMMON, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAirline, BankHistory30Days } from 'src/app/@core/services/api/airline/dto';
import { IInfoMessage } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { AmsAirlineEditDialog } from 'src/app/@share/components/dialogs/airline-edit/airline-edit.dialog';
import { AmsWadService } from 'src/app/wad/wad.service';
import { AcMarketService } from '../ac-market.service';
import { AirlineService } from 'src/app/airline/airline.service';
import { AmsMac } from 'src/app/@core/services/api/aircraft/dto';


@Component({
    selector: 'ams-acm-filter-panel-body',
    templateUrl: './acm-filter-panel.component.html',
})
export class AcMarketFilterPanelBodyComponent implements OnInit, OnDestroy {

    @Output() showMessage: EventEmitter<IInfoMessage> = new EventEmitter<IInfoMessage>();

    bankImg = URL_COMMON_IMAGE_AMS_COMMON + 'Bank.png';
    logoUrl: string;
    liveryUrl: string;

    routs = AppRoutes;
    
    airline: AmsAirline;
    airlineChanged: Subscription;
    hqAirport: AmsAirport;
    acmmac:AmsMac;
    acmmacChanged: Subscription;
    showMac:boolean=false;

    tabIdx: number;
    tabIdxChanged: Subscription;
    ourApUrl: string;
    canEdit: boolean;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinnerService: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private alService: AirlineService,
        private acmService: AcMarketService) {

            router.events.forEach((event) => {
                if(event instanceof NavigationEnd) {
                    this.showMac = this.router.isActive(this.router.createUrlTree([AppRoutes.Root, AppRoutes.acMarket, AppRoutes.aircafts]), true);
                    //console.log(' NavigationEnd -> showMac ', this.showMac);
                }
              });

    }


    ngOnInit(): void {

        this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
            this.initFields();
        });
        this.initFields();

        this.tabIdxChanged = this.acmService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.acmmacChanged = this.acmService.acMarketMacChanged.subscribe((mac: AmsMac) => {
            //console.log(' manufacturerChanged -> = mfr ', mfr);
            this.initFields();

        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
        if (this.acmmacChanged) { this.acmmacChanged.unsubscribe(); }
    }

    initFields() {
        this.airline = this.acmService.airline;
        this.hqAirport = this.acmService.hqAirport;
        this.logoUrl = this.airline?.logo ? this.airline?.logo : this.airline?.amsLogoUrl;
        this.liveryUrl = this.airline?.amsLiveryUrl;
        this.canEdit = this.cus.user.userId === this.airline?.userId;
        this.tabIdx = this.acmService.tabIdx;
        this.acmmac = this.acmService.acMarketMac;
        this.showMac = this.router.isActive(this.router.createUrlTree([AppRoutes.Root, AppRoutes.acMarket, AppRoutes.aircafts]), true);
        //console.log(' initFields -> showMac ', this.showMac);
    }

    //#region Dialogs

    ediAirline() {
        const dialogRef = this.dialogService.open(AmsAirlineEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                airline: this.airline,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('ediAirline-> res:', res);
            if (res) {
                this.acmService.airline = res;
            }
            this.initFields();
        });
    }

    solrAirport(){

    }

    refreshAirline(){
        
    }

    //#endregion

    //#region SPM

    showMessageDialog(tit: string, msg: string): void {
        this.showMessage.emit({ title: tit, message: msg });
    }

    spmAirportOpen(airport: AmsAirport) {
        this.acmService.spmAirportPanelOpen.next(airport);
    }
    spmAirlineOpen(al: AmsAirline) {
        this.acmService.spmAirlinePanelOpen.next(al);
    }

    

    //#endregion

    //#region Chart

    
    //#endregion

}
