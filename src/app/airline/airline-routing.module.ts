import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { AirlineComponent } from './airline.component';
import { AirlineHomeComponent } from './home.component';
import { AirlineDashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './profile/user-profile.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { AirlineCreateFormComponent } from './create/airline-create.component';
import { AirlineInfoComponent } from './info/airline-info.component';
import { AirlineInfoDashboardComponent } from './info/dashboard/info-dashboard.component';
import { AirlineInfoAircraftsComponent } from './info/aircrafts/info-aircrafts.component';
import { AlInfoBankHomeComponent } from './info/bank/al-info-bank-home.component';
import { AmsAlAircraftHomeComponent } from './aircraft/al-aircraft.component';
import { AmsAlAircraftTabsHomeComponent } from './aircraft/tabs/al-aircraft-tabs-home.component';
import { AirlineInfoHubsComponent } from './info/hubs/al-info-hubs.component';
import { AirlineCreateDashboardComponent } from './create-dashboard/dashboard.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: AirlineComponent,
    children: [
      {
        path: AppRoutes.Root, component: AirlineHomeComponent,
        children: [
          { path: AppRoutes.Root, pathMatch: 'full', component: AirlineDashboardComponent, canActivate: [AuthGuard] },
          { path: AppRoutes.profile, component: UserProfileComponent },
          { path: AppRoutes.create, component: AirlineCreateDashboardComponent },
          {
            path: AppRoutes.info, component: AirlineInfoComponent,
            children: [
              { path: AppRoutes.Root, pathMatch: 'full', component: AirlineInfoDashboardComponent },
              { path: AppRoutes.dashboard, component: AirlineInfoDashboardComponent },
              { path: AppRoutes.aircafts, component: AirlineInfoAircraftsComponent },
              { path: AppRoutes.bank, component: AlInfoBankHomeComponent },
              { path: AppRoutes.hubs, component: AirlineInfoHubsComponent },
            ]
          },
          { path: AppRoutes.aircaft, component: AmsAlAircraftHomeComponent, children: [
              { path: AppRoutes.Root, pathMatch: 'full', component: AmsAlAircraftTabsHomeComponent, canActivate: [AuthGuard], children: [
                  { path: ':acId', component: AmsAlAircraftHomeComponent, canActivate: [AuthGuard] }
                ]
              }
            ]
          },

        ]
      },
      // { path: AppRoutes.Dashboard, component: DashboardComponent },
    ]
  },
  // { path: 'copper-pricing', component: CopperPricingComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AirlineRoutingModule { }

export const routedComponents = [
  AirlineComponent,
  AirlineHomeComponent,
  AirlineDashboardComponent,
  AirlineInfoComponent,
  AirlineInfoDashboardComponent,
  AirlineInfoAircraftsComponent,
  AlInfoBankHomeComponent,
  AmsAlAircraftHomeComponent,
  AmsAlAircraftTabsHomeComponent,
  AirlineInfoHubsComponent,
  AirlineCreateDashboardComponent,
];
