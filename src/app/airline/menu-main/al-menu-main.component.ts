import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import { COMMON_IMG_LOGO_RED } from "src/app/@core/const/app-storage.const";
import { AmsAirline, BankHistory30Days } from "src/app/@core/services/api/airline/dto";
import { UserModel } from "src/app/@core/services/auth/api/dto";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { AmsCommTransactionService } from "src/app/@core/services/common/ams-comm-transaction.service";
import { SpinnerService } from "src/app/@core/services/spinner.service";
import { AmsAirlineEditDialog } from "src/app/@share/components/dialogs/airline-edit/airline-edit.dialog";
import { AmsUserNameEditDialog } from "src/app/@share/components/dialogs/user-edit/user-name-edit.dialog";
import { environment } from "src/environments/environment";
import { AirlineInfoService } from "../info/airline-info.service";
import packageJson from './../../../../package.json';

@Component({
    selector: 'ams-al-menu-main',
    templateUrl: './al-menu-main.component.html',
    // animations: [PageTransition]
})
export class AirlineMenuMainComponent implements OnInit, OnDestroy {

    @Output() sidebarToggle: EventEmitter<any> = new EventEmitter<any>();

    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    roots = AppRoutes;
    appVersion: string = packageJson.version;

    user: UserModel;
    userName: string;
    userChanged: Subscription;

    airline: AmsAirline;
    avatarUrl: string;
    canEdit: boolean;
    env: string;

    constructor(private router: Router,
        public dialogService: MatDialog,
        private spiner: SpinnerService,
        private cus: CurrentUserService,
        private trService: AmsCommTransactionService,
        private alService: AirlineInfoService,) { }

    ngOnInit(): void {

        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.env = environment.abreviation;
        this.airline = this.cus.airline;
        this.canEdit = this.cus.user.userId === this.airline?.userId;
        this.avatarUrl = this.cus.avatarUrl;
        let name = 'Guest';
        if (this.cus.user) {
            name = this.cus.user.userName ? this.cus.user.userName : this.cus.user.userEmail;
        }
        this.userName = name;
        // console.log('initFields -> cus.user:', this.cus.user);
    }

    //#region Action Methods

    public loadBankHistoryChanrt(): void {
        if (this.airline && this.airline.alId) {
            this.spiner.show();
            this.trService.loadBankHistoryChanrt(this.airline.alId)
                .subscribe((res: BankHistory30Days[]) => {
                    this.spiner.hide();
                    this.initFields();
                },
                    err => {
                        this.spiner.hide();
                        console.log('autenticate-> err: ', err);
                    });
        }

    }

    ediAirline(): void {
        const dialogRef = this.dialogService.open(AmsAirlineEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                airline: this.airline,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('ediAirline-> res:', res);
            if (res) {
                this.alService.airline = res;
            }
            this.initFields();
        });
    }

    edituser(data: UserModel) {

        const dialogRef = this.dialogService.open(AmsUserNameEditDialog, {
            width: '721px',
            //height: '520px',
            data: {
                user: data,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('edituser-> res:', res);
            if (res) {

            }
            this.initFields();
        });

    }

    //#endregion

}
