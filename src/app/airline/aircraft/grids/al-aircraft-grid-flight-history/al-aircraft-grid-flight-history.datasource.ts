import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightTableData } from 'src/app/@core/services/api/flight/dto';
import { AmsAirlineAircraftService } from '../../al-aircraft.service';

export const KEY_CRITERIA = 'ams_al_aircraft_flight_history_criteria';

@Injectable({providedIn:'root'})
export class AmsAlAircraftGridFlightHistoryDataSource extends DataSource<AmsFlight> {

    seleted: AmsFlight;

    private _data: Array<AmsFlight>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsFlight[]> = new BehaviorSubject<AmsFlight[]>([]);
    listSubject = new BehaviorSubject<AmsFlight[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsFlight> {
        return this._data;
    }

    set data(value: Array<AmsFlight>) {
        this._data = value;
        this.listSubject.next(this._data as AmsFlight[]);
    }

    constructor(
        private cus: CurrentUserService,
        private flClient: AmsFlightClient,
        private acService: AmsAirlineAircraftService,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsFlightTableCriteria>();

    public get criteria(): AmsFlightTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsFlightTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsFlightTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsFlightTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'atime';
            obj.sortDesc = true;
            obj.alId = this.cus.isAdmin?undefined:this.cus.airline.alId;
            obj.acId = this.acService.aircraft?this.acService.aircraft.acId:undefined;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsFlightTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsFlight[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsFlight>> {
        
        this.isLoading = true;
        
        return new Observable<Array<AmsFlight>>(subscriber => {

            this.flClient.flightsHistoryTableP(this.criteria)
                .then((resp: AmsFlightTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('flightsHistoryTable-> resp=', resp);
                    this.data = new Array<AmsFlight>();
                    this.itemsCount = resp.rowsCount?resp.rowsCount:0;
                    if (resp && resp.flights && resp.flights.length > 0) {
                        this.data = resp.flights
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);
                    this.isLoading = false;
                }, msg => {
                    console.log('flightsHistoryTable -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsFlight>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
