import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraft, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight, AmsFlightTableCriteria } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { IAmsFlightsEstimateDto } from 'src/app/@share/components/dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { AmsAirlineAircraftService } from '../../al-aircraft.service';
import { AmsAlAircraftGridFlightHistoryDataSource } from './al-aircraft-grid-flight-history.datasource';


@Component({
    selector: 'ams-al-aircraft-grid-flight-history',
    templateUrl: './al-aircraft-grid-flight-history.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class AmsAlAircraftGridFlightHistoryComponent implements OnInit {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsFlight>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    
    //@Output() charterEstimate: EventEmitter<IAmsFlightsEstimateDto> = new EventEmitter<IAmsFlightsEstimateDto>();


    airport: AmsAirport;

    aircraft: AmsAircraft;
    aircraftChanged: Subscription;

    updateFlightQueue: Subscription;

    criteria: AmsFlightTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;
    // 'atime','flDescription'
    displayedColumns = ['flId', 'dtime',  'payloads', 'distanceKm', 'delayMin',  'revenue', 'income', 'expences'];
    airline: AmsAirline;

    get selFlitgh():AmsFlight{
        return this.acService.flight;
    }

    set selFlight(flh:AmsFlight){
        if(flh){
            this.acService.flight = flh;
        }
        //console.log('selFlight -> flh:', this.selFlight);
    }
    selectFlight(flh:AmsFlight):void{
        if(flh && flh.flId){
            this.acService.flight = flh;
        }
    }

    isSelectedRow(flh:AmsFlight):boolean{
        let rv = false;
        if(this.acService.flight){
            rv = this.acService.flight.flId === flh?.flId;
        }
        return rv;
    }

    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acService: AmsAirlineAircraftService,
        public tableds: AmsAlAircraftGridFlightHistoryDataSource) {

    }

    

    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsFlight>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            //this.dataCount = this.tableds.itemsCount;
            //this.initFields();
        });
        this.aircraftChanged = this.acService.aircraftChanged.subscribe((ac: AmsAircraft) => {
            this.initFields();
            this.criteria.sortCol = undefined;
            this.criteria.offset = 0;
            this.criteria.acId = this.aircraft ? this.aircraft?.acId : undefined;
            this.tableds.criteria = this.criteria;
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsFlightTableCriteria) => {
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.criteria.sortCol = undefined;
        this.criteria.offset = 0;
        this.criteria.alId = this.cus.isAdmin?undefined:this.cus.airline.alId;
        this.criteria.acId = this.aircraft ? this.aircraft?.acId : undefined;
        this.tableds.criteria = this.criteria;
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.aircraftChanged) { this.aircraftChanged.unsubscribe(); }
        if (this.updateFlightQueue) { this.updateFlightQueue.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.aircraft = this.acService.aircraft;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 500);
    }

    setFilter(value: string) {

        this.criteria.filter = value?.trim();
        this.criteria.sortCol = undefined;
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.criteria.alId = this.cus.isAdmin?undefined:this.cus.airline.alId;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.criteria.sortCol = undefined;
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.criteria.alId = this.cus.isAdmin?undefined:this.cus.airline.alId;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    gotoflightt(flight: AmsFlight) {
        console.log('gotoflightt -> flight:' + flight);
    }

    gotoAircraft(aicraft: AmsAircraft) {
        console.log('gotoAircraft -> aicraft:' + aicraft);
    }


    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if(airport && airport.apId){
            this.cus.spmAirportPanelOpen.next(airport.apId);
          }
    }

    spmAircraftOpen(value: AmsAircraft) {
        if (value) {
            this.aliService.spmAircraftPanelOpen.next(value);
        }
    }

    spmFlqOpen(value: AmsFlight) {
        console.log('spmFlqOpen-> value:', value);
        if (value) {
            //this.flpService.spmFlqOpen.next(value);
        }
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);
        if (data) {
            //this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }
    //#endregion

    //#region Data

    /*
    gotoAirport(data: AmsAirport) {
       
  
        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }
  
    }*/

    //#endregion

}
