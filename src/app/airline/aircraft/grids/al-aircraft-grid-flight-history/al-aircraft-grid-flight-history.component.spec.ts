import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmsAlAircraftGridFlightHistoryComponent } from './al-aircraft-grid-flight-history.component';


describe('Ams -> airline -> Grid -> AmsAlAircraftGridFlightHistoryComponent', () => {
  let component: AmsAlAircraftGridFlightHistoryComponent;
  let fixture: ComponentFixture<AmsAlAircraftGridFlightHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsAlAircraftGridFlightHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsAlAircraftGridFlightHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
