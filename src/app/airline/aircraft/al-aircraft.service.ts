import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// -Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Nodels
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AmsAircraft, AmsAircraftStatDayData, AmsAircraftStatDayTableCriteria, AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AirlineService } from '../airline.service';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { AmsFlightPlanTransferTableData } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';


export const SR_LEFT_PANEL_IN_KEY = 'ams_al_ac_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_al_ac_tab_idx';
export const SR_SEL_AC_CFG_KEY = 'ams_al_ac_cfg_aircraft';
export const AL_FL_HIS_KEY = 'ams_al_fl_his';

@Injectable({
  providedIn: 'root',
})
export class AmsAirlineAircraftService {

  constructor(
    private cus: CurrentUserService,
    private acClient: AmsAircraftClient,
    private alClient: AmsAirlineClient) {
  }

  get airline(): AmsAirline {
    return this.cus.airline;
  }

  set airline(value: AmsAirline) {
    this.cus.airline = value
  }


  //#region Tree panel

  panelInChanged = new BehaviorSubject<boolean>(true);

  get panelIn(): boolean {
    let rv = true;
    const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
    if (valStr) {
      rv = JSON.parse(valStr) as boolean;
    }
    return rv;
  }

  set panelIn(value: boolean) {
    localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
    this.panelInChanged.next(value);
  }

  //#endregion

  //#region Tab Idx

  tabIdxChanged = new BehaviorSubject<number>(undefined);

  get tabIdx(): number {
    let rv = 0;
    const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
    //console.log('selFolderId-> dataStr', dataStr);
    if (dataStr) {
      try {
        rv = parseInt(dataStr, 10);
      }
      catch {
        localStorage.removeItem(SR_SEL_TABIDX_KEY);
        rv = 1;
      }

    }
    // console.log('selTabIdx-> rv', rv);
    return rv;
  }

  set tabIdx(value: number) {
    // console.log('selTabIdx->', value);
    const oldValue = this.tabIdx;

    localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
    if (oldValue !== value) {
      this.tabIdxChanged.next(value);
    }
  }

  //#endregion

  //#region Aircraft

  aircraftChanged = new BehaviorSubject<AmsAircraft>(undefined);

  get aircraft(): AmsAircraft {
    let rv: AmsAircraft
    const dataStr = localStorage.getItem(SR_SEL_AC_CFG_KEY);
    if (dataStr) {
      try {
        rv = AmsAircraft.fromJSON(JSON.parse(dataStr));
      }
      catch {
        localStorage.removeItem(SR_SEL_AC_CFG_KEY);
      }

    }
    return rv;
  }

  set aircraft(value: AmsAircraft) {
    // console.log('selTabIdx->', value);
    const oldValue = this.aircraft;
    if(value){
      localStorage.setItem(SR_SEL_AC_CFG_KEY, JSON.stringify(value));
    }else{
      localStorage.removeItem(SR_SEL_AC_CFG_KEY);
    }
    
    this.aircraftChanged.next(value);
  }

  flightChanged = new BehaviorSubject<AmsFlight>(undefined);

  get flight(): AmsFlight {
    let rv: AmsFlight
    const dataStr = localStorage.getItem(AL_FL_HIS_KEY);
    if (dataStr) {
      try {
        rv = AmsFlight.fromJSON(JSON.parse(dataStr));
      }
      catch {
        localStorage.removeItem(AL_FL_HIS_KEY);
      }

    }
    return rv;
  }

  set flight(value: AmsFlight) {
    // console.log('selTabIdx->', value);
    const oldValue = this.flight;

    localStorage.setItem(AL_FL_HIS_KEY, JSON.stringify(value));
    if (oldValue && value && oldValue.flId !== value.flId) {
      this.flightChanged.next(value);
    }
  }
  //#endregion

  //#region Aircraft Day Stat

  _dayStatCriteria: AmsAircraftStatDayTableCriteria

  dayStatCriteriaChanged = new BehaviorSubject<AmsAircraftStatDayTableCriteria>(undefined);

  get dayStatCriteria(): AmsAircraftStatDayTableCriteria {
    let rv: AmsAircraftStatDayTableCriteria = this._dayStatCriteria;

    if (!this._dayStatCriteria) {
      rv = new AmsAircraftStatDayTableCriteria();
      rv.alId = this.cus.airline.alId;
      rv.acId = this.aircraft.acId;
      rv.limit = 7;
      rv.offset = 0;
      rv.pageIndex = 0;
      this._dayStatCriteria = rv;
    }
    return rv;
  }

  set dayStatCriteria(value: AmsAircraftStatDayTableCriteria) {
    const oldValue = this.dayStatCriteria;
    this._dayStatCriteria = value

    if (oldValue && value && oldValue.acId !== value.acId) {
      this.dayStatCriteriaChanged.next(value);
    }
  }

  public loadAircraftDayStat(): Observable<AmsAircraftStatDayData> {

    return new Observable<AmsAircraftStatDayData>(subscriber => {

      this.acClient.getAcStatDayTable(this.dayStatCriteria)
        .subscribe((resp: AmsAircraftStatDayData) => {
          if (resp) {
            subscriber.next(resp);
          } else {
            subscriber.next(undefined);
          }
        },
          err => {
            throw err;
          });
    });

  }

  //#region Data Load

  public loadAircraft(acId: number): Observable<AmsAircraft> {

    return new Observable<AmsAircraft>(subscriber => {

      this.acClient.aircraftGet(acId)
        .subscribe((resp: AmsAircraft) => {
          if (resp) {

            subscriber.next(resp);
          } else {
            subscriber.next(undefined);
          }
        },
          err => {
            throw err;
          });
    });

  }

  //#endregion


}
