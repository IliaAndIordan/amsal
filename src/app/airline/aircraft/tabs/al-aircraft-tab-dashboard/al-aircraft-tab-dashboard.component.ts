import { Component, Input, OnDestroy, type OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AmsTransactionsSum } from 'src/app/@core/services/api/airline/transaction';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AirlineService } from 'src/app/airline/airline.service';
import { AmsAirlineAircraftService } from '../../al-aircraft.service';
import { AmsAircraftStatDay, AmsAircraftStatDayData } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { GridAlAircraftRevenueDataSource } from 'src/app/@share/components/grids/grid-al-aircraft-revenue-table/grid-al-aircraft-revenue-table.datasource';

@Component({
  selector: 'ams-al-aircraft-tab-dashboard',
  templateUrl: './al-aircraft-tab-dashboard.component.html',
})
export class AmsAlAircraftTabDashboardComponent implements OnInit, OnDestroy {

  @Input() tabIdx: number;

  statDayLastWeekChanged: Subscription;
  statDayLastWeekData = [
    { data: [0, 0, 0, 0], label: 'Profit' },
  ];
  statDayLastWeekLabels: any[] =  ['January', 'February', 'Mars', 'April'];
  statDay: AmsAircraftStatDay[];

  selTabIdx: number;
  tabIdxChanged: Subscription;

  dataChanged:Subscription;

  acId:number;
  timerMinOne:Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private preloader: SpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private acClient: AmsAircraftClient,
    private alService: AirlineService,
    private acService: AmsAirlineAircraftService,
    public tableds: GridAlAircraftRevenueDataSource) {

  }


  ngOnInit(): void {

    this.timerMinOne = this.cus.timerMinOneSubj.subscribe(min => {
      console.log('timerMinOneSubj -> min', min);
    });

    this.tabIdxChanged = this.acService.tabIdxChanged.subscribe(tabIdx => {
      this.initFields();
      if (this.tabIdx === this.selTabIdx) {
        this.getStatDayLastWeek();
      }
    });
    this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsAircraftStatDay>) => {
      // console.log('MyCompanyUsersComponent:userListSubject()->', users);
      this.statDay = this.tableds.data;
      this.statDayLastWeekData = AmsAircraftStatDay.getLineChartData(this.statDay);
      this.statDayLastWeekLabels = AmsAircraftStatDay.getLineChartLabels(this.statDay);
      
  });
    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.timerMinOne) { this.timerMinOne.unsubscribe(); }
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
    if (this.dataChanged) { this.dataChanged.unsubscribe(); }
  }

  initFields(): void {
    this.selTabIdx = this.acService.tabIdx;

  }

  getStatDayLastWeek(): void {
    this.acId = this.acService.aircraft.acId;
    const criteria = this.acService.dayStatCriteria;
    criteria.alId = this.cus.airline.alId;
    criteria.acId = this.acService.aircraft.acId;
    criteria.offset = 0;
    criteria.limit = 14;
    //this.preloader.show();
    //console.log('getStatDayLastWeek-> criteria2: ', criteria);
    /*
    const statDay$ = this.acClient.getAcStatDayTable(criteria);
    statDay$.subscribe(res => {
      this.preloader.hide();
      this.statDay = res;
      //console.log('getStatDayLastWeek-> statDay: ', this.statDay);
      this.statDayLastWeekData = AmsAircraftStatDay.getLineChartData(this.statDay.rows);
      this.statDayLastWeekLabels = AmsAircraftStatDay.getLineChartLabels(this.statDay.rows);
    });
    */
  }

  onStatDayLastWeekChartClick(event: any): void {
    console.log('onStatDayLastWeekChartClick-> event: ', event);
  }
}
