import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlAircraftTabFightHistoryComponent } from './al-aircraft-tab-fight-history.component';

describe('AlAircraftTabFightHistoryComponent', () => {
  let component: AlAircraftTabFightHistoryComponent;
  let fixture: ComponentFixture<AlAircraftTabFightHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlAircraftTabFightHistoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AlAircraftTabFightHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
