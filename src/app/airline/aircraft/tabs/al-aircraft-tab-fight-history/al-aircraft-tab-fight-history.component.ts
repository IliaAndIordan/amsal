import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AirlineService } from 'src/app/airline/airline.service';
import { AmsAirlineAircraftService } from '../../al-aircraft.service';

@Component({
  selector: 'ams-al-aircraft-tab-fight-history',
  templateUrl: './al-aircraft-tab-fight-history.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class AlAircraftTabFightHistoryComponent implements OnInit {

  @Input() tabindex: string;

  panelIChanged: Subscription;

  manufacturer: AmsManufacturer;
  manufacturerChanged: Subscription;
  mac: AmsMac;
  macChanged: Subscription;

  selTabIdx: number;
  tabIdxChanged: Subscription;

  flight: AmsFlight;
  flightChanged: Subscription;

  get tabIdx(): number {
    return parseInt(this.tabindex);
  }

  get panelIn(): boolean {
    return this.acService.panelIn;
  }

  set panelIn(value: boolean) {
    this.acService.panelIn = value;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private preloader: SpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private alService: AirlineService,
    private acService: AmsAirlineAircraftService) {

  }


  ngOnInit(): void {

    this.panelIChanged = this.acService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });

    this.tabIdxChanged = this.acService.tabIdxChanged.subscribe(tabIdx => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });
    this.flightChanged = this.acService.flightChanged.subscribe(flight => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });


    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

  }

  initFields() {
    this.selTabIdx = this.acService.tabIdx;
    this.preloader.hide();
    this.flight = this.acService.flight;
  }

  //#region Mobile dialog methods

  //#endregion    

  //#region  Tab Panel Actions

  selectedTabChanged(tabIdx: number) {
    const oldTab = this.selTabIdx;
    this.selTabIdx = tabIdx;
    this.acService.tabIdx = this.selTabIdx;
  }


  //#endregion

  //#region SPM Methods
  panelInOpen() {
    this.acService.panelIn = true;
  }

  //#endregion

}
