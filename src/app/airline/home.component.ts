import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/const/animation.const';
// --- Models
import { AppRoutes } from '../@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from '../@core/const/app-storage.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger } from '../@core/const/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AmsSidePanelModalComponent } from '../@share/components/common/side-panel-modal/side-panel-modal';
import { AmsAirport } from '../@core/services/api/airport/dto';
import { AirlineService } from './airline.service';
import { AmsCountry, AmsState } from '../@core/services/api/country/dto';
import { IInfoMessage, InfoMessageDialog } from '../@share/components/dialogs/info-message/info-message.dialog';
import { AmsAirline } from '../@core/services/api/airline/dto';
import { ApiAuthClient } from '../@core/services/auth/api/api-auth.client';
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { AmsFlight } from '../@core/services/api/flight/dto';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { SxSidebarNavComponent } from '../@share/components/common/sidebar/nav/sx-sidebar-nav.component';

@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab]
})
export class AirlineHomeComponent implements OnInit, OnDestroy {

  @ViewChild('spmAirport') spmAirport: AmsSidePanelModalComponent;
  @ViewChild('spmAirline') spmAirline: AmsSidePanelModalComponent;
  @ViewChild('spmFlh') spmFlh: AmsSidePanelModalComponent;
  @ViewChild('sxSide') sxSide: SxSidebarNavComponent;
  
  
  @ViewChild('snav') snav: MatSidenav;
  
  
  sideNavState: string = Animate.open;
  snavmode: MatDrawerMode = 'side';
  
  country: AmsCountry;
  state: AmsState;
  
  airport: AmsAirport;
  airportPanelOpen: Subscription;
  spmFlhPanelOpen: Subscription;

  airline: AmsAirline;
  airlinePanelOpen: Subscription;
  spmAl: AmsAirline;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';
  // --- Side Tab
  expandTabVar: string = Animate.in;
  showTabContentsVar: string = Animate.hide;
  total: number = 0;
  aggressiveTotal: number = 0;

  // public dialogService: MatDialog
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private authClient:ApiAuthClient,
    private alService: AirlineService,
    private cus: CurrentUserService,
    public dialogService: MatDialog) { }


  ngOnInit(): void {

    this.airportPanelOpen = this.alService.airportPanelOpen.subscribe((airport: AmsAirport) => {
      this.spmAirportOpen(airport);
    });
    this.airlinePanelOpen = this.alService.airlinePanelOpen.subscribe((al: AmsAirline) => {
      this.spmAirlineOpen(al);
    });
    this.authClient.getUserSettingsP().then(res =>{
      //console.log('getUserSettings -> res:', res);
    });
    this.spmFlhPanelOpen = this.alService.spmFlhPanelOpen.subscribe((flh: AmsFlight) => {
      this.spmFlhOpen(flh);
    });
  }

  ngOnDestroy(): void {
    if (this.airportPanelOpen) { this.airportPanelOpen.unsubscribe(); }
    if (this.airlinePanelOpen) { this.airlinePanelOpen.unsubscribe(); }
    if (this.spmFlhPanelOpen) { this.spmFlhPanelOpen.unsubscribe(); }
  }

  exapandTabClick() {
    this.expandTabVar = this.expandTabVar === Animate.in ? Animate.out : Animate.in;
    this.showTabContentsVar = this.showTabContentsVar === Animate.show ? Animate.hide : Animate.show;
  }

  loginDialogShow(): void {
    /*
    const dialogRef = this.dialogService.open(LoginModal, {
      width: '500px',
      height: '340px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });
    */
  }

  

  //#region SPM

  showMessage(data: IInfoMessage) {
    console.log('showMessage-> data:', data);
    
    const dialogRef = this.dialog.open(InfoMessageDialog, {
        width: '721px',
        height: '320px',
        data: data
    });

    dialogRef.afterClosed().subscribe(res => {
        console.log('showMessage-> res:', res);
        if (res) {
            
        }
    });
    
}

sidebarToggle() {
  this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
}

  spmAirportClose(): void {
    if (this.spmAirport) {
      this.spmAirport.closePanel();
    }
  }

  spmAirportOpen(airport: AmsAirport) {
    this.airport = airport;
    if (this.airport && this.spmAirport) {
      this.spmAirport.expandPanel();
    }
    else {
      this.spmAirportClose();
    }

  }

  spmAirlineClose(): void {
    if (this.spmAirline) {
      this.spmAirline.closePanel();
    }
  }

  spmAirlineOpen(al: AmsAirline) {
    this.spmAl = al;
    
    if (this.spmAl && this.spmAirline) {
      this.showMessage({title:'Not Implemented', message:'SPM component not implemented'});
      //this.spmAirline.expandPanel();
    }
    else {
      //this.spmAirlineClose();
      
    }

  }

  //#endregion

  //#region  Expand/Close Menu


  snavToggleClick() {
    console.log('snavToggleClick-> sxSide', this.sxSide);
    if(this.sxSide){
      this.sxSide.toggle();
    }
  }

  //#endregion

   //#region SPM

   spmFlhObj: AmsFlight;
   spmFlhClose(): void {
     if (this.spmFlh) {
       this.spmFlh.closePanel();
     }
   }
   
   spmFlhOpen(ac: AmsFlight) {
     this.spmFlhObj = ac;
     if (this.spmFlh && this.spmFlhObj) {
       this.spmFlh.expandPanel();
     }
     else {
       this.spmFlhClose();
     }
   }

   //#endregion


}
