import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AppStore } from '../@core/const/app-storage.const';
import { AmsAircraft } from '../@core/services/api/aircraft/dto';
import { AmsAirlineClient } from '../@core/services/api/airline/api-client';
import { AmsAirline } from '../@core/services/api/airline/dto';
import { AmsAirportClient } from '../@core/services/api/airport/api-client';
import { AmsAirport } from '../@core/services/api/airport/dto';
import { AmsFlight } from '../@core/services/api/flight/dto';
import { SxApiProjectClient } from '../@core/services/api/project/api-client';
// -Services
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { AmsAirlineAircraftService } from './aircraft/al-aircraft.service';

export const SR_LEFT_PANEL_IN_KEY = 'ams_airline_dashboard_filter_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_airline_dashboard_filter_panel_tab_idx';


@Injectable({
    providedIn: 'root',
})
export class AirlineService {

    airlinePanelOpen = new BehaviorSubject<AmsAirline>(undefined);
    airportPanelOpen = new BehaviorSubject<AmsAirport>(undefined);
    spmFlhPanelOpen = new BehaviorSubject<AmsFlight>(undefined);

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private apClient: AmsAirportClient,
        private acService: AmsAirlineAircraftService) {
    }

    //#region Airline

    get airline(): AmsAirline {
        return this.cus.airline;
    }

    set airline(value: AmsAirline) {
        this.cus.airline = value
    }

    get hqAirport(): AmsAirport {
        return this.cus.hqAirport;
    }

    set hqAirport(value: AmsAirport) {
        this.cus.hqAirport = value;
    }

    get aircraftCfg(): AmsAircraft {
        return this.acService.aircraft;
    }

    set aircraftCfg(value: AmsAircraft) {
        this.acService.aircraft = value
    }

    //#endregion

    //#region Dashboard Filter Panel

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }


    //#endregion

    //#region Data

    public loadAirline(alId: number): Observable<AmsAirline> {

        return new Observable<AmsAirline>(subscriber => {

            this.alClient.airlineGet(alId)
                .subscribe((resp: AmsAirline) => {
                    subscriber.next(resp);
                },
                    err => {

                        throw err;
                    });
        });

    }

    solrAirport(apId: number): Observable<AmsAirport> {

        return new Observable<AmsAirport>(subscriber => {

            this.apClient.geSolrAirport(apId)
                .subscribe((resp: AmsAirport) => {
                    console.log('geSolrAirport-> resp', resp);
                    subscriber.next(resp);
                },
                    err => {

                        throw err;
                    });
        });

    }
    //#endregion


}
