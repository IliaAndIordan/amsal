

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Models
import { environment } from 'src/environments/environment';
import { URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AirlineInfoService } from '../info/airline-info.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { Animate } from 'src/app/@core/const/animation.const';
import { IInfoMessage, InfoMessageDialog } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { MatDialog } from '@angular/material/dialog';
import { AmsAirlineDashboarFlightdDataSource } from './dashboard-flight.datasource';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsFlight, AmsFlightTableCriteria } from 'src/app/@core/services/api/flight/dto';
import { AmsAirlineDashboardDeparturesDataSource } from './dashboard-departures.datasource';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from 'src/app/@core/const/animations-triggers';

export const SR_LEFT_PANEL_IN_KEY = 'ams_al_home_arr_dep_left_panel_in';

@Component({
    templateUrl: './dashboard.component.html',
    animations: [PageTransition,
        ShowHideTriggerBlock,
        ShowHideTriggerFlex,
        SpinExpandIconTrigger,
        ExpandTab, TogleBtnTopRev]
    })
export class AirlineDashboardComponent implements OnInit, OnDestroy {

    in = Animate.in;
    logo = '../../../assets/images/common/trimble-logo-white.png';
    airportImg = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';
    icaoImg = URL_COMMON_IMAGE_AMS_COMMON + 'icao.png';
    noLogoUrl = URL_COMMON_IMAGE_AIRLINE + 'logo_0.png';
    bankImg = URL_COMMON_IMAGE_AMS_COMMON + 'Bank.png';
    rootes = AppRoutes;

    redirectTo: string;
    copyrightCurrentYear = new Date().getFullYear();
    copyrightStartYear = 2018;
    errMsg: string;
    env: string;


    user: UserModel;
    userChanged: Subscription;
    avatarUrl: string;

    airline: AmsAirline;
    airlineChanged: Subscription;
    alCreated: boolean;
    logoUrl: string;
    liveryUrl: string;
    hqAirport: AmsAirport;

    flightsCount = 0;
    flights: AmsFlight[];
    flightsChanged: Subscription;
    interval: any;

    flightsCriteria: AmsFlightTableCriteria;
    flightsCriteriaChanged: Subscription;

    departuresCount = 0;
    departures: AmsFlight[];
    departuresChanged: Subscription;

    departuresCriteria: AmsFlightTableCriteria;
    departuresCriteriaChanged: Subscription;


    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private apCache: AmsAirportCacheService,
        private acCache: AmsAirlineAircraftCacheService,
        private alService: AirlineInfoService,
        public flightsds: AmsAirlineDashboarFlightdDataSource,
        public departuresds: AmsAirlineDashboardDeparturesDataSource) {

    }


    ngOnInit(): void {
        this.env = environment.abreviation;

        this.panelInVar = this.panelIn ? Animate.out : Animate.in;
        this.panelInChanged = this.alService.panelInChanged.subscribe((panelIn: boolean) => {
          this.panelInVar = this.panelIn ? Animate.out : Animate.in;
        });
    
        this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
            this.initFields();
        });

        this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
            this.initFields();
        });

        this.flightsChanged = this.flightsds.listSubject.subscribe((mscs: Array<AmsFlight>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.flightsCount = this.flightsds.itemsCount;
            this.initFields();
        });
        this.flightsCriteriaChanged = this.flightsds.criteriaChanged.subscribe((criteria: AmsFlightTableCriteria) => {
            this.flightsCriteria = this.flightsds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.departuresChanged = this.departuresds.listSubject.subscribe((mscs: Array<AmsFlight>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.departuresCount = this.departuresds.itemsCount;
            this.initFields();
        });
        this.departuresCriteriaChanged = this.departuresds.criteriaChanged.subscribe((criteria: AmsFlightTableCriteria) => {
            this.departuresCriteria = this.departuresds.criteria;
            this.initFields();
            this.loadPage();
        });
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.interval = setInterval(() => {
            //console.log('interval ->');
            this.loadPage();
        }, 60_000);

        this.initFields();
        this.loadAirport();
    }



    ngOnDestroy(): void {
        if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
        if (this.userChanged) { this.userChanged.unsubscribe(); }
        if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
        if (this.flightsChanged) { this.flightsChanged.unsubscribe(); }
        if (this.flightsCriteriaChanged) { this.flightsCriteriaChanged.unsubscribe(); }
        if (this.departuresChanged) { this.departuresChanged.unsubscribe(); }
        if (this.departuresCriteriaChanged) { this.departuresCriteriaChanged.unsubscribe(); }
        if (this.interval) { clearInterval(this.interval); }
    }

    initFields() {
        this.user = this.cus.user;
        this.avatarUrl = this.cus.avatarUrl;
        this.airline = this.cus.airline;
        this.alCreated = this.airline && this.airline.apId ? true : false;
        this.hqAirport = this.cus.hqAirport;
        this.logoUrl = this.airline?.logo ? this.airline.logo : this.airline?.amsLogoUrl;
        this.liveryUrl = this.airline?.amsLiveryUrl;

        this.flightsCriteria = this.flightsds.criteria;
        this.flightsCount = this.flightsds.itemsCount;
        this.flights = this.flightsds.data;

        this.departuresCriteria = this.departuresds.criteria;
        this.departuresCount = this.departuresds.itemsCount;
        this.departures = this.departuresds.data;
    }



    //#region  Actions

    onChartClick(event) {
        console.log('onChartClick -> event:', event);
    }

    showMessage(data: IInfoMessage) {
        console.log('showMessage-> data:', data);

        const dialogRef = this.dialogService.open(InfoMessageDialog, {
            width: '721px',
            height: '320px',
            data: data
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('showMessage-> res:', res);
            if (res) {

            }
        });

    }

    //#endregion

    //#region Filter Panel

    panelInVar = Animate.in;
    panelInChanged: Subscription;
    sidebarClass = 'sidebar-closed';

    get panelIn(): boolean {
        let rv = this.alService.panelIn;
        //this.panelInVar = rv ? Animate.out : Animate.in;
        return this.alService.panelIn;
    }

    set panelIn(value: boolean) {
        this.alService.panelIn = value;
        this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    }

    fpSubtitleClick(value: any): void {

    }
    sidebarToggle() {
        this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
    }

    closeFilterPanelClick(): void {
        this.panelIn = false;
    }

    expandFilterPanelClick(): void {
        this.panelIn = true;
    }

    //#endregion

    //#region Data

    airport$: Observable<AmsAirport>;
    aircrafts$: Observable<AmsAircraft[]>;
    loadAirport() {
        console.log()
        if (this.hqAirport) {
            this.airport$ = this.apCache.getAirport(this.hqAirport.apId);
            this.airport$.subscribe(airport => {
                this.loadPage();
            });

        }
    }

    loadPage() {
        //this.spinerService.display(true);

        this.flightsds.loadData()
            .subscribe(res => {
                this.flightsCount = this.flightsds.itemsCount;
            });
        this.departuresds.loadData()
            .subscribe(res => {
                this.departuresCount = this.departuresds.itemsCount;
            });
        this.aircrafts$ = this.acCache.reloadCache();
        this.aircrafts$.subscribe(aircrafts => {
            //this.spinerService.display(false);
            this.initFields();
        });
    }


    //#endregion
}
