import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AirlineDashboardFilterPanelComponent } from './airline-dashboard-filter-panel.component';

describe('AirlineDashboardFilterPanelComponent', () => {
  let component: AirlineDashboardFilterPanelComponent;
  let fixture: ComponentFixture<AirlineDashboardFilterPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AirlineDashboardFilterPanelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AirlineDashboardFilterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
