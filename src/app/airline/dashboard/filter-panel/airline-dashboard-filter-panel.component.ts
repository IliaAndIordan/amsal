import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineEditDialog } from 'src/app/@share/components/dialogs/airline-edit/airline-edit.dialog';
import { IInfoMessage } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { AmsWadService } from 'src/app/wad/wad.service';
import { AirlineService } from '../../airline.service';

@Component({
  selector: 'ams-airline-dashboard-filter-panel',
  templateUrl: './airline-dashboard-filter-panel.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class AirlineDashboardFilterPanelComponent implements OnInit, OnDestroy {

  @Input() arrivals: AmsFlight[];
  @Input() departures: AmsFlight[];
  @Output() showMessage: EventEmitter<IInfoMessage> = new EventEmitter<IInfoMessage>();
  @Output() refreshData: EventEmitter<any> = new EventEmitter<any>();

  imgUrlFlight = URL_COMMON_IMAGE_AMS_COMMON + 'flight.png';
  imgUrlFlq = URL_COMMON_IMAGE_AMS_COMMON + 'route_markers_red.png';
  routs = AppRoutes;
  airline: AmsAirline;
  hqAirport: AmsAirport;
  canEdit:boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinnerService: SpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private wadService: AmsWadService,
    private trService: AmsCommTransactionService,
    private alService: AirlineService) {
  }


  ngOnInit(): void {
    this.tabIdxChanged = this.alService.tabIdxChanged.subscribe(tabIdx => {
      this.initFields();
    });


    this.initFields();
  }

  ngOnDestroy(): void {
    //if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }

  }

  
  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['arrivals']) {
      //this.flight = changes['flight'].currentValue;
      this.initFields();
    }
  }

  initFields() {
    this.selTabIdx = this.alService.tabIdx;
    this.airline = this.alService.airline;
    this.hqAirport = this.alService.hqAirport;
    this.canEdit = this.cus.user.userId === this.airline?.userId;
  }

  //#region Tab Idx
  selTabIdx: number;
  tabIdxChanged: Subscription;


  selectedTabChanged(tabIdx: number) {
    const oldTab = this.selTabIdx;
    this.selTabIdx = tabIdx;
    this.alService.tabIdx = this.selTabIdx;
  }

  //#endregion
  
  //#region Action Methods

  refreshDataClick() {
    this.refreshData.emit(true);
  }

  showMessageDialog(tit: string, msg: string): void {
    this.showMessage.emit({ title: tit, message: msg });
  }
  ediAirline(): void {
    const dialogRef = this.dialogService.open(AmsAirlineEditDialog, {
      width: '721px',
      height: '520px',
      data: {
        airline: this.airline,
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log('ediAirline-> res:', res);
      if (res) {
        this.alService.airline = res;
      }
      this.initFields();
    });
  }

  //#endregion


}
