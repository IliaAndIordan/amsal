import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightTableData } from 'src/app/@core/services/api/flight/dto';

export const KEY_CRITERIA = 'ams_al_dashboard_flight_map_criteria';
@Injectable({providedIn:'root'})
export class AmsAirlineDashboarFlightdDataSource extends DataSource<AmsFlight> {

    seleted: AmsFlight;

    private _data: Array<AmsFlight>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsFlight[]> = new BehaviorSubject<AmsFlight[]>([]);
    listSubject = new BehaviorSubject<AmsFlight[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsFlight> {
        return this._data;
    }

    set data(value: Array<AmsFlight>) {
        this._data = value;
        this.listSubject.next(this._data as AmsFlight[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private flClient: AmsFlightClient) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsFlightTableCriteria>();

    public get criteria(): AmsFlightTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsFlightTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsFlightTableCriteria(), JSON.parse(onjStr));
            if(!obj.alId || obj.alId !== this.cus.airline.alId){
                obj.alId = this.cus.airline.alId;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }
            if(this.cus.isAdmin){
                obj.alId = undefined;
            }
            
        } else {
            obj = new AmsFlightTableCriteria();
            obj.limit = 100;
            obj.offset = 0;
            obj.sortCol = 'etaTime';
            obj.sortDesc = false;
            obj.alId = this.cus.airline.alId;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsFlightTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsFlight[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsFlight>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<AmsFlight>>(subscriber => {

            this.flClient.flightsTable(this.criteria)
                .subscribe((resp: AmsFlightTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    this.data = new Array<AmsFlight>();

                    if (resp && resp.flights && resp.flights.length > 0) {
                        this.data = resp.flights
                        this.itemsCount = resp.rowsCount?resp.rowsCount:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsFlight>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
