import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from '../../../wad/wad.service';
// ---Models
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS_COMMON, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AirlineInfoService } from '../airline-info.service';
import { AmsAirline, BankHistory30Days } from 'src/app/@core/services/api/airline/dto';
import { AirlineService } from '../../airline.service';
import { IInfoMessage } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { AmsAirlineEditDialog } from 'src/app/@share/components/dialogs/airline-edit/airline-edit.dialog';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';


@Component({
    selector: 'ams-airline-info-filter-panel-body',
    templateUrl: './airline-info-filter-panel.component.html',
})
export class AirlineInfoFilterPanelBodyComponent implements OnInit, OnDestroy {

    @Output() showMessage: EventEmitter<IInfoMessage> = new EventEmitter<IInfoMessage>();

    routs = AppRoutes;
    airline: AmsAirline;
    airlineChanged: Subscription;

    logoUrl: string;
    liveryUrl: string;
    fleetUrl =  URL_COMMON_IMAGE_AMS_COMMON + 'fleet.png';
    bankImg = URL_COMMON_IMAGE_AMS_COMMON + 'Bank.png';
    hubUrl =  URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';
    hqAirport: AmsAirport;


    tabIdx: number;
    tabIdxChanged: Subscription;
    ourApUrl: string;
    canEdit: boolean;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinnerService: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService: AmsWadService,
        private trService: AmsCommTransactionService,
        private alService: AirlineService,
        private aliService: AirlineInfoService) {

    }


    ngOnInit(): void {

        this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
            this.initFields();
        });

        this.tabIdxChanged = this.aliService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.airline = this.aliService.airline;
        this.hqAirport = this.aliService.hqAirport;
        this.logoUrl = this.airline?.logo ? this.airline.logo : this.airline?.amsLogoUrl;
        this.liveryUrl = this.airline?.amsLiveryUrl;
        this.canEdit = this.cus.user?.userId === this.airline?.userId;
        this.tabIdx = this.aliService.tabIdx;
    }

    //#region Dialogs

    ediAirline() {
        const dialogRef = this.dialogService.open(AmsAirlineEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                airline: this.airline,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('ediAirline-> res:', res);
            if (res) {
                this.aliService.airline = res;
            }
            this.initFields();
        });
    }

    //#endregion

    //#region SPM

    showMessageDialog(tit: string, msg: string): void {
        this.showMessage.emit({ title: tit, message: msg });
    }

    spmAirportOpen(airport: AmsAirport) {
        this.alService.airportPanelOpen.next(airport);
    }
    spmAirlineOpen(al: AmsAirline) {
        this.alService.airlinePanelOpen.next(al);
    }

    refreshAirline(): void {
        if (this.airline) {
            this.spinnerService.show();
            this.loadBankHistoryChanrt();
            this.alService.loadAirline(this.airline.alId)
                .subscribe((res: AmsAirline) => {
                    this.cus.airline = res;
                });
        }
    }

    solrAirport(): void {
        if (this.airline && this.airline.apId) {
           
            this.aliService.solrAirport(this.airline.apId)
                .subscribe((res: any) => {
                   console.log('solrAirport -> responce:', res);
                });
        }
    }

    //#endregion

    //#region Chart

    loadBankHistoryChanrt(): void {
        this.spinnerService.show();
        this.trService.loadBankHistoryChanrt(this.airline.alId)
            .subscribe((res: BankHistory30Days[]) => {
                this.spinnerService.hide();
                this.initFields();
            },
                err => {
                    this.spinnerService.hide();
                    console.log('autenticate-> err: ', err);
                });

    }

    //#endregion

}
