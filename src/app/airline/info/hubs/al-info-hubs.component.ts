

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirline, BankHistory30Days, ResponseBankHistory30Days } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AirlineInfoService } from '../airline-info.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';


@Component({
    templateUrl: './al-info-hubs.component.html',
    // animations: [PageTransition]
})
export class AirlineInfoHubsComponent implements OnInit, OnDestroy {

    
    airportImg = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';
    icaoImg = URL_COMMON_IMAGE_AMS_COMMON + 'icao.png';
    noLogoUrl = URL_COMMON_IMAGE_AIRLINE + 'logo_0.png';
    rootes = AppRoutes;

    redirectTo: string;
    copyrightCurrentYear = new Date().getFullYear();
    copyrightStartYear = 2018;
    errMsg: string;
    env: string;


    user: UserModel;
    userChanged: Subscription;
    avatarUrl: string;

    airline: AmsAirline;
    airlineChanged: Subscription;
    alCreated: boolean;
    logoUrl: string;
    liveryUrl: string;
    hqAirport: AmsAirport;
    bankHistory30DaysChanged: Subscription;
    bankHistory30DaysData: ResponseBankHistory30Days;
    chartData = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabels = ['January', 'February', 'Mars', 'April'];

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private trService: AmsCommTransactionService,
        private alClient: AmsAirlineClient,
        private aService: AirlineInfoService) { }


    ngOnInit(): void {
        this.env = environment.abreviation;

        this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
            this.initFields();
        });

        this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
            this.initFields();
        });

        this.initFields();
    }



    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
        if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.avatarUrl = this.cus.avatarUrl;
        this.airline = this.cus.airline;
        this.alCreated = this.airline && this.airline.apId ? true : false;
        this.hqAirport = this.cus.hqAirport;
        this.logoUrl = this.airline?.logo ? this.airline.logo : this.airline?.amsLogoUrl;
        this.liveryUrl = this.airline?.amsLiveryUrl;
    }

    //#region  data

    //#endregion

    //#region  Actions

    

    //#endregion

   
}
