import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlInfoBankTabDashboardComponent } from './al-info-bank-tab-dashboard.component';

describe('AlInfoBankTabDashboardComponent', () => {
  let component: AlInfoBankTabDashboardComponent;
  let fixture: ComponentFixture<AlInfoBankTabDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlInfoBankTabDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlInfoBankTabDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
