import { CurrencyPipe } from '@angular/common';
import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirline, BankHistory30Days } from 'src/app/@core/services/api/airline/dto';
import { AmsTransactionsSum, AmsTransactionsSumLastWeekData, AmsTransactionTypeSum, AmsTransactionTypeSumData } from 'src/app/@core/services/api/airline/transaction';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { Ng2ChartJsCahartPieData, Ng2ChartJsCahartPieDataset } from 'src/app/@share/charts/ams-chart-line/dto';
import { environment } from 'src/environments/environment';
import { AirlineInfoService } from '../../../airline-info.service';

@Component({
  selector: 'ams-al-info-bank-tab-dashboard',
  templateUrl: './al-info-bank-tab-dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
  providers: [CurrencyPipe],
})
export class AlInfoBankTabDashboardComponent implements OnInit, OnDestroy {

  airline: AmsAirline;
  airlineChanged: Subscription;

  env: string;
  bankHistory30DaysChanged: Subscription;
  bh30DaysChartData = [
    { data: [330, 600, 260, 700], label: 'Account A' },
    { data: [120, 455, 100, 340], label: 'Account B' },
    { data: [45, 67, 800, 500], label: 'Account C' }
  ];

  bh30DaysChartLabels = ['January', 'February', 'Mars', 'April'];
  trSumLastWeekChanged: Subscription;
  trSumLastWeekData = [
    { data: [330, 600, 260, 700], label: 'Account A' },
    { data: [120, 455, 100, 340], label: 'Account B' },
    { data: [45, 67, 800, 500], label: 'Account C' }
  ];

  trSumLastWeekLabels = ['January', 'February', 'Mars', 'April'];

  trTypeSumTodayChanged: Subscription;
  trTypeSumTodayData: any;

  trTypeSumTodayLabels = ['January', 'February', 'Mars', 'April'];
  trTypeSumTodayColors: any[];

  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private alClient: AmsAirlineClient,
    private aService: AirlineInfoService,
    private currencyPipe: CurrencyPipe) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
      this.initFields();
    });

    this.bankHistory30DaysChanged = this.trService.bankHistory30DaysChanged.subscribe(data => {
      this.initFields();
    });

    this.trSumLastWeekChanged = this.trService.trSumLastWeekChanged.subscribe(data => {
      this.initFields();
    });

    this.trTypeSumTodayChanged = this.trService.trTypeSumTodayChanged.subscribe(data => {
      this.getTransactionsTypeSumToday();
    });

    this.initFields();
    this.loadTransactionsSumLastWeek();
    this.loadTransactionsTypeSumToday();
  }

  ngOnDestroy(): void {
    if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
    if (this.bankHistory30DaysChanged) { this.bankHistory30DaysChanged.unsubscribe(); }
    if (this.trSumLastWeekChanged) { this.trSumLastWeekChanged.unsubscribe(); }
    if (this.trTypeSumTodayChanged) { this.trTypeSumTodayChanged.unsubscribe(); }
  }

  initFields() {
    this.airline = this.cus.airline;
    this.getBankHistory30DaysChart();
    this.getTransactionsSumLastWeek();
    this.getTransactionsTypeSumToday();
  }


  //#region Action Methods

  onChartClick(event) {
    console.log('onChartClick -> event:', event);
  }

  onTrSumLastWeekChartClick(event) {
    console.log('onChartClick -> event:', event);
  }
  //#endregion

  //#region Data

  getTransactionsTypeSumToday(): void {
    let trTypeSumToday: AmsTransactionTypeSumData = this.trService.trTypeSumToday;
    if (trTypeSumToday) {
      const data = AmsTransactionTypeSum.dataToList(trTypeSumToday);
      let dataArr = AmsTransactionTypeSum.getPieChartData(data);
      this.trTypeSumTodayData = [{ data: dataArr }];
      this.trTypeSumTodayLabels = AmsTransactionTypeSum.getPieChartLabels(data, this.currencyPipe);
      this.trTypeSumTodayData[0].backgroundColor = AmsTransactionTypeSum.getPieChartColors(data);
      //console.log('getTransactionsTypeSumToday -> trTypeSumTodayData: ', this.trTypeSumTodayData);
    }
  }

  loadTransactionsTypeSumToday(): void {
    if (this.airline && this.airline.alId) {
      this.spinerService.show();
      this.trService.loadTrTypeSumToday(this.airline.alId)
        .subscribe((res: AmsTransactionTypeSum[]) => {
          //console.log('loadTransactionsTypeSumToday-> loadTrTypeSumToday: ', res);
          this.spinerService.hide();
          this.getTransactionsTypeSumToday();
        },
          err => {
            this.spinerService.hide();
            console.log('autenticate-> err: ', err);
          });
    }

  }

  getTransactionsSumLastWeek(): void {
    const trSumLastWeek = this.trService.trSumLastWeek;

    if (trSumLastWeek && trSumLastWeek.trSumList &&
      trSumLastWeek.trSumList.length > 0) {
      const data = AmsTransactionsSum.dataToList(trSumLastWeek);
      this.trSumLastWeekData = AmsTransactionsSum.getLineChartData(data);
      //console.log('getTransactionsSumLastWeek-> trSumLastWeekData: ', this.trSumLastWeekData);
      this.trSumLastWeekLabels = AmsTransactionsSum.getLineChartLabels(data);
    }
  }

  public loadTransactionsSumLastWeek(): void {
    if (this.airline && this.airline.alId) {
      this.spinerService.show();
      this.trService.loadTrSumLastWeek(this.airline.alId)
        .subscribe((res: AmsTransactionsSum[]) => {
          this.spinerService.hide();
          this.getTransactionsSumLastWeek();
        },
          err => {
            this.spinerService.hide();
            console.log('autenticate-> err: ', err);
          });
    }

  }

  getBankHistory30DaysChart(): void {
    const bankHistory30DaysData = this.trService.bankHistory30DaysData;
    if (bankHistory30DaysData && bankHistory30DaysData.data &&
      bankHistory30DaysData.data.rows) {
      const data = BankHistory30Days.dataToList(bankHistory30DaysData.data);
      this.bh30DaysChartData = BankHistory30Days.getLineChartData(data);
      this.bh30DaysChartLabels = BankHistory30Days.getLineChartLabels(data);
    } else {
      this.loadBankHistoryChanrt();
    }
  }

  public loadBankHistoryChanrt(): void {
    if (this.airline && this.airline.alId) {
      this.spinerService.show();
      this.trService.loadBankHistoryChanrt(this.airline.alId)
        .subscribe((res: BankHistory30Days[]) => {
          this.spinerService.hide();
          this.initFields();
        },
          err => {
            this.spinerService.hide();
            console.log('autenticate-> err: ', err);
          });
    }

  }
  //#endregion


}
