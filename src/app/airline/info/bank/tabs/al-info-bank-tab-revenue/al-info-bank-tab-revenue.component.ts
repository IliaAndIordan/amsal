import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AirlineInfoService } from '../../../airline-info.service';
import { environment } from 'src/environments/environment';
import { AmsRevenue } from 'src/app/@core/services/api/airline/transaction';

@Component({
  selector: 'ams-al-info-bank-tab-revenue',
  templateUrl: './al-info-bank-tab-revenue.component.html',
})
export class AlInfoBankTabRevenueComponent {

  @Input() tabIdx:number;

  airline: AmsAirline;
  airlineChanged: Subscription;

  env: string;

  selTabIdx: number;
  tabIdxChanged: Subscription;


  
  profitChartData = [
    { data: [0, 0, 0, 0], label: 'Profit' },
  ];

  profitChartLabels = ['January', 'February', 'Mars', 'April'];

  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private alClient: AmsAirlineClient,
    private aService: AirlineInfoService) {

  }

  ngOnInit(): void {
    this.env = environment.abreviation;
    this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
      this.initFields();
    });

    this.tabIdxChanged = this.aService.tabIdxChangedBank.subscribe(tabIdx => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });

    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
  }

  initFields() {
    this.selTabIdx = this.aService.tabIdxBank;
    this.airline = this.cus.airline;
    //this.getBankHistory30DaysChart();
  }

  fillRevenueChart(value:AmsRevenue[]):void{
    console.log('fillRevenueChart -> value:', value);
    if(value && value.length){
      this.profitChartData = AmsRevenue.getRevenueLineChartData(value);
      this.profitChartLabels = AmsRevenue.getRevenueLineChartLabels(value);
    }
  }

  onProfitChartClick(value:any):void{
    console.log('onProfitChartClick -> value:', value);
  }

}
