import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlInfoBankTabRevenueComponent } from './al-info-bank-tab-revenue.component';

describe('AlInfoBankTabRevenueComponent', () => {
  let component: AlInfoBankTabRevenueComponent;
  let fixture: ComponentFixture<AlInfoBankTabRevenueComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AlInfoBankTabRevenueComponent]
    });
    fixture = TestBed.createComponent(AlInfoBankTabRevenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
