import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlInfoBankHomeComponent } from './al-info-bank-home.component';

describe('AlInfoBankHomeComponent', () => {
  let component: AlInfoBankHomeComponent;
  let fixture: ComponentFixture<AlInfoBankHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlInfoBankHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlInfoBankHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
