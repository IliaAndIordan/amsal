import { Component, Directive, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirline, BankHistory30Days, ResponseBankHistory30Days } from 'src/app/@core/services/api/airline/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { environment } from 'src/environments/environment';
import { AirlineInfoService } from '../airline-info.service';

@Component({
  templateUrl: './al-info-bank-home.component.html',
})
export class AlInfoBankHomeComponent implements OnInit, OnDestroy {

  airline: AmsAirline;
  airlineChanged: Subscription;

  env: string;

  selTabIdx: number;
  tabIdxChanged: Subscription;


  bankHistory30DaysChanged: Subscription;
  bh30DaysChartData = [
    { data: [330, 600, 260, 700], label: 'Account A' },
    { data: [120, 455, 100, 340], label: 'Account B' },
    { data: [45, 67, 800, 500], label: 'Account C' }
  ];

  bh30DaysChartLabels = ['January', 'February', 'Mars', 'April'];

  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private alClient: AmsAirlineClient,
    private aService: AirlineInfoService) {

  }

  ngOnInit(): void {
    this.env = environment.abreviation;
    this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
      this.initFields();
    });

    this.tabIdxChanged = this.aService.tabIdxChangedBank.subscribe(tabIdx => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });

    this.bankHistory30DaysChanged = this.trService.bankHistory30DaysChanged.subscribe(data => {
      this.initFields();
    });

    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
    if (this.bankHistory30DaysChanged) { this.bankHistory30DaysChanged.unsubscribe(); }
  }

  initFields() {
    this.selTabIdx = this.aService.tabIdxBank;
    this.airline = this.cus.airline;
    this.getBankHistory30DaysChart();
  }

  //#region  Tab Panel Actions

  selectedTabChanged(tabIdx: number) {
    const oldTab = this.selTabIdx;
    this.selTabIdx = tabIdx;
    this.aService.tabIdxBank = this.selTabIdx;
  }


  //#endregion

  //#region Data

  getBankHistory30DaysChart(): void {
    const bankHistory30DaysData = this.trService.bankHistory30DaysData;
    if (bankHistory30DaysData && bankHistory30DaysData.data &&
      bankHistory30DaysData.data.rows) {
      const data = BankHistory30Days.dataToList(bankHistory30DaysData.data);
      this.bh30DaysChartData = BankHistory30Days.getLineChartData(data);
      this.bh30DaysChartLabels = BankHistory30Days.getLineChartLabels(data);
    } else {
      this.loadBankHistoryChanrt();
    }
  }

  public loadBankHistoryChanrt(): void {
    if (this.airline && this.airline.alId) {
    this.spinerService.show();
    this.trService.loadBankHistoryChanrt(this.airline.alId)
      .subscribe((res: BankHistory30Days[]) => {
        this.spinerService.hide();
        this.initFields();
      },
        err => {
          this.spinerService.hide();
          console.log('autenticate-> err: ', err);
        });
      }

  }

  //#endregion

}
