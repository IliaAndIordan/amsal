

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { Animate } from 'src/app/@core/const/animation.const';
import { AirlineInfoService } from './airline-info.service';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from 'src/app/@core/const/animations-triggers';
import { AirlineService } from '../airline.service';
import { IInfoMessage, InfoMessageDialog } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { MatDialog } from '@angular/material/dialog';
import { AmsSidePanelModalComponent } from 'src/app/@share/components/common/side-panel-modal/side-panel-modal';
import { AmsAircraft, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsFlightEstimateDialog, IAmsFlightEstimateDto } from 'src/app/@share/components/dialogs/flight/flight-estimate/flight-estimate.dialog';
import { AmsFlightEstimate, ResponseAmsFlightEstimate } from 'src/app/@core/services/api/flight/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlightCharterEstimateDialog, IAmsFlightsEstimateDto } from 'src/app/@share/components/dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';


@Component({
    templateUrl: './airline-info.component.html',
    animations: [PageTransition,
        ShowHideTriggerBlock,
        ShowHideTriggerFlex,
        SpinExpandIconTrigger,
        ExpandTab, TogleBtnTopRev]
})
export class AirlineInfoComponent implements OnInit, OnDestroy {

    @ViewChild('spmAirport') spmAirport: AmsSidePanelModalComponent;
    @ViewChild('spmAirline') spmAirline: AmsSidePanelModalComponent;
    @ViewChild('spmAcmMac') spmAcmMac: AmsSidePanelModalComponent;
    @ViewChild('spmAircraft') spmAircraft: AmsSidePanelModalComponent;

    in = Animate.in;
    spmApCountry: AmsCountry;
    spmApState: AmsState;

    spmAp: AmsAirport;
    airportPanelOpen: Subscription;
    spmAl: AmsAirline;
    spmAlPanelOpen: Subscription;
    spmAc: AmsAircraft;
    spmAircraftPanelOpen: Subscription;
    spmMac: AmsMac;
    spmAcmMacPanelOpen: Subscription;

    logo = '../../../assets/images/common/trimble-logo-white.png';
    airportImg = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';
    icaoImg = URL_COMMON_IMAGE_AMS_COMMON + 'icao.png';
    noLogoUrl = URL_COMMON_IMAGE_AIRLINE + 'logo_0.png';
    rootes = AppRoutes;

    redirectTo: string;
    copyrightCurrentYear = new Date().getFullYear();
    copyrightStartYear = 2018;
    errMsg: string;
    env: string;

    user: UserModel;
    userChanged: Subscription;
    avatarUrl: string;

    airline: AmsAirline;
    airlineChanged: Subscription;
    alCreated: boolean;
    logoUrl: string;
    liveryUrl: string;
    hqAirport: AmsAirport;


    panelInVar = Animate.in;
    panelInChanged: Subscription;
    sidebarClass = 'sidebar-closed';

    get panelIn(): boolean {
        let rv = this.aliService.panelIn;
        this.panelInVar = rv ? Animate.out : Animate.in;
        return this.aliService.panelIn;
    }

    set panelIn(value: boolean) {
        this.aliService.panelIn = value;
        this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    }

    chartDataUsers = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUsers = ['January', 'February', 'Mars', 'April'];

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private alService: AirlineService,
        private aliService: AirlineInfoService,
        private flClient: AmsFlightClient,
        private acClient: AmsAircraftClient) { }


    ngOnInit(): void {
        this.env = environment.abreviation;
        this.panelInVar = this.panelIn ? Animate.out : Animate.in;

        this.airportPanelOpen = this.aliService.spmAirportPanelOpen.subscribe((airport: AmsAirport) => {
            this.spmAirportOpen(airport);
        });
        this.spmAlPanelOpen = this.aliService.spmAirlinePanelOpen.subscribe((al: AmsAirline) => {
            this.spmAirlineOpen(al);
        });
        this.spmAcmMacPanelOpen = this.aliService.spmAcmMacPanelOpen.subscribe((mac: AmsMac) => {
            this.spmAcmMacOpen(mac);
        });

        this.spmAircraftPanelOpen = this.aliService.spmAircraftPanelOpen.subscribe((ac: AmsAircraft) => {
            this.spmAircraftOpen(ac);
        });

        this.panelInChanged = this.aliService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });
        this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
            this.initFields();
        });

        this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
            this.initFields();
        });
        this.initFields();
    }



    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
        if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
        if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.avatarUrl = this.cus.avatarUrl;
        this.airline = this.cus.airline;
        this.alCreated = this.airline && this.airline.apId ? true : false;
        this.hqAirport = this.cus.hqAirport;
        this.logoUrl = this.airline?.logo ? this.airline.logo : this.airline?.amsLogoUrl;
        this.liveryUrl = this.airline?.amsLiveryUrl;
    }

    //#region  data

    public gatGvdtTsoSearchHits(): void {
        /*
        const gvdt: GvDataTable = GvdtTsoSearchHits.GvdtTsoSearchHitsTotalDefinition();
        // console.log('getCompanyBaseInfo-> gvdt', gvdt);
        this.lService.getGvDataTable(gvdt)
            .subscribe((resp: GetGvDataTableResponse) => {
                console.log('gatGvdtTsoSearchHits-> resp', resp);
                this.gvdtHits = resp;
                this.chartData = GvdtTsoSearchHits.getLineChartData(resp.dataTable);
                this.chartLabels = GvdtTsoSearchHits.getLineChartLabels(resp.dataTable);
                console.log('chartData=', this.chartData);
            });
            */
    }
    //#endregion

    //#region  Actions

    generateLogoClick(airline: AmsAirline) {
        this.logoCreate(airline);
    }

    logoCreate(airline: AmsAirline) {

        if (airline && airline.alId) {
            this.spinerService.display(true);

            this.alClient.airlineLogo(this.airline.alId)
                .subscribe((res: AmsAirline) => {
                    this.spinerService.display(false);
                    //console.log('airlineLogo -> res:', res);
                    if (res && res.alId) {
                        this.cus.airline = res;
                        this.toastr.success('Airline', 'Operation Succesfull: Generate default logo ');
                    }
                },
                    err => {
                        this.spinerService.display(false);
                        console.error('Observer got an error: ' + err)
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    generateLiveryClick(airline: AmsAirline) {
        this.liveryCreate(airline);
    }


    liveryCreate(airline: AmsAirline) {

        if (airline && airline.alId) {
            this.spinerService.display(true);

            this.alClient.airlineLivery(this.airline.alId)
                .subscribe((res: AmsAirline) => {
                    this.spinerService.display(false);
                    //console.log('airlineLogo -> res:', res);
                    if (res && res.alId) {
                        this.cus.airline = res;
                        this.toastr.success('Airline', 'Operation Succesfull: Generate default livery ');
                    }
                },
                    err => {
                        this.spinerService.display(false);
                        console.error('Observer got an error: ' + err)
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    onChartClick(event) {
        console.log('onChartClick -> event:', event);
    }

    //#endregion

    //#region Dialogs

    FlightEstimateDialog(dto: IAmsFlightEstimateDto) {
        const dialogRef = this.dialogService.open(AmsFlightEstimateDialog, {
            width: '721px',
            height: '520px',
            data: {
                flight: dto.flight,
                airline: dto.airline,
                aircraft: dto.aircraft,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('FlightEstimate-> res:', res);
            if (res) {
                //this.aliService.airline = res;
            }
            this.initFields();
        });
    }

    FlightsCharterEstimateDialog(dto: IAmsFlightsEstimateDto) {
        const dialogRef = this.dialogService.open(AmsFlightCharterEstimateDialog, {
            width: '721px',
            height: '520px',
            data: {
                flights: dto.flights,
                airline: dto.airline,
                aircraft: dto.aircraft,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('FlightsCharterEstimateDialog-> res:', res);
            if (res) {
                //this.aliService.airline = res;
            }
            this.initFields();
        });
    }
    //#endregion

    //#region SPM

    showMessage(data: IInfoMessage) {
        console.log('showMessage-> data:', data);

        const dialogRef = this.dialogService.open(InfoMessageDialog, {
            width: '721px',
            height: '320px',
            data: data
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('showMessage-> res:', res);
            if (res) {

            }
        });

    }

    spmAirportClose(): void {
        if (this.spmAirport) {
            this.spmAirport.closePanel();
        }
    }

    spmAirportOpen(airport: AmsAirport) {
        this.spmAp = airport;
        if (this.spmAp && this.spmAirport) {
            this.spmAirport.expandPanel();
        }
        else {
            this.spmAirportClose();
        }

    }

    spmAirlineClose(): void {
        if (this.spmAirline) {
            this.spmAirline.closePanel();
        }
    }

    spmAirlineOpen(al: AmsAirline) {
        this.spmAl = al;

        if (this.spmAl && this.spmAirline) {
            this.showMessage({ title: 'Not Implemented', message: 'SPM component not implemented' });
            //this.spmAirline.expandPanel();
        }
        else {
            //this.spmAirlineClose();

        }

    }

    spmAcmMacClose(): void {
        if (this.spmAcmMac) {
            this.spmAcmMac.closePanel();
        }
    }

    spmAcmMacOpen(mac: AmsMac) {
        this.spmMac = mac;

        if (this.spmMac && this.spmAcmMac) {
            //this.showMessage({ title: 'Not Implemented', message: 'SPM component not implemented' });
            this.spmAcmMac.expandPanel();
        }
        else {
            //this.spmAcmMacClose();

        }

    }

    spmAircraftClose(): void {
        if (this.spmAircraft) {
            this.spmAircraft.closePanel();
        }
    }

    spmAircraftOpen(ac: AmsAircraft) {
        this.spmAc = ac;
        if (this.spmAc && this.spmAircraft) {
            this.spmAircraft.expandPanel();
        }
    }

    spmAircraftFlightTransferEstimate(apId: number): void {

        if (this.spmAc) {
            this.spinerService.show();
            this.flClient.flightTransferEstimate(this.spmAc.acId, apId)
                .subscribe((resp: ResponseAmsFlightEstimate) => {
                    this.spinerService.hide();
                    const flEstimate = (resp && resp.data && resp.data.flight) ? AmsFlightEstimate.fromJSON(resp.data.flight) : undefined;
                    console.log('flEstimate: ', flEstimate);
                    const dto: IAmsFlightEstimateDto = {
                        flight: flEstimate,
                        airline: this.airline,
                        aircraft: this.spmAc,
                    };
                    this.FlightEstimateDialog(dto);
                }, err => {
                    this.spinerService.hide();
                    console.log('flightTransferEstimate -> err', err);
                });

        }
    }

    //#endregion

    //#region Tree Panel

    sidebarToggle() {
        this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
    }

    public closePanelLeftClick(): void {
        this.panelIn = false;
    }

    public expandPanelLeftClick(): void {
        // this.treeComponent.openPanelClick();
        this.panelIn = true;
    }

    //#endregion
}
