import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// -Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Nodels
import { AmsCity, AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { registerLocaleData } from '@angular/common';
import { AmsAirport, AmsRunway } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AmsAirline, BankHistory30Days, ResponseBankHistory30Days } from 'src/app/@core/services/api/airline/dto';
import { MatDialog } from '@angular/material/dialog';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAircraft, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsFlightEstimate, ResponseAmsFlightsEstimate } from 'src/app/@core/services/api/flight/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsTransactionsSum, AmsTransactionsSumLastWeekData, AmsTransactionTypeSum, AmsTransactionTypeSumData, IAmsTransactionTypeSum } from 'src/app/@core/services/api/airline/transaction';


export const SR_LEFT_PANEL_IN_KEY = 'ams_airline_info_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_airline_info_tab_idx';
export const SR_SEL_TABIDX_BANK_KEY = 'ams_airline_info_tab_bank_idx';



@Injectable({ providedIn: 'root' })
export class AirlineInfoService {

    spmAirlinePanelOpen = new BehaviorSubject<AmsAirline>(undefined);
    spmAirportPanelOpen = new BehaviorSubject<AmsAirport>(undefined);
    spmAcmMacPanelOpen = new BehaviorSubject<AmsMac>(undefined);
    spmAircraftPanelOpen = new BehaviorSubject<AmsAircraft>(undefined);

    constructor(
        private cus: CurrentUserService,

        private client: AmsAirlineClient,
        private apClient: AmsAirportClient,
        private flClient: AmsFlightClient) {
    }

    //#region Properties

    get user(): UserModel {
        return this.cus.user;
    }

    get airline(): AmsAirline {
        return this.cus.airline;
    }

    set airline(value: AmsAirline) {
        this.cus.airline = value;
    }

    get hqAirport(): AmsAirport {
        return this.cus.hqAirport;
    }

    set hqAirport(value: AmsAirport) {
        this.cus.hqAirport = value;
    }

    //#endregion 

    //#region subregion tree panel

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Bank Tabs



    tabIdxChangedBank = new BehaviorSubject<number>(undefined);

    get tabIdxBank(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_BANK_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_BANK_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdxBank(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_BANK_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Charter Flights

    estimateCharterFlights(aicraft: AmsAircraft): Observable<AmsFlightEstimate[]> {

        return new Observable<AmsFlightEstimate[]>(subscriber => {

            this.flClient.flightCharterEstimate(aicraft.acId, aicraft.currApId)
                .subscribe((resp: AmsFlightEstimate[]) => {
                    console.log('estimateCharterFlights-> resp', resp);
                    subscriber.next(resp);
                },
                    err => {
                        throw err;
                    });
        });

    }

    //#endregion

    //#region Data

    loadAirport(apId: number): Observable<AmsAirport> {

        return new Observable<AmsAirport>(subscriber => {

            this.apClient.getAirport(apId)
                .subscribe((resp: AmsAirport) => {
                    console.log('loadAirport-> resp', resp);
                    subscriber.next(resp);
                },
                    err => {

                        throw err;
                    });
        });

    }

    solrAirport(apId: number): Observable<AmsAirport> {

        return new Observable<AmsAirport>(subscriber => {

            this.apClient.geSolrAirport(apId)
                .subscribe((resp: AmsAirport) => {
                    console.log('geSolrAirport-> resp', resp);
                    subscriber.next(resp);
                },
                    err => {

                        throw err;
                    });
        });

    }

    loadRunway(rwId: number): Observable<AmsRunway> {

        return new Observable<AmsRunway>(subscriber => {

            this.apClient.getRunway(rwId)
                .subscribe((resp: AmsRunway) => {
                    //console.log('loadRunway-> resp', resp);
                    subscriber.next(resp);
                },
                    err => {

                        throw err;
                    });
        });

    }

    //#endregion
   
}
