

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Models
import { environment } from 'src/environments/environment';
import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAirline, BankHistory30Days, ResponseBankHistory30Days } from 'src/app/@core/services/api/airline/dto';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AirlineInfoService } from '../info/airline-info.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';


@Component({
    templateUrl: './dashboard.component.html',
    // animations: [PageTransition]
})
export class AirlineCreateDashboardComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    logo = '../../../assets/images/common/trimble-logo-white.png';
    airportImg = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';
    icaoImg = URL_COMMON_IMAGE_AMS_COMMON + 'icao.png';
    noLogoUrl = URL_COMMON_IMAGE_AIRLINE + 'logo_0.png';
    bankImg = URL_COMMON_IMAGE_AMS_COMMON + 'Bank.png';
    rootes = AppRoutes;

    redirectTo: string;
    copyrightCurrentYear = new Date().getFullYear();
    copyrightStartYear = 2018;
    errMsg: string;
    env: string;


    user: UserModel;
    userChanged: Subscription;
    avatarUrl: string;

    airline: AmsAirline;
    airlineChanged: Subscription;
    alCreated: boolean;
    logoUrl: string;
    liveryUrl: string;
    hqAirport: AmsAirport;

    bankHistory30DaysChanged: Subscription;
    bankHistory30DaysData: ResponseBankHistory30Days;
    chartData = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabels = ['January', 'February', 'Mars', 'April'];

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private trService: AmsCommTransactionService,
        private alClient: AmsAirlineClient,
        private aService: AirlineInfoService) {

    }


    ngOnInit(): void {
        this.env = environment.abreviation;

        this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
            this.initFields();
        });

        this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
            this.initFields();
        });
        this.bankHistory30DaysChanged = this.trService.bankHistory30DaysChanged.subscribe(data => {
            this.initFields();
        });

        this.initFields();
    }



    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
        if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
        if (this.bankHistory30DaysChanged) { this.bankHistory30DaysChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.avatarUrl = this.cus.avatarUrl;
        this.airline = this.cus.airline;
        this.alCreated = this.airline && this.airline.apId ? true : false;
        this.hqAirport = this.cus.hqAirport;
        this.logoUrl = this.airline?.logo ? this.airline.logo : this.airline?.amsLogoUrl;
        this.liveryUrl = this.airline?.amsLiveryUrl;

        this.bankHistory30DaysData = this.trService.bankHistory30DaysData;
        if (this.bankHistory30DaysData && this.bankHistory30DaysData.data && this.bankHistory30DaysData.data.rows) {
            const data = BankHistory30Days.dataToList(this.bankHistory30DaysData.data);
            this.chartData = BankHistory30Days.getLineChartData(data);
            this.chartLabels = BankHistory30Days.getLineChartLabels(data);
        } else {
            this.loadBankHistoryChanrt();
        }
    }



    //#region  Actions

    generateLogoClick(airline: AmsAirline) {
        this.logoCreate(airline);
    }

    logoCreate(airline: AmsAirline) {

        if (airline && airline.alId) {
            this.spinerService.display(true);

            this.alClient.airlineLogo(this.airline.alId)
                .subscribe((res: AmsAirline) => {
                    this.spinerService.display(false);
                    //console.log('airlineLogo -> res:', res);
                    if (res && res.alId) {
                        this.cus.airline = res;
                        this.toastr.success('Airline', 'Operation Succesfull: Generate default logo ');
                    }
                },
                    err => {
                        this.spinerService.display(false);
                        console.error('Observer got an error: ' + err)
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    generateLiveryClick(airline: AmsAirline) {
        this.liveryCreate(airline);
    }


    liveryCreate(airline: AmsAirline) {

        if (airline && airline.alId) {
            this.spinerService.display(true);

            this.alClient.airlineLivery(this.airline.alId)
                .subscribe((res: AmsAirline) => {
                    this.spinerService.display(false);
                    //console.log('airlineLogo -> res:', res);
                    if (res && res.alId) {
                        this.cus.airline = res;
                        this.toastr.success('Airline', 'Operation Succesfull: Generate default livery ');
                    }
                },
                    err => {
                        this.spinerService.display(false);
                        console.error('Observer got an error: ' + err)
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    onChartClick(event) {
        console.log('onChartClick -> event:', event);
    }

    //#endregion

    //#region Chart

    public loadBankHistoryChanrt(): void {
        if (this.airline?.alId) {
            this.spinerService.show();
            this.trService.loadBankHistoryChanrt(this.airline?.alId)
                .subscribe((res: BankHistory30Days[]) => {
                    this.spinerService.hide();
                    this.initFields();
                },
                    err => {
                        this.spinerService.hide();
                        console.log('autenticate-> err: ', err);
                    });
        }

    }

    //#endregion
}
