import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// --- Models
import { AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS, URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { RwTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMac, AmsMacCabin, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMacCabinEditDialog } from 'src/app/@share/components/dialogs/mac-cabin-edit/mac-cabin-edit.dialog';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AirlineInfoService } from '../../info/airline-info.service';
import { AmsFlight, AmsFlightEstimate, AmsFlightTableCriteria } from 'src/app/@core/services/api/flight/dto';
import { AircraftActionType, AircraftStatus } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsFlightCharterEstimateDialog, IAmsFlightsEstimateDto } from 'src/app/@share/components/dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { AmsAirlineGridFlightDashboardDataSource } from './grid-flight-dashboard.datasource';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';


@Component({
    selector: 'ams-grid-flight-dashboard',
    templateUrl: './grid-flight-dashboard.component.html',
})
export class AmsGridAirlineFlightsDashboardComponent implements OnInit, OnDestroy, AfterViewInit {
   
    @ViewChild(MatTable, { static: true }) table: MatTable<AmsFlight>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Output() charterEstimate: EventEmitter<IAmsFlightsEstimateDto> = new EventEmitter<IAmsFlightsEstimateDto>();
    
   
    selFlight:AmsFlight;
    selaircraft: AmsAircraft;
    airport: AmsAirport;
    

    criteria: AmsAircraftTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;
    

    dataCount = 0;
    dataChanged: Subscription;
    interval: any;
    //
    displayedColumns = ['flId'];
    airline: AmsAirline;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private client: AmsAircraftClient,
        private flClient: AmsFlightClient,
        public tableds: AmsAirlineGridFlightDashboardDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsFlight>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsFlightTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });
        if(this.interval){
            clearInterval(this.interval);
        }
        this.interval = setInterval(() => {
            this.loadPage();
        }, 60_000);

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if(this.interval){ clearInterval(this.interval);}
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    gotoflightt(flight: AmsFlight) {
        console.log('gotoflightt -> flight:' + flight);
    }

    gotoAircraft(aicraft: AmsAircraft) {
        console.log('gotoAircraft -> aicraft:' + aicraft);
    }

   
    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if(airport && airport.apId){
            this.cus.spmAirportPanelOpen.next(airport.apId);
          }
    }

    spmAircraftOpen(value: AmsAircraft) {
        if (value) {
            this.aliService.spmAircraftPanelOpen.next(value);
        }
    }


    ediMacCabin(data: AmsMacCabin) {
        console.log('ediMacCabin-> data:', data);
        //console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            const dialogRef = this.dialogService.open(AmsMacCabinEditDialog, {
                width: '721px',
                height: '620px',
                data: {
                    cabin: data,
                    //mac: this.acmService.acMarketMac
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediMac-> res:', res);
                if (res) {
                    //this.table.renderRows();
                    this.tableds.loadData()
                        .subscribe(res => {
                            this.initFields();
                        });
                }
                else {
                    this.initFields();
                }


            });

        }
        else {
            this.toastr.info('Please select manufacturer acrcraft model first.', 'Edit Cabin Configuration');
        }


    }

    addCabin() {
        // console.log('inviteUser-> ');
        let dto = new AmsMacCabin();
        //dto.macId = this.acmService.acMarketMac.macId;
        this.ediMacCabin(dto);
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);

        if (data) {
            //this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    acCharterFlights: AmsFlightEstimate[];

   

    viewCharterFlights(aicraft: AmsAircraft) {
        if (aicraft && aicraft.acStatusId == AircraftStatus.Operational &&
            aicraft.acActionTypeId == AircraftActionType.PlaneStand) {
            this.preloader.show();
            console.log('viewCharterFlights-> aicraft', aicraft);
            this.aliService.estimateCharterFlights(aicraft)
                .subscribe((resp: AmsFlightEstimate[]) => {
                    console.log('loadAirport-> resp', resp);
                    this.acCharterFlights = resp;
                    const data = {
                        flights: this.acCharterFlights,
                        airline: this.airline,
                        aircraft: aicraft,
                    };
                    this.preloader.hide();
                    this.FlightsCharterEstimateDialog(data);

                },
                    err => {
                        this.preloader.hide();
                        throw err;
                    });
        } else {

        }
    }

    FlightsCharterEstimateDialog(dto: IAmsFlightsEstimateDto) {
        const dialogRef = this.dialogService.open(AmsFlightCharterEstimateDialog, {
            width: '921px',
            height: '520px',
            data: {
                flights: dto.flights,
                airline: dto.airline,
                aircraft: dto.aircraft,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('FlightsCharterEstimateDialog-> res:', res);
            if (res && res > -1) {
                this.loadPage();
            }
            this.initFields();
        });
    }

    //#region Data
    /*
        public loadAirport(apId: number): Observable<AmsAirport> {
            
            this.preloader.show();
            return new Observable<AmsAirport>(subscriber => {
    
                this.aService.loadAirport(apId)
                    .subscribe((resp: AmsAirport) => {
                        //console.log('loadAirport-> resp', resp);
                        subscriber.next(resp);
                        this.preloader.hide();
                    },
                        err => {
                            this.preloader.hide();
                            throw err;
                        });
            });
            
        }
    */
    //#endregion


    /*
    gotoAirport(data: AmsAirport) {
        

        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }

    }*/

    //#endregion
}

