import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { AirlineRoutingModule, routedComponents } from './airline-routing.module';
import { AirlineService } from './airline.service';
import { AirlineMenuMainComponent } from './menu-main/al-menu-main.component';
import { AirlineInfoFilterPanelBodyComponent } from './info/filter-panel/airline-info-filter-panel.component';
import { AirlineCreateFormComponent } from './create/airline-create.component';
import { AirlineInfoService } from './info/airline-info.service';
import { AmsGridAirlineAircraftTableDataSource } from './grids/aircrafts/grid-al-aircrafts.datasource';
import { AmsGridAirlineAircraftsComponent } from './grids/aircrafts/grid-al-aircrafts.component';
import { AmsGridAirlineFlightsDashboardComponent } from './grids/flight-dashboard/grid-flight-dashboard.component';
import { AlInfoBankTabDashboardComponent } from './info/bank/tabs/al-info-bank-tab-dashboard/al-info-bank-tab-dashboard.component';
import { AmsAirlineAircraftService } from './aircraft/al-aircraft.service';
import { AlAircraftFilterPanel } from './aircraft/filter-panel/al-aircraft-filter-panel.component';
import { AmsAlAircraftTabsHomeComponent } from './aircraft/tabs/al-aircraft-tabs-home.component';
import { AlAircraftTabFightHistoryComponent } from './aircraft/tabs/al-aircraft-tab-fight-history/al-aircraft-tab-fight-history.component';
import { AmsAlAircraftGridFlightHistoryDataSource } from './aircraft/grids/al-aircraft-grid-flight-history/al-aircraft-grid-flight-history.datasource';
import { AmsAlAircraftGridFlightHistoryComponent } from './aircraft/grids/al-aircraft-grid-flight-history/al-aircraft-grid-flight-history.component';
import { AirlineDashboardFilterPanelComponent } from './dashboard/filter-panel/airline-dashboard-filter-panel.component';
import { AmsAirlineDashboarFlightdDataSource } from './dashboard/dashboard-flight.datasource';
import { AirlineDashboardComponent } from './dashboard/dashboard.component';
import { AlInfoBankTabRevenueComponent } from './info/bank/tabs/al-info-bank-tab-revenue/al-info-bank-tab-revenue.component';
import { AmsAirlineDashboardDeparturesDataSource } from './dashboard/dashboard-departures.datasource';
import { UserProfileComponent } from './profile/user-profile.component';
import { AmsAlAircraftTabDashboardComponent } from './aircraft/tabs/al-aircraft-tab-dashboard/al-aircraft-tab-dashboard.component';




@NgModule({
    imports: [
        CommonModule,
        SxMaterialModule,
        SxPipesModule,
        SxShareModule,
        //
        AirlineRoutingModule,
    ],
    declarations: [
        routedComponents,
        AirlineMenuMainComponent,
        AirlineInfoFilterPanelBodyComponent,
        UserProfileComponent,
        //Formd
        AirlineCreateFormComponent,
        //-Dialogs
        //Grids
        AmsGridAirlineAircraftsComponent,
        AmsGridAirlineFlightsDashboardComponent,
        AlInfoBankTabDashboardComponent,
        // Aircraft
        AlAircraftFilterPanel,
        AlAircraftTabFightHistoryComponent,
        AmsAlAircraftTabDashboardComponent,
        AmsAlAircraftGridFlightHistoryComponent,
        AirlineDashboardFilterPanelComponent,

        AlInfoBankTabRevenueComponent,
    ],
    exports: [
        SxShareModule,
        AlInfoBankTabRevenueComponent,
    ],
    /*
    providers: [
        AirlineService,
        AirlineInfoService,
        AmsGridAirlineAircraftTableDataSource,
        AmsAirlineAircraftService,
        AmsAlAircraftGridFlightHistoryDataSource,
        AmsAirlineDashboarFlightdDataSource,
        AmsAirlineDashboardDeparturesDataSource,
    ]*/
})
export class AirlineModule { }
