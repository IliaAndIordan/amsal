

import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { FormControl, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsWadService } from 'src/app/wad/wad.service';
import { AmsCountry, AmsRegion, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';


@Component({
    selector: 'ams-al-create-page',
    templateUrl: './airline-create.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class AirlineCreateFormComponent implements OnInit, OnDestroy {

    airportImg = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';
    alLogoImg = URL_COMMON_IMAGE_AIRLINE + 'logo_0.png';

    rootes = AppRoutes;
    hasSpinner = false;
    errorMessage: string;
    env: string;

    formGrp: UntypedFormGroup;

    user: UserModel;
    airline: AmsAirline;
    alCreated: boolean;

    subregions: AmsSubregion[];
    subregionsOpt: Observable<AmsSubregion[]>;
    countries: AmsCountry[];
    countriesOpt: Observable<AmsCountry[]>;
    airports: AmsAirport[];
    airportsOpt: Observable<AmsAirport[]>;


    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private wadService: AmsWadService) { }


    ngOnInit(): void {
        this.env = environment.abreviation;
        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.initFields();

        if (!this.airline) { this.airline = new AmsAirline(); }

        this.formGrp = this.fb.group({
            alName: new UntypedFormControl('', [Validators.required, Validators.maxLength(512)]),
            iata: new UntypedFormControl('', [Validators.required, Validators.maxLength(3)]),
            slogan: new UntypedFormControl('', [Validators.maxLength(512)]),
            logo: new UntypedFormControl('', [Validators.maxLength(2000)]),
            subregion: new UntypedFormControl('', [Validators.required]),
            country: new UntypedFormControl('', [Validators.required]),
            airport: new UntypedFormControl('', [Validators.required]),
        });
        this.loadRegions();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    initFields(): void {

        this.user = this.cus.user;

        this.airline = this.cus.airline;
        console.log('initFields -> airline:', this.airline);
        this.alCreated = this.airline && this.airline.apId ? true : false;
        console.log('initFields -> user:', this.user);
    }

    //#region FORM

    get alName() { return this.formGrp.get('alName') as FormControl; }
    get iata() { return this.formGrp.get('iata') as FormControl; }
    get slogan() { return this.formGrp.get('slogan') as FormControl; }
    get logo() { return this.formGrp.get('logo') as FormControl; }
    get subregion() { return this.formGrp.get('subregion') as FormControl; }
    get country() { return this.formGrp.get('country') as FormControl; }
    get airport() { return this.formGrp.get('airport') as FormControl; }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }



    onCansel(): void {
        //this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;
            const message = 'Create';

            this.airline = new AmsAirline();
            const airport = this.airport.value as AmsAirport;

            this.airline.alName = this.alName.value;
            this.airline.iata = this.iata.value;
            this.airline.slogan = this.slogan.value;
            this.airline.logo = this.logo.value;
            this.airline.apId = airport.apId;
            this.airline.userId = this.user.userId;

            this.spinerService.display(true);

            this.alClient.airlineSave(this.airline)
                .subscribe((res: AmsAirline) => {
                    this.spinerService.display(false);
                    //console.log('airlineSave -> res:', res);
                    if (res && res.alId) {
                        this.cus.airline = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Airline', 'Operation Succesfull: Airline ' + message);
                        this.router.navigate([AppRoutes.Root, AppRoutes.airline]);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Airline ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.router.navigate([AppRoutes.Root, AppRoutes.airline]);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

    //#endregion

    //#region Filter Subregions

    sunregionValueChange(event: MatAutocompleteSelectedEvent) {
        console.log('sunregionValueChange-> event:', event);
        if (this.subregion.valid) {
            const subregion: AmsSubregion = this.subregion.value;
            console.log('sunregionValueChange-> subregion:', subregion);
            if (subregion && subregion.subregionId) {
                this.loadCountries(subregion);
            }
        }
    }

    displaySubregion(value: AmsSubregion): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.srName ? value.srName : value.srCode;
        }
        return rv;
    }

    filterSubregion(val: any): AmsSubregion[] {
        // console.log('filterDpc -> val', val);
        let sr: AmsSubregion;
        if (val && val.subregionId) {
            sr = val as AmsSubregion;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.srName || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<AmsSubregion>()
        if (this.subregions && this.subregions.length) {
            rv = this.subregions.filter(x => (
                x.srName.toLowerCase().indexOf(filterValue) > -1 ||
                x.srCode.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }

    initSubregions() {
        this.subregionsOpt = of(this.subregions);

        this.subregionsOpt = this.subregion.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterSubregion(val) :
                    (this.subregions ? this.subregions.slice() : new Array<AmsSubregion>()))
            );

        let sr: AmsSubregion;

        if (this.subregions && this.subregions.length > 0) {
            sr = this.subregion && this.subregion.value ? this.subregions.find(x => x.subregionId === this.subregion.value.subregionId) : undefined;
        }

        this.formGrp.patchValue({ subregion: sr });
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region Filter Country

    countryValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.country.valid) {
            const country: AmsCountry = this.country.value;
            this.loadAirports(country);
        }
    }

    displayCountry(value: AmsCountry): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.cName ? value.cName : value.cCode;
        }
        return rv;
    }

    filterCountry(val: any): AmsCountry[] {
        // console.log('filterDpc -> val', val);
        let sr: AmsCountry;
        if (val && val.countryId) {
            sr = val as AmsCountry;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.cName || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<AmsCountry>()
        if (this.countries && this.countries.length) {
            rv = this.countries.filter(x => (
                x.cName.toLowerCase().indexOf(filterValue) > -1 ||
                x.cCode.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }

    initCountry() {
        this.countriesOpt = of(this.countries);

        this.countriesOpt = this.country.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterCountry(val) :
                    (this.countries ? this.countries.slice() : new Array<AmsCountry>()))
            );

        let sr: AmsCountry;

        if (this.countries && this.countries.length > 0) {
            sr = this.country && this.country.value ? this.countries.find(x => x.countryId === this.country.value.countryId) : undefined;
        }

        this.formGrp.patchValue({ country: sr });
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region Filter Airports

    airportValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.airport.valid) {
            const airport: AmsAirport = this.airport.value;
        }
    }

    displayAirport(value: AmsAirport): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv += value.iata ? value.iata : value.icao + ' ';
            rv += value.apName ? value.apName : value.apId;
            rv += ', ' + value.ctName + ' ';
            rv += value.iso2;
        }
        return rv;
    }

    filterAirport(val: any): AmsAirport[] {
        let sr: AmsAirport;
        if (val && val.countryId) {
            sr = val as AmsAirport;
        }

        const value = val.apName || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        let rv = new Array<AmsAirport>()
        if (this.airports && this.airports.length) {
            rv = this.airports.filter(x => (
                x.apName.toLowerCase().indexOf(filterValue) > -1 ||
                x.iata.toLowerCase().indexOf(filterValue) > -1 ||
                x.icao.toLowerCase().indexOf(filterValue) > -1));
        }
        return rv;
    }

    initAirports() {
        this.airportsOpt = of(this.airports);

        this.airportsOpt = this.airport.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterAirport(val) :
                    (this.airports ? this.airports.slice() : new Array<AmsAirport>()))
            );

        let sr: AmsAirport;

        if (this.airports && this.airports.length > 0) {
            sr = this.airport && this.airport.value ? this.airports.find(x => x.apId === this.airport.value.apId) : undefined;
        }

        this.formGrp.patchValue({ airport: sr });
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region Data

    loadRegions(): void {
        this.spinerService.show();
        this.wadService.loadRegions()
            .subscribe((resp: ResponseAmsRegionsData) => {
                console.log('loadRegions -> resp:', resp);
                this.subregions = resp.data.subregions;
                this.spinerService.hide();
                this.initSubregions();
            },
                err => {
                    this.spinerService.hide();
                    console.log('loadRegions-> err: ', err);
                });
    }

    loadCountries(subregion: AmsSubregion): void {
        this.spinerService.show();
        this.wadService.loadCountryBySubregion(subregion.subregionId)
            .subscribe((resp: Array<AmsCountry>) => {
                console.log('loadCountries-> resp:', resp);

                this.countries = resp;
                this.spinerService.hide();
                this.initCountry();
            },
                err => {
                    this.spinerService.hide();
                    console.log('loadRegions-> err: ', err);
                });

    }

    loadAirports(country: AmsCountry): void {
        this.spinerService.show();
        this.wadService.loadAirportsByCountry(country.countryId)
            .subscribe((resp: Array<AmsAirport>) => {
                console.log('loadAirports-> resp:', resp);

                this.airports = resp;
                this.spinerService.hide();
                this.initAirports();
            },
                err => {
                    this.spinerService.hide();
                    console.log('loadAirports-> err: ', err);
                });

    }


    //#endregion
}
