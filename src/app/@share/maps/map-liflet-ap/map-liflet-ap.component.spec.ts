import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapLifletApComponent } from './map-liflet-ap.component';

describe('MAPS -> MapLifletApComponent', () => {
  let component: MapLifletApComponent;
  let fixture: ComponentFixture<MapLifletApComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapLifletApComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapLifletApComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
