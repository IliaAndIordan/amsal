import { ConnectedOverlayPositionChange } from '@angular/cdk/overlay';
import { AfterViewInit, ChangeDetectionStrategy, Component, Input, OnChanges, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { LatLng } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { iconDefault, MarkerService } from '../map-liflet-ap-marker.service';
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
import 'leaflet.geodesic';
//import * as L from 'leaflet-arc';
declare let L;

L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'ams-map-liflet-ap',
  templateUrl: './map-liflet-ap.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class MapLifletApComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() isVisible: boolean = false;

  map;
  usrLocation: LatLng;

  airportsChanged: Subscription;
  airportChanged: Subscription;
  apCriteriaChanged: Subscription;

  airports: AmsAirport[];
  airport: AmsAirport;
  viewInit: boolean = false;

  constructor(
    private cus: CurrentUserService,
    private markerService: MarkerService) {
  }


  ngOnInit(): void {

    this.cus.getLocation().subscribe(location => {
      //console.log('ngOnInit -> getLocation:', location);
      this.usrLocation = location;
      if (this.usrLocation && this.map) {
        this.map.setView(new L.LatLng(this.usrLocation.lat, this.usrLocation.lng), 8);
      }

    });

    this.apCriteriaChanged = this.markerService.apCriteriaChanged.subscribe(criteria => {
      if (criteria) {
        //console.log('apCriteriaChanged -> criteria', criteria);
        this.markerService.loadAirportData().subscribe(airports => {
          console.log('loadAirportData -> airports', airports);
        });
      }

    });

    this.airportsChanged = this.markerService.airportsChanged.subscribe(airports => {
      this.makeMarkers();
    });
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['isVisible']) {
      this.isVisible = changes['isVisible'].currentValue;
      /*
      if (this.isVisible) {
        if (!this.map) {
          this.initMap();
        } else {
          this.makeMarkers();
        }
      }
      */
    }
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  private initMap(): void {
    
    //console.log('initMap -> Location:', this.usrLocation);
    this.map = L.map('map', {
      center: [this.usrLocation ? this.usrLocation.lat : 39.8282, this.usrLocation ? this.usrLocation.lng : -98.5795],
      zoom: 3
    });
  
    

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    if (this.map) {
      tiles.addTo(this.map);
      this.makeMarkers();
    }
  }

  makeMarkers(): void {
    this.airports = this.markerService.airports;
      if (!this.map) {
        this.initMap();
      } else {
        if (this.airports && this.airports.length > 0) {
          this.airport = this.airports[0];
        }
        this.markerService.makeAirportMarkers(this.map);
      }
  }
}
