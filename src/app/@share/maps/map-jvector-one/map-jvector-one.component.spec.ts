import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapJvectorOneComponent } from './map-jvector-one.component';

describe('MapJvectorOneComponent', () => {
  let component: MapJvectorOneComponent;
  let fixture: ComponentFixture<MapJvectorOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapJvectorOneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapJvectorOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
