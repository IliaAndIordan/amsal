import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";

import { AmsAirport, IAmsAirport } from "src/app/@core/services/api/airport/dto";
import { AppStore, URL_COMMON_IMAGE_AIRCRAFT } from "src/app/@core/const/app-storage.const";
import { BehaviorSubject, Subject } from "rxjs";
import { AmsAirportClient } from "src/app/@core/services/api/airport/api-client";
import { AmsWadCountryService } from "src/app/wad/country/country.service";
import { LengthKmPipe, LengthPipe } from "src/app/@core/models/pipes/length.pipe";
import { ApTypeDisplayPipe } from "src/app/@core/models/pipes/ap-type.pipe";
import { AmsFlight } from "src/app/@core/services/api/flight/dto";
import { AmsFlightStatusOpt, AmsFlightType, AmsFlightTypeDisplayPipe } from "src/app/@core/services/api/flight/enums";
import { PeriodMinPipe } from "src/app/@core/models/pipes/interval.pipe";
import { AmsHubTypeDisplayPipe } from "src/app/@core/services/api/airline/enums";
import { AmsAlSheduleRoute } from "src/app/@core/services/api/airline/al-flp-shedule";
import { AmsAlFlightNumber, AmsAlFlightNumberSchedule } from "src/app/@core/services/api/airline/al-flp-number";
import { AmsMapAlFlightNumberSchedule, AmsMapFlight } from "./dto";
import { AmsAcIfeTypeDisplayPipe } from "src/app/@core/models/pipes/ac-ife-type.pipe";
import { AcTypeDisplayPipe, AcTypeOpt } from "src/app/@core/models/pipes/ac-type.pipe";
import { MtowPipe } from "src/app/@core/models/pipes/mtow.pipe";
import { CurrencyPipe } from "@angular/common";

import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
//import * as L from 'leaflet-arc';

declare let L;

export const ICON_CHARTER: string = 'schedule_send';
export const ICON_TRANSFER: string = 'send_time_extension';
export const ICON_FLIGHT: string = 'flight';


@Injectable({
    providedIn: 'root'
})
export class MapLeafletPopupAirportService {


    constructor(
        private cus: CurrentUserService,
        private lenM: LengthPipe,
        private apType: ApTypeDisplayPipe,
        private lenkm: LengthKmPipe,
        private fltype: AmsFlightTypeDisplayPipe,
        private actype: AcTypeDisplayPipe,
        private periodmin: PeriodMinPipe,
        private mtow: MtowPipe,
        private hubType: AmsHubTypeDisplayPipe,
        private curency:CurrencyPipe) {
    }

    makeAirportPopup(data: AmsAirport, canCreateFlightNumber: boolean = false, flightsNumbers: AmsAlFlightNumber[] = undefined): string {
        const seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_e.png';
        const cargoImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';
        const rwLen = this.lenM.transform(data.maxRwLenght);
        const apTypeStr = this.apType.transform(data.typeId);
        const hubTypeStr = data.hub ? this.hubType.transform(data.hub.hubTypeId) : '';
        const routesCount = data.routes ? data.routes.length : 0;

        const cargoDemandPeriodKgStr = this.mtow.transform(data.cargoDemandPeriodKg);
        const canCreateRoute = false;
        return `` +
            `<div class="flex-align-items w-100 border-b1">
                <div class="col-10 flex-align-items ellipsis pe-1">
                    <div class="icon-value mr-1">
                        <img src="${data.flagUrl}" alt="${data.iso2}">
                    </div>
                    <div class="icon-value mr-1" >${data.iata ? data.iata : data.icao}</div>
                    <div class="icon-value mr-1">${data.apName},</div>
                    <div class="icon-value mr-1"> ${data.ctName} ${data.iso2} </div>
                </div>
                <div class="col-2 flex-align-items">
                    <div class="icon-value link-blue flex-end" id="spmApOpen">
                        <mat-icon class="material-icons">launch</mat-icon>
                    </div>
                </div>
            </div>` +
            `<div class="flex-align-items w-100 mt-2">` +
            `<div class="flex-start flex-column">
                    <div class="ellipsis"><b> ${apTypeStr}</b></div>
                    <div class="ellipsis">Max Rw: <b> ${rwLen}</b></div>
                    <div class="ellipsis">Active: <b>${data.active ? "Yes" : "No"}</b></div>
                    ${data.hub ? `<div class="ellipsis">${hubTypeStr} Airport: <b>Yes</b></div>` : ``}
                    ${routesCount > 0 ? `<div class="ellipsis">Routes: <b>${routesCount.toString()}</b></div>` : ``}
                    <div class="flex-align-items w-100 mt-1 ellipces">
                        <div class="icon-value">
                            ${data.paxDemandPeriod}
                            <img src="${seatsEImgUrl}" alt="Pax" class="ml-1" />
                        </div>
                        <div class="icon-value ml-2" >
                            ${cargoDemandPeriodKgStr}
                            <img src="${cargoImgUrl}" alt="Cargo" class="ml-1" />
                        </div>
                        <div class="icon-value ml-2">
                            ${data.paxDemandPeriodDays}
                            <i class="material-icons ml-1" >restore</i>
                        </div>
                    </div>
                </div>` +
            `   <div class="flex-end img-150 ml-2">
                    <img src="${data.staticmapUrl}" alt="${data.icao}">
                </div>`+
            `</div>` +
            `<div class="flex-align-items w-100 pt-2 border-t1">
                    ${canCreateRoute ? `<div class="icon-value link-blue flex me-2" id="btnRouteOpen" title="Create Route"><mat-icon class="material-icons">add_road</mat-icon></div>` : ''}
                    ${canCreateFlightNumber ? `<div class="icon-value link-blue flex me-2" id="btnFlpnOpen" title="Create Flight Number"><mat-icon class="material-icons">route</mat-icon></div>` : ''}
                    ${!data.hub ? `<div class="icon-value link-green flex me-2" id="btnHubOpen" title="Open Hub"><mat-icon class="material-icons">domain_add</mat-icon></div>` : ''}
             </div>`;
    }


    makeFlightPopup(data: AmsFlight, apDep: AmsAirport, apArr: AmsAirport): string {
        //console.log('makeFlightopup -> data:', data);
        let rv = '';
        const seatsPaxImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/persons_b.png';
        const cargoImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';
        const icon = this.getIconFlight(data.flTypeId);
        const lenkmStr = this.lenkm.transform(data.distanceKm);
        const flTypeStr = this.fltype.transform(data.flTypeId);
        const flDurationMin = this.periodmin.transform(data.flDurationMin);
        const remainTimeMin = this.periodmin.transform(data.remainTimeMin);
        const payloadKg = this.mtow.transform(data.payloadKg);

        rv += `<div class="flex-align-items w-100 border-b1">
                <div class="col-10 flex-align-items">
                    <div class="flex-start pe-1" title="${flTypeStr} flight">
                        <i class="material-icons size-20 pb-1" >${icon}</i>
                    </div>
                    <div class="flex pe-1" title="${apDep.apName}" >${apDep.iata ? apDep.iata : apDep.icao}</div>
                    <div class="flex pe-1">-</div>
                    <div class="flex pe-1" title="${apArr.apName}" >${apArr.iata ? apArr.iata : apArr.icao}</div>
                </div>
                <div class="col-2 flex-align-items">
                    <div class="flex-end icon-value link-blue" id="flightSpm">
                        <mat-icon class="material-icons size-20 pb-1">launch</mat-icon>
                    </div>
                </div>
            </div>`;

        if (data.flId) {

            rv += `<div class="flex-align-items w-100">
                    <div class="col-6 flex-column text-center pe-1 img-150">
                        <img src="${data.acImgTableUrl}" alt="${data.acRegNr}" title="${data.acRegNr} (${data.acId})">
                        <div class="icon-value m-0 mt-3 livery livery-table">
                            <img src=\"${data.amsLiveryUrl}" alt="${data.alName}" title="${data.alName}">
                        </div>
                    </div>
                    <div class="col-6 flex-column ps-1">
                        <div class="flex-align-items w-100 mt-1"> 
                            Distance: <b> ${lenkmStr}</b>
                        </div>
                        <div class="flex-align-items w-100 mt-1"> 
                            Flight Time: <b>${flDurationMin}</b>
                        </div>
                        <div class="flex-align-items w-100 mt-1"> 
                            Remaining: <b>${remainTimeMin}</b>
                        </div>
                        <div class="flex-align-items w-100 mt-1 text-nowrap"> 
                            <div class="flex pe-1">${data.pax}</div>
                            <div class="icon-value flex me-2">
                                <img src="${seatsPaxImgUrl}" alt="Pax"/>
                            </div>
                            <div class="flex pe-1">${payloadKg}</div>
                            <div class="icon-value flex me-2" >
                                <img src="${cargoImgUrl}" alt="Cargo"/>
                            </div>
                        </div>
                    </div>
                   </div>`;
        }

      



        rv += `<div class="flex-align-items w-100 mt-1"> 
                    <div class="ellipsis">${data.flLogDescription ? data.flLogDescription : ''}</div>
                </div>
            `;
        //console.log('makeFlightopup -> rv:', rv);
        return rv;
    }

    makeFlightSchedulePopup(data: AmsMapAlFlightNumberSchedule,): string {
        //console.log('makeFlightopup -> data:', data);
        let rv = '';
        const apDep: AmsAirport = data.depAp;
        const apArr: AmsAirport = data.arrAp;
        const icon = this.getIconFlight(AmsFlightType.Schedule);
        const lenkmStr = this.lenkm.transform(data.flight.distanceKm);
        const flTypeStr = this.fltype.transform(AmsFlightType.Schedule);
        const flDurationMin = this.periodmin.transform(data.flight.flightH * 60);
        const remainTimeMin = this.periodmin.transform(data.flight.flightH * 60);
        const dtime = this.periodmin.transform(((data.flight.dtimeH * 60) + data.flight.dtimeMin));
        const atime = this.periodmin.transform(((data.flight.atimeH * 60) + data.flight.atimeMin));
        const acTypeName = data.group ? this.actype.transform(data.group?.acTypeId) : '';
        const acType = AcTypeOpt.find(x => x.acTypeId === (data.group ? data.group.acTypeId : AcTypeOpt[0].acTypeId));
        const al = this.cus.airline;

        rv += `<div class="flex-align-items w-100 border-b1">
                    <div class="icon-value mr-1">${flTypeStr}</div>
                    <i class="material-icons size-20 mr-1" >${icon}</i>
                    <div class="icon-value mr-1">${apDep.iata ? apDep.iata : apDep.icao}</div> - 
                    <div class="icon-value mx-1">${apArr.iata ? apArr.iata : apArr.icao}</div>
                </div>`;
        rv += `<div class="flex-align-items w-100 mt-2"> 
                <div class="col-5">
                    <div class="icon-value flex-center">
                        <img src="${data.acImgMapUrl}" alt="${data.group?.acId}">
                    </div>
                    <div class="flex-center mt-2 w-100 ellipsis"><b> ${acType?.name}</b></div>
                </div>
                <div class="col-7">
                    <div class="flex-align-items mt-1 w-100">
                        <div class="col-6 ellipsis">Flight #:</div> 
                        <div class="col-6 ps-1 semi-bold ellipsis">${data.flight?.flpnNumber.toString().padStart(4, "0")}</div>
                    </div>
                    <div class="flex-align-items mt-1 w-100">
                        <div class="col-6 ellipsis">Depatture:</div> 
                        <div class="col-6 ps-1 semi-bold ellipsis">${dtime}</div>
                    </div>
                    <div class="flex-align-items mt-1 w-100">
                        <div class="col-6 ellipsis">Arrival:</div> 
                        <div class="col-6 ps-1 semi-bold ellipsis">${atime}</div>
                    </div>
                </div>
            </div>`;

        if (data.group?.alId === al.alId) {
            rv += `<div class="flex-align-items w-100 mt-2"> 
                    <div class="col-5 icon-value flex-center">
                        <img src="${al.amsLiveryUrl}" alt="${data.group?.alId}">
                    </div>
                    <div class="col-7">
                        ${al.alName}
                    </div>
                </div>`;
        }

        rv += `<div class="flex-align-items w-100 mt-2"> 
                    <div class="col-5">
                    
                    </div>
                    <div class="col-7">
                        <div class="flex-align-items mt-1 w-100">
                            <div class="col-6 ellipsis">Distance:</div> 
                            <div class="col-6 ps-1 semi-bold ellipsis">${lenkmStr}</div>
                        </div>
                        <div class="flex-align-items mt-1 w-100">
                            <div class="col-6 ellipsis">Flight Time:</div> 
                            <div class="col-6 ps-1 semi-bold ellipsis">${flDurationMin}</div>
                        </div>
                        <div class="flex-align-items mt-1 w-100">
                            <div class="col-6 ellipsis">Weekday:</div> 
                            <div class="col-6 ps-1 semi-bold ellipsis">${data?.weekday?.name}</div>
                        </div>
                    </div>
                </div>`;
        rv += `<div class="flex-align-items mt-1 w-100">
                <div class="col-4 ellipsis">Group:</div> 
                <div class="col-8 ps-1 semi-bold ellipsis">${data.group?.grpName ?? ''}</div>
            </div>`;
        rv += `<div class="flex-align-items mt-1 w-100">
                <div class="col-4 ellipsis">Flight:</div> 
                <div class="col-8 ps-1 semi-bold ellipsis">${data.flight.flpnsName ? data.flight.flpnsName : ''}</div>
            </div>`;

        return rv;
    }

    makeRoutePopup(data: AmsAlSheduleRoute, apDep: AmsAirport, apArr: AmsAirport): string {
        //console.log('makeFlightopup -> data:', data);
        let rv = '';

        const icon = this.getIconFlight(AmsFlightType.Schedule);
        const lenkmStr = this.lenkm.transform(data.distanceKm);
        const flTypeStr = this.fltype.transform(AmsFlightType.Schedule);
        const flDurationMin = this.periodmin.transform(data.flightH * 60);


        rv += `<div class="flex-align-items w-100 border-b1">
                    <div class="col-10 flex-align-items ellipsis pe-1">
                        <div class="icon-value mr-1">${flTypeStr}</div>
                        <i class="material-icons size-20 mr-1" >${icon}</i>
                        <div class="icon-value mr-1">${apDep.iata ? apDep.iata : apDep.icao}</div> - 
                        <div class="icon-value mx-1">${apArr.iata ? apArr.iata : apArr.icao}</div>
                    </div>
                    <div class="col-2 flex-align-items">
                        <div class="icon-value link-blue flex-end" id="spmRouteOpen">
                        <mat-icon class="material-icons">launch</mat-icon>
                    </div>
                </div>
            </div>`;

        if (data.routeId && false) {

            rv += `<div class="flex-align-items w-100 mt-2"> 
                        <div class="icon-value mr-1">${data.routeNr}</div>
                    </div>`;
        }

        rv += `<div class="flex-align-items w-100 mt-2"> 
                    <div class="col-6 flex-start-top">
                        <div class="icon-value mr-0 livery livery-table">
                            <div class="icon-value mr-1">${data.routeNr}</div>
                        </div>
                    </div>
                    <div class="col-6 flex-start-top">
                        <div class="flex-align-items mt-1 w-100 ellipsis">Distance: <b> ${lenkmStr}</b></div>
                        <div class="flex-align-items mt-1 w-100 ellipsis">Duration: <b>${flDurationMin}</b></div>
                        <div class="flex-align-items mt-1 w-100 ellipsis">Group #: <b>${data.routeNr}</b></div>
                    </div>
                
                </div>`;



        rv += `<div class="flex-align-items w-100 mt-1"> 
                    <div class="ellipsis">${data.routeNr ? data.routeNr : ''}</div>
            </div>`;
        //console.log('makeFlightopup -> rv:', rv);
        return rv;
    }

    makeACPopup(data: AmsMapFlight): string {
        //console.log('makeFlightopup -> data:', data);
        let rv = '';
        const flight = data.flight;
        const apDep: AmsAirport = data.depAp;
        const apArr: AmsAirport = data.arrAp;
        const icon = this.getIconFlight(flight.flTypeId);
        const lenkmStr = this.lenkm.transform(flight.distanceKm);
        const flTypeStr = this.fltype.transform(flight.flTypeId);
        const flDurationMin = this.periodmin.transform(flight.flDurationMin);
        const remainTimeMin = this.periodmin.transform(flight.remainTimeMin);
        const acType = AcTypeOpt.find(x => x.acTypeId === flight?.acTypeId);
        const airportCostWeek = this.curency.transform(acType.airportCostWeek);
        const insuranceCostWeek = this.curency.transform(acType.insuranceCostWeek);
        const hangarCostWeek = this.curency.transform(acType.hangarCostWeek);
        const flStatus = AmsFlightStatusOpt.find(x => x.id === flight?.acfsId);
        
        rv += `<div class="flex-align-items w-100 border-b1" style="width:320px;">
                    <div class="col-10 flex-align-items">
                        <div class="icon-value mx-1 blue semi-bold">${flight.acRegNr}</div>
                    </div>
                     <div class="col-2 flex-align-items">
                        <div class="flex-end icon-value link-blue" id="acImage">
                            <mat-icon class="material-icons">launch</mat-icon>
                        </div>
                    </div>
                </div>`;

        rv += `<div class="flex-align-items w-100 mt-1">
                    <div class="col-6 flex-column text-center px-1 img-150">
                        <img src="${flight.acImgTableUrl}" alt="${flight.acRegNr}">
                        <div class="icon-value m-0 mt-3 livery livery-table">
                            <img src=\"${flight.amsLiveryUrl}" alt="${flight.alName}">
                        </div>
                    </div>
                    <div class="col-6 flex-column px-1">
                        <div class="flex-align-items w-100 mt-1">
                            <div class="flex-start">Type</div>
                            <div class="flex-end">${acType.name}</div>
                        </div>
                        <div class="flex-align-items w-100 mt-1">
                            <div class="flex-start" title="Airport tax per week.">Airport Tax</div>
                            <div class="flex-end">${airportCostWeek}</div>
                        </div>
                         <div class="flex-align-items w-100 mt-1">
                            <div class="flex-start" title="Insurance cost per week.">Insurance</div>
                            <div class="flex-end">${insuranceCostWeek}</div>
                        </div>
                         <div class="flex-align-items w-100 mt-1">
                            <div class="flex-start" title="Hangar cost per week.">Hangar</div>
                            <div class="flex-end">${hangarCostWeek}</div>
                        </div>
                    </div>
                </div>
                <div class="flex-align-items w-100 mt-1">
                    <div class="flex-start pe-1" title="${flTypeStr} flight">
                        <i class="material-icons size-20 pb-1" >${icon}</i>
                    </div>
                    <div class="flex pe-1">${apDep.apName}</div>
                     <div class="flex pe-1">-</div>
                      <div class="flex pe-1">${apArr.apName}</div>
                </div>
                <div class="flex-align-items w-100 mt-1">
                    <div class="flex-start flex-align-items ">
                            Remaining: <b>${remainTimeMin}</b>
                    </div>
                </div>
                <div class="flex-align-items w-100 mt-1">
                    <div class="flex-start flex-align-items ">
                        <b>${flStatus.name}</b>
                    </div>
                </div>`;

        return rv;
    }


    getIconFlight(flType: AmsFlightType): string {
        let icon = ICON_FLIGHT;
        switch (flType) {
            case AmsFlightType.Charter:
                icon = ICON_CHARTER;
                break;
            case AmsFlightType.Transfer:
                icon = ICON_TRANSFER;
                break;
        }
        return icon;
    }

}