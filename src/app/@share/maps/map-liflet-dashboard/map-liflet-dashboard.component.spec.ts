import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapLifletDashboardComponent } from './map-liflet-dashboard.component';

describe('MAPS -> MapLifletDashboardComponent', () => {
  let component: MapLifletDashboardComponent;
  let fixture: ComponentFixture<MapLifletDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapLifletDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapLifletDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
