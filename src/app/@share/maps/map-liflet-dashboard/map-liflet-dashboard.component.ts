import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { LatLng } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { iconDefault, MarkerService } from '../map-liflet-ap-marker.service';
import { MapLeafletDashboardService } from '../map-liflet-dashboard.service';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsHub } from 'src/app/@core/services/api/airline/al-hub';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
import 'leaflet.geodesic';
declare let L;
//import * as L from 'leaflet-arc';


L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'ams-map-liflet-dashboard',
  templateUrl: './map-liflet-dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class MapLifletDashboardComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  @Input() hqAirport: AmsAirport;
  @Input() flights: AmsFlight[];
  //@Output() generateLivery: EventEmitter<AmsAirline> = new EventEmitter<AmsAirline>();

  map;
  usrLocation: LatLng;

  airports$: Observable<AmsAirport[]>;
  hubs$: Observable<AmsHub[]>;

  airportsChanged: Subscription;
  airportChanged: Subscription;
  flightsChanged: Subscription;

  airports: AmsAirport[];
  hubs: AmsHub[];
  viewInit: boolean = false;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private hubCache: AmsAirlineHubsCacheService,
    private mapService: MapLeafletDashboardService) {
  }



  ngOnInit(): void {
    //const s = Snap('#map');
    this.cus.getLocation().subscribe(location => {
      //console.log('d ngOnInit -> getLocation:', location);
      this.usrLocation = location;
    });

    this.airportsChanged = this.mapService.airportsChanged.subscribe(airports => {
      //console.log('d airportsChanged -> airports', airports);
      this.airports = airports;
      //this.makeMarkers();
    });
    this.flightsChanged = this.mapService.flightsChanged.subscribe(flights => {
      //console.log('d flightsChanged -> flights', flights);
      this.makeMarkers();
    });
    this.airports$ = this.apCache.airports;

    this.airports$.subscribe(airports => {
      this.airports = airports;
      this.hubs$ = this.hubCache.hubs;
      this.hubs$.subscribe(hubs => {
        this.hubs = hubs;
        this.initAirportsMarkers();
      });
    });


  }

  ngOnDestroy(): void {
    if (this.airportChanged) { this.airportChanged.unsubscribe(); }
    if (this.flightsChanged) { this.flightsChanged.unsubscribe(); }
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flights']) {
      //this.flights = changes['flights'].currentValue;
      this.mapService.flights = this.flights;
      //this.makeMarkers();
    }
    if (changes['hqAirport']) {
      //this.hqAirport = changes['hqAirport'].currentValue;
      if (this.hqAirport) {
        if (this.airports && this.airports.length > 0) {
          let apIdx = -1;
          apIdx = this.airports.findIndex(x => x.apId === this.hqAirport.apId);

          if (apIdx === -1) {
            this.airports.push(this.hqAirport);
            this.mapService.setAirportsNoEvent(this.airports);
          }
        }

        //this.makeMarkers();
      }


    }
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  private initMap(): void {

    const osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    const esri = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 18,
      minZoom: 3,
      attribution: 'Tiles &copy; Esri '
    });


    const esrigray = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 18,
      minZoom: 3,
      attribution: 'Tiles &copy; Esri',
    });

    this.map = L.map('dashboardMap', {
      center: [this.usrLocation ? this.usrLocation.lat : 39.8282, this.usrLocation ? this.usrLocation.lng : -98.5795],
      zoom: 7,
      layers: [esrigray, esri]
    });

    const baseMaps = {
      //"OpenStreetMap": osm,
      "Esri Gray": esrigray,
      "Esri": esri,
      //"Stadia": stadia,
    };

    if (this.map) {
      this.mapService.mapFlights = undefined;
      //osm.addTo(this.map);
      //tilesoEsri.addTo(this.map);
      const layerControl = L.control.layers(baseMaps).addTo(this.map);

      var component = this;
      this.map.on('zoomend', function (event) {
        const zoom = event.target.getZoom();
        component.showHideAirportGroups(zoom);
      });

    }
  }

  showHideAirportGroups(zoom: number): void {
    this.mapService.showHideAirportGroups(this.map);
  }

  makeMarkers(): void {
    if (this.map) {
      this.mapService.makeMarkers(this.map);
    }
  }

  initAirportsMarkers() {
    if (this.map && this.airports) {
      this.mapService.airports = this.airports;
      this.mapService.hubs = this.hubs;
      this.makeMarkers();
    }
  }

}
