import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { AmsAirport, IAmsAirport } from "src/app/@core/services/api/airport/dto";
import { AppStore, URL_COMMON_IMAGE_AIRCRAFT_ICON, URL_COMMON_IMAGE_AMS_COMMON } from "src/app/@core/const/app-storage.const";
import { BehaviorSubject, Subject } from "rxjs";
import { AmsAirportClient } from "src/app/@core/services/api/airport/api-client";
import { AmsWadCountryService } from "src/app/wad/country/country.service";
import { MapLeafletPopupAirportService } from "./map-liflet-popup-airport.service";
import { AmsFlight, IAmsFlight } from "src/app/@core/services/api/flight/dto";
import { AmsAirportCacheService } from "src/app/@core/services/api/airport/ams-airport-cache.service";
import { AmsFlightStatus } from "src/app/@core/services/api/flight/enums";
import { GeometryUtil, LayerGroup } from "leaflet";
import { AmsMapFlight, AmsMapMarkerOptions } from "./dto";
import { ApType } from "src/app/@core/models/pipes/ap-type.pipe";
import { AmsAirlineHubsCacheService } from "src/app/@core/services/api/airline/al-hub-cache.service";
import { AmsHub, IAmsHub } from "src/app/@core/services/api/airline/al-hub";
import { AmsHubTypes } from "src/app/@core/services/api/airline/enums";
import { AmsAirlineClient } from "src/app/@core/services/api/airline/api-client";
import { SpinnerService } from "src/app/@core/services/spinner.service";
import { Router } from "@angular/router";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import * as $ from 'jquery';
import { ajax, css } from "jquery";
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
//import * as L from 'leaflet-arc';
declare let L;

export class ArcOptions {
    color: string;
    vertices: number;
    offset: number;
}

export const apIconType1 = L.icon({
    iconUrl: URL_COMMON_IMAGE_AIRCRAFT_ICON + 'yellow3.png',
    iconSize: [80, 80],
    iconAnchor: [40, 40],
});

export const DIV_ICON = L.divIcon({
    iconSize: L.point(24, 24), // icon size must same with element size
    iconAnchor: [12, 24],
    className: 'position-relative rotate--marker',
    html: `<div>
          <img style="width:24px;"
            src="${URL_COMMON_IMAGE_AIRCRAFT_ICON + 'yellow3.png'}"
          />
        </div>
      </>`,
});

//L.divIcon({html: "<i class='fa fa-car fa-2x' aria-hidden='true'></i>", iconSize: L.point(60, 60)});
@Injectable({
    providedIn: 'root'
})
export class MapLeafletDashboardService {

    iconsRootUrl = URL_COMMON_IMAGE_AIRCRAFT_ICON;


    flightPathsArray: any[];
    flightPathsGroup: L.FeatureGroup;

    mapFlights: AmsMapFlight[];

    acMarkerArray: Array<any>;

    //#region leaflet.bezier

    bezierPathArray: any[];
    bezierOptionsA = {
        color: 'rgb(145, 146, 150)',
        fillColor: 'rgb(145, 146, 150)',
        dashArray: 8,
        opacity: 0.8,
        weight: '1',
        iconTravelLength: 1,
        iconMaxWidth: 50,
        iconMaxHeight: 50,
        fullAnimatedTime: 7000,
        easeOutPiece: 4,
        easeOutTime: 2500,
    };

    //#endregion

    spmMapPanelOpen = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient,
        private router: Router,
        private preloader: SpinnerService,
        private cus: CurrentUserService,
        private apClient: AmsAirportClient,
        private cService: AmsWadCountryService,
        private apCache: AmsAirportCacheService,
        private hubCache: AmsAirlineHubsCacheService,
        private alClient: AmsAirlineClient,
        private popupApService: MapLeafletPopupAirportService) { }



    //#region AmsFlight

    flightsChanged = new Subject<AmsFlight[]>();
    _flights: AmsFlight[];

    get flights(): AmsFlight[] {
        return this._flights;
        /*
        const objStr = localStorage.getItem(AppStore.mapFlightsSpm);
        let obj: AmsFlight[] = new Array<AmsFlight>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsFlight[];//Object.assign(new AmsAirport(), );
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsFlight) => {
                    const ap = AmsFlight.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
        */
    }

    set flights(value: AmsFlight[]) {

        if (value && value.length > 0) {
            this._flights = value;
            //localStorage.setItem(AppStore.mapFlightsSpm, JSON.stringify(value));
        } else {
            this._flights = new Array<AmsFlight>();
            //localStorage.removeItem(AppStore.mapFlightsSpm);
        }

        this.flightsChanged.next(value);
    }

    makeMarkers(map: L.Map): void {
        //console.log('makeMarkers -> map:', map);
        if (map) {
            this.clearLayers(map);
            //console.log('makeMarkers -> flights:', this.flights);
            if (this.flights && this.flights.length > 0) {
                this.prepareAirports().then(res => {
                    this.makeAirportMarkers(map);
                    //this.makeFlightPaths(map);
                    this.makeMapFlights(map);
                });
            } else if (this.airports && this.airports.length > 0) {
                this.makeAirportMarkers(map);
                //his.makeMapFlights(map);
            }
        }

    }

    mapFlightsToRemove(): AmsMapFlight[] {
        let rv = new Array<AmsMapFlight>();
        //console.log('mapFlightsToRemove -> flights:', this.flights);
        //console.log('mapFlightsToRemove -> mapFlights:', this.mapFlights);
        const filteredIds = this.flights.map(({ flId }) => flId);
        //console.log('mapFlightsToRemove -> flights:', this.flights);
        //console.log('mapFlightsToRemove -> mapFlights:', this.mapFlights);
        if (this.flights && this.mapFlights && this.mapFlights.length > 0) {
            rv = this.mapFlights.filter(function (mf: AmsMapFlight) {
                return filteredIds.indexOf(mf.flight?.flId) === -1;
            });
        }
        //console.log('mapFlightsToRemove -> rv:', rv);
        return rv;
    }

    mapFlightsToUpdate(): AmsMapFlight[] {
        let rv = new Array<AmsMapFlight>();
        //console.log('mapFlightsToUpdate -> flights:', this.flights);
        //console.log('mapFlightsToUpdate -> mapFlights:', this.mapFlights);
        const filteredIds = this.flights.map(({ flId }) => flId);
        //console.log('mapFlightsToUpdate -> filteredIds:', filteredIds);
        if (this.flights && this.mapFlights && this.mapFlights.length > 0) {
            rv = this.mapFlights.filter(function (mf: AmsMapFlight) {
                return filteredIds.indexOf(mf.flight?.flId) !== -1;
            });
        }
        //console.log('mapFlightsToUpdate -> rv:', rv);
        return rv;
    }


    flightsToAdd(): AmsFlight[] {
        let rv = this.flights;
        if (this.flights && this.mapFlights && this.mapFlights.length > 0) {
            const filteredIds = this.mapFlights.map(({ flId }) => flId);
            //console.log('flightsToAdd -> filteredIds:', filteredIds);
            rv = this.flights.filter(function (fl: AmsFlight) {
                return filteredIds.indexOf(fl.flId) === -1;
            });
        }
        //console.log('flightsToAdd -> rv:', rv);
        return rv;
    }

    removeMapFlights(map: L.Map, toRemove: AmsMapFlight[]): void {
        if (toRemove && toRemove.length > 0) {
            toRemove.forEach((mapFlight: AmsMapFlight) => {
                const idx = this.mapFlights.findIndex(x => x.flight.flId === mapFlight.flight.flId);
                //console.log('makeMapFlights -> toRemove idx:', idx);
                let hasLayer = map.hasLayer(mapFlight.acMarker);
                if (hasLayer) {
                    map.removeLayer(mapFlight.acMarker);
                }

                if (mapFlight.featureGroup && mapFlight.featureGroup.getLayers() &&
                    mapFlight.featureGroup.getLayers().length > 0) {
                    mapFlight.featureGroup.clearLayers();
                }
                hasLayer = mapFlight?.flPath ? map.hasLayer(mapFlight?.flPath) : false;
                if (hasLayer) {
                    map.removeLayer(mapFlight?.flPath);
                }
                this.mapFlights.splice(idx, 1);
            });
        }
    }

    makeMapFlights(map: L.Map): void {
        if (map) {
            if (!this.mapFlights) { this.mapFlights = new Array<AmsMapFlight>(); }

            const toRemove = this.mapFlightsToRemove();
            if (toRemove && toRemove.length > 0) {
                this.removeMapFlights(map, toRemove);
            }
            const toUpdate = this.mapFlightsToUpdate();
            if (toUpdate && toUpdate.length > 0) {
                toUpdate.forEach((mapFlight: AmsMapFlight) => {
                    const flight = this.flights.find(x => x.flId === mapFlight.flId);
                    mapFlight.flight = flight;
                });
            }

            const flights = this.flightsToAdd();
            if (flights && flights.length > 0) {

                for (const flight of flights) {
                    let mapFlight = new AmsMapFlight();
                    mapFlight.flight = flight;

                    mapFlight.depAp = this.airports?.find(x => x.apId === flight.depApId);
                    mapFlight.arrAp = this.airports?.find(x => x.apId === flight.arrApId);
                    mapFlight.makeFlightPaths(map, this.popupApService);
                    //console.log('makeMapFlights -> flPath:', mapFlight?.flPath);
                    if (mapFlight?.flPath) {
                        mapFlight.flPath.addTo(map);
                        //console.log('makeMapFlights -> acMarker:', mapFlight?.acMarker);
                        if (mapFlight?.acMarker) {
                            console.log('makeMapFlights -> mapFlight:', mapFlight);
                            mapFlight.acMarker.bindPopup(this.popupApService.makeACPopup(mapFlight));
                            mapFlight.acMarker.addTo(map).on("popupopen", (a) => {
                                var popUp = a.target.getPopup()
                                popUp.getElement()
                                    .querySelector("#acImage")
                                    .addEventListener("click", e => {
                                        this.spmAircradftOpenClick(flight.acId);
                                    })
                            });
                            mapFlight.acMarker.motionResume();
                        }


                    } else {
                        console.log('makeMapFlights -> No flPath  mapFlight:', mapFlight);
                    }

                    //.on('click', function() { alert('Clicked on a group!'); }).addTo(map);
                    //mapFlight.featureGroup.addTo(map);
                    this.mapFlights.push(mapFlight);
                }
            }
        }
    }

    makeFlightPaths(map: L.Map): void {
        if (map) {

            const flights = this.flights
            this.flightPathsArray = new Array<L.Circle>();
            if (!this.acMarkerArray) { this.acMarkerArray = new Array<L.Marker>(); }
            if (flights && flights.length > 0) {

                for (const flight of flights) {
                    let mapFlight = new AmsMapFlight();
                    mapFlight.flight = flight;

                    mapFlight.depAp = this.airports?.find(x => x.apId === flight.depApId);
                    mapFlight.arrAp = this.airports?.find(x => x.apId === flight.arrApId);

                    let color = 'blue';
                    if (flight.flId && flight.acfsId > AmsFlightStatus.PlaneStand) {
                        color = 'yellow';
                    }
                    const latlngDep = L.latLng(mapFlight.depAp.lat, mapFlight.depAp.lon);
                    const latlngArr = L.latLng(mapFlight.arrAp.lat, mapFlight.arrAp.lon);
                    const latlngs = [latlngDep, latlngArr];
                    const angle = GeometryUtil.angle(map, latlngDep, latlngArr);
                    const bearing = GeometryUtil.bearing(latlngDep, latlngArr);
                    const opt = { color: color, vertices: 10, offset: 0, weight: 2, opacity: 0.6, _flId: flight.flId };
                    const path = L.Polyline.Arc(latlngDep, latlngArr, opt);

                    let flIdx = -1;
                    if (flight.flId && this.acMarkerArray && this.acMarkerArray.length > 0) {
                        flIdx = this.acMarkerArray.findIndex(x => x.options._flId === flight.flId);
                    }
                    if (flIdx === -1 && flight.flId && flight.acfsId > AmsFlightStatus.PlaneStand) {
                        let marker: L.Marker;
                        const speedmPh = (flight.speedKmPh / 3.6).toFixed(0); //m/sec
                        const intervalMs = 60_000; // 1 sec
                        const distanseStartM = (flight.distanceKm - flight.remainDistanceKm) * 1000;
                        const duration = flight.remainTimeMin * 60 * 1000;
                        const mPathStart: L.LatLng = flight.remainDistanceKm > 0 ? this.callculateDistance(path, latlngDep, distanseStartM) : latlngArr;

                        const mPath = L.Polyline.Arc(mPathStart, latlngArr, { color: 'blue', vertices: 10, offset: 0 });
                        const animatePolyline = true;
                        const animateMarker = true;
                        let acIcon = apIconType1;
                        acIcon.options.iconUrl = flight.acImgMapUrl;

                        const divIcon = L.divIcon({ html: `<i class="material-icons" aria-hidden="true" motion-base="${angle}">flight</i>`, iconSize: L.point(24, 24), iconAnchor: [12, 24], });

                        const markerOpt = {
                            removeOnEnd: true,
                            showMarker: true,
                            icon: acIcon,
                            rotationAngle: angle,
                        };
                        const motionOpt = {
                            auto: true, //auto start animation when motion object added to the map
                            duration: duration, //Motion duration in ms
                            speed: flight.speedKmPh, //Motion speed in KM/H
                            easing: L.Motion.Ease.easeInOutQuart, //Animation strategy
                        };

                        marker = L.motion.polyline(mPath.getLatLngs(), { color: "green", weight: 1, opacity: 0.8, _flId: flight.flId }, motionOpt, markerOpt);
                        marker.bindPopup(this.popupApService.makeACPopup(mapFlight));
                        marker.addTo(map).on("popupopen", (a) => {
                            var popUp = a.target.getPopup()
                            popUp.getElement()
                                .querySelector("#acImage")
                                .addEventListener("click", e => {
                                    this.spmAircradftOpenClick(flight.acId);
                                })
                        });
                        this.acMarkerArray.push(marker);

                    } else {
                        //let marker: L.Marker = this.acMarkerArray[flIdx];
                        //this.acMarkerArray[flIdx].unbindPopup();
                        //this.acMarkerArray[flIdx].bindPopup(this.popupApService.makeFlightopup(flight, depAp, arrAp));
                        this.acMarkerArray[flIdx].motionResume();
                    }
                    path.bindPopup(this.popupApService.makeFlightPopup(flight, mapFlight.depAp, mapFlight.arrAp));
                    this.flightPathsArray.push(path);
                }

                /*
                this.airport = airports[0];
                if(this.airport){
                    this.centerMap(map, new L.LatLng(this.airport.lat, this.airport.lon), this.airport.mapZoom)
                }*/
            }

            this.flightPathsGroup = L.featureGroup(this.flightPathsArray);
            //.on('click', function() { alert('Clicked on a group!'); }).addTo(map);
            this.flightPathsGroup.addTo(map);
            /*
            if (this.acMarkerArray && this.acMarkerArray.length > 0) {
                this.acMarkerArray.forEach(marker => {
                    //map.addLayer(marker);
                    marker.addTo(map);
                });
            }
            */
        }
    }

    //#region  SPM

    spmAircradftOpenClick(acId: number): void {
        if (acId) {
            this.cus.spmAircraftPanelOpen.next(acId);
        }
    }

    spmAirportOpenClick(apId: number): void {
        if (apId) {
            this.cus.spmAirportPanelOpen.next(apId);
        }
    }

    openHubClick(ap: AmsAirport): void {
        console.log('openHubClick -> ap:', ap);
        if (ap && !ap.hub) {
            let req = new AmsHub();
            req.apId = ap.apId;
            req.alId = this.cus.airline.alId;
            this.preloader.show();
            this.alClient.hubSave(req).then(res => {
                console.log('openHubClick -> res:', res);
                this.preloader.hide();
                this.hubCache.clearCache();
                this.hubs = undefined;
                this.router.navigate(['/', AppRoutes.airline, AppRoutes.info, AppRoutes.hubs]);
            }).catch(err => {
                this.preloader.hide();
                console.log('openHubClick -> err:', err);
            });

        }
    }

    //#endregion

    //#region Airports

    apMarkerArray: Array<L.Marker>;
    hubsMarkerArray: Array<L.Marker>;
    apMarkerGroup: L.FeatureGroup;

    apAirfieldGroup: L.FeatureGroup;
    apLocaldGroup: L.FeatureGroup;
    apRegionaldGroup: L.FeatureGroup;
    apIntdGroup: L.FeatureGroup;
    apLargeGroup: L.FeatureGroup;
    hubsGroup: L.FeatureGroup;

    airportsChanged = new Subject<AmsAirport[]>();
    airportChanged = new Subject<AmsAirport>();

    get apAirfieldMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId < ApType.Local));
        }
        return rv;
    };
    get apLocalMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId > ApType.Airfield && (x.options as AmsMapMarkerOptions).airport.typeId < ApType.Regional));
        }
        return rv;
    };

    get apRegionalMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId > ApType.Local && (x.options as AmsMapMarkerOptions).airport.typeId < ApType.SmallInt));
        }
        return rv;
    };

    get apLargeMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId > ApType.Int));
        }
        return rv;
    };

    get apIntMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId > ApType.Regional && (x.options as AmsMapMarkerOptions).airport.typeId < ApType.LargeInt));
        }
        return rv;
    };

    get hubs(): AmsHub[] {
        const objStr = localStorage.getItem(AppStore.mapHubsSpm);
        let obj: AmsHub[] = new Array<AmsHub>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsHub[];//Object.assign(new AmsAirport(), );
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsHub) => {
                    const ap = AmsHub.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
    }

    set hubs(value: AmsHub[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapHubsSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapHubsSpm);
        }
        let aps = this.airports;
        const hubs = this.hubs;
        let updateAps = false;
        if (aps && aps.length > 0) {
            let hub: AmsHub;
            aps.forEach((ap: AmsAirport) => {
                if (hubs && hubs.length > 0) {
                    hub = hubs.find(x => x.apId === ap.apId);
                }
                if (hub) {
                    updateAps = true;
                    //console.log('set hubs -> hub:', hub);
                    ap.hub = hub
                } else {
                    ap.hub = undefined;
                };
            });
        }
        if (updateAps) {
            this.setAirportsNoEvent(aps);
        }

    }

    _airports: AmsAirport[];
    get airports(): AmsAirport[] {
        return this._airports;
        /*
        const objStr = localStorage.getItem(AppStore.mapAirportsSpm);
        let obj: AmsAirport[] = new Array<AmsAirport>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsAirport[];//Object.assign(new AmsAirport(), );
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsAirport) => {
                    const ap = AmsAirport.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
        */
    }

    set airports(value: AmsAirport[]) {
        this.setAirportsNoEvent(value)
        this.airportsChanged.next(value);
    }

    setAirportsNoEvent(value: AmsAirport[]) {

        if (value && value.length > 0) {
            this._airports = value;
            //localStorage.setItem(AppStore.mapAirportsSpm, JSON.stringify(value));
        } else {
            this._airports = new Array<AmsAirport>();
            //localStorage.removeItem(AppStore.mapAirportsSpm);
        }
    }

    get airport(): AmsAirport {
        const onjStr = localStorage.getItem(AppStore.mapAirportSpm);
        let obj: AmsAirport;
        if (onjStr) {
            obj = Object.assign(new AmsAirport(), JSON.parse(onjStr));
        }
        return obj;
    }

    set airport(value: AmsAirport) {
        const oldValue = this.airport;
        if (value) {
            localStorage.setItem(AppStore.mapAirportSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirportSpm);
        }

        this.airportChanged.next(value);
    }


    makeAirportMarkers(map: L.Map): void {
        if (map) {
            const airports = this.airports
            this.apMarkerArray = new Array<L.Marker>();
            this.hubsMarkerArray = new Array<L.Marker>();
            if (airports && airports.length > 0) {

                for (const ap of airports) {
                    if (ap.lat && ap.lon) {
                        const marker = this.getApMatrker(ap);
                        if (marker) {
                            this.apMarkerArray.push(marker);
                        }
                    }
                    if (ap.hub) {
                        const marker = this.getHubMatrker(ap);
                        if (marker) {
                            this.hubsMarkerArray.push(marker);
                        }
                    }
                }

                /*
                this.airport = airports[0];
                if(this.airport){
                    this.centerMap(map, new L.LatLng(this.airport.lat, this.airport.lon), this.airport.mapZoom)
                }*/
            }
            const zoom = map.getZoom();
            const apAirfields = this.apAirfieldMarkers;
            if (apAirfields && apAirfields.length > 0) {
                this.apAirfieldGroup = L.featureGroup(apAirfields);
                //this.apAirfieldGroup.addTo(map);
            }
            const apLocal = this.apLocalMarkers;
            if (apLocal && apLocal.length > 0) {
                this.apLocaldGroup = L.featureGroup(apLocal);
                //this.apLocaldGroup.addTo(map);
            }
            const apReg = this.apRegionalMarkers;
            if (apReg && apReg.length > 0) {
                this.apRegionaldGroup = L.featureGroup(apReg);
                //this.apRegionaldGroup.addTo(map);
            }
            const apLarge = this.apLargeMarkers;
            if (apLarge && apLarge.length > 0) {
                this.apLargeGroup = L.featureGroup(apLarge);
                this.apLargeGroup.addTo(map);
            }
            const apInt = this.apIntMarkers;
            if (apInt && apInt.length > 0) {
                this.apIntdGroup = L.featureGroup(apInt);
                this.apIntdGroup.addTo(map);
            }
            if (this.hubsMarkerArray && this.hubsMarkerArray.length > 0) {
                this.hubsGroup = L.featureGroup(this.hubsMarkerArray);
                this.hubsGroup.addTo(map);
            }
            //this.showHideAirportGroups(map);
            //this.apMarkerGroup = L.featureGroup(this.apMarkerArray);
            //this.apMarkerGroup.addTo(map);
            if (this.apMarkerArray && this.apMarkerArray.length > 0) {
                //map.fitBounds(this.apMarkerGroup.getBounds());
            }

        }
    }

    clearAirportMarkers(map: L.Map): void {
        //console.log('clearAirportMarkers -> ');

        if (this.apAirfieldGroup && this.apAirfieldGroup.getLayers() &&
            this.apAirfieldGroup.getLayers().length > 0) {
            this.apAirfieldGroup.clearLayers();
        }

        if (this.apLocaldGroup && this.apLocaldGroup.getLayers() &&
            this.apLocaldGroup.getLayers().length > 0) {
            this.apLocaldGroup.clearLayers();
        }

        if (this.apRegionaldGroup && this.apRegionaldGroup.getLayers() &&
            this.apRegionaldGroup.getLayers().length > 0) {
            this.apRegionaldGroup.clearLayers();
        }

        if (this.apIntdGroup && this.apIntdGroup.getLayers() &&
            this.apIntdGroup.getLayers().length > 0) {
            this.apIntdGroup.clearLayers();
        }

        if (this.hubsGroup && this.hubsGroup.getLayers() &&
            this.hubsGroup.getLayers().length > 0) {
            this.hubsGroup.clearLayers();
        }
        if (this.apLargeGroup && this.apLargeGroup.getLayers() &&
            this.apLargeGroup.getLayers().length > 0) {
            this.apLargeGroup.clearLayers();
        }

        if (this.apMarkerGroup && this.apMarkerGroup.getLayers() &&
            this.apMarkerGroup.getLayers().length > 0) {
            this.apMarkerGroup.clearLayers();
        }
    }

    showHideAirportGroups(map: L.Map) {
        const zoom = map.getZoom();
        if (this.apAirfieldGroup && this.apAirfieldGroup.getLayers() &&
            this.apAirfieldGroup.getLayers().length > 0) {
            const hasLayer = map.hasLayer(this.apAirfieldGroup);
            if (zoom > 9) {
                if (!hasLayer) { map.addLayer(this.apAirfieldGroup); }
            } else {
                if (hasLayer) { map.removeLayer(this.apAirfieldGroup); }
            }
        }
        if (this.apLocaldGroup && this.apLocaldGroup.getLayers() &&
            this.apLocaldGroup.getLayers().length > 0) {
            const hasLayer = map.hasLayer(this.apLocaldGroup);
            if (zoom > 8) {
                if (!hasLayer) { map.addLayer(this.apLocaldGroup); }
            } else {
                if (hasLayer) { map.removeLayer(this.apLocaldGroup); }
            }
        }
        if (this.apRegionaldGroup && this.apRegionaldGroup.getLayers() &&
            this.apRegionaldGroup.getLayers().length > 0) {
            const hasLayer = map.hasLayer(this.apRegionaldGroup);
            if (zoom > 7) {
                if (!hasLayer) { map.addLayer(this.apRegionaldGroup); }
            } else {
                if (hasLayer) { map.removeLayer(this.apRegionaldGroup); }
            }
        }
        if (this.apIntdGroup && this.apIntdGroup.getLayers() &&
            this.apIntdGroup.getLayers().length > 0) {
            const hasLayer = map.hasLayer(this.apIntdGroup);
            if (zoom > 6) {
                if (!hasLayer) { map.addLayer(this.apIntdGroup); }
            } else {
                if (hasLayer) { map.removeLayer(this.apIntdGroup); }
            }
        }

    }

    //#endregion

    //#endregion

    //#region Helper Methods

    callculateDistance(polyline: L.Polyline, start: L.LatLng, distanceM: number): L.LatLng {
        let rv: L.LatLng = start;
        let firstPoint: L.LatLng = undefined;
        //console.log('callculateDistance -> distanceM:', distanceM);
        if (polyline) {
            const points = polyline.getLatLngs();
            //console.log('callculateDistance -> points:', points);
            let idx = 0;
            if (points && points.length > 0) {
                points.forEach(function (latLng) {
                    const distanceToM = start.distanceTo(latLng);
                    if (distanceToM < distanceM) {
                        //console.log('callculateDistance -> '+idx+' distanceToM:', distanceToM);
                        rv = latLng;
                    }
                    idx++;
                });

            }
        }
        return rv;
    }

    async prepareAirports(): Promise<void> {
        let airports = new Array<AmsAirport>();
        if (this.flights && this.flights.length > 0) {

            for (const flight of this.flights) {
                //console.log('prepareAirports -> flight:', flight);
                if (airports.findIndex(x => x.apId === flight.depApId) === -1) {


                    await this.apCache.getAirportAsync(flight.depApId).then(ap => {
                        //console.log('prepareAirports -> ap:', ap);
                        airports.push(ap);
                    });
                }
                if (airports.findIndex(x => x.apId === flight.arrApId) === -1) {
                    await this.apCache.getAirportAsync(flight.arrApId).then(ap => {
                        airports.push(ap);
                        //console.log('prepareAirports -> ap:', ap);
                    });
                }
            };
        }
        //console.log('prepareAirports -> airports:', airports);
        //this.setAirportsNoEvent(airports);
    }

    clearLayers(map: L.Map): void {
        this.clearAirportMarkers(map);
        if (this.flightPathsGroup && this.flightPathsGroup.getLayers() &&
            this.flightPathsGroup.getLayers().length > 0) {
            this.flightPathsGroup.clearLayers();
            this.flightPathsArray = new Array<L.Circle>();
        }
        if (this.acMarkerArray && this.acMarkerArray.length > 0) {
            this.acMarkerArray.forEach((acMerker: any) => {
                const hasLayer = map.hasLayer(acMerker);

                if (hasLayer) {
                    map.removeLayer(acMerker);
                }
            });
        }
        this.acMarkerArray = new Array<any>();
    }

    acMarkersResume(map: L.Map): void {
        let activeAcMarkers = new Array<any>();
        if (this.acMarkerArray && this.acMarkerArray.length > 0) {
            //console.log('clearLayers -> acMarkerArray', this.acMarkerArray);
            this.acMarkerArray.forEach((acMerker: any) => {
                //console.log('clearLayers -> acMerker:', acMerker);
                let flIdx = -1;
                if (this.flights && this.flights.length > 0 &&
                    acMerker.options && acMerker.options._flId) {
                    flIdx = this.flights.findIndex(x => x.flId === acMerker.options._flId);
                }
                //console.log('clearLayers -> flIdx:', flIdx);
                if (flIdx === -1) {
                    map.removeLayer(acMerker);
                } else {
                    activeAcMarkers.push(acMerker);
                }
            });
            if (activeAcMarkers && activeAcMarkers.length > 0) {

                activeAcMarkers.forEach(acMerker => {
                    console.log('clearLayers -> acMerker', acMerker);
                    const hasLayer = map.hasLayer(acMerker);

                    if (!hasLayer) {
                        //console.log('clearLayers -> hasLayer', hasLayer);
                        const flight = this.flights.find(x => x.flId === acMerker.options._flId);
                        let mapFlight = new AmsMapFlight();
                        mapFlight.flight = flight;
                        mapFlight.depAp = this.airports?.find(x => x.apId === flight.depApId);
                        mapFlight.arrAp = this.airports?.find(x => x.apId === flight.arrApId);

                        const marker = L.motion.polyline(acMerker.getLatLngs(), acMerker.options, acMerker.motionOptions, acMerker.markerOptions);
                        marker.bindPopup(this.popupApService.makeACPopup(mapFlight));
                        //acMerker.getMarkers();
                        marker.addTo(map).on("popupopen", (a) => {
                            var popUp = a.target.getPopup()
                            popUp.getElement()
                                .querySelector("#acImage")
                                .addEventListener("click", e => {
                                    this.spmAircradftOpenClick(flight.acId);
                                })
                        });
                        marker.motionResume();
                        acMerker = marker;
                    }
                });
            }
            this.acMarkerArray = activeAcMarkers;
            //console.log('clearLayers -> leftmarkers', this.acMarkerArray);
            //console.log('clearLayers -> apMarkerGroup.clearLayers:', this.apMarkerGroup);
        }
    }

    getApMatrker(ap: AmsAirport): any {
        let marker;
        if (ap.lat && ap.lon) {
            const lat = ap.lat;
            const lon = ap.lon;

            const svgIcon = L.divIcon({
                html: ap.svgIconStr,
                className: ap.getStyle(),
                iconSize: [32, 32],
                iconAnchor: [8, 8],
            });

            marker = L.marker([lat, lon], { icon: svgIcon, airport: ap, apId: ap.apId });
            marker.bindPopup(this.popupApService.makeAirportPopup(ap));
            marker.on("popupopen", (a) => {
                var popUp = a.target.getPopup()
                popUp.getElement()
                    .querySelector("#spmApOpen")
                    .addEventListener("click", e => {
                        this.spmAirportOpenClick(ap.apId);
                    });
                if (!ap.hub) {
                    popUp.getElement()
                        .querySelector("#btnHubOpen")
                        .addEventListener("click", e => {
                            this.openHubClick(ap);
                        });
                }
            });
        }
        //console.log('getApMatrker -> marker:', marker);
        return marker;
    }

    getHubMatrker(ap: AmsAirport): any {
        let marker;
        if (ap.lat && ap.lon) {
            const lat = ap.lat;
            const lon = ap.lon;

            const baseIcon = L.IconMaterial.icon({
                icon: 'home',
                iconColor: '#004d00',
                markerColor: 'rgba(26, 255, 26,0.4)',
                outlineColor: 'rgba(0, 51, 0,0.8)',
                outlineWidth: 1,
                iconSize: [28, 38]
            });
            const hubIcon = L.IconMaterial.icon({
                icon: 'hub',                                // Name of Material icon
                iconColor: '#fff',                       // Material icon color (could be rgba, hex, html name...)
                markerColor: 'rgba(0, 92, 230,0.4)',       // Marker fill color
                outlineColor: 'rgba(0, 51, 128,0.8)',      // Marker outline color
                outlineWidth: 1,
                iconSize: [28, 38]                         // Width and height of the icon
            });

            if (ap.hub.hubTypeId === AmsHubTypes.Base) {
                marker = L.marker([lat, lon], { icon: baseIcon, airport: ap, apId: ap.apId });
                marker.bindTooltip(`Base ${ap.hub.hubName}`);
            } else {
                marker = L.marker([lat, lon], { icon: hubIcon, airport: ap, apId: ap.apId });
                marker.bindTooltip(`Hub ${ap.hub.hubName}`);
            }

            /*
            marker.bindPopup(this.popupApService.makeAirportPopup(ap));
            marker.on("popupopen", (a) => {
                var popUp = a.target.getPopup()
                popUp.getElement()
                    .querySelector("#spmApOpen")
                    .addEventListener("click", e => {
                        this.spmAirportOpenClick(ap.apId);
                    })
            });
            */
        }
        //console.log('getApMatrker -> marker:', marker);
        return marker;
    }

    centerMap(map: L.Map, latLnn: L.LatLng, zool: number = 8): void {
        if (map && latLnn) {
            map.setView(latLnn, zool);
        }
    }

    //#endregion




}
