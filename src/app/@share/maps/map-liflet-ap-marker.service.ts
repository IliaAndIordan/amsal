import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";

import { AmsAirport, AmsAirportTableCriteria, IAmsAirport, ResponseAmsAirportTableData } from "src/app/@core/services/api/airport/dto";
import { AppStore } from "src/app/@core/const/app-storage.const";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { AmsAirportClient } from "src/app/@core/services/api/airport/api-client";
import { AmsWadCountryService } from "src/app/wad/country/country.service";
import { AmsCountry, AmsState } from "src/app/@core/services/api/country/dto";

import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
//import * as L from 'leaflet-arc';

declare let L;

export const KEY_CRITERIA = 'ams_map_markers_airport_criteria';

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
export const iconDefault = L.icon({
    iconRetinaUrl,
    iconUrl,
    shadowUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41]
});

@Injectable({
    providedIn: 'root'
})
export class MarkerService {

    markerArray: Array<L.Marker>;
    markerGroup: L.FeatureGroup;


    constructor(private http: HttpClient,
        private cus: CurrentUserService,
        private apClient: AmsAirportClient,
        private cService: AmsWadCountryService) { }



    //#region airport

    airportsChanged = new Subject<AmsAirport[]>();
    airportChanged = new Subject<AmsAirport>();

    get airports(): AmsAirport[] {
        const objStr = localStorage.getItem(AppStore.mapAirports);
        let obj: AmsAirport[] = new Array<AmsAirport>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsAirport[];//Object.assign(new AmsAirport(), );
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsAirport) => {
                    const ap = AmsAirport.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
    }

    set airports(value: AmsAirport[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapAirports, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirports);
        }

        this.airportsChanged.next(value);
    }

    get airport(): AmsAirport {
        const onjStr = localStorage.getItem(AppStore.mapAirport);
        let obj: AmsAirport;
        if (onjStr) {
            obj = Object.assign(new AmsAirport(), JSON.parse(onjStr));
        }
        return obj;
    }

    set airport(value: AmsAirport) {
        const oldValue = this.airport;
        if (value) {
            localStorage.setItem(AppStore.mapAirport, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirport);
        }

        this.airportChanged.next(value);
    }

    makeAirportMarkers(map: L.Map): void {
        if (map) {
            if (this.markerGroup && this.markerGroup.getLayers() &&
                this.markerGroup.getLayers().length > 0) {
                this.markerGroup.clearLayers()
            }
            const airports = this.airports
            this.markerArray = new Array<L.Marker>();
            if (airports && airports.length > 0) {

                for (const ap of airports) {
                    if (ap.lat && ap.lon) {
                        const lat = ap.lat;
                        const lon = ap.lon;

                        //const marker = L.marker([lat, lon]);

                        const svgIcon = L.divIcon({
                            html: ap.svgIconStr,
                            className: ap.getStyle(),
                            iconSize: [32, 32],
                            iconAnchor: [8, 8],
                        });
                        const marker = L.marker([lat, lon], { icon: svgIcon });
                        this.markerArray.push(marker);
                        //marker.addTo(map);
                    }
                }

                /*
                this.airport = airports[0];
                if(this.airport){
                    this.centerMap(map, new L.LatLng(this.airport.lat, this.airport.lon), this.airport.mapZoom)
                }*/
            }
            this.markerGroup = L.featureGroup(this.markerArray);
            this.markerGroup.addTo(map);
            if(this.markerArray && this.markerArray.length>0){
                map.fitBounds(this.markerGroup.getBounds());
            }
            
        }

    }

    centerMap(map: L.Map, latLnn: L.LatLng, zool: number = 8): void {
        if (map && latLnn) {
            map.setView(latLnn, zool);
        }
    }


    //#endregion

    //#region loadingSubject

    private _isLoading = false;
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    //#endregion

    //#region Load Airport

    public apCriteriaChanged = new Subject<AmsAirportTableCriteria>();

    public get apCriteria(): AmsAirportTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsAirportTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsAirportTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsAirportTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'typeId';
            obj.sortDesc = true;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set apCriteria(obj: AmsAirportTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.apCriteriaChanged.next(obj);
    }

    setCountryCriteria(country: AmsCountry): void {
        //console.log('setCountryCriteria -> country:', country)
        if (country) {
            let criteria = this.apCriteria;
            criteria.regionId = undefined;
            criteria.subregionId = undefined;
            criteria.countryId = country.countryId;
            criteria.stateId = undefined;
            criteria.limit = country.airports ? country.airports + 1 : 1000;
            criteria.offset = 0;
            criteria.sortCol = 'typeId';
            criteria.sortDesc = true;
            this.apCriteria = criteria;
        }

    }

    setStateCriteria(state: AmsState): void {
        //console.log('setCountryCriteria -> country:', country)
        if (state) {
            let criteria = this.apCriteria;
            criteria.regionId = undefined;
            criteria.subregionId = undefined;
            criteria.countryId = undefined;
            criteria.stateId = state.stateId;
            criteria.limit = state.airports ? state.airports : 100;
            criteria.offset = 0;
            criteria.sortCol = 'maxRwLenght';
            criteria.sortDesc = true;
            this.apCriteria = criteria;
        }

    }


    loadAirportData(): Observable<Array<AmsAirport>> {
        console.log('loadAirportData->');
        //console.log('loadAirportData-> apCriteria=', this.apCriteria);

        this.isLoading = true;

        return new Observable<Array<AmsAirport>>(subscriber => {

            this.apClient.airportTable(this.apCriteria)
                .subscribe((resp: ResponseAmsAirportTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    let airports = new Array<AmsAirport>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.airports && resp.data.airports.length > 0) {
                            resp.data.airports.forEach(iu => {
                                const obj = AmsAirport.fromJSON(iu);
                                airports.push(obj);
                            });
                        }
                        this.airports = airports;
                        subscriber.next(this.airports);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);
                    let airports = new Array<AmsAirport>();
                    this.airports = airports;
                    subscriber.next(this.airports);
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }



    //#endregion
}