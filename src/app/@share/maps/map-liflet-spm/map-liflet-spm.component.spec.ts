import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapLifletSpmComponent } from './map-liflet-spm.component';

describe('MapLifletSpmComponent', () => {
  let component: MapLifletSpmComponent;
  let fixture: ComponentFixture<MapLifletSpmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapLifletSpmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapLifletSpmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
