import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { LatLng } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { MapLeafletSpmService } from '../map-liflet-spm.service';

import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
import 'leaflet.geodesic';
//import * as L from 'leaflet-arc';
declare let L;

//L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'ams-map-liflet-spm',
  templateUrl: './map-liflet-spm.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class MapLifletSpmComponent implements OnInit, AfterViewInit, OnDestroy {

  map;
  usrLocation: LatLng;

  airportsChanged: Subscription;
  airportChanged: Subscription;
  flightsChanged: Subscription;
  spmMapPanelOpen: Subscription;

  airports$: Observable<AmsAirport[]>;
  airports: AmsAirport[];

  airport: AmsAirport;
  viewInit: boolean = false;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private mapService: MapLeafletSpmService) {
  }



  ngOnInit(): void {
    //const s = Snap('#map');
    /*
    this.cus.getLocation().subscribe(location => {
      //console.log('spm ngOnInit -> getLocation:', location);
      this.usrLocation = location;
    });
    */
    this.airportsChanged = this.mapService.airportsChanged.subscribe(airports => {
      //console.log('spm airportsChanged -> airports', airports);
      //this.makeMarkers();
    });
    this.flightsChanged = this.mapService.flightsChanged.subscribe(flights => {
      //console.log('map flightsChanged -> flights', flights);
      //his.iniFields();
    });
    this.spmMapPanelOpen = this.mapService.spmMapPanelOpen.subscribe((isOpen:boolean) => {
      //console.log('map spmMapPanelOpen -> isOpen', isOpen);
      if(isOpen){
        //this.iniFields();
        this.makeMarkers();
      } else{
        if (this.map) {this.mapService.clearLayers();};
      }
    });
  }

  ngOnDestroy(): void {
    if (this.airportChanged) { this.airportChanged.unsubscribe(); }
    if (this.flightsChanged) { this.flightsChanged.unsubscribe(); }
    if (this.spmMapPanelOpen) { this.spmMapPanelOpen.unsubscribe(); }
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  private initMap(): void {
    //console.log('spm initMap -> Location:', this.usrLocation);
    const osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    const esri = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 18,
      minZoom: 3,
      attribution: 'Tiles &copy; Esri '
    });

    this.map = L.map('spmmap', {
      center: [this.usrLocation?.lat ?? 39.8282, this.usrLocation?.lng ?? -98.5795],
      zoom: 7,
      layers: [osm, esri]
    });

    const baseMaps = {
      "OpenStreetMap": osm,
      "Esri": esri
    };

    if (this.map) {
      //this.mapService.flights = undefined;
      const layerControl = L.control.layers(baseMaps).addTo(this.map);

      var component = this;
      this.map.on('zoomend', function (event) {
        const zoom = event.target.getZoom();
        //console.log('zoomend -> zoom:', zoom);
        //component.showHideAirportGroups(zoom);
      });
    }
  }

  iniFields(): void {
    this.airports$ = this.apCache.airports;
    this.airports$.subscribe(airports => {
      this.mapService.airports = airports;
      this.airports = this.mapService.airports;
      this.airport = this.cus.hqAirport;
    });
  }

  makeMarkers(): void {
    if (!this.map) {
      this.initMap();
    } else {
      this.mapService.makeMarkers(this.map);
    }

  }

}
