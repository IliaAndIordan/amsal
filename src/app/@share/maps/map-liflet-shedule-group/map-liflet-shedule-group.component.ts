import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { iconDefault, MarkerService } from '../map-liflet-ap-marker.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsHub } from 'src/app/@core/services/api/airline/al-hub';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import {  AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import {  AmsAlFlightNumberSchedule } from 'src/app/@core/services/api/airline/al-flp-number';
import { MapLeafletAlSheduleGroupService } from '../map-liflet-al-shedule-group.service';
import { BaseLayerNameEsri,   IBaseLayerChangeEvent } from '../dto';
import { AmsWeekday } from 'src/app/@core/services/api/airline/enums';
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
import 'leaflet.geodesic';
//import * as L from 'leaflet-arc';
declare let L;

L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'ams-map-liflet-shedule-group',
  templateUrl: './map-liflet-shedule-group.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class MapLifletScheduleGroupComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('grpmap', { static: true }) mapContainer;


  map;
  baseAp: AmsAirport;

  airports$: Observable<AmsAirport[]>;
  airports: AmsAirport[];

  hubs$: Observable<AmsHub[]>;
  hubs: AmsHub[];

  apMarkerArray: Array<L.Marker>;
  apMarkerGroup: L.FeatureGroup;

  flightPathsArray: any[];
  flightPathsGroup: L.FeatureGroup;

  group: AmsSheduleGroup;
  groupChanged: Subscription;
  weekday: AmsWeekday;
  weekdayChanged: Subscription;
  flightNumbersChanged: Subscription;

  flnsFlights$: Observable<AmsAlFlightNumberSchedule[]>;
  flnsFlights: AmsAlFlightNumberSchedule[];

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private hubCache: AmsAirlineHubsCacheService,
    private mapService: MapLeafletAlSheduleGroupService) {
  }



  ngOnInit(): void {
    //const s = Snap('#map');
    this.baseAp = this.cus.hqAirport;
    this.groupChanged = this.mapService.groupChanged.subscribe(group => {
      console.log('d weekdayChanged -> group', group);
      this.initFields();
      this.mapService.refreshGroup(this.map);
    });
    this.weekdayChanged = this.mapService.weekdayChanged.subscribe(weekday => {
      console.log('d weekdayChanged -> weekday', weekday);
      this.initFields();
      this.mapService.refreshGroup(this.map);
    });
    this.flightNumbersChanged = this.mapService.flightNumbersChanged.subscribe(flpns => {
      console.log('d flightNumbersChanged -> flpns', flpns);
      this.initFields();
      if (this.map) {
        this.mapService.makeMapFlightNumbersPaths(this.map);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.groupChanged) { this.groupChanged.unsubscribe(); }
    if (this.weekdayChanged) { this.weekdayChanged.unsubscribe(); }
    if (this.flightNumbersChanged) { this.flightNumbersChanged.unsubscribe(); }
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  private initMap(): void {
    /*
    const el = document?.getElementById('grpmap');
    if (document?.contains(el)) {
      this.map = L.map('grpmap', {
        center: [this.baseAp ? this.baseAp.lat : 39.8282, this.baseAp ? this.baseAp.lon : -98.5795],
        zoom: 7,
        layers: [this.osm, this.esri]
      });

    }
    */

    const osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    const esri = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 18,
      minZoom: 3,
      attribution: 'Tiles &copy; Esri &mdash;'
    });

    const esrigray = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 18,
      minZoom: 3,
      attribution: 'Tiles &copy; Esri',
    });


    this.map = L.map(this.mapContainer.nativeElement, {
      center: [this.baseAp ? this.baseAp.lat : 39.8282, this.baseAp ? this.baseAp.lon : -98.5795],
      zoom: 7,
      layers: [esrigray, esri]
    });


    const baseMaps = {
      //"OpenStreetMap": osm,
      "Esri": esri,
      "Esri Gray": esrigray
    };

    if (this.map) {
      var component = this;
      this.map.on('baselayerchange', function (e: IBaseLayerChangeEvent) {
        component.baselayerchange(e);
      });
      this.mapService.mapFlnShedules = undefined;
      const layerControl = L.control.layers(baseMaps).addTo(this.map);

      this.map.on('zoomend', function (event) {
        const zoom = event.target.getZoom();
        component.showHideAirportGroups(zoom);
      });

    }
    this.initFields();
    this.mapService.refreshAirports(this.map);
  }

  initFields(): void {
    this.group = this.mapService.group;
    this.weekday = this.mapService.weekday;
  }

  baselayerchange(event: IBaseLayerChangeEvent): void {
    
    if (event?.name === BaseLayerNameEsri) {
      //console.log('baselayerchange -> flpnNumber:', this.mapService.mapFlpn);
      if (this.mapService.mapFlpn && this.mapService.mapFlpn.length > 0) {
        this.mapService.mapFlpn.forEach(flpn => {
          //console.log('baselayerchange -> flpn:', flpn)
          flpn.flPath.setStyle({
            color: 'white'
          });
        });
      }
    } else {
      if (this.mapService.mapFlpn && this.mapService.mapFlpn.length > 0) {
        this.mapService.mapFlpn.forEach(flpn => {
          //console.log('baselayerchange -> flpn:', flpn)
          flpn.flPath.setStyle({
            color: 'black'
          });
        });
      }
    }
    
  }
  showHideAirportGroups(zoom: number): void {
    //console.log('zoomend -> map', this.map);
    this.mapService.showHideAirportGroups(this.map);
  }



  refresh(): void {
    this.initFields();
    if (!this.map) {
      this.initMap();
    }
    this.map.invalidateSize();
    this.mapService.refreshAirports(this.map);
  }

  refreshGroup(): void {
    this.initFields();
    if (!this.map) {
      this.initMap();
    }

    this.map.invalidateSize();
    this.mapService.refreshGroup(this.map);
    this.map.invalidateSize();
  }


}
