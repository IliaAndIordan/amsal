import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { iconDefault } from '../map-liflet-ap-marker.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsHub } from 'src/app/@core/services/api/airline/al-hub';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { MapLeafletApRoutesService } from '../map-liflet-ap-routes.service';
import { AmsAlSheduleRoute } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsAlFlightNumber, AmsAlFlightNumberSchedule } from 'src/app/@core/services/api/airline/al-flp-number';
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
import 'leaflet.geodesic';
//import * as L from 'leaflet-arc';

declare let L;


L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'ams-map-liflet-ap-routes',
  templateUrl: './map-liflet-ap-routes.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class MapLifletApRoutesComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  map;
  baseAp: AmsAirport;

  airports$: Observable<AmsAirport[]>;
  airports: AmsAirport[];

  hubs$: Observable<AmsHub[]>;
  hubs: AmsHub[];

  routes$: Observable<AmsAlSheduleRoute[]>;
  routes: AmsAlSheduleRoute[];

  flpns$: Observable<AmsAlFlightNumber[]>;
  flpns: AmsAlFlightNumber[];

  schedules$: Observable<AmsAlFlightNumberSchedule[]>;
  schedules: AmsAlFlightNumberSchedule[];

  airportsChanged: Subscription;
  airportChanged: Subscription;
  flightsChanged: Subscription;
  flightNumbersChanged:Subscription;

  viewInit: boolean = false;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private hubCache: AmsAirlineHubsCacheService,
    private mapService: MapLeafletApRoutesService) {
  }



  ngOnInit(): void {
    this.baseAp = this.cus.hqAirport;

    this.airportsChanged = this.mapService.airportsChanged.subscribe(airports => {
      //console.log('map-ap_route airportsChanged -> airports', airports);
      //this.makeMarkers();
    });
    this.flightsChanged = this.mapService.flightsChanged.subscribe(flights => {
      //console.log('d flightsChanged -> flights', flights);
      //this.makeMarkers();
    });
   

    this.refresh();
  }

  ngOnDestroy(): void {
    if (this.airportChanged) { this.airportChanged.unsubscribe(); }
    if (this.flightsChanged) { this.flightsChanged.unsubscribe(); }
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    console.log('map-ap_route ngOnChanges -> changes', changes);
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  private initMap(): void {

    //console.log('spm initMap -> Location:', this.usrLocation);
    const osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    const esri = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 18,
      minZoom: 3,
      attribution: 'Tiles &copy; Esri &mdash;'
    });
    const el = document?.getElementById('apRouteMap');
    if (document.contains(el)) {
      this.map = L.map('apRouteMap', {
        center: [this.baseAp ? this.baseAp.lat : 39.8282, this.baseAp ? this.baseAp.lon : -98.5795],
        zoom: 7,
        layers: [osm, esri]
      });

    }

    const baseMaps = {
      "OpenStreetMap": osm,
      "Esri": esri
    };


    //console.log('initMap -> map', this.map);
    if (this.map) {
      this.mapService.mapFlights = undefined;
      const layerControl = L.control.layers(baseMaps).addTo(this.map);

      var component = this;
      this.map.on('zoomend', function (event) {
        const zoom = event.target.getZoom();
        component.showHideAirportGroups(zoom);
      });

    }
  }

  showHideAirportGroups(zoom: number): void {
    //console.log('zoomend -> map', this.map);
    this.mapService.showHideAirportGroups(this.map);
  }

  makeMarkers(): void {
    if (this.map) {
      this.mapService.makeMarkers(this.map);
    }

  }

  initAirportsMarkers() {
    if (this.map && this.airports) {
      this.mapService.airports = this.airports;
      this.mapService.hubs = this.hubs;
    }
  }

  refresh(): void {
    if (!this.map) {
      this.initMap();
    }
    this.airports$ = this.apCache.airports;

    //this.routes$ = this.mapService.loadRouteData();

    this.airports$.subscribe(airports => {
      this.airports = airports;
      this.hubs$ = this.hubCache.hubs;
      this.hubs$.subscribe(hubs => {
        this.hubs = hubs;
        this.flpns$ = this.mapService.loadFlightNumbersData();
        this.flpns$.subscribe(flights => {
          this.mapService.airports = this.airports;
          this.mapService.hubs = this.hubs;
          //this.mapService.routes = routes;

          if (!this.map) {
            this.initMap();
            if (this.map) { this.mapService.makeMarkers(this.map); }
          } else {
            if (this.map) { this.mapService.makeMarkers(this.map); }
          }

        })

      });
    });
  }

}
