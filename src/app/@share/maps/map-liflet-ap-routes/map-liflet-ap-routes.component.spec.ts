import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapLifletApRoutesComponent } from './map-liflet-ap-routes.component';

describe('MAPS -> MapLifletApRoutesComponent', () => {
  let component: MapLifletApRoutesComponent;
  let fixture: ComponentFixture<MapLifletApRoutesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapLifletApRoutesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapLifletApRoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
