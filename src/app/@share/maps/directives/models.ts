export class JVectorMapColor{
    /** the marker points */
    markerColor: string;
    /**the background */
    bgColor: string;
    /** the color of the region in the serie */
    scaleColors: string[];
    /** the base region color */
    regionFill: string;
  }