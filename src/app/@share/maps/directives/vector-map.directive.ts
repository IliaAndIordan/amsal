import { OnInit, Directive, Input, ElementRef, OnDestroy } from '@angular/core';
declare var $: any;
//import * as $ from 'jquery';
//declare var jQuery:any;
@Directive({
 selector: '[vectorMap]'
})
export class AmsVectormapDirective implements OnInit, OnDestroy {
@Input() mapHeight: number;
@Input() mapName: any;
@Input() mapOptions: any;
@Input() seriesData: any;
@Input() markersData: any;
$element: any;
constructor(public element: ElementRef) { }

ngOnInit() {

    this.$element = $(this.element.nativeElement);
    console.log('vectorMap ngOnInit -> element:', this.$element);
    this.$element.css('height', this.mapHeight);
    console.log('vectorMap ngOnInit -> length:', this.$element.length);
    console.log('vectorMap ngOnInit -> vectorMap:', this.$element.vectorMap)
    //|| !this.$element.vectorMap
    if (!this.$element.length ) {
        return;
    }

    this.$element.vectorMap({
        map: 'world-merc-en',
        backgroundColor: this.mapOptions.bgColor,
        zoomMin: 1,
        zoomMax: 8,
        zoomOnScroll: false,
        regionStyle: {
            initial: {
                'fill': this.mapOptions.regionFill,
                'fill-opacity': 1,
                'stroke': 'none',
                'stroke-width': 1.5,
                'stroke-opacity': 1
            },
            hover: {
                'fill-opacity': 0.8
            },
            selected: {
                fill: 'blue'
            },
            selectedHover: {
            }
        },
        focusOn: { x: 0.4, y: 0.6, scale: this.mapOptions.scale },
        markerStyle: {
            initial: {
                fill: this.mapOptions.markerColor,
                stroke: this.mapOptions.markerColor
            }
        },
        onRegionLabelShow: (e, el, code) => {
            if (this.seriesData && this.seriesData[code]) {
                el.html(el.html() + ': ' + this.seriesData[code] + ' visitors');
            }
        },
        markers: this.markersData,
        series: {
            regions: [{
                values: this.seriesData,
                scale: this.mapOptions.scaleColors,
                normalizeFunction: 'polynomial'
            }]
        },
    });
}

ngOnDestroy() {
    this.$element.vectorMap('get', 'mapObject').remove();
}}