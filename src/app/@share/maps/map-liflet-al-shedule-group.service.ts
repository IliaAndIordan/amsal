import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { AmsAirport, IAmsAirport } from "src/app/@core/services/api/airport/dto";
import { AppStore, URL_COMMON_IMAGE_AIRCRAFT_ICON, URL_COMMON_IMAGE_AMS_COMMON } from "src/app/@core/const/app-storage.const";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { AmsAirportClient } from "src/app/@core/services/api/airport/api-client";
import { AmsWadCountryService } from "src/app/wad/country/country.service";
import { MapLeafletPopupAirportService } from "./map-liflet-popup-airport.service";
import { AmsAirportCacheService } from "src/app/@core/services/api/airport/ams-airport-cache.service";
import { GeometryUtil, LayerGroup } from "leaflet";
import { AmsMapAlFlightNumber, AmsMapAlFlightNumberSchedule, AmsMapFlight, AmsMapMarkerOptions, AmsMapRoute } from "./dto";
import { ApType } from "src/app/@core/models/pipes/ap-type.pipe";
import { AmsAirlineHubsCacheService } from "src/app/@core/services/api/airline/al-hub-cache.service";
import { AmsHub, IAmsHub } from "src/app/@core/services/api/airline/al-hub";
import { AmsHubTypes, AmsWeekday } from "src/app/@core/services/api/airline/enums";
import { AmsAirlineClient } from "src/app/@core/services/api/airline/api-client";
import { SpinnerService } from "src/app/@core/services/spinner.service";
import { Router } from "@angular/router";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import { AmsSheduleGroup, AmsSheduleGroupTableCriteria, AmsSheduleRouteTableCriteria, IAmsAlSheduleRoute, IAmsSheduleGroup, ResponceAmsSheduleGroupTableData, ResponceAmsSheduleRouteTableData } from "src/app/@core/services/api/airline/al-flp-shedule";
import { AmsAlFlightNumber, AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleTableCriteria, AmsAlFlightNumberTableCriteria, IAmsAlFlightNumber, ResponceAmsAlFlightNumberScheduleTableData, ResponceAmsAlFlightNumberTableData } from "src/app/@core/services/api/airline/al-flp-number";
import { ToastrService } from "ngx-toastr";
import { MatDialog } from "@angular/material/dialog";
import { AmsAirlineAircraftCacheService } from "src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service";
import { AmsAlFlightNumberEditDialog } from "../components/dialogs/al-flp-number-edit/al-flp-number-edit.dialog";
import { AmsAlFlightNumberPriceEditDialog } from "../components/dialogs/al-flp-number-edit/al-flp-number-price-edit.dialog";
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
import 'leaflet.geodesic';
//import * as L from 'leaflet-arc';

declare let L;


@Injectable({
    providedIn: 'root'
})
export class MapLeafletAlSheduleGroupService {

    iconsAcCommonUrl = URL_COMMON_IMAGE_AIRCRAFT_ICON;




    flnLabelsArray: Array<any>;

    spmMapPanelOpen = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient,
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private apClient: AmsAirportClient,
        private cService: AmsWadCountryService,
        private apCache: AmsAirportCacheService,
        private acCache: AmsAirlineAircraftCacheService,
        private hubCache: AmsAirlineHubsCacheService,
        private alClient: AmsAirlineClient,
        private popupApService: MapLeafletPopupAirportService) {
    }

    //#region AmsSheduleGroup

    groupChanged = new Subject<AmsSheduleGroup>();

    get group(): AmsSheduleGroup {
        const objStr = localStorage.getItem(AppStore.mapAlGroup);
        let obj: AmsSheduleGroup;
        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsSheduleGroup;
            if (objArr && objArr.grpId > 0) {
                obj = AmsSheduleGroup.fromJSON(objArr);
            }
        }
        return obj;
    }

    set group(value: AmsSheduleGroup) {
        let oldVal = this.group;
        if (value && value.grpId > 0) {
            localStorage.setItem(AppStore.mapAlGroup, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAlGroup);
        }

        this.groupChanged.next(value);
    }

    //#endregion

    //#region AmsWeekday

    weekdayChanged = new Subject<AmsWeekday>();

    get weekday(): AmsWeekday {
        const objStr = localStorage.getItem(AppStore.mapAlWeekday);
        let obj: AmsWeekday;
        if (objStr) {
            obj = Object.assign(new AmsWeekday(), JSON.parse(objStr));
        }
        return obj;
    }

    set weekday(value: AmsWeekday) {
        let oldVal = this.weekday;
        if (value && value.id > 0) {
            localStorage.setItem(AppStore.mapAlWeekday, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAlWeekday);
        }
        this.weekdayChanged.next(value);
    }

    //#endregion


    //#region Flight Number Schedule

    //#region Data

    flnShedulesChanged = new Subject<AmsAlFlightNumberSchedule[]>();
    flnShedules: AmsAlFlightNumberSchedule[];
    flnShedulesCount: number;

    get flnShedulesCriteria(): AmsAlFlightNumberScheduleTableCriteria {
        let obj = new AmsAlFlightNumberScheduleTableCriteria();
        obj.limit = 1000;
        obj.offset = 0;
        obj.sortCol = undefined;
        obj.sortDesc = false;
        obj.alId = this.cus.airline.alId;
        obj.grpId = this.group?.grpId;
        obj.wdId = this.weekday?.id;

        return obj;
    }

    loadFlightNumberScheduleData(): Observable<Array<AmsAlFlightNumberSchedule>> {
        this.flnShedules = undefined;
        this.flnShedulesCount = 0;

        return new Observable<Array<AmsAlFlightNumberSchedule>>(subscriber => {
            if (!this.group || !this.weekday) {
                this.flnShedulesCount = 0;
                this.flnShedules = new Array<AmsAlFlightNumberSchedule>();
                subscriber.next(this.flnShedules);
            } else {
                this.alClient.alFlightNumberScheduleTable(this.flnShedulesCriteria)
                    .subscribe((resp: ResponceAmsAlFlightNumberScheduleTableData) => {
                        let data = new Array<AmsAlFlightNumberSchedule>();
                        if (resp) {
                            if (resp && resp.flnschedules && resp.flnschedules.length > 0) {
                                data = resp.flnschedules;
                            }
                            this.flnShedulesCount = resp.rowsCount ? resp.rowsCount : 0;
                        } else {
                            this.flnShedulesCount = data ? data.length : 0;
                        }
                        this.flnShedules = data;
                        subscriber.next(this.flnShedules);

                    }, msg => {
                        console.log('loadRouteData -> ' + msg);
                        this.flnShedulesCount = 0;
                        this.flnShedules = new Array<AmsAlFlightNumberSchedule>();
                    });
            }


        });

    }

    //#endregion

    mapFlnShedules: AmsMapAlFlightNumberSchedule[];
    flnShedulesPathsArray: any[];
    flnShedulesPathsGroup: L.FeatureGroup;

    makeMapFlnShedules(map: L.Map): void {
        if (map) {
            if (!this.mapFlnShedules) { this.mapFlnShedules = new Array<AmsMapAlFlightNumberSchedule>(); }

            const toRemove = this.mapFlnShedulesToRemove();
            if (toRemove && toRemove.length > 0) {
                this.removeMapFlnShedules(map, toRemove);
            }
            const toUpdate = this.mapFlnShedulesToUpdate();
            if (toUpdate && toUpdate.length > 0) {
                toUpdate.forEach((mapFlight: AmsMapAlFlightNumberSchedule) => {
                    const flight = this.flnShedules.find(x => x.flpnsId === mapFlight.flpnsId);
                    mapFlight.flight = flight;
                });
            }

            const flights = this.flnShedulesToAdd();
            if (flights && flights.length > 0) {

                for (const flight of flights) {
                    let mapFlight = new AmsMapAlFlightNumberSchedule();
                    mapFlight.flight = flight;
                    mapFlight.group = this.group;
                    mapFlight.weekday = this.weekday;

                    mapFlight.depAp = this.airports?.find(x => x.apId === flight.depApId)??undefined;
                    mapFlight.arrAp = this.airports?.find(x => x.apId === flight.arrApId)??undefined;
                    mapFlight.makeFlightPaths(map, this.popupApService);
                    if (mapFlight.flPath) {
                        mapFlight.flPath.addTo(map);
                    }

                    /*
                    mapFlight.acMarker.bindPopup(this.popupApService.makeACPopup(flight));
                    mapFlight.acMarker.addTo(map).on("popupopen", (a) => {
                        var popUp = a.target.getPopup()
                        popUp.getElement()
                            .querySelector("#acImage")
                            .addEventListener("click", e => {
                                this.spmAircradftOpenClick(flight.acId);
                            })
                    });
                    */
                    //.on('click', function() { alert('Clicked on a group!'); }).addTo(map);
                    //mapFlight.featureGroup.addTo(map);
                    this.mapFlnShedules.push(mapFlight);
                    //mapFlight.acMarker.motionResume();
                }
            }
        }
    }

    mapFlnShedulesToRemove(): AmsMapAlFlightNumberSchedule[] {
        let rv = new Array<AmsMapAlFlightNumberSchedule>();
        const filteredIds = this.flnShedules && this.flnShedules.length > 0 ? this.flnShedules.map(({ flpnsId }) => flpnsId) : [-1];
        if (this.mapFlnShedules && this.mapFlnShedules.length > 0) {
            rv = this.mapFlnShedules.filter(function (mf: AmsMapAlFlightNumberSchedule) {
                return filteredIds.indexOf(mf.flight?.flpnsId) === -1;
            });
        }
        //console.log('mapFlightsToRemove -> rv:', rv);
        return rv;
    }

    mapFlnShedulesToUpdate(): AmsMapAlFlightNumberSchedule[] {
        let rv = new Array<AmsMapAlFlightNumberSchedule>();
        const filteredIds = this.flnShedules && this.flnShedules.length > 0 ? this.flnShedules.map(({ flpnsId }) => flpnsId) : [-1];
        if (this.mapFlnShedules && this.mapFlnShedules.length > 0) {
            rv = this.mapFlnShedules.filter(function (mf: AmsMapAlFlightNumberSchedule) {
                return filteredIds.indexOf(mf.flight?.flpnsId) !== -1;
            });
        }
        //console.log('mapFlightsToUpdate -> rv:', rv);
        return rv;
    }

    flnShedulesToAdd(): AmsAlFlightNumberSchedule[] {
        let rv = new Array<AmsAlFlightNumberSchedule>();
        const filteredIds = this.mapFlnShedules && this.mapFlnShedules.length > 0 ? this.mapFlnShedules.map(({ flpnsId }) => flpnsId) : [-1];
        if (this.flnShedules && this.flnShedules.length > 0) {
            rv = this.flnShedules.filter(function (fl: AmsAlFlightNumberSchedule) {
                return filteredIds.indexOf(fl.flpnsId) === -1;
            });
        }
        //console.log('flightsToAdd -> rv:', rv);
        return rv;
    }

    removeMapFlnShedules(map: L.Map, toRemove: AmsMapAlFlightNumberSchedule[]): void {
        if (toRemove && toRemove.length > 0) {
            toRemove.forEach((mapFlight: AmsMapAlFlightNumberSchedule) => {
                const idx = this.mapFlnShedules.findIndex(x => x.flight.flpnsId === mapFlight.flight?.flpnsId);
                //console.log('makeMapFlights -> toRemove idx:', idx);
                let hasLayer = map.hasLayer(mapFlight.acMarker);
                if (hasLayer) {
                    map.removeLayer(mapFlight.acMarker);
                }

                if (mapFlight.featureGroup && mapFlight.featureGroup.getLayers() &&
                    mapFlight.featureGroup.getLayers().length > 0) {
                    mapFlight.featureGroup.clearLayers();
                }
                hasLayer = mapFlight?.flPath ? map.hasLayer(mapFlight?.flPath) : false;
                if (hasLayer) {
                    map.removeLayer(mapFlight?.flPath);
                }
                this.mapFlnShedules?.splice(idx, 1);
            });
        }
    }

    addFlnShedulesPaths(map: L.Map): void {
        if (map) {
            const flights = this.flnShedules
            this.flnShedulesPathsArray = new Array<L.Circle>();

            if (flights && flights.length > 0) {

                for (const flight of flights) {
                    const depAp = this.airports?.find(x => x.apId === flight.depApId);
                    const arrAp = this.airports?.find(x => x.apId === flight.arrApId);
                    let color = 'blue';
                    if (flight.flpnsId && this.group?.isActive) {
                        color = 'yellow';
                    }
                    const latlngDep = L.latLng(depAp.lat, depAp.lon);
                    const latlngArr = L.latLng(arrAp.lat, arrAp.lon);
                    const latlngs = [latlngDep, latlngArr];
                    const angle = GeometryUtil.angle(map, latlngDep, latlngArr);
                    const bearing = GeometryUtil.bearing(latlngDep, latlngArr);
                    const opt = { color: color, vertices: 10, offset: 0, weight: 2, opacity: 0.6, _flId: flight.flpnsId };
                    const path = L.Polyline.Arc(latlngDep, latlngArr, opt);

                    let flIdx = -1;

                    this.flnShedulesPathsArray.push(path);
                }

                /*
                this.airport = airports[0];
                if(this.airport){
                    this.centerMap(map, new L.LatLng(this.airport.lat, this.airport.lon), this.airport.mapZoom)
                }*/
            }

            this.flnShedulesPathsGroup = L.featureGroup(this.flnShedulesPathsArray);
            //.on('click', function() { alert('Clicked on a group!'); }).addTo(map);
            this.flnShedulesPathsGroup.addTo(map);
            /*
            if (this.acMarkerArray && this.acMarkerArray.length > 0) {
                this.acMarkerArray.forEach(marker => {
                    //map.addLayer(marker);
                    marker.addTo(map);
                });
            }
            */
        }
    }

    clearMapFlnShedules(map: L.Map): void {
        if (this.flnShedulesPathsGroup && this.flnShedulesPathsGroup.getLayers() &&
            this.flnShedulesPathsGroup.getLayers().length > 0) {
            this.flnShedulesPathsGroup.clearLayers();
            this.flnShedulesPathsArray = new Array<L.Circle>();
        }
    }

    //#endregion

    //#region Flight Numbers

    //#region Data

    flightNumbersChanged = new Subject<AmsAlFlightNumber[]>();
    flightNumbersCount: number;

    get flightNumbers(): AmsAlFlightNumber[] {
        const objStr = localStorage.getItem(AppStore.mapFlpnsSpm);
        let obj: AmsAlFlightNumber[] = new Array<AmsAlFlightNumber>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsAlFlightNumber[];
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsAlFlightNumber) => {
                    const ap = AmsAlFlightNumber.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
    }

    set flightNumbers(value: AmsAlFlightNumber[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapFlpnsSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapFlpnsSpm);
        }

        this.flightNumbersChanged.next(value);
    }

    get alFlightNumberCriteria(): AmsAlFlightNumberTableCriteria {

        let obj = new AmsAlFlightNumberTableCriteria();
        obj.limit = 1200;
        obj.offset = 0;
        obj.sortCol = 'flpnNumber';
        obj.sortDesc = false;
        obj.alId = this.cus.airline.alId;

        return obj;
    }

    loadFlightNumbersData(): Observable<Array<AmsAlFlightNumber>> {
        this.flightNumbersCount = 0;
        this.flightNumbers = undefined;

        return new Observable<Array<AmsAlFlightNumber>>(subscriber => {

            this.alClient.alFlightNumberTableP(this.alFlightNumberCriteria)
                .then((resp: ResponceAmsAlFlightNumberTableData) => {

                    if (resp) {
                        this.flightNumbers = resp.flnrs;
                        this.flightNumbersCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else {
                        this.flightNumbers = new Array<AmsAlFlightNumber>()
                        this.flightNumbersCount = 0;
                    }

                    subscriber.next(this.flightNumbers);

                }, msg => {
                    console.log('loadFlightNumbersData -> ' + msg);

                    this.flightNumbersCount = 0;
                    this.flightNumbers = new Array<AmsAlFlightNumber>();
                });

        });

    }

    apflightNumberClick(arrAp: AmsAirport): void {
        //console.log('addAlFlightNumberClick -> arrAp:', arrAp);
        if (arrAp && arrAp.apId && arrAp.apId !== this.group?.apId) {


            let flpn: AmsAlFlightNumber;
            let flpnIdx = -1;
            if (this.flightNumbers && this.flightNumbers.length > 0) {
                flpnIdx = this.flightNumbers.findIndex(x => x.depApId === this.group?.apId &&
                    x.arrApId === arrAp.apId);
            }
            //console.log('addAlFlightNumberClick -> flpnIdx:', flpnIdx);
            if (flpnIdx === -1) {
                flpn = new AmsAlFlightNumber();
                flpn.alId = this.cus.airline.alId;
                const maxNum = Math.max.apply(null,
                    this.flightNumbers.map(function (o) { return o.flpnNumber; }));
                flpn.flpnNumber = (maxNum + 1) ?? 100;
                flpn.depApId = this.group?.apId??this.cus.airline.apId;
                flpn.arrApId = arrAp.apId;
            } else {
                flpn = this.flightNumbers[flpnIdx];
            }

            //console.log('addAlFlightNumberClick -> flpn:', flpn);
            if (flpn) {
                this.addlFlightNumber(flpn);
            }


        }
    }

    addlFlightNumber(value: AmsAlFlightNumber): void {
        console.log('addlFlightNumber -> value:', value);

        if (value && value.alId === this.cus.airline.alId) {
            let flpnReverse:AmsAlFlightNumber;
            if (value.flpnId >0 && this.flightNumbers && this.flightNumbers.length > 0) {
                flpnReverse = this.flightNumbers.find(x => x.arrApId === value?.depApId &&
                    x.depApId === value.arrApId);
            }
            const ac$ = this.acCache.getAircraft(this.group?.acId);
            ac$.subscribe(ac => {
                const dialogRef = this.dialogService.open(AmsAlFlightNumberEditDialog, {
                    autoFocus: false,
                    width: '921px',
                    //height: '600px',
                    data: {
                        flpn: value,
                        aircraft: ac,
                        flpnReverse:flpnReverse,
                        createReverse:true,
                    }
                });

                dialogRef.afterClosed().subscribe(res => {
                    if (res) {
                        const flpn$ = this.loadFlightNumbersData();
                        flpn$.subscribe(flights => {
                            console.log('addlFlightNumber -> flights:', flights);
                        });
                    }
                });
            });



            //this.cus.showMessage({title:'Edit Flight Number', message:'Functionality under development.'})
        }
        else {
            this.toastr.info('Please select airport first.', 'Select City');
        }
    }

    editAlFlightNumber(value: AmsAlFlightNumber): void {
        console.log('editAlFlightNumberClick -> value:', value);

        if (value && value.alId === this.cus.airline.alId) {

            const ac$ = this.acCache.getAircraft(this.group?.acId);
            ac$.subscribe(ac => {
                const dialogRef = this.dialogService.open(AmsAlFlightNumberPriceEditDialog, {
                    width: '921px',
                    //height: '600px',
                    data: {
                        flpn: value,
                        aircraft: ac
                    }
                });

                dialogRef.afterClosed().subscribe(res => {
                    if (res) {
                        this.loadFlightNumbersData();
                    }
                });
            });


            //this.cus.showMessage({title:'Edit Flight Number', message:'Functionality under development.'})
        }
        else {
            this.toastr.info('Please select airport first.', 'Select City');
        }
    }

    //#endregion

    mapFlpn: AmsMapAlFlightNumber[];
    flpnPathsArray: any[];
    flpnPathsGroup: L.FeatureGroup;

    makeMapFlightNumbersPaths(map: L.Map): void {
        console.log('makeMapFlightNumbersPaths ->');
        if (map) {
            if (!this.mapFlpn) { this.mapFlpn = new Array<AmsMapAlFlightNumber>(); }
            if (!this.flnLabelsArray) { this.flnLabelsArray = new Array<L.Marker>(); }
            const toRemove = this.mapFlightNumbersToRemove();
            console.log('makeMapFlightNumbersPaths -> toRemove:', toRemove);
            if (toRemove && toRemove.length > 0) {
                this.removeMapFlightNumbers(map, toRemove);
            }
            const toUpdate = this.mapFlightNumbersToUpdate();
            console.log('makeMapFlightNumbersPaths -> toUpdate:', toUpdate);
            if (toUpdate && toUpdate.length > 0) {
                toUpdate.forEach((mapFln: AmsMapAlFlightNumber) => {
                    const flight = this.flightNumbers.find(x => x.flpnId === mapFln.flpnId);
                    mapFln.fln = flight;
                });
            }

            const flights = this.mapFlightNumbersToAdd();
            console.log('makeMapFlightNumbersPaths -> mapFlightNumbersToAdd:', flights);
            if (flights && flights.length > 0) {

                for (const flight of flights) {
                    let mapFln = new AmsMapAlFlightNumber();
                    mapFln.fln = flight;

                    mapFln.depAp = this.airports?.find(x => x.apId === flight.depApId);
                    mapFln.arrAp = this.airports?.find(x => x.apId === flight.arrApId);
                    mapFln.makeFlightNumberPaths(map, this.popupApService);
                    if (mapFln.flnLabelMarker && mapFln?.flpnId && this.flnLabelsArray && this.flnLabelsArray.length > 0) {
                        const flIdx = this.flnLabelsArray.findIndex(x => x.options._flId === mapFln.flpnId);
                        if (flIdx === -1) { this.flnLabelsArray.push(mapFln.flnLabelMarker) }
                    }
                    if (mapFln.flPath) { mapFln.flPath.addTo(map) }
                    this.mapFlpn.push(mapFln);
                }
            }
        }
    }

    mapFlightNumbersToRemove(): AmsMapAlFlightNumber[] {
        let rv = new Array<AmsMapAlFlightNumber>();
        const filteredIds = this.flightNumbers.map(({ flpnId }) => flpnId);
        if (this.flightNumbers && this.mapFlpn && this.mapFlpn.length > 0) {
            rv = this.mapFlpn.filter(function (mf: AmsMapAlFlightNumber) {
                return filteredIds.indexOf(mf.flpnId) === -1;
            });
        }
        //console.log('mapFlightsToRemove -> rv:', rv);
        return rv;
    }

    mapFlightNumbersToUpdate(): AmsMapAlFlightNumber[] {
        let rv = new Array<AmsMapAlFlightNumber>();
        const filteredIds = this.flightNumbers.map(({ flpnId }) => flpnId);
        if (this.flightNumbers && this.mapFlpn && this.mapFlpn.length > 0) {
            rv = this.mapFlpn.filter(function (mf: AmsMapAlFlightNumber) {
                return filteredIds.indexOf(mf.flpnId) !== -1;
            });
        }
        //console.log('mapFlightsToUpdate -> rv:', rv);
        return rv;
    }

    mapFlightNumbersToAdd(): AmsAlFlightNumber[] {
        let rv = this.flightNumbers;
        if (this.flightNumbers && this.mapFlpn && this.mapFlpn.length > 0) {
            const filteredIds = this.mapFlpn.map(({ flpnId }) => flpnId);
            rv = this.flightNumbers.filter(function (fl: AmsAlFlightNumber) {
                return filteredIds.indexOf(fl.flpnId) === -1;
            });
        }
        //console.log('flightsToAdd -> rv:', rv);
        return rv;
    }

    removeMapFlightNumbers(map: L.Map, toRemove: AmsMapAlFlightNumber[]): void {
        if (toRemove && toRemove.length > 0) {
            toRemove.forEach((mapFlpn: AmsMapAlFlightNumber) => {
                const idx = this.mapFlpn.findIndex(x => x.flpnId === mapFlpn.flpnId);

                if (mapFlpn.featureGroup && mapFlpn.featureGroup.getLayers() &&
                    mapFlpn.featureGroup.getLayers().length > 0) {
                    mapFlpn.featureGroup.clearLayers();
                }
                const hasLayer = mapFlpn?.flPath ? map.hasLayer(mapFlpn.flPath) : false;
                if (hasLayer) {
                    map.removeLayer(mapFlpn.flPath);
                }
                this.mapFlnShedules?.splice(idx, 1);
            });
        }
    }

    clearMapFlightNumbers(map: L.Map): void {
        if (this.flpnPathsGroup && this.flpnPathsGroup.getLayers() &&
            this.flpnPathsGroup.getLayers().length > 0) {
            this.flpnPathsGroup.clearLayers();
            this.flpnPathsArray = new Array<L.Circle>();
        }
        if (this.flnLabelsArray && this.flnLabelsArray.length > 0) {
            this.flnLabelsArray.forEach((acMerker: any) => {
                const hasLayer = map.hasLayer(acMerker);
                if (hasLayer) {
                    map.removeLayer(acMerker);
                }
            });
        }
        this.flnLabelsArray = new Array<any>();
    }

    //#endregion

    //#region Airports

    apMarkerArray: Array<L.Marker>;
    hubsMarkerArray: Array<L.Marker>;
    apMarkerGroup: L.FeatureGroup;

    apAirfieldGroup: L.FeatureGroup;
    apLocaldGroup: L.FeatureGroup;
    apRegionaldGroup: L.FeatureGroup;
    apIntdGroup: L.FeatureGroup;
    apLargeGroup: L.FeatureGroup;
    hubsGroup: L.FeatureGroup;

    airportsChanged = new Subject<AmsAirport[]>();
    airportChanged = new Subject<AmsAirport>();

    get apAirfieldMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId < ApType.Local));
        }
        return rv;
    };

    get apLocalMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId > ApType.Airfield && (x.options as AmsMapMarkerOptions).airport.typeId < ApType.Regional));
        }
        return rv;
    };

    get apRegionalMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId > ApType.Local && (x.options as AmsMapMarkerOptions).airport.typeId < ApType.SmallInt));
        }
        return rv;
    };

    get apLargeMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId > ApType.Int));
        }
        return rv;
    };

    get apIntMarkers(): Array<L.Marker> {
        let rv: Array<L.Marker>;
        if (this.apMarkerArray && this.apMarkerArray.length > 0) {
            rv = this.apMarkerArray.filter(x => (x.options && (x.options as AmsMapMarkerOptions).airport.typeId > ApType.Regional && (x.options as AmsMapMarkerOptions).airport.typeId < ApType.LargeInt));
        }
        return rv;
    };

    get hubs(): AmsHub[] {
        const objStr = localStorage.getItem(AppStore.mapHubsSpm);
        let obj: AmsHub[] = new Array<AmsHub>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsHub[];//Object.assign(new AmsAirport(), );
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsHub) => {
                    const ap = AmsHub.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
    }

    set hubs(value: AmsHub[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapHubsSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapHubsSpm);
        }
    }


    get airports(): AmsAirport[] {
        const objStr = localStorage.getItem(AppStore.mapAirportsSpm);
        let obj: AmsAirport[] = new Array<AmsAirport>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsAirport[];//Object.assign(new AmsAirport(), );
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsAirport) => {
                    const ap = AmsAirport.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
    }

    set airports(value: AmsAirport[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapAirportsSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirportsSpm);
        }

        this.airportsChanged.next(value);
    }

    setAirportsNoEvent(value: AmsAirport[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapAirportsSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirportsSpm);
        }
    }

    get airport(): AmsAirport {
        const onjStr = localStorage.getItem(AppStore.mapAirportSpm);
        let obj: AmsAirport;
        if (onjStr) {
            obj = Object.assign(new AmsAirport(), JSON.parse(onjStr));
        }
        return obj;
    }

    set airport(value: AmsAirport) {
        const oldValue = this.airport;
        if (value) {
            localStorage.setItem(AppStore.mapAirportSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirportSpm);
        }

        this.airportChanged.next(value);
    }

    initAirportHubs(): void {
        let aps = this.airports;
        const hubs = this.hubs;
        let updateAps = false;
        if (aps && aps.length > 0) {
            let hub: AmsHub;
            aps.forEach((ap: AmsAirport) => {
                if (hubs && hubs.length > 0) {
                    hub = hubs.find(x => x.apId === ap.apId);
                }
                if (hub) {
                    updateAps = true;
                    //console.log('set hubs -> hub:', hub);
                    ap.hub = hub
                } else {
                    ap.hub = undefined;
                };
            });
        }
        if (updateAps) {
            this.airports = aps;
        }
    }


    makeAirportMarkers(map: L.Map): void {
        this.initAirportHubs();
        if (map) {
            const airports = this.airports
            this.apMarkerArray = new Array<L.Marker>();
            this.hubsMarkerArray = new Array<L.Marker>();
            if (airports && airports.length > 0) {

                for (const ap of airports) {
                    //ap.routes = this.flights ? this.routes.filter(x => x.arrApId === ap.apId || x.depApId === ap.apId) : undefined;
                    if (ap.lat && ap.lon) {
                        const marker = this.getApMatrker(ap);
                        if (marker) {
                            this.apMarkerArray.push(marker);
                        }

                    }
                    if (ap.hub || ap.apId === this.group?.apId) {
                        const marker = this.getHubMatrker(ap);
                        if (marker) {
                            this.hubsMarkerArray.push(marker);
                        }
                    }
                }
                /*
                this.airport = airports[0];
                if(this.airport){
                    this.centerMap(map, new L.LatLng(this.airport.lat, this.airport.lon), this.airport.mapZoom)
                }*/
            }
            const zoom = map.getZoom();
            const apAirfields = this.apAirfieldMarkers;
            if (apAirfields && apAirfields.length > 0) {
                this.apAirfieldGroup = L.featureGroup(apAirfields);
                //this.apAirfieldGroup.addTo(map);
            }
            const apLocal = this.apLocalMarkers;
            if (apLocal && apLocal.length > 0) {
                this.apLocaldGroup = L.featureGroup(apLocal);
                //this.apLocaldGroup.addTo(map);
            }
            const apReg = this.apRegionalMarkers;
            if (apReg && apReg.length > 0) {
                this.apRegionaldGroup = L.featureGroup(apReg);
                //this.apRegionaldGroup.addTo(map);
            }
            const apLarge = this.apLargeMarkers;
            if (apLarge && apLarge.length > 0) {
                this.apLargeGroup = L.featureGroup(apLarge);
                this.apLargeGroup.addTo(map);
            }
            const apInt = this.apIntMarkers;
            if (apInt && apInt.length > 0) {
                this.apIntdGroup = L.featureGroup(apInt);
                this.apIntdGroup.addTo(map);
            }
            if (this.hubsMarkerArray && this.hubsMarkerArray.length > 0) {
                this.hubsGroup = L.featureGroup(this.hubsMarkerArray);
                this.hubsGroup.addTo(map);
            }
            //this.showHideAirportGroups(map);
            //this.apMarkerGroup = L.featureGroup(this.apMarkerArray);
            //this.apMarkerGroup.addTo(map);
            if (this.apMarkerArray && this.apMarkerArray.length > 0) {
                //map.fitBounds(this.apMarkerGroup.getBounds());
            }
        }
    }

    clearAirportMarkers(map: L.Map): void {
        //console.log('clearAirportMarkers -> ');

        if (this.apAirfieldGroup && this.apAirfieldGroup.getLayers() &&
            this.apAirfieldGroup.getLayers().length > 0) {
            this.apAirfieldGroup.clearLayers();
        }

        if (this.apLocaldGroup && this.apLocaldGroup.getLayers() &&
            this.apLocaldGroup.getLayers().length > 0) {
            this.apLocaldGroup.clearLayers();
        }

        if (this.apRegionaldGroup && this.apRegionaldGroup.getLayers() &&
            this.apRegionaldGroup.getLayers().length > 0) {
            this.apRegionaldGroup.clearLayers();
        }

        if (this.apIntdGroup && this.apIntdGroup.getLayers() &&
            this.apIntdGroup.getLayers().length > 0) {
            this.apIntdGroup.clearLayers();
        }

        if (this.hubsGroup && this.hubsGroup.getLayers() &&
            this.hubsGroup.getLayers().length > 0) {
            this.hubsGroup.clearLayers();
        }
        if (this.apLargeGroup && this.apLargeGroup.getLayers() &&
            this.apLargeGroup.getLayers().length > 0) {
            this.apLargeGroup.clearLayers();
        }

        if (this.apMarkerGroup && this.apMarkerGroup.getLayers() &&
            this.apMarkerGroup.getLayers().length > 0) {
            this.apMarkerGroup.clearLayers();
        }
    }

    showHideAirportGroups(map: L.Map) {
        const zoom = map.getZoom();
        if (this.apAirfieldGroup && this.apAirfieldGroup.getLayers() &&
            this.apAirfieldGroup.getLayers().length > 0) {
            const hasLayer = map.hasLayer(this.apAirfieldGroup);
            if (zoom > 9) {
                if (!hasLayer) { map.addLayer(this.apAirfieldGroup); }
            } else {
                if (hasLayer) { map.removeLayer(this.apAirfieldGroup); }
            }
        }
        if (this.apLocaldGroup && this.apLocaldGroup.getLayers() &&
            this.apLocaldGroup.getLayers().length > 0) {
            const hasLayer = map.hasLayer(this.apLocaldGroup);
            if (zoom > 8) {
                if (!hasLayer) { map.addLayer(this.apLocaldGroup); }
            } else {
                if (hasLayer) { map.removeLayer(this.apLocaldGroup); }
            }
        }
        if (this.apRegionaldGroup && this.apRegionaldGroup.getLayers() &&
            this.apRegionaldGroup.getLayers().length > 0) {
            const hasLayer = map.hasLayer(this.apRegionaldGroup);
            if (zoom > 7) {
                if (!hasLayer) { map.addLayer(this.apRegionaldGroup); }
            } else {
                if (hasLayer) { map.removeLayer(this.apRegionaldGroup); }
            }
        }
        if (this.apIntdGroup && this.apIntdGroup.getLayers() &&
            this.apIntdGroup.getLayers().length > 0) {
            const hasLayer = map.hasLayer(this.apIntdGroup);
            if (zoom > 6) {
                if (!hasLayer) { map.addLayer(this.apIntdGroup); }
            } else {
                if (hasLayer) { map.removeLayer(this.apIntdGroup); }
            }
        }

    }

    //#endregion

    //#region Helper Methods

    airports$: Observable<AmsAirport[]>;
    hubs$: Observable<AmsHub[]>;

    refreshAirports(map: L.Map): void {

        this.airport = undefined;
        this.hubs = undefined;
        this.flightNumbers = undefined;
        this.preloader.show();
        this.airports$ = this.apCache.airports;
        this.airports$.subscribe(airports => {
            this.airports = airports;
            this.hubs$ = this.hubCache.hubs;
            this.hubs$.subscribe(hubs => {
                this.hubs = hubs;
                if (map) {
                    this.clearAirportMarkers(map);
                    this.makeAirportMarkers(map);
                }
                const flpn$ = this.loadFlightNumbersData();
                flpn$.subscribe(flights => {
                    this.preloader.hide();
                    if (map) {
                        this.makeMapFlightNumbersPaths(map);
                    }
                    this.refreshGroup(map);
                });
            });
        });
    }

    refreshGroup(map: L.Map): void {
        //console.log('refreshGroup ->');
        //console.log('refreshGroup -> group:', this.group);
        //console.log('refreshGroup -> weekday:', this.weekday);
        if (this.group && this.weekday) {
            this.preloader.show();
            const flpns$ = this.loadFlightNumberScheduleData();
            flpns$.subscribe(flights => {
                this.preloader.hide();
                if (map) { this.makeMapFlnShedules(map); }
            });

        } else {
            this.mapFlnShedules = undefined;
            this.makeMapFlnShedules(map);
        }
    }

    callculateDistance(polyline: L.Polyline, start: L.LatLng, distanceM: number): L.LatLng {
        let rv: L.LatLng = start;
        let firstPoint: L.LatLng = undefined;
        //console.log('callculateDistance -> distanceM:', distanceM);
        if (polyline) {
            const points = polyline.getLatLngs();
            //console.log('callculateDistance -> points:', points);
            let idx = 0;
            if (points && points.length > 0) {
                points.forEach(function (latLng) {
                    const distanceToM = start.distanceTo(latLng);
                    if (distanceToM < distanceM) {
                        //console.log('callculateDistance -> '+idx+' distanceToM:', distanceToM);
                        rv = latLng;
                    }
                    idx++;
                });

            }
        }
        return rv;
    }

    async prepareAirports(): Promise<void> {
        let airports = new Array<AmsAirport>();
        if (this.flnShedules && this.flnShedules.length > 0) {

            for (const flight of this.flnShedules) {
                if (airports.findIndex(x => x.apId === flight.depApId) === -1) {
                    await this.apCache.getAirportAsync(flight.depApId).then(ap => {
                        airports.push(ap);
                    });
                }
                if (airports.findIndex(x => x.apId === flight.arrApId) === -1) {
                    await this.apCache.getAirportAsync(flight.arrApId).then(ap => {
                        airports.push(ap);
                    });
                }
            };
        }
        //this.setAirportsNoEvent(airports);
    }

    clearLayers(map: L.Map): void {
        this.clearAirportMarkers(map);
        this.clearMapFlightNumbers(map);
        this.clearMapFlnShedules(map);
    }

    getApMatrker(ap: AmsAirport): any {
        let marker;
        if (ap.lat && ap.lon) {
            const lat = ap.lat;
            const lon = ap.lon;

            const svgIcon = L.divIcon({
                html: ap.svgIconStr,
                className: ap.getStyle(),
                iconSize: [32, 32],
                iconAnchor: [8, 8],
            });

            //marker = L.marker([lat, lon], { icon: svgIcon, airport: ap, apId: ap.apId });
            marker = L.marker([lat, lon], { icon: svgIcon, 'airport': ap, 'apId': ap.apId });
            const isGroupStart = ap.apId === this.group?.apId;
            const flights = this.flightNumbers.filter(x => x.depApId === this.group?.apId && x.arrApId === ap.apId);
            const canCreateFlightNumber = !isGroupStart;
            marker.bindPopup(this.popupApService.makeAirportPopup(ap, canCreateFlightNumber, flights));
            marker.on("popupopen", (a) => {
                var popUp = a.target.getPopup()
                popUp.getElement().querySelector("#spmApOpen")
                    .addEventListener("click", e => {
                        this.spmAirportOpenClick(ap.apId);
                    });
                if (popUp.getElement().querySelector("#btnHubOpen")) {
                    popUp.getElement().querySelector("#btnHubOpen")
                        .addEventListener("click", e => {
                            this.openHubClick(ap);
                        });
                }
                if (popUp.getElement().querySelector("#btnRouteOpen")) {
                    popUp.getElement()
                        .querySelector("#btnRouteOpen")
                        .addEventListener("click", e => {
                            this.openRouteClick(ap);
                        });
                }
                if (popUp.getElement().querySelector("#btnFlpnOpen")) {
                    popUp.getElement()
                        .querySelector("#btnFlpnOpen")
                        .addEventListener("click", e => {
                            this.apflightNumberClick(ap);
                        });

                }

            });
        }
        //console.log('getApMatrker -> marker:', marker);
        return marker;
    }

    getHubMatrker(ap: AmsAirport): any {
        let marker;
        if (ap.lat && ap.lon) {
            const lat = ap.lat;
            const lon = ap.lon;

            const baseIcon = L.IconMaterial.icon({
                icon: 'home',
                iconColor: '#004d00',
                markerColor: 'rgba(26, 255, 26,0.4)',
                outlineColor: 'rgba(0, 51, 0,0.8)',
                outlineWidth: 1,
                iconSize: [28, 38]
            });
            const hubIcon = L.IconMaterial.icon({
                icon: 'hub',                                // Name of Material icon
                iconColor: '#fff',                       // Material icon color (could be rgba, hex, html name...)
                markerColor: 'rgba(0, 92, 230,0.4)',       // Marker fill color
                outlineColor: 'rgba(0, 51, 128,0.8)',      // Marker outline color
                outlineWidth: 1,
                iconSize: [28, 38]                         // Width and height of the icon
            });
            const groupIcon = L.IconMaterial.icon({
                icon: 'group_work',                                // Name of Material icon
                iconColor: this.group?.isActive ? '#33cc33' : '#ffff66',// Material icon color (could be rgba, hex, html name...)
                markerColor: 'rgba(0, 92, 230,0.4)',       // Marker fill color
                outlineColor: 'rgba(0, 51, 128,0.8)',      // Marker outline color
                outlineWidth: 1,
                iconSize: [28, 38]                         // Width and height of the icon
            });

            if (ap.apId === this.group?.apId) {
                marker = L.marker([lat, lon], { icon: groupIcon, airport: ap, apId: ap.apId });
                marker.bindTooltip(`Group Start Airport`);
            }
            else if (ap.hub?.hubTypeId === AmsHubTypes.Base) {
                marker = L.marker([lat, lon], { icon: baseIcon, airport: ap, apId: ap.apId });
                marker.bindTooltip(`Base ${ap.hub.hubName}`);
            }
            else if (ap.hub?.hubTypeId === AmsHubTypes.Hub) {
                marker = L.marker([lat, lon], { icon: hubIcon, airport: ap, apId: ap.apId });
                marker.bindTooltip(`Base ${ap.hub.hubName}`);
            }
            else {
                //marker = L.marker([lat, lon], { icon: hubIcon, airport: ap, apId: ap.apId });
                //marker.bindTooltip(`Hub ${ap.hub.hubName}`);
            }

            /*
            marker.bindPopup(this.popupApService.makeAirportPopup(ap));
            marker.on("popupopen", (a) => {
                var popUp = a.target.getPopup()
                popUp.getElement()
                    .querySelector("#spmApOpen")
                    .addEventListener("click", e => {
                        this.spmAirportOpenClick(ap.apId);
                    })
            });
            */
        }
        //console.log('getApMatrker -> marker:', marker);
        return marker;
    }

    centerMap(map: L.Map, latLnn: L.LatLng, zool: number = 8): void {
        if (map && latLnn) {
            map.setView(latLnn, zool);
        }
    }

    //#endregion

    //#region  SPM

    spmRouteOpenClick(routeId: number): void {
        console.log('spmRouteOpenClick -> routeId:', routeId);
        if (routeId) {

            //this.cus.spmAircraftPanelOpen.next(acId);
        }
    }

    spmAircradftOpenClick(acId: number): void {
        if (acId) {
            this.cus.spmAircraftPanelOpen.next(acId);
        }
    }

    spmAirportOpenClick(apId: number): void {
        if (apId) {
            this.cus.spmAirportPanelOpen.next(apId);
        }
    }

    openHubClick(ap: AmsAirport): void {
        console.log('openHubClick -> ap:', ap);
        if (ap && !ap.hub) {
            let req = new AmsHub();
            req.apId = ap.apId;
            req.alId = this.cus.airline.alId;
            this.preloader.show();
            this.alClient.hubSave(req).then(res => {
                console.log('openHubClick -> res:', res);
                this.preloader.hide();
                this.hubCache.clearCache();
                this.hubs = undefined;
                this.router.navigate(['/', AppRoutes.airline, AppRoutes.info, AppRoutes.hubs]);
            }).catch(err => {
                this.preloader.hide();
                console.log('openHubClick -> err:', err);
            });

        }
    }

    openRouteClick(ap: AmsAirport): void {
        console.log('openRouteClick -> ap:', ap);
    }

    //#endregion

    //#region Data

    //#region Shedule Groups

    groupsChanged = new Subject<AmsSheduleGroup[]>();
    groupsCount: number = 0;

    get groups(): AmsSheduleGroup[] {
        const objStr = localStorage.getItem(AppStore.mapAlGroups);
        let obj: AmsSheduleGroup[] = new Array<AmsSheduleGroup>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsSheduleGroup[];
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsSheduleGroup) => {
                    const ap = AmsSheduleGroup.fromJSON(element);
                    obj.push(ap);
                });
            }
        }
        return obj;
    }

    set groups(value: AmsSheduleGroup[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapAlGroups, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAlGroups);
        }

        this.groupsChanged.next(value);
    }

    get groupCriteria(): AmsSheduleGroupTableCriteria {
        let obj = new AmsSheduleGroupTableCriteria();
        obj.limit = 1000;
        obj.offset = 0;
        obj.alId = this.cus.airline.alId;
        obj.sortCol = 'grpName';
        obj.sortDesc = false;
        return obj;
    }


    loadGroups(): Observable<Array<AmsSheduleGroup>> {

        return new Observable<Array<AmsSheduleGroup>>(subscriber => {
            this.alClient.sheduleGroupsTableP(this.groupCriteria)
                .then((resp: ResponceAmsSheduleGroupTableData) => {
                    //console.log('loadData-> resp=', resp);
                    this.groups = new Array<AmsSheduleGroup>();
                    if (resp) {
                        if (resp && resp.groups && resp.groups.length > 0) {
                            this.groups = resp.groups;
                        }
                        this.groupsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else {
                        this.groupsCount = this.groups ? this.groups.length : 0;
                    }
                    subscriber.next(this.groups);
                }, msg => {
                    console.log('loadGroups -> ' + msg);
                    this.groupsCount = 0;
                    this.groups = new Array<AmsSheduleGroup>();
                });

        });

    }

    //#endregion

    //#endregion   
}
