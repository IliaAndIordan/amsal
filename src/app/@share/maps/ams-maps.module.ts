import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { CommonModule, CurrencyPipe } from '@angular/common';
import { MapJvectorOneComponent } from './map-jvector-one/map-jvector-one.component';
import { MapLifletApComponent } from './map-liflet-ap/map-liflet-ap.component';
import { MarkerService } from './map-liflet-ap-marker.service';
import { MapLifletSpmComponent } from './map-liflet-spm/map-liflet-spm.component';
import { MapLeafletSpmService } from './map-liflet-spm.service';
import { MapLeafletPopupAirportService } from './map-liflet-popup-airport.service';
import { MapLifletDashboardComponent } from './map-liflet-dashboard/map-liflet-dashboard.component';
import { MapLifletRouteComponent } from './map-liflet-route/map-liflet-route.component';
import { AmsVectormapDirective } from './directives/vector-map.directive';
import { MapLifletApRoutesComponent } from './map-liflet-ap-routes/map-liflet-ap-routes.component';
import { MapLeafletApRoutesService } from './map-liflet-ap-routes.service';
import { MapLifletScheduleGroupComponent } from './map-liflet-shedule-group/map-liflet-shedule-group.component';



@NgModule({
  declarations: [
    AmsVectormapDirective,
    MapJvectorOneComponent,
    MapLifletApComponent,
    MapLifletSpmComponent,
    MapLifletDashboardComponent,
    MapLifletRouteComponent,
    MapLifletApRoutesComponent,
    MapLifletScheduleGroupComponent,
  ],
  imports: [
    CommonModule,
    LeafletModule,
  ],
  exports:[
    LeafletModule,
    MapLifletApComponent,
    MapLifletSpmComponent,
    MapLifletDashboardComponent,
    MapLifletRouteComponent,
    MapLifletApRoutesComponent,
    MapLifletScheduleGroupComponent,
  ],
  providers:[
    MarkerService,
    MapLeafletPopupAirportService,
    CurrencyPipe,
  ]
})
export class AmsMapsModule { }
