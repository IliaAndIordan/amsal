import { AmsAirport } from "src/app/@core/services/api/airport/dto";
import { AmsFlight } from "src/app/@core/services/api/flight/dto";



import { AmsFlightStatus } from "src/app/@core/services/api/flight/enums";
import { GeometryUtil } from "leaflet";
import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AIRCRAFT_ICON } from "src/app/@core/const/app-storage.const";
import { MapLeafletPopupAirportService } from "./map-liflet-popup-airport.service";
import { AmsAlSheduleRoute, AmsSheduleGroup } from "src/app/@core/services/api/airline/al-flp-shedule";
import { AmsAlFlightNumber, AmsAlFlightNumberSchedule } from "src/app/@core/services/api/airline/al-flp-number";
import { AmsWeekday } from "src/app/@core/services/api/airline/enums";
import * as $ from 'jquery';
import { ajax, css } from "jquery";
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
declare let L;
//import * as L from 'leaflet-arc';


export const BaseLayerNameEsri = "Esri";
export const BaseLayerNameStreetMap = "OpenStreetMap";
export const BaseLayerNameStadiaDark = "Stadia Dark";


export interface IBaseLayerChangeEvent{
    layer:any;
    name: string;
    overlay: any
    sourceTarget: any;
    target: any;
    type: "baselayerchange"
}

export class ArcOptions {
    color: string;
    vertices: number;
    offset: number;
}

export const apIconType1 = L.icon({
    iconUrl: URL_COMMON_IMAGE_AIRCRAFT_ICON + 'yellow3.png',
    iconSize: [80, 80],
    iconAnchor: [40, 40],
});

export const DIV_ICON = L.divIcon({
    iconSize: L.point(24, 24), // icon size must same with element size
    iconAnchor: [12, 24],
    className: 'position-relative rotate--marker',
    html: `<div>
          <img style="width:24px;"
            src="${URL_COMMON_IMAGE_AIRCRAFT_ICON + 'yellow3.png'}"
          />
        </div>
      </>`,
});

//L.divIcon({html: "<i class='fa fa-car fa-2x' aria-hidden='true'></i>", iconSize: L.point(60, 60)});

export const MAP_AC_ICON = L.icon({
    iconUrl: URL_COMMON_IMAGE_AIRCRAFT_ICON + 'yellow3.png',
    iconSize: [80, 80],
    iconAnchor: [40, 40],
});

export class AmsMapMarkerOptions {
    public airport: AmsAirport;
    public apId: number;
    get typeId(): number {
        return (this as AmsMapMarkerOptions).airport ? (this as AmsMapMarkerOptions).airport.typeId : undefined;
    }
}

export class AmsMapFlight {

    public flight: AmsFlight;
    public depAp: AmsAirport;
    public arrAp: AmsAirport;
    public flPath: L.Polyline;
    public acMarker = L.Polyline;//L.motion.polyline
    public labelMarker = L.Marker;//L.Polyline;//L.motion.polyline
    public featureGroup: L.FeatureGroup;

    get flId(): number {
        return this.flight?.flId;
    }

    constructor() {
    }

    callculateDistance(start: L.LatLng, markerDistance: number, end: L.LatLng): L.LatLng {
        let rv: L.LatLng = start;
        let firstPoint: L.LatLng = undefined;
        let isStartPoint = true;
        //console.log('callculateDistance -> distanceM:', distanceM);
        if (this.flPath && markerDistance > 0) {
            const points = this.flPath.getLatLngs();
            //console.log('callculateDistance -> points:', points);
            let lastPont: L.LatLng;
            //let distanceToLastPoint:number;

            if (points && points.length > 0) {

                points.forEach(function (latLng) {
                    const distanceToPoint = start.distanceTo(latLng);
                    if (distanceToPoint < markerDistance) {
                        //console.log('callculateDistance -> '+idx+' distanceToM:', distanceToM);
                        rv = latLng;
                        isStartPoint = false;
                        //distanceToLastPoint = distanceToPoint;
                        lastPont = latLng;
                    }
                });

            }
            if (isStartPoint) {
                rv = lastPont;
            }
        }

        return rv;
    }

    makeFlightPaths(map: L.Map, popupApService: MapLeafletPopupAirportService): void {
       
        if (map && this.flight && this.depAp && this.arrAp) {
           
            const onFlight = (this.flight.flId && this.flight.acfsId > AmsFlightStatus.PlaneStand)?true:false;
            const color = onFlight?'yellow':'blue';
            const latlngDep = L.latLng(this.depAp.lat, this.depAp.lon);
            const latlngArr = L.latLng(this.arrAp.lat, this.arrAp.lon);

            const angle = GeometryUtil.angle(map, latlngDep, latlngArr);
            const bearing = GeometryUtil.bearing(latlngDep, latlngArr);
            const opt = { color: color, vertices: 10, offset: 0, weight: 2, opacity: 0.6, _flId: this.flight.flId };
            this.flPath = L.Polyline.Arc(latlngDep, latlngArr, opt);


            let marker: L.Marker;
            const speedmPh = (this.flight.speedKmPh / 3.6).toFixed(0); //m/sec
            const intervalMs = 60_000; // 1 sec
            const markerDistanse = (this.flight.distanceKm - this.flight.remainDistanceKm) * 1000;
            const duration = this.flight.remainTimeMin * 60 * 1000;
            const mPathStart: L.LatLng = this.callculateDistance(latlngDep, markerDistanse, latlngArr);
            const mPath = L.Polyline.Arc(mPathStart, latlngArr, { color: 'blue', vertices: 10, offset: 0 });
            const animatePolyline = true;
            const animateMarker = true;
            let acIcon = MAP_AC_ICON;
            acIcon.options.iconUrl = this.flight.acImgMapUrl;

            const divIcon = L.divIcon({ html: `<i class=\"material-icons\" aria-hidden=\"true\" motion-base=\"${angle}\">flight</i>`, iconSize: L.point(24, 24), iconAnchor: [12, 24], });

            const markerOpt = {
                removeOnEnd: true,
                showMarker: true,
                icon: acIcon,
                rotationAngle: angle,
            };
            const motionOpt = {
                auto: true, //auto start animation when motion object added to the map
                duration: duration, //Motion duration in ms
                speed: this.flight.speedKmPh, //Motion speed in KM/H
                easing: L.Motion.Ease.easeInOutQuart, //Animation strategy
            };
            if(onFlight){
                this.acMarker = L.motion.polyline(mPath.getLatLngs(), { color: "green", weight: 1, opacity: 0.8, _flId: this.flight.flId }, motionOpt, markerOpt);
            }else{
                this.acMarker = undefined;
            }
            
            this.flPath.bindPopup(popupApService.makeFlightPopup(this.flight, this.depAp, this.arrAp));
            this.featureGroup = L.featureGroup(this.flPath);
        }
    }

}

export class AmsMapAlFlightNumberSchedule {

    public flight: AmsAlFlightNumberSchedule;
    public depAp: AmsAirport;
    public arrAp: AmsAirport;
    public flPath: any;//L.Polyline;
    public acMarker = L.Polyline;//L.motion.polyline
    public featureGroup: L.FeatureGroup;
    weekday: AmsWeekday;
    group: AmsSheduleGroup;

    get flpnsId(): number {
        return this.flight?.flpnsId;
    }

    get acImgMapUrl(): string{
        return URL_COMMON_IMAGE_AIRCRAFT + (this.group?.acTypeId ? 'mac_' + this.group?.acTypeId + '_table.png' : 'aircraft_0_tableimg.png')
    } 

    constructor() {
    }

    callculateDistance(start: L.LatLng, markerDistance: number, end: L.LatLng): L.LatLng {
        let rv: L.LatLng = start;
        let firstPoint: L.LatLng = undefined;
        let isStartPoint = true;
        //console.log('callculateDistance -> distanceM:', distanceM);
        if (this.flPath && markerDistance > 0) {
            const points = this.flPath.getLatLngs();
            //console.log('callculateDistance -> points:', points);
            let lastPont: L.LatLng;
            //let distanceToLastPoint:number;

            if (points && points.length > 0) {

                points.forEach(function (latLng) {
                    const distanceToPoint = start.distanceTo(latLng);
                    if (distanceToPoint < markerDistance) {
                        //console.log('callculateDistance -> '+idx+' distanceToM:', distanceToM);
                        rv = latLng;
                        isStartPoint = false;
                        //distanceToLastPoint = distanceToPoint;
                        lastPont = latLng;
                    }
                });

            }
            if (isStartPoint) {
                rv = lastPont;
            }
        }

        return rv;
    }

    makeFlightPaths(map: L.Map, popupApService: MapLeafletPopupAirportService): void {
        if (map && this.flight && this.depAp && this.arrAp) {
            let color = 'blue';
            if (this.group?.isActive) {
                color = 'yellow';
            }
            let latlngs = [];
            var latlng1 = [this.depAp.lat, this.depAp.lon],
	            latlng2 = [this.arrAp.lat, this.arrAp.lon];
            const latlngDep = L.latLng(this.depAp.lat, this.depAp.lon);
            const latlngArr = L.latLng(this.arrAp.lat, this.arrAp.lon);

            const offsetX = latlng2[1] - latlng1[1];
            const offsetY = latlng2[0] - latlng1[0];
            const r = Math.sqrt( Math.pow(offsetX, 2) + Math.pow(offsetY, 2) );
            const theta = Math.atan2(offsetY, offsetX);
            const thetaOffset = (3.14/20);
            const r2 = (r/2)/(Math.cos(thetaOffset));
	        const theta2 = theta + thetaOffset;
            const midpointX = (r2 * Math.cos(theta2)) + latlng1[1];
            const midpointY = (r2 * Math.sin(theta2)) + latlng1[0];
            const midpointLatLng = [midpointY, midpointX];
            latlngs.push(latlng1, midpointLatLng, latlng2);

            // --- pathOptions 
            let opt:any = { color: color, vertices: 10, dashArray: "10 10", offset: 0, weight: 3, opacity: 0.8, _flId: this.flight.flpnsId };
           
            if (document && document?.getElementById('grpmap') &&
                typeof document?.getElementById('grpmap').animate === "function") { 
                const durationBase = 5000;
                const duration = (r<1?1:Math.sqrt(Math.log(r))) * durationBase;
                  opt.animate = {
                    duration: duration,
                    iterations: Infinity,
                    //easing: 'ease-in-out',
                    //direction: 'alternate'
                }
            }

            this.flPath = L.curve([ 'M', latlng1, 'Q', midpointLatLng, latlng2], opt);
            this.flPath.bindPopup(popupApService.makeFlightSchedulePopup(this));
            this.featureGroup = L.featureGroup(this.flPath);
        }
    }

}

export class AmsMapRoute {

    public route: AmsAlSheduleRoute;
    public depAp: AmsAirport;
    public arrAp: AmsAirport;
    public flPath: L.Polyline;
    //public acMarker = L.Polyline;//L.motion.polyline
    public featureGroup: L.FeatureGroup;

    get routeId(): number {
        return this.route?.routeId;
    }

    constructor() {
    }

    callculateDistance(start: L.LatLng, markerDistance: number, end: L.LatLng): L.LatLng {
        let rv: L.LatLng = start;
        let firstPoint: L.LatLng = undefined;
        let isStartPoint = true;
        //console.log('callculateDistance -> distanceM:', distanceM);
        if (this.flPath && markerDistance > 0) {
            const points = this.flPath.getLatLngs();
            //console.log('callculateDistance -> points:', points);
            let lastPont: L.LatLng;
            //let distanceToLastPoint:number;

            if (points && points.length > 0) {

                points.forEach(function (latLng) {
                    const distanceToPoint = start.distanceTo(latLng);
                    if (distanceToPoint < markerDistance) {
                        //console.log('callculateDistance -> '+idx+' distanceToM:', distanceToM);
                        rv = latLng;
                        isStartPoint = false;
                        //distanceToLastPoint = distanceToPoint;
                        lastPont = latLng;
                    }
                });

            }
            if (isStartPoint) {
                rv = lastPont;
            }
        }

        return rv;
    }

    makeRoutePaths(map: L.Map, popupApService: MapLeafletPopupAirportService): void {
        if (map && this.route && this.depAp && this.arrAp) {
            let color = 'blue';
            if (this.route.routeId && this.route.flightsCount > 0) {
                color = 'yellow';
            }
            const latlngDep = L.latLng(this.depAp.lat, this.depAp.lon);
            const latlngArr = L.latLng(this.arrAp.lat, this.arrAp.lon);

            const angle = GeometryUtil.angle(map, latlngDep, latlngArr);
            const bearing = GeometryUtil.bearing(latlngDep, latlngArr);
            const opt = { color: color, vertices: 10, offset: 0, weight: 2, opacity: 0.6, _routeId: this.route.routeId };
            this.flPath = L.Polyline.Arc(latlngDep, latlngArr, opt);

            //this.acMarker = L.motion.polyline(mPath.getLatLngs(), { color: "green", weight: 1, opacity: 0.8, _flId: this.flight.flId }, motionOpt, markerOpt);
            //this.flPath.bindPopup(popupApService.makeFlightPopup(this.route, this.depAp, this.arrAp));
            this.featureGroup = L.featureGroup(this.flPath);
        }
    }

}

export class AmsMapAlFlightNumber {

    public fln: AmsAlFlightNumber;
    public depAp: AmsAirport;
    public arrAp: AmsAirport;
    public flPath: any;
    public flnLabelMarker = L.Marker;//L.Polyline;//L.motion.polyline
    public featureGroup: L.FeatureGroup;

    get flpnId(): number {
        return this.fln?.flpnId;
    }

    constructor() {
    }

    callculateDistance(start: L.LatLng, markerDistance: number, end: L.LatLng): L.LatLng {
        let rv: L.LatLng = start;
        let firstPoint: L.LatLng = undefined;
        let isStartPoint = true;
        //console.log('callculateDistance -> distanceM:', distanceM);
        if (this.flPath && markerDistance > 0) {
            const points = this.flPath.getLatLngs();
            //console.log('callculateDistance -> points:', points);
            let lastPont: L.LatLng;
            //let distanceToLastPoint:number;

            if (points && points.length > 0) {

                points.forEach(function (latLng) {
                    const distanceToPoint = start.distanceTo(latLng);
                    if (distanceToPoint < markerDistance) {
                        //console.log('callculateDistance -> '+idx+' distanceToM:', distanceToM);
                        rv = latLng;
                        isStartPoint = false;
                        //distanceToLastPoint = distanceToPoint;
                        lastPont = latLng;
                    }
                });

            }
            if (isStartPoint) {
                rv = lastPont;
            }
        }

        return rv;
    }

    makeFlightNumberPaths(map: L.Map, popupApService: MapLeafletPopupAirportService): void {
        if (map && this.fln && this.depAp && this.arrAp) {
            let color = 'white';
            
            let latlngs = [];
            var latlng1 = [this.depAp.lat, this.depAp.lon],
	            latlng2 = [this.arrAp.lat, this.arrAp.lon];
            const latlngDep = L.latLng(this.depAp.lat, this.depAp.lon);
            const latlngArr = L.latLng(this.arrAp.lat, this.arrAp.lon);

            const offsetX = latlng2[1] - latlng1[1];
            const offsetY = latlng2[0] - latlng1[0];
            const r = Math.sqrt( Math.pow(offsetX, 2) + Math.pow(offsetY, 2) );
            const theta = Math.atan2(offsetY, offsetX);
            const thetaOffset = (3.14/50);
            const r2 = (r/2)/(Math.cos(thetaOffset));
	        const theta2 = theta + thetaOffset;
            const midpointX = (r2 * Math.cos(theta2)) + latlng1[1];
            const midpointY = (r2 * Math.sin(theta2)) + latlng1[0];
            const midpointLatLng = [midpointY, midpointX];
            latlngs.push(latlng1, midpointLatLng, latlng2);

            // --- pathOptions 
            let opt:any = { color: color, vertices: 2, dashArray: "5 20", offset: 0, weight: 2, opacity: 0.7, _flId: this.fln.flpnId };

            this.flPath = L.curve([ 'M', latlng1, 'Q', midpointLatLng, latlng2], opt);

            //const MarkerIcon = L.icon(feature.geometry.properties.icon);
            const angle = GeometryUtil.angle(map, latlngDep, latlngArr);
            const MarkerText = L.divIcon(
            {   className: 'map-flpn-label',
                html:`<div motion-base="${angle}">${this.fln.flpnNumber.toString().padStart(4, "0")}</div>`,
                permanent: true,
                direction: 'center',
                rotationAngle: angle,
                iconSize: null,//L.point(48, 24), 
                iconAnchor: [0,0]//[12, 24],
            });
            //const divIcon = L.divIcon({ html: `<i class="material-icons" aria-hidden="true" motion-base="${angle}">flight</i>`, iconSize: L.point(24, 24), iconAnchor: [12, 24], });
            //let  marker = L.marker(this.flPath.getBounds().getCenter(),  {icon: MAP_AC_ICON}).addTo(map); // add marker 
            this.flnLabelMarker = L.marker([midpointY, midpointX], {icon: MarkerText}).addTo(map); // add text on marker
            
            //this.flPath.bindPopup(popupApService.makeFlightSchedulePopup(this));
            this.featureGroup = L.featureGroup(this.flPath);

        }
    }

}