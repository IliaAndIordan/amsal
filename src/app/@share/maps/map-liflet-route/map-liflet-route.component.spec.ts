import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MapLifletRouteComponent } from './map-liflet-route.component';


describe('Share -> Maps -> MapLifletRouteComponent', () => {
  let component: MapLifletRouteComponent;
  let fixture: ComponentFixture<MapLifletRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapLifletRouteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapLifletRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
