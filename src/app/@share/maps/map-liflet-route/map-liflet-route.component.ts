import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { iconDefault, MarkerService } from '../map-liflet-ap-marker.service';
import { MapLeafletSpmService } from '../map-liflet-spm.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsHub } from 'src/app/@core/services/api/airline/al-hub';
import { GeometryUtil } from 'leaflet';
import { MAP_AC_ICON } from '../dto';
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
import 'leaflet.geodesic';
declare let L;
//import * as L from 'leaflet-arc';

L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'ams-map-liflet-route',
  templateUrl: './map-liflet-route.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class MapLifletRouteComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() depApId: number;
  @Input() arrApId: number;
  @Input() routeId: number;


  map;

  depAp: AmsAirport;
  arrAp: AmsAirport;

  baseAp: AmsAirport;

  airports$: Observable<AmsAirport[]>;
  hubs$: Observable<AmsHub[]>;
  airports: AmsAirport[];
  hubs: AmsHub[];

  apMarkerArray: Array<L.Marker>;
  apMarkerGroup: L.FeatureGroup;

  flightPathsArray: any[];
  flightPathsGroup: L.FeatureGroup;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private hubCache: AmsAirlineHubsCacheService,
    private mapService: MapLeafletSpmService) {
  }



  ngOnInit(): void {
    //const s = Snap('#map');
    this.baseAp = this.cus.hqAirport;
  }

  ngOnDestroy(): void {

  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  private initMap(): void {

    console.log('spm initMap -> baseAp:', this.baseAp);
    const osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    const esri = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
      maxZoom: 18,
      minZoom: 3,
      attribution: 'Tiles &copy; Esri &mdash;'
    });
    console.log('route initMap -> baseAp:', this.baseAp);
    this.map = L.map('rmap', {
      center: [this.baseAp ? this.baseAp.lat : 39.8282, this.baseAp ? this.baseAp.lon : -98.5795],
      zoom: 4,
      zoomControl: false,
      layers: [osm, esri]
    });

    const baseMaps = {
      "OpenStreetMap": osm,
      "Esri": esri
    };

    if (this.map) {
      //const layerControl = L.control.layers(baseMaps).addTo(this.map);
    }

    this.initData();
  }

  initData(): void {
    this.airports$ = this.apCache.airports;
    this.airports$.subscribe(airports => {
      this.airports = airports;
      this.hubs$ = this.hubCache.hubs;
      this.hubs$.subscribe(hubs => {
        this.hubs = hubs;
        this.initAirportsMarkers();
      });
    });


  }

  initAirportsMarkers(): void {
    if (!this.map) {
      this.initMap();
    } else {
      this.clearLayers();
      this.makeAirportMarkers();
      this.makeFlightPaths();
    }

  }

  makeAirportMarkers(): void {
    if (this.map) {
      this.apMarkerArray = new Array<L.Marker>();
      if (this.airports && this.airports.length > 0) {
        this.depAp = this.airports?.find(x => x.apId === this.depApId);
        let marker = this.mapService.getApMatrker(this.depAp);
        if(marker){
          this.apMarkerArray.push(marker);
        }
        this.arrAp = this.airports.find(x => x.apId === this.arrApId);
        marker = this.mapService.getApMatrker(this.arrAp);
        if(marker){
          this.apMarkerArray.push(marker);
        }
        this.apMarkerArray.push(marker);
      }
      this.apMarkerGroup = L.featureGroup(this.apMarkerArray);
      this.apMarkerGroup.addTo(this.map);
      if (this.apMarkerArray && this.apMarkerArray.length > 0) {
        this.map.fitBounds(this.apMarkerGroup.getBounds());
      }
    }
  }

  makeFlightPaths(): void {
    this.flightPathsArray = new Array<L.Circle>();
    if (this.map && this.depAp && this.arrAp) {
      let color = 'blue';

      const latlngDep = L.latLng(this.depAp.lat, this.depAp.lon);
      const latlngArr = L.latLng(this.arrAp.lat, this.arrAp.lon);

      const angle = GeometryUtil.angle(this.map, latlngDep, latlngArr);
      const bearing = GeometryUtil.bearing(latlngDep, latlngArr);
      const opt = { color: color, vertices: 10, offset: 0, weight: 2, opacity: 0.6, _routeId: this.routeId };
      const path = L.Polyline.Arc(latlngDep, latlngArr, opt);
      const geodesic = L.geodesic([latlngDep, latlngArr], opt);
      //this.featureGroup = L.featureGroup(path);
      this.flightPathsArray.push(geodesic);
    }
    this.flightPathsGroup = L.featureGroup(this.flightPathsArray);
    this.flightPathsGroup.addTo(this.map);
  }

  clearLayers(): void {
    if (this.apMarkerGroup && this.apMarkerGroup.getLayers() &&
      this.apMarkerGroup.getLayers().length > 0) {
      this.apMarkerGroup.clearLayers()
    }

    if (this.flightPathsGroup && this.flightPathsGroup.getLayers() &&
      this.flightPathsGroup.getLayers().length > 0) {
      this.flightPathsGroup.clearLayers()
    }
  }

}
