import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { AmsAirport, IAmsAirport } from "src/app/@core/services/api/airport/dto";
import { AppStore, URL_COMMON_IMAGE_AIRCRAFT_ICON } from "src/app/@core/const/app-storage.const";
import { BehaviorSubject, Subject } from "rxjs";
import { AmsAirportClient } from "src/app/@core/services/api/airport/api-client";
import { AmsWadCountryService } from "src/app/wad/country/country.service";
import { MapLeafletPopupAirportService } from "./map-liflet-popup-airport.service";
import { AmsFlight, IAmsFlight } from "src/app/@core/services/api/flight/dto";
import { AmsAirportCacheService } from "src/app/@core/services/api/airport/ams-airport-cache.service";
import { AmsFlightStatus } from "src/app/@core/services/api/flight/enums";
import { GeometryUtil } from "leaflet";
import { AmsMapAlFlightNumber, AmsMapFlight } from "./dto";
import * as $ from 'jquery';
import { ajax, css } from "jquery";
import '@elfalem/leaflet-curve';
import 'leaflet.bezier';
import 'leaflet-arc'
import 'leaflet.animatedmarker/src/AnimatedMarker';
import 'leaflet-rotatedmarker';
import 'leaflet-geometryutil';
import 'leaflet.motion/dist/leaflet.motion.min';
import 'leaflet-iconmaterial';
import 'leaflet';
//import * as L from 'leaflet-arc';
declare let L;

export class ArcOptions {
    color: string;
    vertices: number;
    offset: number;
}

export const apIconType1 = L.icon({
    iconUrl: URL_COMMON_IMAGE_AIRCRAFT_ICON + 'yellow3.png',
    iconSize: [60, 60],
    iconAnchor: [30, 60],
});

@Injectable({
    providedIn: 'root'
})
export class MapLeafletSpmService {

    iconsRootUrl = URL_COMMON_IMAGE_AIRCRAFT_ICON;
    apMarkerArray: Array<L.Marker>;
    apMarkerGroup: L.FeatureGroup;

    //#region leaflet.bezier

    bezierPathArray: any[];
    bezierOptionsA = {
        color: 'rgb(145, 146, 150)',
        fillColor: 'rgb(145, 146, 150)',
        dashArray: 8,
        opacity: 0.8,
        weight: '1',
        iconTravelLength: 1,
        iconMaxWidth: 50,
        iconMaxHeight: 50,
        fullAnimatedTime: 7000,
        easeOutPiece: 4,
        easeOutTime: 2500,
    };

    //#endregion

    spmMapPanelOpen = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient,
        private cus: CurrentUserService,
        private apClient: AmsAirportClient,
        private cService: AmsWadCountryService,
        private apCache: AmsAirportCacheService,
        private popupApService: MapLeafletPopupAirportService) { }



    //#region airport

    airportsChanged = new Subject<AmsAirport[]>();
    airportChanged = new Subject<AmsAirport>();

    get airports(): AmsAirport[] {
        const objStr = localStorage.getItem(AppStore.mapAirportsSpm);
        let obj: AmsAirport[] = new Array<AmsAirport>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsAirport[];//Object.assign(new AmsAirport(), );
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsAirport) => {
                    const ap = AmsAirport.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
    }

    set airports(value: AmsAirport[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapAirportsSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirportsSpm);
        }

        this.airportsChanged.next(value);
    }

    setAirportsNoEvent(value: AmsAirport[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapAirportsSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirportsSpm);
        }
    }

    get airport(): AmsAirport {
        const onjStr = localStorage.getItem(AppStore.mapAirportSpm);
        let obj: AmsAirport;
        if (onjStr) {
            obj = Object.assign(new AmsAirport(), JSON.parse(onjStr));
        }
        return obj;
    }

    set airport(value: AmsAirport) {
        const oldValue = this.airport;
        if (value) {
            localStorage.setItem(AppStore.mapAirportSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapAirportSpm);
        }

        this.airportChanged.next(value);
    }

    //#endregion

    //#region Flights

    flightsChanged = new Subject<AmsFlight[]>();

    get flights(): AmsFlight[] {
        const objStr = localStorage.getItem(AppStore.mapFlightsSpm);
        let obj: AmsFlight[] = new Array<AmsFlight>();

        if (objStr) {
            const objArr = JSON.parse(objStr) as IAmsFlight[];//Object.assign(new AmsAirport(), );
            if (objArr && objArr.length > 0) {
                objArr.forEach((element: IAmsFlight) => {
                    const ap = AmsFlight.fromJSON(element);
                    obj.push(ap);
                });
            }


        }
        return obj;
    }

    set flights(value: AmsFlight[]) {

        if (value && value.length > 0) {
            localStorage.setItem(AppStore.mapFlightsSpm, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.mapFlightsSpm);
        }

        this.flightsChanged.next(value);
    }

    mapFlights: AmsMapFlight[];
    flightsPathsArray: any[];
    flightsPathsGroup: L.FeatureGroup;
    flightsLabelsArray: Array<any>;

    makeMapFlightsPaths(map: L.Map): void {
        //console.log('makeMapFlightsPaths ->');
        if (map) {
            if (!this.mapFlights) { this.mapFlights = new Array<AmsMapFlight>(); }
            if (!this.flightsLabelsArray) { this.flightsLabelsArray = new Array<L.Marker>(); }
            const toRemove = this.mapFlightsToRemove();
            //console.log('makeMapFlightsPaths -> toRemove:', toRemove);
            if (toRemove && toRemove.length > 0) {
                this.removeMapFlights(map, toRemove);
            }
            const toUpdate = this.mapFlightsToUpdate();
            //console.log('makeMapFlightsPaths -> toUpdate:', toUpdate);
            if (toUpdate && toUpdate.length > 0) {
                toUpdate.forEach((mapFln: AmsMapFlight) => {
                    const flight = this.flights.find(x => x.flId === mapFln.flId);
                    mapFln.flight = flight;
                });
            }

            const flights = this.mapFlightsToAdd();
            //console.log('makeMapFlightsPaths -> mapFlightsToAdd:', flights);
            if (flights && flights.length > 0) {

                for (const flight of flights) {

                    let mapFln = new AmsMapFlight();
                    mapFln.flight = flight;
                    const onFlight = (mapFln.flight.flId && mapFln.flight.acfsId > AmsFlightStatus.PlaneStand) ? true : false;

                    mapFln.depAp = this.airports?.find(x => x.apId === flight.depApId);
                    mapFln.arrAp = this.airports?.find(x => x.apId === flight.arrApId);
                    mapFln.makeFlightPaths(map, this.popupApService);
                    //console.log('makeMapFlightsPaths -> mapFln:', mapFln);
                    if (mapFln?.flPath && onFlight && mapFln?.acMarker) {
                        mapFln.acMarker.bindPopup(this.popupApService.makeACPopup(mapFln));
                        mapFln.acMarker.addTo(map).on("popupopen", (a) => {
                            var popUp = a.target.getPopup()
                            popUp.getElement()
                                .querySelector("#acImage")
                                .addEventListener("click", e => {
                                    this.spmAircradftOpenClick(flight.acId);
                                })
                        });
                        mapFln.acMarker.motionResume();
                    }

                    if (mapFln.labelMarker && mapFln?.flId && this.flightsLabelsArray && this.flightsLabelsArray.length > 0) {
                        const flIdx = this.flightsLabelsArray.findIndex(x => x.options._flId === mapFln.flId);
                        if (flIdx === -1) { this.flightsLabelsArray.push(mapFln.labelMarker) }
                    }
                    if (mapFln.flPath) { mapFln.flPath.addTo(map) }
                    this.mapFlights.push(mapFln);
                }
            }
        }
    }

    mapFlightsToRemove(): AmsMapFlight[] {
        let rv = new Array<AmsMapFlight>();
        const filteredIds = this.flights.map(({ flId }) => flId);
        if (this.flights && this.mapFlights && this.mapFlights.length > 0) {
            rv = this.mapFlights.filter(function (mf: AmsMapFlight) {
                return filteredIds.indexOf(mf.flId) === -1;
            });
        }
        //console.log('mapFlightsToRemove -> rv:', rv);
        return rv;
    }

    mapFlightsToUpdate(): AmsMapFlight[] {
        let rv = new Array<AmsMapFlight>();
        const filteredIds = this.flights.map(({ flId }) => flId);
        if (this.flights && this.mapFlights && this.mapFlights.length > 0) {
            rv = this.mapFlights.filter(function (mf: AmsMapFlight) {
                return filteredIds.indexOf(mf.flId) !== -1;
            });
        }
        //console.log('mapFlightsToUpdate -> rv:', rv);
        return rv;
    }

    mapFlightsToAdd(): AmsFlight[] {
        let rv = this.flights;
        if (this.flights && this.mapFlights && this.mapFlights.length > 0) {
            const filteredIds = this.mapFlights.map(({ flId }) => flId);
            rv = this.flights.filter(function (fl: AmsFlight) {
                return filteredIds.indexOf(fl.flId) === -1;
            });
        }
        //console.log('flightsToAdd -> rv:', rv);
        return rv;
    }

    removeMapFlights(map: L.Map, toRemove: AmsMapFlight[]): void {
        //console.log('removeMapFlights -> toRemove:',toRemove);
        if (toRemove && toRemove.length > 0) {
            toRemove.forEach((mapFlpn: AmsMapFlight) => {
                const idx = this.mapFlights.findIndex(x => x.flId === mapFlpn.flId);

                if (mapFlpn.featureGroup && mapFlpn.featureGroup.getLayers() &&
                    mapFlpn.featureGroup.getLayers().length > 0) {
                    mapFlpn.featureGroup.clearLayers();
                }
                const hasacMarker = mapFlpn?.acMarker ? map.hasLayer(mapFlpn.acMarker) : false;
                //console.log('removeMapFlights -> hasacMarker:',hasacMarker);
                if (hasacMarker) {
                    map.removeLayer(mapFlpn.acMarker);
                }
                const hasLayer = mapFlpn?.flPath ? map.hasLayer(mapFlpn.flPath) : false;
                if (hasLayer) {
                    map.removeLayer(mapFlpn.flPath);
                }
                this.mapFlights?.splice(idx, 1);
            });
        }
    }

    clearMapFlights(map: L.Map): void {
        if (this.flightsPathsGroup && this.flightsPathsGroup.getLayers() &&
            this.flightsPathsGroup.getLayers().length > 0) {
            this.flightsPathsGroup.clearLayers();
            this.flightsPathsArray = new Array<L.Circle>();
        }
        if (this.flightsLabelsArray && this.flightsLabelsArray.length > 0) {
            this.flightsLabelsArray.forEach((acMerker: any) => {
                const hasLayer = map.hasLayer(acMerker);
                if (hasLayer) {
                    map.removeLayer(acMerker);
                }
            });
        }
        this.flightsLabelsArray = new Array<any>();
    }

    //#endregion

    makeMarkers(map: L.Map): void {
        this.clearLayers();
        if (this.flights && this.flights.length > 0) {
            this.prepareAirports().then(res => {
                this.makeMapFlightsPaths(map);
                this.makeAirportMarkers(map);
            });
        } else if (this.airports && this.airports.length > 0) {
            this.makeAirportMarkers(map);
        }
    }

    makeAirportMarkers(map: L.Map): void {
        if (map) {
            const airports = this.airports
            this.apMarkerArray = new Array<L.Marker>();
            if (airports && airports.length > 0) {
                for (const ap of airports) {
                    if (ap.lat && ap.lon) {
                        const marker = this.getApMatrker(ap);
                        if (marker) {
                            this.apMarkerArray.push(marker);
                        }
                    }
                }
                /*
                this.airport = airports[0];
                if(this.airport){
                    this.centerMap(map, new L.LatLng(this.airport.lat, this.airport.lon), this.airport.mapZoom)
                }*/
            }
            this.apMarkerGroup = L.featureGroup(this.apMarkerArray);
            this.apMarkerGroup.addTo(map);
            if (this.apMarkerArray && this.apMarkerArray.length > 0) {
                map.fitBounds(this.apMarkerGroup.getBounds());
            }
        }
    }

    //#endregion

    //#region Helper Methods

    callculateDistance(polyline: L.Polyline, start: L.LatLng, distanceM: number): L.LatLng {
        let rv: L.LatLng = start;
        let firstPoint: L.LatLng = undefined;
        //console.log('callculateDistance -> distanceM:', distanceM);
        if (polyline) {
            const points = polyline.getLatLngs();
            //console.log('callculateDistance -> points:', points);
            let idx = 0;
            if (points && points.length > 0) {
                points.forEach(function (latLng) {
                    const distanceToM = start.distanceTo(latLng);
                    if (distanceToM < distanceM) {
                        //console.log('callculateDistance -> '+idx+' distanceToM:', distanceToM);
                        rv = latLng;
                    }
                    idx++;
                });

            }
        }
        return rv;
    }

    async prepareAirports(): Promise<void> {
        let airports = new Array<AmsAirport>();
        if (this.flights && this.flights.length > 0) {

            for (const flight of this.flights) {
                //console.log('prepareAirports -> flight:', flight);
                if (airports.findIndex(x => x.apId === flight.depApId) === -1) {
                    await this.apCache.getAirport(flight.depApId).subscribe(ap => {
                        airports.push(ap);
                        //console.log('prepareAirports -> ap:', ap);
                    });
                }
                if (airports.findIndex(x => x.apId === flight.arrApId) === -1) {
                    await this.apCache.getAirport(flight.arrApId).subscribe(ap => {
                        airports.push(ap);
                        //console.log('prepareAirports -> ap:', ap);
                    });
                }
            };
        }
        //console.log('prepareAirports -> airports:', airports);
        this.setAirportsNoEvent(airports);
    }

    clearLayers(): void {
        if (this.apMarkerGroup && this.apMarkerGroup.getLayers() &&
            this.apMarkerGroup.getLayers().length > 0) {
            this.apMarkerGroup.clearLayers();
        }
    }

    getApMatrker(ap: AmsAirport): any {
        let marker;
        if (ap.lat && ap.lon) {
            const lat = ap.lat;
            const lon = ap.lon;

            const svgIcon = L.divIcon({
                html: ap.svgIconStr,
                className: ap.getStyle(),
                iconSize: [32, 32],
                iconAnchor: [8, 8],
            });
            marker = L.marker([lat, lon], { icon: svgIcon });
            marker.bindPopup(this.popupApService.makeAirportPopup(ap));
        }
        return marker;
    }

    centerMap(map: L.Map, latLnn: L.LatLng, zool: number = 8): void {
        if (map && latLnn) {
            map.setView(latLnn, zool);
        }
    }

    //#endregion

    spmAircradftOpenClick(acId: number): void {
        if (acId) {
            this.cus.spmAircraftPanelOpen.next(acId);
        }
    }

    spmAirportOpenClick(apId: number): void {
        if (apId) {
            this.cus.spmAirportPanelOpen.next(apId);
        }
    }



}
