import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxComponentsModule } from './components/components.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { AmsChartsModule } from './charts/ams-charts.module';
import { ToastrModule } from 'ngx-toastr';
import { AmsMapsModule } from './maps/ams-maps.module';
import { FullCalendarModule } from '@fullcalendar/angular';



@NgModule({
  imports: [
    CommonModule,
    //
    ToastrModule.forRoot(/*{
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
    }*/),
    //
    FullCalendarModule,
    SxPipesModule,
    SxComponentsModule,
    AmsChartsModule,
    AmsMapsModule,
  ],
  declarations: [],
  exports: [
    FullCalendarModule,
    SxComponentsModule,
    AmsChartsModule,
    AmsMapsModule,
  ]
})

export class SxShareModule { }
