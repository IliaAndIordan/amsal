import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// -Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { ExpandTab, ExpandTrigger, ShowlinksTrigger, ShowOverlayTrigger } from 'src/app/@core/const/animations-triggers';

@Component({
    selector: 'sx-sidebar-nav',
    templateUrl: 'sx-sidebar-nav.component.html',
    animations: [ExpandTab, ExpandTrigger, ShowlinksTrigger, ShowOverlayTrigger],
})

export class SxSidebarNavComponent implements OnInit, OnDestroy {

    showText: string = 'hide';
    snavWidth: string = 'small';
    showOverlay: string = 'hideOverLay';

    roots = AppRoutes;
    isAuthenticated: boolean;
    isAdmin: boolean;
    user: UserModel;
    userChanged: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private spinerService: SpinnerService,
        private cus: CurrentUserService) { }


    ngOnInit() {
        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.isAuthenticated = this.cus.isAuthenticated;
        this.isAdmin = this.cus.isAdmin;
    }

    logout() {
        console.log('logout->');
        this.spinerService.show();
        this.cus.logout().subscribe((res: Boolean) => {
            this.spinerService.hide();
            this.router.navigate([AppRoutes.Root, AppRoutes.public])
        });
    }

    //#region Expand/Close Menu

    toggle():void{
        if(this.showText === Animate.show){
            this.closeMenu();
        } else{
            this.expandMenu();
        }
    }

    expandMenu(): void {
        console.log('expandMenu ->');
        this.snavWidth = Animate.large;
        this.showOverlay = 'showOverLay';
        this.showText = Animate.show;

    }

    closeMenu(): void {
        console.log('closeMenu ->');
        this.snavWidth = Animate.small;
        this.showOverlay = 'hideOverLay';
        this.showText = Animate.hide;
    }

    //#endregion

}
