import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'sx-side-panel-row-value',
  templateUrl: 'side-panel-row-value.component.html',
})
export class SidePanelRowValueComponent implements OnInit, OnChanges {




  @Input() label: string;
  @Input() infoMessage: string;
  @Input() value: any;
  @Input() isPrice:boolean = false;
  @Input() isDate:boolean = false;
  @Input() isDateTime:boolean = false;
  @Input() size:string = 'med'; // accepts 'small', 'med' or large'
  @Input() valueCap:number = 30;

  valueD: number;
  isNumber:boolean = false;

  constructor(
    private cus:CurrentUserService,) { }

  ngOnInit() {
    this.initFields();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['value']) {
      //this.flight = changes['flight'].currentValue;
      this.initFields();
    } else if (changes['isPrice']) {
      this.initFields();
    } else if (changes['isDate']) {
      this.initFields();
    } else if (changes['isDateTime']) {
      this.initFields();
    }
  }

  initFields():void{
    if (this.size === 'large') {
      this.valueCap = 70;
    }
    this.isNumber =  (typeof this.value === 'number');
    //console.log('initFields-> isNumber:', this.isNumber);
    //console.log('initFields-> isPrice:', this.isPrice);
    if (this.isPrice) {
      this.valueD = parseFloat(this.value);
      //console.log('initFields-> valueD:', this.valueD);
    }
  }


}// end class
