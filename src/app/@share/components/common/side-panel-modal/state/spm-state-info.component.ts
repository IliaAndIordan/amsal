import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { URL_COMMON_IMAGE_AMS, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadService } from 'src/app/wad/wad.service';


@Component({
    selector: 'ams-spm-state-info',
    templateUrl: './spm-state-info.component.html',
})
export class SpmStateInfoComponent implements OnInit, OnChanges {


    @Input() state: AmsState;

    country: AmsCountry;
    subregion: AmsSubregion;
    region: AmsRegion;

    imagevUrl = URL_NO_IMG_SQ;
    ourApImg = URL_IMG_OUAP;
    ourApUrl: string;

    constructor(
        private cus: CurrentUserService,
        private wadService: AmsWadService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['state']) {
            this.state = changes['state'].currentValue;
            this.initFields();
        }
    }

    initFields() {

        this.region = this.wadService.region;
        this.subregion = this.wadService.subregion;
        this.country = this.wadService.country;

        if (this.country) {
            this.imagevUrl = this.country.flagUrl;
           
           
        } else{
            this.imagevUrl = URL_NO_IMG_SQ;
        }

        this.ourApUrl = WEB_OURAP_REGION + (this.state && this.state.iso?this.state.iso.split('-').join('/') + '/':'');
    }

}
