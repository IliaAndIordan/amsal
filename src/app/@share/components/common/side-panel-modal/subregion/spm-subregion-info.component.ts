import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { URL_COMMON_IMAGE_AMS, URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsRegion, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-spm-subregion-info',
    templateUrl: './spm-subregion-info.component.html',
})
export class SpmSubregionInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() subregion: AmsSubregion;
    @Input() region: AmsRegion;
    /**
     * FIELDS
     */
    imagevUrl = URL_NO_IMG_SQ;

    constructor(
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['subregion']) {
            this.subregion = changes['subregion'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.subregion) {
            this.imagevUrl = URL_COMMON_IMAGE_AMS + 'country/subregion_' + this.subregion.srCode + '.png';
        } else{
            this.imagevUrl = URL_NO_IMG_SQ;
        }
    }

}
