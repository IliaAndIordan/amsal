import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { URL_COMMON_IMAGE_AMS, URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsRegion } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-spm-region-info',
    templateUrl: './spm-region-info.component.html',
})
export class SpmRegionInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() region: AmsRegion;

    /**
     * FIELDS
     */
    imagevUrl = URL_NO_IMG_SQ;

    constructor(
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['region']) {
            this.region = changes['region'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.region) {
            this.imagevUrl = URL_COMMON_IMAGE_AMS + 'country/region_' + this.region.rCode + '.png';
        } else{
            this.imagevUrl = URL_NO_IMG_SQ;
        }
    }

}
