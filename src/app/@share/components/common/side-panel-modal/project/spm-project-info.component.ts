import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { SxProjectModel } from 'src/app/@core/services/api/project/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'sx-spm-project-info',
    templateUrl: './spm-project-info.component.html',
})
export class SxSpmProjectInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() project: SxProjectModel;

    /**
     * FIELDS
     */
    managerAvUrl = URL_NO_IMG_SQ;
    estimatorAvUrl = URL_NO_IMG_SQ;

    constructor(
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['project']) {
            this.project = changes['project'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.project) {
            this.managerAvUrl = this.project.manager_email ? 
                this.cus.getAvatarUrl(this.project.manager_email) : URL_NO_IMG_SQ;
                this.estimatorAvUrl = this.project.estimator_email ? 
                this.cus.getAvatarUrl(this.project.estimator_email) : URL_NO_IMG_SQ;
        }
    }

}
