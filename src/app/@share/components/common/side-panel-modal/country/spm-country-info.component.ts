import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { URL_COMMON_IMAGE_AMS, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-spm-country-info',
    templateUrl: './spm-country-info.component.html',
})
export class SpmCountryInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() state: AmsState;
    @Input() country: AmsCountry;

    /**
     * FIELDS
     */
    imagevUrl = URL_NO_IMG_SQ;
    ourApImg = URL_IMG_OUAP;
    ourApUrl: string;

    constructor(
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['country']) {
            this.country = changes['country'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.country) {
            this.imagevUrl = this.country.flagUrl;
        } else{
            this.imagevUrl = URL_NO_IMG_SQ;
        }

        this.ourApUrl = WEB_OURAP_REGION + (this.country && this.country.iso2?this.country.iso2 + '/':'');
    }

}
