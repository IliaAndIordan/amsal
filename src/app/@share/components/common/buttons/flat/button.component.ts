import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'sx-button-flat',
  templateUrl: './button.component.html'
})
// eslint-disable-next-line @angular-eslint/component-class-suffix
export class SxButtonFlat {

  /**
   * BINDINGS
   */
  @Input() type: string;
  @Input() color: string;
  @Input() icon: string;
  @Input() disabled: boolean;
  @Input() active: boolean;

  @Output() btnClick: EventEmitter<any> = new EventEmitter<any>();

  /**
   * FIELDS
   */



  constructor() { }


  handleClick(event: any) {
    this.btnClick.emit('emit!');
  }

}
