import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URL_IMG_AP, URL_IMG_OUAP, URL_IMG_WIKI, WEB_OURAP_AP, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';

@Component({
    selector: 'ams-img-link',
    templateUrl: './image-link.component.html',
})

export class ImageLinkComponent implements OnInit {

    /**
     * Bindings
     */
    @Input() url: string;
    @Input() type: string;
    @Input() ap_icao: string;
    @Input() state_iso: string;

    wikiUrl = URL_IMG_WIKI;
    ourApUrl = URL_IMG_OUAP;
    upImgUrl = URL_IMG_AP;

    ourAirportBase = WEB_OURAP_AP;

    oaLink: string;

    constructor(private router: Router) { }
    ngOnInit(): void {
        if (this.type === 'oa') {
            if(this.ap_icao){
                this.url = this.ap_icao ? this.ourAirportBase + this.ap_icao + '/pilot-info.html#general' : undefined;
            }
            else if(this.state_iso){
                this.url = WEB_OURAP_REGION + this.state_iso.split('-').join('/') + '/';
            }
            
        }

        
    }
}
