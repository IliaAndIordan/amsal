import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';

@Component({
  selector: 'ams-icon-ap-latlon',
  templateUrl: './icon-ap-latlon.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class IconApLatlonComponent implements OnInit {

  
  @Input() apId: number;
  @Input() isright: boolean = false;

  airport$: Observable<AmsAirport>;
  airport: AmsAirport;

  constructor(private apCache: AmsAirportCacheService,) { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['apId']) {
      this.initFields();
    }
    if (changes['isright']) {
      this.initFields();
    }
  }


  initFields(): void {
    if (this.apId) {
      this.airport$ = this.apCache.getAirport(this.apId);
      this.airport$.subscribe(ap => {
        this.airport = ap;
      });
    }
  }

  openMapClick(){
    let url = 'https://www.google.com/maps/search/?api=1&query=';
    url += this.airport.lat.toString()+','+ this.airport.lon.toString();
    window.open(url, "_blank");
  }
}
