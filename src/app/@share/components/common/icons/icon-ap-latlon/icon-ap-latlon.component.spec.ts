import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconApLatlonComponent } from './icon-ap-latlon.component';

describe('IconApLatlonComponent', () => {
  let component: IconApLatlonComponent;
  let fixture: ComponentFixture<IconApLatlonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconApLatlonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IconApLatlonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
