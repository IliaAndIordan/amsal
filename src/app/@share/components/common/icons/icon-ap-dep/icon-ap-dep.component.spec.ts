import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconApDepComponent } from './icon-ap-dep.component';

describe('Share -> Common -> icons -> IconApDepComponent', () => {
  let component: IconApDepComponent;
  let fixture: ComponentFixture<IconApDepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconApDepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconApDepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
