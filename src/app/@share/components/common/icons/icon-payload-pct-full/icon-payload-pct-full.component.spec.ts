import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IconPayloadPercentComponent } from './icon-payload-pct-full.component';

describe('IconPayloadPercentComponent', () => {
  let component: IconPayloadPercentComponent;
  let fixture: ComponentFixture<IconPayloadPercentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconPayloadPercentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconPayloadPercentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
