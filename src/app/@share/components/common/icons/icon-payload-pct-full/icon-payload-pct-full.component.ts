import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PayloadTypeBImgUrl, PayloadTypeCImgUrl, PayloadTypeEImgUrl, PayloadTypeFImgUrl, PayloadTypeGImgUrl, URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';

@Component({
  selector: 'ams-icon-payload-pct-full',
  templateUrl: './icon-payload-pct-full.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconPayloadPercentComponent implements OnInit, OnChanges {

  

  seatsFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_c.png';
  seatFImgUrl = PayloadTypeFImgUrl;
  seatsBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/persons_b.png';
  seatBImgUrl = PayloadTypeBImgUrl;
  seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/persons_e.png';
  seatEImgUrl = PayloadTypeEImgUrl;
  cargoImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';
  crewImgUrl = PayloadTypeGImgUrl;

  @Input() paxPct: number;
  @Input() cargoKgPct: number;

  typeE = AmsPayloadType.E;
  typeB = AmsPayloadType.B;
  typeF = AmsPayloadType.F;
  typeC = AmsPayloadType.C;
  typeG = AmsPayloadType.G;

  imagePaxUrl: string;
  imageCargoUrl: string;


  constructor() { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['paxPct']) {
      this.initFields();
    } else if (changes['cargoKgPct']) {
      this.initFields();
    }
  }


  initFields(): void {
    this.imagePaxUrl = this.seatsEImgUrl;
    this.imageCargoUrl = this.cargoImgUrl;
  }

}
