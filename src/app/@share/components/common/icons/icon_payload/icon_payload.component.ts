import { Component, Input } from '@angular/core';
import { PayloadTypeBImgUrl, PayloadTypeCImgUrl, PayloadTypeEImgUrl, PayloadTypeFImgUrl, PayloadTypeGImgUrl} from 'src/app/@core/const/app-storage.const';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';


@Component({
    selector: 'ams-icon-paylod',
    templateUrl: './icon_payload.component.html',
})

export class IconPayloadComponent {

    @Input() type: AmsPayloadType;
   
    typeE = AmsPayloadType.E;
    typeB = AmsPayloadType.B;
    typeF = AmsPayloadType.F;
    typeC = AmsPayloadType.C;
    typeG = AmsPayloadType.G;
  

    seatsFImgUrl = PayloadTypeFImgUrl;
    seatsBImgUrl = PayloadTypeBImgUrl;
    seatsEImgUrl = PayloadTypeEImgUrl;
    seatsCImgUrl = PayloadTypeCImgUrl;
    seatsGImgUrl = PayloadTypeGImgUrl;

}
