import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconFlightTypeComponent } from './icon-flight-type.component';

describe('Share -> Common -> icons -> IconFlightTypeComponent', () => {
  let component: IconFlightTypeComponent;
  let fixture: ComponentFixture<IconFlightTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconFlightTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconFlightTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
