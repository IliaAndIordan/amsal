import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AmsFlightType } from 'src/app/@core/services/api/flight/enums';

@Component({
  selector: 'ams-icon-flight-type',
  templateUrl: './icon-flight-type.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconFlightTypeComponent implements OnInit, OnChanges {

  @Input() flType: AmsFlightType;
  @Input() showText: string;
  iconCharter:string = 'schedule_send';
  iconTransfer:string = 'send_time_extension';
  iconFlight:string = 'flight';

  icon:string;

  constructor() { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['flType']) {
      this.initFields();
    }
  }


  initFields(): void {
    this.icon = this.iconFlight;
    switch (this.flType) {
      case AmsFlightType.Charter:
        this.icon = this.iconCharter;
        break;
      case AmsFlightType.Transfer:
        this.icon = this.iconTransfer;
        break;
    }
  }

}
