import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';

@Component({
  selector: 'ams-icon-ap-arr',
  templateUrl: './icon-ap-arr.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconApArrComponent implements OnInit {

  @Input() apId: number;
  @Input() right: boolean = false;
  @Input() showIcon: boolean = true;

  airport$:Observable<AmsAirport>;
  airport:AmsAirport;
  constructor(private apCache: AmsAirportCacheService,) { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['apId']) {
      this.initFields();
    }
    if (changes['right']) {
      this.initFields();
    }
  }


  initFields(): void {
    if (this.apId) {
      this.airport$ =  this.apCache.getAirport(this.apId);
      this.airport$.subscribe(ap=>{
        this.airport = ap;
      });
    }
  }

}
