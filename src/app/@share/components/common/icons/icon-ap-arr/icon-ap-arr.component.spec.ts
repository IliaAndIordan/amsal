import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconApArrComponent } from './icon-ap-arr.component';

describe('Share -> Common -> icons -> IconApArrComponent', () => {
  let component: IconApArrComponent;
  let fixture: ComponentFixture<IconApArrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconApArrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconApArrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
