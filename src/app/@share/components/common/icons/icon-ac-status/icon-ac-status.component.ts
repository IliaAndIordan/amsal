import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AircraftStatus } from 'src/app/@core/models/pipes/aircraft-statis.pipe';

@Component({
  selector: 'ams-icon-ac-status',
  templateUrl: './icon-ac-status.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class IconAcStatusComponent implements OnInit, OnChanges {

  @Input() acStatusId: AircraftStatus;
  @Input() showText: string;

  iconNew: string = 'fiber_new';
  iconOperational: string = 'assured_workload';
  iconMaintenance: string = 'engineering';
  iconForLeaseOrSale: string = 'shopping_cart_checkout';
  iconRetired: string = 'museum';

  icon: string;
  iconClass:string = 'gray';

  constructor() { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['acStatusId']) {
      this.initFields();
    }
  }

  initFields(): void {
    this.icon = this.iconOperational;
    switch (this.acStatusId) {
      case AircraftStatus.New:
        this.icon = this.iconNew;
        this.iconClass = 'yellow';
        break;
      case AircraftStatus.Operational:
        this.icon = this.iconOperational;
        this.iconClass = 'green-dark';
        break;
      case AircraftStatus.Maintenance:
        this.icon = this.iconMaintenance;
        this.iconClass = 'blue';
        break;
      case AircraftStatus.ForLeaseOrSale:
        this.icon = this.iconForLeaseOrSale;
        this.iconClass = 'gray';
        break;
      case AircraftStatus.Retired:
        this.icon = this.iconRetired;
        this.iconClass = 'orange';
        break;
    }
  }
}
