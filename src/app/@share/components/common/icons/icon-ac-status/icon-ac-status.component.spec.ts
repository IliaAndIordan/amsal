import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconAcStatusComponent } from './icon-ac-status.component';

describe('Ams -> Share -> Common -> Icons -> IconAcStatusComponent', () => {
  let component: IconAcStatusComponent;
  let fixture: ComponentFixture<IconAcStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconAcStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconAcStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
