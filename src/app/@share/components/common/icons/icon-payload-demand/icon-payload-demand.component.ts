import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';

@Component({
  selector: 'ams-icon-payload-demand',
  templateUrl: './icon-payload-demand.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class IconPayloadDemandComponent implements OnInit, OnChanges {

  seatsFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_f.png';
  seatsBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_b.png';
  seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/persons_e.png';
  cargoImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';
  crewImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_c.png';

  @Input() periodDays: number;
  @Input() paxDemandPeriod: number;
  @Input() cargoDemandPeriodKg: number;

  @Input() paxDemandDay: number;

  typeE = AmsPayloadType.E;
  typeB = AmsPayloadType.B;
  typeF = AmsPayloadType.F;
  typeC = AmsPayloadType.C;
  typeG = AmsPayloadType.G;

  imageUrl: string;

  constructor() { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['paxDemandPeriod']) {
      this.initFields();
    }
  }


  initFields(): void {
    
  }

}
