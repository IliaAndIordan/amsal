import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconPayloadDemandComponent } from './icon-payload-demand.component';

describe('IconPayloadDemandComponent', () => {
  let component: IconPayloadDemandComponent;
  let fixture: ComponentFixture<IconPayloadDemandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconPayloadDemandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconPayloadDemandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
