import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-icon-ap-code',
  templateUrl: './icon-ap-code.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class IconApCodeComponent implements OnInit, OnChanges {


  @Input() apId: number;
  @Input() showName: boolean = false;
  @Input() showCity: boolean = false;
  @Input() showNSvg: boolean = true;
  @Input() label: string = 'Airport';
  @Output() centerMap: EventEmitter<AmsAirport> = new EventEmitter<AmsAirport>();

  airport$: Observable<AmsAirport>;
  airport: AmsAirport;

  constructor(
    private apCache: AmsAirportCacheService,
    private cus: CurrentUserService) {
  }

  ngOnInit(): void {
    this.initFields();
    this.loadAirport()
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['apId']) {
      this.loadAirport();
    }
    if (changes['showNSvg']) {
      this.initFields();
    }
  }


  initFields(): void {
    //console.log('initFields -> showName:', this.showName);
    //console.log('initFields -> showNSvg:', this.showNSvg);

  }

  loadAirport() {
    if (this.apId) {
      this.airport$ = this.apCache.getAirport(this.apId);
      this.airport$.subscribe(ap => {
        this.airport = ap;
        this.initFields();
      });
    }
  }

  centerMapOnAirport() {
    if (this.airport) {
      this.centerMap.emit(this.airport);
    }
  }

  spmAirportOpen() {
    if (this.airport && this.airport.apId) {
      this.cus.spmAirportPanelOpen.next(this.airport.apId);
    }
  }

}
