import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconApCodeComponent } from './icon-ap-code.component';

describe('IconApCodeComponent', () => {
  let component: IconApCodeComponent;
  let fixture: ComponentFixture<IconApCodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconApCodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconApCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
