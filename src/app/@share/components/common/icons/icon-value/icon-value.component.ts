import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { URL_IMG_IATA, URL_IMG_ICAO } from 'src/app/@core/const/app-storage.const';

@Component({
    selector: 'ams-icon-value',
    templateUrl: './icon-value.component.html',
})

export class IconValueComponent implements OnInit, OnChanges {

    @Input() icon: string;
    @Input() value: any;
    @Input() label: string;
    @Input() tooltip: string;
    @Input() pipe: string;

    icaoUrl = URL_IMG_ICAO;
    iataUrl = URL_IMG_IATA;
    dateVal:Date;

    
    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['value']) {
            this.initFields();
        }
        if (changes['label']) {
            this.initFields();
        }
        if (changes['pipe']) {
            this.initFields();
        }
        
    }

    initFields() {
        this.dateVal = this.getDateVal();
    }

    getDateVal():Date{
        let rv:Date;
        if(this.value){
            const isDate:boolean = typeof this.value.getTime === 'function'?true:false;
            if(isDate){
                rv = this.value;
            } else{
                try{
                    rv = new Date(this.value);
                }catch(ex){}
                
            }
        }
        
        return rv;
    }

}
