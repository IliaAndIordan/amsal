import { CommonModule } from '@angular/common';
import { Component, Input, OnChanges, SimpleChanges, type OnInit } from '@angular/core';
import { find } from 'rxjs';
import { URL_COMMON_IMAGE_AIRCRAFT_ICON } from 'src/app/@core/const/app-storage.const';
import { AcTypeOpt, AmsAcType } from 'src/app/@core/models/pipes/ac-type.pipe';

@Component({
  selector: 'ams-icon-ac-type',
  templateUrl: './icon-ac-type.component.html',
})
export class AmsIconAcTypeComponent implements OnInit, OnChanges {

  @Input() acTypeId: number;
  
  
  acImgMapUrl:string;
  acType:AmsAcType;

  
  constructor() { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['acTypeId']) {
      this.initFields();
    }
  }

 
   
  initFields(){
    this.acType = AcTypeOpt.find(x=>x.acTypeId === this.acTypeId);
    this.acImgMapUrl= URL_COMMON_IMAGE_AIRCRAFT_ICON + (this.acTypeId ? 'ac-type-' + this.acTypeId + '-yellow.png': 'ac-type-2-yellow.png');
  }

}
