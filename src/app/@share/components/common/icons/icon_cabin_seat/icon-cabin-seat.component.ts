import { Component, Input } from '@angular/core';
import { PayloadTypeBImgUrl, PayloadTypeCImgUrl, PayloadTypeCrewImgUrl, PayloadTypeEImgUrl, PayloadTypeFImgUrl, PayloadTypeGImgUrl} from 'src/app/@core/const/app-storage.const';

@Component({
    selector: 'ams-cabin-seat',
    templateUrl: './icon-cabin-seat.component.html',
})

export class IconCabinSeatComponent {

    @Input() paxE: number;
    @Input() paxB: number;
    @Input() paxF: number;
    @Input() crew: number;
    @Input() pilots: number;
    @Input() cargoKg: number;

    seatsFImgUrl = PayloadTypeFImgUrl;
    seatsBImgUrl = PayloadTypeBImgUrl;
    seatsEImgUrl = PayloadTypeEImgUrl;
    seatsCImgUrl = PayloadTypeCImgUrl;
    seatsGImgUrl = PayloadTypeGImgUrl;
    seatsPilotImgUrl = PayloadTypeGImgUrl;
    seatsCrewImgUrl = PayloadTypeCrewImgUrl

}
