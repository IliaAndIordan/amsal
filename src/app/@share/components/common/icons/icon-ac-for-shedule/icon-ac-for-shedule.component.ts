import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges, type OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsFlightType } from 'src/app/@core/services/api/flight/enums';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { MapLeafletAlSheduleGroupService } from 'src/app/@share/maps/map-liflet-al-shedule-group.service';

@Component({
  selector: 'ams-icon-ac-for-shedule',
  templateUrl: './icon-ac-for-shedule.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
})
export class IconAcForSheduleComponent implements OnInit, OnChanges {

  @Input() isforschedule: boolean = false;
  @Input() showText: string;
  @Input() grpId: number;
  @Input() grpName: string;

  iconCharter: string = 'schedule_send';
  iconSchedule: string = 'send_time_extension';
  iconFlight: string = 'flight';

  icon: string;
  label: string;

  constructor(
    private router: Router) { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['isforschedule']) {
      this.initFields();
    }
  }


  initFields(): void {
    this.icon = this.isforschedule ? this.iconSchedule : this.iconCharter;
    this.label = this.isforschedule ? `For Schedule group ${this.grpId ? (this.grpName?this.grpName:' #' + this.grpId) : 'No group assignet.'}` : 'For Charter';
  }

  gotoGroup(): void {
    if (this.grpId) {
      this.router.navigate([AppRoutes.Root, AppRoutes.flightPlan, AppRoutes.shedule, this.grpId]);
    }
  }

}