import { Component, EventEmitter, Input, Output } from '@angular/core';
import { URL_IMG_IATA, URL_IMG_ICAO } from 'src/app/@core/const/app-storage.const';

@Component({
    selector: 'ams-icon-play',
    templateUrl: './icon-play.component.html',
})

export class IconPlayComponent {

    @Input() value: boolean;
    @Input() tooltip: string;
    @Output() iconClick: EventEmitter<boolean> = new EventEmitter<boolean>();

    handleClick() {
        this.iconClick.emit(this.value);
      }
    
}
