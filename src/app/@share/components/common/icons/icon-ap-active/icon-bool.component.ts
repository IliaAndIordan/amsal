import { Component, EventEmitter, Input, Output } from '@angular/core';
import { URL_IMG_IATA, URL_IMG_ICAO } from 'src/app/@core/const/app-storage.const';

@Component({
    selector: 'ams-icon-ap-active',
    templateUrl: './icon-bool.component.html',
})

export class IconAirportActiveComponent {

    /**
     * Bindings
     */
    @Input() value: any;
    @Output() btnClick: EventEmitter<any> = new EventEmitter<any>();

    handleClick(event: any) {
        //console.log('handleClick-> event:', event);
        this.btnClick.emit(event);
      }
    
}
