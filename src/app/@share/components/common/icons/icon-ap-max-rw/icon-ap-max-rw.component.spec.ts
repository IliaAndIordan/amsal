import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconApMaxRunwayComponent } from './icon-ap-max-rw.component';

describe('IconApMaxRunwayComponent', () => {
  let component: IconApMaxRunwayComponent;
  let fixture: ComponentFixture<IconApMaxRunwayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconApMaxRunwayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconApMaxRunwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
