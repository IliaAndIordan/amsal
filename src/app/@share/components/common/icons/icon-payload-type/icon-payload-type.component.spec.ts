import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconPayloadTypeComponent } from './icon-payload-type.component';

describe('IconPayloadTypeComponent', () => {
  let component: IconPayloadTypeComponent;
  let fixture: ComponentFixture<IconPayloadTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconPayloadTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconPayloadTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
