import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PayloadTypeBImgUrl, PayloadTypeCImgUrl, PayloadTypeEImgUrl, PayloadTypeFImgUrl, PayloadTypeGImgUrl, URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';

@Component({
  selector: 'ams-icon-payload-type',
  templateUrl: './icon-payload-type.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconPayloadTypeComponent implements OnInit, OnChanges {

  

  seatsFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_c.png';
  seatFImgUrl = PayloadTypeFImgUrl;
  seatsBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/persons_b.png';
  seatBImgUrl = PayloadTypeBImgUrl;
  seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/persons_e.png';
  seatEImgUrl = PayloadTypeEImgUrl;
  cargoImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';
  crewImgUrl = PayloadTypeGImgUrl;

  @Input() payloadType: AmsPayloadType;
  @Input() pax: number;
  @Input() cargoKg: number;
  @Input() description: string;

  typeE = AmsPayloadType.E;
  typeB = AmsPayloadType.B;
  typeF = AmsPayloadType.F;
  typeC = AmsPayloadType.C;
  typeG = AmsPayloadType.G;

  imageUrl: string;

  constructor() { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['payloadType']) {
      this.initFields();
    } else if (changes['pax']) {
      this.initFields();
    }
  }


  initFields(): void {
    this.imageUrl = this.crewImgUrl;
    switch (this.payloadType) {
      case this.typeE:
        this.imageUrl = this.pax>1?this.seatsEImgUrl:this.seatEImgUrl;
        break;
      case this.typeB:
        this.imageUrl = this.pax>1?this.seatsBImgUrl:this.seatBImgUrl;
        break;
      case this.typeF:
        this.imageUrl = this.pax>1?this.seatsFImgUrl:this.seatFImgUrl;
        break;
      case this.typeC:
        this.imageUrl = this.cargoImgUrl;
        break;
      case this.typeG:
        this.imageUrl = this.crewImgUrl;
        break;
    }
  }

}
