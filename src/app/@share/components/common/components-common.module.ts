import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner.component';
import { SxNavbarComponent } from './navbar/sx-navbar.component';
import { SxMaterialModule } from '../material.module';
import { SxSidebarNavComponent } from './sidebar/nav/sx-sidebar-nav.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { SxButtonRound } from './buttons/round/button.component';
import { AmsSidePanelModalComponent } from './side-panel-modal/side-panel-modal';
import { SxFilterPanelComponent } from './filter-panel/filter-panel.component';
import { SxButtonFlat } from './buttons/flat/button.component';
import { SxFormatControl } from './buttons/format-control/format-control.component';
import { SxSpmProjectInfoComponent } from './side-panel-modal/project/spm-project-info.component';
import { SidePanelRowValueComponent } from './side-panel-modal/row/side-panel-row-value.component';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { ButtonInfo } from './buttons/info/button.component';
import { ImageLinkComponent } from './buttons/image-link/image-link.component';
import { IconValueComponent } from './icons/icon-value/icon-value.component';
import { SpmRegionInfoComponent } from './side-panel-modal/region/spm-region-info.component';
import { SpmSubregionInfoComponent } from './side-panel-modal/subregion/spm-subregion-info.component';
import { SpmStateInfoComponent } from './side-panel-modal/state/spm-state-info.component';
import { IconBoolComponent } from './icons/icon-bool/icon-bool.component';
import { IconAirportActiveComponent } from './icons/icon-ap-active/icon-bool.component';
import { AmsAirportIconSvgComponent } from './icons/airport-icon-svg.component';
import { SpmCountryInfoComponent } from './side-panel-modal/country/spm-country-info.component';
import { AlILogo480CardComponent } from '../cards/airline/logo/al-logo-480-card.component';
import { IconCabinSeatComponent } from './icons/icon_cabin_seat/icon-cabin-seat.component';
import { IconPayloadTypeComponent } from './icons/icon-payload-type/icon-payload-type.component';
import { IconFlightTypeComponent } from './icons/icon-flight-type/icon-flight-type.component';
import { IconApDepComponent } from './icons/icon-ap-dep/icon-ap-dep.component';
import { IconApArrComponent } from './icons/icon-ap-arr/icon-ap-arr.component';
import { IconAcStatusComponent } from './icons/icon-ac-status/icon-ac-status.component';
import { IconApCodeComponent } from './icons/icon-ap-code/icon-ap-code.component';
import { IconApMaxRunwayComponent } from './icons/icon-ap-max-rw/icon-ap-max-rw.component';
import { AmsSidePanelModalLargeComponent } from './side-panel-modal/side-panel-modal-large';
import { IconPayloadDemandComponent } from './icons/icon-payload-demand/icon-payload-demand.component';
import { IconApLatlonComponent } from './icons/icon-ap-latlon/icon-ap-latlon.component';
import { IconPlayComponent } from './icons/icon-bool/icon-play.component';
import { IconPayloadComponent } from './icons/icon_payload/icon_payload.component';
import { IconAcForSheduleComponent } from './icons/icon-ac-for-shedule/icon-ac-for-shedule.component';
import { AmsIconAcTypeComponent } from './icons/icon-ac-type/icon-ac-type.component';
import { IconPayloadPercentComponent } from './icons/icon-payload-pct-full/icon-payload-pct-full.component';
import { IconPayloadTypePriceComponent } from './icons/icon-payload-type price/icon-payload-type-price.component';




@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SxMaterialModule,
    SxPipesModule,
  ],
  declarations: [
    SpinnerComponent,
    SxNavbarComponent,
    SxSidebarNavComponent,
    // -Buttons
    SxButtonRound,
    SxButtonFlat,
    ButtonInfo,
    SxFormatControl,
    ImageLinkComponent,
    IconValueComponent,
    IconBoolComponent,
    IconAirportActiveComponent,
    AmsIconAcTypeComponent,
    // -icons
    AmsAirportIconSvgComponent,
    IconCabinSeatComponent,
    // -SPM Panels
    AmsSidePanelModalComponent,
    AmsSidePanelModalLargeComponent,
    SidePanelRowValueComponent,
    SxSpmProjectInfoComponent,
    SpmRegionInfoComponent,
    SpmSubregionInfoComponent,
    SpmStateInfoComponent,
    SpmCountryInfoComponent,
    AlILogo480CardComponent,
    // -Filter Panels
    SxFilterPanelComponent,
    IconPayloadTypeComponent,
    IconPayloadDemandComponent,
    IconFlightTypeComponent,
    IconApDepComponent,
    IconApArrComponent,
    IconAcStatusComponent,
    IconApCodeComponent,
    IconApMaxRunwayComponent,
    IconApLatlonComponent,
    IconPlayComponent,
    IconPayloadComponent,
    IconAcForSheduleComponent,
    IconPayloadPercentComponent,
    IconPayloadTypePriceComponent,
  ],
  exports: [
    SxMaterialModule,
    SpinnerComponent,
    SxNavbarComponent,
    SxSidebarNavComponent,
    // -Buttons
    SxButtonRound,
    SxButtonFlat,
    ButtonInfo,
    SxFormatControl,
    ImageLinkComponent,
    IconValueComponent,
    IconBoolComponent,
    IconAirportActiveComponent,
    AmsIconAcTypeComponent,
    // -icons
    AmsAirportIconSvgComponent,
    IconCabinSeatComponent,
    // -SPM Panels
    AmsSidePanelModalComponent,
    AmsSidePanelModalLargeComponent,
    SidePanelRowValueComponent,
    SxSpmProjectInfoComponent,
    SpmRegionInfoComponent,
    SpmSubregionInfoComponent,
    SpmStateInfoComponent,
    SpmCountryInfoComponent,
    AlILogo480CardComponent,
    IconPayloadDemandComponent,
    // -Filter Panels
    SxFilterPanelComponent,
    IconPayloadTypeComponent,
    IconFlightTypeComponent,
    IconApDepComponent,
    IconApArrComponent,
    IconAcStatusComponent,
    IconApCodeComponent,
    IconApMaxRunwayComponent,
    IconApLatlonComponent,
    IconPlayComponent,
    IconPayloadComponent,
    IconAcForSheduleComponent,
    IconPayloadPercentComponent,
    IconPayloadTypePriceComponent,
  ]
})

export class SxComponentsCommonModule { }
