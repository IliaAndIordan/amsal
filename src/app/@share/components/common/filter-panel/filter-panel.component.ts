import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Animate } from 'src/app/@core/const/animation.const';
import { TogglePanelTrigger } from './animation';

@Component({
  selector: 'sx-filter-panel',
  templateUrl: './filter-panel.component.html',
  animations: [TogglePanelTrigger]
})

export class SxFilterPanelComponent implements OnInit {

  @Output() closePanel: EventEmitter<any> = new EventEmitter<any>();
  @Output() subtitleClick: EventEmitter<any> = new EventEmitter<any>();

  @Input() panelVar: string;
  @Input() fptitle: string;
  @Input() subtitle: string;

  interval: any;
  currTime:Date;

  constructor() { }


  ngOnInit() {
    this.currTime = new Date();
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.interval = setInterval(() => {
      this.refreshTime();
    }, 60_000);

  }

  openPanelClick(): void {
    // this.panelVar = Animate.out;
  }

  closePanelClick(): void {
    // this.panelVar = Animate.in;
    this.closePanel.emit();
  }

  subtitleClickEv(): void {
    // this.panelVar = Animate.in;
    this.subtitleClick.emit();
  }

  refreshTime(){
    this.currTime = new Date();
  }

}
