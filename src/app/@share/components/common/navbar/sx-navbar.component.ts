import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
// Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// Models
import { Subscribable, Subscription } from 'rxjs';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { environment } from 'src/environments/environment';
import { COMMON_IMG_LOGO_IZIORDAN, COMMON_IMG_LOGO_RED } from 'src/app/@core/const/app-storage.const';
import { Router } from '@angular/router';
import packageJson from '../../../../../../package.json'
import { MatDialog } from '@angular/material/dialog';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineEditDialog } from '../../dialogs/airline-edit/airline-edit.dialog';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsUserNameEditDialog } from '../../dialogs/user-edit/user-name-edit.dialog';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';

@Component({
    selector: 'ts-sx-navbar',
    templateUrl: './sx-navbar.component.html',
    // animations: [PageTransition]
})
export class SxNavbarComponent implements OnInit, OnDestroy {

    @Output() sidebarToggle: EventEmitter<any> = new EventEmitter<any>();
    @Output() snavToggle: EventEmitter<void> = new EventEmitter<void>();
   
    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    roots = AppRoutes;
    appVersion: string = packageJson.version;

    user: UserModel;
    airline: AmsAirline;

    userName: string;
    userChanged: Subscription;
    avatarUrl: string;
    env: string;

    constructor(
        private router: Router,
        public dialogService: MatDialog,
        private spiner: SpinnerService,
        private cus: CurrentUserService, ) { }

    ngOnInit(): void {
        this.env = environment.abreviation;
        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.airline = this.cus.airline;
        this.avatarUrl = this.cus.avatarUrl;
        this.logoImgUrl = this.cus.airline?.amsLogoUrl??COMMON_IMG_LOGO_RED;
        let name = 'Guest';
        if(this.cus.user){
            name = this.cus.user.userName?this.cus.user.userName:this.cus.user.userEmail;
        }
        this.userName = name;
        //console.log('initFields -> cus.user:', this.cus.user);
    }

    

    //#region Action Methods

    snavToggleClick():void{
        console.log('snavToggleClick -> ', this.cus.user);
        this.snavToggle.emit();
    }

    ediAirline(): void {
        const dialogRef = this.dialogService.open(AmsAirlineEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                airline: this.airline,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('ediAirline-> res:', res);
            if (res) {
                this.cus.airline = res;
            }
            this.initFields();
        });
    }

    edituser(data: UserModel) {

        const dialogRef = this.dialogService.open(AmsUserNameEditDialog, {
            width: '721px',
            //height: '520px',
            data: {
                user: data,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('edituser-> res:', res);
            if (res) {
                this.cus.user = res;
            }
            this.initFields();
        });

    }

    logout() {
        console.log('logout->');
        this.spiner.show();
        this.cus.logout().subscribe((res: Boolean) => {
            this.spiner.hide();
            this.router.navigate([AppRoutes.Root, AppRoutes.public])
        });
    }

    //#endregion
}
