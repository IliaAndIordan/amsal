import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription, tap } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAircraft, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight, AmsFlightLog, AmsFlightTableCriteria } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { IAmsFlightsEstimateDto } from '../../dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { GridAlFlightsQueueDataSource } from '../grid-al-flights-queue/grid-al-flights-queue.datasource';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';

@Component({
  selector: 'ams-grid-ac-flight-log-list',
  templateUrl: './grid-ac-flight-log-list.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class GridAcFlightLogListComponent implements OnInit {

  @Output() spmAircraftOpen: EventEmitter<AmsAircraft> = new EventEmitter<AmsAircraft>();

  @ViewChild(MatTable, { static: true }) table: MatTable<AmsFlight>; // initialize
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() flight: AmsFlight;

  
  aircraft: AmsAircraft;
  sortActive: string;
  sortDirection: SortDirection;

  flightLogs: AmsFlightLog[];
  dataCount = 0;

  displayedColumns = ['acfsId', 'trApId', 'amount'];
  airline: AmsAirline;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private preloader: SpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private aliService: AirlineInfoService,
    private acService: AmsAirlineAircraftService,
    private acClient: AmsAircraftClient) {

  }


  ngOnInit(): void {
    this.initFields();
  }

  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flight']) {
      this.initFields();
    }
  }


  initFields() {
    //console.log('initFields-> flight:', this.flight);
    this.airline = this.cus.airline;
    this.flightLogs = this.flight?this.flight.flightLogs:new Array<AmsFlightLog>();
    this.dataCount = this.flightLogs?this.flightLogs.length:0;
    //console.log('initFields-> flightLogs:', this.flightLogs);
  }


  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => {
      this.sortActive = this.sort.active;
      this.sortDirection = this.sort.direction;
    });
  }

  //#region Actions

  gotoflightt(flight: AmsFlight) {
    console.log('gotoflightt -> flight:' + flight);
  }

  gotoAircraft(aicraft: AmsAircraft) {
    if (this.aircraft) {
      this.acService.aircraft = this.aircraft;
      this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
  }
  }


  gotoMac(data: AmsMac) {
    console.log('gotoMac-> data:', data);
    if (data) {
      //this.acmService.acMarketMac = data;
      this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
    }

  }
  //#endregion

  //#region SPM


  spmAirportOpen(airport: AmsAirport) {
    if(airport && airport.apId){
      this.cus.spmAirportPanelOpen.next(airport.apId);
    }
  }

  spmAircraftOpenClick(acId: number) {
    //console.log('spmAircraftOpenClick -> acId:', acId);
    if (acId) {
      this.preloader.show();
      this.acClient.aircraftGet(acId)
        .subscribe((ac: AmsAircraft) => {
          //console.log('aircraftGet -> ac:', ac);
          this.preloader.hide();
          if (ac) {
            this.spmAircraftOpen.emit(ac);
          }
        });

    }
  }

  //#endregion

}
