import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridAcFlightLogListComponent } from './grid-ac-flight-log-list.component';

describe('GridAcFlightLogListComponent', () => {
  let component: GridAcFlightLogListComponent;
  let fixture: ComponentFixture<GridAcFlightLogListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAcFlightLogListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridAcFlightLogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
