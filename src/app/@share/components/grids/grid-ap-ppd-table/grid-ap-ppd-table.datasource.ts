import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightTableData } from 'src/app/@core/services/api/flight/dto';
import { AmsPaxPayloadDemand, AmsPaxPayloadDemandTableCriteria, ResponseAmsPaxPayloadDemandTableData } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';

export const KEY_CRITERIA = 'ams_ap_ppd_table_criteria';

@Injectable({ providedIn: 'root' })
export class GridAirportPpdDataSource extends DataSource<AmsPaxPayloadDemand> {

    seleted: AmsPaxPayloadDemand;

    private _data: Array<AmsPaxPayloadDemand>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsPaxPayloadDemand[]> = new BehaviorSubject<AmsPaxPayloadDemand[]>([]);
    listSubject = new BehaviorSubject<AmsPaxPayloadDemand[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsPaxPayloadDemand> {
        return this._data;
    }

    set data(value: Array<AmsPaxPayloadDemand>) {
        this._data = value;
        this.listSubject.next(this._data as AmsPaxPayloadDemand[]);
    }

    constructor(
        private cus: CurrentUserService,
        private apClient: AmsAirportClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsPaxPayloadDemandTableCriteria>();

    public get criteria(): AmsPaxPayloadDemandTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsPaxPayloadDemandTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsPaxPayloadDemandTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsPaxPayloadDemandTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'adate';
            obj.sortDesc = true;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsPaxPayloadDemandTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsPaxPayloadDemand[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsPaxPayloadDemand>> {

        this.isLoading = true;

        return new Observable<Array<AmsPaxPayloadDemand>>(subscriber => {

            this.apClient.airportPpdTable(this.criteria)
                .subscribe((res: ResponseAmsPaxPayloadDemandTableData) => {
                    const resp = res.data;
                    this.data = new Array<AmsPaxPayloadDemand>();
                    if (resp) {
                        if (resp && resp.payloads && resp.payloads.length > 0) {
                            resp.payloads.forEach(element => {
                                this.data.push(AmsPaxPayloadDemand.fromJSON(element));
                            });
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsPaxPayloadDemand>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
