import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GridAirportPpdTableComponent } from './grid-ap-ppd-table.component';


describe('GridAirportPpdTableComponent', () => {
  let component: GridAirportPpdTableComponent;
  let fixture: ComponentFixture<GridAirportPpdTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAirportPpdTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAirportPpdTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
