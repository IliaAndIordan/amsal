import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport, AmsPaxPayloadDemandTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { GridAirlineHubsDataSource } from './grid-al-hubs-table.datasource';
import { AmsHub, AmsHubTableCriteria } from 'src/app/@core/services/api/airline/al-hub';
import { AmsHubTypes } from 'src/app/@core/services/api/airline/enums';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsHubEditDialog } from '../../dialogs/al-hub-edit/al-hub-edit.dialog';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';

@Component({
    selector: 'ams-grid-al-hubs-table',
    templateUrl: './grid-al-hubs-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class GridAirlineHubsTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsHub>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Output() spmHubOpen: EventEmitter<AmsHub> = new EventEmitter<AmsHub>();

    selHub: AmsHub;

    criteria: AmsHubTableCriteria;
    criteriaChanged: Subscription;
    base = AmsHubTypes.Base;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;

    //, 'hubName'
    displayedColumns = ['hubId', 'apId', 'hubTypeId', 'hubName', 'flightRoutesrCount', 'taxPerWeek', 'discount', 'adate', 'upkeepDay'];
    airline: AmsAirline;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private alClient: AmsAirlineClient,
        private hubCache: AmsAirlineHubsCacheService,
        private apCache:AmsAirportCacheService,
        private spmMapService: MapLeafletSpmService,
        public tableds: GridAirlineHubsDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsHub>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsPaxPayloadDemandTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.loadPage();
        });

        this.initFields();
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        this.criteria.alId = this.airline?.alId;
        //console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.criteria.sortCol = 'adate';
        this.criteria.sortDesc = true;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }


    deleteHubClick(value: AmsHub): void {
        console.log('deleteHubClick -> value:', value);
        this.selHub = value;
        if (value && value.hubId && value.alId === this.cus.airline.alId) {
            this.preloader.show();
            this.alClient.hubDelete(value.hubId).then(res => {
                this.preloader.hide();
                this.hubCache.clearCache();
                this.refreshClick();
            }).catch(err => {
                this.preloader.hide();
                console.log('deleteMacCabin -> err:', err);
            });
        }
    }

    async editHubClick(value: AmsHub): Promise<void> {
        console.log('editHubClick -> value:', value);
        this.selHub = value;
        if (value && value.hubId && value.alId === this.cus.airline.alId) {
           
            const dialogRef = this.dialogService.open(AmsHubEditDialog, {
                width: '721px',
                //height: '600px',
                data: {
                    hub: this.selHub,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.selHub = res;
                    this.hubCache.clearCache();
                    this.refreshClick();
                }
                this.initFields();
            });
        }
        else {
            this.toastr.info('Please select airport first.', 'Select City');
        }

    }
    spmHubOpenClick(value: AmsHub) {
        console.log('spmHubOpenClick -> value:', value);
        this.selHub = value;
        if (value) {
            //this.spmFlOpen.emit(fl);
        }
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }

    //#endregion

}
