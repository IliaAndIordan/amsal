import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GridAirlineHubsTableComponent } from './grid-al-hubs-table.component';


describe('GridAirlineHubsTableComponent', () => {
  let component: GridAirlineHubsTableComponent;
  let fixture: ComponentFixture<GridAirlineHubsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAirlineHubsTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAirlineHubsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
