import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightTableData } from 'src/app/@core/services/api/flight/dto';
import { AmsPaxPayloadDemand, AmsPaxPayloadDemandTableCriteria, ResponseAmsPaxPayloadDemandTableData } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { AmsHub, AmsHubTableCriteria, ResponceAmsHubTableData } from 'src/app/@core/services/api/airline/al-hub';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';

export const KEY_CRITERIA = 'ams_al_hubs_table_criteria';

@Injectable({ providedIn: 'root' })
export class GridAirlineHubsDataSource extends DataSource<AmsHub> {

    seleted: AmsHub;

    private _data: Array<AmsHub>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsHub[]> = new BehaviorSubject<AmsHub[]>([]);
    listSubject = new BehaviorSubject<AmsHub[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsHub> {
        return this._data;
    }

    set data(value: Array<AmsHub>) {
        this._data = value;
        this.listSubject.next(this._data as AmsHub[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsHubTableCriteria>();

    public get criteria(): AmsHubTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsHubTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsHubTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsHubTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'adate';
            obj.sortDesc = true;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsHubTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsHub[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsHub>> {

        this.isLoading = true;

        return new Observable<Array<AmsHub>>(subscriber => {

            this.alClient.airlineHubsTable(this.criteria)
                .subscribe((resp: ResponceAmsHubTableData) => {
                    //console.log('airlineHubsTable -> resp=', resp);
                    this.data = new Array<AmsHub>();
                    if (resp) {
                        if (resp && resp.hubs && resp.hubs.length > 0) {
                            this.data = resp.hubs;
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsHub>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
