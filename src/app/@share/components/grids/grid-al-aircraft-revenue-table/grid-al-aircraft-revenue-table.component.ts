import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AircraftStatus, AircraftActionType } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMacCabin, AmsMac, AmsAircraftStatDay, AmsAircraftStatDayTableCriteria } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport, AmsCargoPayloadDemand, AmsCargoPayloadDemandTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightEstimate } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { IAmsFlightsEstimateDto, AmsFlightCharterEstimateDialog } from '../../dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { AmsRevenue, AmsRevenueTableCriteria } from 'src/app/@core/services/api/airline/transaction';
import { GridAlAircraftRevenueDataSource } from './grid-al-aircraft-revenue-table.datasource';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';

@Component({
    selector: 'ams-grid-al-aircraft-revenue-table',
    templateUrl: './grid-al-aircraft-revenue-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class GridAlAircraftRevenueTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsAircraftStatDay>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() alId: number;
    @Input() acId: number;
    @Output() dataChangedEv: EventEmitter<AmsAircraftStatDay[]> = new EventEmitter<AmsAircraftStatDay[]>();

    //@Output() charterEstimate: EventEmitter<IAmsFlightsEstimateDto> = new EventEmitter<IAmsFlightsEstimateDto>();

    selDto: AmsRevenue;



    criteria: AmsAircraftStatDayTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 14;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;

    //
    displayedColumns = ['id', 'stdate', 'income', 'expences', 'revenue'];
    airline: AmsAirline;
    aircraft: AmsAircraft;
    aircraft$: Observable<AmsAircraft>;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private acCache: AmsAirlineAircraftCacheService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private spmMapService: MapLeafletSpmService,
        public tableds: GridAlAircraftRevenueDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsAircraftStatDay>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
            this.dataChangedEv.emit(this.tableds.data);
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsAircraftStatDayTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.loadPage();
        });
       
        this.initFields();
        this.criteria.offset = 0;
        this.criteria.alId = this.aircraft ? this.aircraft.ownerAlId : undefined;
        this.criteria.acId = this.acId;
        this.tableds.criteria = this.criteria;
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['acId']) {
           
            this.initFields();
            if (this.criteria.acId !== this.acId) {
                //console.log('ngOnChanges -> acId', this.acId);
                this.criteria.pageIndex = 1;
                this.criteria.offset = 0;
                this.aircraft$ = this.acCache.getAircraft(this.acId);
                this.aircraft$.subscribe(ac => {
                    this.aircraft = ac;
                    this.initFields();
                    this.criteria.acId = this.aircraft?this.aircraft.acId:this.acId;
                    this.criteria.alId = this.aircraft ? this.aircraft.ownerAlId : undefined;
                    this.tableds.criteria = this.criteria;
                });
            }
            

        }

    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;

        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        //console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.criteria.sortCol = 'id';
        this.criteria.sortDesc = true;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    spmPpdOpenClick(value: AmsRevenue) {
        console.log('spmPpdOpenClick -> value:', value);
        this.selDto = value;
        if (value) {
            //this.spmFlOpen.emit(fl);
        }
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }

    //#endregion

}
