import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAircraftStatDay, AmsAircraftStatDayData, AmsAircraftStatDayTableCriteria } from 'src/app/@core/services/api/aircraft/dto';

export const KEY_CRITERIA = 'ams-grid-al-ac-revenue-table-criteria';

@Injectable({ providedIn: 'root' })
export class GridAlAircraftRevenueDataSource extends DataSource<AmsAircraftStatDay> {

    seleted: AmsAircraftStatDay;

    private _data: Array<AmsAircraftStatDay>;
    private _isLoading = false;

    numberOfPages: number = 0;
    itemsCount: number = 0;
    totalRevenue: number = 0

    dataChange: BehaviorSubject<AmsAircraftStatDay[]> = new BehaviorSubject<AmsAircraftStatDay[]>([]);
    listSubject = new BehaviorSubject<AmsAircraftStatDay[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsAircraftStatDay> {
        return this._data;
    }

    set data(value: Array<AmsAircraftStatDay>) {
        this._data = value;
        this.listSubject.next(this._data as AmsAircraftStatDay[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private acClient: AmsAircraftClient) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsAircraftStatDayTableCriteria>();

    public get criteria(): AmsAircraftStatDayTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsAircraftStatDayTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsAircraftStatDayTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsAircraftStatDayTableCriteria();
            obj.limit = 14;
            obj.offset = 0;
            obj.sortCol = 'id';
            obj.sortDesc = true;
            obj.alId = this.cus.airline.alId;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsAircraftStatDayTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsAircraftStatDay[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsAircraftStatDay>> {

        this.isLoading = true;

        return new Observable<Array<AmsAircraftStatDay>>(subscriber => {

            this.acClient.getAcStatDayTableP(this.criteria)
                .then((resp: AmsAircraftStatDayData) => {
                    //console.log('airlineRevenuesTable -> resp=', resp);
                    this.data = resp && resp.rows ? resp.rows : new Array<AmsAircraftStatDay>();
                    this.itemsCount = resp && resp.rowsCount ? resp.rowsCount : this.data ? this.data.length : 0;
                    this.totalRevenue = resp && resp.totalRevenue ? resp.totalRevenue : 0;
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsAircraftStatDay>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
