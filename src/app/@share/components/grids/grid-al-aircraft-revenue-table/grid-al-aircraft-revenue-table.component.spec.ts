import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GridAlAircraftRevenueTableComponent } from './grid-al-aircraft-revenue-table.component';


describe('GridAlAircraftRevenueTableComponent', () => {
  let component: GridAlAircraftRevenueTableComponent;
  let fixture: ComponentFixture<GridAlAircraftRevenueTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAlAircraftRevenueTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAlAircraftRevenueTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
