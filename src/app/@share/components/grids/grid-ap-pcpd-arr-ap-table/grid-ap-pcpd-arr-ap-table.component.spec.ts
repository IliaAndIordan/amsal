import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GridAirportPpdByArrApTableComponent } from './grid-ap-pcpd-arr-ap-table.component';


describe('Share -> GRIDS - GridAirportPpdByArrApTableComponent', () => {
  let component: GridAirportPpdByArrApTableComponent;
  let fixture: ComponentFixture<GridAirportPpdByArrApTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAirportPpdByArrApTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAirportPpdByArrApTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
