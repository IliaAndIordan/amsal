import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import {  AmsPcpdByArrAirport, AmsPcpdByArrAirportTableCriteria, AmsPcpdByArrAirportTableData } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';

export const KEY_CRITERIA = 'ams-grid-ap-pcpd-arr-ap-criteria';

@Injectable({ providedIn: 'root' })
export class GridAirportPcpdArrApDataSource extends DataSource<AmsPcpdByArrAirport> {

    seleted: AmsPcpdByArrAirport;

    private _data: Array<AmsPcpdByArrAirport>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsPcpdByArrAirport[]> = new BehaviorSubject<AmsPcpdByArrAirport[]>([]);
    listSubject = new BehaviorSubject<AmsPcpdByArrAirport[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsPcpdByArrAirport> {
        return this._data;
    }

    set data(value: Array<AmsPcpdByArrAirport>) {
        this._data = value;
        this.listSubject.next(this._data as AmsPcpdByArrAirport[]);
    }

    constructor(
        private cus: CurrentUserService,
        private apClient: AmsAirportClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsPcpdByArrAirportTableCriteria>();

    public get criteria(): AmsPcpdByArrAirportTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsPcpdByArrAirportTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsPcpdByArrAirportTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsPcpdByArrAirportTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = undefined;
            obj.sortDesc = undefined;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsPcpdByArrAirportTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsPcpdByArrAirport[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsPcpdByArrAirport>> {

        this.isLoading = true;

        return new Observable<Array<AmsPcpdByArrAirport>>(subscriber => {

            this.apClient.pcpdByArrAirportTable(this.criteria)
                .subscribe((resp: AmsPcpdByArrAirportTableData) => {
                    //console.log('pcpdByArrAirportTable -> resp=', resp);
                    this.data = new Array<AmsPcpdByArrAirport>();
                    if (resp) {
                        if (resp && resp.payloads && resp.payloads.length > 0) {
                            resp.payloads.forEach(element => {
                                this.data.push(AmsPcpdByArrAirport.fromJSON(element));
                            });
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsPcpdByArrAirport>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
