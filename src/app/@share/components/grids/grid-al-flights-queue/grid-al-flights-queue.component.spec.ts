import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridAlFlightsQueueComponent } from './grid-al-flights-queue.component';

describe('GridAlFlightsQueueComponent', () => {
  let component: GridAlFlightsQueueComponent;
  let fixture: ComponentFixture<GridAlFlightsQueueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAlFlightsQueueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAlFlightsQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
