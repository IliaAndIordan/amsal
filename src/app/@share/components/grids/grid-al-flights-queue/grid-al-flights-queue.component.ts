import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, OnDestroy, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight, AmsFlightTableCriteria } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { IAmsFlightsEstimateDto } from '../../dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { GridAlFlightsQueueDataSource } from './grid-al-flights-queue.datasource';
import { AirlineService } from 'src/app/airline/airline.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';

@Component({
    selector: 'ams-grid-al-flights-queue',
    templateUrl: './grid-al-flights-queue.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridAlFlightsQueueComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsFlight>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Output() charterEstimate: EventEmitter<IAmsFlightsEstimateDto> = new EventEmitter<IAmsFlightsEstimateDto>();
    @Output() spmAircraftOpen: EventEmitter<AmsAircraft> = new EventEmitter<AmsAircraft>();
    @Output() spmFlqOpen: EventEmitter<AmsFlight> = new EventEmitter<AmsFlight>();


    selFlight: AmsFlight;
    selaircraft: AmsAircraft;
    airport: AmsAirport;


    criteria: AmsFlightTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 4;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;
    interval: any;
    //
    displayedColumns = ['flId', 'acId', 'payloads', 'etdTime', 'distanceKm', 'remainTimeMin'];
    airline: AmsAirline;
    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private alService: AirlineService,
        private spmMapService: MapLeafletSpmService,
        public tableds: GridAlFlightsQueueDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsFlight>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsFlightTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.loadPage();
        });
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.interval = setInterval(() => {
            this.loadPage();
        }, 60_000);

        this.initFields();
        this.criteria.offset = 0;
        this.criteria.limit = this.pageSize;
        this.tableds.criteria = this.criteria;
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.interval) { clearInterval(this.interval); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        //this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 800);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = 'dtime';
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    gotoflightt(flight: AmsFlight) {
        console.log('gotoflightt -> flight:' + flight);
    }

    gotoAircraft(aicraft: AmsAircraft) {
        if (aicraft) {
            this.alService.aircraftCfg = aicraft;
            this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
        }
    }


    refreshClick() {
        this.loadPage();
    }


    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);
        if (data) {
            //this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }
    //#endregion

    //#region SPM

    spmFlqOpenClick(value: AmsFlight) {
        if (value && value.flId) {
            this.selFlight = value;
            this.cus.spmFlightQueuePanelOpen.next(value.flId);
        }
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }

    spmAircraftOpenClick(acId: number) {
        //console.log('spmAircraftOpenClick -> acId:', acId);
        if (acId) {
            this.cus.spmAircraftPanelOpen.next(acId);
        }
    }

    spmMapOpen() {
        this.spmMapService.flights = this.tableds.data;
        this.spmMapService.spmMapPanelOpen.next(true);
    }
    //#endregion

    //#region Data

    /*
    gotoAirport(data: AmsAirport) {
        
  
        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }
  
    }*/

    //#endregion
}
