import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsAircraft, AmsAircraftTableCriteria, ResponseAmsAircraftTable } from 'src/app/@core/services/api/aircraft/dto';

export const KEY_CRITERIA = 'ams_grid_al_aircraftsCommon_criteria';
@Injectable({providedIn:'root'})
export class AmsGridAlAircraftTableDataSource extends DataSource<AmsAircraft> {

    seleted: AmsAircraft;

    private _data: Array<AmsAircraft>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsAircraft[]> = new BehaviorSubject<AmsAircraft[]>([]);
    listSubject = new BehaviorSubject<AmsAircraft[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsAircraft> {
        return this._data;
    }

    set data(value: Array<AmsAircraft>) {
        this._data = value;
        this.listSubject.next(this._data as AmsAircraft[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsAircraftClient) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsAircraftTableCriteria>();

    public get criteria(): AmsAircraftTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsAircraftTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsAircraftTableCriteria(), JSON.parse(onjStr));
            if(!obj.ownerAlId || obj.ownerAlId !== this.cus.airline.alId){
                obj.ownerAlId = this.cus.airline.alId;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }
            
        } else {
            obj = new AmsAircraftTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'acId';
            obj.sortDesc = true;
            obj.newAc = undefined;
            obj.ownerAlId = this.cus.airline.alId;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsAircraftTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsAircraft[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsAircraft>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<AmsAircraft>>(subscriber => {

            this.client.aircraftTable(this.criteria)
                .subscribe((resp: ResponseAmsAircraftTable) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsAircraft>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.aircrafts && resp.data.aircrafts.length > 0) {
                            resp.data.aircrafts.forEach(iu => {
                                const obj = AmsAircraft.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsAircraft>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion

   
}
