import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef, Output, EventEmitter, OnChanges, Input } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// --- Models
import { AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS, URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { RwTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMac, AmsMacCabin, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMacCabinEditDialog } from 'src/app/@share/components/dialogs/mac-cabin-edit/mac-cabin-edit.dialog';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsFlightEstimate } from 'src/app/@core/services/api/flight/dto';
import { AircraftActionType, AircraftStatus } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsFlightCharterEstimateDialog, IAmsFlightsEstimateDto } from 'src/app/@share/components/dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { AmsAcmpEditDialog } from 'src/app/@share/components/dialogs/acmp-create/acmp-edit.dialog';
import { IInfoMessage, InfoMessageDialog } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { AmsGridAlAircraftTableDataSource } from './grid-al-aircrafts.datasource';
import { AirlineService } from 'src/app/airline/airline.service';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';

@Component({
    selector: 'ams-grid-al-aircraft-common',
    templateUrl: './grid-al-aircrafts.component.html',
})
export class AmsGridAlAircraftComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {

    @Input() tabIdx:number;
    @Input() selTabIdx:number;

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsState>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Output() charterEstimate: EventEmitter<IAmsFlightsEstimateDto> = new EventEmitter<IAmsFlightsEstimateDto>();

    seatsFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_f.png';
    seatsBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_b.png';
    seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_e.png';
    typeOpt = RwTypeOpt;
    opMnt = AircraftActionType.MaintenanceCheck;
    selaircraft: AmsAircraft;
    airport: AmsAirport;
    manufacturer: AmsManufacturer;

    mac: AmsMac;
    macChanged: Subscription;

    criteria: AmsAircraftTableCriteria;
    criteriaChanged: Subscription;
    acatPlaneStand = AircraftActionType.PlaneStand;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;

    pageIndex = 0;
    pageSize = 12;
    sortActive: string = 'id';
    sortDirection: SortDirection = 'asc';


    dataCount = 0;
    dataChanged: Subscription;
    //'homeApId', 'homeAp',  'distanceKm', 'state', 'price'
    displayedColumns = ['action', 'image', 'acId', 'registration', 'currApId', 'flightHours', 'distanceKm', 'acStatusId', 'state', 'price', 'cabinId'];
    canEdit: boolean;
    airline: AmsAirline;
    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private alService: AirlineService,
        private flClient: AmsFlightClient,
        public tableds: AmsGridAlAircraftTableDataSource) {

    }


    ngOnInit(): void {
        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsAircraft>) => {
            //this.dataCount = this.tableds.itemsCount;
            //this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsAircraftTableCriteria) => {
            this.criteria = this.tableds.criteria;
            this.initFields();
            this.loadPage();
        });

        this.initFields();

        this.criteria.offset = 0;
        this.criteria.ownerAlId = this.cus.airline.alId;
        this.tableds.criteria = this.criteria;
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['selTabIdx']) {
           //this.mac = changes['mac'].currentValue;
            this.initFields();
            if(this.tabIdx === this.selTabIdx){
                this.criteria.ownerAlId = this.cus.airline.alId;
                this.tableds.criteria = this.criteria;
            }
        }
        
    }

    ngOnDestroy(): void {
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.macChanged) { this.macChanged.unsubscribe(); }
    }

    initFields() {
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        const tq = '?m=' + new Date().getTime();
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 800);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = 'acId';
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    togleIsforschedule(value: AmsAircraft): void {
        console.log('togleIsforschedule -> aircraft:', value);
        if (value && this.cus.airline?.alId === value.ownerAlId) {
            if (value.flInQueue > 0) {
                this.cus.showMessage({ title: 'Set for Schedule ', message: `The arcraft has ${value.flInQueue} flights in queue.</br>Can not execute this action. Please, wait for flights to compleate.` }).then(res => {
                    return;
                });
            } else if (value.grpId > 0) {
                this.cus.showMessage({ title: 'Set for Schedule ', message: `The arcraft is assignet to shedule group #${value.grpId}.</br>Can not execute this action. Please, edit group first and wait for flights to compleate.` }).then(res => {
                    return;
                });
            } else {
                value.forSheduleFlights = value.forSheduleFlights ? false : true;
                this.preloader.show();
                this.client.aircraftSave(value)
                    .then(res => {
                        this.preloader.hide();
                        console.log('aircraftSave -> res:', res);
                        this.refreshClick();
                    }, msg => {
                        console.log('aircraftSave -> ' + msg);
                        this.preloader.hide();
                        this.refreshClick();
                    });
            }
        }
    }

    gotoAircraft(aicraft: AmsAircraft) {
        if (aicraft) {
            this.alService.aircraftCfg = aicraft;
            this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
        }
    }

    buyAircraft(aicraft: AmsAircraft): void {
        if (aicraft && this.cus.airline) {
            this.preloader.show();
            this.client.acBuy(aicraft.acId, this.cus.airline.alId)
                .subscribe(res => {
                    this.preloader.hide();
                    console.log('buyAircraft -> res:', res);
                    this.refreshClick();
                }, msg => {
                    console.log('loadData -> ' + msg);
                    this.preloader.hide();
                    this.refreshClick();
                });
        }
    }

    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }

    spmAircraftOpen(value: AmsAircraft) {
        if (value) {
            this.cus.spmAircraftPanelOpen.next(value.acId);
        }
    }


    ediMacCabin(data: AmsMacCabin) {
        console.log('ediMacCabin-> data:', data);
        //console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            const dialogRef = this.dialogService.open(AmsMacCabinEditDialog, {
                width: '721px',
                height: '620px',
                data: {
                    cabin: data,
                    //mac: this.acmService.acMarketMac
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediMac-> res:', res);
                if (res) {
                    //this.table.renderRows();
                    this.tableds.loadData()
                        .subscribe(res => {
                            this.initFields();
                        });
                }
                else {
                    this.initFields();
                }


            });

        }
        else {
            this.toastr.info('Please select manufacturer acrcraft model first.', 'Edit Cabin Configuration');
        }


    }

    addCabin() {
        // console.log('inviteUser-> ');
        let dto = new AmsMacCabin();
        //dto.macId = this.acmService.acMarketMac.macId;
        this.ediMacCabin(dto);
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);

        if (data) {
            //this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    //#region Charter Flights

    acCharterFlights: AmsFlightEstimate[];

    estimateCharterFlights(aicraft: AmsAircraft): Observable<AmsFlightEstimate[]> {

        return new Observable<AmsFlightEstimate[]>(subscriber => {

            this.flClient.flightCharterEstimate(aicraft.acId, aicraft.currApId)
                .subscribe((resp: AmsFlightEstimate[]) => {
                    console.log('estimateCharterFlights-> resp', resp);
                    subscriber.next(resp);
                },
                    err => {
                        throw err;
                    });
        });

    }

    viewCharterFlights(aicraft: AmsAircraft) {
        if (aicraft && aicraft.acStatusId == AircraftStatus.Operational &&
            aicraft.acActionTypeId == AircraftActionType.PlaneStand) {
            this.preloader.show();
            console.log('viewCharterFlights-> aicraft', aicraft);
            this.estimateCharterFlights(aicraft)
                .subscribe((resp: AmsFlightEstimate[]) => {
                    console.log('loadAirport-> resp', resp);
                    this.acCharterFlights = resp;
                    const data = {
                        flights: this.acCharterFlights,
                        airline: this.airline,
                        aircraft: aicraft,
                    };
                    this.preloader.hide();
                    this.FlightsCharterEstimateDialog(data);

                },
                    err => {
                        this.preloader.hide();
                        throw err;
                    });
        } else {

        }
    }

    FlightsCharterEstimateDialog(dto: IAmsFlightsEstimateDto) {
        const dialogRef = this.dialogService.open(AmsFlightCharterEstimateDialog, {
            /*width: '921px',
            height: '520px',*/
            data: {
                flights: dto.flights,
                airline: dto.airline,
                aircraft: dto.aircraft,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('FlightsCharterEstimateDialog-> res:', res);
            if (res && res > -1) {
                this.loadPage();
            }
            this.initFields();
        });
    }

    //#endregion

    setMnt(aircraft: AmsAircraft) {
        //if (aircraft && aircraft.acStatusId == AircraftStatus.Operational &&
        //    aircraft.acActionTypeId == AircraftActionType.PlaneStand){
        const dialogRef = this.dialogService.open(AmsAcmpEditDialog, {
            width: '921px',
            height: '520px',
            data: {
                aircraft: aircraft,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            //console.log('pendingMnt-> res:', res);
            if (res && res > -1) {
                this.loadPage();
            }
            this.initFields();
        });
    }

    showMessage(data: IInfoMessage): Promise<any> {
        let promise = new Promise<AmsAircraft>((resolve, reject) => {
            const dialogRef = this.dialogService.open(InfoMessageDialog, {
                width: '721px',
                height: '320px',
                data: data
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('showMessage-> res:', res);
                if (res) {
                    resolve(res);
                }
            });
        });
        return promise;

    }

}

