import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsCargoPayloadDemand, AmsCargoPayloadDemandTableCriteria, ResponseAmsCargoPayloadDemandTableData, ResponseAmsPaxPayloadDemandTableData } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { AmsRevenue, AmsRevenueTableCriteria, ResponceAmsRevenueTableData } from 'src/app/@core/services/api/airline/transaction';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';

export const KEY_CRITERIA = 'ams-grid-al-revenue-table-criteria';

@Injectable({ providedIn: 'root' })
export class GridAirlineRevenueDataSource extends DataSource<AmsRevenue> {

    seleted: AmsRevenue;

    private _data: Array<AmsRevenue>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsRevenue[]> = new BehaviorSubject<AmsRevenue[]>([]);
    listSubject = new BehaviorSubject<AmsRevenue[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsRevenue> {
        return this._data;
    }

    set data(value: Array<AmsRevenue>) {
        this._data = value;
        this.listSubject.next(this._data as AmsRevenue[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsRevenueTableCriteria>();

    public get criteria(): AmsRevenueTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsRevenueTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsRevenueTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsRevenueTableCriteria();
            obj.limit = 14;
            obj.offset = 0;
            obj.sortCol = 'rdId';
            obj.sortDesc = true;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsRevenueTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsRevenue[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsRevenue>> {

        this.isLoading = true;

        return new Observable<Array<AmsRevenue>>(subscriber => {

            this.alClient.airlineRevenuesTable(this.criteria)
                .subscribe((resp: ResponceAmsRevenueTableData) => {
                    //console.log('airlineRevenuesTable -> resp=', resp);
                    this.data = resp && resp.revenues?resp.revenues: new Array<AmsRevenue>();
                    this.itemsCount = resp && resp.rowsCount ? resp.rowsCount :  this.data?this.data.length:0;
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsRevenue>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
