import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AircraftStatus, AircraftActionType } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMacCabin, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport, AmsCargoPayloadDemand, AmsCargoPayloadDemandTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightEstimate } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { IAmsFlightsEstimateDto, AmsFlightCharterEstimateDialog } from '../../dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { GridAirlineRevenueDataSource } from './grid-al-revenue-table.datasource';
import { AmsRevenue, AmsRevenueTableCriteria } from 'src/app/@core/services/api/airline/transaction';

@Component({
    selector: 'ams-grid-al-revenue-table',
    templateUrl: './grid-al-revenue-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class GridAirlineRevenueTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsRevenue>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() alId: number;
    @Output() dataChangedEv: EventEmitter<AmsRevenue[]> = new EventEmitter<AmsRevenue[]>();

    //@Output() charterEstimate: EventEmitter<IAmsFlightsEstimateDto> = new EventEmitter<IAmsFlightsEstimateDto>();

    selDto: AmsRevenue;
    


    criteria: AmsRevenueTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 14;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;
    
    //
    displayedColumns = ['rdId', 'rdDate', 'income', 'expences', 'revenue'];
    airline: AmsAirline;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private spmMapService: MapLeafletSpmService,
        public tableds: GridAirlineRevenueDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsRevenue>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
            this.dataChangedEv.emit(this.tableds.data);
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsRevenueTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.loadPage();
        });

        this.initFields();
        this.criteria.offset = 0;
        this.criteria.alId = this.alId?this.alId:this.cus.airline.alId;
        this.tableds.criteria = this.criteria;
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['alId']) {
          this.initFields();
          this.criteria.alId = this.alId?this.alId:this.cus.airline.alId;
          this.tableds.criteria = this.criteria;
        }
    
      }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;

        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        //console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.criteria.sortCol = 'rdId';
        this.criteria.sortDesc = true;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    spmPpdOpenClick(value: AmsRevenue) {
        console.log('spmPpdOpenClick -> value:', value);
        this.selDto = value;
        if (value) {
            //this.spmFlOpen.emit(fl);
        }
    }

    spmAirportOpen(airport: AmsAirport) {
        if(airport && airport.apId){
            this.cus.spmAirportPanelOpen.next(airport.apId);
          }
    }

    //#endregion

}
