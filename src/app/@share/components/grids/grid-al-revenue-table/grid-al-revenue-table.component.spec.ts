import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GridAirlineRevenueTableComponent } from './grid-al-revenue-table.component';


describe('GridAirlineRevenueTableComponent', () => {
  let component: GridAirlineRevenueTableComponent;
  let fixture: ComponentFixture<GridAirlineRevenueTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAirlineRevenueTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAirlineRevenueTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
