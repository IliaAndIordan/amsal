import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../material.module';
import { SxComponentsCardsModule } from '../cards/components-cards.module';
import { SxComponentsCommonModule } from '../common/components-common.module';
import { AmsChartsModule } from '../../charts/ams-charts.module';
import { GridAlFlightsTableComponent } from './grid-al-flights-table/grid-al-flights-table.component';
import { GridAlFlightsDataSource } from './grid-al-flights-table/grid-al-flights-table.datasource';
import { GridAlFlightsQueueComponent } from './grid-al-flights-queue/grid-al-flights-queue.component';
import { GridAlFlightsQueueDataSource } from './grid-al-flights-queue/grid-al-flights-queue.datasource';
import { GridAcFlightLogListComponent } from './grid-ac-flight-log-list/grid-ac-flight-log-list.component';
import { GridAirportPpdTableComponent } from './grid-ap-ppd-table/grid-ap-ppd-table.component';
import { GridAirportPpdDataSource } from './grid-ap-ppd-table/grid-ap-ppd-table.datasource';
import { GridAirportCpdDataSource } from './grid-ap-cpd-table/grid-ap-cpd-table.datasource';
import { GridAirportCpdTableComponent } from './grid-ap-cpd-table/grid-ap-cpd-table.component';
import { GridAirlineRevenueDataSource } from './grid-al-revenue-table/grid-al-revenue-table.datasource';
import { GridAirlineRevenueTableComponent } from './grid-al-revenue-table/grid-al-revenue-table.component';
import { GridAirlineHubsDataSource } from './grid-al-hubs-table/grid-al-hubs-table.datasource';
import { GridAirlineHubsTableComponent } from './grid-al-hubs-table/grid-al-hubs-table.component';
import { GridAirportPcpdArrApDataSource } from './grid-ap-pcpd-arr-ap-table/grid-ap-pcpd-arr-ap-table.datasource';
import { GridAirportPpdByArrApTableComponent } from './grid-ap-pcpd-arr-ap-table/grid-ap-pcpd-arr-ap-table.component';
import { GridAlAircraftRevenueTableComponent } from './grid-al-aircraft-revenue-table/grid-al-aircraft-revenue-table.component';
import { GridAirlineTransactionsNoFlightsDataSource } from './grid-al-transactions-no-flight/grid-al-transactions-no-flight.datasource';
import { AmsGridAlTransactionsNoFlightComponent } from './grid-al-transactions-no-flight/grid-al-transactions-no-flight.component';
import { AmsGridAlFlightBookingDataSource } from './grid-al-flp-booking/grid-al-flp-booking.datasource';
import { AmsGridAlFlightBookingComponent } from './grid-al-flp-booking/grid-al-flp-booking.component';
import { AmsGridAlAircraftComponent } from './grid-al-aircrafts/grid-al-aircrafts.component';



@NgModule({
  imports: [
    CommonModule,
    SxPipesModule,
    SxMaterialModule,
    AmsChartsModule,
    //
    SxComponentsCommonModule,
    SxComponentsCardsModule,
  ],
  declarations: [
    GridAlFlightsTableComponent,
    GridAlFlightsQueueComponent,
    GridAcFlightLogListComponent,
    GridAirportPpdTableComponent,
    GridAirportCpdTableComponent,
    GridAirlineRevenueTableComponent,
    GridAirlineHubsTableComponent,
    GridAirportPpdByArrApTableComponent,
    GridAlAircraftRevenueTableComponent,
    AmsGridAlTransactionsNoFlightComponent,
    AmsGridAlFlightBookingComponent,
    AmsGridAlAircraftComponent,
  ],
  exports: [
    GridAlFlightsTableComponent,
    GridAlFlightsQueueComponent,
    GridAcFlightLogListComponent,
    GridAirportPpdTableComponent,
    GridAirportCpdTableComponent,
    GridAirlineRevenueTableComponent,
    GridAirlineHubsTableComponent,
    GridAirportPpdByArrApTableComponent,
    GridAlAircraftRevenueTableComponent,
    AmsGridAlTransactionsNoFlightComponent,
    AmsGridAlFlightBookingComponent,
    AmsGridAlAircraftComponent,
  ],
  /*
  providers:[
    GridAlFlightsDataSource,
    GridAlFlightsQueueDataSource,
    GridAirportPpdDataSource,
    GridAirportCpdDataSource,
    GridAirlineRevenueDataSource,
    GridAirlineHubsDataSource,
    GridAirportPcpdArrApDataSource,
    GridAirlineTransactionsNoFlightsDataSource,
    AmsGridAlFlightBookingDataSource,
  ]*/
})
export class AmsCommonGridModule { }
