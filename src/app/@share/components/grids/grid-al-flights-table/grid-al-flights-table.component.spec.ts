import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridAlFlightsTableComponent } from './grid-al-flights-table.component';

describe('GridAlFlightsTableComponent', () => {
  let component: GridAlFlightsTableComponent;
  let fixture: ComponentFixture<GridAlFlightsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAlFlightsTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAlFlightsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
