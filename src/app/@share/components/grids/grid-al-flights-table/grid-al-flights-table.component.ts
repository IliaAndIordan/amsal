import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output, ViewChild, OnDestroy, SimpleChanges, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AircraftStatus, AircraftActionType } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMacCabin, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightEstimate } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { IAmsFlightsEstimateDto, AmsFlightCharterEstimateDialog } from '../../dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { AmsMacCabinEditDialog } from '../../dialogs/mac-cabin-edit/mac-cabin-edit.dialog';
import { GridAlFlightsDataSource } from './grid-al-flights-table.datasource';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';

@Component({
    selector: 'ams-grid-al-flights-table',
    templateUrl: './grid-al-flights-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class GridAlFlightsTableComponent implements OnInit, OnDestroy, OnChanges {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsFlight>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Output() charterEstimate: EventEmitter<IAmsFlightsEstimateDto> = new EventEmitter<IAmsFlightsEstimateDto>();
    @Output() spmAircraftOpen: EventEmitter<AmsAircraft> = new EventEmitter<AmsAircraft>();
    @Output() spmFlOpen: EventEmitter<AmsFlight> = new EventEmitter<AmsFlight>();


    selFlight: AmsFlight;
    selaircraft: AmsAircraft;
    airport: AmsAirport;


    criteria: AmsAircraftTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 4;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;
    interval: any;
    //
    displayedColumns = ['flId', 'acId', 'payloads', 'remainTimeMin', 'acfsId'];
    airline: AmsAirline;
    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private spmMapService: MapLeafletSpmService,
        private acService: AmsAirlineAircraftService,
        public tableds: GridAlFlightsDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsFlight>) => {
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsFlightTableCriteria) => {
            this.criteria = this.tableds.criteria;
            this.loadPage();
        });
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.interval = setInterval(() => {
            this.loadPage();
        }, 60_000);

        this.initFields();
        this.criteria.offset = 0;
        this.criteria.limit = this.pageSize;
        this.tableds.criteria = this.criteria;
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.interval) { clearInterval(this.interval); }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['tabindex']) {
            this.initFields();
            this.tableds.criteria = this.criteria;
        } else if (changes['acId']) {
            this.initFields();
            this.tableds.criteria = this.criteria;
        }
    }

    initFields() {
        this.airline = this.cus.airline;
        this.dataCount = this.tableds.itemsCount;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        //this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
    }

    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 500);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = 'atime';
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    gotoflightt(flight: AmsFlight) {
        console.log('gotoflightt -> flight:' + flight);
    }

    gotoAircraft(aicraft: AmsAircraft) {
        if (aicraft) {
            this.acService.aircraft = aicraft;
            this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
        }
    }


    refreshClick() {
        this.loadPage();
    }


    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);
        if (data) {
            //this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    spmFlOpenClick(fl: AmsFlight) {
        console.log('spmFlOpenClick -> fl:', fl);
        this.selFlight = fl;
        if (fl) {
            this.spmFlOpen.emit(fl);
        }
    }

    spmAircraftOpenClick(acId: number) {
        if (acId) {
            this.preloader.show();
            this.acClient.aircraftGet(acId)
                .subscribe((ac: AmsAircraft) => {
                    this.preloader.hide();
                    if (ac) {
                        this.spmAircraftOpen.emit(ac);
                    }
                });

        }
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }

    spmMapOpen() {
        this.spmMapService.flights = this.tableds.data;
        this.spmMapService.spmMapPanelOpen.next(true);
    }
    //#endregion

}
