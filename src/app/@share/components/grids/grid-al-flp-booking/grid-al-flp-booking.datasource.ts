import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsFlightPlanPayload, AmsFlightPlanPayloadTableCriteria, AmsFlightPlanPayloadTableData } from 'src/app/@core/services/api/airline/al-flp-shedule';

export const KEY_CRITERIA = 'ams_ms-grid-al-flp-booking-v01';

@Injectable({ providedIn: 'root' })
export class AmsGridAlFlightBookingDataSource extends DataSource<AmsFlightPlanPayload> {

    seleted: AmsFlightPlanPayload;

    private _data: Array<AmsFlightPlanPayload>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange = new Subject<AmsFlightPlanPayload[]>();
    listSubject = new BehaviorSubject<AmsFlightPlanPayload[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsFlightPlanPayload> {
        return this._data;
    }

    set data(value: Array<AmsFlightPlanPayload>) {
        this._data = value;
        this.listSubject.next(this._data as AmsFlightPlanPayload[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsFlightPlanPayloadTableCriteria>();

    public get criteria(): AmsFlightPlanPayloadTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsFlightPlanPayloadTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsFlightPlanPayloadTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsFlightPlanPayloadTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = undefined;
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsFlightPlanPayloadTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsFlightPlanPayload[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsFlightPlanPayload>> {

        this.isLoading = true;

        return new Observable<Array<AmsFlightPlanPayload>>(subscriber => {

            this.alClient.alFlightPlanPayloadTableP(this.criteria)
                .then((resp: AmsFlightPlanPayloadTableData) => {
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsFlightPlanPayload>();
                    if (resp) {
                        if (resp && resp.flightplans && resp.flightplans.length > 0) {
                            this.data = resp.flightplans;
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsFlightPlanPayload>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
