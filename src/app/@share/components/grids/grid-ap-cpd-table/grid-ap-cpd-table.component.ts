import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport, AmsCargoPayloadDemand, AmsCargoPayloadDemandTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { GridAirportCpdDataSource } from './grid-ap-cpd-table.datasource';

@Component({
    selector: 'ams-grid-ap-cpd-table',
    templateUrl: './grid-ap-cpd-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class GridAirportCpdTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsCargoPayloadDemand>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() depAirport: AmsAirport;
    @Input() arrAirport: AmsAirport;

    selPpd: AmsCargoPayloadDemand;



    criteria: AmsCargoPayloadDemandTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;

    //, 'apCodes', 'payloadKg'
    displayedColumns = ['cpdId', 'depApId', 'arrApId', 'payloadKg', 'maxPrice', 'note', 'sequence', 'udate'];
    airline: AmsAirline;
    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private spmMapService: MapLeafletSpmService,
        public tableds: GridAirportCpdDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsCargoPayloadDemand>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            //this.dataCount = this.tableds.itemsCount;
            //this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsCargoPayloadDemandTableCriteria) => {
            this.criteria = this.tableds.criteria;
            this.loadPage();
        });

        this.initFields();
        this.criteria.sortCol = undefined;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['depAirport']) {
            this.initFields();
            this.criteria.depApId = this.depAirport ? this.depAirport.apId : undefined;
            this.tableds.criteria = this.criteria;
        } else if (changes['arrAirport']) {
            this.initFields();
            this.criteria.arrApId = this.arrAirport ? this.arrAirport.apId : undefined;
            this.tableds.criteria = this.criteria;
        }
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        //console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 500);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = undefined;
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    spmPpdOpenClick(value: AmsCargoPayloadDemand) {
        console.log('spmPpdOpenClick -> value:', value);
        this.selPpd = value;
        if (value) {
            //this.spmFlOpen.emit(fl);
        }
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }

    //#endregion

}
