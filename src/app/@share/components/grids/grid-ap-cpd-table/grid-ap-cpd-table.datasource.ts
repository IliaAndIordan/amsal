import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsCargoPayloadDemand, AmsCargoPayloadDemandTableCriteria, ResponseAmsCargoPayloadDemandTableData, ResponseAmsPaxPayloadDemandTableData } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';

export const KEY_CRITERIA = 'ams_ap_cpd_table_criteria';

@Injectable({ providedIn: 'root' })
export class GridAirportCpdDataSource extends DataSource<AmsCargoPayloadDemand> {

    seleted: AmsCargoPayloadDemand;

    private _data: Array<AmsCargoPayloadDemand>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsCargoPayloadDemand[]> = new BehaviorSubject<AmsCargoPayloadDemand[]>([]);
    listSubject = new BehaviorSubject<AmsCargoPayloadDemand[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsCargoPayloadDemand> {
        return this._data;
    }

    set data(value: Array<AmsCargoPayloadDemand>) {
        this._data = value;
        this.listSubject.next(this._data as AmsCargoPayloadDemand[]);
    }

    constructor(
        private cus: CurrentUserService,
        private apClient: AmsAirportClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsCargoPayloadDemandTableCriteria>();

    public get criteria(): AmsCargoPayloadDemandTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsCargoPayloadDemandTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsCargoPayloadDemandTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsCargoPayloadDemandTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'adate';
            obj.sortDesc = true;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsCargoPayloadDemandTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsCargoPayloadDemand[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsCargoPayloadDemand>> {

        this.isLoading = true;

        return new Observable<Array<AmsCargoPayloadDemand>>(subscriber => {

            this.apClient.airportCpdTable(this.criteria)
                .subscribe((res: ResponseAmsCargoPayloadDemandTableData) => {
                    const resp = res.data;
                    //console.log('airportCpdTable -> resp=', resp);
                    this.data = new Array<AmsCargoPayloadDemand>();
                    if (resp) {
                        if (resp && resp.payloads && resp.payloads.length > 0) {
                            resp.payloads.forEach(element => {
                                this.data.push(AmsCargoPayloadDemand.fromJSON(element));
                            });
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsCargoPayloadDemand>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
