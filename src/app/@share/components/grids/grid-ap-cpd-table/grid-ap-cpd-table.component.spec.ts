import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GridAirportCpdTableComponent } from './grid-ap-cpd-table.component';


describe('GridAirportCpdTableComponent', () => {
  let component: GridAirportCpdTableComponent;
  let fixture: ComponentFixture<GridAirportCpdTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAirportCpdTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAirportCpdTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
