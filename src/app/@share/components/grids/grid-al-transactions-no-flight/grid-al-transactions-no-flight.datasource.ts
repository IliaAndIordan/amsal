import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsTransaction, AmsTransactionTableCriteria, ResponceAmsTransactionTableData } from 'src/app/@core/services/api/airline/transaction';

export const KEY_CRITERIA = 'ams_al_transactions_no_flight_table_criteria';

@Injectable({ providedIn: 'root' })
export class GridAirlineTransactionsNoFlightsDataSource extends DataSource<AmsTransaction> {

    seleted: AmsTransaction;

    private _data: Array<AmsTransaction>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsTransaction[]> = new BehaviorSubject<AmsTransaction[]>([]);
    listSubject = new BehaviorSubject<AmsTransaction[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsTransaction> {
        return this._data;
    }

    set data(value: Array<AmsTransaction>) {
        this._data = value;
        this.listSubject.next(this._data as AmsTransaction[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsTransactionTableCriteria>();

    public get criteria(): AmsTransactionTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsTransactionTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsTransactionTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsTransactionTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'adate';
            obj.sortDesc = true;
            obj.alId = this.cus.airline.alId;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsTransactionTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsTransaction[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsTransaction>> {

        this.isLoading = true;

        return new Observable<Array<AmsTransaction>>(subscriber => {

            this.alClient.alTransactionsNoFlightTable(this.criteria)
                .subscribe((resp: ResponceAmsTransactionTableData) => {
                    //console.log('loadData -> resp=', resp);
                    this.data = new Array<AmsTransaction>();
                    if (resp) {
                        if (resp && resp.transactions && resp.transactions.length > 0) {
                            this.data = resp.transactions;
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsTransaction>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
