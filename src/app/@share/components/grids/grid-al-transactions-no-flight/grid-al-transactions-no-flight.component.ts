import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, Output, ViewChild, input, type OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription, tap } from 'rxjs';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsHub, AmsHubTableCriteria } from 'src/app/@core/services/api/airline/al-hub';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsHubTypes, AmsTransactionType } from 'src/app/@core/services/api/airline/enums';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsPaxPayloadDemandTableCriteria, AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { AmsHubEditDialog } from '../../dialogs/al-hub-edit/al-hub-edit.dialog';
import { GridAirlineHubsDataSource } from '../grid-al-hubs-table/grid-al-hubs-table.datasource';
import { AmsTransaction, AmsTransactionTableCriteria } from 'src/app/@core/services/api/airline/transaction';
import { GridAirlineTransactionsNoFlightsDataSource } from './grid-al-transactions-no-flight.datasource';

@Component({
  selector: 'ams-grid-al-transactions-no-flight',
  templateUrl: './grid-al-transactions-no-flight.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class AmsGridAlTransactionsNoFlightComponent implements OnInit, OnDestroy {

  @ViewChild(MatTable, { static: true }) table: MatTable<AmsTransaction>; // initialize
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() acId: number;
  @Input() tabindex: number;

  @Output() spmHubOpen: EventEmitter<AmsTransaction> = new EventEmitter<AmsTransaction>();

  selected: AmsTransaction;

  criteria: AmsTransactionTableCriteria;
  criteriaChanged: Subscription;
  maintenance = AmsTransactionType.Maintenance;

  pageSizeOpt = PAGE_SIZE_OPTIONS;
  pageIndex = 0;
  pageSize = 12;
  sortActive: string;
  sortDirection: SortDirection;


  dataCount = 0;
  dataChanged: Subscription;

  //, 'alId'
  displayedColumns = ['trId', 'paymentTime', 'trTypeId', 'amount', 'description', 'apId'];
  airline: AmsAirline;
  filter:string;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private preloader: SpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private hubCache: AmsAirlineHubsCacheService,
    private apCache: AmsAirportCacheService,
    public tableds: GridAirlineTransactionsNoFlightsDataSource) {

  }


  ngOnInit(): void {

    this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsTransaction>) => {
      // console.log('MyCompanyUsersComponent:userListSubject()->', users);
      this.dataCount = this.tableds.itemsCount;
      this.initFields();
    });
    this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsTransactionTableCriteria) => {
      this.criteria = this.tableds.criteria;
      // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
      // this.paginator._changePageSize(this.paginator.pageSize);
      this.loadPage();
    });

    this.initFields();
    this.criteria.offset = 0;
    this.tableds.criteria = this.criteria;
  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.dataChanged) { this.dataChanged.unsubscribe(); }
    if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
  }

  initFields() {
    //this.company = this.cus.company;
    this.airline = this.cus.airline;
    this.criteria = this.tableds.criteria;
    this.filter = this.criteria?.filter;
    this.pageIndex = this.criteria.pageIndex;
    this.pageSize = this.criteria.limit;
    this.dataCount = this.tableds.itemsCount;
    //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
    this.sortActive = this.criteria.sortCol;
    this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
    this.criteria.alId = this.cus.isAdmin?undefined:this.airline?.alId;
    this.criteria.acId = this.acId;
    //console.log('initFields-> criteria:', this.criteria);
    const tq = '?m=' + new Date().getTime();


  }


  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => {
      this.criteria.pageIndex = 0;
      this.criteria.sortCol = this.sort.active;
      this.criteria.sortDesc = this.sort.direction !== 'asc';
      this.tableds.criteria = this.criteria;
    });
    this.paginator.page
      .pipe(
        tap(() => {
          this.criteria.pageIndex = this.paginator.pageIndex;
          this.criteria.limit = this.paginator.pageSize;
          this.tableds.criteria = this.criteria;
        })
      )
      .subscribe();
  }

  //#region Table events

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.criteria.filter = filterValue.trim();
    this.criteria.sortCol = 'adate';
    this.criteria.sortDesc = true;
    this.tableds.criteria = this.criteria;
  }

  clearFilter(){
    this.criteria.filter = undefined;
    this.tableds.criteria = this.criteria;
  }

  loadPage() {
    this.tableds.loadData()
      .subscribe(res => {
        this.initFields();
      });
  }

  //#endregion

  //#region Actions

  rowClicked(value: AmsTransaction): void {
    console.log('rowClicked -> value:', value);
    this.selected = value;
  }

  refreshClick() {
    this.loadPage();
  }

  spmAirportOpen(apId: number) {
    if (apId) {
      this.cus.spmAirportPanelOpen.next(apId);
    }
  }
  spmAircraftOpen(acId: number) {
    if (acId) {
      this.cus.spmAircraftPanelOpen.next(acId);
    }
  }

  //#endregion

}