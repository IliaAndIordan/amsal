import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { URL_COMMON_IMAGE_AIRLINE, URL_IMG_IATA, URL_IMG_ICAO, URL_IMG_OUAP, URL_IMG_WIKI } from 'src/app/@core/const/app-storage.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-al-livery',
    templateUrl: './al-livery.component.html',
})

export class AlLiveryComponent implements OnInit, OnDestroy, OnChanges {


    @Input() alId: number;
    @Input() tooltip: string;
    @Input() imgclass: string = 'livery-table';
    @Output() liveryClick: EventEmitter<number> = new EventEmitter<number>();

    icaoUrl = URL_IMG_ICAO;
    iataUrl = URL_IMG_IATA;
    wikiUrl = URL_IMG_WIKI;
    ourApUrl = URL_IMG_OUAP;

    liveryUrl: string;
    noLogoUrl: string = URL_COMMON_IMAGE_AIRLINE + 'livery_7.png?163';
    constructor(
        private router: Router,
        private cus: CurrentUserService) { }


    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {

    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['alId']) {
            this.alId = changes['alId'].currentValue;
            this.initFields();
        }
        if (changes['tooltip']) {
            this.tooltip = changes['tooltip'].currentValue;
            //this.initFields();
        }
    }

    initFields() {
        if(this.alId){
            this.liveryUrl = URL_COMMON_IMAGE_AIRLINE + 'livery_' + this.alId.toString() + '.png?' + new Date().getTime();
        } else{
            this.liveryUrl = undefined;
        }
        //console.log('initFields-> liveryUrl:', this.liveryUrl);
    }

    liveryClicked() {
        this.liveryClick.emit(this.alId);
    }

    onImageLoad() {
        //console.log('onImageLoad -> liveryUrl:', this.liveryUrl);
        this.getImageClass();
    }

    onImageError() {
        //console.log('onImageError -> liveryUrl:', this.liveryUrl);

        this.liveryUrl = undefined;
    }

    getImageClass() {
        const image = new Image();
        image.src = this.liveryUrl;
        if (image.width > image.height + 20 || image.width === image.height) {
            // return wide image class
            //return Wide;
        } else {
            //return Tall;
        }
    }

}

