import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { URL_COMMON_IMAGE_AIRLINE, URL_IMG_IATA, URL_IMG_ICAO, URL_IMG_OUAP, URL_IMG_WIKI } from 'src/app/@core/const/app-storage.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-al-info-card',
    templateUrl: './al-info-card.component.html',
})

export class AlInfoCardComponent implements OnInit, OnDestroy, OnChanges {


    @Input() hqAirport: AmsAirport;
    @Input() airline: AmsAirline;
    @Output() generateLivery: EventEmitter<AmsAirline> = new EventEmitter<AmsAirline>();

    icaoUrl = URL_IMG_ICAO;
    iataUrl = URL_IMG_IATA;
    wikiUrl = URL_IMG_WIKI;
    ourApUrl = URL_IMG_OUAP;
    noLogoUrl = URL_COMMON_IMAGE_AIRLINE + 'logo_0.png';

    logoUrl:string;
    liveryUrl:string;
    constructor(
        private router: Router,
        private cus: CurrentUserService) { }

   
    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {

    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['airline']) {
            this.airline = changes['airline'].currentValue;
            this.initFields();
        }
        if (changes['hqAirport']) {
            this.hqAirport = changes['hqAirport'].currentValue;
            this.initFields();
        }
    }

    initFields(){
        this.logoUrl = this.airline?.logo?this.airline?.logo: this.airline?.amsLogoUrl;
        this.liveryUrl =  this.airline?.amsLiveryUrl;
    }

    generateLiveryClick(){
        if(this.airline){
            this.generateLivery.emit(this.airline);
        }
        
    }

}

