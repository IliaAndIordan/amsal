import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { URL_IMG_IATA, URL_IMG_ICAO, URL_IMG_OUAP, URL_IMG_WIKI } from 'src/app/@core/const/app-storage.const';
import { AmsCountry } from 'src/app/@core/services/api/country/dto';

@Component({
    selector: 'ams-card-country',
    templateUrl: './card-country.component.html',
})

export class AmsCardCountryComponent implements OnInit, OnChanges {

    /**
     * Bindings
     */
    @Input() country: AmsCountry;
    @Input() selCountry: AmsCountry;
    @Output() selectCountry: EventEmitter<AmsCountry> = new EventEmitter<AmsCountry>();


    /**
     * FIELDS
     */
    icaoUrl = URL_IMG_ICAO;
    iataUrl = URL_IMG_IATA;
    wikiUrl = URL_IMG_WIKI;
    ourApUrl = URL_IMG_OUAP;

    constructor(private router: Router) { }

    ngOnInit(): void {
        this.initFields();
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['country']) {
            this.country = changes['country'].currentValue;
            this.initFields();
        }
        if (changes['selCountry']) {
            this.selCountry = changes['selCountry'].currentValue;
            this.initFields();
        }
    }

    initFields() {
    }

    //#region Actions

    selectClick() {
        this.selectCountry.emit(this.country);
        // this.router.navigate([AmsRoutes.Region, AmsRoutes.RsCountry]);
    }

    

    //#endregion
}
