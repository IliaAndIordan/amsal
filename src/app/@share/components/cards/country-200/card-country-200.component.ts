import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { URL_IMG_IATA, URL_IMG_ICAO, URL_IMG_OUAP, URL_IMG_WIKI } from 'src/app/@core/const/app-storage.const';
import { AmsCountry } from 'src/app/@core/services/api/country/dto';
import { WadStatus } from 'src/app/@core/models/pipes/ams-status.enums';


@Component({
    selector: 'ams-card-country-200',
    templateUrl: './card-country-200.component.html',
})

export class AmsCardCountry200Component implements OnInit, OnChanges {
    /**
     * Bindings
     */
    @Input() dragScope: string[];
    @Input() country: AmsCountry;
    @Input() selCountry: AmsCountry;
    @Output() selectCountry: EventEmitter<AmsCountry> = new EventEmitter<AmsCountry>();

    /**
     * FIELDS
     */
    icaoUrl = URL_IMG_ICAO;
    iataUrl = URL_IMG_IATA;
    wikiUrl = URL_IMG_WIKI;
    ourApUrl = URL_IMG_OUAP;
    pkgDragScope: Array<string> = ['todo', 'inprogress', 'review', 'done'];
    
    isSelected: boolean;
    subclass: string;

    constructor(private router: Router) { }

    ngOnInit(): void {
        this.initFields();
    }

   
    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['country']) {
            this.country = changes['country'].currentValue;
            this.initFields();
        }
        if (changes['selCountry']) {
            this.selCountry = changes['selCountry'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        
        // console.log('initFields -> selCountry:', this.selCountry);
        this.isSelected = (this.selCountry && this.selCountry.countryId === this.country.countryId) ? true : false;
        // console.log('initFields -> isSelected:', this.isSelected);
        switch (this.country.wadStatus) {
            case WadStatus.ToDo:
                this.subclass = 'card-200-todo';
                break;
            case WadStatus.InProgress:
                this.subclass = 'card-200-wip';
                break;
            case WadStatus.Review:
                this.subclass = 'card-200-review';
                break;
            case WadStatus.Done:
                this.subclass = 'card-200-done';
                break;
            default:
                this.subclass = 'card-200';
                break;
        }
        this.subclass += this.isSelected ? ' very-light-blue-background' : '';
        // console.log('initFields -> subclass:', this.subclass);
    }

    //#region Actions

    selectClick() {
        this.selectCountry.emit(this.country);
        //this.router.navigate([AmsRoutes.Region, AmsRoutes.RsCountry]);
    }

    //#endregion
}
