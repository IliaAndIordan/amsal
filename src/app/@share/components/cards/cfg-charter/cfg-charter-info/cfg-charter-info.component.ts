import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges } from '@angular/core';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCfgCharter } from 'src/app/@core/services/api/flight/charter-dto';

@Component({
  selector: 'ams-cfg-charter-info',
  templateUrl: './cfg-charter-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CfgCharterInfoComponent implements OnInit, OnChanges {

  @Input() flight: AmsCfgCharter;
  depAp: AmsAirport;
  arrAp: AmsAirport;

  constructor(private apCache: AmsAirportCacheService,) { }

  ngOnInit(): void {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flight']) {
      this.initFields();
    }
  }


  initFields(): void {
    if (this.flight) {
      this.apCache.getAirport(this.flight.depApId).subscribe(ap => {
        this.depAp = ap;
      });
      this.apCache.getAirport(this.flight.arrApId).subscribe(ap => {
        this.arrAp = ap;
      })
    }
  }
}
