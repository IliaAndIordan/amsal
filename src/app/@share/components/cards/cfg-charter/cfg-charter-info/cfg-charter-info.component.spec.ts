import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CfgCharterInfoComponent } from './cfg-charter-info.component';

describe('CfgCharterInfoComponent', () => {
  let component: CfgCharterInfoComponent;
  let fixture: ComponentFixture<CfgCharterInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CfgCharterInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CfgCharterInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
