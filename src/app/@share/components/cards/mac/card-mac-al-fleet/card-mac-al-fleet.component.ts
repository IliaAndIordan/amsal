import { CommonModule } from '@angular/common';
import { Component, Input, type OnInit } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-card-mac-al-fleet',
  templateUrl: './card-mac-al-fleet.component.html',
})
export class AmsCardMacAlFleetComponent implements OnInit {

  @Input() mac: AmsMac;
  
  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;

  constructor(private cus:CurrentUserService){}

  ngOnInit(): void { 
    this.initFields();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['mac']) {
        //this.mac = changes['mac'].currentValue;
        this.initFields();
    }
    
}

  initFields() {
    if(this.mac){
      this.imagevUrl = this.mac.imgTableUrl;
    }
  }

}
