import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AMS_COMMON, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import {  AcTypeOpt, AmsAcType } from 'src/app/@core/models/pipes/ac-type.pipe';
import { AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-card-mac-info',
    templateUrl: './card-mac-info.component.html',
})
export class CardMacInfoComponent implements OnInit, OnChanges {

    /**
     * BINDINGS
     */
    @Input() mac: AmsMac;

    /**
     * FIELDS
     */
    nextProducedOn:Date;
    roots = AppRoutes;
    imagevUrl = URL_NO_IMG_SQ;
    ourApImg = URL_IMG_OUAP;
    ourApUrl: string;
    pilotImgUrl = URL_COMMON_IMAGE_AMS_COMMON + 'pilot_m_1.png';
    seatsImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/seat_icon_3.png';
    acImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'icon-ac-model.png';
    acType:AmsAcType;
    constructor(
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['mac']) {
            this.mac = changes['mac'].currentValue;
            this.initFields();
        }
        
    }

    initFields() {
      if(this.mac){
          this.acType = AcTypeOpt.find(x => x.acTypeId === this.mac.acTypeId);
          const productionRateMs = this.mac.productionRateH?this.mac.productionRateH*60*60*1000:0;
          this.nextProducedOn = this.mac?this.mac.nextProducedOn: new Date();
      }
    }

    //#region Action Methods

    centerMapOnAirport(){
        console.log('centerMapOnAirport-> ap:', this.mac);
    }

    //#endregion

}
