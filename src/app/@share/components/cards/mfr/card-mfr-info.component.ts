import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-card-mfr-info',
    templateUrl: './card-mfr-info.component.html',
})
export class CardManufacturerInfoComponent implements OnInit, OnChanges {

    @Input() airport: AmsAirport;
    @Input() mfr: AmsManufacturer;

    
    roots = AppRoutes;
    imagevUrl = URL_NO_IMG_SQ;
    ourApImg = URL_IMG_OUAP;
    ourApUrl: string;

    constructor(
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['mfr']) {
            this.initFields();
        }
        if (changes['airport']) {
            this.airport = changes['airport'].currentValue;
            console.log('ngOnChanges-> ap:', this.airport);
            this.initFields();
        }
    }

    initFields() {
      
    }

    //#region Action Methods

    centerMapOnAirport(){
        console.log('centerMapOnAirport-> ap:', this.airport);
    }

    //#endregion

}
