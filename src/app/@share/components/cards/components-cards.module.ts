import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxMaterialModule } from '../material.module';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { AmsCardCountryComponent } from './country/card-country.component';
import { AmsCardCountry200Component } from './country-200/card-country-200.component';
import { CardAirportInfoComponent } from './airport/card-airport-info.component';
import { SxComponentsCommonModule } from '../common/components-common.module';
import { CardManufacturerInfoComponent } from './mfr/card-mfr-info.component';
import { CardMacInfoComponent } from './mac/card-mac-info.component';
import { AlInfoCardComponent } from './airline/info/al-info-card.component';
import { AlLiveryComponent } from './airline/livery/al-livery.component';
import { CardAircraftInfoComponent } from './aircraft/info/card-aircraft-info.component';
import { CardFlightEstimateInfoComponent } from './flight/card-flight-estimate-info/card-flight-estimate-info.component';
import { CardPayloadsListComponent } from './flight/card-payloads-list/card-payloads-list.component';
import { CfgCharterInfoComponent } from './cfg-charter/cfg-charter-info/cfg-charter-info.component';
import { CardFlightEstimateTableRowComponent } from './flight/card-flight-estimate-table-row/card-flight-estimate-table-row.component';
import { CardAircraftRowComponent } from './aircraft/card-aircraft-row/card-aircraft-row.component';
import { CardAircraftIdRowComponent } from './aircraft/card-acid-row/card-aircraft-id-row.component';
import { CardAirportIcaoComponent } from './airport/card-ap-icao.component';
import { CardFlightInfoComponent } from './flight/card-flight-info/card-flight-info.component';
import { CardFlightArrivalComponent } from './flight/card-flight-arrival/card-flight-arrival.component';
import { AmsCardFlightDepartureComponent } from './flight/card-flight-departure/card-flight-departure.component';
import { AmsAircraftImageComponent } from './aircraft/acid-img/acid-img.component';
import { AmsCardPayloadSumComponent } from './flight/card-payload-sum/card-payload-sum.component';
import { CardFlightPlanInfoComponent } from './flight/card-flight-plan-info/card-flight-plan-info.component';
import { AmsCardMacAlFleetComponent } from './mac/card-mac-al-fleet/card-mac-al-fleet.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SxMaterialModule,
    SxPipesModule,
    //
    SxComponentsCommonModule,
  ],
  declarations: [
    // -Country
    AmsCardCountryComponent,
    AmsCardCountry200Component,
    // -Airport
    CardAirportInfoComponent,
    // Manufacturer
    CardManufacturerInfoComponent,
    CardMacInfoComponent,
    // Airline
    AlInfoCardComponent,
    AlLiveryComponent,
    CardAircraftInfoComponent,
    CardFlightEstimateTableRowComponent,
    CardFlightEstimateInfoComponent,
    CardPayloadsListComponent,
    CfgCharterInfoComponent,
    CardAircraftRowComponent,
    CardAircraftIdRowComponent,
    CardAirportIcaoComponent,
    CardFlightInfoComponent,
    CardFlightArrivalComponent,
    AmsCardFlightDepartureComponent,
    AmsAircraftImageComponent,
    AmsCardPayloadSumComponent,
    CardFlightPlanInfoComponent,
    AmsCardMacAlFleetComponent,
  ],
  exports: [
    // -Country
    AmsCardCountryComponent,
    AmsCardCountry200Component,
    // -Airport
    CardAirportInfoComponent,
    // Manufacturer
    CardManufacturerInfoComponent,
    CardMacInfoComponent,
    // Airline
    AlInfoCardComponent,
    AlLiveryComponent,
    CardAircraftInfoComponent,
    CardFlightEstimateTableRowComponent,
    CardFlightEstimateInfoComponent,
    CardPayloadsListComponent,
    CfgCharterInfoComponent,
    CardAircraftRowComponent,
    CardAircraftIdRowComponent,
    CardAirportIcaoComponent,
    CardFlightInfoComponent,
    CardFlightArrivalComponent,
    AmsCardFlightDepartureComponent,
    AmsAircraftImageComponent,
    AmsCardPayloadSumComponent,
    CardFlightPlanInfoComponent,
    AmsCardMacAlFleetComponent,
  ]
})

export class SxComponentsCardsModule { }
