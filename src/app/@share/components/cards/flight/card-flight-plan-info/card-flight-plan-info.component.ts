import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_IMG_OUAP, URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsAlFlightNumber, AmsFlpPayloads, ResponseAmsAlFlightNumberScheduleUpdateFromFlp } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsFlightPlanPayload } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight, AmsFlightPayload } from 'src/app/@core/services/api/flight/dto';
import { AmsPayloadType, AmsPayloadTypeOpt } from 'src/app/@core/services/api/flight/enums';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';

@Component({
  selector: 'ams-card-flight-plan-info',
  templateUrl: './card-flight-plan-info.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class CardFlightPlanInfoComponent implements OnInit, OnChanges {


  @Input() flight: AmsFlightPlanPayload;
  @Input() payloads: AmsFlpPayloads[];
  @Input() payloadsCount: number;
  @Input() small: boolean = false;

  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;
  ourApUrl: string;

  ptE = AmsPayloadType.E;
  ptB = AmsPayloadType.B;
  ptF = AmsPayloadType.F;
  ptC = AmsPayloadType.C;

  depAp: AmsAirport;
  arrAp: AmsAirport;
  staticMapUrl: string;
  payloadsum: AmsFlightPayload[];

  flpn: AmsAlFlightNumber;
  canEdit:boolean = false;


  constructor(
    private spiner: SpinnerService,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private alClient: AmsAirlineClient,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.loadAirports();
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes['flight']) {
      this.loadAirports();
      this.loadFlightNumber();
    }
    if (changes['payloads']) {
      this.loadAirports();
    }
  }

  initFields() {
    if (this.flight && this.depAp && this.arrAp) {
      this.staticMapUrl = AmsFlight.getStaticMapUrl(this.depAp, this.arrAp);
    }
    this.canEdit = this.flight?.alId === this.cus.airline?.alId;
    if (this.payloads && this.payloads.length > 0) {
      this.payloadsum = new Array<AmsFlightPayload>();
      AmsPayloadTypeOpt.forEach(pt => {

        const payloads = this.payloads.filter(x => x.payloadType === pt.id);
        if (payloads && payloads.length > 0) {
          let dto = new AmsFlightPayload();
          dto.payloadType = pt.id;
          dto.flplId = payloads[0].flpplId;
          dto.flId = payloads[0].flpId;
          dto.cargoKg = payloads.reduce((sum, current) => sum + current.payloadKg, 0);
          dto.pax = payloads.reduce((sum, current) => sum + current.pax, 0);
          dto.price = payloads.reduce((sum, current) => sum + current.price, 0);
          dto.payloadKg = payloads.reduce((sum, current) => sum + current.payloadKg, 0);
          payloads.forEach(element => {
            dto.description += element.description;
          });
          this.payloadsum.push(dto);
        }
      });
    } else {
      this.payloadsum = undefined;
    }
  }

  loadAirports() {
    if (this.flight) {
      this.apCache.getAirport(this.flight.dApId).subscribe(ap => {
        this.depAp = ap;
        this.apCache.getAirport(this.flight.aApId).subscribe(ap => {
          this.arrAp = ap;
          this.initFields();
        });
      });
    }
  }

  loadFlightNumber() {
    console.log('loadFlightNumber -> flpnId:', this.flight?.flpnId);
    if (this.flight?.flpnId > 0) {
      this.alClient.alFlightNumberGet(this.flight.flpnId).then((flpn: AmsAlFlightNumber) => {
        this.flpn = flpn;
        console.log('loadFlightNumber -> flpn:', this.flpn);
      })
    }
  }

  decreasePricePax(): void {
    let flpn = Object.assign(new AmsAlFlightNumber(), this.flpn);
    flpn.priceE = parseFloat((this.flpn.priceE * 0.9).toFixed(2));
    flpn.priceB = parseFloat((this.flpn.priceB * 0.9).toFixed(2));
    flpn.priceF = parseFloat((this.flpn.priceF * 0.9).toFixed(2));
    //flpn.priceCpKg = parseFloat((this.flpn.priceCpKg * 0.9).toFixed(2));
    this.flpnSave(flpn);
  }
  decreasePriceCargo(): void {
    let flpn = Object.assign(new AmsAlFlightNumber(), this.flpn);
    flpn.priceCpKg = parseFloat((this.flpn.priceCpKg * 0.8).toFixed(2));
    this.flpnSave(flpn);
  }

  increasePricePax(): void {
    let flpn = Object.assign(new AmsAlFlightNumber(), this.flpn);
    flpn.priceE = parseFloat((this.flpn.priceE * 1.1).toFixed(2));
    flpn.priceB = parseFloat((this.flpn.priceB * 1.1).toFixed(2));
    flpn.priceF = parseFloat((this.flpn.priceF * 1.1).toFixed(2));
    //flpn.priceCpKg = parseFloat((this.flpn.priceCpKg * 1.1).toFixed(2));
    this.flpnSave(flpn);
  }
  increasePriceCargo(): void {
    let flpn = Object.assign(new AmsAlFlightNumber(), this.flpn);
    flpn.priceCpKg = parseFloat((this.flpn.priceCpKg * 1.2).toFixed(2));
    this.flpnSave(flpn);
  }

  flpnSave(value: AmsAlFlightNumber) {
    console.log('flpnSave -> value:', value);
    this.spiner.display(true);

    this.alClient.alFlightNumberSave(value).then((res: AmsAlFlightNumber) => {

      console.log('flpnSave -> res:', res);
      if (res) {
        this.flpn = res;
        this.alClient.alFlightNumberScheduleUpdateFromFlpnP(this.flpn.routeId)
          .then((result: ResponseAmsAlFlightNumberScheduleUpdateFromFlp) => {
            this.spiner.hide();
            if (result.success) {
              this.toastr.success(
                `Updated <b>${result.data.affectedRows}</b> Flight Number Shedules.`,
                'Update Prices',
                { enableHtml: true });
            } else {
              this.toastr.success('Update Prices Failed', result.message);
            }
          }).catch(err => {
            this.spiner.display(false);
          });
        this.toastr.success(`Operation Succesfull: Price Changed`);
      } else{
        this.spiner.display(false);
      }
    }, err => {
      this.spiner.display(false);
      console.error('Observer got an error: ' + err);
      this.toastr.error('Price Changed', 'Operation failed. Please try again later.');
    });
  }

}
