import { CommonModule } from '@angular/common';
import { Component, Input, SimpleChanges, type OnInit } from '@angular/core';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsFlightPayload } from 'src/app/@core/services/api/flight/dto';
import { AmsPayloadType, AmsPayloadTypeOpt } from 'src/app/@core/services/api/flight/enums';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-card-payload-sum',
  templateUrl: './card-payload-sum.component.html',
})
export class AmsCardPayloadSumComponent implements OnInit {

  @Input() payloads: AmsFlightPayload[];

  typeCargo = AmsPayloadType.C;
  payloadsum: AmsFlightPayload[];

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes['payloads']) {
      this.initFields();
    }
  }

  initFields() {

    if (this.payloads && this.payloads.length > 0) {
      this.payloadsum = new Array<AmsFlightPayload>();
      AmsPayloadTypeOpt.forEach(pt => {
        let dto = new AmsFlightPayload();
        dto.payloadType = pt.id;
        const payloads = this.payloads.filter(x => x.payloadType === pt.id);
        if (payloads && payloads.length > 0) {
          dto.flplId = payloads[0].flplId;
          dto.flId = payloads[0].flId;
          dto.flId = payloads[0].flId;
          dto.cargoKg = payloads.reduce((sum, current) => sum + current.cargoKg, 0);
          dto.pax = payloads.reduce((sum, current) => sum + current.pax, 0);
          dto.price = payloads.reduce((sum, current) => sum + current.price, 0);
          dto.payloadKg = payloads.reduce((sum, current) => sum + current.payloadKg, 0);
          payloads.forEach(element => {
            dto.description += element.description;
          });
          this.payloadsum.push(dto);
        }
      });
    } else {
      this.payloadsum = undefined;
    }

  }
}
