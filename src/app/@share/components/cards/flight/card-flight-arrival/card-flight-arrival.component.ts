import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_IMG_OUAP, URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-card-flight-arrival',
  templateUrl: './card-flight-arrival.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class CardFlightArrivalComponent {
  
  @Input() flight: AmsFlight;
  
  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flight']) {
      //this.flight = changes['flight'].currentValue;
      this.initFields();
    }
  }

  initFields() {
    if (this.flight) {
      
    }
  }

  flightTypeClick(){
    if(this.flight){
      this.cus.spmAircraftPanelOpen.next(this.flight.acId);
    }
  }

  flightClick(){
    if(this.flight){
      this.cus.spmFlightPanelOpen.next(this.flight.flId);
    }
  }

  airportClick(apId:number){
    if(apId){
      this.cus.spmAirportPanelOpen.next(apId);
    }
  }

  spmAirlineOpenClick(alId:number):void{
    if(alId){
      this.cus.spmAirlinePanelOpen.next(alId);
    }
  }
}
