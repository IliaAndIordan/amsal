import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardFlightArrivalComponent } from './card-flight-arrival.component';

describe('CardFlightArrivalComponent', () => {
  let component: CardFlightArrivalComponent;
  let fixture: ComponentFixture<CardFlightArrivalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardFlightArrivalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardFlightArrivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
