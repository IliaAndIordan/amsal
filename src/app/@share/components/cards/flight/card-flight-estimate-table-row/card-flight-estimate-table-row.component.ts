import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsFlightEstimate } from 'src/app/@core/services/api/flight/dto';

@Component({
  selector: 'ams-card-flight-estimate-table-row',
  templateUrl: './card-flight-estimate-table-row.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardFlightEstimateTableRowComponent implements OnInit {


  @Input() aircraft: AmsAircraft;
  @Input() flight: AmsFlightEstimate;
  @Input() showHeader: boolean = false;

  @Output() selectFlight: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  selectCharterFlight(): void {
    this.selectFlight.emit(this.flight.fleId);
  }
}
