import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CardFlightEstimateTableRowComponent } from './card-flight-estimate-table-row.component';


describe('Share -> Components -> Cards -> CardFlightEstimateTableRowComponent', () => {
  let component: CardFlightEstimateTableRowComponent;
  let fixture: ComponentFixture<CardFlightEstimateTableRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardFlightEstimateTableRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardFlightEstimateTableRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
