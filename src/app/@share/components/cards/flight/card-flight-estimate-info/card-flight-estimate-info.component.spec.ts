import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardFlightEstimateInfoComponent } from './card-flight-estimate-info.component';

describe('Share -> Components -> Cards -> CardFlightCharterInfoComponent', () => {
  let component: CardFlightEstimateInfoComponent;
  let fixture: ComponentFixture<CardFlightEstimateInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardFlightEstimateInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardFlightEstimateInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
