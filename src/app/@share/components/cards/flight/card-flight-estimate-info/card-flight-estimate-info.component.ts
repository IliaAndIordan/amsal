import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlightEstimate } from 'src/app/@core/services/api/flight/dto';

@Component({
  selector: 'ams-card-flight-estimate-info',
  templateUrl: './card-flight-estimate-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardFlightEstimateInfoComponent implements OnInit, OnChanges {


  @Input() aircraft: AmsAircraft;
  @Input() flight: AmsFlightEstimate;
  @Input() hasSpinner: boolean = false;

  depAp: AmsAirport;
  arrAp: AmsAirport;

  @Output() selectFlight: EventEmitter<number> = new EventEmitter<number>();

  constructor(private apCache: AmsAirportCacheService,) { }

  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flight']) {
      this.initFields();
    }
    if (changes['hasSpinner']) {
      this.initFields();
    }
  }


  initFields(): void {
    if (this.flight) {
      this.apCache.getAirport(this.flight.depApId).subscribe(ap => {
        this.depAp = ap;
      });
      this.apCache.getAirport(this.flight.arrApId).subscribe(ap => {
        this.arrAp = ap;
      })
    }
  }

  selectCharterFlight(): void {
    this.selectFlight.emit(this.flight.fleId);
  }
}
