import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_IMG_OUAP, URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight, AmsFlightPayload } from 'src/app/@core/services/api/flight/dto';
import { AmsPayloadTypeOpt } from 'src/app/@core/services/api/flight/enums';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-card-flight-info',
  templateUrl: './card-flight-info.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class CardFlightInfoComponent implements OnInit, OnChanges {


  @Input() flight: AmsFlight;
  @Input() small: boolean = false;

  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;
  ourApUrl: string;

  depAp: AmsAirport;
  arrAp: AmsAirport;
  staticMapUrl: string;
  payloadsum: AmsFlightPayload[];


  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.loadAirports();
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes['flight']) {
      this.loadAirports();
    }
  }

  initFields() {
    if (this.flight && this.depAp && this.arrAp) {
      this.staticMapUrl = AmsFlight.getStaticMapUrl(this.depAp, this.arrAp);
    }

    if (this.flight?.payloads && this.flight?.payloads.length > 0) {
      this.payloadsum = new Array<AmsFlightPayload>();
      AmsPayloadTypeOpt.forEach(pt => {
        let dto = new AmsFlightPayload();
        dto.payloadType = pt.id;
        const payloads = this.flight.payloads.filter(x => x.payloadType === pt.id);
        if (payloads && payloads.length > 0) {
          dto.flplId = payloads[0].flplId;
          dto.flId = payloads[0].flId;
          dto.flId = payloads[0].flId;
          dto.cargoKg = payloads.reduce((sum, current) => sum + current.cargoKg, 0);
          dto.pax = payloads.reduce((sum, current) => sum + current.pax, 0);
          dto.price = payloads.reduce((sum, current) => sum + current.price, 0);
          dto.payloadKg = payloads.reduce((sum, current) => sum + current.payloadKg, 0);
          payloads.forEach(element => {
            dto.description += element.description;
          });
          this.payloadsum.push(dto);
        }
      });
    } else {
      this.payloadsum = undefined;
    }
    if (!this.flight.income && this.payloadsum && this.payloadsum.length > 0) {
      this.flight.income = this.payloadsum.reduce((sum, current) => sum + current.price, 0);
    }
  }

  loadAirports() {
    if (this.flight) {
      this.apCache.getAirport(this.flight.depApId).subscribe(ap => {
        this.depAp = ap;
        this.apCache.getAirport(this.flight.arrApId).subscribe(ap => {
          this.arrAp = ap;
          this.initFields();
        });
      });
    }
  }

}
