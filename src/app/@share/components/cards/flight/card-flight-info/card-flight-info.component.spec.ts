import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardFlightInfoComponent } from './card-flight-info.component';

describe('CardFlightInfoComponent', () => {
  let component: CardFlightInfoComponent;
  let fixture: ComponentFixture<CardFlightInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardFlightInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardFlightInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
