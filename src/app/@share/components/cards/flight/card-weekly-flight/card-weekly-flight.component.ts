import { CommonModule } from '@angular/common';
import { Component, Input, type OnInit } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_IMG_OUAP, URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsAlFlightNumberSchedule, AmsWeeklyFlights, AmsWeeklyFlightSchedule } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsWeekdaysOpt } from 'src/app/@core/services/api/airline/enums';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-card-weekly-flight',
  templateUrl: './card-weekly-flight.component.html',
})
export class AmsCardWeeklyFlightComponent implements OnInit {
  @Input() weekFlights: AmsWeeklyFlights;
  @Input() departure: boolean = false;

  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;
  weekdaysOpt = AmsWeekdaysOpt;

  
  airport:AmsAirport;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['weekFlights']) {
      this.initFields();
    }
  }

  initFields() {
    if (this.weekFlights && this.weekFlights.depApId) {
      const airport = this.apCache.getAirport(this.weekFlights.depApId);
      airport.subscribe(ap=>{
        this.airport = ap;
      });
    }
  }

  flightTypeClick(value:AmsAlFlightNumberSchedule) {
    if (value && value.acId) {
      this.cus.spmAircraftPanelOpen.next(value.acId);
    }
  }

  flightClick(value:AmsAlFlightNumberSchedule) {
    if (value && value.flpnId) {
      this.cus.spmFlightPanelOpen.next(value.flpnId);
    }
  }

  airportClick(apId: number) {
    if (apId) {
      this.cus.spmAirportPanelOpen.next(apId);
    }
  }

  spmAirlineOpenClick(alId: number): void {
    if (alId) {
      this.cus.spmAirlinePanelOpen.next(alId);
    }
  }

  spmAircraftOpenClick(acId: number): void {
    if (acId) {
      this.cus.spmAircraftPanelOpen.next(acId);
    }
  }
}
