import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { AmsFlightEstimatePayload } from 'src/app/@core/services/api/flight/dto';

@Component({
  selector: 'ams-card-payloads-list',
  templateUrl: './card-payloads-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardPayloadsListComponent implements OnInit {

  @Input() payloads: AmsFlightEstimatePayload[];

  constructor() { }

  ngOnInit(): void {
  }

}
