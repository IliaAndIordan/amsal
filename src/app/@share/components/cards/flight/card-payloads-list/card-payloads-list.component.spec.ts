import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPayloadsListComponent } from './card-payloads-list.component';

describe('CardPayloadsListComponent', () => {
  let component: CardPayloadsListComponent;
  let fixture: ComponentFixture<CardPayloadsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardPayloadsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPayloadsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
