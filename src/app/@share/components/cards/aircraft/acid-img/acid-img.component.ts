import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input, OnChanges, type OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { EnumViewModel } from 'src/app/@core/models/common/enum-view.model';
import { AircraftActionType } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';

@Component({
  selector: 'ams-acid-img',
  templateUrl: './acid-img.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
})
export class AmsAircraftImageComponent implements OnInit, OnChanges {

  @Input() acId: number;

  acImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'icon-ac-model.png';
  aircraft$: Observable<AmsAircraft>;
  aircraft: AmsAircraft;

  mntCheck = AircraftActionType.MaintenanceCheck;
  acatFlight = AircraftActionType.Flight;
  nextMntOpt: EnumViewModel;
  nextMntTimeMin: number;

  pendingMntOpt: EnumViewModel;
  mntOpt: EnumViewModel;
  mntTimeLeftMin: number;

  constructor(
    private router: Router,
    private acCache: AmsAirlineAircraftCacheService,
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private acService: AmsAirlineAircraftService) { }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['acId']) {
      this.initFields();
    }

  }

  initFields(): void {
    this.mntOpt = undefined;
    this.mntTimeLeftMin = 0;
    if (this.acId) {
      this.aircraft$ = this.acCache.getAircraft(this.acId);
      this.aircraft$.subscribe(ac => {
        this.aircraft = ac;
      });
    }
  }

  gotoAircraft() {
    if (this.aircraft) {
      this.acService.aircraft = this.aircraft;
      this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
    }
  }

  spmAircraftOpen():void{
    if(this.aircraft?.acId){
      this.cus.spmAircraftPanelOpen.next(this.aircraft.acId);
    }
  }
}
