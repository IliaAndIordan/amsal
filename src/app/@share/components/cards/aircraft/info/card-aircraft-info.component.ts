import { Component, OnInit, Input, OnChanges, Output, EventEmitter, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Toast } from 'bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_AMS_COMMON, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { EnumViewModel } from 'src/app/@core/models/common/enum-view.model';
import { AcType, AcTypeOpt, AmsAcType } from 'src/app/@core/models/pipes/ac-type.pipe';
import { AircraftActionType, AircraftStatus } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAircraft, AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';
import { AmsAcmpEditDialog } from '../../../dialogs/acmp-create/acmp-edit.dialog';
import { VoidExpression } from 'typescript';
import { AmsMaintenancePlan } from 'src/app/@core/services/api/aircraft/maintenance-plan';
import { AmsAircraftHomeApEditDialog } from '../../../dialogs/mac-edit/ac-homeap-edit/ac-homeap-edit.dialog';
import { AmsMaintenanceType } from 'src/app/@core/services/api/aircraft/enums';


@Component({
    selector: 'ams-card-aircraft-info',
    templateUrl: './card-aircraft-info.component.html',
})
export class CardAircraftInfoComponent implements OnInit, OnChanges, OnDestroy {


    @Input() aircraft: AmsAircraft;
    @Input() hideBtns: boolean = false;
    @Output() transferEstimate: EventEmitter<number> = new EventEmitter<number>();

    nextProducedOn: Date;
    roots = AppRoutes;
    noImgUrl = URL_NO_IMG_SQ;
    mac: AmsMac;
    mntCheck = AircraftActionType.MaintenanceCheck;
    acatFlight = AircraftActionType.Flight;
    nextMntOpt: AmsMaintenanceType;
    nextMntTimeMin: number;
    mntRequired: boolean = false;

    pilotImgUrl = URL_COMMON_IMAGE_AMS_COMMON + 'pilot_m_1.png';
    seatsImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/seat_icon_3.png';
    acImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'icon-ac-model.png';
    acActive = AircraftStatus.Operational;
    canTransfer: boolean = false;
    pendingMntOpt: AmsMaintenanceType;

    minFromlogAdate: number;
    mntOpt: AmsMaintenanceType;
    logAdateLapsedMin: number;
    mntTimeLeftMin: number;

    canStartMeintenance: boolean;
    timerMinOne: Subscription;
    opMnt = AircraftActionType.MaintenanceCheck;
    mtn: AmsMaintenancePlan;
    canDelMtn: boolean = false;
    timeToMntH: number;

    canEdit:boolean = false;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private apCache: AmsAirportCacheService,
        private acClient: AmsAircraftClient,
        private acService: AmsAirlineAircraftService) {
    }


    ngOnInit() {
        this.timerMinOne = this.cus.timerMinOneSubj.subscribe(min => {
            this.loadData();
        });
        this.initFields();
        this.loadData();
    }

    ngOnDestroy(): void {
        if (this.timerMinOne) { this.timerMinOne.unsubscribe(); }
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['aircraft']) {
            const obj = changes['aircraft'].currentValue;
            if (obj && obj.acId) {
                this.aircraft = AmsAircraft.fromJSON(obj);
            } else {
                this.aircraft = undefined;
            }
            this.initFields();
            this.loadData();
        }

    }

    initFields() {
        this.canEdit = (this.aircraft?.ownerAlId === this.cus.airline?.alId);
        this.mntOpt = undefined;
        this.mntTimeLeftMin = 0;
        this.logAdateLapsedMin = 0;
        //console.log('initFields -> aircraft:', this.aircraft);
        if (this.mac) {
            const productionRateMs = this.mac.productionRateH ? this.mac.productionRateH * 60 * 60 * 1000 : 0;
            this.nextProducedOn = new Date();
            if (this.mac.lastProducedOn) {
                this.nextProducedOn.setTime(new Date(this.mac.lastProducedOn).getTime() + productionRateMs);
            } else {
                this.nextProducedOn.setTime(this.nextProducedOn.getTime() + productionRateMs);
            }
        }
        this.pendingMntOpt = this.aircraft ? this.aircraft.getPendingMntOpt() : undefined;
        this.nextMntOpt = this.aircraft ? this.aircraft.getNextMntOpt() : undefined;
        this.nextMntTimeMin = this.aircraft ? this.aircraft.nextMntTimeMin() : undefined;
        this.mntRequired = this.aircraft ? this.aircraft.mntRequired : undefined;
        this.canStartMeintenance = (this.aircraft && this.aircraft.acStatusId === AircraftStatus.Operational &&
            this.aircraft?.acActionTypeId === AircraftActionType.PlaneStand);

        if (this.aircraft?.acActionTypeId == this.mntCheck) {
            this.mntOpt = this.aircraft.getPendingMntOpt();
            this.logAdateLapsedMin = this.aircraft.logAdateLapsedMin;
            this.mntTimeLeftMin = this.mntOpt.timeMin - this.logAdateLapsedMin;
        }
        //console.log('initFields -> aircraft:', this.aircraft);
        //&& this.aircraft?.homeApId != this.aircraft?.currApId 
        this.canTransfer = (this.aircraft &&
            this.aircraft?.acStatusId == AircraftStatus.Operational &&
            this.aircraft?.acActionTypeId == AircraftActionType.PlaneStand);
        try {
            this.minFromlogAdate = this.aircraft && this.aircraft.logAdate ? (new Date().getTime() - this.aircraft.logAdate.getTime()) / (60 * 1000) : 0;
        } catch (ex) {
            console.log('initFields -> ex:', ex);
        }
        if (this.mtn) {
            this.timeToMntH = ((this.mtn.startTime.getTime() - new Date().getTime()) / 3600000);
            this.canDelMtn = this.mtn &&
                this.aircraft.acActionTypeId !== AircraftActionType.MaintenanceCheck &&
                (this.timeToMntH > 1);
        } else {
            this.timeToMntH = 0;
            this.canDelMtn = false;
        }
        //console.log('initFields -> canDelMtn:', this.canDelMtn);
    }

    //#region Action Methods

    centerMapOnAirport(): void {
        console.log('centerMapOnAirport-> aircraft:', this.aircraft);
    }

    homeApEditClick() {
        if (this.aircraft.ownerAlId === this.cus.airline.alId) {

            const dialogRef = this.dialogService.open(AmsAircraftHomeApEditDialog, {
                width: '721px',
                //height: '520px',
                data: {
                    aircraft: this.aircraft,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                //console.log('pendingMnt-> res:', res);
                if (res && res > -1) {
                    this.loadData();
                }
                this.initFields();
            });
        }

    }

    transferEstimat(): void {
        if (this.canTransfer) {
            this.transferEstimate.emit(this.aircraft.homeApId);
        } else {
            this.toastr.info('Aircraft is bissy', 'Transfer Flight')
        }
    }

    setMnt(): void {
        const dialogRef = this.dialogService.open(AmsAcmpEditDialog, {
            width: '721px',
            //height: '520px',
            data: {
                aircraft: this.aircraft,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            //console.log('pendingMnt-> res:', res);
            if (res && res > -1) {
                this.loadData();
            }
            this.initFields();
        });
    }

    deleteMtn(): void {
        if (this.aircraft.ownerAlId === this.cus.airline.alId &&
            this.aircraft.nextMntId > 0) {
            const msg = `<p class="w-100 text-j">Please, confirm the deletion of <b>maintenance plan #${this.aircraft.nextMntId}</b>? </br>This can not be undone. </br>Would you like to continue?</p>`;
            this.cus.showMessage({ title: 'Delete Confirmation', message: msg }).then(res => {
                if (res) {
                    this.acClient.maintenanceDeleteP(this.aircraft.nextMntId)
                        .then((mtn: number) => {
                            this.mtn = undefined;
                            this.initFields();
                        });
                }
            });
        }
    }

    gotoAircraft() {
        if (this.aircraft) {
            this.acService.aircraft = this.aircraft;
            this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
        }
    }

    loadData(): void {

        if (this.aircraft && this.aircraft.acId) {
            this.acClient.aircraftGetP(this.aircraft.acId)
                .then((res: AmsAircraft) => {
                    this.aircraft = res;
                    if (this.aircraft.nextMntId > 0) {
                        this.acClient.maintenanceGetP(this.aircraft.nextMntId)
                            .then((mtn: AmsMaintenancePlan) => {
                                this.mtn = mtn;
                                this.initFields();
                            });
                    } else {
                        this.mtn = undefined;
                        this.initFields();
                    }

                });
        }
    }

    //#endregion

}
