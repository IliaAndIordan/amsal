import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardAircraftRowComponent } from './card-aircraft-row.component';

describe('Ams -> Share -> Card -> CardAircraftRowComponent', () => {
  let component: CardAircraftRowComponent;
  let fixture: ComponentFixture<CardAircraftRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardAircraftRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardAircraftRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
