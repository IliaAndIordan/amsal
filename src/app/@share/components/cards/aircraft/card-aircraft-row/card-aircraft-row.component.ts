import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { AircraftActionType } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-card-aircraft-row',
  templateUrl: './card-aircraft-row.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardAircraftRowComponent implements OnInit {

  @Input() aircraft: AmsAircraft;
  @Input() showFh:boolean = true;
  

  acImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'icon-ac-model.png';
  mntCheck = AircraftActionType.MaintenanceCheck;
  
  constructor(private cus: CurrentUserService,) { }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['aircraft']) {
      this.initFields();
    }
    if (changes['showFh']) {
      this.initFields();
    }

  }

  initFields() {

  }

  spmAircraftOpen(): void {
    if (this.aircraft?.acId) {
      this.cus.spmAircraftPanelOpen.next(this.aircraft.acId);
    }
  }
}