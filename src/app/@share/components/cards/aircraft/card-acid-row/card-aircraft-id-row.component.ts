import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { EnumViewModel } from 'src/app/@core/models/common/enum-view.model';
import { AircraftActionType } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMaintenanceType } from 'src/app/@core/services/api/aircraft/enums';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';

@Component({
  selector: 'ams-card-aircraft-id-row',
  templateUrl: './card-aircraft-id-row.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardAircraftIdRowComponent implements OnInit {

  @Input() acId: number;
  @Input() aircraft: AmsAircraft;

  acImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'icon-ac-model.png';
  aircraft$: Observable<AmsAircraft>;


  mntCheck = AircraftActionType.MaintenanceCheck;
  acatFlight = AircraftActionType.Flight;
  nextMntOpt: AmsMaintenanceType;
  nextMntTimeMin: number;

  pendingMntOpt: AmsMaintenanceType;
  mntOpt: AmsMaintenanceType;
  mntTimeLeftMin: number;
  logAdateLapsedMin: number;

  constructor(
    private router: Router,
    private acCache: AmsAirlineAircraftCacheService,
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private acService: AmsAirlineAircraftService) { }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['acId']) {
      this.initFields();
    }
    if (changes['aircraft']) {
      this.acId = this.aircraft?.acId;
      this.initFields();
    }

  }

  initFields(): void {
    this.mntOpt = undefined;
    this.mntTimeLeftMin = 0;
    this.logAdateLapsedMin = 0;
    if (this.acId) {
      this.aircraft$ = this.acCache.getAircraft(this.acId);
      this.aircraft$.subscribe(ac => {
        this.aircraft = ac;
        this.initAircraft();
      });
    } else{
      this.initAircraft();
    }
  }

  initAircraft(): void {

    if (this.aircraft) {
      this.pendingMntOpt = this.aircraft ? this.aircraft.getPendingMntOpt() : undefined;
      this.nextMntOpt = this.aircraft ? this.aircraft.getNextMntOpt() : undefined;
      this.nextMntTimeMin = this.aircraft ? this.aircraft.nextMntTimeMin() : undefined;
      if (this.aircraft?.acActionTypeId == this.mntCheck) {
        this.mntOpt = this.aircraft.getPendingMntOpt();
        this.logAdateLapsedMin = this.aircraft.logAdateLapsedMin;
        this.mntTimeLeftMin = this.mntOpt.timeMin - this.logAdateLapsedMin;
      }
    }
  }

  gotoAircraft() {
    if (this.aircraft) {
      this.acService.aircraft = this.aircraft;
      this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
    }
  }

  spmAircraftOpen(): void {
    if (this.aircraft?.acId) {
      this.cus.spmAircraftPanelOpen.next(this.aircraft.acId);
    }
  }

}