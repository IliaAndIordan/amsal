import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CardAircraftIdRowComponent } from './card-aircraft-id-row.component';


describe('Ams -> Share -> Card -> CardAircraftIdRowComponent', () => {
  let component: CardAircraftIdRowComponent;
  let fixture: ComponentFixture<CardAircraftIdRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardAircraftIdRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardAircraftIdRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
