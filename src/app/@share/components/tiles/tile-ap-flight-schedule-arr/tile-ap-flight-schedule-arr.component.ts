import { CommonModule } from '@angular/common';
import { Component, Input, type OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleTableCriteria, AmsWeeklyFlights } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { TileApFlightScheduleArrDataSource } from './tile-ap-flight-schedule-arr.datasource';
import { AmsWeekdaysOpt } from 'src/app/@core/services/api/airline/enums';

@Component({
  selector: 'ams-tile-ap-flight-schedule-arr',
  templateUrl: './tile-ap-flight-schedule-arr.component.html',
  providers: [TileApFlightScheduleArrDataSource]
})
export class TileApFlightScheduleArrComponent implements OnInit {

  @Input() apId: number;

  imgUrlFlArr = URL_COMMON_IMAGE_AMS_COMMON + 'landing.png';
  imgUrlFlDep = URL_COMMON_IMAGE_AMS_COMMON + 'departure.svg';
  weekdaysOpt = AmsWeekdaysOpt;

  user: UserModel;
  airline: AmsAirline;

  criteriaChanged: Subscription;
  criteria: AmsAlFlightNumberScheduleTableCriteria

  flightsChanged: Subscription;
  flightsCount: number;
  flights: AmsWeeklyFlights[];

  airport$: Observable<AmsAirport>;
  aircrafts$: Observable<AmsAircraft[]>;

  timerMinOne: Subscription;

  constructor(
    private cus: CurrentUserService,
    private acCache: AmsAirlineAircraftCacheService,
    public tableds: TileApFlightScheduleArrDataSource
  ) { }

  ngOnInit(): void {

    this.timerMinOne = this.cus.timerMinOneSubj.subscribe(min => {
      this.initFields();
      //this.loadData();
    });

    this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsAlFlightNumberScheduleTableCriteria) => {
      this.criteria = this.tableds.criteria;
      this.initFields();
      this.loadData();
    });

    this.aircrafts$ = this.acCache.aircrafts;
    this.aircrafts$.subscribe(aircrafts => {
      this.initFields();
    });

    this.initFields();
    this.criteria.arrApId = this.apId;
    this.criteria.depApId = undefined;
    this.criteria.alId = undefined;
    this.criteria.grpId = undefined;
    this.criteria.wdId = undefined;
    this.tableds.criteria = this.criteria;
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['apId']) {
      this.criteria = this.tableds.criteria;
      this.criteria.arrApId = this.apId;
      this.criteria.depApId = undefined;
      this.criteria.alId = undefined;
      this.criteria.grpId = undefined;
      this.criteria.wdId = undefined;
      this.tableds.criteria = this.criteria;
    }
  }


  initFields(): void {
    this.user = this.cus.user;
    this.airline = this.cus.airline;
    this.flightsCount = this.tableds.itemsCount;
    this.flights = this.tableds.data;
    this.criteria = this.tableds.criteria;
  }

  loadData() {
    //this.spinerService.display(true);
    this.tableds.loadData()
      .subscribe(res => {
        this.flightsCount = this.tableds.itemsCount;
        this.initFields();
      });

  }
}
