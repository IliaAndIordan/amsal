import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleTableCriteria, AmsWeeklyFlights, ResponceAmsAlFlightNumberScheduleTableData } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

export const KEY_CRITERIA = 'ams-tile-ap-flight-schedule-arr-criteria-v01';

@Injectable({
    providedIn: 'root'
})
export class TileApFlightScheduleArrDataSource extends DataSource<AmsWeeklyFlights> {

    seleted: AmsWeeklyFlights;

    private _flpnsData: Array<AmsAlFlightNumberSchedule>;
    private _data:AmsWeeklyFlights[];
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsWeeklyFlights[]> = new BehaviorSubject<AmsWeeklyFlights[]>([]);
    listSubject = new BehaviorSubject<AmsWeeklyFlights[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }


    get data(): Array<AmsWeeklyFlights> {
        return this._data;
    }

    set data(value: Array<AmsWeeklyFlights>) {
        this._data = value;
        this.listSubject.next(this._data as AmsWeeklyFlights[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria
    public criteriaChanged = new Subject<AmsAlFlightNumberScheduleTableCriteria>();

    public get criteria(): AmsAlFlightNumberScheduleTableCriteria {
        const objStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsAlFlightNumberScheduleTableCriteria;
        if (objStr) {
            obj = Object.assign(new AmsAlFlightNumberScheduleTableCriteria(), JSON.parse(objStr));
            if (obj && obj.alId) {
                obj.alId = undefined;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }
        } else {
            obj = new AmsAlFlightNumberScheduleTableCriteria();
            obj.limit = 1000;
            obj.offset = 0;
            //obj.alId = this.cus.isAdmin ? undefined : this.cus.airline.alId;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsAlFlightNumberScheduleTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsWeeklyFlights[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsWeeklyFlights>> {

        this.isLoading = true;

        return new Observable<Array<AmsWeeklyFlights>>(subscriber => {

            this.alClient.alFlightNumberScheduleTableP(this.criteria)
            .then((resp: ResponceAmsAlFlightNumberScheduleTableData) => {
                //console.log('loadData-> resp=', resp);
                this._flpnsData = new Array<AmsAlFlightNumberSchedule>();
                if (resp) {
                    if (resp && resp.flnschedules && resp.flnschedules.length > 0) {
                        this._flpnsData = resp.flnschedules;
                        this.data = AmsWeeklyFlights.fromAmsAlFlightNumberSchedule(this._flpnsData);
                        //console.log('loadData-> weekFlights=', this.data);
                    }
                    this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                } else{
                    this.itemsCount = this.data?this.data.length:0;
                }
                this.listSubject.next(this.data);
                subscriber.next(this.data);

                this.isLoading = false;
            }, msg => {
                console.log('loadData -> ' + msg);

                this.itemsCount = 0;
                this.data = new Array<AmsWeeklyFlights>();
                this.isLoading = false;

                // component.errorMessage = msg;
            });


        });

    }
    //#endregion
}
