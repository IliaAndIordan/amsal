import { Component, Input, OnChanges, OnDestroy, SimpleChanges, type OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight, AmsFlightTableCriteria } from 'src/app/@core/services/api/flight/dto';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { TileAlAcFlightQueueDataSource } from './tile-al-ac-flight-queue.datasource';

@Component({
  selector: 'ams-tile-al-ac-flight-queue',
  templateUrl: './tile-al-ac-flight-queue.component.html',
})
export class TileAlAcFlightQueueComponent implements OnInit, OnDestroy, OnChanges {

  @Input() acId: number;

  imgUrlFlq = URL_COMMON_IMAGE_AMS_COMMON + 'departure.svg';

  user: UserModel;
  airline: AmsAirline;

  criteriaChanged: Subscription;
  criteria: AmsFlightTableCriteria

  flightsChanged: Subscription;
  flightsCount: number;
  flights: AmsFlight[];

  airport$: Observable<AmsAirport>;
  aircrafts$: Observable<AmsAircraft[]>;

  timerMinOne: Subscription;

  constructor(
    private cus: CurrentUserService,
    private acCache: AmsAirlineAircraftCacheService,
    public tableds: TileAlAcFlightQueueDataSource
  ) { }

  ngOnInit(): void {


    this.flightsChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsFlight>) => {
      this.initFields();
      if (this.flightsCount > this.criteria.limit) {
        this.criteria.limit = this.flightsCount + 12;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
      }
    });

    this.timerMinOne = this.cus.timerMinOneSubj.subscribe(min => {
      this.initFields();
      this.loadData();
    });

    this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsFlightTableCriteria) => {
      this.criteria = this.tableds.criteria;
      this.initFields();
      this.loadData();
    });

    this.aircrafts$ = this.acCache.aircrafts;
    this.aircrafts$.subscribe(aircrafts => {
      this.initFields();
    });

    this.initFields();
    this.criteria.acId = this.acId;
    this.criteria.alId = undefined;
    this.tableds.criteria = this.criteria;
  }

  ngOnDestroy(): void {
    if (this.timerMinOne) { this.timerMinOne.unsubscribe(); }
    if (this.flightsChanged) { this.flightsChanged.unsubscribe(); }
    if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['acId']) {
      this.initFields();
      this.tableds.criteria = this.criteria;
    }
  }

  initFields(): void {
    this.user = this.cus.user;
    this.airline = this.cus.airline;

    this.criteria = this.tableds.criteria;
    this.criteria.acId = this.acId;
    this.criteria.alId = this.acId ? undefined : this.cus.airline.alId;

    this.flightsCount = this.tableds.itemsCount;
    this.flights = this.tableds.data;
  }

  loadData() {
    //this.spinerService.display(true);
    this.tableds.loadData()
      .subscribe(res => {
        this.flightsCount = this.tableds.itemsCount;
        this.initFields();
      });

  }
}


