import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightTableData } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

export const KEY_CRITERIA = 'ams_tile-al-ac-flight-queue_criteria';

@Injectable({
  providedIn: 'root'
})
export class TileAlAcFlightQueueDataSource extends DataSource<AmsFlight>{

  seleted: AmsFlight;

  private _data: Array<AmsFlight>;
  private _isLoading = false;

  numberOfPages = 0;
  itemsCount = 0;

  dataChange: BehaviorSubject<AmsFlight[]> = new BehaviorSubject<AmsFlight[]>([]);
  listSubject = new BehaviorSubject<AmsFlight[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();

  get isLoading(): boolean {
      return this._isLoading;
  }

  set isLoading(value: boolean) {
      this._isLoading = value;
      this.loadingSubject.next(this._isLoading);
  }

  get data(): Array<AmsFlight> {
      return this._data;
  }

  set data(value: Array<AmsFlight>) {
      this._data = value;
      this.listSubject.next(this._data as AmsFlight[]);
  }

  constructor(
      private cus: CurrentUserService,
      private flClient: AmsFlightClient,) {
      super();
  }

  //#region criteria

  public criteriaChanged = new Subject<AmsFlightTableCriteria>();

  public get criteria(): AmsFlightTableCriteria {
      const objStr = localStorage.getItem(KEY_CRITERIA);
      let obj: AmsFlightTableCriteria;
      if (objStr) {
          obj = Object.assign(new AmsFlightTableCriteria(), JSON.parse(objStr));
          if(!obj.alId || obj.alId !== this.cus.airline.alId){
              obj.alId = this.cus.airline.alId;
              localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
          }
          if(this.cus.isAdmin){
            obj.alId = undefined;
          }
          
      } else {
          obj = new AmsFlightTableCriteria();
          obj.limit = 50;
          obj.offset = 0;
          obj.sortCol = 'dtime';
          obj.sortDesc = false;
          obj.alId = this.cus.isAdmin?undefined:this.cus.airline.alId;
          localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
      }
      return obj;
  }

  public set criteria(obj: AmsFlightTableCriteria) {
      if (obj) {
          localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
      } else {
          localStorage.removeItem(KEY_CRITERIA);
      }
      this.criteriaChanged.next(obj);
  }

  //#endregion

  //#region DataSource Interface Methods

  connect(collectionViewer: CollectionViewer): Observable<AmsFlight[]> {
      // return this.userListSubject.asObservable();
      return this.listSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
      // this.userListSubject.complete();
      // this.loadingSubject.complete();
  }


  loadData(): Observable<Array<AmsFlight>> {
      
      this.isLoading = true;
      
      return new Observable<Array<AmsFlight>>(subscriber => {
          
          this.flClient.flightsQueueTable(this.criteria)
              .subscribe((resp: AmsFlightTableData) => {
                  //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                  this.data = new Array<AmsFlight>();

                  if (resp && resp.flights && resp.flights.length > 0) {
                      this.data = resp.flights
                      this.itemsCount = resp.rowsCount?resp.rowsCount:0;
                  }
                  this.listSubject.next(this.data);
                  subscriber.next(this.data);
                  this.isLoading = false;
              }, msg => {
                  console.log('loadData -> ' + msg);

                  this.itemsCount = 0;
                  this.data = new Array<AmsFlight>();
                  this.isLoading = false;

                  // component.errorMessage = msg;
              });

      });

  }
  //#endregion
}
