import { Component, Input, OnChanges, type OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';

const LABELS = ['Transfer', 'Charter', 'Schedule'];
const DATA = [0, 0, 0];

@Component({
  selector: 'ams-tile-al-compleated-flights-chart',
  templateUrl: './tile-al-compleated-flights-chart.component.html',
})
export class AmsTileAlCompleatedFlightsChartComponent implements OnInit, OnChanges {

  @Input() airline: AmsAirline;
  @Input() loading$: Observable<boolean>;

  chartData = DATA;
  chartLabels = LABELS;
  tileImgUrl: string = URL_COMMON_IMAGE_TILE + '/barcode-white.png';

  constructor() { }

  ngOnInit(): void {
    this.initFields()
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['airline']) {
      this.initFields();
    }
  }

  initFields() {
    if (!this.tileImgUrl) {
      this.tileImgUrl = URL_COMMON_IMAGE_TILE + '/barcode-white.png';
    }
    this.getChartData();
  }

  getChartData(): void {
    console.log('getChartData -> airline:', this.airline);
    if (this.airline) {
      this.chartData = this.airline.getCompleateFlightPieChartData();
      this.chartLabels = this.airline.getCompleateFlightPieChartLabels();
    } else {
      this.chartData = DATA;
      this.chartLabels = LABELS;
    }
    console.log('getChartData -> chartData:', this.chartData);
  }

  chartClicked(event: any) {
    console.log('chartClicked -> event:', event);
  }

}
