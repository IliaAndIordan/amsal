import { Component, Input, OnChanges, type OnInit } from '@angular/core';
import { ChartConfiguration } from 'chart.js';
import { Observable } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';

const LABELS = ['Transfer', 'Charter', 'Schedule'];
const DATA = [{data:[0, 0, 0]}];

@Component({
  selector: 'ams-mini-chart-al-compleated-flights',
  templateUrl: './mini-chart-al-compleated-flights.component.html',
})
export class AmsMiniChartAlCompleatedFlightsComponent implements OnInit, OnChanges {

  @Input() airline: AmsAirline;
  @Input() loading$: Observable<boolean>;

  roots = AppRoutes;
  chartData:any = DATA;
  chartLabels = LABELS;
  tileImgUrl: string = URL_COMMON_IMAGE_TILE + '/barcode-white.png';
  logoUrl: string;
  chartOptions: ChartConfiguration['options'] = {
    color: 'white',
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
        title: {
            display: false,
            color: 'white',
        },
        legend: {
            display: false,
            position: 'chartArea',
        },
    },
  };


  constructor() { }

  ngOnInit(): void {
    this.initFields()
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['airline']) {
      this.initFields();
    }
  }

  initFields() {
    this.logoUrl = this.airline?.logo ? this.airline.logo : this.airline?.amsLogoUrl;
    if (!this.tileImgUrl) {
      this.tileImgUrl = URL_COMMON_IMAGE_TILE + '/barcode-white.png';
    }
    this.getChartData();
  }

  getChartData(): void {
    if (this.airline) {
      this.chartData = this.airline.getCompleateFlightPieChartData();
      this.chartLabels = this.airline.getCompleateFlightPieChartLabels();
    } else {
      this.chartData = DATA;
      this.chartLabels = LABELS;
    }
  }

  chartClicked(event: any) {
    console.log('chartClicked -> event:', event);
  }
}
