import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TileTrSumLastWeekComponent } from './tile-tr-sum-last-week.component';

describe('TileTrSumLastWeekComponent', () => {
  let component: TileTrSumLastWeekComponent;
  let fixture: ComponentFixture<TileTrSumLastWeekComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TileTrSumLastWeekComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TileTrSumLastWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
