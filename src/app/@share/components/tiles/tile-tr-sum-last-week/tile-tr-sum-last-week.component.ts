import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AmsTransactionsSum, AmsTransactionsSumLastWeekData } from 'src/app/@core/services/api/airline/transaction';

const LABELS = ['Mon', 'Tue', 'Wen', 'Thu', 'Fri', 'Sat', 'Sun'];
const DATA = [
  { data: [0, 0, 0, 0, 0, 0, 0], label: 'Income' },
  { data: [0, 0, 0, 0, 0, 0, 0], label: 'Revenue' },
  { data: [0, 0, 0, 0, 0, 0, 0], label: 'Expences' }
];

@Component({
  selector: 'ams-tile-tr-sum-last-week',
  templateUrl: './tile-tr-sum-last-week.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class TileTrSumLastWeekComponent implements OnInit, OnChanges {

  @Input() trSumLastWeek: AmsTransactionsSumLastWeekData;
  @Input() loading$: Observable<boolean>;
  @Input() tileImgUrl: string;
  @Output() chartCliched: EventEmitter<number> = new EventEmitter<number>();
  @Output() refreshData: EventEmitter<any> = new EventEmitter<any>();
  
  trSumLastWeekChanged: Subscription;
  trSumLastWeekData = DATA;
  trSumLastWeekLabels = LABELS;

  constructor() { }

  ngOnInit(): void {
    this.initFields()
  }

  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['trSumLastWeek']) {
        //this.trSumLastWeek = changes['trSumLastWeek'].currentValue;
        this.initFields();
    }
    if (changes['tileImgUrl']) {
      //this.trSumLastWeek = changes['tileImgUrl'].currentValue;
      this.initFields();
  }
}

  initFields() {
    if(!this.tileImgUrl){
      this.tileImgUrl = URL_COMMON_IMAGE_TILE + '/barcode-white.png';
    }
    this.getTransactionsSumLastWeek();
  }

  getTransactionsSumLastWeek(): void {
    
    if (this.trSumLastWeek && this.trSumLastWeek.trSumList &&
      this.trSumLastWeek.trSumList.length > 0) {
      const data = AmsTransactionsSum.dataToList(this.trSumLastWeek);
      this.trSumLastWeekData = AmsTransactionsSum.getLineChartData(data);
      this.trSumLastWeekLabels = AmsTransactionsSum.getLineChartLabels(data);
    } else {
      this.trSumLastWeekData = DATA;
      this.trSumLastWeekLabels = LABELS;
    }
    //console.log('getTransactionsSumLastWeek -> trSumLastWeekData:', this.trSumLastWeekData);
    //console.log('getTransactionsSumLastWeek -> trSumLastWeekLabels:', this.trSumLastWeekLabels);
  }

  chartClicked(event:any){
    console.log('chartClicked -> event:', event);
  }

}
