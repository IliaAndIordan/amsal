import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../material.module';
import { SxComponentsCardsModule } from '../cards/components-cards.module';
import { SxComponentsCommonModule } from '../common/components-common.module';
import { TileTrSumLastWeekComponent } from './tile-tr-sum-last-week/tile-tr-sum-last-week.component';
import { AmsChartsModule } from '../../charts/ams-charts.module';
import { AmsTileAlCompleatedFlightsChartComponent } from './tile-al-compleated-flights-chart/tile-al-compleated-flights-chart.component';
import { AmsMiniChartAlCompleatedFlightsComponent } from './mini-chart-al-compleated-flights/mini-chart-al-compleated-flights.component';
import { RouterModule } from '@angular/router';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SxPipesModule,
    SxMaterialModule,
    AmsChartsModule,
    //
    SxComponentsCommonModule,
    SxComponentsCardsModule,
  ],
  declarations: [
    TileTrSumLastWeekComponent,
    AmsTileAlCompleatedFlightsChartComponent,
    AmsMiniChartAlCompleatedFlightsComponent,
  ],
  exports: [
    TileTrSumLastWeekComponent,
    AmsTileAlCompleatedFlightsChartComponent,
    AmsMiniChartAlCompleatedFlightsComponent,
  ],
})
export class AmsCommonTilesModule { }
