import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxComponentsCommonModule } from './common/components-common.module';
import { SxMaterialModule } from './material.module';
import { SxUseCaseEditDialog } from './dialogs/usecase-edit/usecase-edit.dialog';
import { AmsUserEditDialog } from './dialogs/user-edit/user-edit.dialog';
import { AmsCountryEditDialog } from './dialogs/country-edit/country-edit.dialog';
import { SxComponentsCardsModule } from './cards/components-cards.module';
import { AmsStateEditDialog } from './dialogs/state-edit/state-edit.dialog';
import { AmsCityEditDialog } from './dialogs/city-edit/city-edit.dialog';
import { AmsAirportEditDialog } from './dialogs/airport-edit/airport-edit.dialog';
import { AmsRunwayEditDialog } from './dialogs/runway-edit/runway-edit.dialog';
import { AmsManufacturerEditDialog } from './dialogs/mfr-edit/mfr-edit.dialog';
import { AmsMacEditDialog } from './dialogs/mac-edit/mac-edit.dialog';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { GetJsonDataDialog } from './dialogs/get-json-data/get-jeon-data.dialog';
import { AmsMacCabinEditDialog } from './dialogs/mac-cabin-edit/mac-cabin-edit.dialog';
import { InfoMessageDialog } from './dialogs/info-message/info-message.dialog';
import { AmsMfrAirportEditDialog } from './dialogs/mfr-airport-edit/mfr-airport-edit.dialog';
import { AmsUserNameEditDialog } from './dialogs/user-edit/user-name-edit.dialog';
import { AmsAirlineEditDialog } from './dialogs/airline-edit/airline-edit.dialog';
import { SpmModule } from './spm/spm.module';
import { AmsFlightEstimateDialog } from './dialogs/flight/flight-estimate/flight-estimate.dialog';
import { AmsFlightCharterEstimateDialog } from './dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { AmsCfgCharterEditDialog } from './dialogs/charter/cfg-charter-edit/cfg-charter-edit.dialog';
import { AmsCommonTilesModule } from './tiles/ams-common-tiles.module';
import { AmsCommonGridModule } from './grids/ams-common-grids.module';
import { AmsAcmpEditDialog } from './dialogs/acmp-create/acmp-edit.dialog';
import { AmsHubEditDialog } from './dialogs/al-hub-edit/al-hub-edit.dialog';
import { AmsSheduleGroupEditDialog } from './dialogs/al-flp-group-edit/al-flp-group-edit.dialog';
import { AmsAlSheduleRouteEditDialog } from './dialogs/al-flp-route-edit/al-flp-route-edit.dialog';
import { AmsMapsModule } from '../maps/ams-maps.module';
import { AmsAlFlightNumberEditDialog } from './dialogs/al-flp-number-edit/al-flp-number-edit.dialog';
import { AmsAlFlightNumberPriceEditDialog } from './dialogs/al-flp-number-edit/al-flp-number-price-edit.dialog';
import { AmsAlFlightNumberScheduleEditDialog } from './dialogs/al-flpn-schedule-edit/al-flpn-schedule-edit.dialog';
import { AmsAlpGroupWeekdayScheduleCopyDialog } from './dialogs/al-flp-group-weekday-schedule-copy/al-flp-group-weekday-schedule-copy.dialog';
import { TileAlAcFlightComponent } from './tiles/tile-al-ac-flight/tile-al-ac-flight.component';
import { TileAlAcFlightQueueComponent } from './tiles/tile-al-ac-flight-queue/tile-al-ac-flight-queue.component';
import { TileApFlightScheduleArrComponent } from './tiles/tile-ap-flight-schedule-arr/tile-ap-flight-schedule-arr.component';
import { AmsCardWeeklyFlightComponent } from './cards/flight/card-weekly-flight/card-weekly-flight.component';
import { TileApFlightScheduleDepComponent } from './tiles/tile-ap-flight-schedule-dep/tile-ap-flight-schedule-dep.component';
import { AmsAircraftHomeApEditDialog } from './dialogs/mac-edit/ac-homeap-edit/ac-homeap-edit.dialog';



@NgModule({
    imports: [
        CommonModule,
        SxPipesModule,
        //
        SxMaterialModule,
        SxComponentsCommonModule,
        SxComponentsCardsModule,
        SpmModule,
        AmsCommonTilesModule,
        AmsCommonGridModule,
        AmsMapsModule,
    ],
    declarations: [
        GetJsonDataDialog,
        SxUseCaseEditDialog,
        AmsUserEditDialog,
        AmsCountryEditDialog,
        AmsStateEditDialog,
        AmsCityEditDialog,
        AmsAirportEditDialog,
        AmsRunwayEditDialog,
        AmsManufacturerEditDialog,
        AmsMacEditDialog,
        AmsMacCabinEditDialog,
        InfoMessageDialog,
        AmsMfrAirportEditDialog,
        AmsUserNameEditDialog,
        AmsAirlineEditDialog,
        AmsFlightEstimateDialog,
        AmsFlightCharterEstimateDialog,
        AmsCfgCharterEditDialog,
        AmsAcmpEditDialog,
        AmsHubEditDialog,
        AmsAircraftHomeApEditDialog,
        AmsSheduleGroupEditDialog,
        AmsAlSheduleRouteEditDialog,
        AmsAlFlightNumberEditDialog,
        AmsAlFlightNumberPriceEditDialog,
        AmsAlFlightNumberScheduleEditDialog,
        AmsAlpGroupWeekdayScheduleCopyDialog,
        TileAlAcFlightQueueComponent,
        TileAlAcFlightComponent,
        TileApFlightScheduleArrComponent,
        TileApFlightScheduleDepComponent,
        AmsCardWeeklyFlightComponent,
    ],
    exports: [
        SxMaterialModule,
        SxComponentsCommonModule,
        SxComponentsCardsModule,
        SpmModule,
        AmsCommonTilesModule,
        AmsCommonGridModule,
        AmsAlFlightNumberPriceEditDialog,
        TileAlAcFlightQueueComponent,
        TileAlAcFlightComponent,
        TileApFlightScheduleArrComponent,
        TileApFlightScheduleDepComponent,
        AmsCardWeeklyFlightComponent,
        AmsAircraftHomeApEditDialog,
    ]
})

export class SxComponentsModule { }
