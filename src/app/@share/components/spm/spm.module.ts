import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { SxComponentsCommonModule } from '../common/components-common.module';
import { SxComponentsCardsModule } from '../cards/components-cards.module';
import { SxMaterialModule } from '../material.module';
import { SpmAcmMacInfoComponent } from './acm/mac/spm-acm-mac-info.component';
import { SpmAircraftInfoComponent } from './aircraft/spm-aircraft-info.component';
import { SpmCfgCharterInfoComponent } from './cfg-charter/spm-cfg-charter-info.component';
import { SpmFleCharterComponent } from './flight/fle-charter/fle-charter.component';
import { SpmFlQueueComponent } from './flight/fl-queue/fl-queue.component';
import { SpmAirportInfoComponent } from '../spm/airport/spm-airport-info.component';
import { AmsSpmFlightHistoryComponent } from './flight/ams-spm-flight-history/ams-spm-flight-history.component';
import { AmsCommonGridModule } from "../grids/ams-common-grids.module";
import { SpmAirlineInfoComponent } from './airline/spm-airline-info.component';
import { SpmFlInfoComponent } from './flight/info/spm-fl-info.component';
import { SpmFlPlanComponent } from './flight/fl-plan/fl-plan.component';



@NgModule({
    declarations: [
        SpmAcmMacInfoComponent,
        SpmAircraftInfoComponent,
        SpmCfgCharterInfoComponent,
        SpmFleCharterComponent,
        SpmFlQueueComponent,
        SpmAirportInfoComponent,
        AmsSpmFlightHistoryComponent,
        SpmAirlineInfoComponent,
        SpmFlInfoComponent,
        SpmFlPlanComponent,
    ],
    exports: [
        SpmAcmMacInfoComponent,
        SpmAircraftInfoComponent,
        SpmCfgCharterInfoComponent,
        SpmFleCharterComponent,
        SpmFlQueueComponent,
        SpmAirportInfoComponent,
        AmsSpmFlightHistoryComponent,
        SpmAirlineInfoComponent,
        SpmFlInfoComponent,
        SpmFlPlanComponent,
    ],
    imports: [
        CommonModule,
        SxPipesModule,
        SxMaterialModule,
        //
        SxComponentsCommonModule,
        SxComponentsCardsModule,
        AmsCommonGridModule
    ]
})

export class SpmModule { }
