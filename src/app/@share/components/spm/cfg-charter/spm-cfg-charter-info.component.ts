import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AircraftStatus } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { AmsCfgCharter } from 'src/app/@core/services/api/flight/charter-dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-spm-cfg-charter-info',
    templateUrl: './spm-cfg-charter-info.component.html',
})
export class SpmCfgCharterInfoComponent implements OnInit, OnChanges {

    
    @Input() flight: AmsCfgCharter;
    @Output() transferEstimate: EventEmitter<number> = new EventEmitter<number>();

    /**
     * FIELDS
     */
    roots = AppRoutes;
    imagevUrl = URL_NO_IMG_SQ;
    ourApImg = URL_IMG_OUAP;
    ourApUrl: string;

    constructor( 
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['flight']) {
            this.flight = changes['flight'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.flight) {
            
        } 
    }

}
