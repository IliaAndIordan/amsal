import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpmAirlineInfoComponent } from './spm-airline-info.component';

describe('SpmAirlineInfoComponent', () => {
  let component: SpmAirlineInfoComponent;
  let fixture: ComponentFixture<SpmAirlineInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpmAirlineInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpmAirlineInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
