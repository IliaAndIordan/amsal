import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_IMG_OUAP, URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';

@Component({
  selector: 'ams-spm-airline-info',
  templateUrl: './spm-airline-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpmAirlineInfoComponent implements OnInit, OnChanges {


  @Input() airline: AmsAirline;
  //@Output() transferEstimate: EventEmitter<number> = new EventEmitter<number>();

  roots = AppRoutes;

  imageUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;
  ourApUrl: string;
  hqAirport: AmsAirport;

  constructor(
    private router: Router,
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private acService: AmsAirlineAircraftService) {
  }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['airline']) {
      //this.aircraft = changes['aircraft'].currentValue;
      this.initFields();
    }
  }

  initFields() {
    //console.log('initFields -> airline:', this.airline);
    if (this.airline) {
      this.apCache.getAirport(this.airline?.apId).subscribe(ap => {
        this.hqAirport = ap;
      })
      this.imageUrl = this.airline?.amsLogoUrl;
    } else {
      this.imageUrl = URL_NO_IMG_SQ;
    }
  }

  gotoAircraft() {
    console.log('gotoAircraft -> airline:', this.airline);
    if (this.airline) {
      //this.acService.aircraft = this.aircraft;
      this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
    }
  }

}
