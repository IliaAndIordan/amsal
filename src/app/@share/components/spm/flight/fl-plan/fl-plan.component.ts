import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Input, Output } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_NO_IMG_SQ, URL_IMG_OUAP } from 'src/app/@core/const/app-storage.const';
import { AmsAlFlightNumber, AmsAlFlpPayloadsCriteria, AmsAlFlpPayloadsData, AmsFlpPayloads } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsFlightPlanPayload } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';

@Component({
  selector: 'ams-spm-fl-plan',
  templateUrl: './fl-plan.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class SpmFlPlanComponent implements OnInit {

  @Input() flp: AmsFlightPlanPayload;

  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;
  ourApUrl: string;

  depAp: AmsAirport;
  arrAp: AmsAirport;
  staticMapUrl: string;

  payloads: AmsFlpPayloads[];
  payloadsCount: number;

 

  constructor(
    private spinner: SpinnerService,
    private cus: CurrentUserService,
    private alClient: AmsAirlineClient,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.loadAirports();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flp']) {
      //this.flight = changes['flight'].currentValue;
      this.loadAirports();
    }
  }

  initFields() {
    //console.log('ams-spm-fl-queue initFields() -> flight:', this.flight);
    if (this.flp) {
      this.staticMapUrl = AmsFlight.getStaticMapUrl(this.depAp, this.arrAp);
    }
  }

  loadAirports() {

    if (this.flp && this.flp.flpId) {
      this.spinner.show();
      const criteria: AmsAlFlpPayloadsCriteria = {
        flpId: this.flp.flpId,
        offset: 0,
        pageIndex: 0,
        limit: 1000,
      };
      const payloads$ = this.alClient.alFlpPayloads(criteria);
      payloads$.subscribe((data: AmsAlFlpPayloadsData) => {
        this.payloads = data?.payloads;
        this.payloadsCount = data.rowsCount ?? 0;

        this.apCache.getAirport(this.flp.dApId).subscribe(ap => {
          this.depAp = ap;
          this.apCache.getAirport(this.flp.aApId).subscribe(ap => {
            this.arrAp = ap;
            this.spinner.hide();
            this.initFields();
          });
        });

      });

    }
  }

}
