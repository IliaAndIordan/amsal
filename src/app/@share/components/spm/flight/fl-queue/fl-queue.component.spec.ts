import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpmFlQueueComponent } from './fl-queue.component';

describe('SpmFlQueueComponent', () => {
  let component: SpmFlQueueComponent;
  let fixture: ComponentFixture<SpmFlQueueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpmFlQueueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpmFlQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
