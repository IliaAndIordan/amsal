import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Input, Output } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_NO_IMG_SQ, URL_IMG_OUAP } from 'src/app/@core/const/app-storage.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-spm-fl-queue',
  templateUrl: './fl-queue.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class SpmFlQueueComponent implements OnInit {

  @Input() flight: AmsFlight;
  
  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;
  ourApUrl: string;

  depAp: AmsAirport;
  arrAp: AmsAirport;
  staticMapUrl:string;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.loadAirports();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flight']) {
      //this.flight = changes['flight'].currentValue;
      this.loadAirports();
    }
  }

  initFields() {
    //console.log('ams-spm-fl-queue initFields() -> flight:', this.flight);
    if (this.flight) {
      this.staticMapUrl = AmsFlight.getStaticMapUrl(this.depAp, this.arrAp);
    }
  }
  loadAirports() {
    if (this.flight) {
      this.apCache.getAirport(this.flight.depApId).subscribe(ap => {
        this.depAp = ap;
        this.apCache.getAirport(this.flight.arrApId).subscribe(ap => {
          this.arrAp = ap;
          this.initFields();
        });
      });
    }
  }

}
