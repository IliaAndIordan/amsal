import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmsSpmFlightHistoryComponent } from './ams-spm-flight-history.component';

describe('AmsSpmFlightHistoryComponent', () => {
  let component: AmsSpmFlightHistoryComponent;
  let fixture: ComponentFixture<AmsSpmFlightHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsSpmFlightHistoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AmsSpmFlightHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
