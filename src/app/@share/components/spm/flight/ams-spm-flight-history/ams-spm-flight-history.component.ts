import { ChangeDetectionStrategy, Component, Input, OnInit, OnChanges } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_IMG_OUAP, URL_NO_IMG_SQ } from 'src/app/@core/const/app-storage.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-spm-flight-history',
  templateUrl: './ams-spm-flight-history.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class AmsSpmFlightHistoryComponent implements OnInit, OnChanges {

  @Input() flight: AmsFlight;
  
  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;
  ourApUrl: string;

  depAp: AmsAirport;
  arrAp: AmsAirport;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flight']) {
      //this.flight = changes['flight'].currentValue;
      this.initFields();
    }
  }

  initFields() {
    if (this.flight) {
      this.apCache.getAirport(this.flight.depApId).subscribe(ap => {
        this.depAp = ap;
      });
      this.apCache.getAirport(this.flight.arrApId).subscribe(ap => {
        this.arrAp = ap;
      })
    }
  }

}
