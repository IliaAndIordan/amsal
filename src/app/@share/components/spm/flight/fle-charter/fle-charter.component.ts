import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Input, Output, OnChanges } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_NO_IMG_SQ, URL_IMG_OUAP } from 'src/app/@core/const/app-storage.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCfgCharter } from 'src/app/@core/services/api/flight/charter-dto';
import { AmsFlightEstimate } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';

@Component({
  selector: 'ams-spm-fle-charter',
  templateUrl: './fle-charter.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpmFleCharterComponent implements OnInit, OnChanges {

  @Input() flight: AmsFlightEstimate;
  @Output() saveEstimate: EventEmitter<number> = new EventEmitter<number>();
  
  roots = AppRoutes;
  imagevUrl = URL_NO_IMG_SQ;
  ourApImg = URL_IMG_OUAP;
  ourApUrl: string;

  depAp: AmsAirport;
  arrAp: AmsAirport;

  constructor(
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,) {
  }

  ngOnInit() {
    this.initFields();
  }


  ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
    if (changes['flight']) {
      this.flight = changes['flight'].currentValue;
      this.initFields();
    }
  }

  initFields() {
    if (this.flight) {
      this.apCache.getAirport(this.flight.depApId).subscribe(ap => {
        this.depAp = ap;
      });
      this.apCache.getAirport(this.flight.arrApId).subscribe(ap => {
        this.arrAp = ap;
      })
    }
  }

}
