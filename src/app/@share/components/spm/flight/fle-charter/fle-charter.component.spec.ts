import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FleCharterComponent } from './fle-charter.component';

describe('FleCharterComponent', () => {
  let component: FleCharterComponent;
  let fixture: ComponentFixture<FleCharterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FleCharterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FleCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
