import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpmFlInfoComponent } from './spm-fl-info.component';

describe('SpmFlInfoComponent', () => {
  let component: SpmFlInfoComponent;
  let fixture: ComponentFixture<SpmFlInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpmFlInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpmFlInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
