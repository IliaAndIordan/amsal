import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AircraftStatus } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';


@Component({
    selector: 'ams-spm-aircraft-info',
    templateUrl: './spm-aircraft-info.component.html',
})
export class SpmAircraftInfoComponent implements OnInit, OnChanges {


    @Input() aircraft: AmsAircraft;
    @Output() transferEstimate: EventEmitter<number> = new EventEmitter<number>();

    /**
     * FIELDS
     */
    roots = AppRoutes;
    active = AircraftStatus.Operational;
    imagevUrl = URL_NO_IMG_SQ;
    ourApImg = URL_IMG_OUAP;
    ourApUrl: string;

    constructor(
        private router: Router,
        private cus: CurrentUserService,
        private acService: AmsAirlineAircraftService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['aircraft']) {
            //this.aircraft = changes['aircraft'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.aircraft) {
            this.imagevUrl = this.aircraft.imgAirUrl;
        } else {
            this.imagevUrl = URL_NO_IMG_SQ;
        }
    }

    gotoAircraft() {
        if (this.aircraft) {
            this.acService.aircraft = this.aircraft;
            this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
        }
    }

}
