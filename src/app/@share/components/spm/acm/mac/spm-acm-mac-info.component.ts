import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS, URL_IMG_OUAP, URL_NO_IMG_SQ, WEB_OURAP_BASE_URL, WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AircraftStatus } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraft, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';


@Component({
    selector: 'ams-spm-acm-mac-info',
    templateUrl: './spm-acm-mac-info.component.html',
})
export class SpmAcmMacInfoComponent implements OnInit, OnChanges {

    @Input() acmmac: AmsMac;

    roots = AppRoutes;
    active = AircraftStatus.Operational;
    imagevUrl = URL_NO_IMG_SQ;
    ourApImg = URL_IMG_OUAP;
    ourApUrl: string;

    constructor(
        private cus: CurrentUserService) {
    }

    ngOnInit() {
        this.initFields();
    }


    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['acmmac']) {
            this.acmmac = changes['acmmac'].currentValue;
            this.initFields();
        }
    }

    initFields() {
        if (this.acmmac) {
            this.imagevUrl = this.acmmac.imgAirUrl;
        } else{
            this.imagevUrl = URL_NO_IMG_SQ;
        }
    }

}
