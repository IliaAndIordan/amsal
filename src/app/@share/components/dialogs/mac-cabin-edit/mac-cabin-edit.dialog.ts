import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAdminService } from 'src/app/admin/admin.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsStatus, AmsStatusOpt, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsMac, AmsMacCabin, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AcSeatType, AcSeatTypeOpt, AmsAcSeatType } from 'src/app/@core/models/pipes/ac-seat-type.pipe';
import { PAX_WEIGHT_KG, URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { AcTypeOpt, AmsAcType } from 'src/app/@core/models/pipes/ac-type.pipe';
import { AcIfeType, AcIfeTypeOpt, AmsAcIfeTypeO } from 'src/app/@core/models/pipes/ac-ife-type.pipe';



export interface IAmsMacCabinEdit {
    mac: AmsMac;
    cabin: AmsMacCabin;
}

@Component({
    templateUrl: './mac-cabin-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsMacCabinEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    pilotImgUrl = URL_COMMON_IMAGE_AMS_COMMON + 'pilot_m_1.png';
    seatsImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/seat_icon_3.png';
    seatEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_e.png';

    seatBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_b.png';
    seatFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_f.png';
    seatCImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_c_one.png';
    jumpOneImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/jump-seat-one-tr.png';


    seatTypes = AcSeatTypeOpt;
    seatTypesE = AcSeatTypeOpt.filter(x => x.class === 'E');
    seatTypesB = AcSeatTypeOpt.filter(x => x.class === 'B');
    seatTypesF = AcSeatTypeOpt.filter(x => x.class === 'F');
    seatTypesC = AcSeatTypeOpt.filter(x => x.class === 'C');
    seatTypesCId = AcSeatType.CrewJumpSeat;
    seatTypesGId = AcSeatType.CargoSpace;
    ifeOpt = AcIfeTypeOpt;
    mac: AmsMac;
    cabin: AmsMacCabin;
    cargoKg: number;
    selIfeType:AmsAcIfeTypeO;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private adminService: AmsAdminService,
        public dialogRef: MatDialogRef<IAmsMacCabinEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsMacCabinEdit) {
        this.mac = data.mac;
        this.cabin = data.cabin;
    }
    get cName() { return this.formGrp.get('cName'); }
    get seatE() { return this.formGrp.get('seatE'); }
    get seatEtypeId() { return this.formGrp.get('seatEtypeId'); }
    get seatB() { return this.formGrp.get('seatB'); }
    get seatBtypeId() { return this.formGrp.get('seatBtypeId'); }
    get seatF() { return this.formGrp.get('seatF'); }
    get seatFtypeId() { return this.formGrp.get('seatFtypeId'); }
    get seatCrew() { return this.formGrp.get('seatCrew'); }
    get confort() { return this.formGrp.get('confort'); }
    get price() { return this.formGrp.get('price'); }
    get maxIFEId() { return this.formGrp.get('maxIFEId'); }
    get isDefault() { return this.formGrp.get('isDefault'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('min') ? 'Field value to small ' + fname + '.' :
                field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                    field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.cargoKg = this.mac?.loadKg;

        this.formGrp = this.fb.group({
            cName: new UntypedFormControl(this.cabin ? this.cabin.cName : '', [Validators.required, Validators.maxLength(256)]),
            seatE: new UntypedFormControl(this.cabin && this.cabin.seatE ? this.cabin.seatE : 0, [Validators.required, Validators.min(0)]),
            seatB: new UntypedFormControl(this.cabin && this.cabin.seatB ? this.cabin.seatB : 0, [Validators.min(0)]),
            seatF: new UntypedFormControl(this.cabin && this.cabin.seatF ? this.cabin.seatF : 0, [Validators.min(0)]),
            seatEtypeId: new UntypedFormControl(this.cabin && this.cabin.seatEtypeId ? this.cabin.seatEtypeId : undefined),
            seatBtypeId: new UntypedFormControl(this.cabin && this.cabin.seatBtypeId ? this.cabin.seatBtypeId : undefined),
            seatFtypeId: new UntypedFormControl(this.cabin && this.cabin.seatFtypeId ? this.cabin.seatFtypeId : undefined),
            seatCrew: new UntypedFormControl(this.cabin && this.cabin.seatCrew ? this.cabin.seatCrew : 0, [Validators.min(1)]),
            confort: new UntypedFormControl(this.cabin && this.cabin.confort ? this.cabin.confort : 0.4, [Validators.max(10)]),
            price: new UntypedFormControl(this.cabin && this.cabin.price ? this.cabin.price : 1000, [Validators.min(1000)]),
            isDefault: new UntypedFormControl(this.cabin && this.cabin.isDefault ? this.cabin.isDefault : false),
            maxIFEId: new UntypedFormControl(this.cabin && this.cabin.maxIFEId ? this.cabin.maxIFEId : AcIfeType.Base),
        });
        this.calcPrice();
    }

    calcPrice() {
        let price = 1000;
        let confort = 1;
        let calcCargoKg = this.mac?.loadKg;
        const ifeType = this.maxIFEId.value ? this.ifeOpt.find(x => x.ifeId === this.maxIFEId.value) : undefined;
        this.selIfeType = ifeType;
        const totalSeat = this.totalSeat();
        //console.log('calcPrice -> totalSeat:', totalSeat);
        if (totalSeat && totalSeat > 0) {
            
            let seat = this.seatCrew.value ? this.seatCrew.value : 0;
            let seatTypeId = AcSeatType.Eurobusiness;
            if (seatTypeId && seat > 0) {
                const seatType = this.seatTypesB.find(x => x.seatTypeId === seatTypeId);
                // confort += seatType.confort * (seat / totalSeat);
                price += (seatType.price * seat);

            }
            //console.log('calcPrice -> price1:', price);
            //console.log('calcPrice -> confort1:', confort);
            seat = this.seatE.value ? this.seatE.value : 0;
            seatTypeId = this.seatEtypeId.value ? this.seatEtypeId.value : undefined;
            if (seatTypeId && seat > 0) {
                const seatType = this.seatTypesE.find(x => x.seatTypeId === seatTypeId);
                //console.log('calcPrice -> seatTypeE:', seatType);
                price += seatType.price * seat;
                //console.log('calcPrice -> seatType.confort F:', seatType.confort);
                if (ifeType) {
                    //console.log('calcPrice -> ifeType.stars E:', ifeType.stars);
                    price += (ifeType.priceInstalation * seat);
                    confort += ((seatType.confort * ifeType.stars) * (seat / totalSeat));
                } else {
                    confort += (seatType.confort * (seat / totalSeat));
                }
            }
            //console.log('calcPrice -> price E:', price);
            //console.log('calcPrice -> confort E:', confort);

            seat = this.seatB.value ? this.seatB.value : 0;
            seatTypeId = this.seatBtypeId.value ? this.seatBtypeId.value : undefined;
            if (seatTypeId && seat > 0) {
                const seatType = this.seatTypesB.find(x => x.seatTypeId === seatTypeId);
                //console.log('calcPrice -> seatType B:', seatType);
                price += seatType.price * seat;
                //console.log('calcPrice ->   (totalSeat/seat):', (seat / totalSeat));
                //console.log('calcPrice -> seatType.confort B:', seatType.confort);
                if (ifeType) {
                    //console.log('calcPrice -> ifeType.stars B:', ifeType.stars);
                    price += (ifeType.priceInstalation * seat);
                    confort += ((seatType.confort * ifeType.stars) * (seat / totalSeat));
                } else {
                    confort += (seatType.confort * (seat / totalSeat));
                }
            }
            //console.log('calcPrice -> price B:', price);
            //console.log('calcPrice -> confort B:', confort);
            seat = this.seatF.value ? this.seatF.value : 0;
            seatTypeId = this.seatFtypeId.value ? this.seatFtypeId.value : undefined;
            if (seat > 0) {
                const seatType = this.seatTypesF.find(x => x.seatTypeId === seatTypeId);
                price += seatType.price * seat;
                //console.log('calcPrice -> seatType.confort F:', seatType.confort);
                if (ifeType) {
                    //console.log('calcPrice -> ifeType.stars F:', ifeType.stars);
                    price += (ifeType.priceInstalation * seat);
                    confort += ((seatType.confort * ifeType.stars) * (seat / totalSeat));
                } else {
                    confort += (seatType.confort * (seat / totalSeat));
                }

            }
            seat = this.seatCrew.value ? this.seatCrew.value : 0;
            if (seat > 0) {
                const seatType = AcSeatTypeOpt.find(x => x.seatTypeId === this.seatTypesCId);
                price += seatType.price * seat;
                //console.log('calcPrice -> confort before crew:', confort);
                if (ifeType) {
                    confort = (confort*(1 + (seat*0.1)));
                    //console.log('calcPrice -> confort afer crew:', confort);
                } 
            }
            //console.log('calcPrice -> totalSeat:', totalSeat);
            //console.log('calcPrice -> price:', price);
            //console.log('calcPrice -> confort:', confort);
            calcCargoKg = (this.mac.loadKg - (totalSeat*PAX_WEIGHT_KG));
            //console.log('calcPrice -> calcCargoKg:', confort);
        }
        this.cargoKg = calcCargoKg;
        if (price) { this.formGrp.patchValue({ 'price': price.toFixed(2) }) };
        if (confort) { this.formGrp.patchValue({ 'confort': confort.toFixed(1) }) };
        this.formGrp.updateValueAndValidity();
    }

    totalSeat(): number {
        let seat = 0;
        seat = this.seatE.value ? this.seatE.value : 0;
        seat += this.seatB.value ? this.seatB.value : 0;
        seat += this.seatF.value ? this.seatF.value : 0;
        seat += this.seatCrew.value ? this.seatCrew.value : 0;
        if (seat === 0) {
            seat = this.mac?.maxSeating??0;
        } 
        return seat;
    }

    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.cabin) {
                this.cabin = new AmsMacCabin();
            }
            const message = this.cabin.cabinId > 0 ? 'Update' : 'Create';
            this.cabin.macId = this.adminService.mac.macId;
            this.cabin.cName = this.cName.value;
            this.cabin.seatE = this.seatE.value;
            this.cabin.seatEtypeId = this.seatEtypeId.value;
            this.cabin.seatB = this.seatB.value;
            this.cabin.seatBtypeId = this.seatBtypeId.value;
            this.cabin.seatF = this.seatF.value;
            this.cabin.seatFtypeId = this.seatFtypeId.value;
            this.cabin.seatCrew = this.seatCrew.value;
            this.cabin.confort = this.confort.value;
            this.cabin.price = this.price.value;
            this.cabin.isDefault = this.isDefault.value;
            this.cabin.maxIFEId = this.maxIFEId.value ? this.ifeOpt.find(x => x.ifeId === this.maxIFEId.value).ifeId : AcIfeType.Base;
            // this.spinerService.display(true);

            this.client.macCabinSave(this.cabin)
                .subscribe((res: AmsMacCabin) => {
                    // this.spinerService.display(false);
                    //console.log('countrySave -> res:', res);
                    if (res && res.cabinId) {
                        this.cabin = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Cabin', 'Operation Succesfull: Cabin ' + message);
                        this.dialogRef.close(this.cabin);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Cabin ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
