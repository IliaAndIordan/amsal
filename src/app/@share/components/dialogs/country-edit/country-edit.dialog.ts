import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { AmsSubregionService } from 'src/app/wad/subregion/subregion.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCountry } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';


export interface IAmsCountryEdit {
    country: AmsCountry;
}

@Component({
    templateUrl: './country-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsCountryEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    country: AmsCountry;
    amsStatusOpt = AmsStatusOpt;
    wadStatusOpt = WadStatusOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsCountryClient,
        private subregService: AmsSubregionService,
        public dialogRef: MatDialogRef<IAmsCountryEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsCountryEdit) {
        this.country = data.country;
    }
    get cCode() { return this.formGrp.get('cCode'); }
    get cName() { return this.formGrp.get('cName'); }
    get iso2() { return this.formGrp.get('iso2'); }
    get iso3() { return this.formGrp.get('iso3'); }
    get icao() { return this.formGrp.get('icao'); }
    get wikiUrl() { return this.formGrp.get('wikiUrl'); }
    get imageUrl() { return this.formGrp.get('imageUrl'); }
    get amsStatus() { return this.formGrp.get('amsStatus'); }
    get wadStatus() { return this.formGrp.get('wadStatus'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.formGrp = this.fb.group({
            cCode: new UntypedFormControl(this.country ? this.country.cCode : '', [Validators.maxLength(5)]),
            cName: new UntypedFormControl(this.country ? this.country.cName : '', [Validators.required, Validators.maxLength(256)]),
            iso2: new UntypedFormControl(this.country ? this.country.iso2 : '', [Validators.required, Validators.maxLength(2)]),
            iso3: new UntypedFormControl(this.country ? this.country.iso3 : '', [Validators.maxLength(3)]),
            icao: new UntypedFormControl(this.country ? this.country.icao : '', [Validators.maxLength(8)]),
            wikiUrl: new UntypedFormControl(this.country ? this.country.wikiUrl : '', [Validators.maxLength(500)]),
            imageUrl: new UntypedFormControl(this.country ? this.country.imageUrl : '', [Validators.maxLength(1000)]),
            amsStatus: new UntypedFormControl(this.country && this.country.amsStatus ? this.country.amsStatus : AmsStatus.DataCheckRequired, [Validators.required]),
            wadStatus: new UntypedFormControl(this.country && this.country.wadStatus ? this.country.wadStatus : WadStatus.ToDo, [Validators.required]),
        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.country) {
                this.country = new AmsCountry();
                this.country.regionId = this.subregService.subregion.regionId;
                this.country.subregionId = this.subregService.subregion.subregionId;
            }
            const message = this.country.countryId > 0 ? 'Update' : 'Create';

            this.country.cCode = this.cCode.value;
            this.country.cName = this.cName.value;
            this.country.iso2 = this.iso2.value;
            this.country.iso3 = this.iso3.value;
            this.country.icao = this.icao.value;
            this.country.wikiUrl = this.wikiUrl.value;
            this.country.imageUrl = this.imageUrl.value;
            this.country.amsStatus = this.amsStatus.value;
            this.country.wadStatus = this.wadStatus.value;

            // this.spinerService.display(true);

            this.client.countrySave(this.country)
                .subscribe((res: AmsCountry) => {
                    // this.spinerService.display(false);
                    //console.log('countrySave -> res:', res);
                    if (res && res.countryId) {
                        this.country = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Country', 'Operation Succesfull: Country ' + message);
                        this.dialogRef.close(this.country);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Country ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
