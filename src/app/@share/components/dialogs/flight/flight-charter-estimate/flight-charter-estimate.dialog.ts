import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, FormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { UserRole, UserRoleOpt } from 'src/app/@core/models/pipes/sx-user-role.pipe';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsFlightEstimate, ResponseAmsFlightEstimateCrete } from 'src/app/@core/services/api/flight/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';

export interface IAmsFlightsEstimateDto {
    airline: AmsAirline;
    aircraft: AmsAircraft;
    flights: AmsFlightEstimate[];
}

@Component({
    templateUrl: './flight-charter-estimate.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsFlightCharterEstimateDialog implements OnInit {

    formGrp: UntypedFormGroup;

    airline: AmsAirline;
    aircraft: AmsAircraft;
    depAirport: AmsAirport;
    airports: AmsAirport[];
    flights: AmsFlightEstimate[];
    flightsCount: number;
    alId: number;
    roleOpt = UserRoleOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private flClient: AmsFlightClient,
        private apCache: AmsAirportCacheService,
        public dialogRef: MatDialogRef<IAmsFlightsEstimateDto>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsFlightsEstimateDto) {
        this.airline = data.airline;
        this.aircraft = data.aircraft;
        this.flights = data.flights;
    }



    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.alId = this.airline ? this.airline?.alId : undefined;
        this.flightsCount = this.flights ? this.flights.length : 0;
        this.formGrp = this.fb.group({
            //alName: new FormControl(this.airline ? this.airline.alName : '', [Validators.required, Validators.maxLength(256)]),

        });

        if (this.flights && this.flights.length > 0) {
            const depApId = this.flights[0].depApId;
            this.apCache.getAirport(depApId).subscribe(ap => {
                this.depAirport = ap;
                //console.log('ngOnInit -> depAirport:', this.depAirport);
            })
        }
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    selectFlight(fleId: number): void {
        let flight = this.flights.find(x => x.fleId == fleId);
        this.errorMessage = undefined;
        this.hasSpinner = true;
        if (flight) {
            this.spinerService.display(true);
            this.flClient.flightCharterEstimateCreate(flight.fleId)
                .subscribe((res: ResponseAmsFlightEstimateCrete) => {
                    // this.spinerService.display(false);
                    //console.log('airlineSave -> res:', res);
                    this.spinerService.display(false);
                    if (res && res.success && res.data &&
                        res.data.flId && res.data.flId > -1) {
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Airline Flight', 'Operation Succesfull: Flight registered ' + res.data.flId);
                        this.dialogRef.close(res.data.flId);
                    } else {
                        this.errorMessage = 'Flight create Failed. Uncnown error';
                        this.toastr.error('Airline Flight', this.errorMessage);
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Flight create Failed. ' + err;
                        this.toastr.error('Airline Flight', this.errorMessage);
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));
        }

    }

}
