import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, FormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { UserRole, UserRoleOpt } from 'src/app/@core/models/pipes/sx-user-role.pipe';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsFlightEstimate, ResponseAmsFlightEstimateCrete } from 'src/app/@core/services/api/flight/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';

export interface IAmsFlightEstimateDto {
    airline: AmsAirline;
    aircraft: AmsAircraft;
    flight: AmsFlightEstimate;
}

@Component({
    templateUrl: './flight-estimate.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsFlightEstimateDialog implements OnInit {

    formGrp: UntypedFormGroup;

    airline: AmsAirline;
    aircraft: AmsAircraft;
    flight: AmsFlightEstimate;
    alId: number;
    roleOpt = UserRoleOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private flClient: AmsFlightClient,
        public dialogRef: MatDialogRef<IAmsFlightEstimateDto>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsFlightEstimateDto) {
        this.airline = data.airline;
        this.aircraft = data.aircraft;
        this.flight = data.flight;
    }



    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.alId = this.airline ? this.airline?.alId : undefined;

        this.formGrp = this.fb.group({
            //alName: new FormControl(this.airline ? this.airline.alName : '', [Validators.required, Validators.maxLength(256)]),

        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            this.spinerService.display(true);

            this.flClient.fflightTransferEstimateCreate(this.flight.fleId)
                .subscribe((res: ResponseAmsFlightEstimateCrete) => {
                    this.spinerService.display(false);
                    console.log('fflightTransferEstimateCreate -> res:', res);
                    if (res && res.success && res.data &&
                        res.data.flId && res.data.flId > -1) {
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Airline Flight', 'Operation Succesfull: Flight registered ' + res.data.flId);
                        this.dialogRef.close(res.data.flId);
                    } else {
                        this.errorMessage = 'Flight create Failed. Uncnown error';
                        this.toastr.error('Airline Flight', this.errorMessage);
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                    }
                },
                    err => {
                        this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Flight create Failed. ' + err;
                        this.toastr.error('Airline Flight', this.errorMessage);
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
