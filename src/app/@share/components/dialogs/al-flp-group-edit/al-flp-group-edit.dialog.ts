import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
// -Models
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable, of, startWith, map } from 'rxjs';
import { AmsAircraft, ResponseAmsAircraftTable } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { end } from '@popperjs/core';
import { AmsHub, AmsHubTableCriteria, ResponceAmsHubTableData } from 'src/app/@core/services/api/airline/al-hub';
import { DAY_MS } from 'src/app/@core/const/app-storage.const';


export interface IAmsSheduleGroupEdit {
    group: AmsSheduleGroup;
}

@Component({
    templateUrl: './al-flp-group-edit.dialog.html',
})

export class AmsSheduleGroupEditDialog implements OnInit {

    formGrp: FormGroup;

    airline: AmsAirline;
    group: AmsSheduleGroup;

    hasSpinner = false;
    errorMessage: string;
    minStart: Date = new Date();
    startDate: Date;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spiner: SpinnerService,
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private apCache: AmsAirportCacheService,
        private acCache: AmsAirlineAircraftCacheService,
        public dialogRef: MatDialogRef<IAmsSheduleGroupEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsSheduleGroupEdit) {
        this.group = data.group;
    }
    get grpIdFc(): FormControl { return this.formGrp.get('grpId') as FormControl; }
    get alIdFc(): FormControl { return this.formGrp.get('alId') as FormControl; }
    get grpNameFc(): FormControl { return this.formGrp.get('grpName') as FormControl; }
    get acIdPrototypeFc() { return this.formGrp.get('acIdPrototype') as FormControl; }
    get acIdFc() { return this.formGrp.get('acId') as FormControl; }
    get hubF() { return this.formGrp.get('hubF') as FormControl; }
    get activeFc() { return this.formGrp.get('active') as FormControl; }
    get startDateFc() { return this.formGrp.get('startDate') as FormControl; }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {
        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.airline = this.cus.airline;
        this.spiner.display(true);
        this.spiner.setMessage('Loading aircrafts...');
        this.acSchedule = undefined;
        this.aircrafts = new Array<AmsAircraft>();

        //console.log('ngOnInit -> hubTypeId:', this.hubTypeId);
        this.minStart = new Date(new Date().getTime() + DAY_MS);
        //console.log('ngOnInit -> minStart:', this.minStart);
        this.startDate = this.group && this.group.startDate ? this.group.startDate : this.minStart;
        //console.log('ngOnInit -> startDate:', this.startDate);
        this.formGrp = this.fb.group({
            grpId: new FormControl<number>(this.group ? this.group.grpId : 0, []),
            alId: new FormControl<number>(this.group && this.group.alId ? this.group.alId : this.cus.airline.alId, [Validators.required]),
            grpName: new FormControl<string>(this.group && this.group.grpName ? this.group.grpName : '', [Validators.required, Validators.maxLength(256)]),
            acIdPrototype: new FormControl(this.aircraft ? this.aircraft : '', [Validators.required]),
            acId: new FormControl(this.group ? this.group.acId : '', []),
            hubF: new FormControl(this.hub ? this.hub : '', [Validators.required]),
            active: new FormControl(this.group && this.group.isActive ? this.group.isActive : false),
            startDate: new FormControl(this.group && this.group.startDate ? this.group.startDate : this.minStart, []),
        });

        this.startDateFc.valueChanges.subscribe(data => {
            //console.log('data; ', data);
            this.startDate = this.startDateFc && this.startDateFc.value._d ? this.startDateFc.value._d : this.startDateFc.value;
            console.log('valueChanges -> startDate:', this.startDate);
            this.formGrp.updateValueAndValidity();
        });

        this.aircrafts$ = this.acCache.getAlAircrafts(this.cus.airline.alId);

        this.aircrafts$.subscribe(resp => {
            this.aircrafts = resp;
            this.spiner.display(false);
            this.initAircrafts();
        });

        this.hubs$ = this.alClient.airlineHubsTable(this.hubsCriteria);
        this.hubs$.subscribe((resp: ResponceAmsHubTableData) => {
            this.hubs = resp.hubs;
            this.initHubs();
        });

    }

    refreshClick(): void {
        this.spiner.display(true);
        this.spiner.setMessage('Loading aircrafts...');
        this.acCache.clearCache();
        this.aircrafts$ = this.acCache.getAlAircrafts(this.cus.airline.alId);

        this.aircrafts$.subscribe(resp => {
            this.aircrafts = resp;
            this.spiner.display(false);
            this.initAircrafts();
        });
    }



    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.group) {
                this.group = new AmsSheduleGroup();
            }
            const message = this.group.grpId > 0 ? 'Update' : 'Create';
            this.group.alId = this.alIdFc.value;
            this.group.grpName = this.grpNameFc.value;
            this.group.acIdPrototype = this.aircraft ? this.aircraft.acId : undefined;
            this.group.acId = this.acSchedule ? this.acSchedule.acId : undefined;
            this.group.apId = this.hub ? this.hub.apId : undefined;
            this.group.isActive = this.activeFc.value;
            if (this.startDate) {
                this.group.startDate = new Date(this.startDate.getFullYear(), this.startDate.getMonth(), this.startDate.getDate(), 0, 5, 0);
            } else {
                this.group.startDate = undefined;
            }


            //console.log('groupSave -> group:', this.group);
            this.spiner.display(true);

            this.alClient.sheduleGroupSave(this.group).then((res: AmsSheduleGroup) => {
                this.spiner.display(false);
                //console.log('groupSave -> res:', res);
                if (res && res.grpId) {
                    this.errorMessage = undefined;
                    this.hasSpinner = false;
                    this.toastr.success(`${message} Hub`, `Operation Succesfull: Group ${message}`);
                    this.dialogRef.close(this.group);
                }
            }, err => {
                this.spiner.display(false);
                console.error('Observer got an error: ' + err);
                this.errorMessage = 'Group ' + message + ' Failed. ' + err;
                setTimeout((router: Router,) => {
                    this.errorMessage = undefined;
                    this.hasSpinner = false;
                    this.dialogRef.close(undefined);
                }, 2000);
                // this.spinerService.display(false);
                // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
            });

        }


    }

    //#region  Aiport Hubs

    public get hubsCriteria(): AmsHubTableCriteria {

        let obj = new AmsHubTableCriteria();
        obj.limit = 120;
        obj.offset = 0;
        obj.alId = this.cus.airline.alId;
        obj.sortCol = 'adate';
        obj.sortDesc = true;
        return obj;
    }
    hubs: AmsHub[];
    hubsOpt: Observable<AmsHub[]>;
    hubs$: Observable<ResponceAmsHubTableData>;
    hub: AmsHub;
    displayHub(value: AmsHub): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.hubName ? value.hubName : '';
        }
        return rv;
    }

    hubValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.hubF.valid) {
            const value: AmsHub = AmsHub.fromJSON(this.hubF.value);
            //console.log('hubValueChange -> value', value);
            if (value && value.apId) {
                this.hub = value;
            }
        }
    }

    resethubF(): void {
        console.log('resethubF -> ');
        this.hubF.patchValue('');
        this.hub = undefined;
        this.formGrp.updateValueAndValidity();
    }

    filterHub(val: any): AmsHub[] {
        //console.log('filterDpc -> val', val);
        let sr: AmsHub;
        if (val && val.apId) {
            sr = val as AmsHub;
        }
        const value = val.hubName || val;
        const filterValue = value ? value.toLowerCase() : '';
        //console.log('filterAircraft -> filterValue', filterValue);
        let rv = new Array<AmsHub>()
        if (this.hubs && this.hubs.length) {
            rv = this.hubs.filter(x => (
                x.hubName.toLowerCase().indexOf(filterValue) > -1));
        }
        //console.log('filterAircraft -> rv', rv);
        return rv;
    }


    initHubs() {
        this.hubsOpt = of(this.hubs);

        if (this.group && this.group.apId) {
            this.hub = this.hubs.find(x => x.apId === this.group.apId);
        }
        this.hubsOpt = this.hubF.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterHub(val) :
                    (this.hubs ? this.hubs.slice() : new Array<AmsHub>()))
            );

        this.hubF.patchValue(this.hub);

        this.formGrp.updateValueAndValidity();
    }
    //#endregion

    //#region Aircraft List

    acScheduleOpt: Observable<AmsAircraft[]>;
    acSchedule: AmsAircraft;

    aircrafts: AmsAircraft[];
    aircraftOpt: Observable<AmsAircraft[]>;
    aircrafts$: Observable<AmsAircraft[]>;
    aircraft: AmsAircraft;


    displayAircraft(value: AmsAircraft): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.registration ? value.registration : '';
        }
        return rv;
    }

    acValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.acIdPrototypeFc.valid) {
            const value: AmsAircraft = AmsAircraft.fromJSON(this.acIdPrototypeFc.value);
            console.log('acValueChange -> value', value);
            if (value && value.acId) {
                this.aircraft = value;
            }
        }
    }

    resetAircraftF(): void {
        console.log('resetAircraftF -> ');
        this.acIdPrototypeFc.patchValue('');
        this.aircraft = undefined;
        this.formGrp.updateValueAndValidity();
    }

    filterAircraft(val: any): AmsAircraft[] {
        let sr: AmsAircraft;
        if (val && val.acId) {
            sr = val as AmsAircraft;
        }
        const value = val.registration || val;
        const filterValue = value ? value.toLowerCase() : '';
        let rv = new Array<AmsAircraft>()
        if (this.aircrafts && this.aircrafts.length) {
            rv = this.aircrafts.filter(x => (
                x.registration.toLowerCase().indexOf(filterValue) > -1));
        }
        return rv;
    }

    displaAcSchedule(value: AmsAircraft): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.registration ? value.registration : '';
        }
        return rv;
    }

    resetAcSchedule(): void {
        console.log('resetAcSchedule -> ');
        this.acIdFc.patchValue('');
        this.acSchedule = undefined;
        this.formGrp.updateValueAndValidity();
    }

    acScheduleValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.acIdFc.valid) {
            const value: AmsAircraft = AmsAircraft.fromJSON(this.acIdFc.value);
            if (value && value.acId) {
                this.acSchedule = value;
            }
        }
        if (!this.acSchedule) {
            this.activeFc.patchValue(false);
        }
    }

    filterAcSchedule(val: any): AmsAircraft[] {
        //console.log('filterDpc -> val', val);
        let sr: AmsAircraft;
        if (val && val.acId) {
            sr = val as AmsAircraft;
        }
        const value = val.registration || val;
        const filterValue = value ? value.toLowerCase() : '';
        //console.log('filterAircraft -> filterValue', filterValue);
        let rv = new Array<AmsAircraft>()
        if (this.aircrafts && this.aircrafts.length) {
            rv = this.aircrafts.filter(x => (x.forSheduleFlights === true &&
                (x.grpId == null || x.grpId === this.group?.grpId) &&
                x.registration.toLowerCase().indexOf(filterValue) > -1));
        }
        //console.log('filterAircraft -> rv', rv);
        return rv;
    }

    initAircrafts() {
        this.aircraftOpt = of(this.aircrafts);
        if (this.group && this.group.acIdPrototype) {
            this.aircraft = this.aircrafts.find(x => x.acId === this.group.acIdPrototype);
        }
        this.aircraftOpt = this.acIdPrototypeFc.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterAircraft(val) :
                    (this.aircrafts ? this.aircrafts.slice() : new Array<AmsAircraft>()))
            );




        this.acScheduleOpt = of(this.aircrafts.filter(x => x.forSheduleFlights === true &&
            (x.grpId == null || x.grpId === this.group?.grpId)));

        if (this.group && this.group.acId) {
            this.acSchedule = this.aircrafts.find(x => x.acId === this.group.acId);
        }
        this.acScheduleOpt = this.acIdFc.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterAcSchedule(val) :
                    (this.aircrafts ? this.aircrafts.filter(x => x.forSheduleFlights === true &&
                        (x.grpId == null || x.grpId === this.group?.grpId)).slice() : new Array<AmsAircraft>()))
            );
        this.acIdFc.patchValue(this.acSchedule);
        if (!this.acSchedule) {
            this.activeFc.patchValue(false);
        }


        this.acIdPrototypeFc.patchValue(this.aircraft);
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

}
