import { Component, Inject, type OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AmsAlFlightNumber, AmsAlFlightNumberSchedule, AmsAlFlightNumberTableCriteria, ResponceAmsAlFlightNumberTableData } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { IAmsAlFlightNumberEdit } from '../al-flp-number-edit/al-flp-number-edit.dialog';
import { AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsWeekday, AmsWeekdays, AmsWeekdaysOpt } from 'src/app/@core/services/api/airline/enums';
import { Observable, map, of, startWith } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { tryParse } from '@es-joy/jsdoccomment';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { MapLeafletAlSheduleGroupService } from 'src/app/@share/maps/map-liflet-al-shedule-group.service';
import { ParseFlags } from '@angular/compiler';

export interface IAmsAlFlightNumberScheduleEdit {
  flnschedule: AmsAlFlightNumberSchedule;
  group: AmsSheduleGroup;
}

@Component({
  templateUrl: './al-flpn-schedule-edit.dialog.html',
})
export class AmsAlFlightNumberScheduleEditDialog implements OnInit {

  flnschedule: AmsAlFlightNumberSchedule;
  group: AmsSheduleGroup;
  aircraft: AmsAircraft;

  formGrp: FormGroup;

  pte = AmsPayloadType.E;
  ptb = AmsPayloadType.B;
  ptf = AmsPayloadType.F;
  cpt = AmsPayloadType.C;
  weekdayOpt: AmsWeekday[] = AmsWeekdaysOpt;

  hasSpinner = false;
  errorMessage: string;
  staticMapUrl: string;
  airline: AmsAirline;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private spiner: SpinnerService,
    private cus: CurrentUserService,
    private alClient: AmsAirlineClient,
    private apCache: AmsAirportCacheService,
    private acCache: AmsAirlineAircraftCacheService,
    private hubCache: AmsAirlineHubsCacheService,
    private mapService: MapLeafletAlSheduleGroupService,
    public dialogRef: MatDialogRef<IAmsAlFlightNumberScheduleEdit>,
    @Inject(MAT_DIALOG_DATA) public data: IAmsAlFlightNumberScheduleEdit) {
    this.flnschedule = data.flnschedule;
    this.group = data.group;
  }

  get flpnsIdFc(): FormControl { return this.formGrp.get('flpnsId') as FormControl; }
  get grpIdFc(): FormControl { return this.formGrp.get('grpId') as FormControl; }
  get flpnFc(): FormControl { return this.formGrp.get('flpn') as FormControl; }

  get groupFc() { return this.formGrp.get('group') as FormControl; }

  get alIdFc(): FormControl { return this.formGrp.get('alId') as FormControl; }
  get wdIdFc(): FormControl { return this.formGrp.get('wdId') as FormControl; }

  get dtimeHFc(): FormControl { return this.formGrp.get('dtimeH') as FormControl; }
  get dtimeMinFc(): FormControl { return this.formGrp.get('dtimeMin') as FormControl; }
  get atimeHFc(): FormControl { return this.formGrp.get('atimeH') as FormControl; }
  get atimeMinFc(): FormControl { return this.formGrp.get('atimeMin') as FormControl; }

  get flightHFc(): FormControl { return this.formGrp.get('flightH') as FormControl; }

  get priceEFc(): FormControl { return this.formGrp.get('priceE') as FormControl; }
  get priceBFc(): FormControl { return this.formGrp.get('priceB') as FormControl; }
  get priceFFc(): FormControl { return this.formGrp.get('priceF') as FormControl; }
  get priceCpKgFc(): FormControl { return this.formGrp.get('priceCpKg') as FormControl; }



  fError(fname: string): string {
    const field = this.formGrp.get(fname);
    return field.hasError('required') ? 'Field ' + fname + '  is required.' :
      field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
        field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
  }

  async ngOnInit(): Promise<void> {
    this.errorMessage = undefined;
    this.hasSpinner = false;
    this.airline = this.cus.airline;

    //console.log('ngOnInit -> flnschedule:', this.flnschedule);
    this.formGrp = this.fb.group({
      flpnsId: new FormControl<number>(this.flnschedule ? this.flnschedule.flpnsId : undefined, []),
      grpId: new FormControl<number>(this.group ? this.group.grpId : undefined, [Validators.required]),
      flpn: new FormControl<any>('', [Validators.required]),
      alId: new FormControl<number>(this.flnschedule ? this.flnschedule.alId : this.airline?.alId, [Validators.required]),
      wdId: new FormControl<number>(this.flnschedule && this.flnschedule.wdId ? this.flnschedule.wdId : AmsWeekdays.Monday, [Validators.required]),

      group: new FormControl(this.group ? this.group : '', [Validators.required]),
      dtimeH: new FormControl<number>(this.flnschedule && this.flnschedule.dtimeH ? this.flnschedule.dtimeH : 0, [Validators.required, Validators.min(0), Validators.max(23)]),
      dtimeMin: new FormControl<number>(this.flnschedule && this.flnschedule.dtimeMin ? this.flnschedule.dtimeMin : 0, [Validators.required, Validators.min(0), Validators.max(59)]),
      atimeH: new FormControl<number>(this.flnschedule && this.flnschedule.atimeH ? this.flnschedule.atimeH : 0, [Validators.required, Validators.min(0), Validators.max(23)]),
      atimeMin: new FormControl<number>(this.flnschedule && this.flnschedule.atimeMin ? this.flnschedule.atimeMin : 0, [Validators.required, Validators.min(0), Validators.max(59)]),

      flightH: new FormControl<number>(this.flnschedule && this.flnschedule.flightH ? this.flnschedule.flightH : 0, [Validators.required, Validators.min(0)]),

      priceE: new FormControl<number>(this.flnschedule && this.flnschedule.priceE ? this.flnschedule.priceE : 0, [Validators.required, Validators.min(0)]),
      priceB: new FormControl<number>(this.flnschedule && this.flnschedule.priceB ? this.flnschedule.priceB : 0, [Validators.required, Validators.min(0)]),
      priceF: new FormControl<number>(this.flnschedule && this.flnschedule.priceF ? this.flnschedule.priceF : 0, [Validators.required, Validators.min(0)]),
      priceCpKg: new FormControl<number>(this.flnschedule && this.flnschedule.priceCpKg ? this.flnschedule.priceCpKg : 0, [Validators.required, Validators.min(0)]),
    });

    this.formGrp.updateValueAndValidity();

    const ac$ = this.acCache.getAircraft(this.group?.acId);
    ac$.subscribe(ac => {
      this.aircraft = ac;
    });

    this.flpns$ = this.alClient.alFlightNumberTable(this.criteria);
    this.flpns$.subscribe((resp: ResponceAmsAlFlightNumberTableData) => {
      this.flpns = resp?.flnrs;
      this.initFlpNumbers();
    });


    this.initGroupsFc();
    this.staticMapUrl = await this.apCache.getStaticMapUrl(this.flnschedule?.depApId, this.flnschedule?.arrApId, 1.5);
  }

  onCansel(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.formGrp.updateValueAndValidity();
    if (this.formGrp.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;

      if (!this.flnschedule) {
        this.flnschedule = new AmsAlFlightNumberSchedule();
      }
      const message = this.flnschedule.flpnsId > 0 ? 'Update' : 'Create';
      const value: AmsAlFlightNumber = AmsAlFlightNumber.fromJSON(this.flpnFc.value);

      this.flnschedule.flpnId = this.flpn.flpnId;
      this.flnschedule.flpnNumber = this.flpn.flpnId;
      this.flnschedule.alId = this.cus.airline.alId;

      this.flnschedule.depApId = this.flpn.depApId;
      this.flnschedule.arrApId = this.flpn.arrApId;
      this.flnschedule.grpId = this.group && this.group.grpId ? this.group.grpId : this.flnschedule.grpId;

      this.flnschedule.wdId = this.wdIdFc.value;
      this.flnschedule.dtimeH = this.dtimeHFc.value;
      this.flnschedule.dtimeMin = this.dtimeMinFc.value;
      this.flnschedule.atimeH = this.atimeHFc.value;
      this.flnschedule.atimeMin = this.atimeMinFc.value;

      this.flnschedule.flightH = this.flightHFc.value;
      this.flnschedule.priceE = this.priceEFc.value;
      this.flnschedule.priceB = this.priceBFc.value;
      this.flnschedule.priceF = this.priceFFc.value;
      this.flnschedule.priceCpKg = this.priceCpKgFc.value;


      //console.log('onSubmit -> flnschedule:', this.flpn);
      this.spiner.display(true);

      this.alClient.alFlightNumberScheduleSave(this.flnschedule).then((res: AmsAlFlightNumberSchedule) => {
        this.spiner.display(false);
        this.hasSpinner = false;
        //console.log('AmsAlFlightNumberSchedule -> res:', res);
        if (res) {
          this.flnschedule = res;
          this.errorMessage = undefined;
          this.toastr.success(`Operation Succesfull: Flight Schedule ${message}`, `${message} Flight Schedule`);
          this.dialogRef.close(this.flnschedule);
        }
      }, err => {
        this.spiner.display(false);
        //console.error('Observer got an error: ' + err);
        this.errorMessage = 'Route ' + message + ' Failed. ' + err;
        setTimeout((router: Router,) => {
          this.errorMessage = undefined;
          this.hasSpinner = false;
          this.dialogRef.close(undefined);
        }, 2000);
        // this.spinerService.display(false);
        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
      });

    }


  }

  //#region Aiport Departure

  flpns: AmsAlFlightNumber[];
  flpnsOpt: Observable<AmsAlFlightNumber[]>;
  flpns$: Observable<ResponceAmsAlFlightNumberTableData>;
  flpn: AmsAlFlightNumber;

  get criteria(): AmsAlFlightNumberTableCriteria {
    let obj = new AmsAlFlightNumberTableCriteria();
    obj.limit = 1200;
    obj.offset = 0;
    obj.sortCol = 'flpnNumber';
    obj.sortDesc = false;
    obj.alId = this.cus.airline.alId;
    return obj;
  }

  displayFlpn(value: AmsAlFlightNumber): string {
    let rv: string = '';
    if (value && value.flpnId) {
      rv = value?.flpnName ? value?.flpnName : '';
    } else {
      rv = value.toString();
    }
    return rv;
  }

  flpnValueChange(event: MatAutocompleteSelectedEvent) {
    if (this.flpnFc.valid) {
      const flpn = this.flpnFc.value;
      console.log('hubValueChange -> flpn', flpn);
      const value: AmsAlFlightNumber = this.flpns.find(x => x.flpnId === flpn?.flpnId);
      const oldValue = this.flpn;


      if (value && value.flpnId) {
        this.flpn = value;
        this.flnschedule.flightH = this.flightH;
        this.flightHFc.patchValue(this.flnschedule.flightH);
        this.callculateValues().then(res => {
          //console.log('flpnValueChange -> flpn: ', this.flpn);
        });
      }
    }
  }

  get flightH(): number {
    let flightH = this.flpn?.flightH;
    console.log('flightH -> distanceKm: ', this.flpn.distanceKm);
    console.log('flightH -> cruiseSpeedKmph: ', this.aircraft.cruiseSpeedKmph);
    if (this.aircraft && this.aircraft.cruiseSpeedKmph) {
      flightH = ((this.flpn?.distanceKm ?? 0) / this.aircraft.cruiseSpeedKmph);
    }
    console.log('flightH -> flightH: ', flightH);
    return parseFloat(flightH.toFixed(2));
  }

  async callculateValues(): Promise<void> {
    console.log('callculateValues -> flpn: ', this.flpn);
    if (this.flpn && this.flpn.flpnId) {

      if (this.flnschedule.flpnId !== this.flpn.flpnId) {
        this.flnschedule.flightH = this.flightH;
        this.flnschedule.priceE = this.flpn.priceE;
        this.flnschedule.priceB = this.flpn.priceB;
        this.flnschedule.priceF = this.flpn.priceF;
        this.flnschedule.priceCpKg = this.flpn.priceCpKg;
        this.flnschedule.arrApId = this.flpn.arrApId;
        this.flnschedule.depApId = this.flpn.depApId;
        this.flnschedule.distanceKm = this.flpn.distanceKm;
        this.flnschedule.flightH = this.flightH;
        this.flightHFc.patchValue(this.flnschedule.flightH);
      }

      if (!this.flnschedule.priceE) { this.flnschedule.priceE = this.flpn?.priceE; }
      if (!this.flnschedule.priceB) { this.flnschedule.priceB = this.flpn?.priceB; }
      if (!this.flnschedule.priceF) { this.flnschedule.priceF = this.flpn?.priceF; }
      if (!this.flnschedule.priceCpKg) { this.flnschedule.priceCpKg = this.flpn?.priceCpKg; }
      if (!this.flnschedule.arrApId) { this.flnschedule.arrApId = this.flpn?.arrApId; }
      if (!this.flnschedule.depApId) { this.flnschedule.depApId = this.flpn?.depApId; }
      if (!this.flnschedule.distanceKm) { this.flnschedule.distanceKm = this.flpn?.distanceKm; }
      if (!this.flnschedule.arrApId) { this.flnschedule.arrApId = this.flpn?.arrApId; }
    }

    this.flnschedule.flightH = this.flightHFc.value;
    const dtimeH = this.dtimeHFc.value;
    const dtimeMin = this.dtimeMinFc.value;
    //console.log('flpnValueChange -> dtimeH: ', dtimeH.toString());
    //console.log('flpnValueChange -> dtimeMin: ', dtimeMin.toString());
    const today = new Date();
    const dep = new Date(today.getFullYear(), today.getMonth(), today.getDate(), dtimeH, dtimeMin, 0);
    //console.log('flpnValueChange -> dep: ', dep.toString());
    const arr = new Date(dep.getTime() + (1000 * 60 * (this.flnschedule.flightH * 60)));
    //console.log('flpnValueChange -> arr: ', arr.toString());
    this.atimeHFc.patchValue(arr.getHours());
    this.atimeMinFc.patchValue(arr.getMinutes());
    //console.log('flpnValueChange -> atimeHFc: ', this.atimeHFc.value);
    //console.log('flpnValueChange -> atimeMinFc: ', this.atimeMinFc.value);


    this.staticMapUrl = await this.apCache.getStaticMapUrl(this.flnschedule.depApId, this.flnschedule.arrApId, 1.5);
    //this.flpnFc.patchValue(this.flpn ? this.flpn.flpnId : '');
    this.flightHFc.patchValue(this.flnschedule.flightH);
    this.priceEFc.patchValue(this.flnschedule.priceE);
    this.priceBFc.patchValue(this.flnschedule.priceB);
    this.priceFFc.patchValue(this.flnschedule.priceF);
    this.priceCpKgFc.patchValue(this.flnschedule.priceCpKg);
    this.formGrp.updateValueAndValidity();

  }

  flpnReset(): void {
    this.flpnFc.patchValue('');
    this.flpn = undefined;
    this.formGrp.updateValueAndValidity();
  }

  flpnFilter(val: any): AmsAlFlightNumber[] {
    //console.log('flpnFilter -> val', val);
    let sr: AmsAlFlightNumber;
    if (val && val.flpnId) {
      sr = val as AmsAlFlightNumber;
    } else {
      sr = this.flpns.find(x => x.flpnId === val);
    }
    const value = (val.flpnName || sr ? sr.flpnName : val);
    //console.log('flpnFilter -> value', value);
    const filterValue = value ? value.toLowerCase() : '';
    //console.log('filterAircraft -> filterValue', filterValue);
    let rv = new Array<AmsAlFlightNumber>()
    if (this.flpns && this.flpns.length) {
      rv = this.flpns.filter(x => (
        x.flpnName.toLowerCase().indexOf(filterValue) > -1
      ));
    }
    //console.log('filterAircraft -> rv', rv);
    return rv;
  }


  initFlpNumbers() {
    this.flpnsOpt = of(this.flpns);

    if (this.flnschedule && this.flnschedule.flpnId) {
      this.flpn = this.flpns.find(x => x.flpnId === this.flnschedule.flpnId);
    }
    this.flpnsOpt = this.flpnFc.valueChanges
      .pipe(
        startWith(null),
        map(val => val ? this.flpnFilter(val) :
          (this.flpns ? this.flpns.slice() : new Array<AmsAlFlightNumber>()))
      );

    //console.log('initFlpNumbers -> flpn ', this.flpn);
    this.flpnFc.patchValue(this.flpn ? this.flpn : '');
    this.flnschedule.flightH = this.flightH;
    this.flightHFc.patchValue(this.flnschedule.flightH);
    this.formGrp.updateValueAndValidity();
  }

  //#endregion

  //#region AmsSheduleGroup

  groups: AmsSheduleGroup[];
  groupsCount: number = 0;

  groupsOpt: Observable<AmsSheduleGroup[]>;
  groups$: Observable<AmsSheduleGroup[]>;

  displayGroup(value: AmsSheduleGroup): string {
    let rv: string = '';
    if (value && value.grpName) {
      rv = ' ';
      rv = value.grpName ? value.grpName : value.grpId.toString();
    } else {
      console.log('displayGroup -> value:', value);
      //rv = `All Flight Numbers`;
    }

    return rv;
  }

  valueChangeGroup(event: MatAutocompleteSelectedEvent) {
    console.log('valueChangeGroup -> event:', event);
    if (this.groupFc.valid) {
      const value = this.groupFc.value;
      if (value && value.grpId) {
        const grp: AmsSheduleGroup = AmsSheduleGroup.fromJSON(this.groupFc.value);
        this.group = grp;
        //this.loadCountries(subregion);
      } else {
        this.group = undefined;
      }
      this.formGrp.patchValue({ grpId: this.group?.grpId });
      this.formGrp.patchValue({ group: this.group });
      this.formGrp.updateValueAndValidity();
    }
  }

  resetGroup(): void {
    this.groupFc.patchValue('');
    this.group = undefined;
    this.formGrp.updateValueAndValidity();
  }

  filterGroup(val: any): AmsSheduleGroup[] {
    // console.log('filterDpc -> val', val);
    let sr: AmsSheduleGroup;
    if (val && val.apId) {
      sr = val as AmsSheduleGroup;
    }

    // console.log('filterBranches -> this.selDpc', this.selDpc);
    const value = val.grpName || val; // val can be companyName or string
    const filterValue = value ? value.toLowerCase() : '';
    // console.log('_filter -> filterValue', filterValue);
    let rv = new Array<AmsSheduleGroup>()
    if (this.groups && this.groups.length) {
      rv = this.groups.filter(x => (
        x.grpName.toLowerCase().indexOf(filterValue) > -1));
    }
    // console.log('_filter -> rv', rv);
    return rv;
  }


  initGroupsFc() {
    this.groups = this.mapService.groups;
    this.groupsOpt = of(this.groups);

    this.groupsOpt = this.groupFc.valueChanges
      .pipe(
        startWith(null),
        map(val => val ? this.filterGroup(val) :
          (this.groups ? this.groups.slice() : new Array<AmsSheduleGroup>()))
      );

    let sr: AmsSheduleGroup;
    if (this.groups && this.groups.length > 0) {
      sr = this.group && this.group.grpId ? this.groups.find(x => x.grpId == this.group.grpId) : undefined;
    }
    this.formGrp.patchValue({ group: sr });
    this.formGrp.updateValueAndValidity();
  }

  //#endregion

}
