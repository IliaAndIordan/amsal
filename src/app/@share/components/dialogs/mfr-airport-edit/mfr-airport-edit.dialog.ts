import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAdminService } from 'src/app/admin/admin.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCity, AmsCountry, AmsState, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsWadService } from 'src/app/wad/wad.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';



export interface IAmsMfrAirportEdit {
    mfr: AmsManufacturer;
}

@Component({
    templateUrl: './mfr-airport-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsMfrAirportEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    mfr: AmsManufacturer;
    amsStatusOpt = AmsStatusOpt;
    wadStatusOpt = WadStatusOpt;

    subregions: AmsSubregion[];
    subregionsOpt: Observable<AmsSubregion[]>;
    countries: AmsCountry[];
    countriesOpt: Observable<AmsCountry[]>;
    airports: AmsAirport[];
    airportsOpt: Observable<AmsAirport[]>;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinner: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private adminService: AmsAdminService,
        private wadService: AmsWadService,
        public dialogRef: MatDialogRef<IAmsMfrAirportEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsMfrAirportEdit) {
        this.mfr = data.mfr;
    }
    get subregion() { return this.formGrp.get('subregion'); }
    get country() { return this.formGrp.get('country'); }
    get airport() { return this.formGrp.get('airport'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {
        
        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.formGrp = this.fb.group({
            subregion: new UntypedFormControl('', [Validators.required]),
            country: new UntypedFormControl('', [Validators.required]),
            airport: new UntypedFormControl('', [Validators.required]),
        });

        this.formGrp.updateValueAndValidity();

        this.loadRegions();
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;
            const airport = this.airport.value as AmsAirport;

           
            const message = this.mfr.mfrId > 0 ? 'Update' : 'Create';
            
            this.mfr.apId = airport ? airport.apId : undefined;

            // this.spinerService.display(true);

            this.client.mfrSave(this.mfr)
                .subscribe((res: AmsManufacturer) => {
                    // this.spinerService.display(false);
                    //console.log('countrySave -> res:', res);
                    if (res && res.mfrId) {
                        this.mfr = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Manufacturer', 'Operation Succesfull: Manufacturer ' + message);
                        this.dialogRef.close(this.mfr);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Manufacturer ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

     //#region Filter Subregions

     subregionValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.subregion.valid) {
            const subregion: AmsSubregion = this.subregion.value;
            if (subregion && subregion.subregionId) {
                this.loadCountries(subregion);
            }
        }
    }

    displaySubregion(value: AmsSubregion): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.srName ? value.srName : value.srCode;
        }
        return rv;
    }

    filterSubregion(val: any): AmsSubregion[] {
        // console.log('filterDpc -> val', val);
        let sr: AmsSubregion;
        if (val && val.subregionId) {
            sr = val as AmsSubregion;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.srName || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<AmsSubregion>()
        if (this.subregions && this.subregions.length) {
            rv = this.subregions.filter(x => (
                x.srName.toLowerCase().indexOf(filterValue) > -1 ||
                x.srCode.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }

    initSubregions() {
        this.subregionsOpt = of(this.subregions);

        this.subregionsOpt = this.subregion.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterSubregion(val) :
                    (this.subregions ? this.subregions.slice() : new Array<AmsSubregion>()))
            );

        let sr: AmsSubregion;

        if (this.subregions && this.subregions.length > 0) {
            sr = this.subregion && this.subregion.value ? this.subregions.find(x => x.subregionId === this.subregion.value.subregionId) : undefined;
        }

        this.formGrp.patchValue({ subregion: sr });
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region Filter Country

    countryValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.country.valid) {
            const country: AmsCountry = this.country.value;
            this.loadAirports(country);
        }
    }

    displayCountry(value: AmsCountry): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.cName ? value.cName : value.cCode;
        }
        return rv;
    }

    filterCountry(val: any): AmsCountry[] {
        // console.log('filterDpc -> val', val);
        let sr: AmsCountry;
        if (val && val.countryId) {
            sr = val as AmsCountry;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.cName || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<AmsCountry>()
        if (this.countries && this.countries.length) {
            rv = this.countries.filter(x => (
                x.cName.toLowerCase().indexOf(filterValue) > -1 ||
                x.cCode.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }

    initCountry() {
        this.countriesOpt = of(this.countries);

        this.countriesOpt = this.country.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterCountry(val) :
                    (this.countries ? this.countries.slice() : new Array<AmsCountry>()))
            );

        let sr: AmsCountry;

        if (this.countries && this.countries.length > 0) {
            sr = this.country && this.country.value ? this.countries.find(x => x.countryId === this.country.value.countryId) : undefined;
        }

        this.formGrp.patchValue({ country: sr });
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region Filter Airports

    airportValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.airport.valid) {
            const airport: AmsAirport = this.airport.value;
        }
    }

    displayAirport(value: AmsAirport): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv += value.iata?value.iata:value.icao + ' ';
            rv += value.apName ? value.apName : value.apId;
            rv += ', ' + value.ctName + ' ';
            rv += value.iso2;
        }
        return rv;
    }

    filterAirport(val: any): AmsAirport[] {
        let sr: AmsAirport;
        if (val && val.countryId) {
            sr = val as AmsAirport;
        }

        const value = val.apName || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        let rv = new Array<AmsAirport>()
        if (this.airports && this.airports.length) {
            rv = this.airports.filter(x => (
                x.apName.toLowerCase().indexOf(filterValue) > -1 ||
                (x.iata && x.iata.toLowerCase().indexOf(filterValue) > -1) ||
                (x.icao && x.icao.toLowerCase().indexOf(filterValue) > -1)));
        }
        return rv;
    }

    initAirports() {
        this.airportsOpt = of(this.airports);

        this.airportsOpt = this.airport.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterAirport(val) :
                    (this.airports ? this.airports.slice() : new Array<AmsAirport>()))
            );

        let sr: AmsAirport;

        if (this.airports && this.airports.length > 0) {
            sr = this.airport && this.airport.value ? this.airports.find(x => x.apId === this.airport.value.apId) : undefined;
        }

        this.formGrp.patchValue({ airport: sr });
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region Data
    loadRegions(): void {
        //this.spinner.show();
        this.wadService.loadRegions()
            .subscribe((resp: ResponseAmsRegionsData) => {
                //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
                this.subregions = resp.data.subregions;
                this.spinner.hide();
                this.initSubregions();
            },
            err => {
                this.spinner.hide();
                console.log('loadRegions-> err: ', err);
            });
    }

    loadCountries(subregion: AmsSubregion): void {
        this.spinner.show();
        this.wadService.loadCountryBySubregion(subregion.subregionId)
            .subscribe((resp: Array<AmsCountry>) => {
                console.log('loadCountries-> resp:', resp);

                this.countries = resp;
                this.spinner.hide();
                this.initCountry();
            },
                err => {
                    this.spinner.hide();
                    console.log('loadRegions-> err: ', err);
                });

    }

    loadAirports(country: AmsCountry): void {
        this.spinner.show();
        this.wadService.loadAirportsByCountry(country.countryId)
            .subscribe((resp: Array<AmsAirport>) => {
                console.log('loadAirports-> resp:', resp);

                this.airports = resp;
                this.spinner.hide();
                this.initAirports();
            },
                err => {
                    this.spinner.hide();
                    console.log('loadAirports-> err: ', err);
                });

    }


    //#endregion
}
