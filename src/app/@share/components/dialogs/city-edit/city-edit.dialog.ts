import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { AmsWadCountryService } from 'src/app/wad/country/country.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCity, AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsWadStateService } from 'src/app/wad/state/state.service';


export interface IAmsStateEdit {
    state: AmsState;
    city?: AmsCity;
}

@Component({
    templateUrl: './city-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsCityEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    state: AmsState;
    city: AmsCity;
    amsStatusOpt = AmsStatusOpt;
    wadStatusOpt = WadStatusOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsCountryClient,
        private stService: AmsWadStateService,
        public dialogRef: MatDialogRef<IAmsStateEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsStateEdit) {
        this.state = data.state;
        this.city = data.city;
    }
    get ctName() { return this.formGrp.get('ctName'); }
    get population() { return this.formGrp.get('population'); }
    get wikiUrl() { return this.formGrp.get('wikiUrl'); }
    get amsStatus() { return this.formGrp.get('amsStatus'); }
    get wadStatus() { return this.formGrp.get('wadStatus'); }
    get stateCapital() { return this.formGrp.get('stateCapital'); }
    get countryCapital() { return this.formGrp.get('countryCapital'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.formGrp = this.fb.group({
            ctName: new UntypedFormControl(this.city ? this.city.ctName : '', [Validators.required, Validators.maxLength(256)]),
            population: new UntypedFormControl(this.city ? this.city.population : 0 , [Validators.required]),
            wikiUrl: new UntypedFormControl(this.city ? this.city.wikiUrl : '', [Validators.maxLength(1000)]),
            amsStatus: new UntypedFormControl(this.city && this.city.amsStatus ? this.city.amsStatus : AmsStatus.DataCheckRequired, [Validators.required]),
            wadStatus: new UntypedFormControl(this.city && this.city.wadStatus ? this.city.wadStatus : WadStatus.ToDo, [Validators.required]),
            stateCapital: new UntypedFormControl(this.city && this.city.stateCapital ? this.city.stateCapital : false, [Validators.required]),
            countryCapital: new UntypedFormControl(this.city && this.city.countryCapital ? this.city.countryCapital : false, [Validators.required]),
        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.city) {
                this.city = new AmsCity();
            }
            const message = this.city.cityId > 0 ? 'Update' : 'Create';
            this.city.countryId = this.stService.country.countryId;
            this.city.stateId = this.stService.state.stateId;
            this.city.ctName = this.ctName.value;
            this.city.population = this.population.value;
            this.city.wikiUrl = this.wikiUrl.value;
            this.city.amsStatus = this.amsStatus.value;
            this.city.wadStatus = this.wadStatus.value;
            this.city.stateCapital = this.stateCapital.value;
            this.city.countryCapital = this.countryCapital.value;

            // this.spinerService.display(true);

            this.client.citySave(this.city)
                .subscribe((res: AmsCity) => {
                    // this.spinerService.display(false);
                    //console.log('countrySave -> res:', res);
                    if (res && res.cityId) {
                        this.city = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('State', 'Operation Succesfull: City ' + message);
                        this.dialogRef.close(this.state);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'State ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
