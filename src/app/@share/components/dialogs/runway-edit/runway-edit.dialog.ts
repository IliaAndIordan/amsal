import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { AmsWadCountryService } from 'src/app/wad/country/country.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCity, AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsWadStateService } from 'src/app/wad/state/state.service';
import { AmsAirport, AmsRunway } from 'src/app/@core/services/api/airport/dto';
import { ApType, ApTypeInfoOpt, ApTypeOpt, RwSerface, RwSerfaceOpt, RwType, RwTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { AmsWadAirportService } from 'src/app/wad/airport/airport.service';


export interface IAmsRunwayEdit {
    runway: AmsRunway;
}

@Component({
    templateUrl: './runway-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsRunwayEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    region: AmsRegion;
    subregion: AmsSubregion;
    country: AmsCountry;
    state: AmsState;
    city: AmsCity;
    airport: AmsAirport;
    runway: AmsRunway;
    typeOpt = RwTypeOpt;
    serfaceOpt = RwSerfaceOpt;

    amsStatusOpt = AmsStatusOpt;
    wadStatusOpt = WadStatusOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAirportClient,
        private aService: AmsWadAirportService,
        public dialogRef: MatDialogRef<IAmsRunwayEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsRunwayEdit) {
        this.runway = data.runway;
    }

    get rwType() { return this.formGrp.get('rwType'); }
    get rwDirection() { return this.formGrp.get('rwDirection'); }
    get serfaceId() { return this.formGrp.get('serfaceId'); }
    get width() { return this.formGrp.get('width'); }
    get lenght() { return this.formGrp.get('lenght'); }
    get rwOrder() { return this.formGrp.get('rwOrder'); }
    get rotation() { return this.formGrp.get('rotation'); }
    get radius() { return this.formGrp.get('radius'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.region = this.aService.region;
        this.subregion = this.aService.subregion;
        this.country = this.aService.country;
        this.state = this.aService.state;
        this.city = this.aService.city;
        this.airport = this.aService.airport;



        this.formGrp = this.fb.group({
            rwType: new UntypedFormControl(this.runway && this.runway.rwType ? this.runway.rwType : RwType.Runway, [Validators.required]),
            rwDirection: new UntypedFormControl(this.runway && this.runway.rwDirection ? this.runway.rwDirection : '09-27', [Validators.required, Validators.maxLength(45)]),
            serfaceId: new UntypedFormControl(this.runway && this.runway.serfaceId ? this.runway.serfaceId : RwSerface.Concrete, [Validators.required]),
            width: new UntypedFormControl(this.runway && this.runway.width ? this.runway.width : 25, [Validators.required]),
            lenght: new UntypedFormControl(this.runway && this.runway.lenght ? this.runway.lenght : 1000, [Validators.required]),
            rwOrder: new UntypedFormControl(this.runway && this.runway.rwOrder ? this.runway.rwOrder : 1, [Validators.required]),
            rotation: new UntypedFormControl(this.runway && this.runway.rotation ? this.runway.rotation : 1, [Validators.required]),
            radius: new UntypedFormControl(this.runway && this.runway.radius ? this.runway.radius : 1, [Validators.required]),
        });

        if (!this.airport || this.airport.apId !== this.runway.apId) {
            setTimeout(() => {
                this.loadAirport(this.runway.apId)
                    .subscribe((resp: AmsAirport) => {
                        this.airport = resp;
                        this.initFields();
                    });
            });
        }
        else{
            this.initFields();
        }


    }

    initFields(){
        const apType = ApTypeInfoOpt.find(x=>x.id === this.airport.typeId);
        console.log('apType: ', apType);
        this.formGrp.patchValue({ radius: apType.map_symbol_radius });
        this.formGrp.updateValueAndValidity();
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.city) {
                this.city = new AmsCity();
            }
            const message = this.airport.apId > 0 ? 'Update' : 'Create';
            this.runway.rwDirection = this.rwDirection.value;
            this.runway.rwOrder = this.rwOrder.value;
            this.runway.rwType = this.rwType.value;
            this.runway.serfaceId = this.serfaceId.value;
            this.runway.width = this.width.value;
            this.runway.lenght = this.lenght.value;
            this.runway.rotation = this.rotation.value;
            this.runway.radius = this.radius.value;

            // this.spinerService.display(true);

            this.client.runwaySave(this.runway)
                .subscribe((res: AmsRunway) => {
                    // this.spinerService.display(false);
                    //console.log('countrySave -> res:', res);
                    if (res && res.rwId) {
                        this.runway = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Runway', 'Operation Succesfull: Runway ' + message);
                        this.dialogRef.close(this.state);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Runway ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }

    }

    //#region Data

    loadRunway(rwId: number): Observable<AmsRunway> {
        this.spinerService.show();
        return new Observable<AmsRunway>(subscriber => {

            this.aService.loadRunway(rwId)
                .subscribe((resp: AmsRunway) => {
                    console.log('loadRunway-> resp', resp);
                    subscriber.next(resp);
                    this.spinerService.hide();
                },
                    err => {
                        this.spinerService.hide();
                        throw err;
                    });
        });

    }

    loadAirport(apId: number): Observable<AmsAirport> {
        this.spinerService.show();
        return new Observable<AmsAirport>(subscriber => {

            this.aService.loadAirport(apId)
                .subscribe((resp: AmsAirport) => {
                    console.log('loadAirport-> resp', resp);
                    subscriber.next(resp);
                    this.spinerService.hide();
                },
                    err => {
                        this.spinerService.hide();
                        throw err;
                    });
        });

    }

    //#endregion

}
