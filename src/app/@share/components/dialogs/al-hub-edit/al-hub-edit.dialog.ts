import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
// -Models
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadStateService } from 'src/app/wad/state/state.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsHub } from 'src/app/@core/services/api/airline/al-hub';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsHubTypes } from 'src/app/@core/services/api/airline/enums';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';


export interface IAmsHubEdit {
    hub: AmsHub;
}

@Component({
    templateUrl: './al-hub-edit.dialog.html',
})

export class AmsHubEditDialog implements OnInit {

    formGrp: FormGroup;

    airport: AmsAirport;
    airline: AmsAirline;
    hub: AmsHub;
    minStTime:Date;

    hasSpinner = false;
    errorMessage: string;
    hubTypeId:AmsHubTypes;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spiner: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAirlineClient,
        private apCache:AmsAirportCacheService,
        public dialogRef: MatDialogRef<IAmsHubEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsHubEdit) {
        this.hub = data.hub;
    }
    get hubIdFc(): FormControl { return this.formGrp.get('hubId') as FormControl; }
    get apIdFc(): FormControl { return this.formGrp.get('apId') as FormControl; }
    get alIdFc(): FormControl { return this.formGrp.get('alId') as FormControl; }
    get hubNameFc(): FormControl { return this.formGrp.get('hubName') as FormControl; }
    get upkeepDayFc(): FormControl { return this.formGrp.get('upkeepDay') as FormControl; }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    async ngOnInit(): Promise<void> {
        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.airline = this.cus.airline;
        
        this.minStTime = new Date();
        this.minStTime.setTime(new Date().getTime() + (1 * 60));
        this.hubTypeId = this.hub && this.hubTypeId?this.hubTypeId:
            (this.hub.apId === this.airline?.apId?AmsHubTypes.Base:AmsHubTypes.Hub);
        //console.log('ngOnInit -> hubTypeId:', this.hubTypeId);
        this.formGrp = this.fb.group({
            hubId: new FormControl<number>(this.hub ? this.hub.hubId : 0, []),
            apId: new FormControl<number>(this.hub.apId , [Validators.required]),
            alId: new FormControl<number>(this.hub && this.hub.alId ? this.hub.alId : this.cus.airline.alId, [Validators.required]),
            hubName: new FormControl<string>(this.hub ? this.hub.hubName : '', [Validators.required, Validators.maxLength(256)]),
            upkeepDay: new FormControl(this.hub && this.hub.upkeepDay? this.hub.upkeepDay : this.minStTime, [Validators.required]),
        });
        this.airport = await this.apCache.getAirportAsync(this.hub.apId);
        console.log('ngOnInit -> airport:', this.airport);
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.hub) {
                this.hub = new AmsHub();
            }
            const message = this.hub.hubId > 0 ? 'Update' : 'Create';
            this.hub.apId = this.apIdFc.value;;
            this.hub.alId = this.alIdFc.value;
            this.hub.hubName = this.hubNameFc.value;
            this.hub.upkeepDay = this.upkeepDayFc && this.upkeepDayFc.value._d ? this.upkeepDayFc.value._d : this.upkeepDayFc.value;
            console.log('hubSave -> getTime:', this.hub.upkeepDay.getTime());
            this.hub.upkeepTimeMs =  parseInt((this.hub.upkeepDay.getTime()/1000).toFixed(0));
            
            console.log('hubSave -> hub:', this.hub);
            this.spiner.display(true);

            this.client.hubSave(this.hub).then((res: AmsHub) => {
                this.spiner.display(false);
                console.log('hubSave -> res:', res);
                if (res && res.hubId) {
                    this.errorMessage = undefined;
                    this.hasSpinner = false;
                    this.toastr.success(`${message} Hub`, `Operation Succesfull: Hub ${message}`);
                    this.dialogRef.close(this.hub);
                }
            }, err => {
                this.spiner.display(false);
                console.error('Observer got an error: ' + err);
                this.errorMessage = 'Hub ' + message + ' Failed. ' + err;
                setTimeout((router: Router,) => {
                    this.errorMessage = undefined;
                    this.hasSpinner = false;
                    this.dialogRef.close(undefined);
                }, 2000);
                // this.spinerService.display(false);
                // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
            });

        }


    }

}
