import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { UserRole, UserRoleOpt } from 'src/app/@core/models/pipes/sx-user-role.pipe';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';

export interface IAmsAirlineEdit {
    airline: AmsAirline;
}

@Component({
    templateUrl: './airline-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsAirlineEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    airline: AmsAirline;
    alId:number;
    roleOpt = UserRoleOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        public dialogRef: MatDialogRef<IAmsAirlineEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsAirlineEdit) {
        this.airline = data.airline;
    }

    get alName() { return this.formGrp.get('alName'); }
    get slogan() { return this.formGrp.get('slogan'); }
    get iata() { return this.formGrp.get('iata'); }
    get livery() { return this.formGrp.get('livery'); }
    get logo() { return this.formGrp.get('logo'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.alId=this.airline ? this.airline?.alId : undefined;
        
        this.formGrp = this.fb.group({
            alName: new UntypedFormControl(this.airline ? this.airline.alName : '', [Validators.required, Validators.maxLength(256)]),
            slogan: new UntypedFormControl(this.airline ? this.airline.slogan : '', [Validators.required, Validators.maxLength(512)]),
            iata: new UntypedFormControl(this.airline ? this.airline.iata : '', [Validators.required, Validators.maxLength(3)]),
            livery: new UntypedFormControl(this.airline ? this.airline.livery : '', [Validators.maxLength(1000)]),
            logo: new UntypedFormControl(this.airline ? this.airline.logo : '', [Validators.maxLength(1000)]),
        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.airline) {
                this.airline = new AmsAirline();
            }
            const message = this.airline?.alId > 0 ? 'Update' : 'Create';
            
            this.airline.alName = this.alName.value;
            this.airline.slogan = this.slogan.value;
            this.airline.iata = this.iata.value;
            this.airline.livery = this.livery.value;
            this.airline.logo = this.logo.value;

            // this.spinerService.display(true);

            this.alClient.airlineSave(this.airline)
                .subscribe((res: AmsAirline) => {
                    // this.spinerService.display(false);
                    //console.log('airlineSave -> res:', res);
                    if (res && res.alId) {
                        this.airline = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Airline', 'Operation Succesfull: Airline ' + message);
                        this.dialogRef.close(this.airline);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Airline ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
