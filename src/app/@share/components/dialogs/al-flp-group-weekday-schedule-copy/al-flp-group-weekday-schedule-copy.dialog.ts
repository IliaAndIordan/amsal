import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
// -Models
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable, of, startWith, map } from 'rxjs';
import { AmsAircraft, ResponseAmsAircraftTable } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { end } from '@popperjs/core';
import { AmsHub, AmsHubTableCriteria, ResponceAmsHubTableData } from 'src/app/@core/services/api/airline/al-hub';
import { DAY_MS } from 'src/app/@core/const/app-storage.const';
import { AmsWeekday, AmsWeekdays, AmsWeekdaysOpt } from 'src/app/@core/services/api/airline/enums';
import { AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleCopy, AmsAlFlightNumberScheduleTableCriteria, ResponceAmsAlFlightNumberScheduleTableData } from 'src/app/@core/services/api/airline/al-flp-number';
import { WeekDay } from '@angular/common';


export interface IAmsAlpGroupWeekdayScheduleCopyDialog {
    group: AmsSheduleGroup;
    weekday: AmsWeekday;
    shedulesCount: number
}

export interface IAmsAlpGroupWeekdayScheduleCopyDialogResponce {
    success: boolean;
    result: any[];
}

@Component({
    templateUrl: './al-flp-group-weekday-schedule-copy.dialog.html',
})

export class AmsAlpGroupWeekdayScheduleCopyDialog implements OnInit {

    formGrp: FormGroup;

    airline: AmsAirline;
    group: AmsSheduleGroup;
    weekday: AmsWeekday;
    shedulesCount: number;
    flnschedules: AmsAlFlightNumberSchedule[];

    weekdayOpt = AmsWeekdaysOpt;
    sunOpt = AmsWeekdaysOpt[6];
    monOpt = AmsWeekdaysOpt[0];
    tueOpt = AmsWeekdaysOpt[1];
    wenOpt = AmsWeekdaysOpt[2];
    thuOpt = AmsWeekdaysOpt[3];
    friOpt = AmsWeekdaysOpt[4];
    satOpt = AmsWeekdaysOpt[5];

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spiner: SpinnerService,
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private apCache: AmsAirportCacheService,
        private acCache: AmsAirlineAircraftCacheService,
        public dialogRef: MatDialogRef<IAmsAlpGroupWeekdayScheduleCopyDialog>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsAlpGroupWeekdayScheduleCopyDialog) {
        this.group = data.group;
        this.weekday = data.weekday;
        this.shedulesCount = data.shedulesCount;
    }
    get grpIdFc(): FormControl { return this.formGrp.get('grpId') as FormControl; }
    get fromWeekdayIdFc(): FormControl { return this.formGrp.get('fromWeekdayId') as FormControl; }
    get sundayFc() { return this.formGrp.get('sunday') as FormControl; }
    get mondayFc() { return this.formGrp.get('monday') as FormControl; }
    get tuesdayFc() { return this.formGrp.get('tuesday') as FormControl; }
    get wednesdayFc() { return this.formGrp.get('wednesday') as FormControl; }
    get thursdayFc() { return this.formGrp.get('thursday') as FormControl; }
    get fridayFc() { return this.formGrp.get('friday') as FormControl; }
    get saturdayFc() { return this.formGrp.get('saturday') as FormControl; }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {
        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.airline = this.cus.airline;

        //console.log('ngOnInit -> hubTypeId:', this.hubTypeId);
        this.formGrp = this.fb.group({
            grpId: new FormControl<number>(this.group ? this.group.grpId : -1, [Validators.min(1), Validators.required]),
            fromWeekdayId: new FormControl<number>(this.weekday ? this.weekday.id : 1, [Validators.required]),
            sunday: new FormControl(this.weekday.id === AmsWeekdays.Sunday ? true : false),
            monday: new FormControl<boolean>(this.weekday.id === AmsWeekdays.Monday ? true : false),
            tuesday: new FormControl<boolean>(this.weekday.id === AmsWeekdays.Tuesday ? true : false),
            wednesday: new FormControl<boolean>(this.weekday.id === AmsWeekdays.Wednesday ? true : false),
            thursday: new FormControl<boolean>(this.weekday.id === AmsWeekdays.Thursday ? true : false),
            friday: new FormControl<boolean>(this.weekday.id === AmsWeekdays.Friday ? true : false),
            saturday: new FormControl<boolean>(this.weekday.id === AmsWeekdays.Saturday ? true : false),

        });

        switch (this.weekday?.id) {
            case AmsWeekdays.Sunday:
                this.sundayFc.disable();
                break;
            case AmsWeekdays.Monday:
                this.mondayFc.disable();
                break;
            case AmsWeekdays.Tuesday:
                this.tuesdayFc.disable();
                break;
            case AmsWeekdays.Wednesday:
                this.wednesdayFc.disable();
                break;
            case AmsWeekdays.Thursday:
                this.thursdayFc.disable();
                break;
            case AmsWeekdays.Friday:
                this.fridayFc.disable();
                break;
            case AmsWeekdays.Saturday:
                this.saturdayFc.disable();
                break;
        }

        //console.log('groupSave -> thursdayFc.value:', this.thursdayFc.value);
        this.loadFlnSchedules();
    }



    onCansel(): void {
        this.dialogRef.close();
    }
    result = new Array<any>();
    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;


            let weekdays = new Array<AmsWeekday>();
            if (this.sundayFc.value && this.weekday.id !== AmsWeekdays.Sunday) { weekdays.push(this.sunOpt); };
            if (this.mondayFc.value && this.weekday.id !== AmsWeekdays.Monday) { weekdays.push(this.monOpt); };
            if (this.tuesdayFc.value && this.weekday.id !== AmsWeekdays.Tuesday) { weekdays.push(this.tueOpt); };
            if (this.wednesdayFc.value && this.weekday.id !== AmsWeekdays.Wednesday) { weekdays.push(this.wenOpt); };
            if (this.thursdayFc.value && this.weekday.id !== AmsWeekdays.Thursday) { weekdays.push(this.thuOpt); };
            if (this.fridayFc.value && this.weekday.id !== AmsWeekdays.Friday) { weekdays.push(this.friOpt); };
            if (this.saturdayFc.value && this.weekday.id !== AmsWeekdays.Saturday) { weekdays.push(this.satOpt); };

            //console.log('groupSave -> weekdays:', weekdays);
            this.result = new Array<any>();
            this.spiner.display(true);
            const message = `Copy ${this.weekday.name} schedule to ${weekdays.length} days...`;
            this.spiner.setMessage(message);
            var promises = [];
            weekdays.forEach(wd => {

                const req = new AmsAlFlightNumberScheduleCopy();
                req.grpId = this.group.grpId;
                req.wdIdFrom = this.weekday.id;
                req.wdIdTo = wd.id;

                const promise = this.alClient.alFlightNumberScheduleCopyDayP(req);
                promises.push(promise);
            });

            Promise.all(promises).then((data) => {
                this.spiner.display(false);
                //console.log("data: ", data);
                this.result = data;
                const res: IAmsAlpGroupWeekdayScheduleCopyDialogResponce = {
                    success: (this.result && this.result.length > 0),
                    result: this.result
                }
                this.dialogRef.close(res);

            });


        }


    }

    public get criteria(): AmsAlFlightNumberScheduleTableCriteria {

        let obj = new AmsAlFlightNumberScheduleTableCriteria();
        obj.limit = 1000;
        obj.offset = 0;
        obj.sortCol = undefined;
        obj.sortDesc = false;
        obj.grpId = this.group.grpId;
        return obj;
    }


    loadFlnSchedules(): void {
        this.spiner.show();
        this.alClient.alFlightNumberScheduleTableP(this.criteria)
            .then((resp: ResponceAmsAlFlightNumberScheduleTableData) => {
                //console.log('loadFlnSchedules-> resp=', resp);
                this.flnschedules = resp.flnschedules;
                this.initFlnSchedules();
                this.spiner.hide();
            }, msg => {
                console.log('loadFlnSchedules -> ' + msg);
                this.flnschedules = new Array<AmsAlFlightNumberSchedule>();
                this.initFlnSchedules();
                this.spiner.hide();
            });
    }
    sunFlpsh: AmsAlFlightNumberSchedule[];
    monFlpsh: AmsAlFlightNumberSchedule[];
    tueFlpsh: AmsAlFlightNumberSchedule[];
    wenFlpsh: AmsAlFlightNumberSchedule[];
    thuFlpsh: AmsAlFlightNumberSchedule[];
    friFlpsh: AmsAlFlightNumberSchedule[];
    satFlpsh: AmsAlFlightNumberSchedule[];

    initFlnSchedules(): void {
        this.sunFlpsh = new Array<AmsAlFlightNumberSchedule>();
        this.monFlpsh = new Array<AmsAlFlightNumberSchedule>();
        this.tueFlpsh = new Array<AmsAlFlightNumberSchedule>();
        this.wenFlpsh = new Array<AmsAlFlightNumberSchedule>();
        this.thuFlpsh = new Array<AmsAlFlightNumberSchedule>();
        this.friFlpsh = new Array<AmsAlFlightNumberSchedule>();
        this.satFlpsh = new Array<AmsAlFlightNumberSchedule>();
        if (this.flnschedules && this.flnschedules.length > 0) {
            this.sunFlpsh = this.flnschedules.filter(x => x.wdId === AmsWeekdays.Sunday);
            this.monFlpsh = this.flnschedules.filter(x => x.wdId === AmsWeekdays.Monday);
            this.tueFlpsh = this.flnschedules.filter(x => x.wdId === AmsWeekdays.Tuesday);
            this.wenFlpsh = this.flnschedules.filter(x => x.wdId === AmsWeekdays.Wednesday);
            this.thuFlpsh = this.flnschedules.filter(x => x.wdId === AmsWeekdays.Thursday);
            this.friFlpsh = this.flnschedules.filter(x => x.wdId === AmsWeekdays.Friday);
            this.satFlpsh = this.flnschedules.filter(x => x.wdId === AmsWeekdays.Saturday);
        }
    }

}
