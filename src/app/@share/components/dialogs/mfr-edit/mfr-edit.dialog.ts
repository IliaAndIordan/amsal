import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAdminService } from 'src/app/admin/admin.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCity, AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';



export interface IAmsManufacturerEdit {
    mfr: AmsManufacturer;
    airport?: AmsAirport;
}

@Component({
    templateUrl: './mfr-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsManufacturerEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    mfr: AmsManufacturer;
    airport: AmsAirport;
    amsStatusOpt = AmsStatusOpt;
    wadStatusOpt = WadStatusOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private adminService: AmsAdminService,
        public dialogRef: MatDialogRef<IAmsManufacturerEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsManufacturerEdit) {
        this.mfr = data.mfr;
        this.airport = data.airport;
    }
    get iata() { return this.formGrp.get('iata'); }
    get mfrName() { return this.formGrp.get('mfrName'); }
    get headquartier() { return this.formGrp.get('headquartier'); }
    get notes() { return this.formGrp.get('notes'); }
    get logoImg() { return this.formGrp.get('logoImg'); }
    get wikiUrl() { return this.formGrp.get('wikiUrl'); }
    get amsStatus() { return this.formGrp.get('amsStatus'); }
    get homeUrl() { return this.formGrp.get('homeUrl'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.formGrp = this.fb.group({
            iata: new UntypedFormControl(this.mfr ? this.mfr.iata : '', [Validators.required, Validators.maxLength(2)]),
            mfrName: new UntypedFormControl(this.mfr ? this.mfr.mfrName : '', [Validators.required, Validators.maxLength(512)]),
            headquartier: new UntypedFormControl(this.mfr ? this.mfr.headquartier : '', [Validators.maxLength(256)]),
            notes: new UntypedFormControl(this.mfr ? this.mfr.notes : '', [Validators.maxLength(2000)]),
            logoImg: new UntypedFormControl(this.mfr ? this.mfr.logoImg : '', [Validators.maxLength(1000)]),
            homeUrl: new UntypedFormControl(this.mfr ? this.mfr.homeUrl : '', [Validators.maxLength(1000)]),
            wikiUrl: new UntypedFormControl(this.mfr ? this.mfr.wikiUrl : '', [Validators.maxLength(1000)]),
            amsStatus: new UntypedFormControl(this.mfr && this.mfr.amsStatus ? this.mfr.amsStatus : AmsStatus.DataCheckRequired, [Validators.required]),
        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.mfr) {
                this.mfr = new AmsManufacturer();
            }
            const message = this.mfr.mfrId > 0 ? 'Update' : 'Create';
            this.mfr.iata = this.iata.value;
            this.mfr.mfrName = this.mfrName.value;
            this.mfr.headquartier = this.headquartier.value;
            this.mfr.notes = this.notes.value;
            this.mfr.logoImg = this.logoImg.value;
            this.mfr.homeUrl = this.homeUrl.value;
            this.mfr.wikiUrl = this.wikiUrl.value;
            this.mfr.amsStatus = this.amsStatus.value;
            this.mfr.apId = this.airport ? this.airport.apId : undefined;

            // this.spinerService.display(true);

            this.client.mfrSave(this.mfr)
                .subscribe((res: AmsManufacturer) => {
                    // this.spinerService.display(false);
                    //console.log('countrySave -> res:', res);
                    if (res && res.mfrId) {
                        this.mfr = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Manufacturer', 'Operation Succesfull: Manufacturer ' + message);
                        this.dialogRef.close(this.mfr);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Manufacturer ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
