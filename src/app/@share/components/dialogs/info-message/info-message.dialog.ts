import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { UserRole, UserRoleOpt } from 'src/app/@core/models/pipes/sx-user-role.pipe';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';

export interface IInfoMessage {
    title: string;
    message: string;
}

@Component({
    templateUrl: './info-message.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class InfoMessageDialog implements OnInit {

    formGrp: FormGroup;

    title: string;
    message: string;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        public dialogRef: MatDialogRef<IInfoMessage>,
        @Inject(MAT_DIALOG_DATA) public data: IInfoMessage) {
        this.title = data.title;
        this.message = data.message;
    }

    ngOnInit(): void {
        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.formGrp = this.fb.group({
           
        });
    }


    onCansel(): void {
        this.dialogRef.close(false);
    }

    onSubmit() {
        this.dialogRef.close(true);
    }

   

}
