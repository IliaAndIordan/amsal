import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
// -Models
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAlSheduleRoute, AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { Observable, map, of, startWith } from 'rxjs';
import { AmsAirport, AmsAirportTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsHub, AmsHubTableCriteria, ResponceAmsHubTableData } from 'src/app/@core/services/api/airline/al-hub';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';
import { AmsAlFlightNumber, AmsDestination } from 'src/app/@core/services/api/airline/al-flp-number';
import { EnumViewModel } from 'src/app/@core/models/common/enum-view.model';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';


export interface IAmsAlFlightNumberEdit {
    flpn: AmsAlFlightNumber;
    aircraft?: AmsAircraft
    createReverse?: boolean;
    flpnReverse?:AmsAlFlightNumber;
}

@Component({
    templateUrl: './al-flp-number-edit.dialog.html',
})

export class AmsAlFlightNumberEditDialog implements OnInit {

    formGrp: FormGroup;

    airline: AmsAirline;
    flpn: AmsAlFlightNumber;
    aircraft?: AmsAircraft;
    createReverse: boolean = false;
    flpnReverse:AmsAlFlightNumber;

    flpnNumberOpt: EnumViewModel[] = Array.from({ length: 999 }, (_, i) => {
        return { id: (i + 1), name: String((i + 1)).padStart(4, "0") };
    });
    pte = AmsPayloadType.E;
    ptb = AmsPayloadType.B;
    ptf = AmsPayloadType.F;
    cpt = AmsPayloadType.C;

    hasSpinner = false;
    errorMessage: string;
    staticMapUrl: string;

    dest: AmsDestination;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spiner: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAirlineClient,
        private apCache: AmsAirportCacheService,
        private hubCache: AmsAirlineHubsCacheService,
        public dialogRef: MatDialogRef<IAmsAlFlightNumberEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsAlFlightNumberEdit) {
        this.flpn = data.flpn;
        this.aircraft = data.aircraft;
        this.createReverse = data.createReverse ?? false;
        this.flpnReverse = data.flpnReverse;
    }

    get flpnIdFc(): FormControl { return this.formGrp.get('flpnId') as FormControl; }
    get flpnNumberFc(): FormControl { return this.formGrp.get('flpnNumber') as FormControl; }
    get depApIdFc(): FormControl { return this.formGrp.get('depApId') as FormControl; }
    get arrApIdFc(): FormControl { return this.formGrp.get('arrApId') as FormControl; }

    get destIdFc(): FormControl { return this.formGrp.get('destId') as FormControl; }
    get routeIdFc(): FormControl { return this.formGrp.get('routeId') as FormControl; }

    get flightHFc(): FormControl { return this.formGrp.get('flightH') as FormControl; }

    get priceEFc(): FormControl { return this.formGrp.get('priceE') as FormControl; }
    get priceBFc(): FormControl { return this.formGrp.get('priceB') as FormControl; }
    get priceFFc(): FormControl { return this.formGrp.get('priceF') as FormControl; }
    get priceCpKgFc(): FormControl { return this.formGrp.get('priceCpKg') as FormControl; }

    get routeNameFc(): FormControl { return this.formGrp.get('routeName') as FormControl; }



    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {
        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.airline = this.cus.airline;
        if (!this.flpn) { this.flpn = new AmsAlFlightNumber(); }

        //console.log('ngOnInit -> hubTypeId:', this.hubTypeId);
        this.formGrp = this.fb.group({
            flpnId: new FormControl<number>(this.flpn ? this.flpn.flpnId : 0, []),
            flpnNumber: new FormControl<number>(this.flpn && this.flpn.flpnNumber ? this.flpn.flpnNumber : 999, []),
            depApId: new FormControl<number>(this.flpn ? this.flpn.depApId : 108, [Validators.required]),
            arrApId: new FormControl<number>(this.flpn ? this.flpn.arrApId : 75, [Validators.required]),

            destId: new FormControl<number>(this.flpn ? this.flpn.destId : 0, []),
            routeId: new FormControl<number>(this.flpn ? this.flpn.routeId : 0, []),

            flightH: new FormControl<number>(this.flpn && this.flpn.flightH ? this.flpn.flightH : 0, [Validators.required, Validators.min(0)]),

            priceE: new FormControl<number>(this.flpn && this.flpn.priceE ? this.flpn.priceE : 0, [Validators.required, Validators.min(0)]),
            priceB: new FormControl<number>(this.flpn && this.flpn.priceB ? this.flpn.priceB : 0, [Validators.required, Validators.min(0)]),
            priceF: new FormControl<number>(this.flpn && this.flpn.priceF ? this.flpn.priceF : 0, [Validators.required, Validators.min(0)]),
            priceCpKg: new FormControl<number>(this.flpn && this.flpn.priceCpKg ? this.flpn.priceCpKg : 0, [Validators.required, Validators.min(0)]),
        });

        this.formGrp.updateValueAndValidity();

        this.airports$ = this.apCache.airports;
        this.airports$.subscribe(async (resp: AmsAirport[]) => {
            this.depAps = resp;
            this.arrAps = resp;
            this.initAirports();
            this.initArrAirports();
            if (this.flpn.depApId && this.flpn.arrApId) {
                this.staticMapUrl = await this.apCache.getStaticMapUrl(this.flpn.depApId, this.flpn.arrApId, 1.5);
            }
            if (!this.flpn.flpnId) {
                this.loadDestination();
            }
        });
    }

    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.flpn) {
                this.flpn = new AmsAlFlightNumber();
            }
            
            const isUpdate = this.flpn.flpnId > 0?true:false;
            const message = isUpdate ? 'Update' : 'Create';
            this.flpn.flpnNumber = this.flpnNumberFc.value;
            this.flpn.alId = this.cus.airline.alId;

            this.flpn.depApId = this.depAp.apId;
            this.flpn.arrApId = this.arrAp.apId;
            this.flpn.destId = this.dest ? this.dest.destId : this.flpn.destId;

            this.flpn.flightH = this.flightHFc.value;
            this.flpn.priceE = this.priceEFc.value;
            this.flpn.priceB = this.priceBFc.value;
            this.flpn.priceF = this.priceFFc.value;
            this.flpn.priceCpKg = this.priceCpKgFc.value;


            console.log('onSubmit -> flpn:', this.flpn);
            this.spiner.display(true);

            this.client.alFlightNumberSave(this.flpn).then(async (res: AmsAlFlightNumber) => {
                this.spiner.display(false);
                this.hasSpinner = false;
                console.log('AmsAlFlightNumber -> res:', res);
                if (res) {
                    console.log('AmsAlFlightNumber -> createReverse:', this.createReverse);
                    this.flpn = res;
                    if (this.createReverse) {
                        let flpnr = new AmsAlFlightNumber();
                        if(isUpdate && this.flpnReverse){
                            flpnr = this.flpnReverse;
                        } else{
                            flpnr.flpnNumber = this.flpn.flpnNumber + 1
                            flpnr.alId = this.flpn.alId;

                            flpnr.depApId = this.flpn.arrApId;
                            flpnr.arrApId = this.flpn.depApId;
                        }
                        
                        const destr = await this.client.destinationGet(flpnr.depApId, flpnr.arrApId).catch(err => {
                            console.error('destinationGet got an error: ' + err);
                            return undefined;
                        });
                        flpnr.destId = destr ? destr.destId : this.flpn.destId;

                        flpnr.flightH = this.flightHFc.value;
                        flpnr.priceE = this.priceEFc.value;
                        flpnr.priceB = this.priceBFc.value;
                        flpnr.priceF = this.priceFFc.value;
                        flpnr.priceCpKg = this.priceCpKgFc.value;
                        console.log('onSubmit -> flpnr:', flpnr);
                        this.flpnReverse = await this.client.alFlightNumberSave(flpnr).catch(err => {
                            console.error('Observer got an error: ' + err);
                            this.errorMessage = 'Route ' + message + ' Failed. ' + err;
                            setTimeout((router: Router,) => {
                                this.errorMessage = undefined;
                                this.hasSpinner = false;
                                this.dialogRef.close(undefined);
                            }, 2000);
                            return undefined;
                        });
                        this.errorMessage = undefined;
                        this.toastr.success(`Operation Succesfull: 2 Routes ${message}`, `${message} Route`);
                        this.dialogRef.close(this.flpnReverse);
                    }
                    else {
                        this.errorMessage = undefined;
                        this.toastr.success(`Operation Succesfull: Route ${message}`, `${message} Route`);
                        this.dialogRef.close(this.flpn);
                    }

                }
            }, err => {
                this.spiner.display(false);
                console.error('Observer got an error: ' + err);
                this.errorMessage = 'Route ' + message + ' Failed. ' + err;
                setTimeout((router: Router,) => {
                    this.errorMessage = undefined;
                    this.hasSpinner = false;
                    this.dialogRef.close(undefined);
                }, 2000);
                // this.spinerService.display(false);
                // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
            });

        }


    }

    //#region  Aiport Arrival

    arrAps: AmsAirport[];
    arrApsOpt: Observable<AmsAirport[]>;

    arrAp: AmsAirport;

    arrApValueChange(event: MatAutocompleteSelectedEvent) {
        //console.log('arrApValueChange -> event', event);
        //console.log('arrApValueChange -> valid', this.arrApIdFc.valid);
        if (this.arrApIdFc.valid) {
            const value: AmsAirport = AmsAirport.fromJSON(this.arrApIdFc.value);
            //console.log('hubValueChange -> value', value);
            const oldValue = this.arrAp;
            //console.log('hubValueChange -> oldValue', oldValue);
            if (value && value.apId) {
                this.arrAp = value;
                //console.log('hubValueChange -> arrAp', this.arrAp);
                if (this.arrAp && this.arrAp.apId !== oldValue?.apId) {
                    this.loadDestination();
                }
            }
        }
    }

    arrApReset(): void {
        //console.log('arrApReset -> ');
        this.arrApIdFc.patchValue('');
        this.arrAp = undefined;
        this.formGrp.updateValueAndValidity();
    }

    arrApFilter(val: any): AmsAirport[] {
        //console.log('arrApFilter -> val', val);
        let sr: AmsAirport;
        if (val && val.apId) {
            sr = val as AmsAirport;
        }
        const value = val.apName || val;
        const filterValue = value ? value.toLowerCase() : '';
        //console.log('arrApFilter -> filterValue', filterValue);
        let rv = new Array<AmsAirport>()
        if (this.arrAps && this.arrAps.length) {
            rv = this.arrAps.filter(x => (
                x.apName.toLowerCase().indexOf(filterValue) > -1 ||
                x.icao?.toLowerCase().indexOf(filterValue) > -1 ||
                x.iata?.toLowerCase().indexOf(filterValue) > -1));
        }
        //console.log('arrApFilter -> rv', rv);
        return rv;
    }


    initArrAirports() {
        this.arrApsOpt = of(this.arrAps);

        if (this.flpn && this.flpn.arrApId) {
            this.arrAp = this.depAps.find(x => x.apId === this.flpn.arrApId);
            console.log('initArrAirports -> arrAp ', this.arrAp);
        }
        this.arrApsOpt = this.arrApIdFc.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.arrApFilter(val) :
                    (this.arrAps ? this.arrAps.slice() : new Array<AmsAirport>()))
            );


        this.arrApIdFc.patchValue(this.arrAp ? this.arrAp : '');
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region  Aiport Departure

    depAps: AmsAirport[];
    depApsOpt: Observable<AmsAirport[]>;
    airports$: Observable<AmsAirport[]>;
    depAp: AmsAirport;

    displayAp(value: AmsAirport): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.apLabel ? value.apLabel : '';
        }
        return rv;
    }

    depApValueChange(event: MatAutocompleteSelectedEvent) {
        if (this.depApIdFc.valid) {
            const value: AmsAirport = AmsAirport.fromJSON(this.depApIdFc.value);
            //console.log('hubValueChange -> value', value);
            const oldValue = this.depAp;
            if (value && value.apId) {
                this.depAp = value;
                if (this.depAp && this.depAp.apId !== oldValue?.apId) {
                    this.loadDestination();
                }
            }
        }
    }

    depApReset(): void {
        console.log('depApReset -> ');
        this.depApIdFc.patchValue('');
        this.depAp = undefined;
        this.formGrp.updateValueAndValidity();
    }

    depApFilter(val: any): AmsAirport[] {
        //console.log('filterDpc -> val', val);
        let sr: AmsAirport;
        if (val && val.apId) {
            sr = val as AmsAirport;
        }
        const value = val.apName || val;
        const filterValue = value ? value.toLowerCase() : '';
        //console.log('filterAircraft -> filterValue', filterValue);
        let rv = new Array<AmsAirport>()
        if (this.depAps && this.depAps.length) {
            rv = this.depAps.filter(x => (
                x.apName.toLowerCase().indexOf(filterValue) > -1 ||
                x.icao?.toLowerCase().indexOf(filterValue) > -1 ||
                x.iata?.toLowerCase().indexOf(filterValue) > -1));
        }
        //console.log('filterAircraft -> rv', rv);
        return rv;
    }


    initAirports() {
        this.depApsOpt = of(this.depAps);

        if (this.flpn && this.flpn.depApId) {
            this.depAp = this.depAps.find(x => x.apId === this.flpn.depApId);
            console.log('initAircrafts -> depAp ', this.depAp);
        }
        this.depApsOpt = this.depApIdFc.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.depApFilter(val) :
                    (this.depAps ? this.depAps.slice() : new Array<AmsAirport>()))
            );


        this.depApIdFc.patchValue(this.depAp ? this.depAp : '');
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region Destination

    loadDestination(): void {
        //console.log('loadDestination -> arrAp', this.arrAp);
        if (this.arrAp && this.depAp) {
            //console.log('loadDestination -> depAp', this.depAp);
            this.spiner.display(true);
            this.client.destinationGet(this.depAp.apId, this.arrAp.apId)
                .then(async (resp: AmsDestination) => {
                    //console.log('loadDestination -> resp', resp);
                    this.spiner.display(false);
                    if (resp) {
                        this.dest = resp;
                        console.log('loadDestination -> aircraft', this.aircraft);
                        if (!this.flpn) { this.flpn = new AmsAlFlightNumber(); }
                        this.flpn.destId = this.dest.destId;
                        this.flpn.distanceKm = this.dest.distanceKm;
                        this.staticMapUrl = await this.apCache.getStaticMapUrl(this.dest.depApId, this.dest.arrApId, 1.5);

                        const flightH = this.aircraft ? (this.dest.distanceKm / this.aircraft.cruiseSpeedKmph) : this.dest.flightH;
                        this.flightHFc.patchValue(flightH);
                        this.priceEFc.patchValue(this.dest.priceE);
                        this.priceBFc.patchValue(this.dest.priceB);
                        this.priceFFc.patchValue(this.dest.priceF);
                        this.priceCpKgFc.patchValue(this.dest.priceCpKg);
                        this.formGrp.updateValueAndValidity();
                    }

                });
        }

    }

    //#endregion
}
