import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
// -Models
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAlSheduleRoute, AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { Observable, map, of, startWith } from 'rxjs';
import { AmsAirport, AmsAirportTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsHub, AmsHubTableCriteria, ResponceAmsHubTableData } from 'src/app/@core/services/api/airline/al-hub';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';
import { AmsAlFlightNumber, AmsDestination } from 'src/app/@core/services/api/airline/al-flp-number';
import { IAmsAlFlightNumberEdit } from './al-flp-number-edit.dialog';
import { EnumViewModel } from 'src/app/@core/models/common/enum-view.model';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';

@Component({
    templateUrl: './al-flp-number-price-edit.dialog.html',
})

export class AmsAlFlightNumberPriceEditDialog implements OnInit {

    formGrp: FormGroup;

    airline: AmsAirline;
    flpn: AmsAlFlightNumber;

    flpnNumberOpt: EnumViewModel[] = Array.from({ length: 999 }, (_, i) => {
        return { id:(i + 1), name:String((i + 1)).padStart(4, "0")};
    });
    pte = AmsPayloadType.E;
    ptb = AmsPayloadType.B;
    ptf = AmsPayloadType.F;
    cpt = AmsPayloadType.C;

    hasSpinner = false;
    errorMessage: string;
    staticMapUrl: string;

    dest: AmsDestination;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spiner: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAirlineClient,
        private apCache: AmsAirportCacheService,
        private hubCache: AmsAirlineHubsCacheService,
        public dialogRef: MatDialogRef<IAmsAlFlightNumberEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsAlFlightNumberEdit) {
        this.flpn = data.flpn;
    }

    get flpnIdFc(): FormControl { return this.formGrp.get('flpnId') as FormControl; }
    get flpnNumberFc(): FormControl { return this.formGrp.get('flpnNumber') as FormControl; }
    get depApIdFc(): FormControl { return this.formGrp.get('depApId') as FormControl; }
    get arrApIdFc(): FormControl { return this.formGrp.get('arrApId') as FormControl; }

    get destIdFc(): FormControl { return this.formGrp.get('destId') as FormControl; }
    get routeIdFc(): FormControl { return this.formGrp.get('routeId') as FormControl; }

    get flightHFc(): FormControl { return this.formGrp.get('flightH') as FormControl; }

    get priceEFc(): FormControl { return this.formGrp.get('priceE') as FormControl; }
    get priceBFc(): FormControl { return this.formGrp.get('priceB') as FormControl; }
    get priceFFc(): FormControl { return this.formGrp.get('priceF') as FormControl; }
    get priceCpKgFc(): FormControl { return this.formGrp.get('priceCpKg') as FormControl; }

    get routeNameFc(): FormControl { return this.formGrp.get('routeName') as FormControl; }



    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {
        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.airline = this.cus.airline;

        //console.log('ngOnInit -> hubTypeId:', this.hubTypeId);
        this.formGrp = this.fb.group({
            flpnId: new FormControl<number>(this.flpn ? this.flpn.flpnId : 0, []),
            flpnNumber: new FormControl<number>(this.flpn && this.flpn.flpnNumber ? this.flpn.flpnNumber : 999, []),
            depApId: new FormControl<number>(this.flpn ? this.flpn.depApId : 108, [Validators.required]),
            arrApId: new FormControl<number>(this.flpn ? this.flpn.arrApId : 75, [Validators.required]),

            destId: new FormControl<number>(this.flpn ? this.flpn.destId : 0, []),
            routeId: new FormControl<number>(this.flpn ? this.flpn.routeId : 0, []),

            flightH: new FormControl<number>(this.flpn && this.flpn.flightH ? this.flpn.flightH : 0, [Validators.required, Validators.min(0)]),

            priceE: new FormControl<number>(this.flpn && this.flpn.priceE ? this.flpn.priceE : 0, [Validators.required, Validators.min(0)]),
            priceB: new FormControl<number>(this.flpn && this.flpn.priceB ? this.flpn.priceB : 0, [Validators.required, Validators.min(0)]),
            priceF: new FormControl<number>(this.flpn && this.flpn.priceF ? this.flpn.priceF : 0, [Validators.required, Validators.min(0)]),
            priceCpKg: new FormControl<number>(this.flpn && this.flpn.priceCpKg ? this.flpn.priceCpKg : 0, [Validators.required, Validators.min(0)]),
        });

        this.formGrp.updateValueAndValidity();

        const airports$ = this.apCache.airports;
        airports$.subscribe(async (resp: AmsAirport[]) => {
            this.airports = resp;

            this.initAirports();

            if (this.flpn.depApId && this.flpn.arrApId) {
                this.staticMapUrl = await this.apCache.getStaticMapUrl(this.flpn.depApId, this.flpn.arrApId, 1.5);
            }
        });
    }

    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.flpn) {
                this.flpn = new AmsAlFlightNumber();
            }
            const message = this.flpn.flpnId > 0 ? 'Update' : 'Create';

            this.flpn.flpnNumber = this.flpnNumberFc.value;
            this.flpn.alId = this.cus.airline.alId;

            this.flpn.flightH = this.flightHFc.value;
            this.flpn.priceE = this.priceEFc.value;
            this.flpn.priceB = this.priceBFc.value;
            this.flpn.priceF = this.priceFFc.value;
            this.flpn.priceCpKg = this.priceCpKgFc.value;


            //console.log('groupSave -> group:', this.group);
            this.spiner.display(true);

            this.client.alFlightNumberSave(this.flpn).then((res: AmsAlFlightNumber) => {
                this.spiner.display(false);
                this.hasSpinner = false;
                console.log('AmsAlFlightNumber -> res:', res);
                if (res) {
                    this.flpn = res;
                    this.errorMessage = undefined;
                    this.toastr.success(`Operation Succesfull: Route ${message}`, `${message} Route`);
                    this.dialogRef.close(this.flpn);
                }
            }, err => {
                this.spiner.display(false);
                console.error('Observer got an error: ' + err);
                this.errorMessage = 'Flight Numbe ' + message + ' Failed. ' + err;
                setTimeout((router: Router,) => {
                    this.errorMessage = undefined;
                    this.hasSpinner = false;
                    this.dialogRef.close(undefined);
                }, 2000);
                // this.spinerService.display(false);
                // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
            });

        }


    }

    //#region  Aiports

    airports: AmsAirport[];
    arrAp: AmsAirport;
    depAp: AmsAirport;

    initAirports() {

        if (this.flpn && this.flpn.depApId) {
            this.depAp = this.airports?.find(x => x.apId === this.flpn.depApId);

        }
        //console.log('initAircrafts -> depAp ', this.depAp);
        if (this.flpn && this.flpn.arrApId) {
            this.arrAp = this.airports?.find(x => x.apId === this.flpn.arrApId);
        }
        //console.log('initAircrafts -> arrAp ', this.arrAp);
        this.formGrp.updateValueAndValidity();
    }

    //#endregion

    //#region Destination

    loadDestination(): void {
        if (this.arrAp && this.depAp) {
            this.spiner.display(true);
            this.client.destinationGet(this.depAp.apId, this.arrAp.apId)
                .then(async (resp: AmsDestination) => {
                    this.spiner.display(false);
                    if (resp) {
                        this.dest = resp;
                        this.flpn.destId = this.dest.destId;
                        this.flpn.distanceKm = this.dest.distanceKm;
                        const flightH = this.dest.flightH;
                        this.staticMapUrl = await this.apCache.getStaticMapUrl(this.dest.depApId, this.dest.arrApId, 1.5);
                        this.flightHFc.patchValue(flightH);
                        this.priceEFc.patchValue(this.dest.priceE);
                        this.priceBFc.patchValue(this.dest.priceB);
                        this.priceFFc.patchValue(this.dest.priceF);
                        this.priceCpKgFc.patchValue(this.dest.priceCpKg);
                        this.formGrp.updateValueAndValidity();
                    }

                });
        }

    }

    //#endregion
}
