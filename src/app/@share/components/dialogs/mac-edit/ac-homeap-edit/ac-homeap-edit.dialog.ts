import { CommonModule } from '@angular/common';
import { Component, Inject, type OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { map, Observable, of, startWith } from 'rxjs';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsHub } from 'src/app/@core/services/api/airline/al-hub';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';

export interface IAmsAircraftHomeApEdit {
  aircraft: AmsAircraft;
}

@Component({
  templateUrl: './ac-homeap-edit.dialog.html',
})
export class AmsAircraftHomeApEditDialog implements OnInit {
  formGrp: FormGroup;

  aircraft: AmsAircraft;

  hasSpinner = false;
  errorMessage: string;



  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private spiner: SpinnerService,
    private cus: CurrentUserService,
    private client: AmsAircraftClient,
    private hubCache: AmsAirlineHubsCacheService,
    public dialogRef: MatDialogRef<IAmsAircraftHomeApEdit>,
    @Inject(MAT_DIALOG_DATA) public data: IAmsAircraftHomeApEdit) {
    this.aircraft = data.aircraft;
  }

  get homeApIdFc(): FormControl { return this.formGrp.get('homeApId') as FormControl; }


  fError(fname: string): string {
    const field = this.formGrp.get(fname);
    return field.hasError('required') ? 'Field ' + fname + '  is required.' :
      field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
        field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
  }

  ngOnInit(): void {

    this.errorMessage = undefined;
    this.hasSpinner = false;

    this.formGrp = this.fb.group({
      homeApId: new FormControl(this.aircraft && this.aircraft.homeApId ? this.aircraft.homeApId : this.cus.airline.apId, [Validators.required]),
    });
    this.formGrp.updateValueAndValidity();
    this.hubs$ = this.hubCache.hubs;
    this.hubs$.subscribe(hubs => {
      this.hubs = hubs;
      this.initHubsOpt();
    });
  }

  initAirportsOpt() {

  }

  onCansel(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.formGrp.updateValueAndValidity();
    if (this.formGrp.valid) {
      this.errorMessage = undefined;
      this.hasSpinner = true;


      const message = this.aircraft.acId > 0 ? 'Update' : 'Create';
      const value: AmsHub = AmsHub.fromJSON(this.homeApIdFc.value);

      if (value && value.apId) {
        this.aircraft.homeApId = value.apId;

        this.spiner.display(true);
        this.client.aircraftSave(this.aircraft)
          .then((res: AmsAircraft) => {
            this.spiner.display(false);
            console.log('countrySave -> res:', res);
            if (res && res.acId) {
              this.aircraft = res;
              this.errorMessage = undefined;
              this.hasSpinner = false;
              this.toastr.success('Aircraft Edit', 'Operation Succesfull: Aircraft ' + message);
              this.dialogRef.close(this.aircraft);
            }
          },
            err => {
              console.error('Observer got an error: ' + err);
              this.errorMessage = 'Aicreaft ' + message + ' Failed. ' + err;
              /*
              setTimeout((router: Router,) => {
                  this.errorMessage = undefined;
                  this.hasSpinner = false;
                  this.dialogRef.close(undefined);
              }, 2000);*/
              this.spiner.display(false);
              this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
            });
      }
    }

  }

  //#region  AmsHub

  hubs$: Observable<AmsHub[]>;
  hubs: AmsHub[];
  homeAp: AmsHub;

  hubsOpt: Observable<AmsHub[]>;

  homeApDisplay(value: AmsHub): string {
    let rv: string = '';
    if (value) {
        rv = ' ';
        rv = value.hubName ? value.hubName : '';
    }
    return rv;
}

  homeApValueChange(event: MatAutocompleteSelectedEvent) {
    if (this.homeApIdFc.valid) {
      const value: AmsHub = AmsHub.fromJSON(this.homeApIdFc.value);
      const oldValue = this.homeAp;
      if (value && value.apId) {
        this.homeAp = value;
      }
    }
  }

  homeApReset(): void {
    //console.log('arrApReset -> ');
    this.homeApIdFc.patchValue('');
    this.hubsOpt = of(this.hubs);
    this.formGrp.updateValueAndValidity();
  }

  homeApFilter(val: any): AmsHub[] {
    //console.log('arrApFilter -> val', val);
    let sr: AmsHub;
    if (val && val.apId) {
      sr = val as AmsHub;
    }
    const value = val.hubName || val;
    const filterValue = value ? value.toLowerCase() : '';
    //console.log('arrApFilter -> filterValue', filterValue);
    let rv = new Array<AmsHub>()
    if (this.hubs && this.hubs.length) {
      rv = this.hubs.filter(x => (
        x.hubName.toLowerCase().indexOf(filterValue) > -1));
    }
    //console.log('arrApFilter -> rv', rv);
    return rv;
  }


  initHubsOpt() {
    this.hubsOpt = of(this.hubs);

    if (this.aircraft && this.aircraft.homeApId) {
      this.homeAp = this.hubs.find(x => x.apId === this.aircraft.homeApId);
    }
    this.hubsOpt = this.homeApIdFc.valueChanges
      .pipe(
        startWith(null),
        map(val => val ? this.homeApFilter(val) :
          (this.hubs ? this.hubs.slice() : new Array<AmsHub>()))
      );

    this.homeApIdFc.patchValue(this.homeAp ? this.homeAp : '');
    this.formGrp.updateValueAndValidity();
  }

  //#endregion

}
