import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';

// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCity } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsWadStateService } from 'src/app/wad/state/state.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AcType, AcTypeOpt } from 'src/app/@core/models/pipes/ac-type.pipe';
import { AmsAdminService } from 'src/app/admin/admin.service';
import { URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';


export interface IAmsMacEdit {
    mac: AmsMac;
    mfr?:AmsManufacturer;
}

@Component({
    templateUrl: './mac-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsMacEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    iconsTabOne = URL_COMMON_IMAGE_AIRCRAFT + 'icon-ac-model.png';
    iconsTabOTwo = URL_COMMON_IMAGE_AIRCRAFT + 'icon-ac-service.png';
    mac: AmsMac;
    mfr:AmsManufacturer;
    typeOpt = AcTypeOpt;
    selTabIdx:number;

    amsStatusOpt = AmsStatusOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private adminService: AmsAdminService,
        public dialogRef: MatDialogRef<IAmsMacEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsMacEdit) {
        this.mac = data.mac;
        this.mfr = data.mfr;
    }

    get acTypeId() { return this.formGrp.get('acTypeId'); }
    get model() { return this.formGrp.get('model'); }
    get price() { return this.formGrp.get('price'); }
    get pilots() { return this.formGrp.get('pilots'); }
    get cruiseSpeedKmph() { return this.formGrp.get('cruiseSpeedKmph'); }
    get cruiseAltitudeM() { return this.formGrp.get('cruiseAltitudeM'); }
    get takeOffM() { return this.formGrp.get('takeOffM'); }
    get maxTakeOffKg() { return this.formGrp.get('maxTakeOffKg'); }
    get landingM() { return this.formGrp.get('landingM'); }
    get maxLandingKg() { return this.formGrp.get('maxLandingKg'); }
    get maxRangeKm() { return this.formGrp.get('maxRangeKm'); }
    get emptyWeightKg() { return this.formGrp.get('emptyWeightKg'); }

    get fuelVolL() { return this.formGrp.get('fuelVolL'); }
    get loadKg() { return this.formGrp.get('loadKg'); }
    get wikiUrl() { return this.formGrp.get('wikiUrl'); }
    get amsStatus() { return this.formGrp.get('amsStatus'); }

    get notes() { return this.formGrp.get('notes'); }
    get fuelConsumptionLp100km() { return this.formGrp.get('fuelConsumptionLp100km'); }
    get costPerFh() { return this.formGrp.get('costPerFh'); }
    get operationTimeMin() { return this.formGrp.get('operationTimeMin'); }
    get popularity() { return this.formGrp.get('popularity'); }
    get maxSeating() { return this.formGrp.get('maxSeating'); }

    get asmiRate() { return this.formGrp.get('asmiRate'); }
    get powerplant() { return this.formGrp.get('powerplant'); }
    get productionStart() { return this.formGrp.get('productionStart'); }
    get productionRateH() { return this.formGrp.get('productionRateH'); }
    

   

    




    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.selTabIdx = 0;
        const start: Date = this.mac && this.mac.productionStart ? this.mac.productionStart : undefined;

        this.formGrp = this.fb.group({
            model: new UntypedFormControl(this.mac && this.mac.model ? this.mac.model : '', [Validators.required, Validators.maxLength(250)]),
            acTypeId: new UntypedFormControl(this.mac && this.mac.acTypeId? this.mac.acTypeId : AcType.Piston, [Validators.required]),
            wikiUrl: new UntypedFormControl(this.mac && this.mac.wikiUrl ? this.mac.wikiUrl : '', [Validators.maxLength(1000)]),
            amsStatus: new UntypedFormControl(this.mac && this.mac.amsStatus ? this.mac.amsStatus : AmsStatus.DataCheckRequired, [Validators.required]),
            maxSeating: new UntypedFormControl(this.mac && this.mac.maxSeating ? this.mac.maxSeating : 3),
            pilots: new UntypedFormControl(this.mac && this.mac.pilots ? this.mac.pilots : 1),
            price: new UntypedFormControl(this.mac && this.mac.price ? this.mac.price : 100000),
            notes: new UntypedFormControl(this.mac && this.mac.notes ? this.mac.notes : ''),
            cruiseSpeedKmph: new UntypedFormControl(this.mac && this.mac.cruiseSpeedKmph ? this.mac.cruiseSpeedKmph : 200),
            cruiseAltitudeM: new UntypedFormControl(this.mac && this.mac.cruiseAltitudeM ? this.mac.cruiseAltitudeM : 3000),
            
            powerplant: new UntypedFormControl(this.mac && this.mac.powerplant ? this.mac.powerplant : ''),
            takeOffM: new UntypedFormControl(this.mac && this.mac.takeOffM ? this.mac.takeOffM : 1),
            maxTakeOffKg: new UntypedFormControl(this.mac && this.mac.maxTakeOffKg ? this.mac.maxTakeOffKg : 1),
            landingM: new UntypedFormControl(this.mac && this.mac.landingM ? this.mac.landingM : 1),
            maxLandingKg: new UntypedFormControl(this.mac && this.mac.maxLandingKg ? this.mac.maxLandingKg : 1),

            maxRangeKm: new UntypedFormControl(this.mac && this.mac.maxRangeKm ? this.mac.maxRangeKm : ''),
            emptyWeightKg: new UntypedFormControl(this.mac && this.mac.emptyWeightKg ? this.mac.emptyWeightKg : 1),
            fuelVolL: new UntypedFormControl(this.mac && this.mac.fuelVolL ? this.mac.fuelVolL : 1),
            loadKg: new UntypedFormControl(this.mac && this.mac.loadKg ? this.mac.loadKg : 1),
            fuelConsumptionLp100km: new UntypedFormControl(this.mac && this.mac.fuelConsumptionLp100km ? this.mac.fuelConsumptionLp100km : 1),

            costPerFh: new UntypedFormControl(this.mac && this.mac.costPerFh ? this.mac.costPerFh : 85),
            operationTimeMin: new UntypedFormControl(this.mac && this.mac.operationTimeMin ? this.mac.operationTimeMin : 5),
            popularity: new UntypedFormControl(this.mac && this.mac.popularity ? this.mac.popularity : 1),
            asmiRate: new UntypedFormControl(this.mac && this.mac.asmiRate ? this.mac.asmiRate : 1),
            productionRateH: new UntypedFormControl(this.mac && this.mac.productionRateH ? this.mac.productionRateH : 1),
            productionStart: new UntypedFormControl(this.mac && this.mac.productionStart ? this.mac.productionStart : undefined),
        });
       
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.mac) {
                this.mac = new AmsMac();
            }
            const message = this.mac.macId > 0 ? 'Update' : 'Create';
            this.mac.mfrId = this.mfr.mfrId;
            this.mac.acTypeId = this.acTypeId.value;
            this.mac.model = this.model.value;
            this.mac.price = this.price.value;
            this.mac.pilots = this.pilots.value;
            this.mac.cruiseSpeedKmph = this.cruiseSpeedKmph.value;
            this.mac.cruiseAltitudeM = this.cruiseAltitudeM.value;
            this.mac.takeOffM = this.takeOffM.value;

            this.mac.maxTakeOffKg = this.maxTakeOffKg.value;
            this.mac.landingM = this.landingM.value;
            this.mac.maxLandingKg = this.maxLandingKg.value;
            this.mac.maxRangeKm = this.maxRangeKm.value;
            this.mac.emptyWeightKg = this.emptyWeightKg.value;
            this.mac.fuelVolL = this.fuelVolL.value;
            this.mac.loadKg = this.loadKg.value;

            this.mac.wikiUrl = this.wikiUrl.value;
            this.mac.amsStatus = this.amsStatus.value;
            this.mac.notes = this.notes.value;

            this.mac.fuelConsumptionLp100km = this.fuelConsumptionLp100km.value;
            this.mac.costPerFh = this.costPerFh.value;
           
            this.mac.operationTimeMin = this.operationTimeMin.value;
            this.mac.popularity = this.popularity.value;
            this.mac.maxSeating = this.maxSeating.value;
            this.mac.asmiRate = this.asmiRate.value;
            this.mac.powerplant = this.powerplant.value;
            this.mac.productionStart = this.productionStart.value? this.productionStart.value.toISOString()
            .slice(0, 19).replace('T', ' ') : undefined;
            this.mac.productionRateH = this.productionRateH.value;
            this.mac.apId = this.mfr && this.mfr.apId?this.mfr.apId:undefined;

            this.client.macSave(this.mac)
                .subscribe((res: AmsMac) => {
                    // this.spinerService.display(false);
                    console.log('countrySave -> res:', res);
                    if (res && res.macId) {
                        this.mac = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Airport', 'Operation Succesfull: Airport ' + message);
                        this.dialogRef.close(this.mac);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Aicreaft ' + message + ' Failed. ' + err;
                        /*
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);*/
                        this.spinerService.display(false);
                        this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }

    }

      //#region  Tab Panel Actions

      selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        
        this.selTabIdx = tabIdx;
        this.formGrp.updateValueAndValidity();
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }

    //#endregion
    //#region Data

    public loadCity(cityId: number): Observable<AmsCity> {
        this.spinerService.show();
        return new Observable<AmsCity>(subscriber => {
            /*
            this.adminService.load(cityId)
                .subscribe((resp: AmsCity) => {
                    console.log('loadCity-> resp', resp);
                    subscriber.next(resp);
                    this.spinerService.hide();
                },
                err => {
                    this.spinerService.hide();
                    throw err;
                });*/
        });

    }

    //#endregion

}
