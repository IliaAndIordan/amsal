import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
// -Models
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAlSheduleRoute, AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { Observable, map, of, startWith } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsHub } from 'src/app/@core/services/api/airline/al-hub';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';


export interface IAmsAlSheduleRouteEdit {
    route: AmsAlSheduleRoute;
}

@Component({
    templateUrl: './al-flp-route-edit.dialog.html',
})

export class AmsAlSheduleRouteEditDialog implements OnInit {

    formGrp: FormGroup;

    airline: AmsAirline;
    group: AmsSheduleGroup;
    route: AmsAlSheduleRoute;
    routeNrOpt: number[] = Array.from({ length: 999 }, (_, i) => i + 1);
    pte = AmsPayloadType.E;
    ptb = AmsPayloadType.B;
    ptf = AmsPayloadType.F;
    cpt = AmsPayloadType.C;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spiner: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAirlineClient,
        private apCache: AmsAirportCacheService,
        private hubCache: AmsAirlineHubsCacheService,
        public dialogRef: MatDialogRef<IAmsAlSheduleRouteEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsAlSheduleRouteEdit) {
        this.route = data.route;
    }
    get routeIdFc(): FormControl { return this.formGrp.get('routeId') as FormControl; }
    get routeNrFc(): FormControl { return this.formGrp.get('routeNr') as FormControl; }
    get routeNameFc(): FormControl { return this.formGrp.get('routeName') as FormControl; }

    get flightHFc(): FormControl { return this.formGrp.get('flightH') as FormControl; }
    get depApIdFc(): FormControl { return this.formGrp.get('depApId') as FormControl; }
    get arrApIdFc(): FormControl { return this.formGrp.get('arrApId') as FormControl; }
    get priceEFc(): FormControl { return this.formGrp.get('priceE') as FormControl; }
    get priceBFc(): FormControl { return this.formGrp.get('priceB') as FormControl; }
    get priceFFc(): FormControl { return this.formGrp.get('priceF') as FormControl; }
    get priceCPerKgFc(): FormControl { return this.formGrp.get('priceCPerKg') as FormControl; }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    async ngOnInit(): Promise<void> {
        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.airline = this.cus.airline;

        //console.log('ngOnInit -> hubTypeId:', this.hubTypeId);
        this.formGrp = this.fb.group({
            routeId: new FormControl<number>(this.route ? this.route.routeId : 0, []),
            routeNr: new FormControl<number>(this.route ? this.route.routeNr : 0, []),
            routeName: new FormControl<string>(this.route ? this.route.routeName : '', [Validators.maxLength(256)]),
            flightH: new FormControl<number>(this.route ? this.route.flightH : 0, [Validators.required, Validators.min(0)]),
            depApId: new FormControl<number>(this.route ? this.route.depApId : 0, [Validators.required]),
            arrApId: new FormControl<number>(this.route ? this.route.arrApId : 0, [Validators.required]),
            priceE: new FormControl<number>(this.route ? this.route.priceE : 0, [Validators.required, Validators.min(0)]),
            priceB: new FormControl<number>(this.route ? this.route.priceB : 0, [Validators.required, Validators.min(0)]),
            priceF: new FormControl<number>(this.route ? this.route.priceF : 0, [Validators.required, Validators.min(0)]),
            priceCPerKg: new FormControl<number>(this.route ? this.route.priceCPerKg : 0, [Validators.required, Validators.min(0)]),
        });

        this.formGrp.updateValueAndValidity();
    }

    //#region initAirports

    //#endregion

    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.route) {
                this.route = new AmsAlSheduleRoute();
            }
            const message = this.route.routeId > 0 ? 'Update' : 'Create';
            
            this.route.routeNr = this.routeNrFc.value;
            this.route.routeName = this.routeNameFc.value;
            this.route.depApId = this.depApIdFc.value;
            this.route.arrApId = this.arrApIdFc.value;
            
            this.route.flightH = this.flightHFc.value;
            this.route.priceE = this.priceEFc.value;
            this.route.priceB = this.priceBFc.value;
            this.route.priceF = this.priceFFc.value;
            this.route.priceCPerKg = this.priceCPerKgFc.value;


            //console.log('groupSave -> group:', this.group);
            this.spiner.display(true);

            this.client.sheduleRouteSave(this.route).then((res: AmsAlSheduleRoute) => {
                this.spiner.display(false);
                this.hasSpinner = false;
                console.log('groupSave -> res:', res);
                if (res) {
                    this.route = res;
                    this.errorMessage = undefined;
                    this.toastr.success(`Operation Succesfull: Route ${message}`, `${message} Route`);
                    this.dialogRef.close(this.route);
                }
            }, err => {
                this.spiner.display(false);
                console.error('Observer got an error: ' + err);
                this.errorMessage = 'Route ' + message + ' Failed. ' + err;
                setTimeout((router: Router,) => {
                    this.errorMessage = undefined;
                    this.hasSpinner = false;
                    this.dialogRef.close(undefined);
                }, 2000);
                // this.spinerService.display(false);
                // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
            });

        }


    }

}
