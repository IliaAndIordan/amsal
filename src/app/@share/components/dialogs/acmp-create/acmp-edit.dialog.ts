import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMaintenanceType, AmsMaintenanceTypes, AmsMaintenanceTypesOpt } from 'src/app/@core/services/api/aircraft/enums';
import { EnumViewModel } from 'src/app/@core/models/common/enum-view.model';
import { AmsMaintenancePlan, AmsMaintenancePlanCreateData, AmsMaintenancePlanSaveData } from 'src/app/@core/services/api/aircraft/maintenance-plan';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AircraftStatus, AircraftActionType } from 'src/app/@core/models/pipes/aircraft-statis.pipe';

export interface IAmsAcmpEdit {
    aircraft: AmsAircraft;
    //acmp: AmsMaintenancePlan;
}

@Component({
    templateUrl: './acmp-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsAcmpEditDialog implements OnInit {

    formGrp: FormGroup;

    aircraft: AmsAircraft;
    acmp: AmsMaintenancePlan;

    acmtOpt = AmsMaintenanceTypesOpt;
    acmpType: AmsMaintenanceType;
    hasSpinner = false;
    errorMessage: string;
    minStTime: Date;
    canStartMeintenance: boolean = false;
    idPlaneStand: boolean = false;
    timeToNextFlighthMin:number;
    timeToNextMtnMin:number;
    acmpEndTime:Date;
    price:number;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private acClient: AmsAircraftClient,
        public dialogRef: MatDialogRef<IAmsAcmpEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsAcmpEdit) {
        this.aircraft = data.aircraft;
        //this.acmp = data.acmp;
    }


    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        if (!this.acmp) {
            this.acmp = new AmsMaintenancePlan();
            this.acmp.acId = this.aircraft.acId;
            this.acmp.apId = this.aircraft.currApId;
        }
        
        this.canStartMeintenance = (this.aircraft && this.aircraft.acStatusId === AircraftStatus.Operational &&
            this.aircraft?.acActionTypeId === AircraftActionType.PlaneStand);

        this.acmp.amtId = this.aircraft.getPendingMntType() ?? AmsMaintenanceTypes.ACheck;
        //console.log('ngOnInit -> amtId:', this.acmp.amtId);
        this.acmpType = AmsMaintenanceTypesOpt.find(x => x.amtId === this.acmp.amtId);
        this.acmp.description = this.aircraft.currApCode + ' ' + this.acmpType.name;
        this.acmp.isHomeAp = this.aircraft.currApId == this.cus.airline.apId;
        console.log('ngOnInit -> aircraft:', this.aircraft);
        if (this.aircraft?.nextFlqDepTime) {
            this.timeToNextFlighthMin = ((this.aircraft.nextFlqDepTime.getTime() - new Date().getTime()) / (60 * 1000))
        }
        this.timeToNextMtnMin - this.aircraft.nextMntTimeMin();
        console.log('ngOnInit -> timeToNextFlighthMin:', this.timeToNextFlighthMin);
        this.timeToNextMtnMin = this.aircraft?.nextMntTimeMin();
        console.log('ngOnInit -> timeToNextMtnMin:', this.timeToNextMtnMin);

        this.minStTime = new Date();
        this.acmp.startTime = new Date();
        this.price = AmsMaintenanceType.getPrice(this.aircraft, this.acmpType);
        if (this.aircraft?.acActionTypeId !== AircraftActionType.PlaneStand){

        }
        
        //this.acmp.startTime = this.aircraft.lastFlqArrApId ? this.aircraft.lastFlqArrTime : new Date();
        this.acmp.endTime = new Date();
        this.acmp.endTime.setTime((this.acmp.startTime.getTime() + (this.acmpType.timeMin * 60 * 1000)));
        this.acmpEndTime = this.acmp.endTime;
        //console.log('ngOnInit -> acmp:', this.acmp);
        this.formGrp = this.fb.group({
            amtId: new FormControl(this.acmp ? this.acmp.amtId : '', [Validators.required]),
            stTime: new FormControl(this.acmp ? this.acmp.startTime : this.minStTime, [Validators.required]),
        });

        this.stTimeFc.valueChanges.subscribe(data => {
            
            this.acmpType = AmsMaintenanceTypesOpt.find(x => x.amtId === this.amtIdFc.value);
            this.acmp.startTime = this.stTimeFc && this.stTimeFc.value._d ? this.stTimeFc.value._d : this.stTimeFc.value;
            //console.log('valueChanges -> startDate:', this.acmp.startTime );
            this.acmp.endTime.setTime((this.acmp.startTime.getTime() + (this.acmpType.timeMin * 60 * 1000)));
            this.acmpEndTime = this.acmp.endTime;
            this.price = AmsMaintenanceType.getPrice(this.aircraft, this.acmpType);
            console.log('valueChanges -> endTime:', this.acmp.endTime );
            this.formGrp.updateValueAndValidity();
            
        });

        this.amtIdFc.valueChanges.subscribe(data => {
            //console.log('valueChanges -> stTimeFc; ', data);
            console.log('valueChanges -> amtIdFc; ', this.amtIdFc.value);
            this.acmpType = AmsMaintenanceTypesOpt.find(x => x.amtId === this.amtIdFc.value);
            this.acmp.startTime = this.stTimeFc && this.stTimeFc.value._d ? this.stTimeFc.value._d : this.stTimeFc.value;
            //console.log('valueChanges -> startDate:', this.acmp.startTime );
            this.acmp.endTime.setTime((this.acmp.startTime.getTime() + (this.acmpType.timeMin * 60 * 1000)));
            this.acmpEndTime = this.acmp.endTime;
            this.price = AmsMaintenanceType.getPrice(this.aircraft, this.acmpType);
            console.log('valueChanges -> endTime:', this.acmp.endTime );
            this.formGrp.updateValueAndValidity();
            
        });
    }

    get amtIdFc() { return (this.formGrp.get('amtId') as FormControl); }
    get stTimeFc() { return (this.formGrp.get('stTime') as FormControl); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }


    amtIdChanged(): void {
        //console.log('amtIdChanged ->  value', this.amtId.value);
        const acmtType = this.amtIdFc.value as EnumViewModel;
        this.acmp.amtId = acmtType.id;
        this.formGrp.updateValueAndValidity();
    }

    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;


            const message = this.acmp.acmpId > 0 ? 'Update' : 'Create';

            this.acmp.amtId = this.amtIdFc.value;
            this.acmp.startTime = this.stTimeFc && this.stTimeFc.value._d ? this.stTimeFc.value._d : this.stTimeFc.value;
            //console.log('acMaintenancePlanCreate -> acmp:', this.acmp);
            // this.spinerService.display(true);
            const req = new AmsMaintenancePlanCreateData();
            req.acId = this.acmp.acId;
            req.amtId = this.acmp.amtId;
            req.stTimeMs = parseInt((this.acmp.startTime.getTime() / 1000).toFixed(0));
            this.acClient.acMaintenancePlanCreate(req)
                .subscribe((res: AmsMaintenancePlanSaveData) => {
                    //this.spinerService.display(false);
                    //console.log('acMaintenancePlanCreate -> res:', res);
                    if (res && res.maintenance) {
                        this.acmp = AmsMaintenancePlan.fromJSON(res.maintenance);
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Operation Succesfull: Maintenance Plan ' + message, 'Aircraft Maintenance');
                        this.dialogRef.close(this.acmp);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Maintenance Plan ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
