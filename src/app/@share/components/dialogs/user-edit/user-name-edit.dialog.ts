import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { UserRole, UserRoleOpt } from 'src/app/@core/models/pipes/sx-user-role.pipe';
import { ApiAuthClient } from 'src/app/@core/services/auth/api/api-auth.client';

export interface IAmsUserNameEditDialog {
    user: UserModel;
}

@Component({
    templateUrl: './user-name-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsUserNameEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    user: UserModel;
    userId:number;
    roleOpt = UserRoleOpt;
    admin = UserRole.Admin;

    hasSpinner = false;
    errorMessage: string;
    isAdmin:boolean;
    isOwnUser:boolean;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private authClient: ApiAuthClient,
        public dialogRef: MatDialogRef<IAmsUserNameEditDialog>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsUserNameEditDialog) {
        this.user = data.user;
    }

    get userName() { return this.formGrp.get('userName') as FormControl; }
    get userEmail() { return this.formGrp.get('userEmail') as FormControl; }
    get userPwd() { return this.formGrp.get('userPwd') as FormControl; }
    get userRole() { return this.formGrp.get('userRole') as FormControl; }
    get isReceiveEmails() { return this.formGrp.get('isReceiveEmails') as FormControl; }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.userId=this.user ? this.user.userId : undefined;
        this.isAdmin = this.cus.isAdmin;
        this.isOwnUser = (this.cus.user && this.user && this.cus.user.userId === this.user.userId);
        console.log('edituser-> isAdmin:', this.isAdmin);
        this.formGrp = this.fb.group({
            userName: new FormControl(this.user ? this.user.userName : '', [Validators.required, Validators.maxLength(256)]),
            userEmail: new FormControl(this.user ? this.user.userEmail : '', [Validators.required, Validators.email, Validators.maxLength(512)]),
            userPwd: new FormControl(this.user ? this.user.userPwd : '', [Validators.required, Validators.maxLength(50)]),
            userRole: new FormControl(this.user ? this.user.userRole :  UserRole.User, [Validators.required]),
            isReceiveEmails: new FormControl(this.user ? this.user.isReceiveEmails : false, []),
        });
        if(this.isOwnUser || this.isAdmin){
            this.userPwd.enable();
        }else{
            this.userPwd.disable();
        }
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.user) {
                this.user = new UserModel();
            }
            const message = this.user.userId > 0 ? 'Update' : 'Create';
            
            this.user.userName = this.userName.value;
            this.user.userEmail = this.userEmail.value;
            this.user.userPwd = this.userPwd.value;
            this.user.userRole = this.userRole.value;
            this.user.isReceiveEmails = this.isReceiveEmails.value;

            // this.spinerService.display(true);

            this.authClient.userSave(this.user)
                .subscribe((res: UserModel) => {
                    // this.spinerService.display(false);
                    console.log('userSave -> res:', res);
                    if (res && res.userId) {
                        this.user = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('User', 'Operation Succesfull: User ' + message);
                        this.dialogRef.close(this.user);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'User ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
