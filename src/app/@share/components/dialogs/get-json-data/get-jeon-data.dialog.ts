import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';

// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import {  ResponseAmsJsonData } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAdminService } from 'src/app/admin/admin.service';


export interface IGetJsonData {
    sql: string;
}

@Component({
    templateUrl: './get-jeon-data.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class GetJsonDataDialog implements OnInit {

    formGrp: UntypedFormGroup;

    sql: string;
    result:any[];
    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private adminService: AmsAdminService,
        public dialogRef: MatDialogRef<IGetJsonData>,
        @Inject(MAT_DIALOG_DATA) public data: IGetJsonData) {
        this.sql = data.sql;
    }

    get sqlStr() { return this.formGrp.get('sqlStr'); }

    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
       
        this.formGrp = this.fb.group({
            sqlStr: new UntypedFormControl(this.sql ? this.sql : '', [Validators.required, Validators.minLength(20), Validators.maxLength(4000)]),
        });
       
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;
            this.sql = this.sqlStr.value;

            this.client.getJsonData(this.sql, 1000)
                .subscribe((res: ResponseAmsJsonData) => {
                    // this.spinerService.display(false);
                    console.log('getJsonData -> res:', res);
                    if (res && res.data) {
                        this.result = res.data.jsondata;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        // this.dialogRef.close(this.mac);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'call failed Failed. ' + err;
                        /*
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);*/
                        this.spinerService.display(false);
                        this.hasSpinner = false;
                        this.toastr.error('Compnay', 'Operation Failed: message ' + err);
                    },
                    () => console.log('Observer got a complete notification'));

        }

    }

      //#region  Tab Panel Actions

     
    //#endregion
    

}
