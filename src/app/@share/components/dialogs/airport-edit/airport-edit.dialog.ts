import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { AmsWadCountryService } from 'src/app/wad/country/country.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCity, AmsCityTableCriteria, AmsCountry, AmsRegion, AmsState, AmsSubregion, ResponseAmsCityTableData } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsWadStateService } from 'src/app/wad/state/state.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { ApType, ApTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';


export interface IAmsAirportEdit {
    city: AmsCity;
    airport: AmsAirport;
}

@Component({
    templateUrl: './airport-edit.dialog.html',
})

export class AmsAirportEditDialog implements OnInit {

    formGrp: FormGroup;

    region: AmsRegion;
    subregion: AmsSubregion;
    country: AmsCountry;
    state: AmsState;
    city: AmsCity;
    airport: AmsAirport;
    typeOpt = ApTypeOpt;

    cities$: Observable<AmsCity[]>;
    citiesOpt: Observable<AmsCity[]>;
    citiesAll: AmsCity[];


    amsStatusOpt = AmsStatusOpt;
    wadStatusOpt = WadStatusOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private apClient: AmsAirportClient,
        private cntClient: AmsCountryClient,
        private stService: AmsWadStateService,
        public dialogRef: MatDialogRef<IAmsAirportEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsAirportEdit) {
        this.city = data.city;
        this.airport = data.airport;
    }

    get iata() { return this.formGrp.get('iata'); }
    get icao() { return this.formGrp.get('icao'); }
    get apNameFc(): FormControl { return this.formGrp.get('apName') as FormControl; }
    get lat() { return this.formGrp.get('lat'); }
    get lon() { return this.formGrp.get('lon'); }
    get elevation() { return this.formGrp.get('elevation'); }
    get typeId() { return this.formGrp.get('typeId'); }
    get active() { return this.formGrp.get('active'); }
    get wikiUrl() { return this.formGrp.get('wikiUrl'); }
    get amsStatus() { return this.formGrp.get('amsStatus'); }
    get wadStatus() { return this.formGrp.get('wadStatus'); }

    get homeUrl() { return this.formGrp.get('homeUrl'); }
    get notes() { return this.formGrp.get('notes'); }
    get utc() { return this.formGrp.get('utc'); }
    get baseTypeId() { return this.formGrp.get('baseTypeId'); }
    get apCityFc(): FormControl { return this.formGrp.get('apCity') as FormControl; }




    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.region = this.stService.region;
        this.subregion = this.stService.subregion;
        this.country = this.stService.country;
        this.state = this.stService.state;
        if (!this.city && this.stService.city) {
            this.city = this.stService.city;
        }


        this.formGrp = this.fb.group({
            iata: new FormControl(this.airport && this.airport.iata ? this.airport.iata : '', [Validators.maxLength(4)]),
            icao: new FormControl(this.airport && this.airport.icao ? this.airport.icao : '', [Validators.maxLength(25)]),
            apName: new FormControl(this.airport && this.airport.apName ? this.airport.apName : '', [Validators.required, Validators.maxLength(1020)]),
            lat: new FormControl(this.airport && this.airport.lat ? this.airport.lat : 0, [Validators.required]),
            lon: new FormControl(this.airport && this.airport.lon ? this.airport.lon : 0, [Validators.required]),
            elevation: new FormControl(this.airport && this.airport.elevation ? this.airport.elevation : 0, [Validators.required]),
            typeId: new FormControl(this.airport && this.airport.typeId ? this.airport.typeId : ApType.Airfield, [Validators.required]),
            active: new FormControl(this.airport && this.airport.active ? this.airport.active : false),
            wikiUrl: new FormControl(this.airport && this.airport.wikiUrl ? this.airport.wikiUrl : '', [Validators.maxLength(1000)]),
            amsStatus: new FormControl(this.airport && this.airport.amsStatus ? this.airport.amsStatus : AmsStatus.DataCheckRequired, [Validators.required]),
            wadStatus: new FormControl(this.airport && this.airport.wadStatus ? this.airport.wadStatus : WadStatus.ToDo, [Validators.required]),
            homeUrl: new FormControl(this.airport ? this.airport.homeUrl : '', [Validators.maxLength(1000)]),
            notes: new FormControl(this.airport ? this.airport.notes : '', [Validators.maxLength(1000)]),
            utc: new FormControl(this.airport && this.airport.utc ? this.airport.utc : 0, [Validators.required]),
            baseTypeId: new FormControl(this.airport && this.airport.baseTypeId ? this.airport.baseTypeId : ApType.Airfield, [Validators.required]),
            apCity: new FormControl(this.city ? this.city.cityId : '', [Validators.required]),
        });

        this.citiesOpt = this.apCityFc.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterCities(val) :
                    (this.citiesAll ? this.citiesAll.slice() : new Array<AmsCity>()))
            );

        
            this.loadCitiesAll();

    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.city) {
                this.city = new AmsCity();
            }
            const message = this.airport.apId > 0 ? 'Update' : 'Create';
            this.airport.iata = this.iata.value;
            this.airport.icao = this.icao.value;
            this.airport.apName = this.apNameFc.value;
            this.airport.lat = this.lat.value;
            this.airport.lon = this.lon.value;
            this.airport.elevation = this.elevation.value;
            this.airport.typeId = this.typeId.value;
            this.airport.active = this.active.value;
            this.airport.wikiUrl = this.wikiUrl.value;
            this.airport.amsStatus = this.amsStatus.value;
            this.airport.wadStatus = this.wadStatus.value;
            this.airport.homeUrl = this.homeUrl.value;
            this.airport.notes = this.notes.value;
            this.airport.utc = this.utc.value;
            this.airport.baseTypeId = this.baseTypeId.value;
            this.airport.regionId = this.stService.region.regionId;
            this.airport.subregionId = this.stService.subregion.subregionId;
            this.airport.countryId = this.stService.country.countryId;
            this.airport.stateId = this.stService.state.stateId;
            this.airport.cityId = this.city.cityId;// this.stService.city.cityId;
            // this.spinerService.display(true);

            this.apClient.airportSave(this.airport)
                .subscribe((res: AmsAirport) => {
                    // this.spinerService.display(false);
                    //console.log('countrySave -> res:', res);
                    if (res && res.apId) {
                        this.airport = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Airport', 'Operation Succesfull: Airport ' + message);
                        this.dialogRef.close(this.airport);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Airport ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }

    }

    //#region Filter City

    apCityChange(event: MatAutocompleteSelectedEvent) {
        console.log('apCityChange ->  apCityFc value', this.apCityFc.value);
        if (this.apCityFc.valid) {
            const value: AmsCity = this.apCityFc.value;
            if (value && value.cityId) {
                this.city = value;
            }
        }
    }

    displayCity(value: AmsCity): string {
        let rv: string = '';
        if (value) {
            rv = value.ctName ? value.ctName : value?.cityId?value.cityId.toString():undefined;
        }
        return rv;
    }

    filterCities(val: any): AmsCity[] {
        // console.log('filterDpc -> val', val);
        let sr: AmsCity;
        if (val && val.cityId) {
            sr = Object.assign(new AmsCity(), val);
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.ctName || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<AmsCity>();
        if (this.citiesAll && this.citiesAll.length) {
            rv = this.citiesAll.filter(x =>
                x.ctName.toLowerCase().indexOf(filterValue) > -1);
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }


    initCity() {
        this.citiesOpt = of(this.citiesAll);

        let sr: AmsCity;

        if (this.citiesAll && this.citiesAll.length > 0 && this.city) {
            sr = this.city ? this.citiesAll.find(x => x.cityId === this.city.cityId) : undefined;
        }

        this.formGrp.patchValue({ apCity: sr });
        this.formGrp.updateValueAndValidity();
    }





    //#endregion

    //#region Data

    public loadCity(cityId: number): Observable<AmsCity> {
        this.spinerService.show();
        return new Observable<AmsCity>(subscriber => {

            this.stService.loadCity(cityId)
                .subscribe((resp: AmsCity) => {
                    //console.log('loadCity-> resp', resp);
                    subscriber.next(resp);
                    this.spinerService.hide();
                },
                    err => {
                        this.spinerService.hide();
                        throw err;
                    });
        });

    }

     loadCitiesAll():void {
        this.spinerService.show();
        let criteria = new AmsCityTableCriteria()
        criteria.stateId = this.state.stateId;
        criteria.limit = 250;
        criteria.offset = 0;
        criteria.sortCol = 'ctName';
        criteria.sortDesc = false;
         this.cntClient.cityTableReq(criteria).then((resp: ResponseAmsCityTableData)=>{
            this.citiesAll = new Array<AmsCity>();
           
            if(resp && resp.success){
                if(resp.data?.cities && resp.data.cities.length>0){
                    resp.data.cities.forEach(element => {
                        this.citiesAll.push(AmsCity.fromJSON(element));
                    });
                }
            }
            if (this.airport && this.airport.cityId !== this.city.cityId) {
                    this.loadCity(this.airport.cityId)
                        .subscribe((resp: AmsCity) => {
                            this.spinerService.hide();
                            this.citiesAll.push(resp);
                            this.city = resp;
                            this.initCity();
                        });
                
            } else{
                this.spinerService.hide();
                this.initCity();
            }
            
         });
        


    }

    //#endregion

}
