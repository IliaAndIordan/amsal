import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { AmsWadCountryService } from 'src/app/wad/country/country.service';
// -Models
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';


export interface IAmsStateEdit {
    state: AmsState;
    country?: AmsCountry;
}

@Component({
    templateUrl: './state-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsStateEditDialog implements OnInit {

    formGrp: UntypedFormGroup;

    state: AmsState;
    country:AmsCountry;
    amsStatusOpt = AmsStatusOpt;
    wadStatusOpt = WadStatusOpt;

    hasSpinner = false;
    errorMessage: string;

    constructor(
        private fb: UntypedFormBuilder,
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private client: AmsCountryClient,
        private cService: AmsWadCountryService,
        public dialogRef: MatDialogRef<IAmsStateEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsStateEdit) {
        this.state = data.state;
        this.country = data.country;
    }
    get stName() { return this.formGrp.get('stName'); }
    get iso() { return this.formGrp.get('iso'); }
    get wikiUrl() { return this.formGrp.get('wikiUrl'); }
    get amsStatus() { return this.formGrp.get('amsStatus'); }
    get wadStatus() { return this.formGrp.get('wadStatus'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field ' + fname + '  is required.' :
            field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
                field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;

        this.formGrp = this.fb.group({
            stName: new UntypedFormControl(this.state ? this.state.stName : '', [Validators.required, Validators.maxLength(256)]),
            iso: new UntypedFormControl(this.state ? this.state.iso : '', [Validators.required, Validators.maxLength(10)]),
            wikiUrl: new UntypedFormControl(this.state ? this.state.wikiUrl : '', [Validators.maxLength(1000)]),
            amsStatus: new UntypedFormControl(this.state && this.state.amsStatus ? this.state.amsStatus : AmsStatus.DataCheckRequired, [Validators.required]),
            wadStatus: new UntypedFormControl(this.state && this.state.wadStatus ? this.state.wadStatus : WadStatus.ToDo, [Validators.required]),
        });
    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;

            if (!this.state) {
                this.state = new AmsState();
                this.state.countryId = this.cService.country.countryId;
            }
            const message = this.state.stateId > 0 ? 'Update' : 'Create';

            this.state.stName = this.stName.value;
            this.state.iso = this.iso.value;
            this.state.wikiUrl = this.wikiUrl.value;
            this.state.amsStatus = this.amsStatus.value;
            this.state.wadStatus = this.wadStatus.value;

            // this.spinerService.display(true);

            this.client.stateSave(this.state)
                .subscribe((res: AmsState) => {
                    // this.spinerService.display(false);
                    console.log('stateSave -> res:', res);
                    if (res && res.stateId) {
                        this.state = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('State', 'Operation Succesfull: State ' + message);
                        this.dialogRef.close(this.state);
                    }
                },
                    err => {
                        // this.spinerService.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'State ' + message + ' Failed. ' + err;
                        this.toastr.error(this.errorMessage, 'WAD State Update');
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);
                        // this.spinerService.display(false);
                        // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }

}
