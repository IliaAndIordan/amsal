import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormBuilder, UntypedFormControl, Validators, FormControl } from '@angular/forms';
// -Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAdminService } from 'src/app/admin/admin.service';
// -Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCity, AmsCountry, AmsState, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { AmsStatus, AmsStatusOpt, WadStatus, WadStatusOpt } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsWadService } from 'src/app/wad/wad.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { AmsCfgCharter } from 'src/app/@core/services/api/flight/charter-dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsPayloadType, AmsPayloadTypeOpt } from 'src/app/@core/services/api/flight/enums';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';

export class CharterCode {
    code: string;
    note: string;
}
export const CharterCodeOpt: CharterCode[] = [
    { code: 'DRC', note: 'Danube River Cruises Tourists' },
    { code: 'SBT', note: 'Sunny Beach Tourists' },
    { code: 'GST', note: 'Golden Sands Tourists' },
    { code: 'GBG', note: 'BG Government Personnel' }
]

export interface IAmsCfgCharterEdit {
    charter: AmsCfgCharter;
}

@Component({
    templateUrl: './cfg-charter-edit.dialog.html',
})

// eslint-disable-next-line @angular-eslint/component-class-suffix
export class AmsCfgCharterEditDialog implements OnInit {

    cargoImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';

    formGrp: UntypedFormGroup;

    payloadTypeOpt = AmsPayloadTypeOpt;
    charter: AmsCfgCharter;

    airports: AmsAirport[];
    airports$: Observable<AmsAirport[]>;
    depApOpt: Observable<AmsAirport[]>;
    arrApOpt: Observable<AmsAirport[]>;
    disableCargo: boolean = true;
    disablePax: boolean = false;

    periodDaysOpt: number[] = [1, 3, 7, 14, 30];
    minNextRun: Date;
    hasSpinner = false;
    errorMessage: string;
    charterCodeOpt = CharterCodeOpt;
    charterCode: CharterCode;

    constructor(
        private fb: UntypedFormBuilder,
        private toastr: ToastrService,
        private spinner: SpinnerService,
        private cus: CurrentUserService,
        private adminService: AmsAdminService,
        private client: AmsFlightClient,
        private apCache: AmsAirportCacheService,
        public dialogRef: MatDialogRef<IAmsCfgCharterEdit>,
        @Inject(MAT_DIALOG_DATA) public data: IAmsCfgCharterEdit) {
        this.charter = data.charter;
    }

    get arrAp() { return this.formGrp.get('arrAp'); }
    get depAp() { return this.formGrp.get('depAp'); }
    get flightName() { return this.formGrp.get('flightName'); }
    get flightDescription() { return this.formGrp.get('flightDescription'); }
    get payloadType() { return this.formGrp.get('payloadType'); }
    get pax() { return this.formGrp.get('pax'); }
    get cargoKg() { return this.formGrp.get('cargoKg'); }
    get periodDays() { return this.formGrp.get('periodDays'); }
    get nextRun() { return this.formGrp.get('nextRun'); }
    get codeOpt() { return this.formGrp.get('codeOpt'); }


    fError(fname: string): string {
        const field = this.formGrp.get(fname);
        return field.hasError('required') ? 'Field is required.' :
            field.hasError('min') ? 'Validation Error. Field value to low.' :
                field.hasError('max') ? 'Validation Error. Field value to height.' :
                    field.hasError('minlength') ? 'Validation Error. Field  to short.' :
                        field.hasError('maxlength') ? 'Field to long.' : '';
    }

    ngOnInit(): void {

        this.errorMessage = undefined;
        this.hasSpinner = false;
        this.charterCode = this.charter && this.charter.flightName ? this.charterCodeOpt.find(x => x.code === this.charter.flightName) : undefined;
        this.minNextRun = new Date(new Date().getTime() + (1000 * 60 * 60 * 1));

        this.formGrp = this.fb.group({
            arrAp: new UntypedFormControl(this.charter ? this.charter.arrApId : '', [Validators.required]),
            depAp: new UntypedFormControl(this.charter ? this.charter.depApId : '', [Validators.required]),
            flightName: new UntypedFormControl(this.charter ? this.charter.flightName : '', [Validators.required, Validators.minLength(3), Validators.maxLength(5)]),
            flightDescription: new UntypedFormControl(this.charter ? this.charter.flightDescription : '', [Validators.required, Validators.maxLength(500)]),
            payloadType: new UntypedFormControl(this.charter ? this.charter.payloadType : AmsPayloadType.E, [Validators.required]),
            pax: new UntypedFormControl(this.charter ? this.charter.pax : 0, [Validators.min(1)]),
            cargoKg: new UntypedFormControl(this.charter ? this.charter.cargoKg : 0, [Validators.min(1)]),
            periodDays: new UntypedFormControl(this.charter ? this.charter.periodDays : 30, [Validators.required]),
            nextRun: new UntypedFormControl(this.charter && this.charter.nextRun ? this.charter.nextRun : 30, []),
            codeOpt: new FormControl(this.charterCode ? this.charterCode.code : '', []),
        });
        
        this.airports$ = this.apCache.airports;

        this.airports$.subscribe(airports => {
            this.airports = airports;
            this.initDepAirports();
            this.initArrAirports();
        });

        this.formGrp.updateValueAndValidity();
        this.payloadTypeChanged();

    }


    onCansel(): void {
        this.dialogRef.close();
    }

    onSubmit() {
        this.formGrp.updateValueAndValidity();
        if (this.formGrp.valid) {
            this.errorMessage = undefined;
            this.hasSpinner = true;


            if (!this.charter) {
                this.charter = new AmsCfgCharter();
            }
            const message = this.charter.charterId > 0 ? 'Update' : 'Create';
            const depAp = this.depAp.value as AmsAirport;
            const arrAp = this.arrAp.value as AmsAirport;

            this.charter.depApId = depAp ? depAp.apId : undefined;
            this.charter.arrApId = arrAp ? arrAp.apId : undefined;
            const pt = this.payloadType.value as AmsPayloadType;
            this.charter.payloadType = pt;
            this.charter.pax = parseInt(this.pax.value);
            this.charter.cargoKg = parseFloat(this.cargoKg.value);
            this.charter.flightName = this.flightName.value;
            this.charter.flightDescription = this.flightDescription.value;
            this.charter.periodDays = parseInt(this.periodDays.value);
            //this.charter.nextRun = this.nextRun.value;
            this.charter.nextRun = this.nextRun && this.nextRun.value._d ? this.nextRun.value._d : this.nextRun.value;
            this.spinner.display(true);
            console.log('countrySave -> charter:', this.charter);
            this.client.cfgCharterSave(this.charter)
                .subscribe((res: AmsCfgCharter) => {
                    this.spinner.display(false);
                    //console.log('countrySave -> res:', res);
                    if (res && res.charterId) {
                        this.charter = res;
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.toastr.success('Charter Configuration', 'Operation Succesfull: Charter ' + message);
                        this.dialogRef.close(this.charter);
                    }
                },
                    err => {
                        this.spinner.display(false);
                        console.error('Observer got an error: ' + err);
                        this.errorMessage = 'Charter ' + message + ' Failed. ' + err;
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.dialogRef.close(undefined);
                        }, 2000);

                        this.toastr.error('Charter Configuration', 'Operation Failed: Charter ' + message);
                    },
                    () => console.log('Observer got a complete notification'));

        }


    }



    //#region Filter Airports

    payloadTypeChanged(): void {
        //console.log('payloadTypeChanged ->  value', this.payloadType.value);
        const payloadType = this.payloadType.value as AmsPayloadType;
        if (payloadType === AmsPayloadType.C) {
            this.disableCargo = false;
            this.disablePax = true;
            this.formGrp.patchValue({ pax: '' });
            this.formGrp.patchValue({ cargoKg: this.charter && this.charter.cargoKg ? this.charter.cargoKg : 100 });
            this.pax.disable();
            this.cargoKg.enable();
        } else {
            this.disableCargo = true;
            this.disablePax = false;
            this.formGrp.patchValue({ pax: this.charter && this.charter.pax ? this.charter.pax : 50 });
            this.formGrp.patchValue({ cargoKg: '' });
            this.pax.enable();
            this.cargoKg.disable();
        }
        this.formGrp.updateValueAndValidity();
    }

    depApValueChange(event: MatAutocompleteSelectedEvent) {
        //console.log('depApValueChange ->  depAp value', this.depAp.value);
        if (this.depAp.valid) {
            const value: AmsAirport = this.depAp.value;
            if (value && value.apId) {
                //this.loadCountries(subregion);
            }
        }
    }

    arrApValueChange(event: MatAutocompleteSelectedEvent) {
        //console.log('arrApValueChange ->  arrAp value', this.arrAp.value);
        if (this.arrAp.valid) {
            const value: AmsAirport = this.arrAp.value;
            if (value && value.apId) {
                //this.loadCountries(subregion);
            }
        }
    }

    displayAirport(value: AmsAirport): string {
        let rv: string = '';
        if (value) {
            rv = ' ';
            rv = value.apLabel ? value.apLabel : value.apName;
            /*
            rv = ' ';
            rv += value.iata?value.iata:value.icao + ' ';
            rv += value.apName ? value.apName : value.apId;
            rv += ', ' + value.ctName + ' ';
            rv += value.iso2;
            */
        }
        return rv;
    }

    filterAirports(val: any): AmsAirport[] {
        // console.log('filterDpc -> val', val);
        let sr: AmsAirport;
        if (val && val.apId) {
            sr = val as AmsAirport;
        }

        // console.log('filterBranches -> this.selDpc', this.selDpc);
        const value = val.apName || val; // val can be companyName or string
        const filterValue = value ? value.toLowerCase() : '';
        // console.log('_filter -> filterValue', filterValue);
        let rv = new Array<AmsAirport>()
        if (this.airports && this.airports.length) {
            rv = this.airports.filter(x => (
                x.apName.toLowerCase().indexOf(filterValue) > -1 ||
                (x.iata && x.iata.toLowerCase().indexOf(filterValue) > -1) ||
                (x.icao && x.icao.toLowerCase().indexOf(filterValue) > -1) ||
                x?.cName.toLowerCase().indexOf(filterValue) > -1 ||
                x.ctName.toLowerCase().indexOf(filterValue) > -1));
        }
        // console.log('_filter -> rv', rv);
        return rv;
    }


    initDepAirports() {
        this.depApOpt = of(this.airports);

        this.depApOpt = this.depAp.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterAirports(val) :
                    (this.airports ? this.airports.slice() : new Array<AmsAirport>()))
            );

        let sr: AmsAirport;

        if (this.airports && this.airports.length > 0) {
            sr = this.airports && this.depAp.value ? this.airports.find(x => x.apId === this.depAp.value) : undefined;
        }

        this.formGrp.patchValue({ depAp: sr });
        this.formGrp.updateValueAndValidity();
    }

    initArrAirports() {
        this.arrApOpt = of(this.airports);

        this.arrApOpt = this.arrAp.valueChanges
            .pipe(
                startWith(null),
                map(val => val ? this.filterAirports(val) :
                    (this.airports ? this.airports.slice() : new Array<AmsAirport>()))
            );

        let sr: AmsAirport;

        if (this.airports && this.airports.length > 0) {
            sr = this.airports && this.arrAp.value ? this.airports.find(x => x.apId === this.arrAp.value) : undefined;
        }

        this.formGrp.patchValue({ arrAp: sr });
        this.formGrp.updateValueAndValidity();
    }

    codeOptChanged(): void {
       
        const code = this.codeOpt.value;
        //console.log('codeOptChanged ->  value', code);
        this.charterCode = code ? this.charterCodeOpt.find(x => x.code === code) : undefined;
        //console.log('codeOptChanged -> charterCode:', this.charterCode);
        if (this.charterCode) {
            this.formGrp.patchValue({ flightName: this.charterCode.code });
            this.formGrp.patchValue({ flightDescription: this.charterCode.note });
            this.formGrp.updateValueAndValidity();
        }
    }

    //#endregion


}
