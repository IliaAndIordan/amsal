import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core';
import { ChartConfiguration, ChartOptions, ChartData, ChartEvent, ChartType, ChartDataset, ScatterDataPoint, BubbleDataPoint } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

// Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsChartColorsOpt, Ng2ChartJsCahartData, Ng2ChartJsCahartPieData, Ng2ChartJsCahartPieDataset } from '../ams-chart-line/dto';
import { Color } from '../models/color.model';


export const chartColorYellow = 'rgba(148,159,177,0.5)';
export const chartColorLightGray = 'rgba(255, 221, 153,0.5)';
export const chartColorGreen = 'rgba(0, 230, 77,0.5)';
export const chartColorBlue = 'rgba(0, 0, 255,0.5)';
export const chartColorRed = 'rgba(255, 0, 0,0.5)';
export const chartColoDarkGray = 'rgba(77,83,96,0.2)';


@Component({
    selector: 'ams-chart-pie-sm',
    templateUrl: './ams-chart-pie-sm.component.html',
})
export class AmsChartPieSmallComponent implements OnInit, OnDestroy, OnChanges {

    /**
     * BINDINGS
     */
    @Input() chartData: any;
    @Input() chartLabels: string[];
    @Input() title: string;
    @Input() sizepx: number;
    @Input() legend: boolean;
    @Input() environment: string;
    @Input() chartColors: string[];
    @Output() chartClick: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
    data: Ng2ChartJsCahartPieData;
    public pieChartOptions: ChartOptions<'pie'> = {
        responsive: true,
      };
      /*
    pieChartOptions: ChartConfiguration['options'] = {
        color: 'white',
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            title: {
                display: true,
                color: 'white',
            },
            legend: {
                display: true,
                position: 'top',
            },
            /*
            datalabels: {
                formatter: (value, ctx) => {
                    if (ctx.chart.data.labels) {
                        return ctx.chart.data.labels[ctx.dataIndex];
                    }
                },
            },*
        },

    };
    */
    chartType: ChartType = 'pie';
    public pieChartPlugins = [];

    pieChartColors = [
        {
            backgroundColor: ['rgba(148,159,177,0.5)', 'rgba(255, 221, 153,0.5)',
                'rgba(0, 230, 77,0.5)', 'rgba(0, 0, 255,0.5)', 'rgba(77,83,96,0.5)'],
        },
    ];
    lineChartColors: Color[] = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // yelow grey
            backgroundColor: 'rgba(255, 221, 153,0.6)',
            borderColor: 'rgba(255, 221, 153,1)',
            pointBackgroundColor: 'rgba(255, 221, 153,1)',
            pointBorderColor: '#ffdd99',
            pointHoverBackgroundColor: '#ffdd99',
            pointHoverBorderColor: 'rgba(255, 221, 153,1)'
        },
        { // green
            backgroundColor: 'rgba(0, 179, 60,0.5)',
            borderColor: 'green',
            pointBackgroundColor: 'rgba(0, 102, 34,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(0, 102, 34,0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // red
            backgroundColor: 'rgba(255,0,0,0.3)',
            borderColor: 'red',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];

    public pieChartLabels = [ [ 'Download', 'Sales' ], [ 'In', 'Store', 'Sales' ], 'Mail Sales' ];
  public pieChartDatasets = [ {
    data: [ 300, 500, 100 ]
  } ];
    styleSize = 'height: 100px;width: 100px;';
    constructor(private cus: CurrentUserService) {
    }


    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['chartData']) {
            this.initFields();
        }

    }

    initFields(): void {
       
        //this.data.labels = this.chartLabels;
        /*
        if (this.title && this.title.length > 0) {
            this.pieChartOptions.plugins.title.display = true;
            this.pieChartOptions.plugins.title.text = (this.title ? this.title : '') + ' (' + this.environment + ')';
        } else {
            this.pieChartOptions.plugins.title.display = false;
        }
        */

        if (this.sizepx) {
            this.styleSize = 'height: ' + this.sizepx.toString() + 'px;width: ' + this.sizepx.toString() + 'px;';
        }
        if (this.chartColors) {
            this.pieChartColors = [{ backgroundColor: this.chartColors, }];
        }

        //console.log('this.chartOptions.scales:',  this.chartOptions.scales);

    }

    onChartClick(event) {
        console.log('onChartClick() -> event: ', event);
        this.chartClick.emit(event);
    }

}
