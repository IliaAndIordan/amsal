import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmsChartLineComponent } from './ams-chart-line/ams-chart-line.component';
import { AmsChartPieSmallComponent } from './ams-chart-pie-sm/ams-chart-pie-sm.component';
import { AmsChartLineXLabelLessComponent } from './ams-chart-line/x-label-less/chart-line-x-label-less.component';
import { AmsChartPieComponent } from './pie/ams-chart-pie/ams-chart-pie.component';
import { AmsChartLineTwoYComponent } from './ams-chart-line/ams-chart-line-2y.component';
import { BaseChartDirective, provideCharts, withDefaultRegisterables } from 'ng2-charts';

@NgModule({
   
    imports: [
        CommonModule,
        BaseChartDirective,
    ],
    declarations: [
        AmsChartLineComponent,
        AmsChartPieSmallComponent,
        AmsChartLineXLabelLessComponent,
        AmsChartLineTwoYComponent,
        AmsChartPieComponent,

    ],
    exports: [
        BaseChartDirective,
        AmsChartLineComponent,
        AmsChartPieSmallComponent,
        AmsChartLineXLabelLessComponent,
        AmsChartLineTwoYComponent,
        AmsChartPieComponent,
    ],
    providers: [provideCharts(withDefaultRegisterables())],
})
export class AmsChartsModule { }
