import { ChartConfiguration, ChartOptions, ChartData, ChartEvent, ChartType } from 'chart.js';

export class AmsColorRgb {
    public red: number;
    public green: number;
    public blue: number;

    constructor(r: number, g: number, b: number) {
        this.red = r;
        this.green = g;
        this.blue = b;
    }
}

export const AmsChartColors = {
    white: new AmsColorRgb(242, 242, 242),
    gray: new AmsColorRgb(148, 159, 177),
    yelow: new AmsColorRgb(255, 221, 153),
    brown: new AmsColorRgb(191, 128, 64),
    darkGrey: new AmsColorRgb(77, 83, 96),
    red: new AmsColorRgb(255, 0, 0),
    green: new AmsColorRgb(0, 230, 77),
    blue: new AmsColorRgb(63, 81, 181),
    blueDark: new AmsColorRgb(53, 68, 151),
    blueLight: new AmsColorRgb(0, 183, 255),
}

export const AmsChartColorsOpt:AmsColorRgb[] = [
    AmsChartColors.white,
    AmsChartColors.yelow,
    AmsChartColors.gray,
    AmsChartColors.brown,
    AmsChartColors.darkGrey,
    AmsChartColors.red,
    AmsChartColors.green,
]

export class Ng2ChartJsLineColor {
    public backgroundColor: string;
    public borderColor: string;
    public pointBackgroundColor: string;
    public pointBorderColor: string;
    public pointHoverBackgroundColor: string;
    public pointHoverBorderColor: string;
}

export class Ng2ChartJsCahartData extends Ng2ChartJsLineColor {
    public data: any[];
    public label: string;
    public fill?: string;

    public setColorRgb(r: number, g: number, b: number, fill: string = 'origin'): void {
            this.backgroundColor = 'rgba(' + r.toString() + ', ' + g.toString() + ', ' + b.toString() + ',0.2)',
            this.borderColor = 'rgba(' + r.toString() + ', ' + g.toString() + ', ' + b.toString() + ',1)',
            this.pointBackgroundColor = 'rgba(' + r.toString() + ', ' + g.toString() + ', ' + b.toString() + ',1)',
            this.pointBorderColor = 'rgba(' + r.toString() + ', ' + g.toString() + ', ' + b.toString() + ',1)',
            this.pointHoverBackgroundColor = 'rgba(' + r.toString() + ', ' + g.toString() + ', ' + b.toString() + ',1)',
            this.pointHoverBorderColor = 'rgba(' + r.toString() + ', ' + g.toString() + ', ' + b.toString() + ',1)',
            this.fill = 'origin'
    }

    public setColorAms(color: AmsColorRgb): void {
        this.setColorRgb(color.red, color.green, color.blue);
    }
}

export class Ng2ChartJsCahartPieDataset{
    public label?:string;
    public data?: number[];
    public backgroundColor?:string[];
}

export class Ng2ChartJsCahartPieData{
    public labels?: string[];
    public datasets?:Ng2ChartJsCahartPieDataset[];
    public hoverOffset:number = 4;
}

