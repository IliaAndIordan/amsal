import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core';
// Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { Chart, ChartConfiguration, ChartEvent, ChartType } from 'chart.js';
import { AmsChartColorsOpt, Ng2ChartJsCahartData } from './dto';
import { ChartOptions } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

export const chartColors = {
    gray:    { // grey
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    yelow:{ // yelow grey
        backgroundColor: 'rgba(255, 221, 153,0.6)',
        borderColor: 'rgba(255, 221, 153,1)',
        pointBackgroundColor: 'rgba(255, 221, 153,1)',
        pointBorderColor: '#ffdd99',
        pointHoverBackgroundColor: '#ffdd99',
        pointHoverBorderColor: 'rgba(255, 221, 153,1)'
    },
    darkGrey: { // dark grey
        backgroundColor: 'rgba(77,83,96,0.2)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    red:{ // red
        backgroundColor: 'rgba(255,0,0,0.3)',
        borderColor: 'red',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    green:{ // red
        backgroundColor: 'rgba(0, 230, 77,0.3)',
        borderColor: 'rgba(0, 230, 77,1)',
        pointBackgroundColor: 'rgba(0, 230, 77,1)',
        pointBorderColor: 'rgba(0, 230, 77,1)',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(0, 230, 77,0.8)'
    }
}

export const lineColorYellow = { // yelow grey
    backgroundColor: 'rgba(255, 221, 153,0.6)',
    borderColor: 'rgba(255, 221, 153,1)',
    pointBackgroundColor: 'rgba(255, 221, 153,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(255, 221, 153,1)'
};
export const lineColorLightGray = { // grey
    backgroundColor: 'rgba(148,159,177,0.2)',
    borderColor: 'rgba(148,159,177,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
};
export const lineColorGreen = {
    backgroundColor: 'rgba(0, 230, 77,0.6)',
    borderColor: 'rgba(0, 230, 77,1)',
    pointBackgroundColor: 'rgba(0, 230, 77,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(0, 230, 77,1)'
};
export const lineColorBlue = {
    backgroundColor: 'rgba(0, 0, 255,0.6)',
    borderColor: 'rgba(0, 0, 255,1)',
    pointBackgroundColor: 'rgba(0, 0, 255,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(0, 0, 255,1)'
};
export const lineColorRed = {
    backgroundColor: 'rgba(255, 0, 0,0.6)',
    borderColor: 'rgba(255, 0, 0,1)',
    pointBackgroundColor: 'rgba(255, 0, 0,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(255, 0, 0,1)'
};
export const lineColorDarkGray = {
    backgroundColor: 'rgba(77,83,96,0.2)',
    borderColor: 'rgba(77,83,96,1)',
    pointBackgroundColor: 'rgba(77,83,96,1)',
    pointBorderColor: '#ffdd99',
    pointHoverBackgroundColor: '#ffdd99',
    pointHoverBorderColor: 'rgba(77,83,96,1)'
};

@Component({
    selector: 'ams-chart-line-2y',
    templateUrl: './ams-chart-line-2y.component.html',
})

export class AmsChartLineTwoYComponent implements OnInit, OnDestroy, OnChanges {

    @Input() chartData: any;
    @Input() chartLabels: string[];
    @Input() title: string;
    @Input() yAxisShow: boolean = false;
    @Input() yGridShow: boolean = false;
    @Input() xAxisShow: boolean = true;
    @Input() xGridShow: boolean = false;
    @Input() legend: boolean = false;
    @Input() options: ChartConfiguration['options'];

    @Output() chartClick: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(BaseChartDirective, { static: false }) chart: BaseChartDirective;
    
    chartOptionsBase:ChartConfiguration['options'] = {
        elements: {
            line: {
                tension: 0.5
            }
        },
        color:'white',
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            x: {
                display: true,
                position:'bottom',
                beginAtZero:false,
                grid: {
                    display: false,
                    color: 'rgba(255, 255, 255, 0.3)',
                    //borderColor: 'rgba(255, 255, 255, 0.3)',
                },
                ticks: {
                    color: 'rgba(255, 255, 255, 0.8)',
                },
            },
            yLeft:{
                position: 'left',
                display: true,
                beginAtZero:false,
                grid: {
                    display: true,
                    color: 'rgba(255, 255, 255, 0.2)',
                    //borderColor: 'rgba(255, 255, 255, 0.3)',
                },
                ticks: {
                    color: 'rgba(255, 255, 255, 0.8)',
                },
                title:{display:true, color: 'rgba(255, 255, 255, 0.8)'},
            },
            yRight:{
                position: 'right',
                display: true,
                beginAtZero:false,
                grid: {
                    display: false,
                    color: 'rgba(255, 255, 255, 0.2)',
                    //borderColor: 'rgba(255, 255, 255, 0.3)',
                },
                ticks: {
                    color: 'rgba(255, 255, 255, 0.8)',
                },
                title:{display:true, color: 'rgba(255, 255, 255, 0.8)'},
            },
        
        },
        plugins: {
            title: {
                display: true,
                color: 'white',
            },
            legend: { display: true },
        }
    };
   

    lineChartColors: any[] = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // yelow grey
            backgroundColor: 'rgba(255, 221, 153,0.6)',
            borderColor: 'rgba(255, 221, 153,1)',
            pointBackgroundColor: 'rgba(255, 221, 153,1)',
            pointBorderColor: '#ffdd99',
            pointHoverBackgroundColor: '#ffdd99',
            pointHoverBorderColor: 'rgba(255, 221, 153,1)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // red
            backgroundColor: 'rgba(255,0,0,0.3)',
            borderColor: 'red',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];

    styleSize = 'height: 100%;width: 100%;';

    constructor(private cus: CurrentUserService) {
    }


    ngOnInit(): void {
        this.initFields();
    }

    ngOnDestroy(): void {
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['chartData']) {
            this.initFields();
        } else if (changes['options']) {
            this.initFields();
        }
        
    }

    initFields():void{
        if(this.chartData && this.chartData.length>0){
            if (this.chartData && this.chartData.length > 0) {
                for (let idx = 0; idx < this.chartData.length; idx++) {
                    let element = Object.assign(new Ng2ChartJsCahartData(), this.chartData[idx]);
                    if (!element.backgroundColor && AmsChartColorsOpt.length > idx) {
                        const color = AmsChartColorsOpt[idx];
                        //console.log('initFieldsd -> color:', color, ' idx:', idx);
                        element.setColorAms(color);
                        //console.log('initFieldsd -> element:', element, ' idx:', idx);
                        this.chartData[idx] = element;
                    }
                }
            }
        }
        
        if (this.title && this.title.length>0) {
            this.options.plugins.title.display = true;
            this.options.plugins.title.text = this.title;
        } else{
            this.options.plugins.title.display = false;
        }
        //console.log('this.chartOptions.scales:',  this.chartOptions.scales);
        this.options.scales.x.display = this.xAxisShow === true ? true : false;
        //this.options.scales.yAxes[0].display = this.yAxisShow === true ? true : false;
        this.options.scales.x.grid.display = this.xGridShow ? this.xGridShow : false;
        //this.chartOptions.scales.yAxes[0].beginAtZero = this.beginAtZero === true ? true : false;
        
    }

    onChartClick(event) {
        console.log('onChartClick() -> event: ', event);
        this.chartClick.emit(event);
    }

}

export const chartOptPaxCargo:ChartConfiguration['options'] = {
    elements: {
        line: {
            tension: 0.5
        }
    },
    color:'white',
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        x: {
            display: true,
            position:'bottom',
            beginAtZero:false,
            grid: {
                display: false,
                color: 'rgba(255, 255, 255, 0.3)',
                //borderColor: 'rgba(255, 255, 255, 0.3)',
            },
            ticks: {
                color: 'rgba(255, 255, 255, 0.8)',
            },
        },
        yLeft:{
            position: 'left',
            display: true,
            beginAtZero:false,
            grid: {
                display: true,
                color: 'rgba(255, 255, 255, 0.2)',
                //borderColor: 'rgba(255, 255, 255, 0.3)',
            },
            ticks: {
                color: 'rgba(255, 255, 255, 0.8)',
            },
            title:{display:true, color: 'rgba(255, 255, 255, 0.8)', text:'PAX',},
        },
        yRight:{
            position: 'right',
            display: true,
            beginAtZero:false,
            grid: {
                display: false,
                color: 'rgba(255, 255, 255, 0.2)',
                //borderColor: 'rgba(255, 255, 255, 0.3)',
            },
            ticks: {
                color: 'rgba(255, 255, 255, 0.8)',
            },
            title:{display:true, color: 'rgba(255, 255, 255, 0.8)', text:'Cargo',},
        },
    
    },
    plugins: {
        title: {
            display: true,
            color: 'white',
        },
        legend: { display: true },
    }
};
