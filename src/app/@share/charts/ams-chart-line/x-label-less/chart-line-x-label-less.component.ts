import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ViewChild, OnChanges, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { ChartConfiguration } from 'chart.js';
// Services
import { BaseChartDirective } from 'ng2-charts';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { Color } from '../../models/color.model';
import { AmsChartColorsOpt, Ng2ChartJsCahartData } from '../dto';

@Component({
    selector: 'ams-chart-line-x-label-less',
    templateUrl: './chart-line-x-label-less.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})

export class AmsChartLineXLabelLessComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

    
    @Input() chartData: any;
    @Input() chartLabels: string[];
    @Input() title: string;
    @Input() legend: boolean = false;
    @Input() environment: string;
    @Input() sizepx: number;
    @Input() yAxisShow: boolean = false;
    @Input() yGridShow: boolean = false;
    @Input() xAxisShow: boolean = true;
    @Input() xGridShow: boolean = false;
    @Input() chartColors: Color[];
    @Input() fontColor: string;
    @Input() beginAtZero: boolean = false;
    @Input() labelSkipNr: number = 4;

    @Output() chartClick: EventEmitter<any> = new EventEmitter<any>();
    
    @ViewChild(BaseChartDirective, { static: false }) chart: BaseChartDirective;
    @ViewChild('parent', { static: false }) parent: any;
    @ViewChild('canvasXl', { static: false }) canvasXl: any;

    chartHeight:number;
    chartOptions: ChartConfiguration['options'] = {
        color: 'white',
        responsive: true,
        /*maintainAspectRatio: false,*/
        plugins: {
            title: {
                display: true,
                color: 'white',
            }
        },
        scales: {
            x: {
              grid: {
                display:true,
                color:'rgba(255, 255, 255, 0.2)',
                //borderColor: 'rgba(255, 255, 255, 0.2)'
              },
              ticks: {
                color: 'rgba(255, 255, 255, 0.8)',
            },
            },
            y: {
                display:true,
                grid: {
                    display:true,
                    color:'rgba(255, 255, 255, 0.2)',
                    //borderColor: 'rgba(255, 255, 255, 0.2)',
                },
                ticks: {
                    color: 'rgba(255, 255, 255, 0.8)',
                },
              }
        }
    };

    styleSize = 'height: 100%;width: 100%;';
    constructor(private cus: CurrentUserService) {
    }
   


    ngOnInit(): void {
        this.initFields();
        /*
        this.chartOptions.title.display = this.title ? true : false;
        this.chartOptions.legend.display = this.legend ? true : false;
        this.chartOptions.title.text = this.title ? this.title : '';

        if (this.environment) {
            this.chartOptions.title.text += ' (' + this.environment + ')';
        }
        if (this.sizepx) {
            this.styleSize = 'height: ' + this.sizepx.toString() + 'px;width: ' + (this.sizepx * 2).toString() + 'px;';
        }
        if (this.fontColor) {
            this.chartOptions.scales.xAxes[0].ticks.fontColor = this.fontColor;
            this.chartOptions.scales.yAxes[0].ticks.fontColor = this.fontColor;
            this.chartOptions.title.fontColor = this.fontColor;
        }
        if (this.yAxisShow) {
            this.chartOptions.scales.yAxes[0].display = this.yAxisShow === true ? true : false;
        }
        if (this.xAxisShow) {
            this.chartOptions.scales.xAxes[0].display = this.xAxisShow === true ? true : false;
        }
        if (this.chartColors) {
            this.lineChartColors = this.chartColors;
        }*/
    }

    
    ngOnDestroy(): void {
    }

    ngAfterViewInit(): void {
        
        console.log('initFieldsd -> parent:', this.parent);
        if(this.parent){
            //this.parent.nativeElement.firstChild.width = this.parent.nativeElement.offsetHeight;
            this.chartHeight = this.parent.nativeElement.offsetHeight;
            //this.chart.height = parent.nativeElement.offsetHeight;
        }
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        if (changes['chartData']) {
            this.initFields();
        }
    }

    initFields(): void {
        console.log('initFieldsd -> chartData:', this.chartData);
        if (this.chartData && this.chartData.length > 0) {
            for (let idx = 0; idx < this.chartData.length; idx++) {
                let element = Object.assign(new Ng2ChartJsCahartData(), this.chartData[idx]);
                if (!element.backgroundColor && AmsChartColorsOpt.length > idx) {
                    const color = AmsChartColorsOpt[idx];
                    //console.log('initFieldsd -> color:', color, ' idx:', idx);
                    element.setColorAms(color);
                    //console.log('initFieldsd -> element:', element, ' idx:', idx);
                    this.chartData[idx] = element;
                }
            }
        }
        //console.log('initFieldsd -> options:', this.chart.options);
       
        if (this.title && this.title.length > 0) {
            this.chartOptions.plugins.title.display = true;
            this.chartOptions.plugins.title.text = this.title;
        } else {
            this.chartOptions.plugins.title.display = false;
        }
         
        this.chartOptions.scales.x.display = this.xAxisShow === true ? true : false;
        this.chartOptions.scales.y.display = this.yAxisShow === true ? true : false;   
        
        this.chartOptions.scales.x.grid.display = this.xGridShow ? this.xGridShow : false;
    }

    onChartClick(event) {
        console.log('onChartClick() -> event: ', event);
        this.chartClick.emit(event);
    }

}
