import { OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges, SimpleChanges, Component } from '@angular/core';
import { ChartConfiguration, ChartType } from 'chart.js';
import { Color } from '../../models/color.model';


@Component({
  selector: 'ams-chart-pie',
  templateUrl: './ams-chart-pie.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class AmsChartPieComponent implements OnInit, OnChanges {

  @Input() chartData: any;
  @Input() chartLabels: string[];
  @Input() title: string;
  @Input() sizepx: number;
  @Input() legend: boolean = true;
  @Input() environment: string;
  @Input() colors: Color[];
  @Output() chartClick: EventEmitter<any> = new EventEmitter<any>();

  //#region Fields
  chartType: ChartType = 'pie';

  chartOptions: ChartConfiguration['options'] = {
    color: 'white',
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
        title: {
            display: true,
            color: 'white',
        },
        legend: {
            display: true,
            labels: {
              color: 'rgb(255, 99, 132)'
            },
            position: 'chartArea',
        },
        /*
        datalabels: {
            formatter: (value, ctx) => {
                if (ctx.chart.data.labels) {
                    return ctx.chart.data.labels[ctx.dataIndex];
                }
            },
        },*/
    },
  };

  chartColors: any[] = [
    {
      backgroundColor: ['rgba(148,159,177,0.2)',
        'rgba(148,159,177,0.5)', 'rgba(255, 221, 153,0.5)',
        'rgba(0, 230, 77,0.5)', 'rgba(0, 0, 255,0.5)', 'rgba(77,83,96,0.5)'],
    },
  ];

  styleSize = 'height: 100px;width: 100px;';

  //#endregion


  constructor() { }


  ngOnInit(): void {
    this.initFields();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['chartData']) {
      //this.chartData = changes['chartData'].currentValue;
      this.initFields();
    }
  }

  initFields() {
    //this.data.labels = this.chartLabels;

    if (this.title && this.title.length > 0) {
      this.chartOptions.plugins.title.display = true;
      this.chartOptions.plugins.title.text = (this.title ? this.title : '') + ' (' + this.environment + ')';
    } else {
      this.chartOptions.plugins.title.display = false;
    }

    if (this.sizepx) {
      this.styleSize = 'height: ' + this.sizepx.toString() + 'px;width: ' + this.sizepx.toString() + 'px;';
    }

    if (this.colors) {
      this.chartColors = this.colors;
    }
  }

  onChartClick(event) {
    console.log('onChartClick() -> event: ', event);
    this.chartClick.emit(event);
  }


}
