import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmsChartPieComponent } from './ams-chart-pie.component';

describe('AmsChartPieComponent', () => {
  let component: AmsChartPieComponent;
  let fixture: ComponentFixture<AmsChartPieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsChartPieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsChartPieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
