import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import { COMMON_IMG_LOGO_RED } from "src/app/@core/const/app-storage.const";
import { UserModel } from "src/app/@core/services/auth/api/dto";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { AmsUserNameEditDialog } from "src/app/@share/components/dialogs/user-edit/user-name-edit.dialog";
import { environment } from "src/environments/environment";
import { AmsAlFleetService } from "../al-fleet.service";

@Component({
    selector: 'ams-al-fleet-menu-main',
    templateUrl: './al-fleet-menu-main.component.html',
    // animations: [PageTransition]
})
export class AmsAlFleetMenuMainComponent implements OnInit, OnDestroy {

    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    roots = AppRoutes;
    user: UserModel;
    userName: string;

    tabIdx: number;
    tabIdxChanged: Subscription;

    constructor(private router: Router,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private fleetService: AmsAlFleetService) { }

    ngOnInit(): void {

        this.tabIdxChanged = this.fleetService.tabIdxChanged.subscribe(value => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.tabIdx = this.fleetService.tabIdx;
    }


    edituser(data: UserModel) {

        const dialogRef = this.dialogService.open(AmsUserNameEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                user: data,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('edituser-> res:', res);
            if (res) {

            }
            this.initFields();
        });

    }
}
