import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AppStore } from '../@core/const/app-storage.const';
import { LocalUserSettings } from '../@core/models/app/local-user-settings';
import { AmsAircraftClient } from '../@core/services/api/aircraft/api-client';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMac, AmsManufacturer, ResponseAmsAircraftTable, ResponseAmsMacTableData } from '../@core/services/api/aircraft/dto';
import { AmsAirlineClient } from '../@core/services/api/airline/api-client';
import { AmsAirline } from '../@core/services/api/airline/dto';
import { AmsAirport } from '../@core/services/api/airport/dto';
import { UserModel } from '../@core/services/auth/api/dto';
// -Services
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { AmsAdminService } from '../admin/admin.service';


export const SR_LEFT_PANEL_IN_KEY = 'ams_al_fleet_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_al_fleet_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class AmsAlFleetService {

    get user(): UserModel {
        return this.cus.user;
    }

    get airline(): AmsAirline {
        return this.cus.airline;
    }

    set airline(value: AmsAirline) {
        this.cus.airline = value;
    }

    get hqAirport(): AmsAirport {
        return this.cus.hqAirport;
    }

    set hqAirport(value: AmsAirport) {
        this.cus.hqAirport = value;
    }

    get settings(): LocalUserSettings {
       
        return this.cus.localSettings;
    }

    set settings(value: LocalUserSettings) {
        this.cus.localSettings = value;
    }

    get manufacturers(): AmsManufacturer[] {
        return this.adminService.manufacturers;
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private acClient: AmsAircraftClient,
        private adminService: AmsAdminService) {
    }

    //#region tabIdx

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Data
    acMarketAllResp: ResponseAmsMacTableData;

    public acMarketTableAll(): Observable<ResponseAmsMacTableData> {

        const criteria = new AmsAircraftTableCriteria()
        criteria.limit = 100;
        criteria.offset = 0;
        return new Observable<ResponseAmsMacTableData>(subscriber => {

            this.acClient.acMarketTable(criteria)
                .subscribe((resp: ResponseAmsMacTableData) => {
                    //console.log('acMarketTableAll -> resp', resp);
                    this.acMarketAllResp = resp;
                    subscriber.next(resp);
                },
                    err => {
                        throw err;
                    });
        });

    }

    public loadAirline(alId: number): Observable<AmsAirline> {

        return new Observable<AmsAirline>(subscriber => {

            this.alClient.airlineGet(alId)
                .subscribe((resp: AmsAirline) => {
                    subscriber.next(resp);
                },
                    err => {

                        throw err;
                    });
        });

    }

    //#endregion


}
