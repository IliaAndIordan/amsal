import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { AuthGuard } from '../@core/guards/auth.guard';
import { AmsAlFleetComponent } from './al-fleet.component';
import { AmsAlFleetHomeComponent } from './home.component';
import { AmsAlFleetTabsComponent } from './tabs/al-fleet-tabs.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: AmsAlFleetComponent,
    children: [
      { path: AppRoutes.Root, component: AmsAlFleetHomeComponent,
        
        children: [
          
          { path: AppRoutes.Root,   pathMatch: 'full', component: AmsAlFleetTabsComponent, canActivate: [AuthGuard]},
          /*
          { path: AppRoutes.aircafts,  component: AmsAcmAircraftListComponent },
          
          { path: AppRoutes.profile, component: UserProfileComponent },
          { path: AppRoutes.create, component: AirlineCreateFormComponent },
          { path: AppRoutes.info, component: AirlineInfoComponent,  
            children: [
            { path: AppRoutes.Root,   pathMatch: 'full', component: AirlineInfoDashboardComponent },
            // { path: AppRoutes.Dashboard, component: DashboardComponent },
          ]},
          // { path: AppRoutes.Dashboard, component: DashboardComponent },
          */
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AmsAlFleetRoutingModule { }

export const routedComponents = [
  AmsAlFleetComponent, 
  AmsAlFleetHomeComponent,
  AmsAlFleetTabsComponent,
];
