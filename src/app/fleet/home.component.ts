import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/const/animation.const';
// --- Models
import { AppRoutes } from '../@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from '../@core/const/app-storage.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from '../@core/const/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AmsSidePanelModalComponent } from '../@share/components/common/side-panel-modal/side-panel-modal';
import { AmsAirport } from '../@core/services/api/airport/dto';
import { AmsCountry, AmsState } from '../@core/services/api/country/dto';
import { IInfoMessage, InfoMessageDialog } from '../@share/components/dialogs/info-message/info-message.dialog';
import { AmsAirline } from '../@core/services/api/airline/dto';
import { UserModel } from '../@core/services/auth/api/dto';
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { SpinnerService } from '../@core/services/spinner.service';
import { AmsAircraft, AmsMac, IAmsAircraft, IAmsMac, ResponseAmsAircraftTable, ResponseAmsMacTableData } from '../@core/services/api/aircraft/dto';
import { SxSidebarNavComponent } from '../@share/components/common/sidebar/nav/sx-sidebar-nav.component';
import { MatDrawerMode } from '@angular/material/sidenav';
import { AmsAlFleetService } from './al-fleet.service';
// import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AmsAlFleetHomeComponent implements OnInit, OnDestroy {

  @ViewChild('sxSide') sxSide: SxSidebarNavComponent;

  snavmode: MatDrawerMode = 'side';
  in = Animate.in;

  hqAirport: AmsAirport;

  user: UserModel;
  userChanged: Subscription;

  airline: AmsAirline;
  airlineChanged: Subscription;

  sxLogo = COMMON_IMG_LOGO_RED;
  // --- Side Tab
  expandTabVar: string = Animate.in;
  showTabContentsVar: string = Animate.hide;
  total: number = 0;
  aggressiveTotal: number = 0;

  panelInVar = Animate.in;
  panelInChanged: Subscription;
  sidebarClass = 'sidebar-closed';

  acMarketAllResp: ResponseAmsMacTableData;
  acMacs: IAmsMac[];

  tabIdx: number;
  tabIdxChanged: Subscription;

  // public dialogService: MatDialog
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    public dialog: MatDialog,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private fleetService: AmsAlFleetService) { }


  ngOnInit(): void {

    this.userChanged = this.cus.userChanged.subscribe((user: UserModel) => {
      this.initFields();
    });

    this.airlineChanged = this.cus.airlineChanged.subscribe((airline: AmsAirline) => {
      this.initFields();
    });

    this.tabIdxChanged = this.fleetService.tabIdxChanged.subscribe(value => {
      this.initFields();
    });
    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.airlineChanged) { this.airlineChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields() {
    this.user = this.cus.user;
    this.airline = this.cus.airline;
    this.hqAirport = this.cus.hqAirport;
    this.tabIdx = this.fleetService.tabIdx;
  }


  //#region Data


  public loadAcMacs(): void {
    /*
    this.spinerService.show();
    const acMarket$ = this.acmService.acMarketTableAll();

    //this.acmService.acMarketTableAll()
    acMarket$.subscribe((resp: ResponseAmsMacTableData) => {
      this.spinerService.hide();
      this.initFields();
    },
      err => {
        this.spinerService.hide();
        console.log('loadRegions-> err: ', err);
      });
    */
  }

  //#endregion

  //#region  Expand/Close Menu


  snavToggleClick() {
    console.log('snavToggleClick-> sxSide', this.sxSide);
    if (this.sxSide) {
      this.sxSide.toggle();
    }
  }

  //#endregion

  //#region SPM

  showMessage(data: IInfoMessage) {
    console.log('showMessage-> data:', data);

    const dialogRef = this.dialog.open(InfoMessageDialog, {
      width: '721px',
      height: '320px',
      data: data
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log('showMessage-> res:', res);
      if (res) {

      }
    });

  }


  spmAirportOpen(airport: AmsAirport) {
    if (airport) {
      this.cus.spmAirportPanelOpen.next(airport.apId);
    }
  }

  spmAirlineOpen(al: AmsAirline) {
    if (al) {
      //this.showMessage({ title: 'Not Implemented', message: 'SPM component not implemented' });
      this.cus.spmAirlinePanelOpen.next(al.alId);
    }
  }

  spmAcmMacOpen(mac: AmsMac) {
    if (mac) {
      //this.showMessage({ title: 'Not Implemented', message: 'SPM component not implemented' });
      this.cus.spmMacPanelOpen.next(mac);
    }
  }

  spmAircraftOpen(value: AmsAircraft) {

    if (value) {
      this.cus.spmAircraftPanelOpen.next(value.acId);
    }
  }

  //#endregion



}
