import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { AmsAlFleetRoutingModule, routedComponents } from './al-fleet-routing.module';
import { AmsAlFleetMenuMainComponent } from './menu-main/al-fleet-menu-main.component';
import { AmsGridAcMarketAircraftsComponent } from './tabs/al-fleet-tab-macs.component';


@NgModule({
    imports: [
        CommonModule,

        SxShareModule,
        SxMaterialModule,
        SxPipesModule,
        //
        AmsAlFleetRoutingModule,
    ],
    declarations: [
        routedComponents,
        AmsAlFleetMenuMainComponent,
        AmsGridAcMarketAircraftsComponent,
    ],
    exports: [],
})
export class AmsAlFleetModule { }
