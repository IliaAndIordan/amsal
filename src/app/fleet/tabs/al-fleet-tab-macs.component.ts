import { AfterViewInit, Component, Input, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription, tap } from "rxjs";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import { AmsAircraftClient } from "src/app/@core/services/api/aircraft/api-client";
import { AmsAircraft, AmsManufacturer, AmsMac, AmsAircraftTableCriteria, AmsMacCabin } from "src/app/@core/services/api/aircraft/dto";
import { AmsAirline } from "src/app/@core/services/api/airline/dto";
import { AmsAirport } from "src/app/@core/services/api/airport/dto";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { SpinnerService } from "src/app/@core/services/spinner.service";
import { AmsMacCabinEditDialog } from "src/app/@share/components/dialogs/mac-cabin-edit/mac-cabin-edit.dialog";
import { AcMarketService } from "src/app/ac-market/ac-market.service";
import { AmsAlFleetTabMacsDataSource } from "./al-fleet-tab-macs.datasource";
import { AmsAlFleetService } from "../al-fleet.service";

@Component({
    selector: 'ams-al-fleet-tab-macs',
    templateUrl: './al-fleet-tab-macs.component.html',
})
export class AmsGridAcMarketAircraftsComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input() tabIdx:number;
    
    selected: AmsMac;
    manufacturer: AmsManufacturer;

    criteria: number;
    criteriaChanged: Subscription;

    dataCount = 0;
    dataChanged: Subscription;

    airline: AmsAirline;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private acmService: AcMarketService,
        private client: AmsAircraftClient,
        private fleetService:AmsAlFleetService,
        public tableds: AmsAlFleetTabMacsDataSource,) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((data: Array<AmsMac>) => {
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });

        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: number) => {
            this.initFields();
            if(this.tabIdx = this.fleetService.tabIdx){
                this.loadPage();
            }
        });

        this.initFields();
        this.criteria = this.cus.airline.alId;
        this.tableds.criteria = this.criteria;
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
    }

    ngAfterViewInit() {
    }

    //#region Data

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    buyAircraft(aicraft: AmsAircraft): void {
        if (aicraft && this.cus.airline) {
            this.preloader.show();
            this.client.acBuy(aicraft.acId, this.cus.airline.alId)
                .subscribe(res => {
                    this.preloader.hide();
                    console.log('buyAircraft -> res:', res);
                    this.refreshClick();
                }, msg => {
                    console.log('loadData -> ' + msg);
                    this.preloader.hide();
                    this.refreshClick();
                });
        }
    }

    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }

    spmAircraftOpen(value: AmsAircraft) {
        if (value) {
            this.acmService.spmAircraftPanelOpen.next(value);
        }
    }


    ediMacCabin(data: AmsMacCabin) {
        console.log('ediMacCabin-> data:', data);
        //console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            const dialogRef = this.dialogService.open(AmsMacCabinEditDialog, {
                width: '721px',
                height: '620px',
                data: {
                    cabin: data,
                    mac: this.acmService.acMarketMac
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediMac-> res:', res);
                if (res) {
                    //this.table.renderRows();
                    this.tableds.loadData()
                        .subscribe(res => {
                            this.initFields();
                        });
                }
                else {
                    this.initFields();
                }


            });

        }
        else {
            this.toastr.info('Please select manufacturer acrcraft model first.', 'Edit Cabin Configuration');
        }


    }

    addCabin() {
        // console.log('inviteUser-> ');
        let dto = new AmsMacCabin();
        dto.macId = this.acmService.acMarketMac.macId;
        this.ediMacCabin(dto);
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);

        if (data) {
            this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    //#endregion
}
