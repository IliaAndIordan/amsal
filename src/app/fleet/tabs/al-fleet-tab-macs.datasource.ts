import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsMac } from 'src/app/@core/services/api/aircraft/dto';

export const KEY_CRITERIA = 'ams_al_fleet_list_criteria';
@Injectable({
    providedIn: 'root',
})
export class AmsAlFleetTabMacsDataSource extends DataSource<AmsMac> {

    seleted: AmsMac;

    private _data: Array<AmsMac>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsMac[]> = new BehaviorSubject<AmsMac[]>([]);
    listSubject = new BehaviorSubject<AmsMac[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();
    data$ = this.listSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsMac> {
        return this._data;
    }

    set data(value: Array<AmsMac>) {
        this._data = value;
        this.listSubject.next(this._data as AmsMac[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsAircraftClient) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<number>();

    public get criteria(): number {
        const objStr = localStorage.getItem(KEY_CRITERIA);
        let obj: number;
        if (objStr) {
            obj = parseInt(objStr);
        } else {
            obj = this.cus.airline.alId;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: number) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsMac[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsMac>> {
       
        this.isLoading = true;
        
        return new Observable<Array<AmsMac>>(subscriber => {

            this.client.macListByAlIdP(this.criteria)
                .then((resp: AmsMac[]) => {
                    this.data = new Array<AmsMac>();
                    if (resp && resp.length>0) {
                        this.data = resp;
                        this.itemsCount = this.data?.length??0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);
                    this.itemsCount = 0;
                    this.data = new Array<AmsMac>();
                    this.itemsCount = this.data?.length??0;
                    this.isLoading = false;
                    // component.errorMessage = msg;
                });

        });

    }

    //#endregion
}
