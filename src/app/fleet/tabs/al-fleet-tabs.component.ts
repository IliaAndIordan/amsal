import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AirlineService } from 'src/app/airline/airline.service';
import { AmsAlFleetService } from '../al-fleet.service';


@Component({
    selector: 'ams-al-fleet-tabs',
    templateUrl: './al-fleet-tabs.component.html',
})
export class AmsAlFleetTabsComponent implements OnInit, OnDestroy {
   
    mac:AmsMac;
    
    selTabIdx: number;
    tabIdxChanged: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private alService: AirlineService,
        private fleetService: AmsAlFleetService) {

    }


    ngOnInit(): void {

        this.tabIdxChanged = this.fleetService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.fleetService.tabIdx;
    }

    //#region Mobile dialog methods

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> usecase:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.fleetService.tabIdx = this.selTabIdx;
    }


    //#endregion

    //#region SPM Methods

    spmMacOpenClick(value:AmsMac) {
        this.mac = value;
        if (this.mac) {
            this.cus.spmMacPanelOpen.next(this.mac);
        }
    }

    //#endregion

}
