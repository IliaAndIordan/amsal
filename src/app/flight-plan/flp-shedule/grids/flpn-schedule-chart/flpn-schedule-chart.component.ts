import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, ViewChild, signal, type OnInit } from '@angular/core';
import { CalendarOptions, EventApi, EventClickArg, EventInput } from '@fullcalendar/core';
import { Observable, Subject, Subscription, firstValueFrom, of } from 'rxjs';
import { AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleTableCriteria, ResponceAmsAlFlightNumberScheduleTableData } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsWeekday } from 'src/app/@core/services/api/airline/enums';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { MapLeafletAlSheduleGroupService } from 'src/app/@share/maps/map-liflet-al-shedule-group.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { AcType, AcTypeOpt, AmsAcType } from 'src/app/@core/models/pipes/ac-type.pipe';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { MatDialog } from '@angular/material/dialog';
import { AmsAlFlightNumberScheduleEditDialog } from 'src/app/@share/components/dialogs/al-flpn-schedule-edit/al-flpn-schedule-edit.dialog';
import { FlpSheduleService } from '../../flp-shedule.service';
import { WeekDay } from '@angular/common';

const KEY_CRITERIA = 'ams-flpn-schedule-chart-criteria';
const KEY_SELECTED = 'ams-flpns-selected';
@Component({
  selector: 'ams-flpn-schedule-chart',
  templateUrl: './flpn-schedule-chart.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class AmsAlFlpnScheduleChartComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() tabindex: number;

  @ViewChild('calendar') calendar: FullCalendarComponent;

  calendarOptions: CalendarOptions = {
    initialView: 'timeGridWeek',
    headerToolbar: {
      start: '',
      center: 'title',
      right: 'btnAddShedule'
    },
    views: {
      timeGridWeek: {
        titleFormat: { year: 'numeric', month: 'long' }
        // other view-specific options here
      }
    },
    plugins: [timeGridPlugin, interactionPlugin],
    dayHeaders: true,
    allDaySlot: false,
    slotDuration: { minutes: 15 },
    businessHours: {
      daysOfWeek: [0, 1, 2, 3, 4, 5, 6],
      startTime: '06:00',
      endTime: '23:00',
    },
    dayHeaderFormat: { weekday: 'short' },
    //eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    dayCellClassNames: this.dayCellClassNames.bind(this),
    eventClassNames: this.eventClassNames.bind(this),
    customButtons: {
      btnAddShedule: {
        text: 'Add Schedule',
        hint: 'Add a new scheduled flight for selected weekday after selected flight.',
        click: this.addSchedule.bind(this)
      }
    },
  };

  calEvents = signal<EventApi[]>([]);
  events$: Promise<EventInput[]>;
  data: AmsAlFlightNumberSchedule[];
  dataCount = 0;

  group: AmsSheduleGroup;
  groupChanged: Subscription;
  acType: AmsAcType;

  weekday: AmsWeekday;
  weekdayChanged: Subscription;

  aircraft$: Observable<AmsAircraft>;
  aircraft: AmsAircraft;
  opTimeMin: number = 5;

  criteriaChanged = new Subject<AmsAlFlightNumberScheduleTableCriteria>();

  tabIdxChanged: Subscription;

  get selFlpns(): AmsAlFlightNumberSchedule {
    let rv: AmsAlFlightNumberSchedule;
    const valStr = localStorage.getItem(KEY_SELECTED);
    rv = valStr ? AmsAlFlightNumberSchedule.fromJSON(JSON.parse(valStr)) : undefined;
    return rv;
  }

  set selFlpns(value: AmsAlFlightNumberSchedule) {
    if (value) { localStorage.setItem(KEY_SELECTED, JSON.stringify(value)); }
    else { localStorage.removeItem(KEY_SELECTED); }

  }

  get criteria(): AmsAlFlightNumberScheduleTableCriteria {
    const onjStr = localStorage.getItem(KEY_CRITERIA);
    let obj: AmsAlFlightNumberScheduleTableCriteria;
    if (onjStr) {
      obj = Object.assign(new AmsAlFlightNumberScheduleTableCriteria(), JSON.parse(onjStr));

    } else {
      obj = new AmsAlFlightNumberScheduleTableCriteria();
      obj.limit = 1000;
      obj.offset = 0;
      obj.sortCol = undefined;
      obj.sortDesc = false;
      obj.alId = this.cus.airline.alId;
      obj.grpId = this.group?.grpId ?? undefined;
      localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
    }
    return obj;
  }

  set criteria(obj: AmsAlFlightNumberScheduleTableCriteria) {
    if (obj) {
      localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
    } else {
      localStorage.removeItem(KEY_CRITERIA);
    }
    this.criteriaChanged.next(obj);
  }

  constructor(
    private changeDetector: ChangeDetectorRef,
    public dialogService: MatDialog,
    private preloader: SpinnerService,
    private cus: CurrentUserService,
    private mapService: MapLeafletAlSheduleGroupService,
    private acCache: AmsAirlineAircraftCacheService,
    private alClient: AmsAirlineClient,
    private flpShService: FlpSheduleService,) {
  }

  ngOnInit(): void {
    this.groupChanged = this.mapService.groupChanged.subscribe((group: AmsSheduleGroup) => {
      this.initFields();
      if (this.tabindex === this.flpShService.tabIdx) {
        this.loadData();
      }
    });

    this.weekdayChanged = this.mapService.weekdayChanged.subscribe((weekday: AmsWeekday) => {
      this.initFields();
      if (this.tabindex === this.flpShService.tabIdx) {
        this.loadData();
      }
    });

    this.tabIdxChanged = this.flpShService.tabIdxChanged.subscribe(tabIdx => {
      this.initFields();
      if (this.tabindex === this.flpShService.tabIdx) {
        this.loadData();
      }
    });
    this.initFields();
    this.loadData();

  }

  ngAfterViewInit(): void {

  }

  ngOnDestroy(): void {
    if (this.groupChanged) { this.groupChanged.unsubscribe(); }
    if (this.weekdayChanged) { this.weekdayChanged.unsubscribe(); }
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
  }

  initFields(): void {
    this.group = this.mapService.group;
    const acTypeId = this.group?.acTypeId ?? AcType.Piston;
    this.acType = AcTypeOpt.find(x => x.acTypeId === acTypeId);
    let criteria = this.criteria;
    criteria.grpId = this.group ? this.group.grpId : undefined;
    criteria.alId = this.cus.airline.alId;
    this.criteria = criteria;
    this.weekday = this.mapService.weekday;
    if (this.group?.acId) {
      this.aircraft$ = this.acCache.getAircraft(this.group?.acId);
      this.aircraft$.subscribe(ac => {
        this.aircraft = ac;
        this.opTimeMin = this.aircraft.opTimeMin;
      });
    }
    if (this.weekday && this.calendar) {
      const date = this.selDate;

    }
  }

  //#region Calendar Events

  get selDate(): Date {
    const date = new Date();
    const currentDay = date.getDay();
    const distance = (this.weekday.id - 1) - currentDay;
    return new Date(new Date(date).setDate(date.getDate() + distance));
  }

  handleEventClick(clickInfo: EventClickArg) {
    if (clickInfo && clickInfo?.event && clickInfo?.event?.id) {
      // Flight schedule event
      if (clickInfo?.event.display === 'block') {
        const flpnsId = parseInt(clickInfo.event.id);
        const flpnsStr = (clickInfo.event.extendedProps)?.flight
        const flpns = flpnsStr ? AmsAlFlightNumberSchedule.fromJSON(flpnsStr) : undefined;
        if (flpns && flpns.flpnsId) {
          this.selFlpns = flpns;
          this.editAlFlightNumberScheduleClick(flpns);
        }
      }
    }
  }

  addSchedule(): void {
    console.log('addSchedule -> ');
    if (this.group && this.group.grpId) {
      console.log('addSchedule -> group:', this.group);
      let flpns = new AmsAlFlightNumberSchedule();
      flpns.alId = this.cus.airline.alId;
      flpns.grpId = this.group.grpId;
      let last = this.selFlpns;
      const acType = AcTypeOpt.find(x => x.acTypeId === this.group.acTypeId);
      console.log('addSchedule -> last:', last);
      if (last && last.flpnId) {
        const dep = last.nextFlightDepDate(this.opTimeMin);
        console.log('addAlFlightNumberScheduleClick -> dep:', dep);
        flpns.wdId = last.wdId;
        flpns.dtimeH = dep.getHours();
        flpns.dtimeMin = dep.getMinutes();
        flpns.depApId = last.arrApId;

      } else {
        flpns.wdId = this.weekday?.id ?? WeekDay.Monday;
        flpns.dtimeH = 6;
        flpns.dtimeMin = 30;
      }

      this.editAlFlightNumberScheduleClick(flpns);
    }
  }

  deleteSchedule(value: AmsAlFlightNumberSchedule) {
    this.deleteAlFlightNumberScheduleClick(value);
  }

  editSchedule(value: AmsAlFlightNumberSchedule) {
    this.selFlpns = value;
    if (value && value.flpnsId && value.alId === this.cus.airline.alId) {
      this.selFlpns = value;
      this.editAlFlightNumberScheduleClick(value);
    }
  }

  handleEvents(events: EventApi[]) {
    this.calEvents.set(events);
    this.changeDetector.detectChanges(); // workaround for pressionChangedAfterItHasBeenCheckedError
  }

  dayCellClassNames(arg: any) {
    const date = arg.date;
    const dow = arg.dow;
    let rv = ['normal'];
    if (this.weekday) {
      if ((this.weekday.id - 1) === dow) {
        rv = ['wdid-day-range'];
      }
    }

    return rv;
  }

  eventClassNames(arg: any) {
    let rv = ['normal'];
    const flpnsStr = (arg.event.extendedProps)?.flight;
    const id = arg.event.id;
    if (flpnsStr) {
      const flpns = flpnsStr ? AmsAlFlightNumberSchedule.fromJSON(flpnsStr) : undefined;
      if (this.selFlpns && this.selFlpns.flpnsId === flpns?.flpnsId) {
        rv = ['event-selected'];
      }
    }

    return rv;
  }

  //#endregion

  //#region Data

  editAlFlightNumberScheduleClick(value: AmsAlFlightNumberSchedule): void {

    if (this.group && this.group.isActive) {
      const msg = `<p class="w-100 text-j">The flight schedule <b>${value.flpnsName}</b> can not be edited because the group <b>${this.group.grpName}</b> is <b class="green">active</b><br/><br/>Please, deactive the group before editing flight schedules.</p>`;
      this.cus.showMessage({ title: 'Not Editable', message: msg }).then(res => {
        return;
      });
    } else {
      if (value && value.alId === this.cus.airline.alId) {
        const dialogRef = this.dialogService.open(AmsAlFlightNumberScheduleEditDialog, {
          autoFocus: false,
          width: '971px',
          //height: '600px',
          data: {
            flnschedule: value,
            group: this.group,
          }
        });

        dialogRef.afterClosed().subscribe((res: AmsAlFlightNumberSchedule) => {
          if (res) {
            if (res.flpnsId) { this.selFlpns = res; }
            this.loadData();
          }
          this.initFields();
          this.calendar.getApi().render();
        });
      }
    }

  }

  deleteAlFlightNumberScheduleClick(value: AmsAlFlightNumberSchedule): void {
    if (value && value.alId === this.cus.airline.alId) {
      const msg = `<p class="w-100 text-j">Are you sure you want to delete flight schedule <b>${value.flpnsName}</b>. <br/>Once the delete action is taken it cannot be undone.</p>`;
      this.cus.showMessage({ title: 'Delete Flight Schedule', message: msg }).then(confirm => {
        if (confirm) {
          this.preloader.show();
          this.alClient.alFlightNumberScheduleDelete(value.flpnsId).then(res => {
            this.preloader.hide();
            this.loadData();
            this.initFields();
            this.calendar.getApi().render();
          }).catch(err => {
            this.preloader.hide();
            console.log('deleteMacCabin -> err:', err);
          });
        }
      });


    }
  }

  loadData(): void {
    this.preloader.show();
    
    this.events$ = this.alClient.alFlightNumberScheduleTableP(this.criteria)
      .then((resp: ResponceAmsAlFlightNumberScheduleTableData) => {
        this.preloader.hide();
        this.data = new Array<AmsAlFlightNumberSchedule>();
        if (resp) {
          if (resp && resp.flnschedules && resp.flnschedules.length > 0) {
            this.data = resp.flnschedules;
          }
          this.dataCount = resp.rowsCount ? resp.rowsCount : 0;
          //console.log('loadData -> data:', this.data);

        } else {
          this.dataCount = this.data ? this.data.length : 0;
        }
        let events = new Array<EventInput>();
        if (this.data && this.data.length > 0) {
          this.data.forEach(flpns => { 
            let toEventBkg = flpns.toEventBkg(this.opTimeMin);
            let toEvent = flpns.toEvent();
            events.push(toEventBkg);
            events.push(toEvent);
          });

        }
        //console.log('loadData -> events:', events);
        return firstValueFrom(of(events));

      }, msg => {
        this.preloader.hide();
        console.log('loadData -> msg:', msg);

        this.dataCount = 0;
        this.data = new Array<AmsAlFlightNumberSchedule>();
        let events = new Array<EventInput>();
        return firstValueFrom(of(events));
      });

  }

  //#endregion
}
