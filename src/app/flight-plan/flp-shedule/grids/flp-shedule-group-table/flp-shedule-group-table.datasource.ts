import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightTableCriteria, AmsFlightTableData } from 'src/app/@core/services/api/flight/dto';
import { AmsPaxPayloadDemand, AmsPaxPayloadDemandTableCriteria, ResponseAmsPaxPayloadDemandTableData } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { AmsHub, AmsHubTableCriteria, ResponceAmsHubTableData } from 'src/app/@core/services/api/airline/al-hub';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsSheduleGroup, AmsSheduleGroupTableCriteria, ResponceAmsSheduleGroupTableData } from 'src/app/@core/services/api/airline/al-flp-shedule';

export const KEY_CRITERIA = 'ams_al_flp_group_table_criteria-v01';

@Injectable({ providedIn: 'root' })
export class AmsFlpSheduleGroupTableDataSource extends DataSource<AmsSheduleGroup> {

    seleted: AmsSheduleGroup;

    private _data: Array<AmsSheduleGroup>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange = new Subject<AmsSheduleGroup[]>();
    listSubject = new BehaviorSubject<AmsSheduleGroup[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsSheduleGroup> {
        return this._data;
    }

    set data(value: Array<AmsSheduleGroup>) {
        this._data = value;
        this.listSubject.next(this._data as AmsSheduleGroup[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsSheduleGroupTableCriteria>();

    public get criteria(): AmsSheduleGroupTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsSheduleGroupTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsSheduleGroupTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsSheduleGroupTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'grpName';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsSheduleGroupTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsSheduleGroup[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsSheduleGroup>> {

        this.isLoading = true;

        return new Observable<Array<AmsSheduleGroup>>(subscriber => {

            this.alClient.sheduleGroupsTableP(this.criteria)
                .then((resp: ResponceAmsSheduleGroupTableData) => {
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsSheduleGroup>();
                    if (resp) {
                        if (resp && resp.groups && resp.groups.length > 0) {
                            this.data = resp.groups;
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsSheduleGroup>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
