import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport, AmsPaxPayloadDemandTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsFlpSheduleGroupTableDataSource } from './flp-shedule-group-table.datasource';
import { AmsSheduleGroup, AmsSheduleGroupTableCriteria } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsSheduleGroupEditDialog } from 'src/app/@share/components/dialogs/al-flp-group-edit/al-flp-group-edit.dialog';
import { MapLeafletAlSheduleGroupService } from 'src/app/@share/maps/map-liflet-al-shedule-group.service';

@Component({
    selector: 'ams-flp-shedule-group-table',
    templateUrl: './flp-shedule-group-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class AmsFlpSheduleGroupTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsSheduleGroup>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() tabindex: number;
    @Output() spmSheduleGroupOpen: EventEmitter<AmsSheduleGroup> = new EventEmitter<AmsSheduleGroup>();

    selGrp: AmsSheduleGroup;

    criteria: AmsSheduleGroupTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;

    //, 'hubName'
    displayedColumns = ['grpId', 'isActive', 'grpName', 'acId', 'apId', 'acIdPrototype', 'minRunwayM', 'maxRangeKm', 'cruiseSpeedKmph', 'fuelConsumptionLp100km', 'alId'];
    airline: AmsAirline;
    filter:string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private alClient: AmsAirlineClient,
        private hubCache: AmsAirlineHubsCacheService,
        private apCache: AmsAirportCacheService,
        private spmMapService: MapLeafletSpmService,
        private mapService: MapLeafletAlSheduleGroupService,
        public tableds: AmsFlpSheduleGroupTableDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsSheduleGroup>) => {
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsPaxPayloadDemandTableCriteria) => {
            this.criteria = this.tableds.criteria;
            this.loadPage();
        });

        this.initFields();
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        this.criteria.alId = this.airline?.alId;
        //console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.criteria.sortCol = 'adate';
        this.criteria.sortDesc = true;
        this.tableds.criteria = this.criteria;
    }

    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick(): void {
        this.loadPage();
    }

  
    addGroupClick(): void {
        //console.log('addGrouplick -> ');
        let grp = new AmsSheduleGroup();
        grp.alId = this.cus.airline.alId;
        grp.grpName = 'Default';
        this.editGroupClick(grp);
    }

    editGroupClick(value: AmsSheduleGroup): void {
        //console.log('editGroupClick -> value:', value);
        this.selGrp = value;
        if (value && value.alId === this.cus.airline.alId) {
           
            const dialogRef = this.dialogService.open(AmsSheduleGroupEditDialog, {
                width: '721px',
                //height: '600px',
                data: {
                    group: this.selGrp,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.selGrp = res;
                    if(this.selGrp.grpId === this.mapService.group?.grpId){
                        this.mapService.group = this.selGrp;
                    }
                    this.refreshClick();
                }
                this.initFields();
            });
        }
        else {
            this.toastr.info('Please select airport first.', 'Select City');
        }
    }

    deleteGroupClick(value: AmsSheduleGroup): void {
        //console.log('deleteGroupClick -> value:', value);
        this.selGrp = value;
        if (value && value.grpId && value.alId === this.cus.airline.alId) {
            this.preloader.show();
            this.alClient.sheduleGroupDelete(value.grpId).then(res => {
                this.preloader.hide();
                this.hubCache.clearCache();
                this.refreshClick();
            }).catch(err => {
                this.preloader.hide();
                console.log('deleteMacCabin -> err:', err);
            });
        }
    }

    spmHubOpenClick(value: AmsSheduleGroup) {
        console.log('spmHubOpenClick -> value:', value);
        this.selGrp = value;
        if (value && value.apId) {
            this.cus.spmAirportPanelOpen.next(value.apId);
        }
    }

    //#endregion

}
