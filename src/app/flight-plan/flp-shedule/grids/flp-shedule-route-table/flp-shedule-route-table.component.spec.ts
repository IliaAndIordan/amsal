import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmsFlpSheduleRouteTableComponent } from './flp-shedule-route-table.component';


describe('AmsFlpSheduleRouteTableComponent', () => {
  let component: AmsFlpSheduleRouteTableComponent;
  let fixture: ComponentFixture<AmsFlpSheduleRouteTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsFlpSheduleRouteTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsFlpSheduleRouteTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
