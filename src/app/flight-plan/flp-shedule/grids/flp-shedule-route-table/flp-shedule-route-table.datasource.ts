import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAlSheduleRoute, AmsSheduleGroup, AmsSheduleGroupTableCriteria, AmsSheduleRouteTableCriteria, ResponceAmsSheduleGroupTableData, ResponceAmsSheduleRouteTableData } from 'src/app/@core/services/api/airline/al-flp-shedule';

export const KEY_CRITERIA = 'ams_al_flp_route_table_criteria-v01';

@Injectable({ providedIn: 'root' })
export class AmsFlpSheduleRouteTableDataSource extends DataSource<AmsAlSheduleRoute> {

    seleted: AmsAlSheduleRoute;

    private _data: Array<AmsAlSheduleRoute>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsAlSheduleRoute[]> = new BehaviorSubject<AmsAlSheduleRoute[]>([]);
    listSubject = new BehaviorSubject<AmsAlSheduleRoute[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsAlSheduleRoute> {
        return this._data;
    }

    set data(value: Array<AmsAlSheduleRoute>) {
        this._data = value;
        this.listSubject.next(this._data as AmsAlSheduleRoute[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsSheduleRouteTableCriteria>();

    public get criteria(): AmsSheduleRouteTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsSheduleRouteTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsSheduleRouteTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsSheduleRouteTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'routeNr';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsSheduleRouteTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsAlSheduleRoute[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsAlSheduleRoute>> {

        this.isLoading = true;

        return new Observable<Array<AmsAlSheduleRoute>>(subscriber => {

            this.alClient.sheduleRouteTable(this.criteria)
                .subscribe((resp: ResponceAmsSheduleRouteTableData) => {
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsAlSheduleRoute>();
                    if (resp) {
                        if (resp && resp.routes && resp.routes.length > 0) {
                            this.data = resp.routes;
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsAlSheduleRoute>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
