import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport, AmsPaxPayloadDemandTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAlSheduleRoute, AmsSheduleGroup, AmsSheduleGroupTableCriteria, AmsSheduleRouteTableCriteria } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsSheduleGroupEditDialog } from 'src/app/@share/components/dialogs/al-flp-group-edit/al-flp-group-edit.dialog';
import { AmsFlpSheduleRouteTableDataSource } from './flp-shedule-route-table.datasource';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';
import { AmsAlSheduleRouteEditDialog } from 'src/app/@share/components/dialogs/al-flp-route-edit/al-flp-route-edit.dialog';
import { FlpSheduleService } from '../../flp-shedule.service';

@Component({
    selector: 'ams-flp-shedule-route-table',
    templateUrl: './flp-shedule-route-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class AmsFlpSheduleRouteTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsAlSheduleRoute>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() tabindex: number;
    @Output() spmSheduleGroupOpen: EventEmitter<AmsAlSheduleRoute> = new EventEmitter<AmsAlSheduleRoute>();

    typeE = AmsPayloadType.E;
    typeB = AmsPayloadType.B;
    typeF = AmsPayloadType.F;
    typeC = AmsPayloadType.C;
    typeG = AmsPayloadType.G;

    selected: AmsAlSheduleRoute;

    criteria: AmsSheduleRouteTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;

    tabIdxChanged: Subscription;

    //, 'hubName'
    displayedColumns = ['routeId', 'routeNr', 'routeName', 'depApId', 'arrApId', 'distanceKm', 'flightH', 'priceF', 'priceB', 'priceE', 'priceCPerKg', 'adate'];
    airline: AmsAirline;

    isAdmin: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private alClient: AmsAirlineClient,
        private hubCache: AmsAirlineHubsCacheService,
        private apCache: AmsAirportCacheService,
        private spmMapService: MapLeafletSpmService,
        private flpShService: FlpSheduleService,
        public tableds: AmsFlpSheduleRouteTableDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsAlSheduleRoute>) => {
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsSheduleRouteTableCriteria) => {
            this.criteria = this.tableds.criteria;
            if (this.tabindex === this.flpShService.tabIdx) {
                this.loadPage();
            }
        });
        this.tabIdxChanged = this.flpShService.tabIdxChanged.subscribe(tabIdx => {
            this.initFields();
            if (this.tabindex === this.flpShService.tabIdx) {
                this.loadPage();
            }
        });

        this.initFields();
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }



    ngOnDestroy(): void {
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.isAdmin = this.cus.isAdmin;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        this.criteria.alId = this.airline?.alId;
        //console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.criteria.sortCol = 'adate';
        this.criteria.sortDesc = true;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick(): void {
        this.loadPage();
    }



    editRouteClick(value: AmsAlSheduleRoute): void {
        //console.log('editRouteClick -> value:', value);
        this.selected = value;
        if (value && this.cus.isAdmin) {

            const dialogRef = this.dialogService.open(AmsAlSheduleRouteEditDialog, {
                width: '921px',
                //height: '600px',
                data: {
                    route: this.selected,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.selected = res;
                    this.refreshClick();
                }
                this.initFields();
            });

        }

    }

    deleteRouteClick(value: AmsAlSheduleRoute): void {
        //console.log('deleteRouteClick -> value:', value);
        this.selected = value;
        if (value && this.cus.isAdmin) {
            this.preloader.show();
            this.alClient.sheduleRouteDelete(value.routeId).then(res => {
                this.preloader.hide();
                this.hubCache.clearCache();
                this.refreshClick();
            }).catch(err => {
                this.preloader.hide();
                console.log('deleteRouteClick -> err:', err);
            });
        }
    }

    spmHubOpenClick(value: AmsAlSheduleRoute) {
        console.log('spmHubOpenClick -> value:', value);
        this.selected = value;
        if (value) {
            //this.spmFlOpen.emit(fl);
        }
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }

    //#endregion

}
