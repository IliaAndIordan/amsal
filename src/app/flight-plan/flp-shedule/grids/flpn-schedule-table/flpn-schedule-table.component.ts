import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAlFlightNumber, AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleTableCriteria } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';
import { AmsAlFlpNumberScheduleTableDataSource } from './flpn-schedule-table.datasource';
import { AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsAlFlightNumberScheduleEditDialog } from 'src/app/@share/components/dialogs/al-flpn-schedule-edit/al-flpn-schedule-edit.dialog';
import { AmsWeekday } from 'src/app/@core/services/api/airline/enums';
import { AcTypeOpt } from 'src/app/@core/models/pipes/ac-type.pipe';
import { WeekDay } from '@angular/common';
import { MapLeafletAlSheduleGroupService } from 'src/app/@share/maps/map-liflet-al-shedule-group.service';
import { AmsAlpGroupWeekdayScheduleCopyDialog } from 'src/app/@share/components/dialogs/al-flp-group-weekday-schedule-copy/al-flp-group-weekday-schedule-copy.dialog';
import { FlpSheduleService } from '../../flp-shedule.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';

@Component({
    selector: 'ams-al-flpn-schedule-table',
    templateUrl: './flpn-schedule-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class AmsAlFlightNumberScheduleTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsAlFlightNumberSchedule>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() tabindex: number;
    @Output() spmAlFlightNumberOpen: EventEmitter<AmsAlFlightNumber> = new EventEmitter<AmsAlFlightNumber>();
    @Output() spmAlFlightNumberScheduleOpen: EventEmitter<AmsAlFlightNumberSchedule> = new EventEmitter<AmsAlFlightNumberSchedule>();

    typeE = AmsPayloadType.E;
    typeB = AmsPayloadType.B;
    typeF = AmsPayloadType.F;
    typeC = AmsPayloadType.C;
    typeG = AmsPayloadType.G;


    selected: AmsAlFlightNumberSchedule;

    criteria: AmsAlFlightNumberScheduleTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;

    group: AmsSheduleGroup;
    groupChanged: Subscription;
    weekday: AmsWeekday;
    weekdayChanged: Subscription;

    tabIdxChanged: Subscription;

    displayedColumns = ['flpnsId', 'flpnsName', 'depApId', 'dtimeH', 'arrApId', 'atimeH', 'distanceKm', 'flightH', 'minRunwayM', 'priceF', 'priceB', 'priceE', 'priceCpKg'];
    airline: AmsAirline;
    canEdit: boolean = false;

    aircraft$: Observable<AmsAircraft>;
    aircraft: AmsAircraft;
    opTimeMin: number = 5;
    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acClient: AmsAircraftClient,
        private alClient: AmsAirlineClient,
        private hubCache: AmsAirlineHubsCacheService,
        private apCache: AmsAirportCacheService,
        private acCache: AmsAirlineAircraftCacheService,
        private spmMapService: MapLeafletSpmService,
        private mapService: MapLeafletAlSheduleGroupService,
        private flpShService: FlpSheduleService,
        public tableds: AmsAlFlpNumberScheduleTableDataSource) {

    }

    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsAlFlightNumberSchedule>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsAlFlightNumberScheduleTableCriteria) => {
            this.initFields();
            if (this.tabindex === this.flpShService.tabIdx) {
                this.loadPage();
            }
        });

        this.tabIdxChanged = this.flpShService.tabIdxChanged.subscribe(tabIdx => {
            this.initFields();
            if (this.tabindex === this.flpShService.tabIdx) {
                this.loadPage();
            }
        });

        this.groupChanged = this.mapService.groupChanged.subscribe((group: AmsSheduleGroup) => {
            this.initFields();
            this.group = this.mapService.group;
            this.criteria = this.tableds.criteria;
            this.criteria.grpId = this.group ? this.group.grpId : undefined;
            this.tableds.criteria = this.criteria;
        });

        this.weekdayChanged = this.mapService.weekdayChanged.subscribe((weekday: AmsWeekday) => {
            this.initFields();
            this.weekday = this.mapService.weekday;
            this.criteria = this.tableds.criteria;
            this.criteria.wdId = this.weekday ? this.weekday.id : undefined;
            this.tableds.criteria = this.criteria;
        });

        this.initFields();
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }

    ngOnDestroy(): void {
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
        if (this.groupChanged) { this.groupChanged.unsubscribe(); }
        if (this.weekdayChanged) { this.weekdayChanged.unsubscribe(); }
    }

    initFields() {
        this.airline = this.cus.airline;
        this.group = this.mapService.group;
        this.weekday = this.mapService.weekday;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        this.criteria.alId = this.airline ? this.airline.alId : undefined;
        this.criteria.grpId = this.group ? this.group.grpId : undefined;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        this.criteria.alId = this.airline?.alId;
        this.canEdit = (this.group && this.group.isActive) ? false : true;
        if (this.group?.acId) {
            this.aircraft$ = this.acCache.getAircraft(this.group?.acId);
            this.aircraft$.subscribe(ac => {
                this.aircraft = ac;
                this.opTimeMin = this.aircraft.opTimeMin;
            });
        }


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.criteria.sortCol = 'adate';
        this.criteria.sortDesc = true;
        this.tableds.criteria = this.criteria;
    }

    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage(): void {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick(): void {
        this.loadPage();
    }

    rowClicked(value: AmsAlFlightNumberSchedule): void {
        console.log('rowClicked -> value:', value);
        this.selected = value;
    }

    addAlFlightNumberScheduleClick(): void {
        if (this.group && this.group.grpId) {
            let flpns = new AmsAlFlightNumberSchedule();
            flpns.alId = this.cus.airline.alId;
            flpns.grpId = this.group.grpId;
            let last = this.selected;
            if (!last && this.tableds.data && this.tableds.data.length > 0) {
                last = this.tableds.data[this.tableds.data.length - 1];
            }
            const acType = AcTypeOpt.find(x => x.acTypeId === this.group.acTypeId);
            console.log('addAlFlightNumberScheduleClick -> last:', last);
            if (last && last.flpnId) {
                const dep = last.nextFlightDepDate(this.opTimeMin);
                console.log('addAlFlightNumberScheduleClick -> dep:', dep);
                flpns.wdId = last.wdId;
                flpns.dtimeH = dep.getHours();
                flpns.dtimeMin = dep.getMinutes();
                flpns.depApId = last.arrApId;

            } else {
                flpns.wdId = this.weekday?.id ?? WeekDay.Monday;
                flpns.dtimeH = 6;
                flpns.dtimeMin = 30;
            }



            this.editAlFlightNumberScheduleClick(flpns);
        }


    }

    copyDayScheduleClick() {
        if (this.group && this.weekday && this.dataCount > 0 &&
            this.group.alId === this.airline.alId) {
            const dialogRef = this.dialogService.open(AmsAlpGroupWeekdayScheduleCopyDialog, {
                width: '821px',
                //height: '600px',
                data: {
                    weekday: this.weekday,
                    group: this.group,
                    shedulesCount: this.dataCount
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.refreshClick();
                }
                this.initFields();
            });
        }
    }

    editAlFlightNumberScheduleClick(value: AmsAlFlightNumberSchedule): void {
        //console.log('editAlFlightNumberScheduleClick -> value:', value);
        this.selected = value;
        if (this.selected && this.selected.alId === this.cus.airline.alId) {
            const dialogRef = this.dialogService.open(AmsAlFlightNumberScheduleEditDialog, {
                autoFocus: false,
                width: '971px',
                //height: '600px',
                data: {
                    flnschedule: this.selected,
                    group: this.group,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.selected = res;
                    this.refreshClick();
                }
                this.initFields();
            });
            //this.cus.showMessage({title:'Edit Flight Number', message:'Functionality under development.'})
        }
        else {
            this.toastr.info('Please select group first.', 'Select City');
        }
    }

    deleteAlFlightNumberScheduleClick(value: AmsAlFlightNumberSchedule): void {
        this.selected = value;
        if (value && value.alId === this.cus.airline.alId) {
            this.preloader.show();
            this.alClient.alFlightNumberScheduleDelete(value.flpnsId).then(res => {
                this.preloader.hide();
                this.hubCache.clearCache();
                this.refreshClick();
            }).catch(err => {
                this.preloader.hide();
                console.log('deleteMacCabin -> err:', err);
            });
        }
    }

    spmAlFlightNumberOpenClick(value: AmsAlFlightNumberSchedule) {
        console.log('spmAlFlightNumberOpenClick -> value:', value);
        this.selected = value;
        if (value && value.flpnId) {
            //this.cus.spmAirportPanelOpen.next(value.apId);
        }
    }

    spmAlFlightNumberScheduleOpenClick(value: AmsAlFlightNumberSchedule) {
        console.log('spmAlFlightNumberScheduleOpenClick -> value:', value);
        this.selected = value;
        if (value && value.flpnsId) {
            //this.cus.spmAirportPanelOpen.next(value.apId);
        }
    }

    //#endregion

}
