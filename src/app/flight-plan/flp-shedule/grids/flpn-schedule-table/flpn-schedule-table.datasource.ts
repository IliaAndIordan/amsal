import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleTableCriteria, AmsAlFlightNumberTableCriteria, ResponceAmsAlFlightNumberScheduleTableData } from 'src/app/@core/services/api/airline/al-flp-number';

export const KEY_CRITERIA = 'ams_al_flpn_schedule_table_criteria-v01';

@Injectable({ providedIn: 'root' })
export class AmsAlFlpNumberScheduleTableDataSource extends DataSource<AmsAlFlightNumberSchedule> {

    seleted: AmsAlFlightNumberSchedule;

    private _data: Array<AmsAlFlightNumberSchedule>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange = new Subject<AmsAlFlightNumberSchedule[]>();
    listSubject = new BehaviorSubject<AmsAlFlightNumberSchedule[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsAlFlightNumberSchedule> {
        return this._data;
    }

    set data(value: Array<AmsAlFlightNumberSchedule>) {
        this._data = value;
        this.listSubject.next(this._data as AmsAlFlightNumberSchedule[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsAlFlightNumberScheduleTableCriteria>();

    public get criteria(): AmsAlFlightNumberScheduleTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsAlFlightNumberTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsAlFlightNumberScheduleTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsAlFlightNumberScheduleTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = undefined;
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsAlFlightNumberScheduleTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsAlFlightNumberSchedule[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsAlFlightNumberSchedule>> {

        this.isLoading = true;

        return new Observable<Array<AmsAlFlightNumberSchedule>>(subscriber => {

            this.alClient.alFlightNumberScheduleTableP(this.criteria)
                .then((resp: ResponceAmsAlFlightNumberScheduleTableData) => {
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsAlFlightNumberSchedule>();
                    if (resp) {
                        if (resp && resp.flnschedules && resp.flnschedules.length > 0) {
                            this.data = resp.flnschedules;
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsAlFlightNumberSchedule>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
