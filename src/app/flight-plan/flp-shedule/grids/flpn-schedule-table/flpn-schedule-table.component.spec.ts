import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmsAlFlightNumberScheduleTableComponent } from './flpn-schedule-table.component';


describe('AmsAlFlightNumberScheduleTableComponent', () => {
  let component: AmsAlFlightNumberScheduleTableComponent;
  let fixture: ComponentFixture<AmsAlFlightNumberScheduleTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsAlFlightNumberScheduleTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsAlFlightNumberScheduleTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
