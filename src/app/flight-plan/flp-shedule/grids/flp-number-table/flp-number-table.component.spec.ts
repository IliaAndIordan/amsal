import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmsAlFlightNumberTableComponent } from './flp-number-table.component';


describe('AmsAlFlightNumberTableComponent', () => {
  let component: AmsAlFlightNumberTableComponent;
  let fixture: ComponentFixture<AmsAlFlightNumberTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsAlFlightNumberTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsAlFlightNumberTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
