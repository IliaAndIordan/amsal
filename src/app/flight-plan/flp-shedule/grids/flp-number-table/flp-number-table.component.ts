import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAlFlightNumber, AmsAlFlightNumberTableCriteria, ResponseAmsAlFlightNumberScheduleUpdateFromFlp } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsAlFlpNumberTableDataSource } from './flp-number-table.datasource';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';
import { AmsAlFlightNumberEditDialog } from 'src/app/@share/components/dialogs/al-flp-number-edit/al-flp-number-edit.dialog';
import { AmsAlFlightNumberPriceEditDialog } from 'src/app/@share/components/dialogs/al-flp-number-edit/al-flp-number-price-edit.dialog';
import { MapLeafletAlSheduleGroupService } from 'src/app/@share/maps/map-liflet-al-shedule-group.service';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';

@Component({
    selector: 'ams-al-flp-number-table',
    templateUrl: './flp-number-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class AmsAlFlightNumberTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsAlFlightNumber>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() tabindex: number;
    @Output() spmAlFlightNumberOpen: EventEmitter<AmsAlFlightNumber> = new EventEmitter<AmsAlFlightNumber>();

    typeE = AmsPayloadType.E;
    typeB = AmsPayloadType.B;
    typeF = AmsPayloadType.F;
    typeC = AmsPayloadType.C;
    typeG = AmsPayloadType.G;


    selected: AmsAlFlightNumber;

    criteria: AmsAlFlightNumberTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;

    //, 'hubName'
    displayedColumns = ['flpnId', 'flpnNumber', 'depApId', 'arrApId', 'distanceKm', 'flightH', 'minRunwayM', 'priceF', 'priceB', 'priceE', 'priceCpKg'];
    airline: AmsAirline;
    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aliService: AirlineInfoService,
        private acCache: AmsAirlineAircraftCacheService,
        private alClient: AmsAirlineClient,
        private hubCache: AmsAirlineHubsCacheService,
        private apCache: AmsAirportCacheService,
        private spmMapService: MapLeafletSpmService,
        private mapService: MapLeafletAlSheduleGroupService,
        public tableds: AmsAlFlpNumberTableDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsAlFlightNumber>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsAlFlightNumberTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.loadPage();
        });

        this.initFields();
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria?.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        this.criteria.alId = this.airline?.alId;
        //console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events


    rowClicked(value: AmsAlFlightNumber): void {
        this.selected = value;
    }

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
          if (event.keyCode != 13) {
            $this.setFilter( (event.target as HTMLInputElement).value);
          }
        }, 1000);
    }

    setFilter(value:string){
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = 'adate';
        this.criteria.sortDesc = true;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }

    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick(): void {
        this.loadPage();
    }

    addAlFlightNumberClick(): void {
        //console.log('addGrouplick -> ');
        let flpn = new AmsAlFlightNumber();
        flpn.alId = this.cus.airline.alId;
        const maxNum = Math.max.apply(null,
            this.tableds.data.map(function (o) { return o.flpnNumber; }));
        flpn.flpnNumber = (maxNum + 1) ?? 100;
        flpn.depApId = this.selected ? this.selected.arrApId : this.mapService.group?.apId;
        if (this.selected && this.selected.arrApId !== this.mapService.group?.apId) {
            flpn.arrApId = this.mapService.group?.apId;
        }

        this.newAlFlightNumberClick(flpn);
    }

    newAlFlightNumberClick(value: AmsAlFlightNumber): void {
        //console.log('editAlFlightNumberClick -> value:', value);

        if (value && value.alId === this.cus.airline.alId) {

            const ac$ = this.acCache.getAircraft(this.mapService?.group?.acId);
            ac$.subscribe(ac => {
                const dialogRef = this.dialogService.open(AmsAlFlightNumberEditDialog, {
                    autoFocus: false,
                    width: '921px',
                    //height: '600px',
                    data: {
                        flpn: value,
                        aircraft: ac
                    }
                });

                dialogRef.afterClosed().subscribe(res => {
                    if (res) {
                        this.selected = res;
                        this.refreshClick();
                    }
                    this.initFields();
                });
            });



            //this.cus.showMessage({title:'Edit Flight Number', message:'Functionality under development.'})
        }
        else {
            this.toastr.info('Please select airport first.', 'Select City');
        }
    }

    editAlFlightNumberClick(value: AmsAlFlightNumber): void {
        console.log('editAlFlightNumberClick -> value:', value);
        this.selected = value;
        if (this.selected && this.selected.alId === this.cus.airline.alId) {

            const ac$ = this.acCache.getAircraft(this.mapService?.group?.acId);
            ac$.subscribe(ac => {
                const dialogRef = this.dialogService.open(AmsAlFlightNumberPriceEditDialog, {
                    width: '921px',
                    //height: '600px',
                    data: {
                        flpn: this.selected,
                        aircraft: ac
                    }
                });

                dialogRef.afterClosed().subscribe(res => {
                    if (res) {
                        this.selected = res;
                        this.refreshClick();
                    }
                    this.initFields();
                });
            });


            //this.cus.showMessage({title:'Edit Flight Number', message:'Functionality under development.'})
        }
        else {
            this.toastr.info('Please select airport first.', 'Select City');
        }
    }

    deleteAlFlightNumberClick(value: AmsAlFlightNumber): void {
        //console.log('deleteGroupClick -> value:', value);
        this.selected = value;
        if (value && value.alId === this.cus.airline.alId) {
            this.preloader.show();
            this.alClient.alFlightNumberDelete(value.flpnId).then(res => {
                this.preloader.hide();
                this.hubCache.clearCache();
                this.refreshClick();
            }).catch(err => {
                this.preloader.hide();
                console.log('deleteMacCabin -> err:', err);
            });
        }
    }

    updateAllFlpsPricesFromFlpn(value: AmsAlFlightNumber): void {
        if (value && value.alId === this.cus.airline.alId) {
            this.cus.showMessage({
                title: 'Update Flight Shedule Prices',
                message: `This action will update prices for all sheduled flights of ${value.flpnName}.<br/>
                        Are you sure you want to continue width update?`
            }).then(res => {
                if (res) {
                    this.preloader.show();
                    this.alClient.alFlightNumberScheduleUpdateFromFlpnP(value.routeId)
                        .then((result: ResponseAmsAlFlightNumberScheduleUpdateFromFlp) => {
                            this.preloader.hide();
                            if (result.success) {
                                this.toastr.success(
                                    `Updated <b>${result.data.affectedRows}</b> Flight Number Shedules.`,
                                    'Update Prices',
                                    { enableHtml: true });
                            } else {
                                this.toastr.success('Update Prices Failed', result.message);
                            }
                        }).catch(err => {
                            this.preloader.hide();
                        });
                }
            });
        }
    }

    spmAlFlightNumberOpenClick(value: AmsAlFlightNumber) {
        console.log('spmAlFlightNumberOpenClick -> value:', value);
        this.selected = value;
        if (value && value.flpnId) {
            //this.cus.spmAirportPanelOpen.next(value.apId);
        }
    }

    //#endregion

}
