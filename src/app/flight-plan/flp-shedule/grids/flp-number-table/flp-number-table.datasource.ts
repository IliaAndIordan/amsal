import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAlFlightNumber, AmsAlFlightNumberTableCriteria, ResponceAmsAlFlightNumberTableData } from 'src/app/@core/services/api/airline/al-flp-number';

export const KEY_CRITERIA = 'ams_al_flp_number_table_criteria-v01';

@Injectable({ providedIn: 'root' })
export class AmsAlFlpNumberTableDataSource extends DataSource<AmsAlFlightNumber> {

    seleted: AmsAlFlightNumber;

    private _data: Array<AmsAlFlightNumber>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange = new Subject<AmsAlFlightNumber[]>();
    listSubject = new BehaviorSubject<AmsAlFlightNumber[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsAlFlightNumber> {
        return this._data;
    }

    set data(value: Array<AmsAlFlightNumber>) {
        this._data = value;
        this.listSubject.next(this._data as AmsAlFlightNumber[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsAlFlightNumberTableCriteria>();

    public get criteria(): AmsAlFlightNumberTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsAlFlightNumberTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsAlFlightNumberTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsAlFlightNumberTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'flpnNumber';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsAlFlightNumberTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsAlFlightNumber[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsAlFlightNumber>> {

        this.isLoading = true;

        return new Observable<Array<AmsAlFlightNumber>>(subscriber => {

            this.alClient.alFlightNumberTableP(this.criteria)
                .then((resp: ResponceAmsAlFlightNumberTableData) => {
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsAlFlightNumber>();
                    if (resp) {
                        if (resp && resp.flnrs && resp.flnrs.length > 0) {
                            this.data = resp.flnrs;
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsAlFlightNumber>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
