import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlpSheduleTabsHomeComponent } from './flp-shedule-tabs-home.component';

describe('Ams -> Flight Plan ->Shedule -> FlpSheduleTabsHomeComponent', () => {
  let component: FlpSheduleTabsHomeComponent;
  let fixture: ComponentFixture<FlpSheduleTabsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpSheduleTabsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpSheduleTabsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
