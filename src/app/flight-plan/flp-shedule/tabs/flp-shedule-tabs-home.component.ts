import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from '../../flight-plan.service';
import { FlpSheduleService } from '../flp-shedule.service';
import { MatTabGroup } from '@angular/material/tabs';
import { MapLifletScheduleGroupComponent } from 'src/app/@share/maps/map-liflet-shedule-group/map-liflet-shedule-group.component';

@Component({
  templateUrl: './flp-shedule-tabs-home.component.html',
})
export class FlpSheduleTabsHomeComponent implements OnInit, OnDestroy {

  @ViewChild('grpMapTab', { static: false }) grpMapTab: MapLifletScheduleGroupComponent;
  @ViewChild('tabGrp', { static: false }) tabGrp: MatTabGroup;

  selTabIdx: number;
  tabIdxChanged: Subscription;



  constructor(
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private spinerService: SpinnerService,
    private flpService: AmsFlightPlanService,
    private flpShService: FlpSheduleService) {

  }

  ngOnInit(): void {
    this.tabIdxChanged = this.flpShService.tabIdxChanged.subscribe(tabIdx => {
      this.initFields();
      if (this.selTabIdx === 0) {
        if (this.grpMapTab) { this.grpMapTab.refreshGroup(); }
      }
    });

    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

  }

  initFields() {
    this.selTabIdx = this.flpShService.tabIdx;
  }

  //#region  Tab Panel Actions

  selectedTabChanged(tabIdx: number) {
    const oldTab = this.selTabIdx;
    this.selTabIdx = tabIdx;
    if (this.tabGrp) { this.tabGrp.realignInkBar() };
    this.flpShService.tabIdx = this.selTabIdx;
  }


  //#endregion

}
