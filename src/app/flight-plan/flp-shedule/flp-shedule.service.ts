import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AcLastArrTime, AmsFlightEstimate, AmsFlightsEstimateTableCriteria } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { AmsFlightPlanService } from '../flight-plan.service';


export const SR_LEFT_PANEL_IN_KEY = 'ams_al_flp_shedule_filter_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_al_flp_shedule_tab_idx';
export const SR_SEL_AC_CHARTER_KEY = 'ams_al_flp_shedule_aircraft';
export const SR_SEL_AC_LAST_ARR_TIME_KEY = 'ams_al_flp_shedule_aircraft_last_arr';

@Injectable({
  providedIn: 'root'
})
export class FlpSheduleService {

  constructor(
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private flClient: AmsFlightClient,
    private flpService: AmsFlightPlanService) {
      this.flpService.aircraftForCharterChanged.subscribe(acCharterList => {
        if (this.flpService.aircraftForCharter&& this.flpService.aircraftForCharter.length > 0) {
          const currSc = this.aircraft;
          //console.log('FlpCharterService -> aircraftForCharterChanged ac:', currSc);
          this.aircraft = currSc  ? this.flpService.aircraftForCharter.find(x => x.acId == currSc.acId) : undefined;
        }
      });
    
  }

  updateFlightQueue = new BehaviorSubject<AmsAircraft>(undefined)
  aircraftForCharter$ = this.flpService.aircraftForCharter;

  //#region Filter Panel and Tab IDX

  panelInChanged = new BehaviorSubject<boolean>(true);

  get panelIn(): boolean {
    let rv = true;
    const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
    if (valStr) {
      rv = JSON.parse(valStr) as boolean;
    }
    return rv;
  }

  set panelIn(value: boolean) {
    localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
    this.panelInChanged.next(value);
  }

  tabIdxChanged = new BehaviorSubject<number>(undefined);

  get tabIdx(): number {
    let rv = 0;
    const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
    //console.log('selFolderId-> dataStr', dataStr);
    if (dataStr) {
      try {
        rv = parseInt(dataStr, 10);
      }
      catch {
        localStorage.removeItem(SR_SEL_TABIDX_KEY);
        rv = 1;
      }

    }
    // console.log('selTabIdx-> rv', rv);
    return rv;
  }

  set tabIdx(value: number) {
    // console.log('selTabIdx->', value);
    const oldValue = this.tabIdx;

    localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
    if (oldValue !== value) {
      this.tabIdxChanged.next(value);
    }
  }

  //#endregion



  
  //#region Aircraft

  aircraftChanged = new BehaviorSubject<AmsAircraft>(undefined);

  get aircraft(): AmsAircraft {
    let rv: AmsAircraft
    const dataStr = localStorage.getItem(SR_SEL_AC_CHARTER_KEY);
    if (dataStr) {
      try {
        rv = AmsAircraft.fromJSON(JSON.parse(dataStr));//Object.assign(new AmsAircraft, JSON.parse(dataStr))
      }
      catch {
        localStorage.removeItem(SR_SEL_AC_CHARTER_KEY);
      }

    }
    return rv;
  }


  set aircraft(value: AmsAircraft) {
    // console.log('selTabIdx->', value);
    const oldValue = this.aircraft;
    localStorage.setItem(SR_SEL_AC_CHARTER_KEY, JSON.stringify(value));
    this.aircraftChanged.next(value);
  }

  //#endregion

  //#region AcLastArrTime

  acLastArrTimeChanged = new BehaviorSubject<AcLastArrTime>(undefined);

  get acLastArrTime(): AcLastArrTime {
    let rv: AcLastArrTime
    const dataStr = localStorage.getItem(SR_SEL_AC_LAST_ARR_TIME_KEY);
    if (dataStr) {
      try {
        rv = AcLastArrTime.fromJSON(JSON.parse(dataStr));
      }
      catch {
        localStorage.removeItem(SR_SEL_AC_LAST_ARR_TIME_KEY);
      }

    }
    return rv;
  }

  set acLastArrTime(value: AcLastArrTime) {
    // console.log('selTabIdx->', value);
    const oldValue = this.aircraft;
    localStorage.setItem(SR_SEL_AC_LAST_ARR_TIME_KEY, JSON.stringify(value));
    this.acLastArrTimeChanged.next(value);
  }

  getLastArrTime(acId: number): Observable<AcLastArrTime> {

    return new Observable<AcLastArrTime>(subscriber => {
      this.flClient.getLastArrTime(acId)
        .subscribe((resp: AcLastArrTime) => {
          this.acLastArrTime = resp;
          subscriber.next(this.acLastArrTime);
        }, err => {
          console.log('loadAirline-> err', err);
          subscriber.next(undefined);
        });
    });

  }

  //#endregion

  //#region Charter Flights

  estimateCharterFlightsForGrid(criteria: AmsFlightsEstimateTableCriteria): Observable<AmsFlightEstimate[]> {
    //console.log('estimateCharterFlightsForGrid-> criteria', criteria);
    //console.log('estimateCharterFlightsForGrid-> depTime', criteria.depTime);
    return new Observable<AmsFlightEstimate[]>(subscriber => {

      this.flClient.flightCharterEstimate(criteria.acId, criteria.apId, criteria.depTime)
        .subscribe((resp: AmsFlightEstimate[]) => {
          //console.log('estimateCharterFlightsForGrid-> resp', resp);
          subscriber.next(resp);
        },
          err => {
            throw err;
          });
    });

  }

  //#endregion
}
