import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from '../flight-plan.service';
import { FlpSheduleService } from './flp-shedule.service';
import { FlpSheduleFilterPanelComponent } from './filter-panel/flp-shedule-filter-panel.component';

@Component({
  templateUrl: './flp-shedule.component.html',
})
export class FlpSheduleComponent  implements OnInit {

  @ViewChild('filterPanel') filterPanel: FlpSheduleFilterPanelComponent;

  in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  get panelIn(): boolean {
    let rv = this.flpShService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.flpShService.panelIn;
  }

  set panelIn(value: boolean) {
    this.flpShService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private spinerService: SpinnerService,
    private flpService: AmsFlightPlanService,
    private flpShService: FlpSheduleService) {
  }

  ngOnInit(): void {
    
    this.panelInChanged = this.flpShService.panelInChanged.subscribe((panelIn: boolean) => {
      this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    });

  }

  //#region Filter Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  refreshPanelLeft():void{
    if(this.filterPanel){
      this.filterPanel.loadPage();
    }
  }

  //#endregion
}
