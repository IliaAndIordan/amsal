import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlpSheduleComponent } from './flp-shedule.component';

describe('FlpSheduleComponent', () => {
  let component: FlpSheduleComponent;
  let fixture: ComponentFixture<FlpSheduleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FlpSheduleComponent]
    });
    fixture = TestBed.createComponent(FlpSheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
