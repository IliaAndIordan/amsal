import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ToastrService } from 'ngx-toastr';
import { Observable, of, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AcLastArrTime } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from '../../flight-plan.service';
import { FlpSheduleService } from '../flp-shedule.service';
import { AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsWeekday, AmsWeekdaysOpt } from 'src/app/@core/services/api/airline/enums';
import { MatSelectChange } from '@angular/material/select';
import { AmsSheduleGroupEditDialog } from 'src/app/@share/components/dialogs/al-flp-group-edit/al-flp-group-edit.dialog';
import { MatDialog } from '@angular/material/dialog';
import { MapLeafletAlSheduleGroupService } from 'src/app/@share/maps/map-liflet-al-shedule-group.service';

@Component({
  selector: 'ams-flp-shedule-filter-panel',
  templateUrl: './flp-shedule-filter-panel.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class FlpSheduleFilterPanelComponent implements OnInit, AfterViewInit {

  @Output() loadAircraftForCharter: EventEmitter<any> = new EventEmitter<any>();

  aircraftOpt: Observable<AmsAircraft[]>;
  aircrafts$: Observable<AmsAircraft[]>;
  aircraft: AmsAircraft;

  acLastArrTimeChanged: Subscription;
  acLastArrTime: AcLastArrTime;

  airport$: Observable<AmsAirport>;
  airport: AmsAirport;
  minEtdTime: Date;

  formGrp: FormGroup;

  constructor(
    private toastr: ToastrService,
    private fb: FormBuilder,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private alClient: AmsAirlineClient,
    private mapService: MapLeafletAlSheduleGroupService,
    private spinerService: SpinnerService,
    private flpService: AmsFlightPlanService,
    private flpShService: FlpSheduleService) {
  }



  ngOnInit(): void {
    this.acLastArrTimeChanged = this.flpShService.acLastArrTimeChanged.subscribe(val => {
      this.initFields();
    });

    this.groupChanged = this.mapService.groupChanged.subscribe(val => {
      this.group = this.mapService.group;
      this.formGrp.patchValue({ group: this.group });
      this.formGrp.updateValueAndValidity();
    });

    this.group = this.mapService.group;
    this.weekday = this.mapService.weekday;
    this.formGrp = this.fb.group({
      group: new FormControl(this.groups ? this.groups : '', []),
      weekday: new FormControl(this.weekday ? this.weekday.id : '', []),
    });

    this.loadPage();

  }

  ngOnDestroy(): void {
    if (this.acLastArrTimeChanged) { this.acLastArrTimeChanged.unsubscribe(); }
  }

  ngAfterViewInit(): void {

  }

  get groupFc() { return this.formGrp.get('group') as FormControl; }
  get weekdayFc() { return this.formGrp.get('weekday') as FormControl; }

  fError(fname: string): string {
    const field = this.formGrp.get(fname);
    return field.hasError('required') ? 'Field ' + fname + '  is required.' :
      field.hasError('minlength') ? 'Field ' + fname + ' to short.' :
        field.hasError('maxlength') ? 'Field ' + fname + ' to long.' : '';
  }


  initFields() {

    this.aircraft = this.flpShService.aircraft;
    this.acLastArrTime = this.flpShService.acLastArrTime;
    this.group = this.mapService.group;
    this.weekday = this.mapService.weekday;


  }

  //#region AC Form

  getLastArrTime(): void {
    if (this.aircraft && this.aircraft.acId) {
      this.flpShService.getLastArrTime(this.aircraft.acId)
        .subscribe(val => {
          this.acLastArrTime = val;
        });
    }
  }

  //#region SPM


  spmAirportOpen(airport: AmsAirport) {
    this.flpService.airportPanelOpen.next(airport);
  }
  spmAirlineOpen(ac: AmsAircraft) {
    //console.log('spmAirlineOpen -> ac:', ac);
    this.flpService.spmAircraftOpen.next(ac);
  }

  //#endregion

  //#region AmsWeekday

  weekday: AmsWeekday;
  weekdays: AmsWeekday[] = AmsWeekdaysOpt;

  weekdayChanged(evnt: MatSelectChange): void {
    if (this.weekdayFc.valid && this.weekdayFc.value) {
      const val = this.weekdayFc.value;
      this.weekday = this.weekdays.find(x => x.id === val);
      this.mapService.weekday = this.weekday;
    }
  }
  //#endregion

  //#region AmsSheduleGroup

  group: AmsSheduleGroup;
  groupChanged: Subscription;
  groups: AmsSheduleGroup[];
  groupsCount: number = 0;

  groupsOpt: Observable<AmsSheduleGroup[]>;
  groups$: Observable<AmsSheduleGroup[]>;

  //#endregion

  //#region Group Selector
  displayGroup(value: AmsSheduleGroup): string {
    let rv: string = '';
    if (value && value.grpName) {
      rv = ' ';
      rv = value.grpName ? value.grpName : value.grpId.toString();
    } else {
      //rv = `All Flight Numbers`;
    }

    return rv;
  }

  valueChangeGroup(event: MatAutocompleteSelectedEvent) {
    //console.log('valueChangeGroup -> event:', event);
    if (this.groupFc.valid) {
      const value = this.groupFc.value;
      if (value && value.grpId) {
        const grp: AmsSheduleGroup = AmsSheduleGroup.fromJSON(this.groupFc.value);
        this.group = grp;
        this.mapService.group = this.group;
        this.getLastArrTime();
        //this.loadCountries(subregion);
      } else {
        this.group = undefined;
        this.mapService.group = this.group;
      }
    }
  }

  resetGroup(): void {
    this.groupFc.patchValue('');
    this.group = undefined;
    this.formGrp.updateValueAndValidity();
  }

  filterGroup(val: any): AmsSheduleGroup[] {
    // console.log('filterDpc -> val', val);
    let sr: AmsSheduleGroup;
    if (val && val.apId) {
      sr = val as AmsSheduleGroup;
    }

    // console.log('filterBranches -> this.selDpc', this.selDpc);
    const value = val.grpName || val; // val can be companyName or string
    const filterValue = value ? value.toLowerCase() : '';
    // console.log('_filter -> filterValue', filterValue);
    let rv = new Array<AmsSheduleGroup>()
    if (this.groups && this.groups.length) {
      rv = this.groups.filter(x => (
        x.grpName.toLowerCase().indexOf(filterValue) > -1));
    }
    // console.log('_filter -> rv', rv);
    return rv;
  }


  initGroupsFc() {
    this.groupsOpt = of(this.groups);

    this.groupsOpt = this.groupFc.valueChanges
      .pipe(
        startWith(null),
        map(val => val ? this.filterGroup(val) :
          (this.groups ? this.groups.slice() : new Array<AmsSheduleGroup>()))
      );

    let sr: AmsSheduleGroup;
    if (this.groups && this.groups.length > 0) {
      sr = this.group && this.group.grpId ? this.groups.find(x => x.grpId == this.group.grpId) : undefined;
    }
    this.formGrp.patchValue({ group: sr });
    this.formGrp.updateValueAndValidity();
  }

  editGroupClick(): void {
    //console.log('editGroupClick -> value:', value);
    if (this.group && this.group.alId === this.cus.airline.alId) {

      const dialogRef = this.dialogService.open(AmsSheduleGroupEditDialog, {
        width: '721px',
        //height: '600px',
        data: {
          group: this.group,
        }
      });

      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.group = res;
          this.mapService.group = this.group;
        }
        this.initFields();
      });
    }
    else {
      this.toastr.info('Please select airport first.', 'Select City');
    }
  }

  //#endregion

  //#region Data

  loadPage() {
    this.mapService.loadGroups()
      .subscribe(res => {
        this.groups = this.mapService.groups;
        this.groupsCount = this.mapService.groupsCount;
        this.group = this.mapService.group;
        this.weekday = this.mapService.weekday;
        this.initGroupsFc();
        //console.log('loadPage -> groups:', this.groups);
        this.initFields();
      });
  }


  refreshAircrafs() {
    this.spinerService.show();
    this.flpService.loadAircraftForCharter()
      .subscribe(res => {
        this.spinerService.hide();
        this.loadPage();
      });
  }

  //#endregion

}
