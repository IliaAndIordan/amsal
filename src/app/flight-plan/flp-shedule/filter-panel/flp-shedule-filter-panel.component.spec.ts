import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlpSheduleFilterPanelComponent } from './flp-shedule-filter-panel.component';


describe('Ams -> Flight Plan ->Charter -> FlpSheduleFilterPanelComponent', () => {
  let component: FlpSheduleFilterPanelComponent;
  let fixture: ComponentFixture<FlpSheduleFilterPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpSheduleFilterPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpSheduleFilterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
