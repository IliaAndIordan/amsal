import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from '../@core/const/app-routes.const';
import { FlpCharterComponent } from './charter/flp-charter.component';
import { FlpCharterTebsHomeComponent } from './charter/tabs/flp-charter-tebs-home.component';
import { FlpDashboardComponent } from './dashboard/flp-dashboard.component';
import { FlightPlanComponent } from './flight-plan.component';
import { FlpSheduleComponent } from './flp-shedule/flp-shedule.component';
import { FlpSheduleTabsHomeComponent } from './flp-shedule/tabs/flp-shedule-tabs-home.component';
import { FlpBookingHomeComponent } from './flp-booking/flp-booking.component';
import { FlpBookingTabsHomeComponent } from './flp-booking/tabs/flp-booking-tabs-home.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: FlightPlanComponent,
    children: [
      { path: AppRoutes.Root, pathMatch: 'full', component: FlpDashboardComponent },
      { path: AppRoutes.charter, component: FlpCharterComponent,
        children:[
          { path: AppRoutes.Root, pathMatch: 'full', component: FlpCharterTebsHomeComponent },
        ]
      },
      { path: AppRoutes.shedule, component: FlpSheduleComponent,
        children:[
         { path: AppRoutes.Root, pathMatch: 'full', component: FlpSheduleTabsHomeComponent },
        ]
      },
      { path: AppRoutes.booking, component: FlpBookingHomeComponent,
        children:[
         { path: AppRoutes.Root, pathMatch: 'full', component: FlpBookingTabsHomeComponent },
        ]
      }

      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FlightPlanRoutingModule { }

export const routableComponents = [
  FlightPlanComponent,
  FlpDashboardComponent,
  FlpCharterComponent,
  FlpCharterTebsHomeComponent,
  //
  FlpSheduleComponent,
  FlpSheduleTabsHomeComponent,
  //
  FlpBookingHomeComponent,
  FlpBookingTabsHomeComponent,
];
