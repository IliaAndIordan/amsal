import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlpDashboardComponent } from './flp-dashboard.component';

describe('Ams -> Flight Plan -> FlpDashboardComponent', () => {
  let component: FlpDashboardComponent;
  let fixture: ComponentFixture<FlpDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
