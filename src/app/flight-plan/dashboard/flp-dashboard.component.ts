import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Subscription } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { URL_COMMON_IMAGE_AMS_COMMON, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from '../flight-plan.service';

@Component({
  selector: 'ams-flp-dashboard',
  templateUrl: './flp-dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FlpDashboardComponent implements OnInit, OnDestroy {

  tileImgRootUrl = URL_COMMON_IMAGE_TILE;
  commonImgRootUrl = URL_COMMON_IMAGE_AMS_COMMON;
  acCharterImg = URL_COMMON_IMAGE_AMS_COMMON + 'charter_01.png';
  acSheduleImg = URL_COMMON_IMAGE_AMS_COMMON + 'shedule-white.png';

  acCharter: AmsAircraft[];
  acCharterCount:number;
  acCharterCountSubj = new BehaviorSubject<number>(0);
  acCharterChanged: Subscription;

  acCharterCount$ = this.acCharterCountSubj.asObservable();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    public dialog: MatDialog,
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private flpService: AmsFlightPlanService) { }
 

  ngOnInit(): void {
    this.acCharterChanged = this.flpService.aircraftForCharterChanged.subscribe(acCharter => {
      //console.log('aircraftForCharterChanged -> acCharter:', acCharter);
      this.spinerService.show();
      this.initFields();
      this.acCharterCountSubj.next(this.acCharterCount);
      this.spinerService.hide();
    });
  }

  ngOnDestroy(): void {
    if (this.acCharterChanged) { this.acCharterChanged.unsubscribe(); }
  }

  initFields() {
    this.acCharter = this.flpService.aircraftForCharter;
    this.acCharterCount = this.acCharter?this.acCharter.length:0;
    //console.log('aircraftForCharterChanged -> acCharterCount:', this.acCharterCount);
  }


  //#region Action Methods

  gotoCharterFlightPlan(): void {
    this.router.navigate([AppRoutes.Root, AppRoutes.flightPlan, AppRoutes.charter]);
  }
  gotoSheduleFlightPlan():void{
    this.router.navigate([AppRoutes.Root, AppRoutes.flightPlan, AppRoutes.shedule]);
  }
  gotoBookingFlightPlan():void{
    this.router.navigate([AppRoutes.Root, AppRoutes.flightPlan, AppRoutes.booking]);
  }
  //#endregion
}
