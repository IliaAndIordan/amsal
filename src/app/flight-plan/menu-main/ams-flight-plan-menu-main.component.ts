import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsTransactionsSum } from 'src/app/@core/services/api/airline/transaction';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { AmsFlightPlanService } from '../flight-plan.service';

@Component({
  selector: 'ams-flight-plan-menu-main',
  templateUrl: './ams-flight-plan-menu-main.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AmsFlightPlanMenuMainComponent implements OnInit, OnDestroy {

  @Output() refreshTransactions: EventEmitter<AmsAirline> = new EventEmitter<AmsAirline>();
  
  
  roots = AppRoutes;
  
  user: UserModel;
  userChanged: Subscription;

  airline:AmsAirline;
  
  constructor(
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private flpService: AmsFlightPlanService) {

  }

  ngOnInit(): void {
    this.userChanged = this.cus.userChanged.subscribe(user => {
      this.initFields();
    });
    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
  }

  initFields() {
    this.user = this.cus.user;
    this.airline = this.cus.airline;
   
  }

  loadTransactionsSumLastWeek(): void {
    
    this.trService.loadTrSumLastWeek(this.airline.alId)
      .subscribe((res: AmsTransactionsSum[]) => {
        this.initFields();
      },
        err => {
          console.log('loadTransactionsSumLastWeek-> err: ', err);
        });

  }

}
