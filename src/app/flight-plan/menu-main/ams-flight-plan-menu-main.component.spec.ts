import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmsFlightPlanMenuMainComponent } from './ams-flight-plan-menu-main.component';


describe('Ams -> Flight Plan -> AmsFlightPlanMenuMainComponent', () => {
  let component: AmsFlightPlanMenuMainComponent;
  let fixture: ComponentFixture<AmsFlightPlanMenuMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsFlightPlanMenuMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsFlightPlanMenuMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
