import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AcLastArrTime } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AirlineService } from 'src/app/airline/airline.service';
import { AmsFlightPlanService } from '../../flight-plan.service';
import { FlpCharterService } from '../flp-charter.service';

@Component({
  selector: 'ams-flp-charter-filter-panel',
  templateUrl: './flp-charter-filter-panel.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class FlpCharterFilterPanelComponent implements OnInit, AfterViewInit {

  @Output() loadAircraftForCharter: EventEmitter<any> = new EventEmitter<any>();


  acCharter: AmsAircraft[];
  acCharterCount: number;
  acCharterChanged: Subscription;

  acformGrp: UntypedFormGroup;
  aircraftOpt: Observable<AmsAircraft[]>;
  aircrafts$: Observable<AmsAircraft[]>;
  aircraft: AmsAircraft;

  acLastArrTimeChanged: Subscription;
  acLastArrTime: AcLastArrTime;

  airport$: Observable<AmsAirport>;
  airport: AmsAirport;
  minEtdTime: Date;

  constructor(
    private fb: UntypedFormBuilder,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private spinerService: SpinnerService,
    private flpService: AmsFlightPlanService,
    private flpChService: FlpCharterService) {
  }



  ngOnInit(): void {

    this.aircraft = this.flpChService.aircraft;
    this.acCharter = this.flpService.aircraftForCharter;
    this.acformGrp = this.fb.group({
      aircraftF: new UntypedFormControl(this.aircraft ? this.aircraft : '', [Validators.required]),
    });
    this.acCharterChanged = this.flpService.aircraftForCharterChanged.subscribe(acCharter => {
      this.acCharter = this.flpService.aircraftForCharter;
      this.initFields();
      this.initAircrafts();
      this.getLastArrTime();
    });
    this.acLastArrTimeChanged = this.flpChService.acLastArrTimeChanged.subscribe(val => {
      this.initFields();
    });

    
    this.aircrafts$ = this.flpService.loadAircraftForCharter();

    this.aircrafts$.subscribe(aircrafts => {
      this.acCharter = aircrafts;
      this.initAircrafts();
    });
   
    //this.initFields();
    this.acformGrp.updateValueAndValidity();

  }

  ngOnDestroy(): void {
    if (this.acCharterChanged) { this.acCharterChanged.unsubscribe(); }
    if (this.acLastArrTimeChanged) { this.acLastArrTimeChanged.unsubscribe(); }
  }

  ngAfterViewInit(): void {
    if (!this.acCharterChanged) {
      this.acCharterChanged = this.flpService.aircraftForCharterChanged.subscribe(acCharter => {
        this.acCharter = this.flpService.aircraftForCharter;
        this.initFields();
        this.initAircrafts();
        this.getLastArrTime();
      });
    }

    if (!this.acLastArrTimeChanged) {
      this.acLastArrTimeChanged = this.flpChService.acLastArrTimeChanged.subscribe(val => {
        this.initFields();
      });
    }

    this.getLastArrTime();
  }

  initFields() {
    this.acCharter = this.flpService.aircraftForCharter;
    this.acCharterCount = this.acCharter ? this.acCharter.length : 0;
    this.aircraft = this.flpChService.aircraft;
    this.acLastArrTime = this.flpChService.acLastArrTime;
    //console.log('initFields -> acLastArrTime:', this.acLastArrTime);

    this.minEtdTime = this.acLastArrTime ? this.acLastArrTime.minEtdTime : new Date();
    //console.log('initFields -> minEtdTime:', this.minEtdTime);
    if (this.acLastArrTime && this.acLastArrTime.apId) {
      this.airport$ = this.apCache.getAirport(this.acLastArrTime.apId);
      this.airport$.subscribe(ap => {
        this.airport = ap;
      });
    }

    //console.log('initFields -> acCharterCount:', this.acCharterCount);

  }

  //#region AC Form

  get aircraftF() { return this.acformGrp.get('aircraftF'); }

  fError(fname: string): string {
    const field = this.acformGrp.get(fname);
    return field.hasError('required') ? 'Field is required.' :
      field.hasError('min') ? 'Validation Error. Field value to low.' :
        field.hasError('max') ? 'Validation Error. Field value to height.' :
          field.hasError('minlength') ? 'Validation Error. Field  to short.' :
            field.hasError('maxlength') ? 'Field to long.' : '';
  }

  displayAircraft(value: AmsAircraft): string {
    let rv: string = '';
    if (value) {
      rv = ' ';
      rv = value.registration ? value.registration : '';
    }
    return rv;
  }

  acValueChange(event: MatAutocompleteSelectedEvent) {
    if (this.aircraftF.valid) {
      const value: AmsAircraft = AmsAircraft.fromJSON(this.aircraftF.value);
      if (value && value.acId) {
        this.aircraft = value;
        this.flpChService.aircraft = this.aircraft;
        this.getLastArrTime();
        //this.loadCountries(subregion);
      }
    }
  }

  resetAircraftF(): void {
    console.log('resetAircraftF -> ');
    this.aircraftF.patchValue('');
    this.aircraft = undefined;
    this.acformGrp.updateValueAndValidity();
  } 

  filterAircraft(val: any): AmsAircraft[] {
    // console.log('filterDpc -> val', val);
    let sr: AmsAircraft;
    if (val && val.apId) {
      sr = val as AmsAircraft;
    }

    // console.log('filterBranches -> this.selDpc', this.selDpc);
    const value = val.registration || val; // val can be companyName or string
    const filterValue = value ? value.toLowerCase() : '';
    // console.log('_filter -> filterValue', filterValue);
    let rv = new Array<AmsAircraft>()
    if (this.acCharter && this.acCharter.length) {
      rv = this.acCharter.filter(x => (
        x.registration.toLowerCase().indexOf(filterValue) > -1));
    }
    // console.log('_filter -> rv', rv);
    return rv;
  }


  initAircrafts() {
    this.aircraftOpt = of(this.acCharter);

    this.aircraftOpt = this.aircraftF.valueChanges
      .pipe(
        startWith(null),
        map(val => val ? this.filterAircraft(val) :
          (this.acCharter ? this.acCharter.slice() : new Array<AmsAircraft>()))
      );

    let sr: AmsAircraft;

    if (this.acCharter && this.acCharter.length > 0) {
      sr = this.aircraft && this.aircraftF.value ? this.acCharter.find(x => x.acId == this.aircraftF.value.acId) : undefined;
    }

    this.acformGrp.patchValue({ aircraft: sr });

    this.acformGrp.updateValueAndValidity();
  }

  getLastArrTime(): void {
    if (this.aircraft && this.aircraft.acId) {
      this.flpChService.getLastArrTime(this.aircraft.acId)
        .subscribe(val => {
          this.acLastArrTime = val;
        });
    }
  }

  //#region SPM


  spmAirportOpen(airport: AmsAirport) {
    this.flpService.airportPanelOpen.next(airport);
  }
  spmAirlineOpen(ac: AmsAircraft) {
    //console.log('spmAirlineOpen -> ac:', ac);
    this.flpService.spmAircraftOpen.next(ac);
  }

  //#endregion

  //#region Data

  refreshAircrafs() {
    this.spinerService.show();
    this.flpService.loadAircraftForCharter()
      .subscribe(res => {
        this.spinerService.hide();
        this.initFields();
      });
  }

  //#endregion

}
