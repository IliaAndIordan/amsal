import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlpCharterFilterPanelComponent } from './flp-charter-filter-panel.component';

describe('Ams -> Flight Plan ->Charter -> FlpCharterFilterPanelComponent', () => {
  let component: FlpCharterFilterPanelComponent;
  let fixture: ComponentFixture<FlpCharterFilterPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpCharterFilterPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpCharterFilterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
