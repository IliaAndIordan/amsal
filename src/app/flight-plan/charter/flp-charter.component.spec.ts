import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlpCharterComponent } from './flp-charter.component';

describe('Ams -> Flight Plan ->Charter -> FlpCharterComponent', () => {
  let component: FlpCharterComponent;
  let fixture: ComponentFixture<FlpCharterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpCharterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
