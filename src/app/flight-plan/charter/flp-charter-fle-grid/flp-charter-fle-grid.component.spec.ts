import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlpCharterFleGridComponent } from './flp-charter-fle-grid.component';

describe('FlpCharterFleGridComponent', () => {
  let component: FlpCharterFleGridComponent;
  let fixture: ComponentFixture<FlpCharterFleGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpCharterFleGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpCharterFleGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
