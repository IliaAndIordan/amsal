import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsFlight, AmsFlightEstimate, AmsFlightsEstimateTableCriteria, AmsFlightTableCriteria, AmsFlightTableData } from 'src/app/@core/services/api/flight/dto';
import { FlpCharterService } from '../flp-charter.service';

export const KEY_CRITERIA = 'ams_al_flp_charter_fle_criteria';

@Injectable({ providedIn: 'root' })
export class FlpCharterFleGridDataSource extends DataSource<AmsFlightEstimate> {

    seleted: AmsFlightEstimate;

    private _data: Array<AmsFlightEstimate>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsFlightEstimate[]> = new BehaviorSubject<AmsFlightEstimate[]>([]);
    listSubject = new BehaviorSubject<AmsFlightEstimate[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsFlightEstimate> {
        return this._data;
    }

    set data(value: Array<AmsFlightEstimate>) {
        this._data = value;
        this.listSubject.next(this._data as AmsFlightEstimate[]);
    }

    constructor(
        private cus: CurrentUserService,
        private flClient: AmsFlightClient,
        private flpChService: FlpCharterService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsFlightsEstimateTableCriteria>();

    public get criteria(): AmsFlightsEstimateTableCriteria {
        const objjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsFlightsEstimateTableCriteria;
        if (objjStr) {
            obj = AmsFlightsEstimateTableCriteria.fromJSON(JSON.parse(objjStr));
            if (!obj.acId || obj.acId !== this.flpChService.aircraft.acId) {
                obj.acId = this.flpChService.aircraft.acId;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }

            if (!obj.apId || obj.apId !== this.flpChService.acLastArrTime.apId) {
                obj.apId = this.flpChService.acLastArrTime.apId;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }
            if (!obj.depTime || obj.depTime.getTime() < this.flpChService.acLastArrTime.minEtdTime.getTime()) {
                obj.depTime = this.flpChService.acLastArrTime.minEtdTime;
                localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
            }

        } else {
            obj = new AmsFlightsEstimateTableCriteria();

            obj.acId = this.flpChService.aircraft ? this.flpChService.aircraft.acId : undefined;
            obj.apId = this.flpChService.acLastArrTime ? this.flpChService.acLastArrTime.apId : undefined;
            obj.depTime = this.flpChService.acLastArrTime?this.flpChService.acLastArrTime.minEtdTime:undefined;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsFlightsEstimateTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    public setCriteriaNoEvent(obj: AmsFlightsEstimateTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsFlightEstimate[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }

    clearData(): Observable<Array<AmsFlightEstimate>> {
        this.isLoading = true;
        return new Observable<Array<AmsFlightEstimate>>(subscriber => {
            this.itemsCount = 0;
            this.data = new Array<AmsFlightEstimate>();
            this.isLoading = false;
            subscriber.next(this.data);
        });
    }


    loadData(): Observable<Array<AmsFlightEstimate>> {

        this.isLoading = true;

        return new Observable<Array<AmsFlightEstimate>>(subscriber => {

            this.flpChService.estimateCharterFlightsForGrid(this.criteria)
                .subscribe((resp: AmsFlightEstimate[]) => {
                    this.data = resp;
                    this.itemsCount = this.data ? this.data.length : 0;
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsFlightEstimate>();
                    this.isLoading = false;
                    subscriber.next(this.data);
                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
