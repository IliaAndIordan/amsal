import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, ViewChild } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AircraftActionType } from 'src/app/@core/models/pipes/aircraft-statis.pipe';
import { AmsAircraft, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AcLastArrTime, AmsFlight, AmsFlightEstimate, AmsFlightsEstimateTableCriteria, AmsFlightTableCriteria, ResponseAmsFlightEstimateCrete } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { IAmsFlightsEstimateDto } from 'src/app/@share/components/dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { AmsFlightPlanService } from '../../flight-plan.service';
import { FlpCharterFlqGridDataSource } from '../flp-charter-flq-grid/flp-charter-flq-grid.datasource';
import { FlpCharterService } from '../flp-charter.service';
import { FlpCharterFleGridDataSource } from './flp-charter-fle-grid.datasource';

@Component({
    selector: 'ams-flp-charter-fle-grid',
    templateUrl: './flp-charter-fle-grid.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class FlpCharterFleGridComponent implements OnInit {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsFlight>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Output() updateFlightQ1: EventEmitter<AmsAircraft> = new EventEmitter<AmsAircraft>();


    selFlight: AmsFlightEstimate;
    airport: AmsAirport;

    aircraft: AmsAircraft;
    aircraftChanged: Subscription;

    acLastArrTime: AcLastArrTime;
    acLastArrTimeChanged: Subscription;
    minEtdTime: Date;

    criteria: AmsFlightsEstimateTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;
    // 'dtime',
    displayedColumns = ['flTypeId', 'fleId', 'atime', 'payloads', 'distanceKm', 'durationMin', 'income', 'flDescription'];
    airline: AmsAirline;
    depTime: UntypedFormControl;
    depTimeValue: Date;
    hasSpinner: boolean = false;
    errorMessage: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private flClient: AmsFlightClient,
        private flpService: AmsFlightPlanService,
        private flpChService: FlpCharterService,
        private spmMapService: MapLeafletSpmService,
        public tableds: FlpCharterFleGridDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsFlightEstimate>) => {
            this.dataCount = this.tableds.itemsCount;
        });

        this.aircraftChanged = this.flpChService.aircraftChanged.subscribe((ac: AmsAircraft) => {
            //console.log('aircraftChanged -> ac:', ac);
            this.initFields();
        });

        this.acLastArrTimeChanged = this.flpChService.acLastArrTimeChanged.subscribe((value: AcLastArrTime) => {
            this.acLastArrTime = this.flpChService.acLastArrTime;
            this.minEtdTime = this.acLastArrTime ? this.acLastArrTime.minEtdTime : new Date();
            this.initFields();
            if (this.depTime) {
                this.depTime.patchValue(this.minEtdTime);
                this.depTime.updateValueAndValidity();

            }
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsFlightsEstimateTableCriteria) => {
            this.initFields();
        });


        this.initFields();
        this.depTime = new UntypedFormControl(this.minEtdTime, []);
        this.depTime.valueChanges.subscribe(data => {
            //console.log('data; ', data);
            this.depTimeValue = this.depTime && this.depTime.value._d ? this.depTime.value._d : this.depTime.value;
            this.updateCriteria();
        });

        if (!this.acLastArrTime) {
            this.flpChService.getLastArrTime(this.aircraft.acId).subscribe(res => {
                this.initFields();
            })
        }
        //this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.aircraftChanged) { this.aircraftChanged.unsubscribe(); }
        if (this.acLastArrTimeChanged) { this.acLastArrTimeChanged.unsubscribe(); }
    }

    initFields() {
        this.criteria = this.tableds.criteria;
        this.airline = this.cus.airline;
        this.aircraft = this.flpChService.aircraft;
        //console.log('fle grid initFields -> aircraft:', this.aircraft);
        this.acLastArrTime = this.flpChService.acLastArrTime;

        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;


        this.minEtdTime = this.acLastArrTime ? this.acLastArrTime.minEtdTime : new Date();

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
    }

    updateCriteria() {
        this.criteria.acId = this.aircraft ? this.aircraft.acId : undefined;
        this.criteria.apId = this.acLastArrTime ? this.acLastArrTime.apId : undefined;
        //console.log('updateCriteria()-> depTimeValue', this.depTimeValue);

        this.criteria.depTime = this.depTimeValue ? this.depTimeValue : new Date();
        //this.tableds.criteria = this.criteria;
        //console.log('updateCriteria()-> criteria', this.criteria);
        this.tableds.setCriteriaNoEvent(this.criteria);
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.setCriteriaNoEvent(this.criteria);
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.setCriteriaNoEvent(this.criteria);
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.setCriteriaNoEvent(this.criteria);
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    clearData() {
        this.tableds.clearData()
            .subscribe(res => {
                this.initFields();
            });
    }

    reloadData() {
        if (this.aircraft && this.aircraft.canFlight) {
            this.spinerService.display(true);
            this.flpChService.getLastArrTime(this.aircraft.acId).subscribe(res => {
                this.spinerService.display(false);
                this.flpChService.updateFlightQueue.next(this.aircraft);
                this.initFields();
                this.clearData();
            });
        } else{
            this.toastr.info('Airline Flight', 'Operation can not be applied: Charter Flight.');
        }
    }

    addFlifght(value: AmsFlightEstimate): void {
        if (value && value.fleId) {
            this.errorMessage = undefined;
            this.hasSpinner = true;
            this.spinerService.display(true);
            this.flClient.flightCharterEstimateCreate(value.fleId)
                .subscribe((res: ResponseAmsFlightEstimateCrete) => {
                    this.spinerService.display(false);
                    this.hasSpinner = false;
                    //console.log('flightCharterEstimateCreate -> res:', res);
                    if (res && res.success && res.data &&
                        res.data.flId && res.data.flId > -1) {
                        this.errorMessage = undefined;
                        this.toastr.success('Airline Flight', 'Operation Succesfull: Flight registered ' + res.data.flId);
                        this.reloadData();
                    } else {
                        this.errorMessage = 'Flight create Failed. Uncnown error';
                        this.toastr.error('Airline Flight', this.errorMessage);
                        setTimeout((router: Router,) => {
                            this.errorMessage = undefined;
                            this.hasSpinner = false;
                            this.reloadData();
                        }, 2000);
                    }
                }, err => {
                    // this.spinerService.display(false);
                    console.error('Observer got an error: ' + err);
                    this.errorMessage = 'Flight create Failed. ' + err;
                    this.toastr.error('Airline Flight', this.errorMessage);
                    setTimeout((router: Router,) => {
                        this.errorMessage = undefined;
                        this.hasSpinner = false;
                        this.reloadData();
                    }, 2000);
                    // this.spinerService.display(false);
                    // this.toastr.error('Compnay', 'Operation Failed: Compnay ' + message);
                });

        }
    }

    //#endregion

    //#region Actions

    gotoflightt(flight: AmsFlight) {
        console.log('gotoflightt -> flight:' + flight);
    }

    gotoAircraft(aicraft: AmsAircraft) {
        console.log('gotoAircraft -> aicraft:' + aicraft);
    }


    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if(airport && airport.apId){
            this.cus.spmAirportPanelOpen.next(airport.apId);
          }
    }

    spmAircraftOpen(value: AmsAircraft) {
        //console.log('spmAircraftOpen -> aicraft:', value);
        if (value) {
            this.flpService.spmAircraftOpen.next(value);
        }
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);
        if (data) {
            //this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    spmFleCharterOpen(value: AmsFlightEstimate) {
        console.log('spmFleCharterOpen-> value:', value);
        if (value) {
            this.flpService.spmFleCharterOpen.next(value);
        }
    }

    spmMapOpen() {
        let flights = new Array<AmsFlight>();
        if(this.tableds.data && this.tableds.data.length>0){
            this.tableds.data.forEach(element => {
                flights.push(element.toFlight());
            });
        }
        this.spmMapService.flights = flights;
        this.spmMapService.spmMapPanelOpen.next(true);
    }

    //#endregion

    //#region Data

    /*
    gotoAirport(data: AmsAirport) {
        
  
        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }
  
    }*/

    //#endregion


}
