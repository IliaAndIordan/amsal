import { TestBed } from '@angular/core/testing';

import { FlpCharterService } from './flp-charter.service';

describe('FlpCharterService', () => {
  let service: FlpCharterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FlpCharterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
