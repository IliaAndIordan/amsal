import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from '../../flight-plan.service';
import { FlpCharterService } from '../flp-charter.service';

@Component({
  templateUrl: './flp-charter-tebs-home.component.html',
})
export class FlpCharterTebsHomeComponent implements OnInit, OnDestroy {

  selTabIdx: number;
  tabIdxChanged: Subscription;

  constructor(
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private spinerService: SpinnerService,
    private flpService: AmsFlightPlanService,
    private flpChService: FlpCharterService) {

  }

  ngOnInit(): void {
    this.tabIdxChanged = this.flpChService.tabIdxChanged.subscribe(tabIdx => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });



    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

  }

  initFields() {
    this.selTabIdx = this.flpChService.tabIdx;
  }

  //#region  Tab Panel Actions

  selectedTabChanged(tabIdx: number) {
    const oldTab = this.selTabIdx;
    this.selTabIdx = tabIdx;
    this.flpChService.tabIdx = this.selTabIdx;
  }


  //#endregion

}
