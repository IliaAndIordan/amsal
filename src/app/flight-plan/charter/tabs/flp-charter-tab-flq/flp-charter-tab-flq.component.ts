import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AcLastArrTime } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from 'src/app/flight-plan/flight-plan.service';
import { FlpCharterService } from '../../flp-charter.service';

@Component({
  selector: 'ams-flp-charter-tab-flq',
  templateUrl: './flp-charter-tab-flq.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FlpCharterTabFlqComponent implements OnInit {

  @Input() tabindex: string;

  aircraft: AmsAircraft;
  aircraftChanged:Subscription;
  updateFlightQueue:Subscription;

  selTabIdx: number;
  tabIdxChanged: Subscription;

  acLastArrTimeChanged: Subscription;
  acLastArrTime: AcLastArrTime;

  get tabIdx(): number {
    return this.tabindex?parseInt(this.tabindex):undefined;
  }

  constructor(
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private spinerService: SpinnerService,
    private flpService: AmsFlightPlanService,
    private flpChService: FlpCharterService) { }

  ngOnInit(): void {
    this.tabIdxChanged = this.flpChService.tabIdxChanged.subscribe(tabIdx => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });

    this.acLastArrTimeChanged = this.flpChService.acLastArrTimeChanged.subscribe(val => {
      this.initFields();
    });

    this.aircraftChanged = this.flpChService.aircraftChanged.subscribe(val => {
      this.initFields();
    });

    this.updateFlightQueue = this.flpChService.updateFlightQueue.subscribe(ac => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });

    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
    if (this.acLastArrTimeChanged) { this.acLastArrTimeChanged.unsubscribe(); }
    if (this.aircraftChanged) { this.tabIdxChanged.unsubscribe(); }

  }

  initFields() {
    this.selTabIdx = this.flpChService.tabIdx;
    this.aircraft = this.flpChService.aircraft;
    this.acLastArrTime = this.flpChService.acLastArrTime;
  }


}
