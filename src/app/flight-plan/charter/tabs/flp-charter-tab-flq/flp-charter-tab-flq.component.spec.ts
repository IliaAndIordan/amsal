import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlpCharterTabFlqComponent } from './flp-charter-tab-flq.component';

describe('FlpCharterTabFlqComponent', () => {
  let component: FlpCharterTabFlqComponent;
  let fixture: ComponentFixture<FlpCharterTabFlqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpCharterTabFlqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpCharterTabFlqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
