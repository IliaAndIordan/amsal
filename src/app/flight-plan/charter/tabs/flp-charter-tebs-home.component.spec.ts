import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlpCharterTebsHomeComponent } from './flp-charter-tebs-home.component';

describe('Ams -> Flight Plan ->Charter -> FlpCharterTebsHomeComponent', () => {
  let component: FlpCharterTebsHomeComponent;
  let fixture: ComponentFixture<FlpCharterTebsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpCharterTebsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpCharterTebsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
