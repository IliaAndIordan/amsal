import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Animate } from 'src/app/@core/const/animation.const';
import { URL_COMMON_IMAGE_TILE, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCommTransactionService } from 'src/app/@core/services/common/ams-comm-transaction.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from '../flight-plan.service';
import { FlpCharterService } from './flp-charter.service';

@Component({
  templateUrl: './flp-charter.component.html',
})
export class FlpCharterComponent implements OnInit {

  tileImgRootUrl = URL_COMMON_IMAGE_TILE;
  acCharterImg = URL_COMMON_IMAGE_AMS_COMMON + 'charter_01.png';

  in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  acCharter: AmsAircraft[];
  acCharterCount:number;
  acCharterCountSubj = new BehaviorSubject<number>(0);
  acCharterChanged: Subscription;

  acCharterCount$ = this.acCharterCountSubj.asObservable();

  get panelIn(): boolean {
    let rv = this.flpChService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.flpChService.panelIn;
  }

  set panelIn(value: boolean) {
    this.flpChService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    public dialog: MatDialog,
    private cus: CurrentUserService,
    private trService: AmsCommTransactionService,
    private flpService: AmsFlightPlanService,
    private flpChService:FlpCharterService,) { }
 

  ngOnInit(): void {
    this.acCharterChanged = this.flpService.aircraftForCharterChanged.subscribe(acCharter => {
      //console.log('aircraftForCharterChanged -> acCharter:', acCharter);
      this.spinerService.show();
      this.initFields();
      this.acCharterCountSubj.next(this.acCharterCount);
      this.spinerService.hide();
    });
    this.panelInChanged = this.flpChService.panelInChanged.subscribe((panelIn: boolean) => {
      this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    });

  }

  ngOnDestroy(): void {
    if (this.acCharterChanged) { this.acCharterChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields() {
    this.acCharter = this.flpService.aircraftForCharter;
    this.acCharterCount = this.acCharter?this.acCharter.length:0;
    //console.log('aircraftForCharterChanged -> acCharterCount:', this.acCharterCount);
  }

//#region Filter Panel

public closePanelLeftClick(): void {
  this.panelIn = false;
}

public expandPanelLeftClick(): void {
  // this.treeComponent.openPanelClick();
  this.panelIn = true;
}

//#endregion

}
