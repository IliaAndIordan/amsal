import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlpCharterFlqGridComponent } from './flp-charter-flq-grid.component';


describe('Ams -> Flight Plan ->Charter -> FlpCharterFlqGridComponent', () => {
  let component: FlpCharterFlqGridComponent;
  let fixture: ComponentFixture<FlpCharterFlqGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpCharterFlqGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpCharterFlqGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
