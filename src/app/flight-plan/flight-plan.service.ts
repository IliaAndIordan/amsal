import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AmsAircraftClient } from '../@core/services/api/aircraft/api-client';
import { AmsAircraft, AmsAircraftTableCriteria, ResponseAmsAircraftTable } from '../@core/services/api/aircraft/dto';
import { AmsAirportCacheService } from '../@core/services/api/airport/ams-airport-cache.service';
import { AmsAirportClient } from '../@core/services/api/airport/api-client';
import { AmsAirport } from '../@core/services/api/airport/dto';
import { AmsFlightClient } from '../@core/services/api/flight/ams-flight.client';
import { AcLastArrTime, AmsFlight, AmsFlightEstimate } from '../@core/services/api/flight/dto';
import { CurrentUserService } from '../@core/services/auth/current-user.service';

@Injectable({
  providedIn: 'root'
})
export class AmsFlightPlanService {

  airportPanelOpen = new BehaviorSubject<AmsAirport>(undefined);
  spmFleCharterOpen = new BehaviorSubject<AmsFlightEstimate>(undefined);
  spmAircraftOpen = new BehaviorSubject<AmsAircraft>(undefined);
  spmFlqOpen = new BehaviorSubject<AmsFlight>(undefined);
  
  constructor(
    private cus: CurrentUserService,
    private flClient: AmsFlightClient,
    private apCache: AmsAirportCacheService,
    private apClient: AmsAirportClient,
    private acClient: AmsAircraftClient,) {

  }

  //#region Aircraft for Charter List

  aircraftForCharter: AmsAircraft[];
  aircraftForCharterChanged = new Subject<AmsAircraft[]>();

  aircraftForCharterFlpCriteria(): AmsAircraftTableCriteria {
    let rv = new AmsAircraftTableCriteria();
    rv.forSheduleFlights = false;
    rv.ownerAlId = this.cus.airline.alId;
    rv.sortCol = 'registration';
    rv.sortDesc = false;
    rv.limit = 1000;
    rv.offset = 0;
    return rv;
  };

  loadAircraftForCharter(): Observable<Array<AmsAircraft>> {

    return new Observable<Array<AmsAircraft>>(subscriber => {
      const criteria = this.aircraftForCharterFlpCriteria();
      this.acClient.aircraftTableP(criteria)
        .then((res: ResponseAmsAircraftTable) => {
          const resp = Object.assign(new ResponseAmsAircraftTable(), res);
          //console.log('loadAircraftForCharter-> resp=', resp);
          let data = new Array<AmsAircraft>();

          if (resp && resp.success) {
            if (resp && resp.data &&
              resp.data.aircrafts && resp.data.aircrafts.length > 0) {
              resp.data.aircrafts.forEach(iu => {
                const obj = AmsAircraft.fromJSON(iu);
                //console.log('loadAircraftForCharter-> obj=', obj);
                data.push(obj);
              });
            }

            this.aircraftForCharter = data;
            this.aircraftForCharterChanged.next(this.aircraftForCharter);
            subscriber.next(this.aircraftForCharter);
          }
        }, msg => {
          console.log('loadAircraftForCharter -> ' + msg);
          this.aircraftForCharter = new Array<AmsAircraft>();
          this.aircraftForCharterChanged.next(this.aircraftForCharter);
        });

    });

  }

  //#endregion

  //#region Aircraft



  //#region 

  //#region Airports

  solrAirport(apId: number): Observable<AmsAirport> {

    return new Observable<AmsAirport>(subscriber => {

      this.apClient.geSolrAirport(apId)
        .subscribe((resp: AmsAirport) => {
          console.log('geSolrAirport-> resp', resp);
          subscriber.next(resp);
        },
          err => {

            throw err;
          });
    });

  }
  //#endregion

}
