import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsFlightPlanTransfer, AmsFlightPlanTransferTableCriteria, AmsFlightPlanTransferTableData } from 'src/app/@core/services/api/airline/al-flp-shedule';

export const KEY_CRITERIA = 'ams_al_flp_transfer_triple_table_criteria-v01';

@Injectable({ providedIn: 'root' })
export class  AmsFlightPlanTransferTableDataSource extends DataSource<AmsFlightPlanTransfer> {

    seleted: AmsFlightPlanTransfer;

    private _data: Array<AmsFlightPlanTransfer>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange = new Subject<AmsFlightPlanTransfer[]>();
    listSubject = new BehaviorSubject<AmsFlightPlanTransfer[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsFlightPlanTransfer> {
        return this._data;
    }

    set data(value: Array<AmsFlightPlanTransfer>) {
        this._data = value;
        this.listSubject.next(this._data as AmsFlightPlanTransfer[]);
    }

    constructor(
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsFlightPlanTransferTableCriteria>();

    public get criteria(): AmsFlightPlanTransferTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsFlightPlanTransferTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsFlightPlanTransferTableCriteria(), JSON.parse(onjStr));

        } else {
            obj = new AmsFlightPlanTransferTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = undefined;
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsFlightPlanTransferTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsFlightPlanTransfer[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsFlightPlanTransfer>> {

        this.isLoading = true;

        return new Observable<Array<AmsFlightPlanTransfer>>(subscriber => {

            this.alClient.getTripleTransferP(this.criteria)
                .then((resp: AmsFlightPlanTransferTableData) => {
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsFlightPlanTransfer>();
                    if (resp) {
                        if (resp && resp.routes && resp.routes.length > 0) {
                            this.data = resp.routes;
                        }
                        this.itemsCount = resp.rowsCount ? resp.rowsCount : 0;
                    } else{
                        this.itemsCount = this.data?this.data.length:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);

                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsFlightPlanTransfer>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
