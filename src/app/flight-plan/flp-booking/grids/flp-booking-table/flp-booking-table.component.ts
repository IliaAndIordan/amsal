import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, ViewChild, OnDestroy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAirlineClient } from 'src/app/@core/services/api/airline/api-client';
import { AmsAirlineHubsCacheService } from 'src/app/@core/services/api/airline/al-hub-cache.service';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAlFlightNumber, AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleTableCriteria } from 'src/app/@core/services/api/airline/al-flp-number';
import { AmsPayloadType } from 'src/app/@core/services/api/flight/enums';
import { AmsFlightPlanPayload, AmsFlightPlanPayloadTableCriteria, AmsFlightPlanStatusSave, AmsSheduleGroup } from 'src/app/@core/services/api/airline/al-flp-shedule';
import { AmsWeekday, FlightPlanStatus } from 'src/app/@core/services/api/airline/enums';

import { AmsAlFlpBookingableDataSource } from './flp-booking-table.datasource';

@Component({
    selector: 'ams-al-flp-booking-table',
    templateUrl: './flp-booking-table.component.html',
    changeDetection: ChangeDetectionStrategy.Default
})
export class AmsAlFlightBookingTableComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsFlightPlanPayload>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Input() tabindex: number;
    @Output() spmAlFlightNumberOpen: EventEmitter<AmsAlFlightNumber> = new EventEmitter<AmsAlFlightNumber>();
    @Output() spmAlFlightNumberScheduleOpen: EventEmitter<AmsAlFlightNumberSchedule> = new EventEmitter<AmsAlFlightNumberSchedule>();

    typeE = AmsPayloadType.E;
    typeB = AmsPayloadType.B;
    typeF = AmsPayloadType.F;
    typeC = AmsPayloadType.C;
    typeG = AmsPayloadType.G;

    soldOut = FlightPlanStatus.FlightIsSoldOut;


    selected: AmsFlightPlanPayload;

    criteria: AmsFlightPlanPayloadTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;

    group: AmsSheduleGroup;
    groupChanged: Subscription;
    weekday: AmsWeekday;
    weekdayChanged: Subscription;

    displayedColumns = ['flpId', 'flName', 'acId', 'dtime', 'dApId', 'atime', 'aApId', 'pax', 'paxPctFull', 'price'];
    airline: AmsAirline;
    canEdit: boolean = false;
    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private alClient: AmsAirlineClient,
        private hubCache: AmsAirlineHubsCacheService,
        private apCache: AmsAirportCacheService,
        public tableds: AmsAlFlpBookingableDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsFlightPlanPayload>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsFlightPlanPayloadTableCriteria) => {
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.groupChanged) { this.groupChanged.unsubscribe(); }
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.weekdayChanged) { this.weekdayChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        //this.group = this.mapService.group;
        //this.weekday = this.mapService.weekday;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;
        this.criteria.alId = this.airline ? this.airline.alId : undefined;
        this.criteria.grpId = this.group ? this.group.grpId : undefined;
        //console.log('spmFlOpenClick -> dataCount:', this.dataCount);
        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        this.criteria.alId = this.cus.isAdmin ? undefined : this.airline?.alId;
        this.canEdit = (this.group && this.group.isActive) ? false : true;
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 500);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = 'adate';
        this.criteria.sortDesc = true;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }

    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage(): void {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick(): void {
        this.loadPage();
    }

    rowClicked(value: AmsFlightPlanPayload): void {
        console.log('rowClicked -> value:', value);
        this.selected = value;
    }

    updateFlightPlanStatusClick(value: AmsFlightPlanPayload): void {
        if (value && value.flpId) {
            const req: AmsFlightPlanStatusSave = {
                flpId: value.flpId,
                flpStatusId: FlightPlanStatus.AvailableForTicketSell,
                payloadJobs: 1
            };
            this.alClient.alFlpStatusSave(req).then(res => {
                this.loadPage();
            });
        }
    }

    spmFlqOpenClick(value: AmsFlightPlanPayload): void {
        console.log('spmFlqOpenClick -> value:', value);
        this.cus.spmFlpPanelOpen.next(value);
    }


    spmAlFlightNumberOpenClick(value: AmsFlightPlanPayload) {
        console.log('spmAlFlightNumberOpenClick -> value:', value);
        this.selected = value;
        if (value && value.flpnsId) {
            //this.cus.spmAirportPanelOpen.next(value.apId);
        }
    }

    spmAlFlightNumberScheduleOpenClick(value: AmsFlightPlanPayload) {
        console.log('spmAlFlightNumberScheduleOpenClick -> value:', value);
        this.selected = value;
        if (value && value.flpnsId) {
            //this.cus.spmAirportPanelOpen.next(value.apId);
        }
    }

    //#endregion

}
