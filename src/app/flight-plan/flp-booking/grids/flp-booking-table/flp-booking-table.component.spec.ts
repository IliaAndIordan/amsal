import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AmsAlFlightBookingTableComponent } from './flp-booking-table.component';


describe('AmsAlFlightBookingTableComponent', () => {
  let component: AmsAlFlightBookingTableComponent;
  let fixture: ComponentFixture<AmsAlFlightBookingTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmsAlFlightBookingTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsAlFlightBookingTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
