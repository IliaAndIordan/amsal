import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from '../../flight-plan.service';
import { FlpBookingService } from '../flp-booking.service';

@Component({
  templateUrl: './flp-booking-tabs-home.component.html',
})
export class FlpBookingTabsHomeComponent implements OnInit, OnDestroy {

  selTabIdx: number;
  tabIdxChanged: Subscription;



  constructor(
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private spinerService: SpinnerService,
    private flpService: AmsFlightPlanService,
    private flpShService: FlpBookingService) {

  }

  ngOnInit(): void {
    this.tabIdxChanged = this.flpShService.tabIdxChanged.subscribe(tabIdx => {
      this.initFields();
      if (this.selTabIdx === 0) {
        
      }
    });

    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

  }

  initFields() {
    this.selTabIdx = this.flpShService.tabIdx;
  }

  //#region  Tab Panel Actions

  selectedTabChanged(tabIdx: number) {
    const oldTab = this.selTabIdx;
    this.selTabIdx = tabIdx;
    this.flpShService.tabIdx = this.selTabIdx;
  }


  //#endregion

}
