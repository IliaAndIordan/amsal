import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlpBookingTabsHomeComponent } from './flp-booking-tabs-home.component';

describe('Ams -> Flight Plan ->Shedule -> FlpBookingTabsHomeComponent', () => {
  let component: FlpBookingTabsHomeComponent;
  let fixture: ComponentFixture<FlpBookingTabsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpBookingTabsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpBookingTabsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
