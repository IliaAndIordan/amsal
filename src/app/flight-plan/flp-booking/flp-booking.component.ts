import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsFlightPlanService } from '../flight-plan.service';
import { FlpBookingService } from './flp-booking.service';
import { FlpBookingFilterPanelComponent } from './filter-panel/flp-booking-filter-panel.component';

@Component({
  templateUrl: './flp-booking.component.html',
})
export class FlpBookingHomeComponent  implements OnInit {

  @ViewChild('filterPanel') filterPanel: FlpBookingFilterPanelComponent;

  in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  get panelIn(): boolean {
    let rv = this.flpBookingService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.flpBookingService.panelIn;
  }

  set panelIn(value: boolean) {
    this.flpBookingService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    private spinerService: SpinnerService,
    private flpService: AmsFlightPlanService,
    private flpBookingService: FlpBookingService) {
  }

  ngOnInit(): void {
    
    this.panelInChanged = this.flpBookingService.panelInChanged.subscribe((panelIn: boolean) => {
      this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    });

  }

  //#region Filter Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  refreshPanelLeft():void{
    if(this.filterPanel){
      this.filterPanel.loadPage();
    }
  }

  //#endregion
}
