import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlpBookingFilterPanelComponent } from './flp-booking-filter-panel.component';


describe('Ams -> Flight Plan ->Charter -> FlpBookingFilterPanelComponent', () => {
  let component: FlpBookingFilterPanelComponent;
  let fixture: ComponentFixture<FlpBookingFilterPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlpBookingFilterPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlpBookingFilterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
