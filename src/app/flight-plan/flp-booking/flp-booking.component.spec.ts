import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlpBookingHomeComponent } from './flp-booking.component';


describe('FlpBookingHomeComponent', () => {
  let component: FlpBookingHomeComponent;
  let fixture: ComponentFixture<FlpBookingHomeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FlpBookingHomeComponent]
    });
    fixture = TestBed.createComponent(FlpBookingHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
