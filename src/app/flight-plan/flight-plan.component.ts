import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatDrawerMode } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AmsAircraft } from '../@core/services/api/aircraft/dto';
import { AmsAirline } from '../@core/services/api/airline/dto';
import { AmsFlight, AmsFlightEstimate } from '../@core/services/api/flight/dto';
import { UserModel } from '../@core/services/auth/api/dto';
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { AmsCommTransactionService } from '../@core/services/common/ams-comm-transaction.service';
import { SpinnerService } from '../@core/services/spinner.service';
import { AmsSidePanelModalComponent } from '../@share/components/common/side-panel-modal/side-panel-modal';
import { SxSidebarNavComponent } from '../@share/components/common/sidebar/nav/sx-sidebar-nav.component';
import { IInfoMessage, InfoMessageDialog } from '../@share/components/dialogs/info-message/info-message.dialog';
import { AirlineService } from '../airline/airline.service';
import { AmsFlightPlanService } from './flight-plan.service';

@Component({
  selector: 'ams-flight-plan',
  templateUrl: './flight-plan.component.html',
})
export class FlightPlanComponent implements OnInit, OnDestroy {

  @ViewChild('spmAircraft') spmAircraft: AmsSidePanelModalComponent;
  @ViewChild('spmFleCharter') spmFleCharter: AmsSidePanelModalComponent;
  @ViewChild('spmFlq') spmFlq: AmsSidePanelModalComponent;
  @ViewChild('sxSide') sxSide: SxSidebarNavComponent;
  @ViewChild('spmMap') spmMap: AmsSidePanelModalComponent;

  snavmode: MatDrawerMode = 'side';
  spmFleCharterChange:Subscription;
  sidebarClass = 'sidebar-closed';


  airline: AmsAirline;

  user: UserModel;
  userChanged: Subscription;

  acCharter: AmsAircraft[];
  acCharterCount: number;

  spmAircrafPanelOpen: Subscription;
  spmFlqPanelOpen: Subscription;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    public dialog: MatDialog,
    private cus: CurrentUserService,
    private alService: AirlineService,
    private trService: AmsCommTransactionService,
    private flpService: AmsFlightPlanService) { }

  ngOnInit(): void {
    this.userChanged = this.cus.userChanged.subscribe(user => {
      this.initFields();
    });

    this.spmAircrafPanelOpen = this.flpService.spmAircraftOpen.subscribe((ac: AmsAircraft) => {
      this.spmAircraftOpen(ac);
    });

    this.spmFleCharterChange = this.flpService.spmFleCharterOpen.subscribe(obj =>{
      this.spmFleCharterOpen(obj);
    });

    this.spmFlqPanelOpen = this.flpService.spmFlqOpen.subscribe(obj =>{
      this.spmFlqOpen(obj);
    });
    
    this.initFields();
    this.loadAircraftForCharter();
  }

  ngOnDestroy(): void {
    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.spmAircrafPanelOpen) { this.spmAircrafPanelOpen.unsubscribe(); }
    if (this.spmFleCharterChange) { this.spmFleCharterChange.unsubscribe(); }
  }

  initFields() {
    this.user = this.cus.user;
    this.airline = this.cus.airline;
    this.acCharter = this.flpService.aircraftForCharter;
    this.acCharterCount = this.acCharter ? this.acCharter.length : 0;
  }

  //#region Sidebar

  showMessage(data: IInfoMessage) {
    console.log('showMessage-> data:', data);

    const dialogRef = this.dialog.open(InfoMessageDialog, {
      width: '721px',
      height: '320px',
      data: data
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log('showMessage-> res:', res);
      if (res) {

      }
    });

  }

  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }

  //#endregion

  
  //#region  Expand/Close Menu


  snavToggleClick() {
    console.log('snavToggleClick-> sxSide', this.sxSide);
    if(this.sxSide){
      this.sxSide.toggle();
    }
  }

  //#endregion

  //#region SPM

  spmFlqObj: AmsFlight;
  spmFlqClose(): void {
    if (this.spmFlq) {
      this.spmFlq.closePanel();
    }
  }
  
  spmFlqOpen(ac: AmsFlight) {
    this.spmFlqObj = ac;
    if (this.spmFlq && this.spmFlqObj) {
      this.spmFlq.expandPanel();
    }
    else {
      this.spmFlqClose();
    }
  }

  spmAircraftObj: AmsAircraft;
  spmAircraftClose(): void {
    if (this.spmAircraft) {
      this.spmAircraft.closePanel();
    }
  }
  
  spmAircraftOpen(ac: AmsAircraft) {
    //console.log('spmAircraftOpen-> ac:', ac); 
    this.spmAircraftObj = ac;
    if (this.spmAircraft && this.spmAircraft) {
      this.spmAircraft.expandPanel();
    }
    else {
      this.spmAircraftClose();
    }
  }

  spmFleCharterObj: AmsFlightEstimate;
  spmFleCharterClose(): void {
    if (this.spmFleCharter) {
      this.spmFleCharter.closePanel();
    }
  }

  spmFleCharterOpen(value: AmsFlightEstimate) {
    this.spmFleCharterObj = value;
    if (this.spmFleCharter && this.spmFleCharterObj) {
      this.spmFleCharter.expandPanel();
    }
    else {
      this.spmFleCharterClose();
    }
  }



  //#endregion

  //#region Data

  loadAircraftForCharter() {
    this.flpService.loadAircraftForCharter()
      .subscribe(res => {
        this.initFields();
      });
  }

  solrAirport(): void {
    if (this.airline && this.airline.apId) {

      this.flpService.solrAirport(this.airline.apId)
        .subscribe((res: any) => {
          console.log('solrAirport -> responce:', res);
        });
    }
  }
  //#endregion
}
