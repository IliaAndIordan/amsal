import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlightPlanRoutingModule, routableComponents } from './flight-plan-routing.module';
import { SxShareModule } from '../@share/@share.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { AmsFlightPlanService } from './flight-plan.service';
import { AmsFlightPlanMenuMainComponent } from './menu-main/ams-flight-plan-menu-main.component';
import { FlpCharterFilterPanelComponent } from './charter/filter-panel/flp-charter-filter-panel.component';
import { FlpCharterService } from './charter/flp-charter.service';
import { FlpCharterTabFlqComponent } from './charter/tabs/flp-charter-tab-flq/flp-charter-tab-flq.component';
import { FlpCharterFlqGridComponent } from './charter/flp-charter-flq-grid/flp-charter-flq-grid.component';
import { FlpCharterFlqGridDataSource } from './charter/flp-charter-flq-grid/flp-charter-flq-grid.datasource';
import { FlpCharterFleGridComponent } from './charter/flp-charter-fle-grid/flp-charter-fle-grid.component';
import { FlpCharterFleGridDataSource } from './charter/flp-charter-fle-grid/flp-charter-fle-grid.datasource';
import { FlpSheduleService } from './flp-shedule/flp-shedule.service';
import { FlpSheduleFilterPanelComponent } from './flp-shedule/filter-panel/flp-shedule-filter-panel.component';
import { AmsFlpSheduleGroupTableDataSource } from './flp-shedule/grids/flp-shedule-group-table/flp-shedule-group-table.datasource';
import { AmsFlpSheduleGroupTableComponent } from './flp-shedule/grids/flp-shedule-group-table/flp-shedule-group-table.component';
import { AmsFlpSheduleRouteTableDataSource } from './flp-shedule/grids/flp-shedule-route-table/flp-shedule-route-table.datasource';
import { AmsFlpSheduleRouteTableComponent } from './flp-shedule/grids/flp-shedule-route-table/flp-shedule-route-table.component';
import { AmsAlFlightNumberTableComponent } from './flp-shedule/grids/flp-number-table/flp-number-table.component';
import { AmsAlFlpNumberTableDataSource } from './flp-shedule/grids/flp-number-table/flp-number-table.datasource';
import { AmsAlFlightNumberScheduleTableComponent } from './flp-shedule/grids/flpn-schedule-table/flpn-schedule-table.component';
import { AmsAlFlpNumberScheduleTableDataSource } from './flp-shedule/grids/flpn-schedule-table/flpn-schedule-table.datasource';
import { FlpBookingService } from './flp-booking/flp-booking.service';
import { FlpBookingFilterPanelComponent } from './flp-booking/filter-panel/flp-booking-filter-panel.component';
import { AmsAlFlightBookingTableComponent } from './flp-booking/grids/flp-booking-table/flp-booking-table.component';
import { AmsAlFlpnScheduleChartComponent } from './flp-shedule/grids/flpn-schedule-chart/flpn-schedule-chart.component';
import { AmsFlightPlanTransferTableDataSource } from './flp-booking/grids/flp-transfer-triple-table/flp-transfer-triple-table.datasource';
import { AmsFlightPlanTransferTableComponent } from './flp-booking/grids/flp-transfer-triple-table/flp-transfer-triple-table.component';
import { AmsFlightPlanTransferPpdTableDataSource } from './flp-booking/grids/flp-transfer-ppd-table/flp-transfer-ppd-table.datasource';
import { AmsFlightPlanTransferPpdTableComponent } from './flp-booking/grids/flp-transfer-ppd-table/flp-transfer-ppd-table.component';
import { AmsFlightPlanTransferCpdTableDataSource } from './flp-booking/grids/flp-transfer-cpd-table/flp-transfer-cpd-table.datasource';
import { AmsFlightPlanTransferCpdTableComponent } from './flp-booking/grids/flp-transfer-cpd-table/flp-transfer-cpd-table.component';


@NgModule({
 
  imports: [
    CommonModule,
    SxShareModule,
    SxMaterialModule,
    SxPipesModule,
   
    //
    FlightPlanRoutingModule,
  ],
  declarations: [
    routableComponents,
    AmsFlightPlanMenuMainComponent,
    FlpCharterFilterPanelComponent,
    FlpCharterTabFlqComponent,
    FlpCharterFlqGridComponent,
    FlpCharterFleGridComponent,
    FlpSheduleFilterPanelComponent,
    AmsFlpSheduleGroupTableComponent,
    AmsFlpSheduleRouteTableComponent,
    AmsAlFlightNumberTableComponent,
    AmsAlFlightNumberScheduleTableComponent,
    FlpBookingFilterPanelComponent,
    AmsAlFlightBookingTableComponent,
    AmsAlFlpnScheduleChartComponent,
    AmsFlightPlanTransferTableComponent,
    AmsFlightPlanTransferPpdTableComponent,
    AmsFlightPlanTransferCpdTableComponent,
  ],
  providers:[
    AmsFlightPlanService,
    FlpCharterService,
    FlpCharterFlqGridDataSource,
    FlpCharterFleGridDataSource,
    FlpSheduleService,
    AmsFlpSheduleGroupTableDataSource,
    AmsFlpSheduleRouteTableDataSource,
    AmsAlFlpNumberTableDataSource,
    AmsAlFlpNumberScheduleTableDataSource,
    FlpBookingService,
    AmsFlightPlanTransferTableDataSource,
    AmsFlightPlanTransferPpdTableDataSource,
    AmsFlightPlanTransferCpdTableDataSource,
  ]
})
export class FlightPlanModule { }
