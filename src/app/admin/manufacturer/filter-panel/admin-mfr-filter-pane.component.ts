import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from '../../../wad/wad.service';
// ---Models
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAdminService } from '../../admin.service';
import { AmsAdminManufacturerService } from '../admin-mfr.service';
import { AmsManufacturerEditDialog } from 'src/app/@share/components/dialogs/mfr-edit/mfr-edit.dialog';
import { AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMfrAirportEditDialog } from 'src/app/@share/components/dialogs/mfr-airport-edit/mfr-airport-edit.dialog';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';


@Component({
    selector: 'ams-admin-mfr-filter-panel-body',
    templateUrl: './admin-mfr-filter-pane.component.html',
})
export class AmsAdminMfrFilterPanelBodyComponent implements OnInit, OnDestroy {
   
    roots = AppRoutes;

    region: AmsRegion;
    subregion: AmsSubregion;
    country: AmsCountry;
    state: AmsState;
    airport: AmsAirport;

    manufacturer: AmsManufacturer;
    manufacturerChanged: Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;
    ourApUrl: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private apCache: AmsAirportCacheService,
        private adminService: AmsAdminService,
        private mfrService: AmsAdminManufacturerService) {

    }


    ngOnInit(): void {

        this.manufacturerChanged = this.adminService.manufacturerChanged.subscribe((mfr: AmsManufacturer) => {
            console.log('manufacturerChanged -> mfr', mfr);
            this.initFields();
        });

        this.tabIdxChanged = this.mfrService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.manufacturerChanged) { this.manufacturerChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.tabIdx = this.mfrService.tabIdx;
        this.manufacturer = this.mfrService.manufacturer;
        this.ourApUrl = WEB_OURAP_REGION + (this.state && this.state.iso ? this.state.iso.split('-').join('/') + '/' : '');
        if(this.manufacturer && this.manufacturer.apId){
            this.apCache.getAirport(this.manufacturer.apId).subscribe(ap => {
                this.airport = ap;
              });
        }
    }

    //#region Dialogs

    ediManufacturer() {
        //console.log('ediManufacturer-> mfr:', this.manufacturer);
        if (this.manufacturer) {


            const dialogRef = this.dialogService.open(AmsManufacturerEditDialog, {
                width: '721px',
                //height: '620px',
                data: {
                    mfr: this.manufacturer,
                    airport: this.airport,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediManufacturer-> res:', res);
                if (res) {
                    this.adminService.manufacturer = res;
                }
                this.initFields();
            });
        }
    }

    ediMfrAirport() {
        //console.log('ediMfrAirport-> mfr:', this.manufacturer);
        if (this.manufacturer) {
            const dialogRef = this.dialogService.open(AmsMfrAirportEditDialog, {
                width: '721px',
                //height: '420px',
                data: {
                    mfr: this.manufacturer,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediMfrAirport-> res:', res);
                if (res) {
                    this.adminService.manufacturer = res;
                    const idx = this.adminService.manufacturers.findIndex(x => x.mfrId === res.mfr);
                    if(idx>-1){
                        this.adminService.manufacturers[idx]=res;
                    }
                    
                }
                this.initFields();
            });
        }
    }
    //#endregion

    //#region SPM

    spmManufacturerOpen(){

    }
    
    spmRegionOpen() {
        //this.wadService.regionPanelOpen.next(true);
    }

    gotoRegion() {
        this.router.navigate([AppRoutes.wad, AppRoutes.region]);
    }

    //#endregion

}
