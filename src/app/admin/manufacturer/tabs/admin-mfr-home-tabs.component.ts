import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAdminService } from '../../admin.service';
import { AmsAdminManufacturerService } from '../admin-mfr.service';
import { AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';


@Component({
    selector: 'ams-admin-mfr-home-tabs',
    templateUrl: './admin-mfr-home-tabs.component.html',
})
export class AmsAdminManufacturerHomeTabsComponent implements OnInit, OnDestroy {

    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();

    panelIChanged: Subscription;
    region: AmsRegion;
    subregion: AmsSubregion;
    subregionChanged: Subscription;
    country: AmsCountry;
    state: AmsState;
    airport: AmsAirport;
    manufacturer: AmsManufacturer;
    manufacturerChanged: Subscription;

    selTabIdx: number;
    tabIdxChanged: Subscription;

    get panelIn(): boolean {
        return this.mfrService.panelIn;
    }

    set panelIn(value: boolean) {
        this.mfrService.panelIn = value;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private adminService: AmsAdminService,
        private mfrService: AmsAdminManufacturerService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.panelIChanged = this.mfrService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.mfrService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.manufacturerChanged = this.adminService.manufacturerChanged.subscribe((mfr: AmsManufacturer) => {
            this.initFields();
        });


        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
        if (this.manufacturerChanged) { this.manufacturerChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.mfrService.tabIdx;
        this.manufacturer = this.mfrService.manufacturer;

        this.preloader.hide();
    }

    //#region Mobile dialog methods

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> usecase:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.mfrService.tabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region SPM Methods
    panelInOpen() {
        this.mfrService.panelIn = true;
    }

    spmProjectOpenClick() {

        // console.log('spmProjectOpen -> spmProjectObj=', this.project);
        /*
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }*/

    }

    //#endregion

}
