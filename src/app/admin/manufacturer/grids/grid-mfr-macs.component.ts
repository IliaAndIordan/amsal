import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsMfrMacTableDataSource } from './grid-mfr-macs.datasource';
import { AmsAdminManufacturerService } from '../admin-mfr.service';
import { AmsAdminService } from '../../admin.service';
// --- Models
import { AmsState } from 'src/app/@core/services/api/country/dto';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { RwTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsMac, AmsMacTableCriteria, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMacEditDialog } from 'src/app/@share/components/dialogs/mac-edit/mac-edit.dialog';





@Component({
    selector: 'ams-admin-grid-mfr-macs',
    templateUrl: './grid-mfr-macs.component.html',
})
export class AmsGridMfrAmsMacComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<AmsState>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    active = AmsStatus.Active;
    typeOpt = RwTypeOpt;
    selMac: AmsMac;
    airport: AmsAirport;
    manufacturer: AmsManufacturer;
    manufacturerChanged: Subscription;
    mfrChanged: Subscription;

    criteria: AmsMacTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'image', 'macId', 'acTypeId', 'model', 'powerplant', 'maxSeating', 'operationTimeMin', 'maxRangeKm', 'amsStatus', 'udate' ];
    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private mfrService: AmsAdminManufacturerService,
        private adminService: AmsAdminService,
        public tableds: AmsMfrMacTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {


        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsMac>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsMacTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });


        this.manufacturerChanged = this.adminService.manufacturerChanged.subscribe((mfr: AmsManufacturer) => {
            //console.log(' manufacturerChanged -> = mfr ', mfr);
            this.criteria = this.tableds.criteria;
            this.criteria.mfrId = mfr.mfrId;
            this.tableds.criteria = this.criteria;

        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.manufacturerChanged) { this.manufacturerChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        if (this.criteria.mfrId !== this.mfrService.manufacturer.mfrId) {
            this.criteria.mfrId = this.mfrService.manufacturer.mfrId;
            this.tableds.criteria = this.criteria;
        }

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if(airport && airport.apId){
            this.cus.spmAirportPanelOpen.next(airport.apId);
          }
    }


    ediMac(data: AmsMac) {
        console.log('ediRunway-> AmsMac:', data);
        //console.log('ediAirport-> city:', this.stService.city);
        
        if (data) {
            const dialogRef = this.dialogService.open(AmsMacEditDialog, {
                //width: '721px',
                //height: '620px',
                data: {
                    mac: data,
                    mfr: this.adminService.manufacturer
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediMac-> res:', res);
                if (res) {
                    //this.table.renderRows();
                    this.tableds.loadData()
                    .subscribe(res => {
                        this.initFields();
                    });
                }
                else{
                    this.initFields();
                }
                
                
            });
            
        }
        else {
            this.toastr.info('Please select airport first.', 'Select Airport');
        }
        

    }

    addMac() {
        // console.log('inviteUser-> ');
        let dto = new AmsMac();
        dto.mfrId = this.mfrService.manufacturer.mfrId;
        this.ediMac(dto);
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);

        if (data) {
            this.adminService.mac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    //#region Data
/*
    public loadAirport(apId: number): Observable<AmsAirport> {
        
        this.preloader.show();
        return new Observable<AmsAirport>(subscriber => {

            this.aService.loadAirport(apId)
                .subscribe((resp: AmsAirport) => {
                    //console.log('loadAirport-> resp', resp);
                    subscriber.next(resp);
                    this.preloader.hide();
                },
                    err => {
                        this.preloader.hide();
                        throw err;
                    });
        });
        
    }
*/
    //#endregion


    /*
    gotoAirport(data: AmsAirport) {
       

        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }

    }*/

    //#endregion
}

