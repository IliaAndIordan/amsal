import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAdminService } from '../admin.service';


export const SR_LEFT_PANEL_IN_KEY = 'ams_admin_cfg_charter_filter_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_admin_cfg_charter_tab_idx';

@Injectable({
  providedIn: 'root'
})
export class CfgCharterService {

  constructor( 
    private cus: CurrentUserService,
    private flClient: AmsFlightClient,
    private adminService: AmsAdminService,
    private apCache:AmsAirportCacheService) { }

    airportsData$ = this.apCache.airports;

    //#region Filter Panel and Tab IDX

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion
}
