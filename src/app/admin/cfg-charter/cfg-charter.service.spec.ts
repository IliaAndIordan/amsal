import { TestBed } from '@angular/core/testing';

import { CfgCharterService } from './cfg-charter.service';

describe('Admin -> Charter -> CfgCharterService', () => {
  let service: CfgCharterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CfgCharterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
