import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CfgCharterFilterPanelComponent } from './cfg-charter-filter-panel.component';

describe('CfgCharterFilterPanelComponent', () => {
  let component: CfgCharterFilterPanelComponent;
  let fixture: ComponentFixture<CfgCharterFilterPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CfgCharterFilterPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CfgCharterFilterPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
