import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CfgCharterService } from '../cfg-charter.service';

@Component({
  selector: 'ams-cfg-charter-filter-panel',
  templateUrl: './cfg-charter-filter-panel.component.html',
})
export class CfgCharterFilterPanelComponent implements OnInit, OnDestroy {

  tabIdx: number;
  tabIdxChanged: Subscription;

  airportsData$ = this.charterService.airportsData$;

  constructor(private charterService: CfgCharterService) { }
  

  ngOnInit(): void {
    this.tabIdxChanged = this.charterService.tabIdxChanged.subscribe(tabIdx => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });
  }

  ngOnDestroy(): void {
    if(this.tabIdxChanged){this.tabIdxChanged.unsubscribe();}
  }

  initFields() {
    this.tabIdx = this.charterService.tabIdx;

  }


}
