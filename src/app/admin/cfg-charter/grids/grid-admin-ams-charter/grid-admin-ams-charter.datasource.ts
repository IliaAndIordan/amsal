import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { CfgCharterService } from '../../cfg-charter.service';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsCfgCharter, AmsCfgCharterTableCriteria, ResponceAmsCfgCharterTable, ResponceAmsCfgCharterTableData } from 'src/app/@core/services/api/flight/charter-dto';

export const KEY_CRITERIA = 'ams_grid_admin_ams_charter_criteria';

@Injectable({ providedIn: 'root' })
export class AdminGridAmsCharterTableDataSource extends DataSource<AmsCfgCharter> {

    seleted: AmsCfgCharter;

    private _data: Array<AmsCfgCharter>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsCfgCharter[]> = new BehaviorSubject<AmsCfgCharter[]>([]);
    listSubject = new BehaviorSubject<AmsCfgCharter[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsCfgCharter> {
        return this._data;
    }

    set data(value: Array<AmsCfgCharter>) {
        this._data = value;
        this.listSubject.next(this._data as AmsCfgCharter[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsFlightClient,
        private charterService: CfgCharterService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsCfgCharterTableCriteria>();

    public get criteria(): AmsCfgCharterTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsCfgCharterTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsCfgCharterTableCriteria(), JSON.parse(onjStr));
            
        } else {
            obj = new AmsCfgCharterTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'depApId';
            obj.sortDesc = true;
            obj.depApId = undefined;
            obj.arrApId = undefined;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsCfgCharterTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsCfgCharter[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsCfgCharter>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<AmsCfgCharter>>(subscriber => {

            this.client.amsCharterTable(this.criteria)
                .subscribe((resp: ResponceAmsCfgCharterTable) => {
                    this.data = new Array<AmsCfgCharter>();
                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.charters && resp.data.charters.length > 0) {
                            resp.data.charters.forEach(iu => {
                                const obj = AmsCfgCharter.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsCfgCharter>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
