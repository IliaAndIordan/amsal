import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// --- Models
import { AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS, URL_COMMON_IMAGE_AIRCRAFT } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { RwTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMac, AmsMacCabin, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMacCabinEditDialog } from 'src/app/@share/components/dialogs/mac-cabin-edit/mac-cabin-edit.dialog';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AdminGridCfgCharterTableDataSource } from './grid-admin-cfg-charter.datasource';
import { CfgCharterService } from '../../cfg-charter.service';
import { AmsFlightClient } from 'src/app/@core/services/api/flight/ams-flight.client';
import { AmsCfgCharter, AmsCfgCharterTableCriteria } from 'src/app/@core/services/api/flight/charter-dto';
import { AmsAdminService } from 'src/app/admin/admin.service';
import { AmsCfgCharterEditDialog } from 'src/app/@share/components/dialogs/charter/cfg-charter-edit/cfg-charter-edit.dialog';
import { ResponseModel } from 'src/app/@core/models/common/responce.model';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';


@Component({
    selector: 'ams-grid-admin-cfg-charter',
    templateUrl: './grid-admin-cfg-charter.component.html',
})
export class AmsGridAdminCfgCharterComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsCfgCharter>;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('filterBox') filterBox: HTMLInputElement;

    seatsFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_f.png';
    seatsBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_b.png';
    seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_e.png';
    typeOpt = RwTypeOpt;
    selCharter: AmsCfgCharter;

    depAirpoer: AmsMac;
    depAirpoerChanged: Subscription;

    criteria: AmsCfgCharterTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    // 'payloadKg'
    displayedColumns = ['action', 'charterId', 'flightName', 'depApCode', 'arrApCode', 'distanceKm', 'payloadType', 'price', 'periodDays', 'nextRun', 'lastRun', 'flightDescription'];
    canEdit: boolean;
    airline: AmsAirline;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private charterService: CfgCharterService,
        private client: AmsFlightClient,
        private adminService: AmsAdminService,
        private acService: AmsAirlineAircraftService,
        public tableds: AdminGridCfgCharterTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsCfgCharter>) => {
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsCfgCharterTableCriteria) => {
            this.criteria = this.tableds.criteria;
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();

    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
            if(this.filterBox){this.filterBox.value = this.criteria.filter;}
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    viewDetails(value: AmsCfgCharter) {
        if (this.canEdit && value) {
            console.log('viewDetails -> Not Implemented.')
        }
    }

    addCfgCharter(): void {
        const dto = new AmsCfgCharter();
        this.editCfgCharter(dto);
    }

    editCfgCharter(value: AmsCfgCharter) {
        if (this.canEdit && value) {
            console.log('editCfgCharter -> value:', value);
            const dialogRef = this.dialogService.open(AmsCfgCharterEditDialog, {
                /*width: '721px',
                height: '620px',*/
                data: {
                    charter: value,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                //console.log('editCfgCharter-> res:', res);
                if (res) {
                    this.loadPage();
                }
                this.initFields();
            });
        }
    }

    precallculateCfgCharter() {
        console.log('precallculateCfgCharter ->');
        this.preloader.display(true);
        this.client.cfgCharterPrecallculate()
            .subscribe((res: ResponseModel) => {
                this.preloader.display(false);
                console.log('precallculateCfgCharter -> res:', res);
                if (res && res.success) {
                    this.toastr.success('Charter Configuration', 'Operation Succesfull: Callculate all charter flights.');
                    this.loadPage();
                }
            },
                err => {
                    this.preloader.display(false);
                    console.error('Observer got an error: ' + err);
                    const errorMessage = 'Charter ' + err + ' Failed. ' + err;
                    this.toastr.error('Charter Configuration', 'Operation Failed: Callculate all charter flights.');
                    this.initFields();
                },
                () => console.log('Observer got a complete notification'));


    }

    gotoAircraft(aicraft: AmsAircraft) {
        if (aicraft) {
            this.acService.aircraft = aicraft;
            this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
        }
    }

    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if(airport && airport.apId){
            this.cus.spmAirportPanelOpen.next(airport.apId);
          }
    }

    spmAircraftOpen(value: AmsAircraft) {
        if (value) {
            //this.aliService.spmAircraftPanelOpen.next(value);
        }
    }


    ediMacCabin(data: AmsMacCabin) {
        console.log('ediMacCabin-> data:', data);
        //console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            const dialogRef = this.dialogService.open(AmsMacCabinEditDialog, {
                width: '721px',
                height: '620px',
                data: {
                    cabin: data,
                    //mac: this.acmService.acMarketMac
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediMac-> res:', res);
                if (res) {
                    //this.table.renderRows();
                    this.tableds.loadData()
                        .subscribe(res => {
                            this.initFields();
                        });
                }
                else {
                    this.initFields();
                }


            });

        }
        else {
            this.toastr.info('Please select manufacturer acrcraft model first.', 'Edit Cabin Configuration');
        }


    }

    addCabin() {
        // console.log('inviteUser-> ');
        let dto = new AmsMacCabin();
        //dto.macId = this.acmService.acMarketMac.macId;
        this.ediMacCabin(dto);
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);

        if (data) {
            //this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    //#region Data
    /*
        public loadAirport(apId: number): Observable<AmsAirport> {
            
            this.preloader.show();
            return new Observable<AmsAirport>(subscriber => {
     
                this.aService.loadAirport(apId)
                    .subscribe((resp: AmsAirport) => {
                        //console.log('loadAirport-> resp', resp);
                        subscriber.next(resp);
                        this.preloader.hide();
                    },
                        err => {
                            this.preloader.hide();
                            throw err;
                        });
            });
            
        }
    */
    //#endregion


    /*
    gotoAirport(data: AmsAirport) {
       
    
        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }
    
    }*/

    //#endregion

    //#region SPM

    spmCfgCharterOpen(value: AmsCfgCharter) {
        if (value) {
            this.adminService.spmCfgCharterPanelOpen.next(value);
        }
    }

    //#endregion
}

