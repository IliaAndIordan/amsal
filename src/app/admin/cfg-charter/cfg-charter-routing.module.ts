import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { CfgCharterComponent } from './cfg-charter.component';
import { AdminCfgCharterHomeTabComponent } from './tabs/admin-cfg-charter-home-tab/admin-cfg-charter-home-tab.component';

const routes: Routes = [
  {
    path: AppRoutes.Root, component: CfgCharterComponent,
    children: [
      { path: AppRoutes.Root, pathMatch: 'full', component: AdminCfgCharterHomeTabComponent },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CfgCharterRoutingModule { }


export const routedComponents = [
  CfgCharterComponent,
  AdminCfgCharterHomeTabComponent,
];
