import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Animate } from 'src/app/@core/const/animation.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from 'src/app/@core/const/animations-triggers';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS } from 'src/app/@core/const/app-storage.const';
import { AmsCfgCharter } from 'src/app/@core/services/api/flight/charter-dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { environment } from 'src/environments/environment';
import { AmsAdminService } from '../admin.service';
import { AmsAdminManufacturerService } from '../manufacturer/admin-mfr.service';
import { CfgCharterService } from './cfg-charter.service';

@Component({
  selector: 'ams-cfg-charter',
  templateUrl: './cfg-charter.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class CfgCharterComponent implements OnInit, OnDestroy {

  in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  logo = COMMON_IMG_LOGO_RED;
  amsImgUrl = URL_COMMON_IMAGE_AMS;
  env: string;

  get panelIn(): boolean {
    let rv = this.charterService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.charterService.panelIn;
  }

  set panelIn(value: boolean) {
    this.charterService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    public dialogService: MatDialog,
    private adminService: AmsAdminService,
    private charterService: CfgCharterService
  ) { }
 

  ngOnInit(): void {
    this.env = environment.abreviation;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    this.panelInChanged = this.charterService.panelInChanged.subscribe((panelIn: boolean) => {
      this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    });

    this.initFields();
  }

  ngOnDestroy(): void {
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }
  

  initFields() {
    
  }

  //#region Action Methods


  //#endregion

  //#region Filter Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM

  spmCfgCharterOpen(value: AmsCfgCharter) {
    if (value) {
        this.adminService.spmCfgCharterPanelOpen.next(value);
    }
}

  //#endregion
}
