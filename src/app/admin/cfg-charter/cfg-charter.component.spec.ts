import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CfgCharterComponent } from './cfg-charter.component';

describe('Admin -> Charter -> CfgCharterComponent', () => {
  let component: CfgCharterComponent;
  let fixture: ComponentFixture<CfgCharterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CfgCharterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CfgCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
