import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CfgCharterRoutingModule, routedComponents } from './cfg-charter-routing.module';
import { CfgCharterFilterPanelComponent } from './filter-panel/cfg-charter-filter-panel.component';
import { SxShareModule } from 'src/app/@share/@share.module';
import { SxMaterialModule } from 'src/app/@share/components/material.module';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { AmsGridAdminCfgCharterComponent } from './grids/grid-admin-cfg-charter/grid-admin-cfg-charter.component';
import { AmsGridAdminAmsCharterComponent } from './grids/grid-admin-ams-charter/grid-admin-ams-charter.component';


@NgModule({
  declarations: [
    routedComponents,
    CfgCharterFilterPanelComponent,
    AmsGridAdminCfgCharterComponent,
    AmsGridAdminAmsCharterComponent,
  ],
  imports: [
    CommonModule,
    SxShareModule,
    SxMaterialModule,
    SxPipesModule,
    
    CfgCharterRoutingModule
  ]
})
export class CfgCharterModule { }
