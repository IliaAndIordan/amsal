import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCfgCharterHomeTabComponent } from './admin-cfg-charter-home-tab.component';

describe('Admin -> Charter -> AdminCfgCharterHomeTabComponent', () => {
  let component: AdminCfgCharterHomeTabComponent;
  let fixture: ComponentFixture<AdminCfgCharterHomeTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminCfgCharterHomeTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCfgCharterHomeTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
