import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CfgCharterService } from '../../cfg-charter.service';

@Component({
  selector: 'ams-admin-cfg-charter-home-tab',
  templateUrl: './admin-cfg-charter-home-tab.component.html',
})
export class AdminCfgCharterHomeTabComponent implements OnInit, OnDestroy {

  panelIChanged: Subscription;

  selTabIdx: number;
  tabIdxChanged: Subscription;

  get panelIn(): boolean {
    return this.charterService.panelIn;
  }

  set panelIn(value: boolean) {
    this.charterService.panelIn = value;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private preloader: SpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private charterService: CfgCharterService) {
  }

  ngOnInit(): void {
    this.panelIChanged = this.charterService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });

    this.tabIdxChanged = this.charterService.tabIdxChanged.subscribe(tabIdx => {
      // console.log('packageCahnged -> pkg', pkg);
      this.initFields();
    });

  }
  ngOnDestroy(): void {
    if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
    if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

  }

  initFields() {
    this.selTabIdx = this.charterService.tabIdx;
  }

  //#region  Tab Panel Actions

  selectedTabChanged(tabIdx: number) {
    this.selTabIdx = tabIdx;
    this.charterService.tabIdx = this.selTabIdx;
  }
  panelInOpen() {
    this.charterService.panelIn = true;
  }

  //#endregion

  //#region SPM Methods

  spmProjectOpenClick() {

    // console.log('spmProjectOpen -> spmProjectObj=', this.project);
    /*
    if (this.project) {
        this.spmProjectOpen.emit(this.project);
    }*/

  }

  //#endregion

}
