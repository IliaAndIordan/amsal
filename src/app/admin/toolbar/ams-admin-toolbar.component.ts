import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from 'src/app/@core/const/app-storage.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAdminService } from '../admin.service';

@Component({
    selector: 'ams-admin-toolbar',
    templateUrl: 'ams-admin-toolbar.component.html'
})

export class AmsAdminToolbarComponent implements OnInit, OnDestroy {

    @Output() subtitleClick: EventEmitter<any> = new EventEmitter<any>();
    @Input() ptitle: string;
    @Input() psubtitle: string;
    /**
     * FIELDS
     */
    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    roots = AppRoutes;

    isAdmin: boolean;
    user: UserModel;
    userChanged: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private aService: AmsAdminService) { }





    ngOnInit() {
        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
    }

}
