

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAdminService } from '../../admin.service';
// -Models
import { environment } from 'src/environments/environment';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { Subscription } from 'rxjs';
import { Tall, URL_NO_IMG_SQ, Wide } from 'src/app/@core/const/app-storage.const';


@Component({
    templateUrl: './user-profile.component.html',
})
export class AdminUserProfileComponent implements OnInit, OnDestroy {

    noImageUrl = URL_NO_IMG_SQ;
    imageClass: string;
    user: UserModel;
    userName: string;
    userChanged: Subscription;
    avatarUrl: string;
    env: string;

    chartDataUserHistory = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUserHistory = ['January', 'February', 'Mars', 'April'];

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private aService: AmsAdminService,) { }


    ngOnInit(): void {
        this.env = environment.abreviation;
        this.userChanged = this.aService.selUserChanged.subscribe(user => {
            this.initFields();
        });
        this.initFields();
    }



    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.aService.selUser;
        if(!this.user){
            this.aService.selUser = this.cus.user;
        }
        this.avatarUrl = this.aService.selUserAvatarUrl;
        let name = 'Guest';
        if(this.user){
            name = this.user.userName?this.user.userName:this.user.userEmail;
        }
        this.userName = name;
        // console.log('initFields -> cus.user:', this.cus.user);
    }

    //#region  data

    public gatGvdtTsoSearchHits(): void {
        /*
        const gvdt: GvDataTable = GvdtTsoSearchHits.GvdtTsoSearchHitsTotalDefinition();
        // console.log('getCompanyBaseInfo-> gvdt', gvdt);
        this.lService.getGvDataTable(gvdt)
            .subscribe((resp: GetGvDataTableResponse) => {
                console.log('gatGvdtTsoSearchHits-> resp', resp);
                this.gvdtHits = resp;
                this.chartData = GvdtTsoSearchHits.getLineChartData(resp.dataTable);
                this.chartLabels = GvdtTsoSearchHits.getLineChartLabels(resp.dataTable);
                console.log('chartData=', this.chartData);
            });
            */
    }
    //#endregion

    //#region  Actions

    onChartClick(event) {
        console.log('onChartClick -> event:', event);
    }

    //#endregion

    

  onImageLoad(): void {
    this.imageClass = this.getImageClass();
  }

  getImageClass(): string {
    let image = new Image();
    image.src = this.avatarUrl;

    if (image.width > image.height + 20 || image.width === image.height) {
      //return wide image class
      return Wide;
    } else {
      return Tall;
    }
  }
}
