

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AMS_COMMON, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { LoginsCountModel, ResponseLoginsCount } from 'src/app/@core/services/api/admin-user/charts-dto';
import { AmsAdminService } from '../admin.service';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { AmsAircraft } from 'src/app/@core/services/api/aircraft/dto';
import { AmsFlight } from 'src/app/@core/services/api/flight/dto';
import { Observable } from 'rxjs';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { AmsCountCityNoDemandResponce } from 'src/app/@core/services/api/country/dto';
import { IInfoMessage, InfoMessageDialog } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { MatDialog } from '@angular/material/dialog';


@Component({
    templateUrl: './dashboard.component.html',
    // animations: [PageTransition]
})
export class AmsAdminDashboardComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    logo = COMMON_IMG_LOGO_RED;
    tileImgUrl = URL_COMMON_IMAGE_TILE;
    acTypeImg = URL_COMMON_IMAGE_AIRCRAFT + 'aircraft_0_tableimg.png';
    acCharterImg = URL_COMMON_IMAGE_AMS_COMMON + 'charter_03.png';
    acCargoImg = URL_COMMON_IMAGE_AMS_COMMON + 'ac-cargo-03.png';
    rootes = AppRoutes;
    
    env: string;
    loginsCountData: ResponseLoginsCount;
    usersCount: number;
    loginsLis: LoginsCountModel[];

    chartDataUsers = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUsers = ['January', 'February', 'Mars', 'April'];

    chartDataUserList;
    chartLabelsUserList;
    mfrCount:number;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private aService: AmsAdminService,
        public dialog: MatDialog,
        private countryClient: AmsCountryClient,
        private apCache: AmsAirportCacheService) { }


    ngOnInit(): void {
        this.env = environment.abreviation;
        if (!this.aService.loginsCountData) {
            this.loadLoginsCount();
        }
        else {
            this.initFields();
        }
        this.loadAirport();
        this.loadCityNoDemandCount();
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    initFields() {
        this.loginsCountData = this.aService.loginsCountData;
        if (this.loginsCountData && this.loginsCountData.data && this.loginsCountData.data.totals) {
            this.usersCount = this.loginsCountData.data.totals[0].total_rows;
            this.loginsLis = LoginsCountModel.dataToList(this.loginsCountData.data);
        }
        this.chartDataUsers = LoginsCountModel.getLineChartData(this.loginsLis);
        this.chartLabelsUsers = LoginsCountModel.getLineChartLabels(this.loginsLis);
        const mfrs = this.aService.manufacturers;
        this.mfrCount = mfrs?mfrs.length:0;
    }
    //#region  data

    public loadLoginsCount(): void {
        this.spinerService.show();
        this.aService.loadLoginsCount()
            .subscribe((list: LoginsCountModel[]) => {
                //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
                this.spinerService.hide();
                this.initFields();
                /*
                this.loginsLis = list;
                this.usersCount = this.aService.usersCount;
                //console.log('gatGvdtTsoSearchHits-> usersCount', this.usersCount);
                this.chartDataUsers = LoginsCountModel.getLineChartData(this.loginsLis);
                this.chartLabelsUsers = LoginsCountModel.getLineChartLabels(this.loginsLis);
                //console.log('chartLabelsUsers=', this.chartLabelsUsers);
                */
            },
                err => {
                    this.spinerService.hide();
                    console.log('autenticate-> err: ', err);
                });

    }

    airport$: Observable<AmsAirport>;
    loadAirport() {
        if (this.cus.airline) {
            this.airport$ = this.apCache.getAirport(this.cus.airline.apId);
            this.airport$.subscribe(airport => {
                this.initFields();
            });
        }
    }
    cityNoDemandCount: number = 0;
    loadCityNoDemandCount(): void {
        this.countryClient.countCityNoDemend()
            .then((res: AmsCountCityNoDemandResponce) => {
                this.cityNoDemandCount = res.data.rowsCount;
            })
    }

    calcCityPayloadDemendClick() {
        const data: IInfoMessage = {
            message: '<div>Callculation of city and airports PAX and cargo demand is a long process.<be/><br/>Are your shire that you ould like to start it?</div>',
            title: 'City Payload Demand Callculation'
        }
        const dialogRef = this.dialog.open(InfoMessageDialog, {
            /*width: '721px',
            height: '320px',*/
            data: data
        });

        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.spinerService.display(true);
                this.spinerService.setMessage('Callculate city payload demans ...')
                this.countryClient.calcCityPayloadDemend()
                    .then((res:AmsCountCityNoDemandResponce) => {
                        //console.log('loadCityNoDemandCount -> res:', res);
                        this.toastr.success(`Success: Paylad deman for ${res?.data?.rowsCount} cities culated.`, 'City Payload Demand Callculation')
                        this.loadCityNoDemandCount();
                        this.spinerService.display(false);
                        this.initFields();
                    },
                        msg => {
                            console.log('loadCityNoDemandCount -> msg:', msg);
                            this.spinerService.display(false);
                            this.toastr.error(`Error: ${msg?.name}`, 'City Payload Demand Callculation')
                            this.loadCityNoDemandCount();
                        }).catch(ex => {
                            console.log('loadCityNoDemandCount -> ex:', ex);
                            this.spinerService.display(false);
                            this.toastr.error(`Error: ${ex}`, 'City Payload Demand Callculation')
                            this.loadCityNoDemandCount();
                        });
            }
        });

    }


    //#endregion

    //#region  Actions

    onChartClick(event) {
        console.log('onChartClick -> event:', event);
    }

    gotoUsersManagement() {
        this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.usersList]);
    }

    gotoManufacturers() {
        this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.manufacturers]);
    }

    spmAircraftOpen(ac: AmsAircraft): void {
        this.aService.spmAircraftOpen.next(ac);
    }

    spmFlqOpenClick(flq: AmsFlight) {
        this.aService.spmFlqOpen.next(flq);
    }

    spmFlOpenClick(flq: AmsFlight) {
        this.aService.spmFlOpen.next(flq);
    }

    //#endregion
}
