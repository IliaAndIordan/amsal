import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { AdminRoutingModule, routedComponents } from './admin-routing.module';
import { AmsAdminService } from './admin.service';
import { AmsAdminToolbarComponent } from './toolbar/ams-admin-toolbar.component';
import { AmsAdminMenuMainComponent } from './menu-main/admin-menu-main.component';
import { AmsUserTableDataSource } from './grids/users/grid-users.datasource';
import { AmsAdminUserService } from './user/admin-user.service';
import { TbMenuManufacturersComponent } from './manufacturers/tb-menu/mfrs-tb-menu.component';
import { AmsAdminMfrFilterPanelBodyComponent } from './manufacturer/filter-panel/admin-mfr-filter-pane.component';
import { AmsMfrMacTableDataSource } from './manufacturer/grids/grid-mfr-macs.datasource';
import { AmsGridMfrAmsMacComponent } from './manufacturer/grids/grid-mfr-macs.component';
import { AmsAdminMacService } from './mac/admin-mac.service';
import { AmsAdminMacFilterPanelBodyComponent } from './mac/filter-panel/admin-mac-filter-pane.component';
import { AmsGridMacCabinsComponent } from './mac/grids/cabins/grid-mac-cabins.component';
import { AmsGridMacAircraftsComponent } from './mac/grids/aircrafts/grid-mac-aircrafts.component';
import { AmsAdminMacDashboardTabComponent } from './mac/tabs/admin-mac-dashboard-tab.component';
import { AmsMacCabinTableDataSource } from './mac/grids/cabins/grid-mac-macs.datasource';
import { AmsMacAircraftTableDataSource } from './mac/grids/aircrafts/grid-mac-aircrafts.datasource';
import { AmsAdminCommonModule } from './common/ams-admin-common.module';



@NgModule({
    imports: [
        CommonModule,
        SxShareModule,
        SxMaterialModule,
        SxPipesModule,
        //
        AdminRoutingModule,
        AmsAdminCommonModule,
    ],
    declarations: [
        routedComponents,
        AmsAdminMenuMainComponent,
        AmsAdminToolbarComponent,
        //-Manufacturer
        TbMenuManufacturersComponent,
        AmsAdminMfrFilterPanelBodyComponent,
        AmsGridMfrAmsMacComponent,
        AmsAdminMacFilterPanelBodyComponent,
        AmsGridMacCabinsComponent,
        AmsGridMacAircraftsComponent,
        AmsAdminMacDashboardTabComponent,
    ],
    exports: [],
    providers: [
        AmsAdminService,
        AmsUserTableDataSource,
        AmsAdminUserService,
        AmsMfrMacTableDataSource,
        AmsAdminMacService,
        AmsMacCabinTableDataSource,
        AmsMacAircraftTableDataSource,
    ]
})
export class AdminModule { }
