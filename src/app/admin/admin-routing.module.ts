import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { AdminComponent } from './admin.component';
import { AdminHomeComponent } from './home.component';
import { AmsAdminDashboardComponent } from './dashboard/dashboard.component';
import { AmsUserListComponent } from './grids/users/grid-users.component';
import { AdminUserProfileComponent } from './pages/user-profile/user-profile.component';
import { AmsAdminManufacturersComponent } from './manufacturers/manufacturers.component';
import { AmsAdminUserHomeComponent } from './user/admin-user.component';
import { AmsAdminManufacturerHomeComponent } from './manufacturer/admin-mfr.component';
import { AmsAdminManufacturerHomeTabsComponent } from './manufacturer/tabs/admin-mfr-home-tabs.component';
import { AmsAdminMacHomeComponent } from './mac/admin-mac.component';
import { AmsAdminMacHomeTabsComponent } from './mac/tabs/admin-mac-home-tabs.component';
import { AmsAdminAircraftHomeComponent } from './aircraft/admin-aircraft.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: AdminComponent,
    children: [
      {
        path: AppRoutes.Root, component: AdminHomeComponent,
        children: [
          { path: AppRoutes.Root, pathMatch: 'full', component: AmsAdminDashboardComponent },
          { path: AppRoutes.usersList, component: AmsUserListComponent },
          { path: AppRoutes.profile, component: AdminUserProfileComponent },

          { path: AppRoutes.manufacturers, component: AmsAdminManufacturersComponent },
          {
            path: AppRoutes.manufacturer, component: AmsAdminManufacturerHomeComponent, children: [
              { path: AppRoutes.Root, pathMatch: 'full', component: AmsAdminManufacturerHomeTabsComponent },
            ]
          },
          {
            path: AppRoutes.mac, component: AmsAdminMacHomeComponent, children: [
              { path: AppRoutes.Root, pathMatch: 'full', component: AmsAdminMacHomeTabsComponent },
            ]
          },
          { path: AppRoutes.user, component: AmsAdminUserHomeComponent },
          {
            path: AppRoutes.cfgCharter,
            loadChildren: () => import('./cfg-charter/cfg-charter.module').then(m => m.CfgCharterModule), data: { preload: false },
          },

        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }

export const routedComponents = [
  AdminComponent,
  AdminHomeComponent,
  AmsAdminDashboardComponent,
  AmsUserListComponent,
  AdminUserProfileComponent,
  AmsAdminManufacturersComponent,
  AmsAdminManufacturerHomeComponent,
  AmsAdminUserHomeComponent,
  AmsAdminManufacturerHomeTabsComponent,
  AmsAdminMacHomeComponent,
  AmsAdminMacHomeTabsComponent,
  AmsAdminAircraftHomeComponent,
];
