

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadService } from '../../wad/wad.service';
// -Models
import { environment } from 'src/environments/environment';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, 
  ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from '../../@core/const/animations-triggers';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsCountry, AmsRegion, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { Animate } from 'src/app/@core/const/animation.const';
import { Subscription } from 'rxjs';
import { AmsSidePanelModalComponent } from 'src/app/@share/components/common/side-panel-modal/side-panel-modal';
import { AmsAdminUserService } from './admin-user.service';
import { AmsAdminService } from '../admin.service';
import { UserModel } from 'src/app/@core/services/auth/api/dto';


@Component({
  templateUrl: './admin-user.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AmsAdminUserHomeComponent implements OnInit, OnDestroy {


  @ViewChild('spmUser') spmUser: AmsSidePanelModalComponent;

  /**
   * Fields
   */
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  logo = COMMON_IMG_LOGO_RED;
  amsImgUrl = URL_COMMON_IMAGE_AMS;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  user: UserModel;
  userChanged: Subscription;
  smpUserPanelOpen: Subscription;
  


  get panelIn(): boolean {
    let rv = this.usersService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.usersService.panelIn;
  }

  set panelIn(value: boolean) {
    this.usersService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private adminService: AmsAdminService,
    private usersService: AmsAdminUserService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    const tmp = this.panelIn;
    this.panelInChanged = this.usersService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });

    

    this.userChanged = this.adminService.selUserChanged.subscribe((user: UserModel) => {
      this.initFields();
    });

    this.initFields();

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
    if (this.userChanged) { this.userChanged.unsubscribe(); }
    if (this.smpUserPanelOpen) { this.smpUserPanelOpen.unsubscribe(); }
  }

  initFields() {
    this.user = this.usersService.selUser;

    console.log('initFields-> user:', this.user);
  }

  //#region  data

  public loadRegions(): void {
    /*
    this.spinerService.show();
    this.wadService.loadRegions()
      .subscribe((resp: ResponseAmsRegionsData) => {
        //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
        this.spinerService.hide();
        this.initFields();
      },
        err => {
          this.spinerService.hide();
          console.log('loadRegions-> err: ', err);
        });
        */
  }
  //#endregion

  //#region  Actions

  // sidebar-closed
  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoSubregion(value: AmsSubregion) {
    //console.log('gotoSubregion -> value:', value);
    if (value) {
      //this.wadService.subregion = value;
      this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }

  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmUserClose(): void {
    //console.log('spmRegionClose ->');
    this.spmUser.closePanel();
  }

  spmUserOpen() {
    //console.log('spmRegionOpen ->');
    this.user = this.usersService.selUser;
    if (this.user && this.spmUser) {
      this.spmUser.expandPanel();
    }

  }

  //#endregion
}
