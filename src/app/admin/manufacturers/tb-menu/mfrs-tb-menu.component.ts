import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import { COMMON_IMG_LOGO_RED } from "src/app/@core/const/app-storage.const";
import { UserModel } from "src/app/@core/services/auth/api/dto";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { environment } from "src/environments/environment";

@Component({
    selector: 'ams-tb-menu-mfrs',
    templateUrl: './mfrs-tb-menu.component.html',
    // animations: [PageTransition]
})
export class TbMenuManufacturersComponent implements OnInit, OnDestroy {

    @Output() refresh: EventEmitter<any> = new EventEmitter<any>();
    @Output() addMfr: EventEmitter<any> = new EventEmitter<any>();
    /**
     * Fields
     */
    
    roots = AppRoutes;
    user: UserModel;
    userChanged: Subscription;
    isAdmin:boolean;

    constructor(private router: Router,
        private cus: CurrentUserService, ) { }

    ngOnInit(): void {
        this.isAdmin = this.cus.isAdmin;
        this.userChanged = this.cus.userChanged.subscribe(user => {
            
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
    }

    refreshCkick(){
        this.refresh.emit();
    }

    addManufacturer(){
        this.addMfr.emit(undefined);
    }
}
