

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { LoginsCountModel } from 'src/app/@core/services/api/admin-user/charts-dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsRegion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { AmsAdminService } from '../admin.service';
import { AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsManufacturerEditDialog } from 'src/app/@share/components/dialogs/mfr-edit/mfr-edit.dialog';
import { MatDialog } from '@angular/material/dialog';


@Component({
    selector: 'ams-admin-manufacturers',
    templateUrl: './manufacturers.component.html',
    // animations: [PageTransition]
})
export class AmsAdminManufacturersComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    logo = COMMON_IMG_LOGO_RED;
    amsImgUrl = URL_COMMON_IMAGE_AMS;
    env: string;

    manufacturers: AmsManufacturer[];
    mfrCount: number;
    
    loginsLis: LoginsCountModel[];
    chartDataUsers = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUsers = ['January', 'February', 'Mars', 'April'];

    chartDataUserList;
    chartLabelsUserList;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private adminService: AmsAdminService) { }


    ngOnInit(): void {
        this.env = environment.abreviation;
        if (!this.adminService.manufacturers || this.adminService.manufacturers.length === 0) {
            this.loadMfrs();
        }
        this.initFields();
    }

    initFields() {
        this.manufacturers = this.adminService.manufacturers;
        this.mfrCount = this.manufacturers ? this.manufacturers.length : 0;
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    //#region  data

    public loadMfrs(): void {
        this.spinerService.show();
        this.adminService.loadMfrsAll()
            .subscribe((resp: AmsManufacturer[]) => {
                //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
                this.spinerService.hide();

                this.initFields();
                console.log('loadMfrs-> manufacturers:', this.manufacturers);
            },
                err => {
                    this.spinerService.hide();
                    this.initFields();
                    console.log('loadRegions-> err: ', err);
                });

    }
    //#endregion

    //#region  Actions

    gotoManufacturer(value: AmsManufacturer) {
        console.log('gotoManufacturer -> value:', value);
        if (value) {
            this.adminService.manufacturer = value;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.manufacturer]);
        }

    }

    ediManufacturer(data: AmsManufacturer, airport:AmsAirport=undefined) {
        console.log('ediManufacturer-> mfr:', data);
        if(data && data.mfrId !== 0){
            this.adminService.manufacturer = data;
        }
        const dialogRef = this.dialogService.open(AmsManufacturerEditDialog, {
            width: '721px',
            //height: '620px',
            data: {
                mfr:data,
                airport: airport,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('ediManufacturer-> res:', res);
            if (res) {
                //this.table.renderRows();
                this.loadMfrs();
            }
            this.initFields();
        });
        
        
    }

    addManufacturer(airport:AmsAirport=undefined){
        // console.log('inviteUser-> ');
        let dto = new AmsManufacturer();
        dto.apId = airport?airport.apId:undefined;
        
        
        this.ediManufacturer(dto, airport);
    }

    //#endregion
}
