import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/const/animation.const';
// --- Models
import { AppRoutes } from '../@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from '../@core/const/app-storage.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger } from '../@core/const/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AmsAircraft } from '../@core/services/api/aircraft/dto';
import { AmsAdminService } from './admin.service';
import { AmsSidePanelModalComponent } from '../@share/components/common/side-panel-modal/side-panel-modal';
import { AmsAirportClient } from '../@core/services/api/airport/api-client';
import { AmsCfgCharter } from '../@core/services/api/flight/charter-dto';
import { AmsFlight } from '../@core/services/api/flight/dto';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { SxSidebarNavComponent } from '../@share/components/common/sidebar/nav/sx-sidebar-nav.component';
// import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab]
})

export class AdminHomeComponent implements OnInit, OnDestroy {

  @ViewChild('spmAircraftPanel') spmAircraftPanel: AmsSidePanelModalComponent;
  @ViewChild('spmCfgCharterPanel') spmCfgCharterPanel: AmsSidePanelModalComponent;
  @ViewChild('spmFlq') spmFlq: AmsSidePanelModalComponent;
  @ViewChild('snav') snav: MatSidenav;
  @ViewChild('sxSide') sxSide: SxSidebarNavComponent;
  
  
  sideNavState: string = Animate.open;
  snavmode: MatDrawerMode = 'side';
  
  
  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  spmAircraftPanelOpen: Subscription;
  spmFlqPanelOpen: Subscription;

  // --- Side Tab
  expandTabVar: string = Animate.in;
  showTabContentsVar: string = Animate.hide;
  total: number = 0;
  aggressiveTotal: number = 0;

  // public dialogService: MatDialog
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private apClient: AmsAirportClient,
    private adminService: AmsAdminService,
    public dialogService: MatDialog) { }


  ngOnInit(): void {


    this.spmAircraftPanelOpen = this.adminService.spmAircraftOpen.subscribe((ac: AmsAircraft) => {
      this.spmAircraftOpen(ac);
    });
    this.spmCfgCharterPanelChange = this.adminService.spmCfgCharterPanelOpen.subscribe((flight: AmsCfgCharter) => {
      this.spmCfgCharterPanelOpen(flight);
    });
    this.spmFlqPanelOpen = this.adminService.spmFlqOpen.subscribe(obj =>{
      this.spmFlqOpen(obj);
    });
  }

  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
  }

  exapandTabClick() {
    this.expandTabVar = this.expandTabVar === Animate.in ? Animate.out : Animate.in;
    this.showTabContentsVar = this.showTabContentsVar === Animate.show ? Animate.hide : Animate.show;
  }

  public loginDialogShow(): void {
    /*
    const dialogRef = this.dialogService.open(LoginModal, {
      width: '500px',
      height: '340px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });
    */
  }

  // sidebar-closed
  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }

  //#region SPM

  
  spmFlqObj: AmsFlight;
  spmFlqClose(): void {
    if (this.spmFlq) {
      this.spmFlq.closePanel();
    }
  }
  
  spmFlqOpen(ac: AmsFlight) {
    this.spmFlqObj = ac;
    if (this.spmFlq && this.spmFlqObj) {
      this.spmFlq.expandPanel();
    }
    else {
      this.spmFlqClose();
    }
  }

  spmAircraftClose(): void {
    // console.log('spmStateClose ->');
    if (this.spmAircraftPanel) {
      this.spmAircraftPanel.closePanel();
    }
  }

  spmAircraft: AmsAircraft;

  spmAircraftOpen(value: AmsAircraft) {
    //console.log('spmStateOpen ->');
    this.spmAircraft = value;
    // console.log('spmStateOpen -> spmState', this.spmState);
    if (this.spmAircraft && this.spmAircraftPanel) {
      this.spmAircraftPanel.expandPanel();
    }
    else {
      this.spmAircraftClose();
    }
  }

  spmCfgCharterPanelClose(): void {
    // console.log('spmStateClose ->');
    if (this.spmCfgCharterPanel) {
      this.spmCfgCharterPanel.closePanel();
    }
  }

  spmCfgCharterPanelChange:Subscription;
  spmCfgCharter:AmsCfgCharter;

  spmCfgCharterPanelOpen(value: AmsCfgCharter) {
    //console.log('spmStateOpen ->');
    this.spmCfgCharter = value;
    // console.log('spmStateOpen -> spmState', this.spmState);
    if (this.spmCfgCharter && this.spmCfgCharterPanel) {
      this.spmCfgCharterPanel.expandPanel();
    }
    else {
      this.spmCfgCharterPanelClose();
    }
  }

  //#endregion

  //#region  Expand/Close Menu


  snavToggleClick() {
    console.log('snavToggleClick-> sxSide', this.sxSide);
    if(this.sxSide){
      this.sxSide.toggle();
    }
  }

  //#endregion


}
