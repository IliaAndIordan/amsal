import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// ---
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// ---
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { UserModel } from 'src/app/@core/services/auth/api/dto';
import { AmsUserTableCriteria } from 'src/app/@core/services/api/admin-user/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAdminService } from '../../admin.service';
import { AmsUserTableDataSource } from './grid-users.datasource';
import { AmsUserEditDialog } from 'src/app/@share/components/dialogs/user-edit/user-edit.dialog';

@Component({
    templateUrl: './grid-users.component.html',
})
export class AmsUserListComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<UserModel>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    selUser: UserModel;
    criteria: AmsUserTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'userId', 'userName', 'userEmail', 'userRole', 'isReceiveEmails',  'udate', 'adate', 'ipAddress'];
    //, 'company_id', 'estimator_id', 'estimator_name' , 'udate'
    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aService: AmsAdminService,
        public tableds: AmsUserTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
       

        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((users: Array<UserModel>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsUserTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });
        /*
        this.companyChanged = this.pService.selCompanyChanged.subscribe((company: CompanyModel) => {
            // console.log('companyProductsChanged ->', companyProducts);
            if (this.pService.selCompany) {
                this.criteria.company_id = this.pService.selCompany.company_id;
                this.tableds.criteria = this.criteria;
            }
        });
        */
        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        /*
        if (this.criteria.company_id !== this.company.company_id) {
            this.criteria.company_id = this.company.company_id;
            this.tableds.criteria = this.criteria;
        }
        */
        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    edituser(data: UserModel) {
        console.log('edituser-> user:', data);
        
        const dialogRef = this.dialogService.open(AmsUserEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                user: data,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('edituser-> res:', res);
            if (res) {
                this.table.renderRows();
            }
            this.initFields();
        });
        
    }

    inviteUser(){
        // console.log('inviteUser-> ');
        let user = new UserModel();
        this.edituser(user);
    }

    gotoUser(data: UserModel) {
        console.log('gotoUser-> user:', data);
        if (data) {
            this.aService.selUser = data;
            this.router.navigate([AppRoutes.admin, AppRoutes.user]);
        }
    }

    gotoProfile(data: UserModel) {
        console.log('gotoUser-> user:', data);
        if (data) {
            this.aService.selUser = data;
            this.router.navigate([AppRoutes.admin, AppRoutes.profile]);
        }
    }

    sendEmail(data: UserModel){
        console.log('sendEmail-> user:', data);
    }

    //#endregion
}

