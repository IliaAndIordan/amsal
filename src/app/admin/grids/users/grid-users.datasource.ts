import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { IUserModel, UserModel } from 'src/app/@core/services/auth/api/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxAdminUserClient } from 'src/app/@core/services/api/admin-user/api-client';
import { AmsAdminService } from '../../admin.service';
import { AmsUserTableCriteria, ResponseAmsUserTableData } from 'src/app/@core/services/api/admin-user/dto';

export const KEY_CRITERIA = 'ams_admin_grid_uswers_criteria';
@Injectable()
export class AmsUserTableDataSource extends DataSource<UserModel> {

    seluser: UserModel;

    private _data: Array<UserModel>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<UserModel[]> = new BehaviorSubject<UserModel[]>([]);
    listSubject = new BehaviorSubject<UserModel[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<UserModel> {
        return this._data;
    }

    set data(value: Array<UserModel>) {
        this._data = value;
        this.listSubject.next(this._data as UserModel[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: SxAdminUserClient,
        private aService: AmsAdminService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsUserTableCriteria>();

    public get criteria(): AmsUserTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsUserTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsUserTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsUserTableCriteria();
            obj.limit = 25;
            obj.offset = 0;
            obj.sortCol = 'user_name';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsUserTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(collectionViewer: CollectionViewer): Observable<UserModel[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<UserModel>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        const limit = this.criteria.limit;
        const offset = this.criteria.offset;
        const orderCol = this.criteria.sortCol;
        const isDesc = this.criteria.sortDesc; // sortDirection && sortDirection === 'asc' ? false : true;
        const userRole = this.criteria.userRole; 
        return new Observable<Array<UserModel>>(subscriber => {

            this.client.usersTable(limit, offset, orderCol, isDesc, userRole, this.criteria.filter)
                .subscribe((resp: ResponseAmsUserTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<UserModel>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.usersList && resp.data.usersList.length > 0) {
                            resp.data.usersList.forEach(iu => {
                                const obj = UserModel.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.totals[0].total_rows;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<UserModel>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
