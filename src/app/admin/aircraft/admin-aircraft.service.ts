import { Injectable } from '@angular/core';
import { BehaviorSubject} from 'rxjs';
// -Services
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Nodels
import { AmsAdminService } from '../admin.service';
import { UserModel } from 'src/app/@core/services/auth/api/dto';


export const SR_LEFT_PANEL_IN_KEY = 'ams_admin_users_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_admin_users_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class AmsAdminUsersService {

    /**
     *  Fields
     */
    
    constructor(
        private cus: CurrentUserService,
        private wadClient: AmsCountryClient,
        private adminService: AmsAdminService) {
    }

    get selUser(): UserModel  {
        return this.adminService.selUser;
    }

    set selUser(value: UserModel) {
       this.adminService.selUser = value;
    }

    //#region subregion tree panel

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Admin User Charts
    /*
    public loadRegions(): Observable<ResponseAmsRegionsData> {

        return new Observable<ResponseAmsRegionsData>(subscriber => {

            this.wadClient.Regions()
                .subscribe((resp: ResponseAmsRegionsData) => {
                    if (resp) {
                        if (resp.data) {
                             let regions = new Array<AmsRegion>();
                            resp.data.regions.forEach((reg: IAmsRegion) => {
                                const val = AmsRegion.fromJSON(reg);
                                regions.push(val);
                            });
                            /*
                            this.regions = regions;
                            let subregions = new Array<AmsSubregion>();
                            resp.data.subregions.forEach((subreg: IAmsSubregion) => {
                                const val = AmsSubregion.fromJSON(subreg);
                                subregions.push(val);
                            });
                            this.subregions = subregions;
                            
                        }


                        subscriber.next(resp);
                    } else {
                        subscriber.next(undefined);
                    }
                },
                err => {

                    throw err;
                });
        });

    }
    */
    //#endregion


}
