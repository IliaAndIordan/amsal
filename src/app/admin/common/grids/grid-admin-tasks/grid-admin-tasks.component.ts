import { ChangeDetectionStrategy, Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription, tap } from 'rxjs';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsTask, AmsTaskTableCriteria } from 'src/app/@core/services/api/admin/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { GridAdminTasksDataSource } from './grid-admin-tasks.datasource';
import { AmsTaskType } from 'src/app/@core/services/api/admin/enums';

@Component({
  selector: 'ams-grid-admin-tasks',
  templateUrl: './grid-admin-tasks.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class GridAdminTasksComponent {
  @ViewChild(MatTable, { static: true }) table: MatTable<AmsTask>; // initialize
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Output() spmTaskOpen: EventEmitter<AmsTask> = new EventEmitter<AmsTask>();

  seltasks: AmsTask;

  criteria: AmsTaskTableCriteria;
  criteriaChanged: Subscription;

  pageSizeOpt = PAGE_SIZE_OPTIONS;
  pageIndex = 0;
  pageSize = 12;
  sortActive: string;
  sortDirection: SortDirection;
  minFive:AmsTaskType = AmsTaskType.minFive;


  dataCount = 0;
  dataChanged: Subscription;
  interval: any;
  //  'spName',  
  displayedColumns = ['taskId', 'taskType', 'taskName', 'periodMin', 'startTime', 'execTimeMs', 'nextRun'];
  airline: AmsAirline;
  filter: string;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private preloader: SpinnerService,
    public dialogService: MatDialog,
    private cus: CurrentUserService,
    private aliService: AirlineInfoService,
    public tableds: GridAdminTasksDataSource) {

  }


  ngOnInit(): void {

    this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsTask>) => {
      // console.log('MyCompanyUsersComponent:userListSubject()->', users);
      this.dataCount = this.tableds.itemsCount;
      this.initFields();
    });
    this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsTaskTableCriteria) => {
      this.criteria = this.tableds.criteria;
      this.loadPage();
    });
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.interval = setInterval(() => {
      this.loadPage();
    }, 60_000);

    this.initFields();
    this.criteria.sortCol = 'nextRun';
    this.criteria.sortDesc = false;
    this.loadPage();
  }

  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.dataChanged) { this.dataChanged.unsubscribe(); }
    if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    if (this.interval) { clearInterval(this.interval); }
  }

  initFields() {
    //this.company = this.cus.company;
    this.airline = this.cus.airline;
    this.criteria = this.tableds.criteria;
    this.pageIndex = this.criteria.pageIndex;
    this.pageSize = this.criteria.limit;
    this.dataCount = this.tableds.itemsCount;

    this.sortActive = this.criteria.sortCol;
    this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

    // console.log('initFields-> criteria:', this.criteria);
    const tq = '?m=' + new Date().getTime();
  }


  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => {
      this.criteria.pageIndex = 0;
      this.criteria.sortCol = this.sort.active;
      this.criteria.sortDesc = this.sort.direction !== 'asc';
      this.tableds.criteria = this.criteria;
    });
    this.paginator.page
      .pipe(
        tap(() => {
          this.criteria.pageIndex = this.paginator.pageIndex;
          this.criteria.limit = this.paginator.pageSize;
          this.tableds.criteria = this.criteria;
        })
      )
      .subscribe();
  }

  //#region Table events

  
  timeout: any = null;
  applyFilter(event: any) {
      clearTimeout(this.timeout);
      var $this = this;
      this.timeout = setTimeout(function () {
        if (event.keyCode != 13) {
          $this.setFilter( (event.target as HTMLInputElement).value);
        }
      }, 500);
  }

  setFilter(value:string){
      this.criteria.filter = value?.trim();
      this.criteria.sortCol = 'nextRun';
      this.criteria.sortDesc = false;
      this.criteria.offset = 0;
      this.tableds.criteria = this.criteria;
  }

  clearFilter() {
      this.criteria.filter = undefined;
      this.criteria.limit = 12;
      this.criteria.offset = 0;
      this.tableds.criteria = this.criteria;
  }

  loadPage() {
    this.tableds.loadData()
      .subscribe(res => {
        this.initFields();
      });
  }

  //#endregion

  //#region Actions

  setTask(value: AmsTask) {
    console.log('setTask -> value:', value);
  }

  gotoflightt(flight: AmsTask) {
    console.log('gotoflightt -> flight:' + flight);
  }

  gotoAircraft(aicraft: AmsTask) {
    console.log('gotoAircraft -> aicraft:' + aicraft);
  }

  refreshClick() {
    this.loadPage();
  }

  


  //#endregion



}
