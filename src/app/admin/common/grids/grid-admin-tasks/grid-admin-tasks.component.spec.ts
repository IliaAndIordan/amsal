import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridAdminTasksComponent } from './grid-admin-tasks.component';

describe('GridAdminTasksComponent', () => {
  let component: GridAdminTasksComponent;
  let fixture: ComponentFixture<GridAdminTasksComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GridAdminTasksComponent]
    });
    fixture = TestBed.createComponent(GridAdminTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
