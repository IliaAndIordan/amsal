import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsTask, AmsTaskTableCriteria, AmsTaskTableData } from 'src/app/@core/services/api/admin/dto';
import { AmsTaskClient } from 'src/app/@core/services/api/admin/api-client';

export const KEY_CRITERIA = 'ams_admin_task_table_criteria_v1';

@Injectable({providedIn:'root'})
export class GridAdminTasksDataSource extends DataSource<AmsTask> {

    seleted: AmsTask;

    private _data: Array<AmsTask>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsTask[]> = new BehaviorSubject<AmsTask[]>([]);
    listSubject = new BehaviorSubject<AmsTask[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsTask> {
        return this._data;
    }

    set data(value: Array<AmsTask>) {
        this._data = value;
        this.listSubject.next(this._data as AmsTask[]);
    }

    constructor(
        private cus: CurrentUserService,
        private taskClient: AmsTaskClient,) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsTaskTableCriteria>();

    public get criteria(): AmsTaskTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsTaskTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsTaskTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsTaskTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'nextRun';
            obj.sortDesc = false;
            obj.taskType = undefined;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsTaskTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsTask[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsTask>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<AmsTask>>(subscriber => {

            this.taskClient.getTaskTable(this.criteria)
                .then((resp: AmsTaskTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsTask>();

                    if (resp && resp.tasks && resp.tasks.length > 0) {
                        this.data = resp.tasks
                        this.itemsCount = resp.rowsCount?resp.rowsCount:0;
                    }
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsTask>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
