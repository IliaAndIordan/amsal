import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, OnDestroy, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsAirlineAircraftCacheService } from 'src/app/@core/services/api/aircraft/ams-airline-aircraft-cache.service';
import { AmsAircraft, AmsAircraftTableCriteria, AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAirline } from 'src/app/@core/services/api/airline/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsFlight, AmsFlightTableCriteria } from 'src/app/@core/services/api/flight/dto';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { IAmsFlightsEstimateDto } from 'src/app/@share/components/dialogs/flight/flight-charter-estimate/flight-charter-estimate.dialog';
import { GridAlFlightsDataSource } from 'src/app/@share/components/grids/grid-al-flights-table/grid-al-flights-table.datasource';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { AirlineInfoService } from 'src/app/airline/info/airline-info.service';
import { GridAdminFlightsDataSource } from './grid-admin-flights.datasource';
import { AmsAirlineAircraftService } from 'src/app/airline/aircraft/al-aircraft.service';

@Component({
    selector: 'ams-grid-admin-flights',
    templateUrl: './grid-admin-flights.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridAdminFlightsComponent implements OnInit, OnDestroy {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsFlight>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    @Output() charterEstimate: EventEmitter<IAmsFlightsEstimateDto> = new EventEmitter<IAmsFlightsEstimateDto>();
    @Output() spmAircraftOpen: EventEmitter<AmsAircraft> = new EventEmitter<AmsAircraft>();
    @Output() spmFlOpen: EventEmitter<AmsFlight> = new EventEmitter<AmsFlight>();

    selFlight: AmsFlight;
    selaircraft: AmsAircraft;
    airport: AmsAirport;


    criteria: AmsFlightTableCriteria;
    criteriaChanged: Subscription;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;


    dataCount = 0;
    dataChanged: Subscription;
    interval: any;
    //
    displayedColumns = ['flId', 'alId', 'acId', 'payloads', 'remainTimeMin', 'acfsId'];
    airline: AmsAirline;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private acCache: AmsAirlineAircraftCacheService,
        private aliService: AirlineInfoService,
        private acService: AmsAirlineAircraftService,
        private spmMapService: MapLeafletSpmService,
        public tableds: GridAdminFlightsDataSource) {

    }


    ngOnInit(): void {

        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsFlight>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsFlightTableCriteria) => {
            this.criteria = this.tableds.criteria;
            console.log('criteriaChanged()->', criteria);
            //this.paginator._changePageSize(this.paginator.pageSize);
            this.loadPage();
        });
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.interval = setInterval(() => {
            this.loadPage();
        }, 60_000);

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.interval) { clearInterval(this.interval); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.airline = this.cus.airline;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.dataCount = this.tableds.itemsCount;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    gotoflightt(flight: AmsFlight) {
        console.log('gotoflightt -> flight:' + flight);
    }

    gotoAircraft(aicraft: AmsAircraft) {
        if (aicraft) {
            this.acService.aircraft = aicraft;
            this.router.navigate([AppRoutes.Root, AppRoutes.airline, AppRoutes.aircaft]);
        }
    }

    clearFiltersClick() {
        const obj = new AmsFlightTableCriteria();
        obj.limit = 12;
        obj.offset = 0;
        obj.sortCol = 'etaTime';
        obj.sortDesc = false;
        this.tableds.criteria = obj;
    }

    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if(airport && airport.apId){
            this.cus.spmAirportPanelOpen.next(airport.apId);
          }
    }



    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);
        if (data) {
            //this.acmService.acMarketMac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    spmAircraftOpenClick(acId: number) {
        if (acId) {
            const aircraft$ = this.acCache.getAircraft(acId);
            aircraft$.subscribe(ac => {
                this.spmAircraftOpen.emit(ac);
            });
        }
    }

    spmMapOpen() {
        this.spmMapService.flights = this.tableds.data;
        this.spmMapService.spmMapPanelOpen.next(true);
    }

    //#endregion



}

