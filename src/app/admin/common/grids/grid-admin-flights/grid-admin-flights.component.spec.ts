import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridAdminFlightsComponent } from './grid-admin-flights.component';

describe('GridAdminFlightsComponent', () => {
  let component: GridAdminFlightsComponent;
  let fixture: ComponentFixture<GridAdminFlightsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAdminFlightsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAdminFlightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
