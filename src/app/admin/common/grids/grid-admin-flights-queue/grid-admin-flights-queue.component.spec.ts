import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridAdminFlightsQueueComponent } from './grid-admin-flights-queue.component';

describe('GridAdminFlightsQueueComponent', () => {
  let component: GridAdminFlightsQueueComponent;
  let fixture: ComponentFixture<GridAdminFlightsQueueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridAdminFlightsQueueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridAdminFlightsQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
