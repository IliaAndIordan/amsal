import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridAdminFlightsComponent } from './grids/grid-admin-flights/grid-admin-flights.component';
import { SxPipesModule } from 'src/app/@core/models/pipes/pipes.module';
import { SxShareModule } from 'src/app/@share/@share.module';
import { SxMaterialModule } from 'src/app/@share/components/material.module';
import { GridAdminFlightsDataSource } from './grids/grid-admin-flights/grid-admin-flights.datasource';
import { GridAdminFlightsQueueComponent } from './grids/grid-admin-flights-queue/grid-admin-flights-queue.component';
import { GridAdminFlightsQueueDataSource } from './grids/grid-admin-flights-queue/grid-admin-flights-queue.datasource';
import { GridAdminTasksComponent } from './grids/grid-admin-tasks/grid-admin-tasks.component';
import { GridAdminTasksDataSource } from './grids/grid-admin-tasks/grid-admin-tasks.datasource';



@NgModule({
  imports: [
    CommonModule,
    SxShareModule,
    SxMaterialModule,
    SxPipesModule,
  ],
  declarations: [
    GridAdminFlightsComponent,
    GridAdminFlightsQueueComponent,
    GridAdminTasksComponent
  ],
  exports: [
    GridAdminFlightsComponent,
    GridAdminFlightsQueueComponent,
    GridAdminTasksComponent
  ],
  providers:[
    GridAdminFlightsDataSource,
    GridAdminFlightsQueueDataSource,
    GridAdminTasksDataSource,
  ]
})
export class AmsAdminCommonModule { }
