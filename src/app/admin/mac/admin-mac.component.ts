

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAdminMacService } from './admin-mac.service';
import { AmsAdminService } from '../admin.service';

// -Models
import { environment } from 'src/environments/environment';
import {
  ExpandTab, PageTransition, ShowHideTriggerBlock,
  ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev
} from '../../@core/const/animations-triggers';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsManufacturerEditDialog } from 'src/app/@share/components/dialogs/mfr-edit/mfr-edit.dialog';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';


@Component({
  templateUrl: './admin-mac.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AmsAdminMacHomeComponent implements OnInit, OnDestroy {

 in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  logo = COMMON_IMG_LOGO_RED;
  amsImgUrl = URL_COMMON_IMAGE_AMS;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  manufacturer: AmsManufacturer;
  manufacturerChanged: Subscription;
  mac: AmsMac;
  macChanged: Subscription;

  //smpUserPanelOpen: Subscription;



  get panelIn(): boolean {
    let rv = this.macService.panelIn;
    //this.panelInVar = rv ? Animate.out : Animate.in;
    return this.macService.panelIn;
  }

  set panelIn(value: boolean) {
    this.macService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    public dialogService: MatDialog,
    private adminService: AmsAdminService,
    private macService: AmsAdminMacService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    this.panelInChanged = this.macService.panelInChanged.subscribe((panelIn: boolean) => {
      this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    });

    this.manufacturerChanged = this.adminService.manufacturerChanged.subscribe((manufacturer: AmsManufacturer) => {
      this.initFields();
    });

    this.macChanged = this.adminService.macChanged.subscribe((mac: AmsMac) => {
      this.initFields();
    });

    this.initFields();

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
    if (this.manufacturerChanged) { this.manufacturerChanged.unsubscribe(); }
    if (this.macChanged) { this.macChanged.unsubscribe(); }
    //if (this.smpUserPanelOpen) { this.smpUserPanelOpen.unsubscribe(); }
  }

  initFields() {
    this.manufacturer = this.macService.manufacturer;
    this.mac = this.macService.mac;
    //console.log('initFields-> user:', this.user);
  }

  //#region  data

  public loadRegions(): void {
    /*
    this.spinerService.show();
    this.wadService.loadRegions()
      .subscribe((resp: ResponseAmsRegionsData) => {
        //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
        this.spinerService.hide();
        this.initFields();
      },
        err => {
          this.spinerService.hide();
          console.log('loadRegions-> err: ', err);
        });
        */
  }
  //#endregion

  //#region Action Methods

  ediManufacturer(data: AmsManufacturer, airport: AmsAirport = undefined) {
    console.log('ediManufacturer-> mfr:', data);
    if (data && data.mfrId !== 0) {
      this.adminService.manufacturer = data;
    }
    const dialogRef = this.dialogService.open(AmsManufacturerEditDialog, {
      width: '721px',
      //height: '620px',
      data: {
        mfr: data,
        airport: airport,
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log('ediManufacturer-> res:', res);
      if (res) {
        //this.table.renderRows();
        //this.loadMfrs();
      }
      this.initFields();
    });


  }

  //#endregion

  //#region  Actions


  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoManufacturer() {
    //console.log('gotoSubregion -> value:', value);
    if (this.manufacturer) {
      //this.wadService.subregion = value;
      this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.manufacturer]);
    }

  }
  gotoManufacturersList() {
      this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.manufacturers]);
  }

  //#endregion

  

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmUserClose(): void {
    //console.log('spmRegionClose ->');
    //this.spmUser.closePanel();
  }

  spmUserOpen() {
    //console.log('spmRegionOpen ->');
    /*
    this.user = this.usersService.selUser;
    if (this.user && this.spmUser) {
      this.spmUser.expandPanel();
    }
    */
  }

  //#endregion
}
