import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsAdminService } from '../../admin.service';
import { AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsAdminMacService } from '../admin-mac.service';
import { URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { PieMiniChartViewModel } from 'src/app/@core/models/common/chart/pie-chart.model';


@Component({
    selector: 'ams-admin-mac-dashboard-tab',
    templateUrl: './admin-mac-dashboard-tab.component.html',
})
export class AmsAdminMacDashboardTabComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Input() tabindex: string;
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();

    acImg = URL_COMMON_IMAGE_AMS_COMMON + 'airline.png';
    /**
     * Fields
     */
    panelIChanged: Subscription;
    region: AmsRegion;
    subregion: AmsSubregion;
    subregionChanged: Subscription;
    country: AmsCountry;
    state:AmsState;
    airport: AmsAirport;
    manufacturer:AmsManufacturer;
    manufacturerChanged: Subscription;
    mac:AmsMac;
    macChanged: Subscription;
    
    selTabIdx:number;
    tabIdxChanged: Subscription;
    productionPieChart:PieMiniChartViewModel;
    nextProducedOn:Date;

    get panelIn(): boolean {
        return this.macService.panelIn;
    }

    set panelIn(value: boolean) {
        this.macService.panelIn = value;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
    private adminService: AmsAdminService,
    private macService: AmsAdminMacService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.panelIChanged = this.macService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.macService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.manufacturerChanged = this.adminService.manufacturerChanged.subscribe((mfr: AmsManufacturer) => {
            this.initFields();
          });

          this.macChanged = this.adminService.macChanged.subscribe((mac: AmsMac) => {
            this.initFields();
          });



        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
        if (this.manufacturerChanged) { this.manufacturerChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.macService.tabIdx;
        this.manufacturer = this.macService.manufacturer;
        this.mac = this.macService.mac;
        this.productionPieChart = PieMiniChartViewModel.fromAmsMac(this.mac);
        console.log('productionPieChart: ',  this.productionPieChart);
        this.nextProducedOn = this.mac?this.mac.nextProducedOn: new Date();
        this.preloader.hide();
    }

    //#region Mobile dialog methods

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> usecase:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.macService.tabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region SPM Methods
    panelInOpen() {
        this.macService.panelIn = true;
    }

    spmProjectOpenClick() {

        // console.log('spmProjectOpen -> spmProjectObj=', this.project);
        /*
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }*/

    }

    //#endregion

}
