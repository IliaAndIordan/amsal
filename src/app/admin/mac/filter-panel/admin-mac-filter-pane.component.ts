import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsAdminMacService } from '../admin-mac.service';
// ---Models
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAdminService } from '../../admin.service';
import { AmsManufacturerEditDialog } from 'src/app/@share/components/dialogs/mfr-edit/mfr-edit.dialog';
import { AmsMac, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';


@Component({
    selector: 'ams-admin-mac-filter-panel-body',
    templateUrl: './admin-mac-filter-pane.component.html',
})
export class AmsAdminMacFilterPanelBodyComponent implements OnInit, OnDestroy {
   
    roots = AppRoutes;
  
    manufacturer: AmsManufacturer;
    manufacturerChanged: Subscription;

    mac:AmsMac;
    macChanged:Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;
    ourApUrl: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private adminService: AmsAdminService,
        private macService: AmsAdminMacService) {

    }


    ngOnInit(): void {

        this.manufacturerChanged = this.adminService.manufacturerChanged.subscribe((mfr: AmsManufacturer) => {
            this.initFields();
        });

        this.tabIdxChanged = this.macService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.macChanged = this.adminService.macChanged.subscribe(mac => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.manufacturerChanged) { this.macChanged.unsubscribe(); }
        if (this.macChanged) { this.manufacturerChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.tabIdx = this.macService.tabIdx;
        this.manufacturer = this.macService.manufacturer;
        this.mac = this.macService.mac;
        //this.ourApUrl = WEB_OURAP_REGION + (this.state && this.state.iso ? this.state.iso.split('-').join('/') + '/' : '');
    }

    //#region Dialogs

    ediManufacturer() {
        //console.log('ediManufacturer-> mfr:', this.manufacturer);
        if (this.manufacturer) {


            const dialogRef = this.dialogService.open(AmsManufacturerEditDialog, {
                width: '721px',
                //height: '620px',
                data: {
                    mfr: this.manufacturer,
                    //airport: this.airport,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediManufacturer-> res:', res);
                if (res) {
                    this.adminService.manufacturer = res;
                } else{
                    this.initFields();
                }
            });
        }


    }
    //#endregion

    //#region SPM

    spmManufacturerOpen(){

    }
    
    spmRegionOpen() {
        //this.wadService.regionPanelOpen.next(true);
    }

    gotoRegion() {
        this.router.navigate([AppRoutes.wad, AppRoutes.region]);
    }

    //#endregion

}
