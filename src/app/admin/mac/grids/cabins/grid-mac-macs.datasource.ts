import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';
import { AmsAdminMacService } from '../../admin-mac.service';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsMacCabin, AmsMacCabinTableCriteria, ResponseAmsMacCabinTableData } from 'src/app/@core/services/api/aircraft/dto';

export const KEY_CRITERIA = 'ams_admin_grid_mac_cabins_criteria';
@Injectable()
export class AmsMacCabinTableDataSource extends DataSource<AmsMacCabin> {

    seleted: AmsMacCabin;

    private _data: Array<AmsMacCabin>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsMacCabin[]> = new BehaviorSubject<AmsMacCabin[]>([]);
    listSubject = new BehaviorSubject<AmsMacCabin[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsMacCabin> {
        return this._data;
    }

    set data(value: Array<AmsMacCabin>) {
        this._data = value;
        this.listSubject.next(this._data as AmsMacCabin[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsAircraftClient,
        private aService: AmsAdminMacService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsMacCabinTableCriteria>();

    public get criteria(): AmsMacCabinTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsMacCabinTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsMacCabinTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsMacCabinTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'macId';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsMacCabinTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsMacCabin[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsMacCabin>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<AmsMacCabin>>(subscriber => {

            this.client.macCabinTable(this.criteria)
                .subscribe((resp: ResponseAmsMacCabinTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsMacCabin>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.cabins && resp.data.cabins.length > 0) {
                            resp.data.cabins.forEach(iu => {
                                const obj = AmsMacCabin.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsMacCabin>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
