import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// --- Services
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsAdminService } from '../../../admin.service';
import { AmsMacCabinTableDataSource } from './grid-mac-macs.datasource';
import { AmsAdminMacService } from '../../admin-mac.service';
// --- Models
import { AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS, URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { RwTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsMac, AmsMacCabin, AmsMacCabinTableCriteria, AmsManufacturer } from 'src/app/@core/services/api/aircraft/dto';
import { AmsMacCabinEditDialog } from 'src/app/@share/components/dialogs/mac-cabin-edit/mac-cabin-edit.dialog';
import { AmsAircraftClient } from 'src/app/@core/services/api/aircraft/api-client';


@Component({
    selector: 'ams-admin-grid-mac-cabins',
    templateUrl: './grid-mac-cabins.component.html',
})
export class AmsGridMacCabinsComponent implements OnInit, OnDestroy, AfterViewInit {
   
    @ViewChild(MatTable, { static: true }) table: MatTable<AmsState>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    seatsFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_f.png';
    seatsBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_b.png';
    seatsEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_e.png';
    pilotImgUrl = URL_COMMON_IMAGE_AMS_COMMON + 'pilot_m_1.png';
    caroImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';
    caro1ImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';
    jumpOneImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/jump-seat-one-tr.png';
    jumpTwoImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/jump-seat-two.png';
    
    typeOpt = RwTypeOpt;
    selMac: AmsMac;
    airport: AmsAirport;
    manufacturer: AmsManufacturer;

    mac: AmsMac;
    macChanged: Subscription;

    criteria: AmsMacCabinTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'cabinId', 'image', 'cName', 'seatF', 'seatB', 'seatE', 'cargoKg', 'seatCrew', 'confort', 'price', 'maxIFEId', 'isDefault', 'acCount'];
    canEdit: boolean;
    filter: string;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private macService: AmsAdminMacService,
        private acClient: AmsAircraftClient,
        private adminService: AmsAdminService,
        public tableds: AmsMacCabinTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {
        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((mscs: Array<AmsMacCabin>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsMacCabinTableCriteria) => {
            this.initFields();
            this.loadPage();
        });


        this.macChanged = this.adminService.macChanged.subscribe((mac: AmsMac) => {
            //console.log(' manufacturerChanged -> = mfr ', mfr);
            this.initFields();
            this.criteria.offset = 0;
            this.criteria.macId = this.macService.mac.macId;
            this.tableds.criteria = this.criteria;
        });

        this.initFields();
        this.criteria.offset = 0;
        this.criteria.macId = this.macService.mac.macId;
        this.tableds.criteria = this.criteria;
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.macChanged) { this.macChanged.unsubscribe(); }
    }

    initFields() {
        //this.company = this.cus.company;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 500);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = 'atime';
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        if (airport && airport.apId) {
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    }


    ediMacCabin(data: AmsMacCabin) {
        console.log('ediMacCabin-> data:', data);
        //console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            const dialogRef = this.dialogService.open(AmsMacCabinEditDialog, {
                //width: '721px',
                //height: '620px',
                data: {
                    cabin: data,
                    mac: this.adminService.mac
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('ediMac-> res:', res);
                if (res) {
                    //this.table.renderRows();
                    this.tableds.loadData()
                        .subscribe(res => {
                            this.initFields();
                        });
                }
                else {
                    this.initFields();
                }


            });

        }
        else {
            this.toastr.info('Please select manufacturer acrcraft model first.', 'Edit Cabin Configuration');
        }


    }

    addCabin() {
        // console.log('inviteUser-> ');
        let dto = new AmsMacCabin();
        dto.macId = this.macService.mac.macId;
        this.ediMacCabin(dto);
    }

    deleteMacCabin(data: AmsMacCabin): void {
        if (data && data.cabinId) {
            this.preloader.show();
            this.acClient.macCabinDelete(data.cabinId).then(res => {
                this.preloader.hide();
                this.refreshClick();
            }).catch(err =>{
                this.preloader.hide();
                console.log('deleteMacCabin -> err:', err);
            });
        }
    }

    gotoMac(data: AmsMac) {
        console.log('gotoMac-> data:', data);

        if (data) {
            this.adminService.mac = data;
            this.router.navigate([AppRoutes.Root, AppRoutes.admin, AppRoutes.mac]);
        }

    }

    //#region Data
    /*
        public loadAirport(apId: number): Observable<AmsAirport> {
            
            this.preloader.show();
            return new Observable<AmsAirport>(subscriber => {
    
                this.aService.loadAirport(apId)
                    .subscribe((resp: AmsAirport) => {
                        //console.log('loadAirport-> resp', resp);
                        subscriber.next(resp);
                        this.preloader.hide();
                    },
                        err => {
                            this.preloader.hide();
                            throw err;
                        });
            });
            
        }
    */
    //#endregion


    /*
    gotoAirport(data: AmsAirport) {
       

        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }

    }*/

    //#endregion
}

