import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGES, URL_COMMON_IMAGE_AMS_COMMON } from "src/app/@core/const/app-storage.const";
import { UserModel } from "src/app/@core/services/auth/api/dto";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { GetJsonDataDialog } from "src/app/@share/components/dialogs/get-json-data/get-jeon-data.dialog";
import { environment } from "src/environments/environment";
import { AmsAdminService } from "../admin.service";
import packageJson from './../../../../package.json';

@Component({
    selector: 'ams-admin-menu-main',
    templateUrl: './admin-menu-main.component.html',
    // animations: [PageTransition]
})
export class AmsAdminMenuMainComponent implements OnInit, OnDestroy {

    @Output() sidebarToggle: EventEmitter<any> = new EventEmitter<any>();

    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    roots = AppRoutes;
    appVersion: string = packageJson.version;

    user: UserModel;
    userName: string;
    userChanged: Subscription;
    avatarUrl: string;
    env: string;
    isAdmin: boolean;

    jsonImgUrl = URL_COMMON_IMAGE_AMS_COMMON + 'cloud-sql-database-.png'; //URL_COMMON_IMAGE_AMS_COMMON + 'pilot_m_1.png';

    constructor(private router: Router,
        private toastr: ToastrService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private adminService: AmsAdminService,) { }

    ngOnInit(): void {
        this.isAdmin = this.cus.isAdmin;
        this.userChanged = this.cus.userChanged.subscribe(user => {

            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.env = environment.abreviation;
        this.avatarUrl = this.cus.avatarUrl;
        let name = 'Guest';
        if (this.cus.user) {
            name = this.cus.user.userName ? this.cus.user.userName : this.cus.user.userEmail;
        }
        this.userName = name;
        // console.log('initFields -> cus.user:', this.cus.user);
    }

    getJsonData() {
        console.log('getJsonData->');
        const dialogRef = this.dialogService.open(GetJsonDataDialog, {
            width: '721px',
            height: '380px',
            data: {
                sql: undefined,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('getJsonData-> res:', res);
            this.initFields();
        });
    }
}
