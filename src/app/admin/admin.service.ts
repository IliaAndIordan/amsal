import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// -Services
import { SxAdminUserClient } from '../@core/services/api/admin-user/api-client';
import { AmsAircraftClient } from '../@core/services/api/aircraft/api-client';
// -Models
import { AppStore, URL_NO_IMG_SQ } from '../@core/const/app-storage.const';
import { CompanyModel, CompanyProductModel } from '../@core/models/common/sx-company.model';
import { LoginsCountModel, ResponseLoginsCount } from '../@core/services/api/admin-user/charts-dto';
import {
    AmsAircraft, AmsMac, AmsManufacturer, AmsManufacturerTableCriteria, AmsManufacturerTableData,
    IAmsManufacturer, ResponseAmsManufacturerTableData
} from '../@core/services/api/aircraft/dto';
import { UserModel } from '../@core/services/auth/api/dto';
// -Services
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { AmsCfgCharter } from '../@core/services/api/flight/charter-dto';
import { AmsFlight } from '../@core/services/api/flight/dto';

@Injectable({
    providedIn: 'root',
})
export class AmsAdminService {

    
    spmCfgCharterPanelOpen = new BehaviorSubject<AmsCfgCharter>(undefined);
    spmAircraftOpen = new BehaviorSubject<AmsAircraft>(undefined);
    spmFlqOpen = new BehaviorSubject<AmsFlight>(undefined);
    spmFlOpen = new BehaviorSubject<AmsFlight>(undefined);


    constructor(
        private cus: CurrentUserService,
        private aUserClient: SxAdminUserClient,
        private acClient: AmsAircraftClient) {
    }

    //#region selUser

    selUserChanged = new Subject<UserModel>();

    get selUser(): UserModel {
        const onjStr = localStorage.getItem(AppStore.adminSelUser);
        let obj: UserModel;
        if (onjStr) {
            obj = Object.assign(new UserModel(), JSON.parse(onjStr));
        }
        return obj;
    }

    set selUser(value: UserModel) {
        const oldValue = this.selUser;
        if (value) {
            localStorage.setItem(AppStore.adminSelUser, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.adminSelUser);
        }

        this.selUserChanged.next(value);
    }

    get selUserAvatarUrl(): string {
        let retValue = URL_NO_IMG_SQ;
        const userObj: UserModel = this.selUser;
        if (userObj && userObj.userEmail) {
            const gravatarImgUrl = this.cus.getAvatarUrl(userObj.userEmail);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    //#endregion

    //#region manufacturer

    manufacturerChanged = new Subject<AmsManufacturer>();

    get manufacturer(): AmsManufacturer {
        const onjStr = localStorage.getItem(AppStore.adminMfr);
        let obj: AmsManufacturer;
        if (onjStr) {
            obj = AmsManufacturer.fromJSON(JSON.parse(onjStr));
        }
        return obj;
    }

    set manufacturer(value: AmsManufacturer) {
        const oldValue = this.manufacturer;
        if (value) {
            localStorage.setItem(AppStore.adminMfr, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.adminMfr);
        }

        this.manufacturerChanged.next(value);
    }

    //#endregion

    //#region mac

    macChanged = new Subject<AmsMac>();

    get mac(): AmsMac {
        const onjStr = localStorage.getItem(AppStore.adminMac);
        let obj: AmsMac;
        if (onjStr) {
            obj = Object.assign(new AmsMac(), JSON.parse(onjStr));
        }
        return obj;
    }

    set mac(value: AmsMac) {
        const oldValue = this.mac;
        if (value) {
            localStorage.setItem(AppStore.adminMac, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.adminMac);
        }

        this.macChanged.next(value);
    }

    //#endregion

    //#region Manufacturers

    get manufacturers(): AmsManufacturer[] {
        const onjStr = localStorage.getItem(AppStore.adminMfrs);
        let obj: AmsManufacturer[];
        if (onjStr) {
            obj = JSON.parse(onjStr);
        }
        return obj;
    }

    set manufacturers(value: AmsManufacturer[]) {
        const oldValue = this.manufacturers;
        if (value) {
            localStorage.setItem(AppStore.adminMfrs, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.adminMfrs);
        }
    }

    loadMfrsAll(): Observable<AmsManufacturer[]> {
        const criteria = new AmsManufacturerTableCriteria();
        criteria.pageIndex = 0;
        criteria.limit = 100;
        return new Observable<AmsManufacturer[]>(subscriber => {

            this.acClient.mfrTable(criteria)
                .subscribe((resp: ResponseAmsManufacturerTableData) => {
                    if (resp && resp.success) {
                        if (resp.data) {
                            let mfrs = new Array<AmsManufacturer>();
                            resp.data.manufacturers.forEach((reg: IAmsManufacturer) => {
                                const val = AmsManufacturer.fromJSON(reg);
                                mfrs.push(val);
                            });
                            this.manufacturers = mfrs;
                        }
                        subscriber.next(this.manufacturers);
                    } else {
                        subscriber.next(undefined);
                    }
                },
                    err => {

                        throw err;
                    });
        });
    }

    //#endregion


    //#region Admin User Charts
    usersCount: number;
    loginsCountData: ResponseLoginsCount;
    public loadLoginsCount(): Observable<LoginsCountModel[]> {

        return new Observable<LoginsCountModel[]>(subscriber => {

            this.aUserClient.loginsCountSeventDays()
                .subscribe((resp: ResponseLoginsCount) => {
                    this.loginsCountData = resp;
                    if (resp) {
                        if (resp.data && resp.data.totals) {
                            this.usersCount = resp.data.totals[0].total_rows;
                        }
                        const data = LoginsCountModel.dataToList(resp.data);

                        subscriber.next(data);
                    } else {
                        subscriber.next(undefined);
                    }
                },
                    err => {

                        throw err;
                    });
        });

    }

    //#endregion

    //#region SPM


    //#endregion


}
