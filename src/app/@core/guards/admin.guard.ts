import { Injectable } from '@angular/core';
import { Router, Route, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppRoutes } from '../const/app-routes.const';
import { CurrentUserService } from '../services/auth/current-user.service';
import { TokenService } from '../services/auth/token.service';

@Injectable({providedIn:'root'})
export class AdminGuard  {

  constructor(private router: Router,
              private cus: CurrentUserService,
              private tokenService: TokenService) { }

  canLoad(route: Route) {
    return true;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.cus.isAuthenticated && this.cus.user && this.cus.isAdmin) {
      return true;
    }

    this.router.navigate(['/', AppRoutes.airline]);
    //this.router.navigate(['/', AppRoutes.public], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
