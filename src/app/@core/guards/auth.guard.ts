import { Injectable } from '@angular/core';
import { Router, Route, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppRoutes } from '../const/app-routes.const';
import { CurrentUserService } from '../services/auth/current-user.service';
import { TokenService } from '../services/auth/token.service';

@Injectable({providedIn:'root'})
export class AuthGuard  {

  constructor(private router: Router,
              private cus: CurrentUserService,
              private tokenService: TokenService) { }

  canLoad(route: Route) {
    return true;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.cus.isAuthenticated && this.cus.user) {
      if(this.cus.airline){
        return true;
      }else{
        //console.log('canActivate -> url:', state.url);
        let canActivate = false;
        switch(state.url){
          case '/'+ AppRoutes.airline:
            //console.log('canActivate -> AppRoutes.airline:', state.url);
            this.router.navigate(['/', AppRoutes.airline, AppRoutes.create]);
            canActivate = false;
            break;
            case '/'+ AppRoutes.airline+'/'+ AppRoutes.create:
            //console.log('canActivate -> AppRoutes.create:', state.url);
            canActivate = true;
            break;
            default:
              //console.log('canActivate -> default:', state.url);
              this.router.navigate(['/', AppRoutes.airline, AppRoutes.create]);
              canActivate = false;
              break;
        }
        return canActivate;
        
      }
      
    }

    this.router.navigate(['/', AppRoutes.public], { queryParams: { redirectTo: state.url } });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
