import { SxProjectStatus } from './enums';
import { IUserTimestampModel } from '../../auth/api/dto';
import { ResponseModel } from 'src/app/@core/models/common/responce.model';

//#region SxProjectModel

export class SxProjectModel {
    public project_id: number;
    public project_name: string;
    public pstatus_id: SxProjectStatus;
    public company_id: number;
    public project_account_id: string;

    public estimator_id: number;
    public estimator_name: string;
    public estimator_email: string;

    public manager_id: number;
    public manager_name: string;
    public manager_email: string;

    public notes: string;

    public adate: Date;
    public udate: Date;
    public start_date: Date;
    public end_date: Date;


    public static fromJSON(json: ISxProjectModel): SxProjectModel {
        const vs = Object.create(SxProjectModel.prototype);
        return Object.assign(vs, json, {
            pstatus_id: json.pstatus_id ? json.pstatus_id as SxProjectStatus : SxProjectStatus.Backlog,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            start_date: (json && json.start_date) ? new Date(json.start_date) : undefined,
            end_date: (json && json.end_date) ? new Date(json.end_date) : undefined
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? SxProjectModel.fromJSON(value) : value;
    }

    public toJSON(): ISxProjectModel {
        const vs = Object.create(SxProjectModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ISxProjectModel {
    project_id: number;
    project_name: string;
    pstatus_id: number;
    company_id: number;
    project_account_id: string;

    estimator_id: number;
    estimator_name: string;
    estimator_email: string;

    manager_id: number;
    manager_name: string;
    manager_email: string;

    notes: string;

    adate: string;
    udate: string;
    start_date: string;
    end_date: string;
}

export class ResponseSxProjectData {
    project: ISxProjectModel;

}


export class ResponseSxProject extends ResponseModel {
    data: ResponseSxProjectData;
}

export class ResponseSxProjectSaveData {
    project: ISxProjectModel[];

}

export class ResponseSxProjectSave extends ResponseModel {
    data: ResponseSxProjectSaveData;
}


export class ResponseSxProjectTableData {
    project_list: ISxProjectModel[];
    totals: {
        total_rows: number;
    };
}

export class ResponseSxProjectTable extends ResponseModel {
    data: ResponseSxProjectTableData;
}

//#endregion

//#region SxProjectModel

export class SxUseCaseModel {
    public usecase_id: number;
    public project_id: number;
    public project_name: string;
    public project_account_id: string;

    public usecase_name: string;
    public usecase_order: number;
    public pstatus_id: SxProjectStatus;

    public usecase_notes: string;
    public usecase_img_url: string;

    public adate: Date;
    public user_id: number;
    public user_name: string;
    public e_mail: string;
    public udate: Date;


    public static fromJSON(json: ISxUseCaseModel): SxUseCaseModel {
        const vs = Object.create(SxUseCaseModel.prototype);
        return Object.assign(vs, json, {
            pstatus_id: json.pstatus_id ? json.pstatus_id as SxProjectStatus : SxProjectStatus.Backlog,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? SxUseCaseModel.fromJSON(value) : value;
    }

    public toJSON(): ISxUseCaseModel {
        const vs = Object.create(SxUseCaseModel.prototype);
        return Object.assign(vs, this);
    }
}

export interface ISxUseCaseModel {
    usecase_id: number;
    project_id: number;
    project_name: string;
    project_account_id: string;

    usecase_name: string;
    usecase_order: number;
    pstatus_id: number;

    usecase_notes: string;
    usecase_img_url: string;

    adate: Date;
    user_id: number;
    user_name: string;
    e_mail: string;
    udate: Date;
}

export class ResponseSxUseCasetData {
    usecase: ISxUseCaseModel;

}


export class ResponseSxUseCase extends ResponseModel {
    data: ResponseSxUseCasetData;
}

export class ResponseSxUseCaseSaveData {
    usecase: ISxUseCaseModel[];

}

export class ResponseSxUseCasetSave extends ResponseModel {
    data: ResponseSxUseCaseSaveData;
}


export class ResponseSxUseCaseTableData {
    usecase_table: ISxUseCaseModel[];
    totals: {
        total_rows: number;
    };
}

export class ResponseSxUseCaseTable extends ResponseModel {
    data: ResponseSxUseCaseTableData;
}

//#endregion
