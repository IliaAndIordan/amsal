import { Pipe, PipeTransform } from '@angular/core';
import { EnumViewModel } from 'src/app/@core/models/common/enum-view.model';

//#region SxProjectStatus

export enum SxProjectStatus {
    Backlog = 1,
    Active = 2,
    Compleated = 3,
    Archive = 4,
}


export const SxProjectStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Backlog'
    },
    {
        id: 2,
        name: 'Active'
    },
    {
        id: 3,
        name: 'Compleated'
    },
    {
        id: 4,
        name: 'Archive'
    },
];


@Pipe({ name: 'sxprojectstatus' })
export class SxProjectStatusDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = SxProjectStatus[value];
        const data: EnumViewModel = SxProjectStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion


