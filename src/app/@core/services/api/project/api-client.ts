import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { ResponseSxProject, ResponseSxProjectSave, ResponseSxProjectTable, 
    ResponseSxUseCaseTable, ResponseSxUseCasetSave, SxProjectModel, 
    SxUseCaseModel } from './dto';
import { environment } from 'src/environments/environment';

// Models

// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({providedIn:'root'})
export class SxApiProjectClient extends ApiBaseClient {

    constructor(
        private client: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    private get sertviceUrl(): string {
        return environment.services.url.project;
    }

    //#region Project

    projectById(id: number): Observable<any> {
        // const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = this.sertviceUrl + '/project';
        const req = { project_id: id };
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                observe: 'response',
            }),
        };

        return this.client.post<HttpResponse<any>>(url, req, httpOptions)
            .pipe(
                map((response: HttpResponse<any>) => {
                    console.log('projectById-> response=', response);
                    let rv: SxProjectModel;
                    if (response.body) {
                        const res = Object.assign(new ResponseSxProject(), response.body);
                        console.log('projectById-> res=', res);
                        if (res && res.data && res.data.project) {
                            rv = SxProjectModel.fromJSON(res.data.project);
                        }

                    }
                    return rv;
                }),
                catchError(this.handleError)
            );
    }

    projectSave(value: SxProjectModel): Observable<SxProjectModel> {
        // value.actor_id = this.cus.user.user_id;
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'project_save',
            { project: value },
            { headers: hdrs }).pipe(
                map((response: ResponseSxProjectSave) => {
                    const res = Object.assign(new ResponseSxProjectSave(), response);
                    let rv: SxProjectModel;
                    if (res && res.success) {
                        if (res && res.data && res.data.project) {
                            rv = SxProjectModel.fromJSON(res.data.project[0]);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Project save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }


    projectTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'project_name',
        isDesc: boolean = false,
        companyId?: number,
        filter?: string): Observable<ResponseSxProjectTable> {

        console.log('projectTable-> offset=', offset);
        console.log('projectTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'project_table',
            {
                company_id: companyId,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseSxProjectTable) => {
                    const res = Object.assign(new ResponseSxProjectTable(), resp);
                    console.log('projectTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap projectTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }


    //#endregion

    //#region UseCase

    usecaseSave(value: SxUseCaseModel): Observable<SxUseCaseModel> {
        // value.actor_id = this.cus.user.user_id;
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'usecase_save',
            { usecase: value },
            { headers: hdrs }).pipe(
                map((response: ResponseSxUseCasetSave) => {
                    const res = Object.assign(new ResponseSxUseCasetSave(), response);
                    let rv: SxUseCaseModel;
                    if (res && res.success) {
                        if (res && res.data && res.data.usecase) {
                            rv = SxUseCaseModel.fromJSON(res.data.usecase[0]);
                        }
                    }
                    return rv;
                }),
                tap(event => {
                    this.log(`tap Project save event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );

    }


    usecaseTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'usecase_order',
        isDesc: boolean = false,
        projectId?: number,
        filter?: string): Observable<ResponseSxUseCaseTable> {

        console.log('usecaseTable-> offset=', offset);
        console.log('usecaseTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'usecase_table',
            {
                project_id: projectId,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseSxUseCaseTable) => {
                    const res = Object.assign(new ResponseSxUseCaseTable(), resp);
                    console.log('usecaseTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap usecaseTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion


}
