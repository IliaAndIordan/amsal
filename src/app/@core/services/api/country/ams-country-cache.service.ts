import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map, shareReplay } from "rxjs/operators";
import { CurrentUserService } from "../../auth/current-user.service";
import { AmsCountryClient } from "./api-client";
import { AmsCountry, AmsCountryTableCriteria, ResponseAmsCountryTableData } from "./dto";

@Injectable({ providedIn: 'root' })
export class AmsCountryCacheService {

  countryCache$: Observable<ResponseAmsCountryTableData>;

  constructor(
    private cus: CurrentUserService,
    private client: AmsCountryClient) {
  }

  get airportTable(): Observable<ResponseAmsCountryTableData> {
    if (!this.countryCache$) {

      this.countryCache$ = this.countryTable().pipe(shareReplay());
    }
    return this.countryCache$;
  }

  get countries(): Observable<AmsCountry[]>{
    return this.airportTable.pipe( 
      map( apTable => {
        let rv = new Array<AmsCountry>();
        if(apTable && apTable.data && apTable.data.countries){
          for (const ap of apTable.data.countries) {
            const airport = AmsCountry.fromJSON(ap);
            rv.push(airport);
          }
        }
        console.log('countries -> rv:', rv);
        return rv;
        
     })
     );
}

  getCountry(id: number): Observable<AmsCountry>{
    return this.countries.pipe(
        map(aps => aps.find(aps => aps.countryId == id))
    );
}

  countryTable(): Observable<ResponseAmsCountryTableData> {
    const criteria = new AmsCountryTableCriteria()
    criteria.limit = 200000;
    criteria.offset = 0;

    return this.client.countryTableC(criteria)
  }

}