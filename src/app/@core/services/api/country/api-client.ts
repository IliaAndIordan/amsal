import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { firstValueFrom, lastValueFrom, Observable, of, Subscription } from 'rxjs';
import { catchError, map, take, tap } from 'rxjs/operators';
// Services
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { environment } from 'src/environments/environment';
import { AmsCity, AmsCityTableCriteria, AmsCityTableData, AmsCountCityNoDemandResponce, AmsCountry, AmsCountryTableCriteria, AmsState, ResponseAmsCityTableData, ResponseAmsCountryTableData, ResponseAmsRegionsData, ResponseAmsStateTableData, ResponseCityDelete, ResponseCitySave, ResponseCountrySave, ResponseStateDelete, ResponseStateSave, RowsCountModel } from './dto';


// Models

@Injectable({ providedIn: 'root' })
export class AmsCountryClient extends ApiBaseClient {

  constructor(
    private client: HttpClient,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return environment.services.url.country;
  }

  //#region  AmsRegions

  Regions(): Observable<ResponseAmsRegionsData> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'regions',
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsRegionsData) => {
          // console.log('companyUsers-> response=', resp);
          const res = Object.assign(new ResponseAmsRegionsData(), resp);
          return res;
        }),
        tap(event => {
          this.log(`tap loginsCountSeventDays event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

  //#region Country

  countryTable(
    limit: number = 25,
    offset: number = 0,
    orderCol: string = 'country_name',
    isDesc: boolean = false,
    subregionId?: number,
    filter?: string): Observable<ResponseAmsCountryTableData> {

    // console.log('countryTable-> offset=', offset);
    // console.log('countryTable-> limit=', limit);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'country_table',
      {
        subregionId: subregionId,
        qry_offset: offset,
        qry_limit: limit,
        qry_orderCol: orderCol,
        qry_isDesc: isDesc,
        qry_filter: filter,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsCountryTableData) => {
          const res = Object.assign(new ResponseAmsCountryTableData(), resp);
          // console.log('countryTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap countryTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  countryTableC(criteria: AmsCountryTableCriteria): Observable<ResponseAmsCountryTableData> {

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'country_table',
      {
        subregionId: criteria.subregionId,
        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsCountryTableData) => {
          const res = Object.assign(new ResponseAmsCountryTableData(), resp);
          // console.log('countryTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap countryTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  countrySave(value: AmsCountry): Observable<AmsCountry> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'country_save',
      { country: value },
      { headers: hdrs }).pipe(
        map((response: ResponseCountrySave) => {
          const res = Object.assign(new ResponseCountrySave(), response);
          let rv: AmsCountry;
          if (res && res.success) {
            if (res && res.data && res.data.country) {
              rv = AmsCountry.fromJSON(res.data.country);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap country Save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

  //#region State

  stateTable(
    limit: number = 25,
    offset: number = 0,
    orderCol: string = 'country_name',
    isDesc: boolean = false,
    countryId?: number,
    filter?: string): Observable<ResponseAmsStateTableData> {

    //console.log('stateTable-> offset=', offset);
    //console.log('stateTable-> limit=', limit);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'state_table',
      {
        countryId: countryId,
        qry_offset: offset,
        qry_limit: limit,
        qry_orderCol: orderCol,
        qry_isDesc: isDesc,
        qry_filter: filter,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsStateTableData) => {
          const res = Object.assign(new ResponseAmsStateTableData(), resp);
          //console.log('stateTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap countryTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  stateGet(value: number): Observable<AmsState> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'state_get',
      { stateId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseStateSave) => {
          const res = Object.assign(new ResponseStateSave(), response);
          let rv: AmsState;
          if (res && res.success) {
            if (res && res.data && res.data.state) {
              rv = AmsState.fromJSON(res.data.state);
            }
          }
          return rv;
        }),
        catchError(this.handleError)
      );
  }

  stateSave(value: AmsState): Observable<AmsState> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'state_save',
      { state: value },
      { headers: hdrs }).pipe(
        map((response: ResponseStateSave) => {
          const res = Object.assign(new ResponseStateSave(), response);
          let rv: AmsState;
          if (res && res.success) {
            if (res && res.data && res.data.state) {
              rv = AmsState.fromJSON(res.data.state);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap country Save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  stateDelete(value: number): Observable<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'state_delete',
      { stateId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseStateDelete) => {
          const res = Object.assign(new ResponseStateDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.stateId) {
              rv = res.data.stateId;
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap country Save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

  //#region City
 
  async cityTableReq(req:AmsCityTableCriteria):Promise<ResponseAmsCityTableData>{

    //console.log('cityTableReq -> req:', req);
    const url = this.sertviceUrl + 'city_table';
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //

    const source$ = this.client.post(url, 
      {
        stateId: req.stateId,
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
      },
      { headers: hdrs })
      .pipe(
        take(1),
        map((response: ResponseAmsCityTableData) => {
          //console.log('cityTableReq -> response:', response);
          const resp = Object.assign(new ResponseAmsCityTableData(), response);
          let rv = new AmsCityTableData();
          rv.cities = new Array<AmsCity>();
          rv.rowsCount = Object.assign( new RowsCountModel(), resp?.data?.rowsCount);
          if(resp?.data?.cities?.length>0){
            resp.data.cities.forEach(city => {
              rv.cities.push(AmsCity.fromJSON(city));
            });
          }
          resp.data = rv;
          return resp;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<ResponseAmsCityTableData>(source$);

  }

  citySave(value: AmsCity): Observable<AmsCity> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'city_save',
      { city: value },
      { headers: hdrs }).pipe(
        map((response: ResponseCitySave) => {
          const res = Object.assign(new ResponseCitySave(), response);
          let rv: AmsCity;
          if (res && res.success) {
            if (res && res.data && res.data.city) {
              rv = AmsCity.fromJSON(res.data.city);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap city Save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  cityDelete(value: number): Observable<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'city_delete',
      { cityId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseCityDelete) => {
          const res = Object.assign(new ResponseCityDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.cityId) {
              rv = res.data.cityId;
            }
          }
          return rv;
        }),
        catchError(this.handleError)
      );
  }

  getCity(value: number): Observable<AmsCity> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'city_get',
      { cityId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseCitySave) => {
          const res = Object.assign(new ResponseCitySave(), response);
          let rv: AmsCity;
          if (res && res.success) {
            if (res && res.data && res.data.city) {
              rv = AmsCity.fromJSON(res.data.city);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap city get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }


  async countCityNoDemend(): Promise<AmsCountCityNoDemandResponce> {

    //console.log('countCityNoDemend ->');
    const url = this.sertviceUrl + 'count_city_no_demand';
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    //const promise = new Promise<AmsCountCityNoDemandResponce>(async (resolve, reject) => {
    //console.log('cityTable-> limit=', limit);
    const source$ = this.client.post(url, {}, { headers: hdrs })
      .pipe(
        take(1),
        map((response: AmsCountCityNoDemandResponce) => {
          //console.log('countCityNoDemend -> subscribe response:', response);
          const res = Object.assign(new AmsCountCityNoDemandResponce(), response);
          return res;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsCountCityNoDemandResponce>(source$);
    //});

    //return promise
  }

  async calcCityPayloadDemend(): Promise<AmsCountCityNoDemandResponce> {

    //console.log('calcCityPayloadDemend ->');
    const url = this.sertviceUrl + 'calc_city_payload_demand';
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //

    const source$ = this.client.post(url, {}, { headers: hdrs })
      .pipe(
        take(1),
        map((response: AmsCountCityNoDemandResponce) => {
          //console.log('calcCityPayloadDemend -> response:', response);
          const res = Object.assign(new AmsCountCityNoDemandResponce(), response);
          return res;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsCountCityNoDemandResponce>(source$);

  }

  async calcCityPayloadDemendCityId(value:number): Promise<AmsCountCityNoDemandResponce> {

    //console.log('calcCityPayloadDemend ->');
    const url = this.sertviceUrl + 'calc_city_payload_demand_city_id';
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //

    const source$ = this.client.post(url, { cityId: value }, { headers: hdrs })
      .pipe(
        take(1),
        map((response: AmsCountCityNoDemandResponce) => {
          //console.log('calcCityPayloadDemend -> response:', response);
          const res = Object.assign(new AmsCountCityNoDemandResponce(), response);
          return res;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsCountCityNoDemandResponce>(source$);

  }

  //#endregion


}

