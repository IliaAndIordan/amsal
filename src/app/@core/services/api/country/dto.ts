import { URL_COMMON_IMAGE_FLAGS } from "src/app/@core/const/app-storage.const";
import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { AmsStatus, WadStatus } from "src/app/@core/models/pipes/ams-status.enums";

//#region Region and Subregion

export class AmsRegion {
    regionId: number;
    rCode: string;
    rName: string;
    rWikiUrl: string;
    subregions: number;
    countries: number;
    states: number;
    airports:number;
    apActive:number;

    public static fromJSON(json: IAmsRegion): AmsRegion {
        const vs = Object.create(AmsRegion.prototype);
        return Object.assign(vs, json, {
            regionId: json.regionId ? json.regionId : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsRegion.fromJSON(value) : value;
    }

}

export interface IAmsRegion {
    regionId: number;
    rCode: string;
    rName: string;
    rWikiUrl: string;
    subregions: number;
    countries: number;
    states: number;
    airports:number;
    apActive:number;
}

export class AmsSubregion {
    subregionId: number;
    srCode: string;
    srName: string;
    srWikiUrl: string;
    regionId: number;
    srImageUrl: string;
    countries: number;
    states: number;
    airports:number;
    apActive:number;

    public static fromJSON(json: IAmsSubregion): AmsSubregion {
        const vs = Object.create(AmsSubregion.prototype);
        return Object.assign(vs, json, {
            subregionId: json.subregionId ? json.subregionId : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsSubregion.fromJSON(value) : value;
    }

}

export interface IAmsSubregion {
    subregionId: number;
    srCode: string;
    srName: string;
    srWikiUrl: string;
    regionId: number;
    srImageUrl: string;
    countries: number;
    states: number;
    airports:number;
    apActive:number;
}

export interface IAmsRegion {
    regionId: number;
    rcode: string;
    rname: string;
    rwikiUrl: string;
    subregions:number;
    countries:number;
    states:number;
    airports:number;
    apActive:number;
}

export class AmsRegionsData {
    regions: IAmsRegion[];
    subregions: IAmsSubregion[];
}

export class ResponseAmsRegionsData extends ResponseModel {
    data: AmsRegionsData;
}

//#endregion

//#region Country

export class AmsCountry {
    countryId: number;
    cCode: string;
    cName: string;

    iso2: string;
    iso3: string;
    icao: string;
    regionId: number;
    subregionId: number;

    wikiUrl: string;
    imageUrl: string;
    amsStatus: AmsStatus;
    wadStatus: WadStatus;
    udate: Date;
    statesCount: number;
    airports?:number;
    apActive?:number;

    flagUrl: string;

    public static fromJSON(json: IAmsCountry): AmsCountry {
        const vs = Object.create(AmsCountry.prototype);
        return Object.assign(vs, json, {
            amsStatus: json.amsStatus ? json.amsStatus as  AmsStatus: AmsStatus.DataCheckRequired,
            wadStatus: json.wadStatus ? json.wadStatus as  AmsStatus: WadStatus.ToDo,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            flagUrl: URL_COMMON_IMAGE_FLAGS + json.iso2.toLowerCase() + '.png',
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsRegion.fromJSON(value) : value;
    }

}

export interface IAmsCountry {
    countryId: number;
    cCode: string;
    cName: string;

    iso2: string;
    iso3: string;
    icao: string;
    regionId: number;
    subregionId: number;

    wikiUrl: string;
    imageUrl: string;
    amsStatus: number;
    wadStatus: number;
    udate: string;
    statesCount: number;
    airports:number;
    apActive:number;
}

export class AmsCountryTableCriteria extends TableCriteriaBase {
    subregionId: number;
}
export class RowsCountModel{
    totalRows: number;
}
export class AmsCountryTableData {
    countries: IAmsCountry[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsCountryTableData extends ResponseModel {
    data: AmsCountryTableData;
}


export class ResponseCountrySaveData {
    country: IAmsCountry;

}

export class ResponseCountrySave extends ResponseModel {
    data: ResponseCountrySaveData;
}



//#endregion

//#region State

export class AmsState {
    stateId: number;
    stName: string;
    iso: string;
    countryId: number;
    wikiUrl: string;
    amsStatus: AmsStatus;
    wadStatus: WadStatus;
    udate: Date;
    cities: number;
    airports?:number;
    apActive:number;


    public static fromJSON(json: IAmsState): AmsState {
        const vs = Object.create(AmsState.prototype);
        return Object.assign(vs, json, {
            amsStatus: json.amsStatus ? json.amsStatus as  AmsStatus: AmsStatus.DataCheckRequired,
            wadStatus: json.wadStatus ? json.wadStatus as  AmsStatus: WadStatus.ToDo,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            // flagUrl: URL_COMMON_IMAGE_FLAGS + json.iso2.toLowerCase() + '.png',
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsState.fromJSON(value) : value;
    }

}

export interface IAmsState {
    stateId: number;
    stName: string;
    iso: string;
    countryId: number;
    wikiUrl: string;
    amsStatus: number;
    wadStatus: number;
    udate: Date;
    cities: number;
    airports?:number;
    apActive:number;
}

export class AmsStateTableCriteria extends TableCriteriaBase {
    countryId: number;
}

export class AmsStateTableData {
    states: IAmsState[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsStateTableData extends ResponseModel {
    data: AmsStateTableData;
}


export class ResponseStateSaveData {
    state: IAmsState;
}

export class ResponseStateSave extends ResponseModel {
    data: ResponseStateSaveData;
}

export class StateDeleteData {
    stateId: number;
}

export class ResponseStateDelete extends ResponseModel {
    data: StateDeleteData;
}

//#endregion


//#region City

export class AmsCity {
    cityId: number;
    ctName: string;
    population: number;
    countryId: number;
    stateId: number;
    wikiUrl: string;
    amsStatus: AmsStatus;
    wadStatus: WadStatus;
    udate: Date;
    stateCapital: boolean;
    countryCapital:boolean;
    
    airports?:number;
    apActive:number;

    paxDemandDay:number;
    paxDemandPeriod:number;
    cargoDemandPeriodKg:number;
    paxDemandPeriodDays:number;
    apTypeSum:number;


    public static fromJSON(json: any): AmsCity {
        const vs = Object.create(AmsCity.prototype);
        return Object.assign(vs, json, {
            amsStatus: json.amsStatus ? json.amsStatus as  AmsStatus: AmsStatus.DataCheckRequired,
            wadStatus: json.wadStatus ? json.wadStatus as  AmsStatus: WadStatus.ToDo,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            stateCapital: (json && json.stateCapital) ? (json.stateCapital) !== 0: false,
            countryCapital: (json && json.countryCapital) ? json.countryCapital !== 0: false,
            // flagUrl: URL_COMMON_IMAGE_FLAGS + json.iso2.toLowerCase() + '.png',
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsState.fromJSON(value) : value;
    }

}

export interface IAmsCity {
    cityId: number;
    ctName: string;
    population: number;
    countryId: number;
    stateId: number;
    wikiUrl: string;
    amsStatus: number;
    wadStatus: number;
    udate: string;
    stateCapital: number;
    countryCapital:number;
    
    airports?:number;
    apActive:number;

    paxDemandDay:number;
    paxDemandPeriod:number;
    cargoDemandPeriodKg:number;
    paxDemandPeriodDays:number;
    apTypeSum:number;
}

export class AmsCityTableCriteria extends TableCriteriaBase {
    stateId: number;
}

export class AmsCityTableData {
    cities: AmsCity[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsCityTableData extends ResponseModel {
    data: AmsCityTableData;
}

export class AmsCountCityNoDemandData {
    rowsCount: number;
}
export class AmsCountCityNoDemandResponce extends ResponseModel {
    data: AmsCountCityNoDemandData;
}


export class ResponseCitySaveData {
    city: IAmsCity;
}

export class ResponseCitySave extends ResponseModel {
    data: ResponseCitySaveData;
}

export class CityStateDeleteData {
    cityId: number;
}

export class ResponseCityDelete extends ResponseModel {
    data: CityStateDeleteData;
}

//#endregion