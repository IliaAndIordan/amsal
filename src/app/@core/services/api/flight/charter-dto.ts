import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_FLAGS, URL_NO_IMG } from "src/app/@core/const/app-storage.const";
import { DateModel } from "src/app/@core/models/common/date.model";
import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { AmsStatus } from "src/app/@core/models/pipes/ams-status.enums";
import { TsDatePipe } from "src/app/@core/models/pipes/ts-date.pipe";
import { RowsCountModel } from "../country/dto";
import { AmsFlightType, AmsPayloadType } from "./enums";

//#region AmsFlight

export class AmsCfgCharter {
    public charterId: number;
    public depApId: number;
    public arrApId: number;
    public flightName: string;
    public flightDescription: number;
    public payloadType: AmsPayloadType;
    public pax?: number;
    public cargoKg?: number;
    public payloadKg?: number;
    public price: number;
    public distanceKm: number;
    public periodDays: number;
    public fuelL: number;
    public lastRun?: Date;
    public nextRun: Date;

    public adate: Date;
    public depApCode: string;
    public arrApCode: string;
    public completed: boolean;
    public paxCompleated: number;
    public cargoKgCompleated: number;
    public payloadKgCompleated: number;
    public priceCompleted: number;
    public expiration: Date;
    public completedPct: number;



    public static fromJSON(json: IAmsCfgCharter): AmsCfgCharter {
        const vs = Object.create(AmsCfgCharter.prototype);
        return Object.assign(vs, json, {
            payloadType: json.payloadType ? AmsPayloadType[json.payloadType] : undefined,
            completed: json.completed !== 0 ? true : false,
            priceCompleted: json.priceCompleted ? json.priceCompleted : 0,
            paxCompleated: json.paxCompleated ? json.paxCompleated : 0,
            cargoKgCompleated: json.cargoKgCompleated ? json.cargoKgCompleated : 0,
            payloadKgCompleated: json.payloadKgCompleated ? json.payloadKgCompleated : 0,
            completedPct: json.completedPct ? json.completedPct : 0,
            //completedPct:json.payloadKgCompleated?(json.payloadKgCompleated/json.payloadKg):0,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            nextRun: (json && json.nextRun) ? new Date(json.nextRun) : undefined,
            lastRun: (json && json.lastRun) ? new Date(json.lastRun) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsCfgCharter.fromJSON(value) : value;
    }

    public toJSON(): IAmsCfgCharter {
        const tzoffset = (new Date()).getTimezoneOffset() * 60000;
        return Object.assign({}, this, {
            payloadType: AmsPayloadType[this.payloadType],
            nextRun: DateModel.getTimeStr(this.nextRun),
        });
    }

}

export interface IAmsCfgCharter {
    charterId: number;
    depApId: number;
    arrApId: number;
    flightName: string;
    flightDescription: number;
    payloadType: string;
    pax?: number;
    cargoKg?: number;
    payloadKg?: number;
    price: number;
    distanceKm: number;
    periodDays: number;
    fuelL: number;
    lastRun?: string;
    nextRun: string;

    adate: string;
    depApCode: string;
    arrApCode: string;

    completed: number;
    paxCompleated: number;
    cargoKgCompleated: number;
    payloadKgCompleated: number;
    priceCompleted: number;
    expiration: Date;
    completedPct:number;
}

export class ResponseCfgCharterData {
    charter: IAmsCfgCharter;
}

export class ResponseCfgCharterGet extends ResponseModel {
    data: ResponseCfgCharterData;
}

export class AmsCfgCharterTableCriteria extends TableCriteriaBase {
    depApId?: number;
    arrApId?: number;
    completed: boolean;
    payloadType: AmsPayloadType;
}

export class ResponceAmsCfgCharterTableData {
    charters: IAmsCfgCharter[];
    rowsCount: RowsCountModel;
}

export class ResponceAmsCfgCharterTable extends ResponseModel {
    data: ResponceAmsCfgCharterTableData;
}


export class AmsCfgCharterDeleteData {
    charterId: number;
}

export class ResponceAmsCfgCharterDelete extends ResponseModel {
    data: AmsCfgCharterDeleteData;
}

//#endregion




