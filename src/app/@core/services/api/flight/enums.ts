import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "src/app/@core/models/common/enum-view.model";

export enum AmsFlightType {
    Schedule = 1,
    Charter = 2,
    Transfer = 3,
}

export const AmsFlightTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Schedule',
        color: 'rgba(51, 204, 51, 0.8)',
    },
    {
        id: 2,
        name: 'Charter',
        color: 'rgba(51, 204, 51, 0.8)',
    },
    {
        id: 3,
        name: 'Transfer ',
        color: 'rgba(51, 204, 51, 0.8)',
    },

];


@Pipe({ name: 'flighttype' })
export class AmsFlightTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsFlightType[value];
        const data: EnumViewModel = AmsFlightTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

export enum AmsPayloadType {
    E=1,
    B=2,
    F=3,
    C=4,
    G=5
}

export const AmsPayloadTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Economy'
    },
    {
        id: 2,
        name: 'Business'
    },
    {
        id: 3,
        name: 'First '
    },
    {
        id: 4,
        name: 'Cargo '
    },
    {
        id: 5,
        name: 'Crew '
    },
];

@Pipe({ name: 'payloadtype' })
export class AmsPayloadTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsPayloadType[value];
        const data: EnumViewModel = AmsPayloadTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}



export enum AmsFlightStatus {
    PlaneStand=1,
    Refuling=2,
    CargoLoad=3,
    Boarding=4,
    TaxyAndTakeoff=5,
    InFlight=6,
    Landing=7,
    Disembarking=8,
    CargoUnload=9,
    Finished=10,
    Finished2=11,
}

export const AmsFlightStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Plane Stand',
        color:'check-in'
    },
    {
        id: 2,
        name: 'Refuling',
        color:'boarding'
    },
    {
        id: 3,
        name: 'Cargo Load',
        color:'boarding'
    },
    {
        id: 4,
        name: 'Boarding',
        color:'boarding'
    },
    {
        id: 5,
        name: 'Taxy and Takeoff',
        color:'taxy'
    },
    {
        id: 6,
        name: 'In Flight',
        color:'on time'
    },
    {
        id: 7,
        name: 'Landing and Taxy to Gate ',
        color:'taxy'
    },
    {
        id: 8,
        name: 'Disembarking',
        color:'landed'
    },
    {
        id: 9,
        name: 'Cargo Unload',
        color:'landed'
    },
    {
        id: 10,
        name: 'Finished ',
        color:'landed'
    },
    {
        id: 11,
        name: 'Finished 2',
        color:'compleated'
    },
];

@Pipe({ name: 'acfl' })
export class AmsFlightStatusDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsFlightStatus[value];
        const data: EnumViewModel = AmsFlightStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

@Pipe({ name: 'acflmon' })
export class AmsFlightStatusMonitorPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsFlightStatus[value];
        const data: EnumViewModel = AmsFlightStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.color;
        }
        return rv;
    }
}