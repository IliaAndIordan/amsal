import { GMAP_API_KEY, URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AIRCRAFT_ICON, URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_FLAGS, URL_NO_IMG } from "src/app/@core/const/app-storage.const";
import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { AircraftStatus } from "src/app/@core/models/pipes/aircraft-statis.pipe";
import { AmsBankTransactionType } from "src/app/@core/models/pipes/ams-bank-transactionType.pipe";
import { AmsStatus } from "src/app/@core/models/pipes/ams-status.enums";
import { TsDatePipe } from "src/app/@core/models/pipes/ts-date.pipe";
import { RowsCountModel } from "../country/dto";
import { AmsFlightStatus, AmsFlightType, AmsPayloadType } from "./enums";
import { AmsAirport, IAmsAirport } from "../airport/dto";

//#region AmsFlight


export class AmsFlightLog {

    public acfsId: number;
    public adate: Date;
    public amount: number;
    public delayMin: number;
    public description: string;
    public flId: number;
    public flLogDescription: string;
    public flLogDistanceKm: number;
    public flLogOpTimeSec: number;
    public flLogTimeMin: number;
    public orderId: number;
    public posted: boolean;
    public trApId: number;
    public trId: number;
    public trTypeId: AmsBankTransactionType;



    public static fromJSON(json: IAmsFlightLog): AmsFlightLog {
        const vs = Object.create(AmsFlightLog.prototype);
        return Object.assign(vs, json, {
            acfsId: json.acfsId ? json.acfsId : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            amount: json.amount ? json.amount : undefined,
            delayMin: json.delayMin ? json.delayMin : undefined,
            posted: json.posted ? json.posted != 0 : false,
            trTypeId:json.trTypeId? json.trTypeId as AmsBankTransactionType:undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsFlightPayload.fromJSON(value) : value;
    }

}

export interface IAmsFlightLog {
    acfsId: number;
    adate: string;
    amount: number;
    delayMin: number;
    description: string;
    flId: number;
    flLogDescription: string;
    flLogDistanceKm: number;
    flLogOpTimeSec: number;
    flLogTimeMin: number;
    orderId: number;
    posted: number;
    trApId: number;
    trId: number;
    trTypeId: number;
}



export class AmsFlightPayload {

    public flplId: number;
    public flId: number;
    public chId: number;
    public payloadType: AmsPayloadType;
    public pax: number;
    public cargoKg: number;
    public payloadKg: number;
    public description: string;
    public price: number;
    public adate: Date;



    public static fromJSON(json: IAmsFlightPayload): AmsFlightPayload {
        const vs = Object.create(AmsFlightPayload.prototype);
        return Object.assign(vs, json, {
            flplId: json.flplId ? json.flplId : undefined,
            flId: json.flId ? json.flId : undefined,
            chId: json.chId ? json.chId : undefined,
            payloadType: json.payloadType ? AmsPayloadType[json.payloadType] : undefined,
            pax: json.pax ? json.pax : 0,
            cargoKg: json.cargoKg ? json.cargoKg : 0,
            payloadKg: json.payloadKg ? json.payloadKg : 0,
            description: json.description ? json.description : 0,
            price: json.price ? json.price : 0,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsFlightPayload.fromJSON(value) : value;
    }

}

export interface IAmsFlightPayload {
    flplId: number;
    flId: number;
    chId: number;
    payloadType: string;
    pax: number;
    cargoKg: number;
    payloadKg: number;
    description: string;
    price: number;
    adate: string;
}


export class AmsFlight {
    public flId: number;
    public flTypeId: AmsFlightType;
    public flName: string;
    public flDescription: string;
    public fleId?: number;
    public flpId?: number;
    public routeId?: number;
    public chId?: number;

    public alId: number;
    public acId: number;
    public acRegNr: string;
    public depApId: number;
    public arrApId: number;
    public distanceKm: number;
    public fuelL: number;
    public pax?: number;
    public payloadKg: number;

    public etdTime?: Date;
    public etaTime?: Date;
    public dtime?: Date;
    public atime?: Date;
    public delayMin: number;
    public expences?: number;
    public income?: number;
    public revenue?: number;

    public flLogId: number;
    public acfsId: AmsFlightStatus;
    public flLogDescription: string;
    public flTimeMin?: number;
    public flDistanceKm?: number;
    public flLogOpTimeSec?: number;
    public flLogDelayMin?: number;
    public adate: Date;
    public udate: Date;

    public macId?: number;
    public acTypeId: number;
    public speedKmPh?: number;
    public remainDistanceKm?: number;
    public remainTimeMin?: number;
    public alName?: string;
    public alIata?: string;
    public depApCode?: string;
    public arrApCode?: string;

    public amsLogoUrl: string;
    public amsLiveryUrl: string;

    public payloads: AmsFlightPayload[];
    public acImgTableUrl: string;
    public acImgMapUrl: string;

    public chCompletedPct:number;
    public chPaxLeft:number;
    public chCargoLeft:number;

    public flightLogs: AmsFlightLog[];

    get flDurationMin(): number{
        let rv:number = 0;
        if(this.dtime && this.atime){
            rv = (this.atime.getTime() - this.dtime.getTime())/(60*1000);
        } else if(this.etdTime && this.etaTime){
            rv = (this.etaTime.getTime() - this.etdTime.getTime())/(60*1000);
        }
        return rv;
    }

    public static fromJSON(json: IAmsFlight): AmsFlight {
        const vs = Object.create(AmsFlight.prototype);
        return Object.assign(vs, json, {
            flId: json.flId ? json.flId : undefined,
            flTypeId: json.flTypeId ? json.flTypeId as AmsFlightType : AmsFlightType.Charter,
            etdTime: (json && json.etdTime) ? new Date(json.etdTime) : undefined,
            etaTime: (json && json.etaTime) ? new Date(json.etaTime) : undefined,
            dtime: (json && json.dtime) ? new Date(json.dtime) : undefined,
            atime: (json && json.atime) ? new Date(json.atime) : undefined,
            amsLogoUrl: URL_COMMON_IMAGE_AIRLINE + 'logo_' + json.alId.toString() + '.png',
            amsLiveryUrl: URL_COMMON_IMAGE_AIRLINE + 'livery_' + json.alId.toString() + '.png',
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            acfsId: json.acfsId ? json.acfsId as AmsFlightStatus : AmsFlightStatus.PlaneStand,
            acImgTableUrl: URL_COMMON_IMAGE_AIRCRAFT + (json.macId ? 'mac_' + json.macId + '_table.png' : 'aircraft_0_tableimg.png'),
            acImgMapUrl: URL_COMMON_IMAGE_AIRCRAFT_ICON + (json.acTypeId ? 'ac-type-' + json.acTypeId + '-yellow.png': 'ac-type-2-yellow.png'),
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsFlight.fromJSON(value) : value;
    }

    public static getStaticMapUrl( dep: AmsAirport, arr: AmsAirport, sizeF:number = 1): string {
        const height = (180*sizeF);//180;
        const width = (294*sizeF);//294;
        const center:AmsLatLon = AmsFlight.findCenter([
            { lat: dep.lat, lng: dep.lon },
            { lat: arr.lat, lng: arr.lon },
          ]);
          //console.log('getStaticMapUrl -> center:',center);
        //const apType: ApTypeInfo = ApTypeInfoOpt.find(x => x.id === json.typeId);
        let rv = 'https://maps.googleapis.com/maps/api/staticmap?center=';
        rv += center.lat + ',' + center.lng;
        //rv += '&zoom=' + AmsAirport.getStaticmapZoom(flight.distanceKm).toFixed(0);
        rv += `&markers=size:mid%7Ccolor:blue%7Clabel:D%7C${dep.lat},${dep.lon}`;
        rv += `&markers=size:mid%7Ccolor:0xFFFF00%7Clabel:A%7C${arr.lat},${arr.lon}`;
        rv += `&path=weight:3%7Ccolor:orange%7C${dep.lat},${dep.lon}%7C${center.lat},${center.lng}%7C${arr.lat},${arr.lon}`;
        rv += '&size=' + width + 'x' + height + '&maptype=terrain';
        rv += '&key=' + GMAP_API_KEY;
        return rv;
    }

    public static getMiddle(prop, markers) {
        let values = markers.map(m => m[prop]);
        let min = Math.min(...values);
        let max = Math.max(...values);
        if (prop === 'lng' && (max - min > 180)) {
          values = values.map(val => val < max - 180 ? val + 360 : val);
          min = Math.min(...values);
          max = Math.max(...values);
        }
        let result = (min + max) / 2;
        if (prop === 'lng' && result > 180) {
          result -= 360
        }
        return result;
      }
      
      public static findCenter(markers):AmsLatLon {
        return {
          lat: AmsFlight.getMiddle('lat', markers),
          lng: AmsFlight.getMiddle('lng', markers)
        }
      }

}

export class AmsLatLon{
    lat:number;
    lng:number;
}

export interface IAmsFlight {
    flId: number;
    flTypeId: number;
    flName: string;
    flDescription: string;
    fleId: number;
    flpId: number;
    routeId: number;
    chId: number;

    alId: number;
    acId: number;
    acRegNr: string;
    depApId: number;
    arrApId: number;
    distanceKm: number;
    fuelL: number;
    pax: number;
    payloadKg: number;

    etdTime: string;
    etaTime: string;
    dtime: string;
    atime: string;
    delayMin: number;
    expences: number;
    income: number;
    revenue: number;

    flLogId: number;
    acfsId: number;
    flLogDescription: string;
    flTimeMin: number;
    flDistanceKm: number;
    flLogOpTimeSec: number;
    flLogDelayMin: number;
    adate: string;
    udate: string;

    macId: number;
    acTypeId:number;
    speedKmPh: number;
    remainDistanceKm: number;
    remainTimeMin: number;
    alName: string;
    alIata: string;
    depApCode: string;
    arrApCode: string;
    chCompletedPct:number;
    chPaxLeft:number;
    chCargoLeft:number;
}

export class ResponseFlightGetData {
    flight: IAmsFlight;
    payloads?: IAmsFlightPayload[];
}

export class ResponseFlightGet extends ResponseModel {
    data: ResponseFlightGetData;
}


export class AmsFlightTableCriteria extends TableCriteriaBase {
    alId?: number;
    acId?: number;
    macId?: number;
    dApId?: number;
    aApId?: number;
}

export class ResponceAmsFlightTableData {
    public flights: IAmsFlight[];
    public payloads: IAmsFlightPayload[];
    public rowsCount: number;
    public flightLogs: IAmsFlightLog[];
}


export class AmsFlightTableData {
    flights: AmsFlight[];
    rowsCount: number;
    payloads: AmsFlightPayload[];
    flightLogs: AmsFlightLog[];
}

export class ResponceAmsFlightTable extends ResponseModel {
    data: ResponceAmsFlightTableData;
}


export class AmsFlightDeleteData {
    flId: number;
}

export class ResponceAmsFlightDelete extends ResponseModel {
    data: AmsFlightDeleteData;
}

//#endregion

//#region Flight Estimate
export class AcLastArrTime {

    public acId: number;
    public acStatusId: AircraftStatus;
    public flqCount: number;
    public flCount: number;
    public arrTime: Date;
    public apId: number;
    public opTimeMin: number;

    get minEtdTime(): Date {
        let rv: Date = new Date();
        //console.log('minEtdTime -> arrTime:' , this.arrTime);
        const arrTimeMs = this.arrTime && this.arrTime.getTime() ? this.arrTime.getTime() : 0;
        const nowMs = rv.getTime();
        //console.log('minEtdTime -> arrTime.getTime:' , this.arrTime.getTime());
        //console.log('minEtdTime -> arrTimeMs:' , arrTimeMs);
        if ((arrTimeMs - nowMs) > 0) {
            rv.setTime(arrTimeMs + (this.opTimeMin * 60 * 1000));
        } else {
            rv.setTime(nowMs + (this.opTimeMin * 60 * 1000));
        }
        //console.log('minEtdTime -> rv:' , rv);
        return rv;
    };



    public static fromJSON(json: IAcLastArrTime): AcLastArrTime {
        const vs = Object.create(AcLastArrTime.prototype);
        return Object.assign(vs, json, {
            acStatusId: json.acStatusId ? json.acStatusId as AircraftStatus : undefined,
            arrTime: (json && json.arrTime) ? new Date(json.arrTime) : new Date(),
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsFlightEstimatePayload.fromJSON(value) : value;
    }

}

export interface IAcLastArrTime {
    acId: number;
    acStatusId: number;
    flqCount: number;
    flCount: number;
    arrTime: string;
    apId: number;
    opTimeMin: number;
}


export class ResponseLastArrTimeData {
    lastArrTime: IAcLastArrTime
}

export class ResponseLastArrTimeGet extends ResponseModel {
    data: ResponseLastArrTimeData;
}

export class AmsFlightEstimatePayload {

    public flepId: number;
    public fleId: number;
    public chId: number;
    public payloadType: AmsPayloadType;
    public pax: number;
    public cargoKg: number;
    public payloadKg: number;
    public description: string;
    public price: number;
    public adate: Date;



    public static fromJSON(json: IAmsFlightEstimatePayload): AmsFlightEstimatePayload {
        const vs = Object.create(AmsFlightEstimatePayload.prototype);
        return Object.assign(vs, json, {
            flepId: json.flepId ? json.flepId : undefined,
            fleId: json.fleId ? json.fleId : undefined,
            chId: json.chId ? json.chId : undefined,
            payloadType: json.payloadType ? AmsPayloadType[json.payloadType] : undefined,
            pax: json.pax ? json.pax : 0,
            cargoKg: json.cargoKg ? json.cargoKg : 0,
            payloadKg: json.payloadKg ? json.payloadKg : 0,
            price: json.price ? json.price : 0,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsFlightEstimatePayload.fromJSON(value) : value;
    }

}

export interface IAmsFlightEstimatePayload {
    flepId: number;
    fleId: number;
    chId: number;
    payloadType: string;
    pax: number;
    cargoKg: number;
    payloadKg: number;
    description: string;
    price: number;
    adate: string;
}

export class AmsFlightEstimate {

    public acId: number;
    public alId: number;
    public arrApId: number;
    public depApId: number;
    public bankIdFrom: number;
    public boardingFee: number;
    public chId: number;
    public distanceKm: number;

    public dtime: Date;
    public atime: Date;

    public durationMin: number;
    public expences: number;
    public flDescription: string;
    public flName: string;
    public flNum: number;
    public flTypeId: AmsFlightType;
    public flPrice: number;
    public fleId: number;
    public flpId?: number;

    public groundCrewFee: number;
    public income: number;
    public landingFee: number;
    public mtow: number;
    public oilL: number;
    public oilPrice: number;
    public pax: number;
    public payloadKg: number;
    public revenue: number;
    public routeId?: number;
    public adate: Date;

    public chCompletedPct:number;
    public chPaxLeft:number;
    public chCargoLeft:number;

    public payloads: AmsFlightEstimatePayload[];


    public static fromJSON(json: IAmsFlightEstimate): AmsFlightEstimate {
        const vs = Object.create(AmsFlightEstimate.prototype);
        return Object.assign(vs, json, {
            acId: json.acId ? json.acId : undefined,
            alId: json.alId ? json.alId : undefined,
            arrApId: json.arrApId ? json.arrApId : undefined,
            depApId: json.depApId ? json.depApId : undefined,
            payloadKg: json.payloadKg ? json.payloadKg : 0,
            income: json.income ? json.income : 0,
            revenue: (json.income ? json.income : 0) + (json.expences ? json.expences : 0),
            flTypeId: json.flTypeId ? json.flTypeId as AmsFlightType : undefined,
            atime: (json && json.atime) ? new Date(json.atime) : undefined,
            dtime: (json && json.dtime) ? new Date(json.dtime) : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    public toFlight(): AmsFlight {
        let rv = Object.assign(new AmsFlight(), this,{
            amsLogoUrl: URL_COMMON_IMAGE_AIRLINE + 'logo_' + this.alId.toString() + '.png',
            amsLiveryUrl: URL_COMMON_IMAGE_AIRLINE + 'livery_' + this.alId.toString() + '.png',
            distanceKm: this.distanceKm,
        });
        return rv;
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsFlightEstimate.fromJSON(value) : value;
    }
}

export interface IAmsFlightEstimate {

    acId: number;
    alId: number;
    arrApId: number;
    depApId: number;
    bankIdFrom: number;
    boardingFee: number;
    chId: number;
    distanceKm: number;

    dtime: string;
    atime: string;

    durationMin: number;
    expences: number;
    flDescription: string;
    flName: string;
    flNum: number;
    flTypeId: number;
    flPrice: number;
    fleId: number;
    flpId?: number;

    groundCrewFee: number;
    income: number;
    landingFee: number;
    mtow: number;
    oilL: number;
    oilPrice: number;
    pax: number;
    payloadKg: number;
    revenue: number;
    routeId?: number;
    adate: string;
    
    chCompletedPct:number;
    chPaxLeft:number;
    chCargoLeft:number;

}



export class AmsFlightEstimateData {
    flight: IAmsFlightEstimate;
}

export class ResponseAmsFlightEstimate extends ResponseModel {
    data: AmsFlightEstimateData;
}

export class AmsFlightsEstimateData {
    flights: IAmsFlightEstimate[];
    payloads: IAmsFlightEstimatePayload[];
}

export class ResponseAmsFlightsEstimate extends ResponseModel {
    data: AmsFlightsEstimateData;
}

export class AmsFlightEstimateCreteData {
    flId: number;
}

export class ResponseAmsFlightEstimateCrete extends ResponseModel {
    data: AmsFlightEstimateCreteData;
}

export class AmsFlightsEstimateTableCriteria extends TableCriteriaBase {
    acId: number;
    apId: number;
    depTime: Date;

    public static fromJSON(json: IAmsFlightsEstimateTableCriteria): AmsFlightsEstimateTableCriteria {
        const vs = Object.create(AmsFlightsEstimateTableCriteria.prototype);
        return Object.assign(vs, json, {
            acId: json.acId ? json.acId : undefined,
            apId: json.apId ? json.apId : undefined,
            depTime: (json && json.depTime) ? new Date(json.depTime) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsFlightsEstimateTableCriteria.fromJSON(value) : value;
    }
}

export interface IAmsFlightsEstimateTableCriteria extends TableCriteriaBase {
    acId: number;
    apId: number;
    depTime: string;

}


//#endregion



