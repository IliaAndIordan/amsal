import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom, Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { DateModel } from 'src/app/@core/models/common/date.model';
import { ResponseModel } from 'src/app/@core/models/common/responce.model';
import { environment } from 'src/environments/environment';
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
import { AmsCfgCharter, AmsCfgCharterTableCriteria, ResponceAmsCfgCharterDelete, ResponceAmsCfgCharterTable, ResponceAmsCfgCharterTableData, ResponseCfgCharterGet } from './charter-dto';
import { AcLastArrTime, AmsFlight, AmsFlightEstimate, AmsFlightEstimatePayload, AmsFlightLog, AmsFlightPayload, AmsFlightTableCriteria, AmsFlightTableData, IAmsFlightPayload, ResponceAmsFlightTable, ResponceAmsFlightTableData, ResponseAmsFlightEstimate, ResponseAmsFlightEstimateCrete, ResponseAmsFlightsEstimate, ResponseFlightGet, ResponseLastArrTimeGet } from './dto';
import { AmsPayloadType } from './enums';

@Injectable({ providedIn: 'root' })
export class AmsFlightClient extends ApiBaseClient {

  constructor(
    private client: HttpClient,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return environment.services.url.flight;
  }

  //#region AmsFlightTableData

  async flightsTableP(value: AmsFlightTableCriteria): Promise<AmsFlightTableData> {
    const source$ = this.flightsTable(value);
    return await lastValueFrom<any>(source$);
  }

  flightsTable(criteria: AmsFlightTableCriteria): Observable<AmsFlightTableData> {
    //console.log('flightsTable-> criteria=', criteria);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'flights_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        alId: criteria.alId,

        acId: criteria.acId,
        macId: criteria.macId,
        dApId: criteria.dApId,
        aApId: criteria.aApId,
      },
      { headers: hdrs }).pipe(
        map((response: ResponceAmsFlightTable) => {
          //console.log('flightsTable -> response=', response);
          const resp = Object.assign(new ResponceAmsFlightTable(), response);
          let res = new AmsFlightTableData();
          res.flights = new Array<AmsFlight>();

          if (resp && resp.success) {
            res.rowsCount = resp.data.rowsCount;
            for (const flightJ of resp.data.flights) {
              let flight = AmsFlight.fromJSON(flightJ);
              flight.payloads = new Array<AmsFlightPayload>();
              if (resp.data.payloads && resp.data.payloads.length > 0) {
                const payloads = resp.data.payloads.filter(x => x.flId == flight.flId);
                if (payloads && payloads.length > 0) {
                  payloads.forEach(element => {
                    flight.payloads.push(AmsFlightPayload.fromJSON(element));
                  });
                }
              }
              res.flights.push(flight);
            }
          }

          //console.log('flightsTable -> res=', res);
          return res;
        }),
        catchError(this.handleError)
      );
  }

  flightGet(value: number): Observable<AmsFlight> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'flight_get',
      { flId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseFlightGet) => {
          const res = Object.assign(new ResponseFlightGet(), response);
          let rv: AmsFlight;
          if (res && res.success) {
            if (res && res.data && res.data.flight) {
              rv = AmsFlight.fromJSON(res.data.flight);
              rv.payloads = new Array<AmsFlightPayload>();
              if (res.data.payloads && res.data.payloads.length > 0) {
                const payloads = res.data.payloads.filter(x => x.flId == rv.flId);
                if (payloads && payloads.length > 0) {
                  payloads.forEach(element => {
                    rv.payloads.push(AmsFlightPayload.fromJSON(element));
                  });
                }
              }
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap flightGet get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }


  async flightsHistoryTableP(value: AmsFlightTableCriteria): Promise<AmsFlightTableData> {
    const source$ = this.flightsHistoryTable(value);
    return await lastValueFrom<any>(source$);
  }

  flightsHistoryTable(criteria: AmsFlightTableCriteria): Observable<AmsFlightTableData> {

    //console.log('flightsHistoryTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'flights_h_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        alId: criteria.alId,

        acId: criteria.acId,
        macId: criteria.macId,
        dApId: criteria.dApId,
        aApId: criteria.aApId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsFlightTable) => {
          //console.log('flightsHistoryTable -> resp=', resp);
          let response = Object.assign(new ResponceAmsFlightTable(), resp);
          //console.log('flightsHistoryTable -> resp=', response);
          let res = new AmsFlightTableData();
          res.flights = new Array<AmsFlight>();

          if (response && response.success) {
            res.rowsCount = response.data.rowsCount;
            for (let flightJ of response.data.flights) {
              let flight = AmsFlight.fromJSON(flightJ);
              flight.payloads = new Array<AmsFlightPayload>();
              if (response.data.payloads && response.data.payloads.length > 0) {
                const payloads = response.data.payloads.filter(x => x.flId == flight.flId);
                if (payloads && payloads.length > 0) {
                  payloads.forEach(element => {
                    flight.payloads.push(AmsFlightPayload.fromJSON(element));
                  });
                }
              }
              flight.flightLogs = new Array<AmsFlightLog>();
              if (response.data.flightLogs && response.data.flightLogs.length > 0) {
                const flightLogs = response.data.flightLogs.filter(x => x.flId == flight.flId);
                if (flightLogs && flightLogs.length > 0) {
                  flightLogs.forEach(element => {
                    flight.flightLogs.push(AmsFlightLog.fromJSON(element));
                  });
                }
              }
              res.flights.push(flight);
            }
          }

          console.log('flightsTable -> res=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap flightsTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  flightsQueueTable(criteria: AmsFlightTableCriteria): Observable<AmsFlightTableData> {
    //console.log('flightsQueueTable-> criteria=', criteria);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'flights_queue_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        alId: criteria.alId,

        acId: criteria.acId,
        macId: criteria.macId,
        dApId: criteria.dApId,
        aApId: criteria.aApId,
      },
      { headers: hdrs }).pipe(
        map((response: ResponceAmsFlightTable) => {
          //console.log('flightsQueueTable -> response=', response);
          const resp = Object.assign(new ResponceAmsFlightTable(), response);
          let res = new AmsFlightTableData();
          res.flights = new Array<AmsFlight>();

          if (resp && resp.success) {
            res.rowsCount = resp.data.rowsCount;
            for (const flightJ of resp.data.flights) {
              let flight = AmsFlight.fromJSON(flightJ);
              flight.payloads = new Array<AmsFlightPayload>();
              if (resp.data.payloads && resp.data.payloads.length > 0) {
                const payloads = resp.data.payloads.filter(x => x.flId == flight.flId);
                if (payloads && payloads.length > 0) {
                  payloads.forEach(element => {
                    flight.payloads.push(AmsFlightPayload.fromJSON(element));
                  });
                }
              }
              res.flights.push(flight);
            }
          }

          //console.log('flightsQueueTable -> res=', res);
          return res;
        }),
        catchError(this.handleError)
      );
  }

  flightQueueGet(value: number): Observable<AmsFlight> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'flight_queue_get',
      { flId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseFlightGet) => {
          const res = Object.assign(new ResponseFlightGet(), response);
          let rv: AmsFlight;
          if (res && res.success) {
            if (res && res.data && res.data.flight) {
              rv = AmsFlight.fromJSON(res.data.flight);
              rv.payloads = new Array<AmsFlightPayload>();
              if (res.data.payloads && res.data.payloads.length > 0) {
                const payloads = res.data.payloads.filter(x => x.flId == rv.flId);
                if (payloads && payloads.length > 0) {
                  payloads.forEach(element => {
                    rv.payloads.push(AmsFlightPayload.fromJSON(element));
                  });
                }
              }
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap flightGet get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  
  getLastArrTime(value: number): Observable<AcLastArrTime> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'last_arr_time',
      { acId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseLastArrTimeGet) => {
          const res = Object.assign(new ResponseLastArrTimeGet(), response);
          //console.log('getLastArrTime -> res:', res);
          let rv: AcLastArrTime;
          if (res && res.success) {
            if (res && res.data && res.data.lastArrTime) {
              rv = AcLastArrTime.fromJSON(res.data.lastArrTime);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap flightGet get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

  //#region Flights Estimate

  flightTransferEstimate(acId: number, apId: number, depTime:Date = new Date()): Observable<ResponseAmsFlightEstimate> {

    //console.log('flightTransferEstimate-> acId=', acId);
    //console.log('flightTransferEstimate-> apId=', apId);
   
    if(!depTime){depTime = new Date();}
    //console.log('flightTransferEstimate-> depTime=', depTime);
    const depTimeStr = depTime.toISOString().slice(0, 19).replace('T', ' ');
    //console.log('flightTransferEstimate-> depTimeStr=', depTimeStr);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ac_flight_estimate_transfer',
      {
        acId: acId,
        apId: apId,
        depTime: depTimeStr,
      },
      { headers: hdrs }).pipe(
        map((resp: any) => {
          console.log('flightTransferEstimate -> resp=', resp);
          const res = Object.assign(new ResponseAmsFlightEstimate(), resp);
          return res;
        }),
        tap(event => {
          this.log(`tap flightTransferEstimate event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  fflightTransferEstimateCreate(fleId: number): Observable<ResponseAmsFlightEstimateCrete> {

    console.log('fflightTransferEstimateCreate-> fleId=', fleId);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ac_flight_estimate_transfer_create',
      {
        fleId: fleId,
      },
      { headers: hdrs }).pipe(
        map((resp: any) => {
          console.log('fflightTransferEstimateCreate -> resp=', resp);
          const res: ResponseAmsFlightEstimateCrete = Object.assign(new ResponseAmsFlightEstimateCrete(), resp);
          console.log('fflightTransferEstimateCreate -> res=', res);
          return res;
        }),
        catchError(this.handleError)
      );
  } 

  flightCharterEstimate(acId: number, apId: number, depTime:Date = undefined): Observable<AmsFlightEstimate[]> {
    
    if(!depTime){depTime = DateModel.nowPlusFiveMin;}
    //console.log('flightCharterEstimate-> depTime=', depTime);
    //console.log('flightCharterEstimate-> acId=', acId);
    const depTimeStr = DateModel.getTimeStr(depTime);
    //console.log('flightCharterEstimate-> depTimeStr=', depTimeStr);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ac_flight_estimate_charter',
      {
        acId: acId,
        apId: apId,
        depTime: depTimeStr,
      },
      { headers: hdrs }).pipe(
        map((resp: any) => {
          //console.log('flightCharterEstimate -> resp=', resp);
          const res: ResponseAmsFlightsEstimate = Object.assign(new ResponseAmsFlightsEstimate(), resp);
          let rv = new Array<AmsFlightEstimate>();
          if (res && res.success && res.data.flights) {
            res.data.flights.forEach(flightJ => {
              const flight = AmsFlightEstimate.fromJSON(flightJ);

              flight.payloads = new Array<AmsFlightEstimatePayload>();
              if (res.data.payloads && res.data.payloads.length > 0) {
                const payloads = res.data.payloads.filter(x => x.fleId == flight.fleId);
                if (payloads && payloads.length > 0) {
                  payloads.forEach(element => {
                    flight.payloads.push(AmsFlightEstimatePayload.fromJSON(element));
                  });
                }
                //console.log('flightCharterEstimate -> flight=', flight);
                rv.push(flight);
              }
            });
          }
          //console.log('flightCharterEstimate -> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap flightTransferEstimate event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  flightCharterEstimateCreate(fleId: number): Observable<ResponseAmsFlightEstimateCrete> {

    //console.log('flightCharterEstimateCreate-> fleId=', fleId);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ac_flight_estimate_charter_create',
      {
        fleId: fleId,
      },
      { headers: hdrs }).pipe(
        map((resp: any) => {
          //console.log('flightCharterEstimateCreate -> resp=', resp);
          const res: ResponseAmsFlightEstimateCrete = Object.assign(new ResponseAmsFlightEstimateCrete(), resp);
          //console.log('flightCharterEstimate -> res=', res);
          return res;
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

  //#region CFG Charter

  cfgCharterTable(criteria: AmsCfgCharterTableCriteria): Observable<ResponceAmsCfgCharterTable> {

    //console.log('cfgCharterTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'cfg_charter_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        depApId: criteria.depApId,
        arrApId: criteria.arrApId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsCfgCharterTable) => {
          const res = Object.assign(new ResponceAmsCfgCharterTable(), resp);
          //console.log('cfgCharterTable -> response=', res);
          return res;
        }),
        tap(event => {
          //this.log(`tap cfgCharterTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  amsCharterTable(criteria: AmsCfgCharterTableCriteria): Observable<ResponceAmsCfgCharterTable> {

    //console.log('amsCharterTable-> criteria=', criteria);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ams_charter_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        depApId: criteria.depApId,
        arrApId: criteria.arrApId,
        completed: criteria.completed ? 1 : 0,
        payloadType: criteria.payloadType ? AmsPayloadType[criteria.payloadType] : undefined
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsCfgCharterTable) => {
          const res = Object.assign(new ResponceAmsCfgCharterTable(), resp);
          //console.log('amsCharterTable -> response=', res);
          return res;
        }),
        tap(event => {
          //this.log(`tap amsCharterTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  getCfgChart(value: number): Observable<AmsCfgCharter> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'cfg_charter_get',
      { charterId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseCfgCharterGet) => {
          const res = Object.assign(new ResponseCfgCharterGet(), response);
          let rv: AmsCfgCharter;
          if (res && res.success) {
            if (res && res.data && res.data.charter) {
              rv = AmsCfgCharter.fromJSON(res.data.charter);
            }
          }
          return rv;
        }),
        catchError(this.handleError)
      );



  }


  cfgCharterSave(value: AmsCfgCharter): Observable<AmsCfgCharter> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'cfg_charter_save',
      { charter: value },
      { headers: hdrs }).pipe(
        map((response: ResponseCfgCharterGet) => {
          const res = Object.assign(new ResponseCfgCharterGet(), response);
          let rv: AmsCfgCharter;
          if (res && res.success) {
            if (res && res.data && res.data.charter) {
              rv = AmsCfgCharter.fromJSON(res.data.charter);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap airport save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  cfgCharterDelete(value: number): Observable<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'cfg_charter_delete',
      { acmpId: value },
      { headers: hdrs }).pipe(
        map((response: ResponceAmsCfgCharterDelete) => {
          const res = Object.assign(new ResponceAmsCfgCharterDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.charterId) {
              rv = res.data.charterId;
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap airport delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  cfgCharterPrecallculate(): Observable<ResponseModel> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'cfg_charter_precallculate',
      { headers: hdrs }).pipe(
        map((response: ResponceAmsCfgCharterDelete) => {
          const res = Object.assign(new ResponceAmsCfgCharterDelete(), response);
          return res;
        }),
        tap(event => {
          this.log(`tap airport delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

}
