import { TestBed } from '@angular/core/testing';
import { AmsFlightClient } from './ams-flight.client';

describe('Ams -> Services -> AmsFlightClient', () => {
  let service: AmsFlightClient;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AmsFlightClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
