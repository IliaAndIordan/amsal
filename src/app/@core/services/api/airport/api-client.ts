import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { environment } from 'src/environments/environment';
import { AmsAirport, AmsAirportTableCriteria, AmsCargoPayloadDemand, AmsCargoPayloadDemandTableCriteria, AmsPaxPayloadDemandTableCriteria, AmsPayloadDemandByDay, AmsPayloadDemandByDayTableCriteria, AmsPayloadDemandByDayTableData, AmsPcpdByArrAirport, AmsPcpdByArrAirportTableCriteria, AmsPcpdByArrAirportTableData, AmsRunway, AmsRunwaySaveData, AmsRunwayTableCriteria, IAmsRunway, ResponseAirportDelete, ResponseAirportSave, ResponseAmsAirportTableData, ResponseAmsCargoPayloadDemandTableData, ResponseAmsPaxPayloadDemandTableData, ResponseAmsPayloadDemandByDayTableData, ResponseAmsPcpdByArrAirportTableData, ResponseAmsRunwayDelete, ResponseAmsRunwaySave, ResponseAmsRunwaysData, ResponseAmsRunwayTableData } from './dto';


// Models

@Injectable({ providedIn: 'root' })
export class AmsAirportClient extends ApiBaseClient {

  constructor(
    private client: HttpClient,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return environment.services.url.airport;
  }


  //#region Airport

  airportTableSolr(criteria: AmsAirportTableCriteria): Observable<ResponseAmsAirportTableData> {

    //console.log('airportTableSolr-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airport_table_solr',
      {
        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        regionId: criteria.regionId,
        subregionId: criteria.subregionId,
        countryId: criteria.countryId,
        stateId: criteria.stateId,
        cityId: criteria.cityId,
        active: 1
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsAirportTableData) => {
          const res = Object.assign(new ResponseAmsAirportTableData(), resp);
          //console.log('airportTableSolr -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap countryTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  airportTable(criteria: AmsAirportTableCriteria): Observable<ResponseAmsAirportTableData> {

    //console.log('airportTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airport_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        regionId: criteria.regionId,
        subregionId: criteria.subregionId,
        countryId: criteria.countryId,
        stateId: criteria.stateId,
        cityId: criteria.cityId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsAirportTableData) => {
          const res = Object.assign(new ResponseAmsAirportTableData(), resp);
          //console.log('airportTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap countryTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  getAirport(value: number): Observable<AmsAirport> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airport_get',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAirportSave) => {
          const res = Object.assign(new ResponseAirportSave(), response);
          let rv: AmsAirport;
          if (res && res.success) {
            if (res && res.data && res.data.airport) {
              rv = AmsAirport.fromJSON(res.data.airport);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap Airport get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }


  airportSave(value: AmsAirport): Observable<AmsAirport> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airport_save',
      { airport: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAirportSave) => {
          const res = Object.assign(new ResponseAirportSave(), response);
          let rv: AmsAirport;
          if (res && res.success) {
            if (res && res.data && res.data.airport) {
              rv = AmsAirport.fromJSON(res.data.airport);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap airport save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  airportDelete(value: number): Observable<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airport_delete',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAirportDelete) => {
          const res = Object.assign(new ResponseAirportDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.apId) {
              rv = res.data.apId;
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap airport delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

  //#region  SOLR Airports

  geSolrAirport(value: number): Observable<AmsAirport> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ap_get',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAirportSave) => {
          const res = Object.assign(new ResponseAirportSave(), response);
          let rv: AmsAirport;
          if (res && res.success) {
            if (res && res.data && res.data.airport) {
              rv = AmsAirport.fromJSON(res.data.airport);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap Airport get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }
  //#endregion

  //#region Runway

  runwayTable(criteria: AmsRunwayTableCriteria): Observable<ResponseAmsRunwayTableData> {

    //console.log('runwayTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'rw_table',
      {
        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        apId: criteria.apId,

      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsRunwayTableData) => {
          const res = Object.assign(new ResponseAmsRunwayTableData(), resp);
          //console.log('runwayTable -> response=', res);
          return res;
        }),
        tap(event => {
          //this.log(`tap runwayTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  getRunway(value: number): Observable<AmsRunway> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'rw_get',
      { rwId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsRunwaySave) => {
          const res = Object.assign(new ResponseAmsRunwaySave(), response);
          let rv: AmsRunway;
          if (res && res.success) {
            if (res && res.data && res.data.runway) {
              rv = AmsRunway.fromJSON(res.data.runway);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap getRinway get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  getAirportRinways(value: number): Observable<AmsRunway[]> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ap_runways',
      { rwId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsRunwaysData) => {
          const res = Object.assign(new ResponseAmsRunwaysData(), response);
          let rv = new Array<AmsRunway>();
          if (res && res.success) {
            if (res && res.data && res.data.runways) {
              res.data.runways.forEach((el: IAmsRunway) => {
                const rw = AmsRunway.fromJSON(el);
                rv.push(rw);
              });

            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap getAirportRinways get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  runwaySave(value: AmsRunway): Observable<AmsRunway> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'rw_save',
      { runway: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsRunwaySave) => {
          const res = Object.assign(new ResponseAmsRunwaySave(), response);
          let rv: AmsRunway;
          if (res && res.success) {
            if (res && res.data && res.data.runway) {
              rv = AmsRunway.fromJSON(res.data.runway);
            }
          }
          console.log('runwaySave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap runwaySave event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  runwayDelete(value: number): Observable<number> {
    console.log('runwayDelete-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'state_delete',
      { rwId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsRunwayDelete) => {
          const res = Object.assign(new ResponseAmsRunwayDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.rwId) {
              rv = res.data.rwId;
            }
          }
          console.log('runwayDelete-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap runwayDelete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

  //#region PPD CPD

  airportPpdTable(criteria: AmsPaxPayloadDemandTableCriteria): Observable<ResponseAmsPaxPayloadDemandTableData> {

    //console.log('airportPpdTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ap_ppd_table',
      {
        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        depApId: criteria.depApId,
        destId: criteria.destId,
        arrApId: criteria.arrApId,

      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsPaxPayloadDemandTableData) => {
          const res = Object.assign(new ResponseAmsPaxPayloadDemandTableData(), resp);
          //console.log('airportPpdTable -> response=', res);
          return res;
        }),
        catchError(this.handleError)
      );
  }

  airportCpdTable(criteria: AmsCargoPayloadDemandTableCriteria): Observable<ResponseAmsCargoPayloadDemandTableData> {

    //console.log('airportCpdTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ap_cpd_table',
      {
        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        depApId: criteria.depApId,
        destId: criteria.destId,
        arrApId: criteria.arrApId,

      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsCargoPayloadDemandTableData) => {
          const res = Object.assign(new ResponseAmsCargoPayloadDemandTableData(), resp);
          //console.log('airportCpdTable -> response=', res);
          return res;
        }),
        catchError(this.handleError)
      );
  }

  payloadDemandSumByDayTable(criteria: AmsPayloadDemandByDayTableCriteria): Observable<AmsPayloadDemandByDayTableData> {

    //console.log('airportCpdTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ap_payload_demand_sum_by_day',
      {
        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        depApId: criteria.depApId,
        destId: criteria.destId,
        arrApId: criteria.arrApId,

      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsPayloadDemandByDayTableData) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponseAmsPayloadDemandByDayTableData(), resp);
          let data = new AmsPayloadDemandByDayTableData();
          data.payloads = new Array<AmsPayloadDemandByDay>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            data.destId = res.data.destId;
            data.depApId = res.data.depApId;
            data.arrApId = res.data.arrApId;

            if (res.data.payloads && res.data.payloads.length > 0) {
              res.data.payloads.forEach(element => {
                data.payloads.push(AmsPayloadDemandByDay.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  airportPayloadDemand14Days(apId: number): Promise<AmsPayloadDemandByDay[]> {

    let promise = new Promise<AmsPayloadDemandByDay[]>((resolve, reject) => {
      let req = new AmsPayloadDemandByDayTableCriteria();
      req.depApId = apId;
      req.limit = 14;
      req.offset = 0;
      req.pageIndex = 0
      this.payloadDemandSumByDayTable(req).subscribe((res: AmsPayloadDemandByDayTableData) => {
        resolve(res.payloads);
      });
    });

    return promise;
  }

  pcpdByArrAirportTable(criteria: AmsPcpdByArrAirportTableCriteria): Observable<AmsPcpdByArrAirportTableData> {

    //console.log('airportCpdTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ap_payload_demand_sum_by_arr_ap',
      {
        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        depApId: criteria.depApId,
        arrApId: criteria.arrApId,

      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsPcpdByArrAirportTableData) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponseAmsPcpdByArrAirportTableData(), resp);
          let data = new AmsPcpdByArrAirportTableData();
          data.payloads = new Array<AmsPcpdByArrAirport>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            data.depApId = res.data.depApId;
            data.arrApId = res.data.arrApId;

            if (res.data.payloads && res.data.payloads.length > 0) {
              res.data.payloads.forEach(element => {
                data.payloads.push(AmsPcpdByArrAirport.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

}
