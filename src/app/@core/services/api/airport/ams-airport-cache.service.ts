import { Injectable } from "@angular/core";
import { Observable, lastValueFrom, of } from "rxjs";
import { catchError, map, shareReplay, take } from "rxjs/operators";
import { CurrentUserService } from "../../auth/current-user.service";
import { AmsAirportClient } from "./api-client";
import { AmsAirport, AmsAirportTableCriteria, IAmsAirport, ResponseAmsAirportTableData } from "./dto";
import { AmsAirlineHubsCacheService } from "../airline/al-hub-cache.service";
import { AmsFlight } from "../flight/dto";

@Injectable({ providedIn: 'root' })
export class AmsAirportCacheService {

  apCache$: Observable<ResponseAmsAirportTableData>;

  constructor(
    private cus: CurrentUserService,
    private apClient: AmsAirportClient,
    private hubCache: AmsAirlineHubsCacheService,) {
  }

  //#region  AmsAirport

  clearCache():void{
    this.apCache$ = undefined;
  }

  get airportTable(): Observable<ResponseAmsAirportTableData> {
    if (!this.apCache$) {

      this.apCache$ = this.airportTableSolr().pipe(shareReplay());
    }
    return this.apCache$;
  }

  get airports(): Observable<AmsAirport[]> {
    return this.airportTable.pipe(
      map(apTable => {
        let rv = new Array<AmsAirport>();
        if (apTable && apTable.data && apTable.data.airports) {
          for (const ap of apTable.data.airports) {
            const airport = AmsAirport.fromJSON(ap);
            rv.push(airport);
          }
        }
        //console.log('amsAirportsList -> rv:', rv);
        return rv;

      })
    );
  }

  getAirport(id: number): Observable<AmsAirport> {
    return this.airports.pipe(map(aps => aps.find(aps => aps.apId == id)));
  }

  async getAirportPromise(id: number): Promise<AmsAirport> {

    const source$ = this.airports
      .pipe(
        take(1),
        map((aps: AmsAirport[]) => {
          //console.log('calcCityPayloadDemend -> response:', response);
          const res = aps.find(aps => aps.apId == id);
          return res;
        }),
        catchError(this.apClient.handleError)
      );
    return await lastValueFrom<AmsAirport>(source$);

  }

  async getAirportAsync(id: number): Promise<AmsAirport> {
    let ap: AmsAirport = await this.getAirportPromise(id);
    if (ap) {
      ap.hub = await this.hubCache.getHubPromise(id);
    }
    return ap;
  }

  airportTableSolr(): Observable<ResponseAmsAirportTableData> {
    const criteria = new AmsAirportTableCriteria()
    criteria.limit = 20000;
    criteria.offset = 0;

    return this.apClient.airportTableSolr(criteria)
  }

  loadAirports(): Promise<AmsAirport[]> {
    let promise = new Promise<AmsAirport[]>((resolve, reject) => {
      const req$ = this.airports;
      req$.subscribe((data: AmsAirport[]) => {
        resolve(data);
      });
    });

    return promise;
  }

  //#endregion

  //#region 

  public async getStaticMapUrl(depApId: number, arrApId: number, sizeF:number = 1): Promise<string> {
    const dep: AmsAirport = await this.getAirportPromise(depApId);
    const arr: AmsAirport = await this.getAirportPromise(arrApId);
    return AmsFlight.getStaticMapUrl(dep, arr, sizeF);
  }

}