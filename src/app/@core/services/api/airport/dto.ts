import { GMAP_API_KEY, URL_COMMON_IMAGE_FLAGS, URL_NO_IMG } from "src/app/@core/const/app-storage.const";
import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { AmsStatus, WadStatus } from "src/app/@core/models/pipes/ams-status.enums";
import { ApType, ApTypeInfo, ApTypeInfoOpt, RwSerface, RwType } from "src/app/@core/models/pipes/ap-type.pipe";
import { fill_color_gray, fill_color_yellow } from "src/app/@share/components/common/icons/airport-icon-svg.component";
import { RowsCountModel } from "../country/dto";
import { AmsPayloadType } from "../flight/enums";
import { chartColors } from "src/app/@share/charts/ams-chart-line/ams-chart-line.component";
import { AmsHub } from "../airline/al-hub";
import { AmsAlSheduleRoute } from "../airline/al-flp-shedule";
import { NumberLiteralType } from "typescript";

//#region Airport

export class AmsAirport {
    apId: number;
    iata: string;
    icao: string;
    apName: string;
    lat: number;
    lon: number;
    elevation: number;
    typeId: ApType;
    regionId: number;
    subregionId: number;
    countryId: number;
    iso2: string;
    cName: string;
    stateId: number;
    stName: string;
    cityId: number;
    ctName: string;
    active: boolean;
    wikiUrl: string;
    amsStatus: AmsStatus;
    wadStatus: WadStatus;
    homeUrl: string;
    notes: string;
    utc: number;
    baseTypeId: ApType;
    svgline: string;
    svgcircle: string;
    adate: Date;
    udate: Date;
    runwaysCount: number;
    oldApId: number;
    maxMtow: number;
    maxRwLenght: number;

    paxDemandDay:number;
    paxDemandPeriod:number;
    cargoDemandPeriodKg:number;
    paxDemandPeriodDays:number;
    plddate: Date;

    flagUrl: string;
    staticmapUrl:string;
    apLabel: string;
    lat_lon:string;
    iso_country_state:string;
    flpns_count:number;

    hub?:AmsHub;
    routes:AmsAlSheduleRoute[];

    get svgIconStr():string {
        let rv:string;
        const fillColor = this.active ? fill_color_yellow : fill_color_gray;
        const ap_circle = this.svgcircle + ' z';
        const rw_lines = this.svgline + ' z';
        const rw_line_color = this.active ? 'blue' : 'red';
        const ap_circle_color = this.active ? 'black' : 'orange';
        const cssStyle = this.getStyle();

        rv = '<svg version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" ';
        rv += ' viewBox="-16 -16 32 32" class="ap-svg" > ';
        rv += ' <g fill="none"> ';
        rv += '   <path stroke-width="1" stroke="' + ap_circle_color + '" fill="' + fillColor + '" style="opacity:0.7;" d="' + ap_circle + '"/> ';
        rv += '   <path stroke-width="2"  stroke="' + rw_line_color + '" d="' + rw_lines + '" /> ';
        rv += '</g> </svg>';
        return rv;
    }

    getStyle(size:number = undefined): string {
        let rv = 'ap-svg';
        if (size) {
            rv = 'ap-svg-' + size.toString();
        }
        return rv;
    }


    public static getStaticmapZoom(maxRwLenght:number):number{
        let rv = 16;
        if(maxRwLenght){
            if(maxRwLenght<400){
                rv = 18;
            }
            else if(maxRwLenght>=400 && maxRwLenght<600){
                rv = 17;
            }
            else if(maxRwLenght>=600 && maxRwLenght<900){
                rv = 16;
            }
            else if(maxRwLenght>=900 && maxRwLenght<1200){
                rv = 15;
            }
            else if(maxRwLenght>=1200 && maxRwLenght<1800){
                rv = 15;
            }
            else if(maxRwLenght>=1800 && maxRwLenght<3000){
                rv = 14;
            }
            else if(maxRwLenght>=3000 && maxRwLenght<4200){
                rv = 13;
            }
            else if(maxRwLenght>=4200){
                rv = 12;
            }
        }
        return rv;
    }

   

    public static fromJSON(json: IAmsAirport): AmsAirport {
        const vs = Object.create(AmsAirport.prototype);
        return Object.assign(vs, json, {
            typeId: json.typeId ? json.typeId as ApType : ApType.Airfield,
            active: json.active ? true : false,
            baseTypeId: json.baseTypeId ? json.baseTypeId as ApType : ApType.Airfield,
            amsStatus: json.amsStatus ? json.amsStatus as AmsStatus : AmsStatus.DataCheckRequired,
            wadStatus: json.wadStatus ? json.wadStatus as AmsStatus : WadStatus.ToDo,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            plddate:(json && json.plddate) ? new Date(json.plddate) : undefined,
            flagUrl: json.iso2 ? URL_COMMON_IMAGE_FLAGS + json.iso2.toLowerCase() + '.png' : URL_NO_IMG,
            staticmapUrl: AmsAirport.getStaticMapUrl(json),
            
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsAirport.fromJSON(value) : value;
    }

    public static getStaticMapUrl(json: IAmsAirport): string {
        const height = 360;//180;
        const width = 566;//294;
        const apType: ApTypeInfo = ApTypeInfoOpt.find(x => x.id === json.typeId);
        let rv = 'https://maps.googleapis.com/maps/api/staticmap?center=';
        rv += json.lat + ',' + json.lon;
        rv += '&zoom=' + AmsAirport.getStaticmapZoom(json.maxRwLenght).toFixed(0);
        rv += '&size=' + width + 'x' + height + '&maptype=satellite';
        rv += '&key=' + GMAP_API_KEY;
        return rv;
    }

}

export interface IAmsAirport {
    apId: number;
    iata: string;
    icao: string;
    apName: string;
    lat: number;
    lon: number;
    elevation: number;
    typeId: number;
    regionId: number;
    subregionId: number;
    countryId: number;
    iso2: string;
    cName: string;
    stateId: number;
    stName: string;
    cityId: number;
    ctName: string;
    active: number;
    wikiUrl: string;
    amsStatus: number;
    wadStatus: number;
    homeUrl: string;
    notes: string;
    utc: number;
    baseTypeId: number;
    svgline: string;
    svgcircle: string;
    adate: Date;
    udate: Date;
    runwaysCount: number;
    oldApId: number;
    maxMtow: number;
    maxRwLenght: number;
    paxDemandDay:number;
    paxDemandPeriod:number;
    cargoDemandPeriodKg:number;
    paxDemandPeriodDays:number;
    plddate:string;
    
}

export class AmsAirportTableCriteria extends TableCriteriaBase {
    regionId: number;
    subregionId: number;
    countryId: number;
    stateId: number;
    cityId: number;
}

export class AmsAirportTableData {
    airports: IAmsAirport[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsAirportTableData extends ResponseModel {
    data: AmsAirportTableData;
}


export class AirportSaveData {
    airport: IAmsAirport;
}

export class ResponseAirportSave extends ResponseModel {
    data: AirportSaveData;
}

export class AirportDeleteData {
    apId: number;
}

export class ResponseAirportDelete extends ResponseModel {
    data: AirportDeleteData;
}

//#endregion

//#region Runway

export class AmsRunway {
    rwId: number;
    apId: number;

    rwType: RwType;
    rwDirection: string;
    serfaceId: RwSerface;
    width: number;
    lenght: number;
    mtow: number;
    rwOrder: number;
    rotation: number;
    radius: number;
    udate: Date;
    svgLine: string;
    svgPath: string;


    public static fromJSON(json: IAmsRunway): AmsRunway {
        const vs = Object.create(AmsRunway.prototype);
        return Object.assign(vs, json, {
            rwType: json.rwType ? json.rwType as RwType : RwType.Runway,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            serfaceId: json.serfaceId ? json.serfaceId as RwSerface : RwSerface.DirtOrGravel,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsRunway.fromJSON(value) : value;
    }

}

export interface IAmsRunway {
    rwId: number;
    apId: number;

    rwType: number;
    rwDirection: string;
    serfaceId: number;
    width: number;
    lenght: number;
    mtow: number;
    rwOrder: number;
    rotation: number;
    radius: number;
    udate: string;
    svgLine: string;
    svgPath: string;
}

export class AmsRunwayTableCriteria extends TableCriteriaBase {
    apId: number;
}

export class AmsRunwayTableData {
    runways: IAmsRunway[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsRunwayTableData extends ResponseModel {
    data: AmsRunwayTableData;
}


export class AmsRunwaySaveData {
    runway: IAmsRunway;
}

export class ResponseAmsRunwaySave extends ResponseModel {
    data: AmsRunwaySaveData;
}

export class AmsRunwayDeleteData {
    rwId: number;
}

export class ResponseAmsRunwayDelete extends ResponseModel {
    data: AmsRunwayDeleteData;
}

export class AmsRunwaysData {
    runways: IAmsRunway[];
}

export class ResponseAmsRunwaysData extends ResponseModel {
    data: AmsRunwaysData;
}

//#endregion

//#region PPD CPD


export class AmsPaxPayloadDemand {
    ppdId: number;
    destId: number;
    depApId:number;
    arrApId: number;
    apCodes:string;
    pax:number;
    paxClassId: AmsPayloadType;
    ppdTypeId: number;
    payloadKg: number;
    maxPrice: number;
    note: string;
    sequence: number;
    adate: Date;
    udate: Date;

    bookingSeqArr:number;
    bookingSeqDep:number;

    priceIncreasePct:number;


    public static fromJSON(json: IAmsPaxPayloadDemand): AmsPaxPayloadDemand {
        const vs = Object.create(AmsPaxPayloadDemand.prototype);
        return Object.assign(vs, json, {
            paxClassId: json.paxClassId ? json.paxClassId as AmsPayloadType : AmsPayloadType.E,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            priceIncreasePct:((((json.bookingSeqArr??0) + (json.bookingSeqDep??0))*20)/100), 
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsRunway.fromJSON(value) : value;
    }

}

export interface IAmsPaxPayloadDemand {
    ppdId: number;
    destId: number;
    depApId:number;
    arrApId: number;
    apCodes:string;
    pax:number;
    paxClassId: number;
    ppdTypeId: number;
    payloadKg: number;
    maxPrice: number;
    note: string;
    sequence: number;
    adate: Date;
    udate: Date;
    bookingSeqArr:number;
    bookingSeqDep:number;
}

export class AmsPaxPayloadDemandTableCriteria extends TableCriteriaBase {
    destId: number;
    depApId: number;
    arrApId: number;
}

export class AmsPaxPayloadDemandTableData {
    payloads: IAmsPaxPayloadDemand[];
    rowsCount: number;
}

export class ResponseAmsPaxPayloadDemandTableData extends ResponseModel {
    data: AmsPaxPayloadDemandTableData;
}

export class AmsCargoPayloadDemand {
    ppdId: number;
    destId: number;
    depApId:number;
    arrApId: number;
    apCodes:string;
    paxClassId: AmsPayloadType;
    ppdTypeId: number;
    payloadKg: number;
    maxPrice: number;
    note: string;
    mtow: number;
    sequence: number;
    adate: Date;
    udate: Date;
    bookingSeqArr:number;
    bookingSeqDep:number;

    priceIncreasePct:number;


    public static fromJSON(json: IAmsCargoPayloadDemand): AmsCargoPayloadDemand {
        const vs = Object.create(AmsCargoPayloadDemand.prototype);
        return Object.assign(vs, json, {
            paxClassId: json.paxClassId ? json.paxClassId as AmsPayloadType : AmsPayloadType.C,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            priceIncreasePct:((((json.bookingSeqArr??0) + (json.bookingSeqDep??0))*20)/100), 
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsRunway.fromJSON(value) : value;
    }

}

export interface IAmsCargoPayloadDemand {
    ppdId: number;
    destId: number;
    depApId:number;
    arrApId: number;
    apCodes:string;
    paxClassId: number;
    ppdTypeId: number;
    payloadKg: number;
    maxPrice: number;
    note: string;
    mtow: number;
    sequence: number;
    adate: Date;
    udate: Date;
    bookingSeqArr:number;
    bookingSeqDep:number;
}

export class AmsCargoPayloadDemandTableCriteria extends TableCriteriaBase {
    destId: number;
    depApId: number;
    arrApId: number;
}

export class AmsCargoPayloadDemandTableData {
    payloads: IAmsCargoPayloadDemand[];
    rowsCount: number;
}

export class ResponseAmsCargoPayloadDemandTableData extends ResponseModel {
    data: AmsCargoPayloadDemandTableData;
}


export class AmsPayloadDemandByDay {
    destId: number;
    depApId: number;
    arrApId: number;

    destinations: number;
    paxE:number;
    paxB: number;
    paxF:number;
    pax:number;
    cargoKg: number;
    adate: Date;


    public static fromJSON(json: IAmsPayloadDemandByDay): AmsPayloadDemandByDay {
        const vs = Object.create(AmsPayloadDemandByDay.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsPayloadDemandByDay.fromJSON(value) : value;
    }

    public static getLineChartData(rows: AmsPayloadDemandByDay[]): any {
        let lineChartData;
        const pax = new Array<number>();
        const cargo = new Array<number>();

        for (let idx = 13; idx >=0; idx--) {
            const row: AmsPayloadDemandByDay = rows.length>idx?rows[idx]:undefined;
            pax.push(row && row.pax ? row.pax : 0);
            cargo.push(row && row?.cargoKg ? row.cargoKg : 0);
        }

        lineChartData = [
            {
                yAxisID: 'yLeft',
                data: pax, label: 'PAX',
                backgroundColor: chartColors.green.backgroundColor,
                borderColor: chartColors.green.borderColor,
                pointBackgroundColor: chartColors.green.pointBackgroundColor,
                pointBorderColor: chartColors.green.pointBorderColor,
                pointHoverBackgroundColor: chartColors.green.pointHoverBackgroundColor,
                pointHoverBorderColor: chartColors.green.pointHoverBorderColor,
                fill: 'origin',
            },
            {
                yAxisID: 'yRight', title:'Cargo',
                data: cargo, label: 'Cargo',
                backgroundColor: chartColors.yelow.backgroundColor,
                borderColor: chartColors.yelow.borderColor,
                pointBackgroundColor: chartColors.yelow.pointBackgroundColor,
                pointBorderColor: chartColors.yelow.pointBorderColor,
                pointHoverBackgroundColor: chartColors.yelow.pointHoverBackgroundColor,
                pointHoverBorderColor: chartColors.yelow.pointHoverBorderColor,
                fill: 'origin'
            },
        ];
        
        return lineChartData;

    }

    public static getLineChartLabels(rows: AmsPayloadDemandByDay[]): any[] {
        let lineChartLabels = new Array<string>();

        let lastDate = rows && rows.length>0 ?rows[0].adate: new Date();
        for (let idx = 13; idx >= 0; idx--) {
            const row: AmsPayloadDemandByDay =  rows.length>idx?rows[idx]:undefined;
            let label = '';
            let jsDate = lastDate;
            if(row && row.adate ){
                jsDate = row.adate;
            } else{
                jsDate.setDate(lastDate.getDate() - idx)
            }
            
            if (jsDate) {
                //const jsDate: Date = GvDatePipe.getDate(dateStr);
                label = jsDate.toString().split(' ')[0];
            }
            lineChartLabels.push(label);
        }

        
        return lineChartLabels;

    }

}

export interface IAmsPayloadDemandByDay {
    destId: number;
    depApId: number;
    arrApId: number;

    destinations: number;
    paxE:number;
    paxB: number;
    paxF:number;
    pax:number;
    cargoKg: number;
    adate: Date;
}

export class AmsPayloadDemandByDayTableCriteria extends TableCriteriaBase {
    destId: number;
    depApId: number;
    arrApId: number;
}

export class AmsPayloadDemandByDayTableData {
    payloads: IAmsPayloadDemandByDay[];
    rowsCount: number;
    destId: number;
    depApId: number;
    arrApId: number;
}

export class ResponseAmsPayloadDemandByDayTableData extends ResponseModel {
    data: AmsPayloadDemandByDayTableData;
}



export class AmsPcpdByArrAirport {
    depApId: number;
    arrApId: number;
    arrApCode:string;
    distanceKm:number;
    maxFlightH:number;
    cargoKg:number;
    ppd:number;
    destId:number;
    ppdE:number;
    ppdB:number;
    ppdF:number;
    depApCode:string;
    depApNme:string;


    public static fromJSON(json: IAmsPcpdByArrAirport): AmsPcpdByArrAirport {
        const vs = Object.create(AmsPcpdByArrAirport.prototype);
        return Object.assign(vs, json, {
            arrApId: (json && json.arrApId) ? json.arrApId: undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsPcpdByArrAirport.fromJSON(value) : value;
    }

    public static getLineChartData(rows: AmsPcpdByArrAirport[]): any {
        let lineChartData;
        const pax = new Array<number>();
        const cargo = new Array<number>();

        for (let idx = 13; idx >=0; idx--) {
            const row: AmsPcpdByArrAirport = rows.length>idx?rows[idx]:undefined;
            pax.push(row && row?.ppd ? row.ppd : 0);
            cargo.push(row && row?.cargoKg ? row.cargoKg : 0);
        }

        lineChartData = [
            {
                yAxisID: 'yLeft',
                data: pax, label: 'PAX',
                backgroundColor: chartColors.green.backgroundColor,
                borderColor: chartColors.green.borderColor,
                pointBackgroundColor: chartColors.green.pointBackgroundColor,
                pointBorderColor: chartColors.green.pointBorderColor,
                pointHoverBackgroundColor: chartColors.green.pointHoverBackgroundColor,
                pointHoverBorderColor: chartColors.green.pointHoverBorderColor,
                fill: 'origin',
            },
            {
                yAxisID: 'yRight', title:'Cargo',
                data: cargo, label: 'Cargo',
                backgroundColor: chartColors.yelow.backgroundColor,
                borderColor: chartColors.yelow.borderColor,
                pointBackgroundColor: chartColors.yelow.pointBackgroundColor,
                pointBorderColor: chartColors.yelow.pointBorderColor,
                pointHoverBackgroundColor: chartColors.yelow.pointHoverBackgroundColor,
                pointHoverBorderColor: chartColors.yelow.pointHoverBorderColor,
                fill: 'origin'
            },
        ];
        
        return lineChartData;

    }

    public static getLineChartLabels(rows: AmsPayloadDemandByDay[]): any[] {
        let lineChartLabels = new Array<string>();

        let lastDate = rows && rows.length>0 ?rows[0].adate: new Date();
        for (let idx = 13; idx >= 0; idx--) {
            const row: AmsPayloadDemandByDay =  rows.length>idx?rows[idx]:undefined;
            let label = '';
            let jsDate = lastDate;
            if(row && row.adate ){
                jsDate = row.adate;
            } else{
                jsDate.setDate(lastDate.getDate() - idx)
            }
            
            if (jsDate) {
                //const jsDate: Date = GvDatePipe.getDate(dateStr);
                label = jsDate.toString().split(' ')[0];
            }
            lineChartLabels.push(label);
        }

        
        return lineChartLabels;

    }

}

export interface IAmsPcpdByArrAirport {
    depApId: number;
    arrApId: number;
    arrApCode:string;
    distanceKm:number;
    maxFlightH:number;
    cargoKg:number;
    ppd:number;
    destId:number;
    ppdE:number;
    ppdB:number;
    ppdF:number;
    depApCode:string;
    depApNme:string;
}


export class AmsPcpdByArrAirportTableCriteria extends TableCriteriaBase {
    depApId: number;
    arrApId: number;
}

export class AmsPcpdByArrAirportTableData {
    payloads: IAmsPcpdByArrAirport[];
    rowsCount: number;
    depApId: number;
    arrApId: number;
}

export class ResponseAmsPcpdByArrAirportTableData extends ResponseModel {
    data: AmsPcpdByArrAirportTableData;
}

//#endregion

