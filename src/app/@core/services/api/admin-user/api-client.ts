import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { RespCompanyUsers, UserModel } from '../../auth/api/dto';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { CompanyStatus } from 'src/app/@core/models/pipes/sx-company-type.pipe';
import { ResponseLoginsCount } from './charts-dto';
import { ResponseAmsUserTableData } from './dto';
import { ToastrService } from 'ngx-toastr';

// Models

// import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({providedIn:'root'})
export class SxAdminUserClient extends ApiBaseClient {

    constructor(
        private client: HttpClient,
        private cus: CurrentUserService) {
        super();
    }

    private get sertviceUrl(): string {
        return environment.services.url.admin;
    }

    //#region  User Charts

    loginsCountSeventDays(): Observable<ResponseLoginsCount> {
        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'logins_count_sevent_days',
            { headers: hdrs }).pipe(
                map((resp: ResponseLoginsCount) => {
                    // console.log('companyUsers-> response=', resp);
                    const res = Object.assign(new ResponseLoginsCount(), resp);
                    return res;
                }),
                tap(event => {
                    this.log(`tap loginsCountSeventDays event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    usersTable(
        limit: number = 25,
        offset: number = 0,
        orderCol: string = 'user_name',
        isDesc: boolean = false,
        userRole?: number,
        filter?: string): Observable<ResponseAmsUserTableData> {

        console.log('usersTable-> offset=', offset);
        console.log('usersTable-> limit=', limit);

        const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
        return this.client.post(this.sertviceUrl + 'users_table',
            {
                userRole: userRole,
                qry_offset: offset,
                qry_limit: limit,
                qry_orderCol: orderCol,
                qry_isDesc: isDesc,
                qry_filter: filter,
            },
            { headers: hdrs }).pipe(
                map((resp: ResponseAmsUserTableData) => {
                    const res = Object.assign(new ResponseAmsUserTableData(), resp);
                    console.log('usersTable -> response=', res);
                    return res;
                }),
                tap(event => {
                    this.log(`tap usersTable event: ` + JSON.stringify(event));
                    // There may be other events besides the response.
                    if (event instanceof HttpResponse) {
                        // cache.put(req, event); // Update the cache.
                    }
                }),
                catchError(this.handleError)
            );
    }

    //#endregion


}
