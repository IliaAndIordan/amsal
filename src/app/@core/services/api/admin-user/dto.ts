import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria.model";
import { IUserModel } from "../../auth/api/dto";

export class AmsUserTableCriteria extends TableCriteriaBase{
    userRole:number;
}
export class AmsUserTableData {
    usersList: IUserModel[];
    totals: {
        total_rows: number;
    };
}

export class ResponseAmsUserTableData extends ResponseModel {
    data: AmsUserTableData;
}
