import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TsDatePipe } from "src/app/@core/models/pipes/ts-date.pipe";

export class LoginsCountModel {
    public logins: number;
    public users: number;
    public logdate: Date;
    public dayAbbr:string;
    public refreshTokens:number;


    public static fromJSON(json: ILoginsCountModel): LoginsCountModel {
        const vs = Object.create(LoginsCountModel.prototype);
        return Object.assign(vs, json, {
            logdate: (json && json.logdate) ? new Date(json.logdate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? LoginsCountModel.fromJSON(value) : value;
    }
    /*
    public toJSON(): ILoginsCountModel {
        const vs = Object.create(LoginsCountModel.prototype);
        return Object.assign(vs, this);
    }
    */

    public static dataToList(data: ResponseLoginsCountData): LoginsCountModel[]{
        let rv = new Array<LoginsCountModel>();
        if(data && data.rows && data.rows.length > 0){
            data.rows.forEach((el:ILoginsCountModel) => {
                const obj = LoginsCountModel.fromJSON(el);
                rv.push(obj);
            });
        }
        return rv;
    }

   public static getLineChartData(rows: LoginsCountModel[]): any {
    const logindData = new Array<number>();
    const rtdData = new Array<number>();
    const usersData = new Array<number>();
    let lineChartData;

    if (rows && rows.length > 0) {
        
        for (let idx = 0; idx < rows.length; idx++) {
            const row: LoginsCountModel = rows[idx];
            logindData.push(row.logins);
            usersData.push(row.users);
            rtdData.push(row.refreshTokens);
        }
    }
    lineChartData = [
        { data: logindData, label: 'Autehtications' },
        { data: usersData, label: 'Users' },
        { data: rtdData, label: 'Tokens' },
    ];
    return lineChartData;

}

public static getLineChartLabels(rows: LoginsCountModel[]): any[] {
    const lineChartLabels = new Array<string>();

    if (rows && rows.length >= 0) {

        for (let idx = 0; idx < rows.length; idx++) {
            const row: LoginsCountModel = rows[idx];
            let label = '';
            if (row || row.logdate) {
                //const jsDate: Date = GvDatePipe.getDate(dateStr);
                //console.log('getLineChartLabels -> logdate= ', row.logdate);
                /*
                label = TsDatePipe.padLeft((row.logdate.getMonth() + 1).toString(), '0', 2) +
                    '-' + TsDatePipe.padLeft(row.logdate.getDate().toString(), '0', 2) +
                    '-' + row.logdate.getFullYear();
                    */
                label = row.dayAbbr;
            }
            lineChartLabels.push(label);
        }

    }
    return lineChartLabels;

}
}

export interface ILoginsCountModel {
    logins: number;
    users: number;
    logdate: string;
    dayAbbr:string;
    refreshTokens:number;
}

export class ResponseLoginsCountData {
    rows: ILoginsCountModel[];
    totals: {
        total_rows: number;
    };

}

export class ResponseLoginsCount extends ResponseModel {
    data: ResponseLoginsCountData;
}


