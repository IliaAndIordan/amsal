import { Injectable } from "@angular/core";
import { Observable, Subscription, lastValueFrom, of } from "rxjs";
import { catchError, map, shareReplay, take } from "rxjs/operators";
import { CurrentUserService } from "../../auth/current-user.service";
import { AmsHub, AmsHubTableCriteria, ResponceAmsHubTable, ResponceAmsHubTableData } from "./al-hub";
import { AmsAirlineClient } from "./api-client";
import { AmsAirline } from "./dto";

@Injectable({ providedIn: 'root' })
export class AmsAirlineHubsCacheService {

  hubCache$: Observable<ResponceAmsHubTableData>;
  criteria:AmsHubTableCriteria;
  airlineChanged:Subscription;

  constructor(
    private cus: CurrentUserService,
    private alClient: AmsAirlineClient) {
        this.airlineChanged = this.cus.airlineChanged.subscribe((airline:AmsAirline)=>{
            //console.log('HubsCache -> airlineChanged al:', airline);
            this.clearCache();
        })
  }

  //#region  AmsAirport

  clearCache():void{
    this.hubCache$ = undefined;
  }

  get hubsTable(): Observable<ResponceAmsHubTableData> {
    if (!this.hubCache$ || this.criteria.alId !== this.cus.airline?.alId) {

      this.hubCache$ = this.hubTable().pipe(shareReplay());
    }
    return this.hubCache$;
  }

  get hubs(): Observable<AmsHub[]> {
    return this.hubsTable.pipe(
      map(data => {
        let rv = new Array<AmsHub>();
        if ( data && data.hubs) {
            rv = data.hubs;
        }
        //console.log('hubs -> rv:', rv);
        return rv;
      })
    );
  }

  getHub(id: number): Observable<AmsHub> {
    return this.hubs.pipe(
      map(aps => aps.find(aps => aps.apId == id))
    );
  }

  async getHubPromise(id: number): Promise<AmsHub> {
    const source$ = this.hubs
      .pipe(
        take(1),
        map((aps: AmsHub[]) => {
          //console.log('calcCityPayloadDemend -> response:', response);
          const res = aps.find(aps => aps.apId == id);
          return res;
        }),
        catchError(this.alClient.handleError)
      );
    return await lastValueFrom<AmsHub>(source$);
  }

  hubTable(): Observable<ResponceAmsHubTableData> {
    this.criteria = new AmsHubTableCriteria()
    this.criteria.limit = 20000;
    this.criteria.offset = 0;
    this.criteria.alId = this.cus.airline?.alId;

    return this.alClient.airlineHubsTable(this.criteria);
  }

  loadHubs(): Promise<AmsHub[]> {
    let promise = new Promise<AmsHub[]>((resolve, reject) => {
        const req$ = this.hubs;
        req$.subscribe((data:AmsHub[]) =>{
          resolve(data);
        });
    });
  
    return promise;
  }

  //#endregion

}