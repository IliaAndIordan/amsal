import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_FLAGS, URL_NO_IMG } from "src/app/@core/const/app-storage.const";
import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { AmsStatus } from "src/app/@core/models/pipes/ams-status.enums";
import { TsDatePipe } from "src/app/@core/models/pipes/ts-date.pipe";
import { RowsCountModel } from "../country/dto";
import { AmsFlightType, AmsFlightTypeOpt } from "../flight/enums";

//#region AmsAirline

export class AmsAirline {
    alId: number;
    alName: string;
    iata: string;
    logo: string;
    livery: string;
    slogan: string;
    userId: number;
    apId: number;
    adate: Date;
    udate: Date;

    countryId: number;
    iso2: string;
    stateId: number;
    subregionId: number;
    regionId: number;
    apLocation: string;
    apLabel: string;

    amsLogoUrl: string;
    amsLiveryUrl: string;

    bankId: number;
    amount: number;
    bankUdate: Date;
    hubsCount: number;

    flCharterCount: number;
    flScheduleCount: number;
    flTransferCount: number;
    aircraftCount: number;


    public static fromJSON(json: IAmsAirline): AmsAirline {
        const vs = Object.create(AmsAirline.prototype);
        return Object.assign(vs, json, {
            apLocation: json.apLocation ? json.apLocation : undefined,
            logo: json.logo ? json.logo : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            amsLogoUrl: URL_COMMON_IMAGE_AIRLINE + 'logo_' + json.alId.toString() + '.png?' + new Date().getTime(),
            amsLiveryUrl: URL_COMMON_IMAGE_AIRLINE + 'livery_' + json.alId.toString() + '.png?' + new Date().getTime(),
            bankUdate: (json && json.bankUdate) ? new Date(json.bankUdate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsAirline.fromJSON(value) : value;
    }

    public get flTotalCount(): number {
        return ((this.flTransferCount ?? 0) +
            (this.flCharterCount ?? 0) +
            (this.flScheduleCount ?? 0));
    }

    public getCompleateFlightPieChartData(): any {
        let dataNum = new Array<number>();
        dataNum.push(this.flTransferCount??0);
        dataNum.push(this.flCharterCount??0);
        dataNum.push(this.flScheduleCount??0);
        
        return [{
            data:dataNum,
            backgroundColor: ['rgba(255, 221, 153,0.8)','rgba(148,159,177,0.5)', 'rgba(148,159,177,0.8)', 'rgba(148,159,177,0.2)',],
            datalabels: {
                display:true,
                font: {size: 16},
              }
            }];

    }

    public getCompleateFlightPieChartLabels(): any[] {
        const lineChartLabels = new Array<string>();
        let fltype = AmsFlightTypeOpt.find(x => x.id == AmsFlightType.Transfer);
        lineChartLabels.push(fltype.name);
        fltype = AmsFlightTypeOpt.find(x => x.id == AmsFlightType.Charter);
        lineChartLabels.push(fltype.name);
        fltype = AmsFlightTypeOpt.find(x => x.id == AmsFlightType.Schedule);
        lineChartLabels.push(fltype.name);
        
        return lineChartLabels;

    }

}

export interface IAmsAirline {
    alId: number;
    alName: number;
    iata: string;
    logo: string;
    livery: string;
    slogan: string;
    userId: number;
    apId: number;
    adate: string;
    udate: string;

    countryId: number;
    iso2: string;
    stateId: number;
    subregionId: number;
    regionId: number;
    apLocation: string;
    apLabel: string;
    bankId: number;
    amount: number;
    bankUdate: string;
    hubsCount: number;

    flCharterCount: number;
    flScheduleCount: number;
    flTransferCount: number;
    aircraftCount: number;
}

export class ResponseAirlineGetData {
    airline: IAmsAirline;
}

export class ResponseAirlineGet extends ResponseModel {
    data: ResponseAirlineGetData;
}

export class AmsAirlineTableTableCriteria extends TableCriteriaBase {
    apId?: number;
    countryId?: number;
    stateId?: number;
    subregionId?: number;
    regionId?: number;
}

export class ResponceAmsAirlineTableData {
    airlines: IAmsAirline[];
    rowsCount: RowsCountModel;
}

export class ResponceAmsAirlineTable extends ResponseModel {
    data: ResponceAmsAirlineTableData;
}


export class AirlineDeleteData {
    alId: number;
}

export class ResponceAirlineDelete extends ResponseModel {
    data: AirlineDeleteData;
}

//#endregion

//#region Bank History Chart

export class BankHistory30Days {
    public amount: number;
    public adate: Date;


    public static fromJSON(json: IBankHistory30Days): BankHistory30Days {
        const vs = Object.create(BankHistory30Days.prototype);
        return Object.assign(vs, json, {
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? BankHistory30Days.fromJSON(value) : value;
    }
    /*
    public toJSON(): ILoginsCountModel {
        const vs = Object.create(LoginsCountModel.prototype);
        return Object.assign(vs, this);
    }
    */

    public static dataToList(data: ResponseBankHistory30DaysData): BankHistory30Days[] {
        let rv = new Array<BankHistory30Days>();
        if (data && data.rows && data.rows.length > 0) {
            data.rows.forEach((el: IBankHistory30Days) => {
                const obj = BankHistory30Days.fromJSON(el);
                rv.push(obj);
            });
        }
        return rv;
    }

    public static getLineChartData(rows: BankHistory30Days[]): any {
        const amountData = new Array<number>();
        let lineChartData;

        if (rows && rows.length > 0) {
            for (let idx = 0; idx < rows.length; idx++) {
                const row: BankHistory30Days = rows[idx];
                amountData.push(row.amount);
            }
        }
        lineChartData = [
            { data: amountData, label: 'End of day Balance' },
        ];
        return lineChartData;

    }

    public static getLineChartLabels(rows: BankHistory30Days[]): any[] {
        const lineChartLabels = new Array<string>();

        if (rows && rows.length >= 0) {

            for (let idx = 0; idx < rows.length; idx++) {
                const row: BankHistory30Days = rows[idx];
                let label = '';
                if (row || row.adate) {
                    label = TsDatePipe.padLeft((row.adate.getMonth() + 1).toString(), '0', 2) +
                        '-' + TsDatePipe.padLeft(row.adate.getDate().toString(), '0', 2) +
                        '-' + row.adate.getFullYear();
                }
                lineChartLabels.push(label);
            }

        }
        return lineChartLabels;

    }
}
export interface IBankHistory30Days {
    amount: number;
    adate: string;
}

export class ResponseBankHistory30DaysData {
    rows: IBankHistory30Days[];
}

export class ResponseBankHistory30Days extends ResponseModel {
    data: ResponseBankHistory30DaysData;
}
//#endregion

