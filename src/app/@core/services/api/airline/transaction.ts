import { CurrencyPipe } from "@angular/common";
import { EnumViewModel } from "src/app/@core/models/common/enum-view.model";
import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { TsDatePipe } from "src/app/@core/models/pipes/ts-date.pipe";
import { chartColors, lineColorGreen, lineColorRed, lineColorYellow } from "src/app/@share/charts/ams-chart-line/ams-chart-line.component";
import { Ng2ChartJsCahartPieData, Ng2ChartJsCahartPieDataset } from "src/app/@share/charts/ams-chart-line/dto";
import { Color } from "src/app/@share/charts/models/color.model";
import { RowsCountModel } from "../country/dto";
import { AmsTransactionType, AmsTransactionTypeOpt } from "./enums";

//#region AmsAirline

export class AmsTransaction {
    trId: number;
    trTypeId: AmsTransactionType;
    trName:string;
    amount: number;
    description: string;
    paymentTime: Date;
    posted: boolean;
    isExpenses:boolean;

    apId: number;
    acId: number;
    flId: number;
    alId: number;

    fromBankId: number;
    fromAlId: number;
    toBankId: number;
    toAlId: number;



    public static fromJSON(json: any): AmsTransaction {
        const vs = Object.create(AmsTransaction.prototype);
        return Object.assign(vs, json, {
            trTypeId: json.trTypeId ? json.trTypeId as AmsTransactionType : undefined,
            amount: json.amount ? json.amount : undefined,
            paymentTime: (json && json.paymentTime) ? new Date(json.paymentTime) : undefined,
            posted: (json && json.posted && json.posted === 0) ? false : true,
            isExpenses: (json && json.isExpenses && json.isExpenses === 0) ? false : true,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsTransaction.fromJSON(value) : value;
    }

}

export interface IAmsTransaction {
    trId: number;
    trTypeId: number;
    trName:string;
    amount: number;
    description: string;
    paymentTime: string;
    posted: number;
    isExpenses:number;

    apId: number;
    acId: number;
    flId: number;
    alId: number;

    fromBankId: number;
    fromAlId: number;
    toBankId: number;
    toAlId: number;
}

export class ResponseAmsTransactionGetData {
    transactions: IAmsTransaction[];
}

export class ResponseAmsTransactionGet extends ResponseModel {
    data: ResponseAmsTransactionGetData;
}

export class AmsTransactionTableCriteria extends TableCriteriaBase {
    alId?: number;
    acId?: number;
}

export class ResponceAmsTransactionTableData {
    transactions: AmsTransaction[];
    rowsCount: number;
}

export class ResponceAmsTransactionTable extends ResponseModel {
    data: ResponceAmsTransactionTableData;
}


export class AmsTransactionDeleteData {
    trId: number;
}

export class AmsTransactionDelete extends ResponseModel {
    data: AmsTransactionDeleteData;
}

//#endregion

//#region AmsTransactionsSum



export class AmsTransactionsSum {
    trDate: Date;
    alId: number;
    weekday: number;
    expences: number;
    income: number;
    revenue: number;

    public static fromJSON(json: IAmsTransactionsSum): AmsTransactionsSum {
        const vs = Object.create(AmsTransactionsSum.prototype);
        return Object.assign(vs, json, {
            weekday: json.weekday ? json.weekday : undefined,
            expences: json.expences ? json.expences : undefined,
            income: json.income ? json.income : undefined,
            revenue: json.revenue ? json.revenue : undefined,
            trDate: (json && json.trDate) ? new Date(json.trDate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsTransactionsSum.fromJSON(value) : value;
    }

    public static dataToList(data: AmsTransactionsSumLastWeekData): AmsTransactionsSum[] {
        let rv = new Array<AmsTransactionsSum>();

        if (data && data.trSumList) {
            for (const iterator of data.trSumList) {
                const element = AmsTransactionsSum.fromJSON(iterator);
                rv.push(element);
            }
        }

        /*
        for (let idx = 1; idx < 7; idx++) {
            let element = new AmsTransactionsSum();
            element.weekday = idx;
            element.expences = 0;
            element.income = 0;
            element.revenue = 0;
            element.trDate = new Date(new Date().getTime() - ((6-idx) * (1000 * 60 * 60 * 24)));
            if (data && data.trSumList && data.trSumList.length > 0) {
                const el: IAmsTransactionsSum = data.trSumList.find(x => x.weekday == idx);
                if (el && el.alId) {
                    element = AmsTransactionsSum.fromJSON(el);
                }
            }
            rv.push(element);
        }
        */


        return rv;
    }

    
    public static getLineChartData(rows: AmsTransactionsSum[]): any {
        const expences = new Array<number>();
        const income = new Array<number>();
        const revenue = new Array<number>();
        let lineChartData;

        if (rows && rows.length > 0) {
            for (let idx = 0; idx < rows.length; idx++) {
                const row: AmsTransactionsSum = rows[idx];
                
                expences.push(row && row?.expences ? row?.expences : 0.01);
                income.push(row && row?.income ? row.income : 0.01);
                revenue.push(row && row?.revenue ? row.revenue : 0.01);
            }
        }
        lineChartData = [
            {
                data: income, label: 'Income',
                backgroundColor: chartColors.green.backgroundColor,
                borderColor: chartColors.green.borderColor,
                pointBackgroundColor: chartColors.green.pointBackgroundColor,
                pointBorderColor: chartColors.green.pointBorderColor,
                pointHoverBackgroundColor: chartColors.green.pointHoverBackgroundColor,
                pointHoverBorderColor: chartColors.green.pointHoverBorderColor,
                fill: 'origin',
            },
            {
                data: revenue, label: 'Revenue',
                backgroundColor: chartColors.yelow.backgroundColor,
                borderColor: chartColors.yelow.borderColor,
                pointBackgroundColor: chartColors.yelow.pointBackgroundColor,
                pointBorderColor: chartColors.yelow.pointBorderColor,
                pointHoverBackgroundColor: chartColors.yelow.pointHoverBackgroundColor,
                pointHoverBorderColor: chartColors.yelow.pointHoverBorderColor,
                fill: 'origin'
            },
            {
                data: expences, label: 'Expences',
                backgroundColor: chartColors.red.backgroundColor,
                borderColor: chartColors.red.borderColor,
                pointBackgroundColor: chartColors.red.pointBackgroundColor,
                pointBorderColor: chartColors.red.pointBorderColor,
                pointHoverBackgroundColor: chartColors.red.pointHoverBackgroundColor,
                pointHoverBorderColor: chartColors.red.pointHoverBorderColor,
                fill: 'origin'
            },
        ];
        return lineChartData;

    }

    public static getLineChartLabels(rows: AmsTransactionsSum[]): any[] {
        const lineChartLabels = new Array<string>();

        if (rows && rows.length >= 0) {

            for (let idx = 0; idx < rows.length; idx++) {
                const row: AmsTransactionsSum = rows[idx];
                let label = '';
                if (row || row.trDate) {
                    label = row.trDate.toString().split(' ')[0];
                }
                lineChartLabels.push(label);
            }

        }
        return lineChartLabels;

    }
}

export interface IAmsTransactionsSum {
    trDate: string;
    alId: number;
    weekday: number;
    expences: number;
    income: number;
    revenue: number;
}

export class AmsTransactionsSumLastWeekData {
    trSumList: IAmsTransactionsSum[];
}

export class ResponceAmsTransactionsSumLastWeek extends ResponseModel {
    data: AmsTransactionsSumLastWeekData;
}


//#endregion

//#region  AmsTransactionTypeSum

export const rgbaGreen = 'rgba(0, 204, 0, 0.8)'
export const rgbaRed = 'rgba(255,0,0,0.9)'
export const chartColorRed: Color = { // red
    backgroundColor: 'rgba(255,0,0,0.9)',
    borderColor: 'red',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
};

export class AmsTransactionTypeSum {
    trTypeId: AmsTransactionType;
    trName: string;
    amount: number;
    trDate: Date;
    apId: number;

    public static fromJSON(json: IAmsTransactionTypeSum): AmsTransactionTypeSum {
        const vs = Object.create(AmsTransactionTypeSum.prototype);
        return Object.assign(vs, json, {
            trTypeId: json.trTypeId ? json.trTypeId as AmsTransactionType : undefined,
            amount: json.amount ? json.amount : undefined,
            trDate: (json && json.trDate) ? new Date(json.trDate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsTransaction.fromJSON(value) : value;
    }

    public static dataToList(data: AmsTransactionTypeSumData): AmsTransactionTypeSum[] {
        let rv = new Array<AmsTransactionTypeSum>();
        if (data && data.trTypeSum && data.trTypeSum.length > 0) {
            data.trTypeSum.forEach((tr: IAmsTransactionTypeSum) => {
                const element = AmsTransactionTypeSum.fromJSON(tr);
                rv.push(element);
            });
        }

        return rv;
    }

    public static getPieChartData(rows: AmsTransactionTypeSum[]): any {
        let dataNum = new Array<number>();
        if (rows && rows.length > 0) {
            let sumTotal = 0;
            for(const row of rows) {
                sumTotal += Math.abs(row.amount);
            }
            for (let idx = 0; idx < rows.length; idx++) {
                const row: AmsTransactionTypeSum = rows[idx];
                dataNum.push(row && row.amount ? row.amount : 0);
            }
        }
        
        return dataNum;

    }

    public static getPieChartLabels(rows: AmsTransactionTypeSum[], currencyPipe: CurrencyPipe = undefined): any[] {
        const lineChartLabels = new Array<string>();

        if (rows && rows.length >= 0) {

            for (let idx = 0; idx < rows.length; idx++) {
                const row: AmsTransactionTypeSum = rows[idx];
                let label = '';
                if (row || row.trName) {
                    //const jsDate: Date = GvDatePipe.getDate(dateStr);
                    //console.log('getLineChartLabels -> logdate= ', row.logdate);
                    label = row.trName;
                    if (currencyPipe && false) {
                        label += ' ' + currencyPipe.transform(row.amount, 'USD', true);
                    }
                }
                lineChartLabels.push(label);
            }

        }
        return lineChartLabels;

    }

    public static getPieChartColors(rows: AmsTransactionTypeSum[]): string[] {
        const colors = new Array<string>();
        
        for(const row of rows) {
            const data: EnumViewModel = AmsTransactionTypeOpt.find(x => x.id === row.trTypeId);
            colors.push(data.color);
        }

        return colors;//[{ backgroundColor: colors, },];
    }

}

export interface IAmsTransactionTypeSum {
    trTypeId: number;
    trName: string;
    amount: number;
    trDate: string;
    apId: number;

}

export class AmsTransactionTypeSumData {
    trTypeSum: IAmsTransactionTypeSum[];
}

export class ResponceAmsTransactionTypeSumData extends ResponseModel {
    data: AmsTransactionTypeSumData;
}

//#endregion

//#region AmsTransactionsSum

export class AmsTransStat8Weeks {
    trTypeId: number;
    trName: string;
    mon: number;
    tue: number;
    wed: number;
    thu: number;
    fri: number;
    sat: number;
    sun: number;

    public static fromJSON(json: IAmsTransStat8Weeks): AmsTransStat8Weeks {
        const vs = Object.create(AmsTransStat8Weeks.prototype);
        return Object.assign(vs, json, {
            trTypeId: json.trTypeId ? json.trTypeId as AmsTransactionType : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsTransStat8Weeks.fromJSON(value) : value;
    }

}

export interface IAmsTransStat8Weeks {
    trTypeId: number;
    trName: string;
    mon: number;
    tue: number;
    wed: number;
    thu: number;
    fri: number;
    sat: number;
    sun: number;
}

export class AmsTransStatWeek {
    trWeek: number;
    revenue: number;

    public static fromJSON(json: IAmsTransStatWeek): AmsTransStatWeek {
        const vs = Object.create(AmsTransStatWeek.prototype);
        return Object.assign(vs, json, {
            trWeek: json.trWeek ? json.trWeek : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsTransStatWeek.fromJSON(value) : value;
    }

}

export interface IAmsTransStatWeek {
    trWeek: number;
    revenue: number;
}

export class AmsTransStat8WeeksData {
    weeks: IAmsTransStatWeek[];
    transStat: IAmsTransStat8Weeks[];
    week: number;
}

export class ResponceAmsTransStat8WeeksData extends ResponseModel {
    data: AmsTransStat8WeeksData;
}


//#endregion

//#region AmsRevenue


export class AmsRevenue {
    rdId: number;
    bankId: number;
    alId: number;
    rdDate: Date;
    expences: number;
    income: number;
    revenue: number;

    public static fromJSON(json: any): AmsRevenue {
        const vs = Object.create(AmsRevenue.prototype);
        return Object.assign(vs, json, {
            rdId: json.rdId,
            expences:json && json.expences?-1*json.expences:0,
            rdDate: (json && json.rdDate) ? new Date(json.rdDate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsRevenue.fromJSON(value) : value;
    }

    public static getRevenueLineChartData(rows: AmsRevenue[]): any {
        let lineChartData;

        if (rows && rows.length > 0) {
            const amountData = new Array<number>();
            
            for (let idx = rows.length-1; idx >= 0; idx--) {
                const row: AmsRevenue = rows[idx];
                amountData.push(row.revenue);
            }

            lineChartData = [
                { data: amountData, label: 'End of day Revenue' },
            ];
        }
        return lineChartData;

    }

    public static getRevenueLineChartLabels(rows: AmsRevenue[]): any[] {
        const lineChartLabels = new Array<string>();
        
        if (rows && rows.length >= 0) {

            for (let idx = rows.length-1; idx >= 0; idx--) {
                const row: AmsRevenue = rows[idx];
                let label = '';
                if (row || row.rdDate) {
                    //const jsDate: Date = GvDatePipe.getDate(dateStr);
                    //console.log('getLineChartLabels -> logdate= ', row.logdate);
                    label = TsDatePipe.padLeft((row.rdDate.getMonth() + 1).toString(), '0', 2) +
                        '-' + TsDatePipe.padLeft(row.rdDate.getDate().toString(), '0', 2) +
                        '-' + row.rdDate.getFullYear();
                }
                lineChartLabels.push(label);
            }

        }
        return lineChartLabels;

    }

}

export interface IAmsRevenue {
    rdId: number;
    bankId: number;
    alId: number;
    rdDate: string;
    expences: number;
    income: number;
    revenue: number;
}

export class AmsRevenueTableCriteria extends TableCriteriaBase {
    alId?: number;
}

export class ResponceAmsRevenueTableData {
    revenues: AmsRevenue[];
    rowsCount:number;
}

export class ResponceAmsRevenueTable extends ResponseModel {
    data: ResponceAmsRevenueTableData;
}


//#endregion

