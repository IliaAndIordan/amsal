import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { URL_COMMON_IMAGE_AIRLINE } from "src/app/@core/const/app-storage.const";
import { AcType } from "src/app/@core/models/pipes/ac-type.pipe";
import { DateModel } from "src/app/@core/models/common/date.model";
import { WeekDay } from "@angular/common";
import { AmsFlightType, AmsPayloadType } from "../flight/enums";
import { FlightPlanStatus } from "./enums";

//#region AmsSheduleGroup

export class AmsSheduleGroup {
    grpId: number;
    grpName: string;
    alId: number;
    acIdPrototype: number;
    apId: number;
    acTypeId: AcType;
    minRunwayM: number;
    maxRangeKm: number;
    cruiseSpeedKmph: number;
    fuelConsumptionLp100km: number;
    acId: number;
    isActive: boolean;
    startDate: Date;

    public static fromJSON(json: any): AmsSheduleGroup {
        //console.log('fromJSON -> json:', json);
        const vs = Object.create(AmsSheduleGroup.prototype);
        const rv = Object.assign(vs, json, {
            grpId: json?.grpId,
            alId: json?.alId,
            acIdPrototype: json?.acIdPrototype,
            acTypeId: json?.acTypeId ? json.acTypeId as AcType : AcType.Piston,
            acId: json?.acId,
            isActive: json && json.isActive > 0 ? true : false,
            startDate: (json && json.startDate) ? new Date(json.startDate) : undefined,
        });
        //console.log('fromJSON -> rv:', rv);
        return rv;
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsSheduleGroup.fromJSON(value) : value;
    }

    public toJSON(): IAmsSheduleGroup {
        const tzoffset = (new Date()).getTimezoneOffset() * 60000;
        return Object.assign({}, this, {
            isActive: this.isActive ? 1 : 0,
            startDate: this.startDate ? DateModel.getTimeStr(this.startDate) : undefined,
        });
    }

}

export interface IAmsSheduleGroup {
    grpId?: number;
    grpName?: string;
    alId?: number;
    acIdPrototype?: number;
    apId?: number;
    acTypeId?: number;
    minRunwayM?: number;
    maxRangeKm?: number;
    cruiseSpeedKmph?: number;
    fuelConsumptionLp100km?: number;
    acId?: number;
    isActive?: number;
    startDate: string;
}


export class ResponseAmsSheduleGroupGetData {
    group: IAmsSheduleGroup;
}

export class ResponseAmsSheduleGroupGet extends ResponseModel {
    data: ResponseAmsSheduleGroupGetData;
}

export class ResponseAmsSheduleGroupDeleteData {
    grpId: number;
}

export class ResponseAmsSheduleGroupDelete extends ResponseModel {
    data: ResponseAmsSheduleGroupDeleteData;
}

export class AmsSheduleGroupTableCriteria extends TableCriteriaBase {
    alId?: number;
}

export class ResponceAmsSheduleGroupTableData {
    groups: AmsSheduleGroup[];
    rowsCount: number;
}

export class ResponceAmsSheduleGroupTable extends ResponseModel {
    data: ResponceAmsSheduleGroupTableData;
}


//#endregion

//#region AmsFlightPlanPayload

export class AmsFlightPlanTransfer {
    ppdId: number;
    cpdId: number;
    flpId: number;
    flpId2: number;
    flpId3?: number;
    flpId4?: number;
    depApId: number;
    arrApId: number;
    distanceKm: number;
    flName?: string;
    flName2?: string;
    flName3?: string;
    flName4?: string;
    remainPax?: number;
    remainPax2?: number;
    remainPax3?: number;
    remainPax4?: number;
    remainPayloadKg?:number;
    remainPayloadKg2?:number;
    remainPayloadKg3?:number;
    remainPayloadKg4?:number;
    diffPrice: number;
    sumPrice: number;
    pPrice: number;
    paxClassId: number;
    pax: number;
    payloadKg: number;
    note: string;
    payloadType:AmsPayloadType;



    public static fromJSON(json: any): AmsFlightPlanTransfer {
        const vs = Object.create(AmsFlightPlanTransfer.prototype);
        return Object.assign(vs, json, {
            ppdId: json?.ppdId,
            cpdId: json?.cpdId,
            payloadType:json.cpdId>0?AmsPayloadType.C:AmsPayloadType.E,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsFlightPlanTransfer.fromJSON(value) : value;
    }

}

export interface IAmsFlightPlanTransfer {
    ppdId: number;
    flpId: number;
    flpId2: number;
    flpId3: number;
    flpId4: number;
    depApId: number;
    arrApId: number;
    distanceKm: number;
    flName?: string;
    flName2?: string;
    flName3?: string;
    flName4?: string;
    remainPaxE?: number;
    remainPaxE2?: number;
    remainPaxE3?: number;
    remainPaxE4?: number;
    remainPayloadKg?:number;
    remainPayloadKg2?:number;
    remainPayloadKg3?:number;
    remainPayloadKg4?:number;
    sumPrice: number;
    diffPrice: number;
    pPrice: number;
    paxClassId: number;
    pax: number;
    payloadKg: number;
    note: string;
}

export class AmsFlightPlanTransferTableCriteria extends TableCriteriaBase {
    apId?: number;
}

export class AmsFlightPlanTransferTableData {
    routes: AmsFlightPlanTransfer[];
    rowsCount: number;
}

export class ResponceAmsFlightPlanTransferTable extends ResponseModel {
    data: AmsFlightPlanTransferTableData;
}


//#endregion

//#region AmsFlightPlanPayload

export class AmsFlightPlanPayload {
    flpId: number;
    flTypeId: AmsFlightType;
    flpnsId: number;
    yearweek: number;
    wdId: number;
    flpnId:number;
    flNumber: number;
    flName: string;
    routeId: number;
    destId: number;
    grpId: number;
    grpName: string;
    dApId: number;
    aApId: number;
    alId: number;

    acId: number;
    macId: number;
    homeApId: number;
    acStatusId: number;
    opTimeMin: number;
    flightHInQueue: number;
    flightInQueue: number;
    lastFlqAtime: Date;
    distanceKm: number;
    oilL: number;
    flightH: number;
    pax: number;
    payloadKg: number;
    price: number;
    dtime: Date;
    atime: Date;
    flpStatusId: FlightPlanStatus;

    availablePaxF: number;
    availablePaxB: number;
    availablePaxE: number;
    availableCargoKg: number;

    flpplCount: number;
    flpplPaxF: number;
    flpplPaxB: number;
    flpplPaxE: number;
    flpplPayloadKg: number;
    flpplPrice: number;

    remainPaxF: number;
    remainPaxB: number;
    remainPaxE: number;
    remainPayloadKg: number;

    paxPctFull: number;
    payloadKgPctFull: number;
    payloadJobsCount: number;


    public static fromJSON(json: any): AmsFlightPlanPayload {
        const vs = Object.create(AmsFlightPlanPayload.prototype);
        return Object.assign(vs, json, {
            grpId: json?.grpId,
            alId: json?.alId,
            flTypeId: json?.flTypeId ? json.flTypeId as AmsFlightType : AmsFlightType.Schedule,
            acTypeId: json?.acTypeId ? json.acTypeId as AcType : AcType.Piston,
            dtime: (json && json.dtime) ? new Date(json.dtime) : undefined,
            atime: (json && json.atime) ? new Date(json.atime) : undefined,
            lastFlqAtime: (json && json.lastFlqAtime) ? new Date(json.lastFlqAtime) : undefined,
            flpStatusId: json?.flpStatusId ? json.flpStatusId as FlightPlanStatus : FlightPlanStatus.AvailableForTicketSell,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsFlightPlanPayload.fromJSON(value) : value;
    }

    public toJSON(): IAmsFlightPlanPayload {
        const tzoffset = (new Date()).getTimezoneOffset() * 60000;
        return Object.assign({}, this, {
            dtime: this.dtime ? DateModel.getTimeStr(this.dtime) : undefined,
            atime: this.atime ? DateModel.getTimeStr(this.atime) : undefined,
            lastFlqAtime: this.lastFlqAtime ? DateModel.getTimeStr(this.lastFlqAtime) : undefined,
        });
    }

}

export interface IAmsFlightPlanPayload {
    flpId: number;
    flTypeId: number;
    flpnsId: number;
    yearweek: number;
    wdId: number;
    flpnId:number;
    flNumber: number;
    flName: string;
    routeId: number;
    destId: number;
    grpId: number;
    grpName: string;
    dApId: number;
    aApId: number;
    alId: number;

    acId: number;
    macId: number;
    homeApId: number;
    acStatusId: number;
    opTimeMin: number;
    flightHInQueue: number;
    flightInQueue: number;
    lastFlqAtime: Date;
    distanceKm: number;
    oilL: number;
    flightH: number;
    pax: number;
    payloadKg: number;
    price: number;
    dtime: Date;
    atime: Date;
    flpStatusId: number;

    availablePaxF: number;
    availablePaxB: number;
    availablePaxE: number;
    availableCargoKg: number;

    flpplCount: number;
    flpplPaxF: number;
    flpplPaxB: number;
    flpplPaxE: number;
    flpplPayloadKg: number;
    flpplPrice: number;

    remainPaxF: number;
    remainPaxB: number;
    remainPaxE: number;
    remainPayloadKg: number;

    paxPctFull: number;
    payloadKgPctFull: number;
    payloadJobsCount: number;
}

export class AmsFlightPlanPayloadTableCriteria extends TableCriteriaBase {
    alId?: number;
    grpId?: number;
    wdId?: number;
    flpnId?: number;
    flpId?: number;
    arrApId?: number;
    depApId?: number;
    acId?:number;
}

export class AmsFlightPlanPayloadTableData {
    flightplans: AmsFlightPlanPayload[];
    rowsCount: number;
}

export class ResponceAmsFlightPlanPayloadTable extends ResponseModel {
    data: AmsFlightPlanPayloadTableData;
}

export class AmsFlightPlanStatusSave {
    flpId: number;
    flpStatusId: number;
    payloadJobs: number;
}


//#endregion

//#region AmsAlSheduleRoute

export class AmsAlSheduleRoute {
    routeId: number;
    routeNr: number;
    routeName: string;
    flightH: number;
    distanceKm: number;
    destId: number;
    revDestId: number;
    depApId: number;
    arrApId: number;
    priceE: number;
    priceB: number;
    priceF: number;
    priceCPerKg: number;
    adate: Date;
    udate: Date;
    maxPriceE: number;
    maxPriceB: number;
    maxPriceF: number;
    maxPriceCPerKg: number;
    flightsCount: number;


    public static fromJSON(json: any): AmsAlSheduleRoute {
        const vs = Object.create(AmsAlSheduleRoute.prototype);
        return Object.assign(vs, json, {
            routeId: json && json.routeId ? json.routeId : undefined,
            routeNr: json && json.routeNr ? json.routeNr : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsAlSheduleRoute.fromJSON(value) : value;
    }

}

export interface IAmsAlSheduleRoute {
    routeId: number;
    routeNr: number;
    routeName: string;
    flightH: number;
    distanceKm: number;
    destId: number;
    revDestId: number;
    depApId: number;
    arrApId: number;
    priceE: number;
    priceB: number;
    priceF: number;
    priceCPerKg: number;
    adate: string;
    udate: string;

    maxPriceE: number;
    maxPriceB: number;
    maxPriceF: number;
    maxPriceCPerKg: number;
    flightsCount: number;
}


export class ResponseAmsSheduleRouteGetData {
    route: IAmsAlSheduleRoute;
}

export class ResponseAmsSheduleRouteGet extends ResponseModel {
    data: ResponseAmsSheduleRouteGetData;
}

export class ResponseAmsSheduleRouteDeleteData {
    routeId: number;
}

export class ResponseAmsSheduleRouteDelete extends ResponseModel {
    data: ResponseAmsSheduleRouteDeleteData;
}

export class AmsSheduleRouteTableCriteria extends TableCriteriaBase {
    alId?: number;
    apId?: number;
    depApId?: number;
    arrApId?: number;
}

export class ResponceAmsSheduleRouteTableData {
    routes: AmsAlSheduleRoute[];
    rowsCount: number;
}

export class ResponceAmsSheduleRouteTable extends ResponseModel {
    data: ResponceAmsSheduleRouteTableData;
}


//#endregion