
//#region AmsRevenue

import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { AmsHubTypes } from "./enums";



export class AmsHub {
    hubId: number;
    apId: number;
    alId: number;
    upkeepDay: Date;
    adate: Date;
    hubTypeId: AmsHubTypes;
    hubName: string;
    taxPerWeek: number;
    discount: number;
    upkeepTimeMs: number;
    flpnCount: number;

    public static fromJSON(json: any): AmsHub {
        const vs = Object.create(AmsHub.prototype);
        return Object.assign(vs, json, {
            hubId: json.hubId,
            hubTypeId: (json && json.hubTypeId) ? json.hubTypeId as AmsHubTypes : AmsHubTypes.Hub,
            upkeepDay: (json && json.upkeepDay) ? new Date(json.upkeepDay) : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsHub.fromJSON(value) : value;
    }

}

export interface IAmsHub {
    hubId: number;
    apId: number;
    alId: number;
    upkeepDay: string;
    adate: string;
    hubTypeId: number;
    hubName: string;
    taxPerWeek: number;
    discount: number;
    upkeepTimeMs: number;
    flpnCount: number;
}


export class ResponseAmsHubGetData {
    hub: IAmsHub;
}

export class ResponseAmsHubGet extends ResponseModel {
    data: ResponseAmsHubGetData;
}

export class ResponseAmsHubDeleteData {
    hubId: number;
}

export class ResponseAmsHubDelete extends ResponseModel {
    data: ResponseAmsHubDeleteData;
}

export class AmsHubTableCriteria extends TableCriteriaBase {
    alId?: number;
    apId?: number;
}

export class ResponceAmsHubTableData {
    hubs: AmsHub[];
    rowsCount: number;
}

export class ResponceAmsHubTable extends ResponseModel {
    data: ResponceAmsHubTableData;
}


//#endregion