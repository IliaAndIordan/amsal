import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { lastValueFrom, Observable, of, Subscription } from 'rxjs';
import { catchError, map, take, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { environment } from 'src/environments/environment';
import { AmsAirline, AmsAirlineTableTableCriteria, ResponceAirlineDelete, ResponceAmsAirlineTable, ResponceAmsAirlineTableData, ResponseAirlineGet, ResponseBankHistory30Days } from './dto';
import { AmsRevenue, AmsRevenueTableCriteria, AmsTransaction, AmsTransactionsSumLastWeekData, AmsTransactionTableCriteria, AmsTransactionTypeSumData, AmsTransStat8WeeksData, IAmsRevenue, IAmsTransaction, ResponceAmsRevenueTable, ResponceAmsRevenueTableData, ResponceAmsTransactionsSumLastWeek, ResponceAmsTransactionTable, ResponceAmsTransactionTableData, ResponceAmsTransactionTypeSumData, ResponceAmsTransStat8WeeksData } from './transaction';
import { AmsHub, AmsHubTableCriteria, ResponceAmsHubTable, ResponceAmsHubTableData, ResponseAmsHubDelete, ResponseAmsHubGet } from './al-hub';
import { AmsAlSheduleRoute, AmsFlightPlanPayload, AmsFlightPlanPayloadTableCriteria, AmsFlightPlanPayloadTableData, AmsFlightPlanStatusSave, AmsFlightPlanTransfer, AmsFlightPlanTransferTableCriteria, AmsFlightPlanTransferTableData, AmsSheduleGroup, AmsSheduleGroupTableCriteria, AmsSheduleRouteTableCriteria, ResponceAmsFlightPlanPayloadTable, ResponceAmsFlightPlanTransferTable, ResponceAmsSheduleGroupTable, ResponceAmsSheduleGroupTableData, ResponceAmsSheduleRouteTable, ResponceAmsSheduleRouteTableData, ResponseAmsSheduleGroupDelete, ResponseAmsSheduleGroupGet, ResponseAmsSheduleRouteDelete, ResponseAmsSheduleRouteGet } from './al-flp-shedule';
import {
  AmsAlFlightNumber, AmsAlFlightNumberSchedule, AmsAlFlightNumberScheduleCopy, AmsAlFlightNumberScheduleTableCriteria, AmsAlFlightNumberTableCriteria, AmsAlFlpPayloadsCriteria, AmsAlFlpPayloadsData, AmsDestination, AmsFlpPayloads, ResponceAmsAlFlightNumberScheduleCopy, ResponceAmsAlFlightNumberScheduleCopyData, ResponceAmsAlFlightNumberScheduleTable, ResponceAmsAlFlightNumberScheduleTableData, ResponceAmsAlFlightNumberTable,
  ResponceAmsAlFlightNumberTableData, ResponseAmsAlFlightNumberGet,
  ResponseAmsAlFlightNumberScheduleGet,
  ResponseAmsAlFlightNumberScheduleUpdateFromFlp,
  ResponseAmsAlFlpPayloads,
  ResponseAmsDestinationGet
} from './al-flp-number';
import { ResponseDelete, ResponseModel } from 'src/app/@core/models/common/responce.model';

@Injectable({ providedIn: 'root' })
export class AmsAirlineClient extends ApiBaseClient {

  constructor(
    private client: HttpClient,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return environment.services.url.airline;
  }


  //#region AmsAirline

  airlinerTable(criteria: AmsAirlineTableTableCriteria): Observable<ResponceAmsAirlineTableData> {

    console.log('airlinerTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'al_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        apId: criteria.apId,

        regionId: criteria.regionId,
        subregionId: criteria.subregionId,
        countryId: criteria.countryId,
        stateId: criteria.stateId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsAirlineTable) => {
          const res = Object.assign(new ResponceAmsAirlineTable(), resp);
          console.log('airlinerTable -> response=', res);
          return res.data;
        }),
        tap(event => {
          this.log(`tap airlinerTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  airlineGet(value: number): Observable<AmsAirline> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airline_get',
      { alId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAirlineGet) => {
          const res = Object.assign(new ResponseAirlineGet(), response);
          let rv: AmsAirline;
          if (res && res.success) {
            if (res && res.data && res.data.airline) {
              rv = AmsAirline.fromJSON(res.data.airline);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap airlineGet get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }


  airlineSave(value: AmsAirline): Observable<AmsAirline> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airline_save',
      { airline: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAirlineGet) => {
          const res = Object.assign(new ResponseAirlineGet(), response);
          let rv: AmsAirline;
          if (res && res.success) {
            if (res && res.data && res.data.airline) {
              rv = AmsAirline.fromJSON(res.data.airline);
            }
          }
          console.log('airlineSave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap airlineSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  airlineDelete(value: number): Observable<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airline_delete',
      { alId: value },
      { headers: hdrs }).pipe(
        map((response: ResponceAirlineDelete) => {
          const res = Object.assign(new ResponceAirlineDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.alId) {
              rv = res.data.alId;
            }
          }
          console.log('airlineDelete-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap airlineDelete delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );

  }

  airlineLogo(value: number): Observable<AmsAirline> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airline_logo',
      { alId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAirlineGet) => {
          const res = Object.assign(new ResponseAirlineGet(), response);
          let rv: AmsAirline;
          if (res && res.success) {
            if (res && res.data && res.data.airline) {
              rv = AmsAirline.fromJSON(res.data.airline);
            }
          }
          //console.log('airlineLogo-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap airlineGet get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  airlineLivery(value: number): Observable<AmsAirline> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airline_livery',
      { alId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAirlineGet) => {
          const res = Object.assign(new ResponseAirlineGet(), response);
          let rv: AmsAirline;
          if (res && res.success) {
            if (res && res.data && res.data.airline) {
              rv = AmsAirline.fromJSON(res.data.airline);
            }
          }
          console.log('airlineLivery-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap airlineGet get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

  //#region Bank


  airlineBankHistory30Days(value: number): Observable<ResponseBankHistory30Days> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airline_bank_h30days',
      { alId: value },
      { headers: hdrs }).pipe(
        map((resp: ResponseBankHistory30Days) => {
          const res = Object.assign(new ResponseBankHistory30Days(), resp);
          return res;
        }),
        tap(event => {
          this.log(`tap airlineBankHistory30Days event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }


  transactionsSumLastWeek(alId: number): Observable<AmsTransactionsSumLastWeekData> {
    //console.log('transactionsSumLastWeek-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_transaction_sum_last_week',
      {
        alId: alId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsTransactionsSumLastWeek) => {
          const res = Object.assign(new ResponceAmsTransactionsSumLastWeek(), resp);
          //console.log('transactionsSumLastWeek -> response=', res);
          return res.data;
        }),
        catchError(this.handleError)
      );
  }

  transactionsSumToday(alId: number): Observable<AmsTransactionTypeSumData> {

    console.log('transactionsSumToday-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_transaction_sum_today',
      {
        alId: alId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsTransactionTypeSumData) => {
          const res = Object.assign(new ResponceAmsTransactionTypeSumData(), resp);
          //console.log('transactionsSumToday -> response=', res);
          return res.data;
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

  //#region Transaction Stat

  transStat8Weeks(alId: number, trWeek: number): Observable<AmsTransStat8WeeksData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_trans_stat_lst_8_weeks',
      {
        alId: alId,
        trweek: trWeek,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsTransStat8WeeksData) => {
          const res = Object.assign(new ResponceAmsTransStat8WeeksData(), resp);
          console.log('transStat8Weeks -> response=', res);
          return res.data;
        }),
        catchError(this.handleError)
      );
  }

  airlineRevenuesTable(req: AmsRevenueTableCriteria): Observable<ResponceAmsRevenueTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_revenues_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsRevenueTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsRevenueTable(), resp);
          let data = new ResponceAmsRevenueTableData();
          data.revenues = new Array<AmsRevenue>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.revenues && res.data.revenues.length > 0) {
              res.data.revenues.forEach(element => {
                data.revenues.push(AmsRevenue.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async alTransactionsNoFlightTableP(criteria: AmsTransactionTableCriteria): Promise<ResponceAmsTransactionTableData> {
    const source$ = this.alTransactionsNoFlightTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  alTransactionsNoFlightTable(req: AmsTransactionTableCriteria): Observable<ResponceAmsTransactionTableData> {
    //console.log('alTransactionsNoFlightTable-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_transaction_table_no_flight',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
        acId: req.acId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsTransactionTable) => {
          //console.log('alTransactionsNoFlightTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsTransactionTable(), resp);
          let data = new ResponceAmsTransactionTableData();
          data.transactions = new Array<AmsTransaction>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.transactions && res.data.transactions.length > 0) {
              res.data.transactions.forEach((element:AmsTransaction) => {
                data.transactions.push(AmsTransaction.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

  //#region Airklinr Hubs

  airlineHubsTable(req: AmsHubTableCriteria): Observable<ResponceAmsHubTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'airline_hub_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
        apId: req.apId
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsHubTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsRevenueTable(), resp);
          let data = new ResponceAmsHubTableData();
          data.hubs = new Array<AmsHub>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.hubs && res.data.hubs.length > 0) {
              res.data.hubs.forEach(element => {
                data.hubs.push(AmsHub.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  hubSaveOld(value: AmsHub): Observable<AmsHub> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'airline_hub_save',
      { hub: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsHubGet) => {
          const res = Object.assign(new ResponseAmsHubGet(), response);
          let rv: AmsHub;
          if (res && res.success) {
            if (res && res.data && res.data.hub) {
              rv = AmsHub.fromJSON(res.data.hub);
            }
          }
          console.log('hubSave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap hubSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  async hubSave(value: AmsHub): Promise<AmsHub> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'airline_hub_save',
      { hub: value },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsHubGet) => {
          const res = Object.assign(new ResponseAmsHubGet(), response);
          let rv: AmsHub;
          if (res && res.success) {
            if (res && res.data && res.data.hub) {
              rv = AmsHub.fromJSON(res.data.hub);
            }
          }
          //console.log('hubSave-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsHub>(source$);
  }

  async hubDelete(id: number): Promise<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'airline_hub_delete',
      { hubId: id },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsHubDelete) => {
          const res = Object.assign(new ResponseAmsHubDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.hubId) {
              rv = res.data.hubId;
            }
          }
          console.log('hubDelete-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<number>(source$);
  }
  //#endregion

  //#region AmsSheduleGroup

  async sheduleGroupsTableP(criteria: AmsSheduleGroupTableCriteria): Promise<ResponceAmsSheduleGroupTableData> {
    const source$ = this.sheduleGroupsTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  sheduleGroupsTable(req: AmsSheduleGroupTableCriteria): Observable<ResponceAmsSheduleGroupTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_shedule_group_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsSheduleGroupTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsSheduleGroupTable(), resp);
          let data = new ResponceAmsSheduleGroupTableData();
          data.groups = new Array<AmsSheduleGroup>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.groups && res.data.groups.length > 0) {
              res.data.groups.forEach(element => {
                data.groups.push(AmsSheduleGroup.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async sheduleGroupSave(value: AmsSheduleGroup): Promise<AmsSheduleGroup> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_shedule_group_save',
      { group: value },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsSheduleGroupGet) => {
          const res = Object.assign(new ResponseAmsSheduleGroupGet(), response);
          let rv: AmsSheduleGroup;
          if (res && res.success) {
            if (res && res.data && res.data.group) {
              rv = AmsSheduleGroup.fromJSON(res.data.group);
            }
          }
          //console.log('hubSave-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsSheduleGroup>(source$);
  }

  async sheduleGroupDelete(id: number): Promise<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_shedule_group_delete',
      { grpId: id },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsSheduleGroupDelete) => {
          const res = Object.assign(new ResponseAmsSheduleGroupDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.grpId) {
              rv = res.data.grpId;
            }
          }
          console.log('hubDelete-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<number>(source$);
  }

  //#endregion

  //#region AmsDestination

  async destinationGet(depId: number, arrId: number): Promise<AmsDestination> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'destination_get',
      { depApId: depId, arrApId: arrId },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsDestinationGet) => {
          const res = Object.assign(new ResponseAmsDestinationGet(), response);
          let rv: AmsDestination;
          if (res && res.success) {
            if (res && res.data && res.data.dest) {
              rv = AmsDestination.fromJSON(res.data.dest);
            }
          }
          console.log('destinationGet-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsDestination>(source$);
  }
  //#endregion

  //#region AmsAlFlightNumber

  async alFlightNumberTableP(criteria: AmsAlFlightNumberTableCriteria): Promise<ResponceAmsAlFlightNumberTableData> {
    const source$ = this.alFlightNumberTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  alFlightNumberTable(req: AmsAlFlightNumberTableCriteria): Observable<ResponceAmsAlFlightNumberTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_flnr_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
        depApId: req.depApId,
        arrApId: req.arrApId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsAlFlightNumberTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsAlFlightNumberTable(), resp);
          let data = new ResponceAmsAlFlightNumberTableData();
          data.flnrs = new Array<AmsAlFlightNumber>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.flnrs && res.data.flnrs.length > 0) {
              res.data.flnrs.forEach(element => {
                data.flnrs.push(AmsAlFlightNumber.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }


  async alFlightNumberGet(value: number): Promise<AmsAlFlightNumber> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_flnr_get',
      { flnrId: value },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsAlFlightNumberGet) => {
          const res = Object.assign(new ResponseAmsAlFlightNumberGet(), response);
          let rv: AmsAlFlightNumber;
          if (res && res.success) {
            if (res && res.data && res.data.flnr) {
              rv = AmsAlFlightNumber.fromJSON(res.data.flnr);
            }
          }
          //console.log('hubSave-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsAlFlightNumber>(source$);
  }

  async alFlightNumberSave(value: AmsAlFlightNumber): Promise<AmsAlFlightNumber> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_flnr_save',
      { flnr: value },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsAlFlightNumberGet) => {
          const res = Object.assign(new ResponseAmsAlFlightNumberGet(), response);
          let rv: AmsAlFlightNumber;
          if (res && res.success) {
            if (res && res.data && res.data.flnr) {
              rv = AmsAlFlightNumber.fromJSON(res.data.flnr);
            }
          }
          //console.log('hubSave-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsAlFlightNumber>(source$);
  }

  async alFlightNumberDelete(id: number): Promise<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_flnr_delete',
      { grpId: id },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseDelete) => {
          const res = Object.assign(new ResponseDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.id) {
              rv = res.data.id;
            }
          }
          console.log('alFlightNumberDelete-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<number>(source$);
  }

  //#endregion

  //#region AmsAlFlightNumberScedule

  async alFlightPlanPayloadTableP(criteria: AmsAlFlightNumberScheduleTableCriteria): Promise<AmsFlightPlanPayloadTableData> {
    const source$ = this.alFlightPlanPayloadTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  alFlightPlanPayloadTable(req: AmsFlightPlanPayloadTableCriteria): Observable<AmsFlightPlanPayloadTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_flp_payload_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
        grpId: req.grpId,
        wdId: req.wdId,
        flpnId: req.flpnId,
        depApId: req.depApId,
        arrApId: req.arrApId,
        flpId:req.flpId,
        acId: req.acId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsFlightPlanPayloadTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsFlightPlanPayloadTable(), resp);
          let data = new AmsFlightPlanPayloadTableData();
          data.flightplans = new Array<AmsFlightPlanPayload>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.flightplans && res.data.flightplans.length > 0) {
              res.data.flightplans.forEach(element => {
                data.flightplans.push(AmsFlightPlanPayload.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async alFlightPlanPayloadHistoryTableP(criteria: AmsAlFlightNumberScheduleTableCriteria): Promise<AmsFlightPlanPayloadTableData> {
    const source$ = this.alFlightPlanPayloadHistoryTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  alFlightPlanPayloadHistoryTable(req: AmsFlightPlanPayloadTableCriteria): Observable<AmsFlightPlanPayloadTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_flp_payload_h_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
        grpId: req.grpId,
        wdId: req.wdId,
        flpnId: req.flpnId,
        depApId: req.depApId,
        arrApId: req.arrApId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsFlightPlanPayloadTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsFlightPlanPayloadTable(), resp);
          let data = new AmsFlightPlanPayloadTableData();
          data.flightplans = new Array<AmsFlightPlanPayload>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.flightplans && res.data.flightplans.length > 0) {
              res.data.flightplans.forEach(element => {
                data.flightplans.push(AmsFlightPlanPayload.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

 //#endregion

  //#region AmsAlFlightNumberScedule

  async alFlightNumberScheduleTableP(criteria: AmsAlFlightNumberScheduleTableCriteria): Promise<ResponceAmsAlFlightNumberScheduleTableData> {
    const source$ = this.alFlightNumberScheduleTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  alFlightNumberScheduleTable(req: AmsAlFlightNumberScheduleTableCriteria): Observable<ResponceAmsAlFlightNumberScheduleTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_flnschedule_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
        flpnId: req.flpnId,
        grpId: req.grpId,
        wdId: req.wdId,
        depApId: req.depApId,
        arrApId: req.arrApId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsAlFlightNumberScheduleTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsAlFlightNumberScheduleTable(), resp);
          let data = new ResponceAmsAlFlightNumberScheduleTableData();
          data.flnschedules = new Array<AmsAlFlightNumberSchedule>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.flnschedules && res.data.flnschedules.length > 0) {
              res.data.flnschedules.forEach(element => {
                data.flnschedules.push(AmsAlFlightNumberSchedule.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async alFlightNumberScheduleGet(value: number): Promise<AmsAlFlightNumberSchedule> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_flnschedule_get',
      { flnrId: value },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsAlFlightNumberScheduleGet) => {
          const res = Object.assign(new ResponseAmsAlFlightNumberScheduleGet(), response);
          let rv: AmsAlFlightNumberSchedule;
          if (res && res.success) {
            if (res && res.data && res.data.flnschedule) {
              rv = AmsAlFlightNumberSchedule.fromJSON(res.data.flnschedule);
            }
          }
          //console.log('hubSave-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsAlFlightNumberSchedule>(source$);
  }

  async alFlightNumberScheduleSave(value: AmsAlFlightNumberSchedule): Promise<AmsAlFlightNumberSchedule> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_flnschedule_save',
      { flnschedule: value },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsAlFlightNumberScheduleGet) => {
          const res = Object.assign(new ResponseAmsAlFlightNumberScheduleGet(), response);
          let rv: AmsAlFlightNumberSchedule;
          if (res && res.success) {
            if (res && res.data && res.data.flnschedule) {
              rv = AmsAlFlightNumberSchedule.fromJSON(res.data.flnschedule);
            }
          }
          //console.log('hubSave-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsAlFlightNumberSchedule>(source$);
  }

  alFlightNumberScheduleCopyDay(req: AmsAlFlightNumberScheduleCopy): Observable<ResponceAmsAlFlightNumberScheduleCopyData> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_flnschedule_copy_day',
      {
        grpId: req.grpId,
        wdIdFrom: req.wdIdFrom,
        wdIdTo: req.wdIdTo,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsAlFlightNumberScheduleCopy) => {
          //console.log('alFlightNumberScheduleCopyDay -> resp=', resp);
          const res = Object.assign(new ResponceAmsAlFlightNumberScheduleTable(), resp);
          let data = new ResponceAmsAlFlightNumberScheduleCopyData();
          data.flnschedules = new Array<AmsAlFlightNumberSchedule>();
          if (res && res.data) {
            if (res.data.flnschedules && res.data.flnschedules.length > 0) {
              res.data.flnschedules.forEach(element => {
                data.flnschedules.push(AmsAlFlightNumberSchedule.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async alFlightNumberScheduleCopyDayP(criteria: AmsAlFlightNumberScheduleCopy): Promise<ResponceAmsAlFlightNumberScheduleCopyData> {
    const source$ = this.alFlightNumberScheduleCopyDay(criteria);
    return await lastValueFrom<any>(source$);
  }

  async alFlightNumberScheduleUpdateFromFlpnP(value: number): Promise<ResponseAmsAlFlightNumberScheduleUpdateFromFlp> {
    const source$ = this.alFlightNumberScheduleUpdateFromFlpn(value);
    return await lastValueFrom<any>(source$);
  }

  alFlightNumberScheduleUpdateFromFlpn(value: number): Observable<ResponseAmsAlFlightNumberScheduleUpdateFromFlp> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_flnschedule_update_from_flpn',
      {
        routeId: value,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsAlFlightNumberScheduleUpdateFromFlp) => {
          //console.log('alFlightNumberScheduleCopyDay -> resp=', resp);
          const res = Object.assign(new ResponceAmsAlFlightNumberScheduleTable(), resp);
          return res;
        }),
        catchError(this.handleError)
      );
  }

  async alFlightNumberScheduleDelete(id: number): Promise<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_flnschedule_delete',
      { flpnsId: id },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseDelete) => {
          const res = Object.assign(new ResponseDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.id) {
              rv = res.data.id;
            }
          }
          console.log('alFlightNumberDelete-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<number>(source$);
  }

  async alFlpStatusSave(value: AmsFlightPlanStatusSave): Promise<AmsAlFlightNumberSchedule> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_flp_status_save',
      { 
        flpId: value.flpId,
        flpStatusId: value.flpStatusId,
        payloadJobs: value.payloadJobs,
        qry_offset: 0,
        qry_limit: 10,
      },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponceAmsAlFlightNumberScheduleTable) => {
          const res = Object.assign(new ResponceAmsAlFlightNumberScheduleTable(), response);
          let data = new ResponceAmsAlFlightNumberScheduleTableData();
          data.flnschedules = new Array<AmsAlFlightNumberSchedule>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.flnschedules && res.data.flnschedules.length > 0) {
              res.data.flnschedules.forEach(element => {
                data.flnschedules.push(AmsAlFlightNumberSchedule.fromJSON(element));
              });
            }
          }

          return data.flnschedules && data.flnschedules.length>0? data.flnschedules[0]:undefined;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsAlFlightNumberSchedule>(source$);
  }

  async alFlpPayloadsP(criteria: AmsAlFlpPayloadsCriteria): Promise<AmsAlFlpPayloadsData> {
    const source$ = this.alFlpPayloads(criteria);
    return await lastValueFrom<any>(source$);
  }
  
  alFlpPayloads(req: AmsAlFlpPayloadsCriteria): Observable<AmsAlFlpPayloadsData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_flp_payloads',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        flpId: req.flpId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsAlFlpPayloads) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponseAmsAlFlpPayloads(), resp);
          let data = new AmsAlFlpPayloadsData();
          data.payloads = new Array<AmsFlpPayloads>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.payloads && res.data.payloads.length > 0) {
              res.data.payloads.forEach(element => {
                data.payloads.push(AmsFlpPayloads.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

  //#region AmsFlightPlanTransfer
  
  async getTripleTransferPpdP(criteria: AmsFlightPlanTransferTableCriteria): Promise<AmsFlightPlanTransferTableData> {
    const source$ = this.getTripleTransferPpd(criteria);
    return await lastValueFrom<any>(source$);
  }
  
  getTripleTransferPpd(req: AmsFlightPlanTransferTableCriteria): Observable<AmsFlightPlanTransferTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'flp_transfer_ppd_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_filter: req.filter,
        apId: req.apId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsFlightPlanTransferTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsFlightPlanTransferTable(), resp);
          let data = new AmsFlightPlanTransferTableData();
          data.routes = new Array<AmsFlightPlanTransfer>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.routes && res.data.routes.length > 0) {
              res.data.routes.forEach(element => {
                data.routes.push(AmsFlightPlanTransfer.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async getTripleTransferCpdP(criteria: AmsFlightPlanTransferTableCriteria): Promise<AmsFlightPlanTransferTableData> {
    const source$ = this.getTripleTransferCpd(criteria);
    return await lastValueFrom<any>(source$);
  }
  
  getTripleTransferCpd(req: AmsFlightPlanTransferTableCriteria): Observable<AmsFlightPlanTransferTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'flp_transfer_cpd_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_filter: req.filter,
        apId: req.apId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsFlightPlanTransferTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsFlightPlanTransferTable(), resp);
          let data = new AmsFlightPlanTransferTableData();
          data.routes = new Array<AmsFlightPlanTransfer>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.routes && res.data.routes.length > 0) {
              res.data.routes.forEach(element => {
                data.routes.push(AmsFlightPlanTransfer.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async getTripleTransferP(criteria: AmsFlightPlanTransferTableCriteria): Promise<AmsFlightPlanTransferTableData> {
    const source$ = this.getTripleTransfer(criteria);
    return await lastValueFrom<any>(source$);
  }
  
  getTripleTransfer(req: AmsFlightPlanTransferTableCriteria): Observable<AmsFlightPlanTransferTableData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'flp_transfer_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_filter: req.filter,
        apId: req.apId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsFlightPlanTransferTable) => {
          //console.log('payloadDemandSumByDayTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsFlightPlanTransferTable(), resp);
          let data = new AmsFlightPlanTransferTableData();
          data.routes = new Array<AmsFlightPlanTransfer>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.routes && res.data.routes.length > 0) {
              res.data.routes.forEach(element => {
                data.routes.push(AmsFlightPlanTransfer.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async populateTripleTransferP(criteria: AmsFlightPlanTransferTableCriteria): Promise<ResponseModel> {
    const source$ = this.populateTripleTransfer(criteria);
    return await lastValueFrom<any>(source$);
  }

  populateTripleTransfer(req: AmsFlightPlanTransferTableCriteria): Observable<ResponseModel> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post(this.sertviceUrl + 'flp_transfer_table_populate',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_filter: req.filter,
        apId: req.apId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseModel) => {
          //console.log('populateTripleTransfer -> resp=', resp);
          const res = Object.assign(new ResponseModel(), resp);
          return res;
        }),
        catchError(this.handleError)
      );
  }


  //#endregion

  //#region AmsAlSheduleRoute

  sheduleRouteTable(req: AmsSheduleRouteTableCriteria): Observable<ResponceAmsSheduleRouteTableData> {
    //console.log('sheduleRouteTable -> req=', req);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post(this.sertviceUrl + 'al_shedule_route_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
        alId: req.alId,
        apId: req.apId,
        depApId: req.depApId,
        arrApId: req.arrApId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponceAmsSheduleRouteTable) => {
          //console.log('sheduleRouteTable -> resp=', resp);
          const res = Object.assign(new ResponceAmsSheduleRouteTable(), resp);
          let data = new ResponceAmsSheduleRouteTableData();
          data.routes = new Array<AmsAlSheduleRoute>();
          data.rowsCount = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            if (res.data.routes && res.data.routes.length > 0) {
              res.data.routes.forEach(element => {
                data.routes.push(AmsAlSheduleRoute.fromJSON(element));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  async sheduleRouteSave(value: AmsAlSheduleRoute): Promise<AmsAlSheduleRoute> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_shedule_route_save',
      { route: value },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsSheduleRouteGet) => {
          const res = Object.assign(new ResponseAmsSheduleRouteGet(), response);
          let rv: AmsAlSheduleRoute;
          if (res && res.success) {
            if (res && res.data && res.data.route) {
              rv = AmsAlSheduleRoute.fromJSON(res.data.route);
            }
          }
          //console.log('hubSave-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsAlSheduleRoute>(source$);
  }

  async sheduleRouteDelete(id: number): Promise<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    const source$ = this.client.post(this.sertviceUrl + 'al_shedule_route_delete',
      { grpId: id },
      { headers: hdrs }).pipe(
        take(1),
        map((response: ResponseAmsSheduleRouteDelete) => {
          const res = Object.assign(new ResponseAmsSheduleRouteDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.routeId) {
              rv = res.data.routeId;
            }
          }
          //console.log('sheduleRouteDelete-> rv=', rv);
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<number>(source$);
  }

  //#endregion

}
