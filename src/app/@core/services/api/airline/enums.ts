import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "src/app/@core/models/common/enum-view.model";

export enum AmsHubTypes {
    Base = 1,
    Hub = 2,
}

export const AmsHubTypesOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Base',
        color: 'rgba(0, 0, 255, 0.7)'
    },
    {
        id: 2,
        name: 'Hub',
        color: 'rgba(102, 102, 255, 0.7)'
    }
];

@Pipe({ name: 'hubtype' })
export class AmsHubTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsHubTypes[value];
        const data: EnumViewModel = AmsHubTypesOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

export enum AmsTransactionType {
    AirlineCreate = 1,
    AircraftPurchase = 2,
    Boarding = 3,
    CargoLoad = 4,
    Disembarking = 5,
    CargoUnload = 6,
    InsuranceWeek = 7,
    LandingAndTaxyToGate = 8,
    Maintenance = 9,
    Refuling = 10,
    TaxAirportWeek = 11,
    TaxyAndTakeoff = 12,
    FlightCrew = 13,
    FlightIncome = 14,
    GovernmentBackedSupport = 15,
    HubUpkeepWeek=16,
    AMSAwardSheduledFlights=17
}


export const AmsTransactionTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Airline Create',
        color: 'rgba(60, 60, 255, 0.8)',
        group:'Airline'
    },
    {
        id: 2,
        name: 'Aircraft Purchase',
        color: 'rgba(255, 212, 128, 0.8)',
        group:'Aircraft'
    },
    {
        id: 3,
        name: 'Boarding',
        color: 'rgba(255, 180, 180, 0.8)',
        group:'Flight'
    },
    {
        id: 4,
        name: 'Cargo Load',
        color: 'rgba(255, 160, 160, 0.8)',
        group:'Flight'
    },
    {
        id: 5,
        name: 'Disembarking',
        color: 'rgba(255, 140, 140, 0.8)',
        group:'Flight'
    },
    {
        id: 6,
        name: 'Cargo Unload',
        color: 'rgba(255, 120, 120, 0.8)',
        group:'Flight'
    },
    {
        id: 7,
        name: 'Insurance Week',
        color: 'rgba(255, 195, 77, 0.8)',
        group:'Aircraft'
    },
    {
        id: 8,
        name: 'Landing and Taxy to Gate',
        color: 'rgba(255, 100, 100, 0.8)',
        group:'Flight'
    },
    {
        id: 9,
        name: 'Maintenance',
        color: 'rgba(255, 179, 26, 0.8)',
        group:'Aircraft'
    },
    {
        id: 10,
        name: 'Refuling',
        color: 'rgba(255, 80, 80, 0.8)',
        group:'Flight'
    },
    {
        id: 11,
        name: 'Tax Airport Week',
        color: 'rgba(230, 153, 0, 0.8)',
        group:'Aircraft'
    },
    {
        id: 12,
        name: 'Taxy and Takeoff',
        color: 'rgba(255, 60, 60, 0.8)',
        group:'Flight'
    },
    {
        id: 13,
        name: 'Flight Crew',
        color: 'rgba(255, 40, 40, 0.8)',
        group:'Flight'
    },
    {
        id: 14,
        name: 'Flight Income',
        color: 'rgba(51, 204, 51, 0.8)',
        group:'Flight'
    },
    {
        id: 15,
        name: 'Government-Backed Support and Finance for Business',
        color: 'rgba(140, 140, 255, 0.8)',
        group:'Airline'
    },
    {
        id: 16,
        name: 'Hub Upkeep Weekly',
        color: 'rgba(100, 100, 255, 0.8)',
        group:'Airline'
    },
    {
        id: 17,
        name: 'AMS Award - 1000 sheduled flights',
        color: 'rgba(140, 140, 255, 0.8)',
        group:'Airline'
    },

];

@Pipe({ name: 'transactiontype' })
export class AmsTransactionTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsTransactionType[value];
        const data: EnumViewModel = AmsTransactionTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

export class AmsWeekday{
    id:number;
    name:string;
    abbr:string;
}

export enum AmsWeekdays {
    Monday = 2,
    Tuesday = 3,
    Wednesday = 4,
    Thursday = 5,
    Friday = 6,
    Saturday = 7,
    Sunday = 1,
}

export const AmsWeekdaysOpt: AmsWeekday[] = [
    {
        id: 2,
        name: 'Monday',
        abbr: 'Mon'
    },
    {
        id: 3,
        name: 'Tuesday',
        abbr: 'Tue'
    },
    {
        id: 4,
        name: 'Wednesday',
        abbr: 'Wed'
    },
    {
        id: 5,
        name: 'Thursday',
        abbr: 'Thu'
    },
    {
        id: 6,
        name: 'Friday',
        abbr: 'Fri'
    },
    {
        id: 7,
        name: 'Saturday',
        abbr: 'Sat'
    },
    {
        id: 1,
        name: 'Sunday',
        abbr: 'Sun'
    },
];

@Pipe({ name: 'weekdayabbr' })
export class AmsWeekdaysAbbrDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsWeekdays[value];
        const data: AmsWeekday = AmsWeekdaysOpt.find(x => x.id === value);
        if (data) {
            rv = data.abbr;
        }
        return rv;
    }
}

@Pipe({ name: 'weekdayname' })
export class AmsWeekdaysDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsWeekdays[value];
        const data: AmsWeekday = AmsWeekdaysOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

export enum FlightPlanStatus{
    AvailableForTicketSell = 1,
    FlightIsSoldOut = 2,
    AddingToFlightQueu = 4,
    CancelFlight=10
}

export const FlightPlanStatusOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Available for ticket sell',
        color: 'rgba(0, 0, 255, 0.7)'
    },
    {
        id: 2,
        name: 'Flight is sold out',
        color: 'rgba(102, 102, 255, 0.7)'
    },
    {
        id: 4,
        name: 'Adding to flight queu',
        color: 'rgba(255, 204, 204, 0.7)'
    },
    {
        id: 10,
        name: 'Cancel Flight',
        color: 'rgba(255, 153, 153, 0.7)'
    },
];

@Pipe({ name: 'flpstatus' })
export class AmsFlpStatusDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = FlightPlanStatus[value];
        const data: EnumViewModel = FlightPlanStatusOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}
