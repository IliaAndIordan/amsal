import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { URL_COMMON_IMAGE_AIRLINE } from "src/app/@core/const/app-storage.const";
import { AcType } from "src/app/@core/models/pipes/ac-type.pipe";
import { EventApi, EventInput } from "@fullcalendar/core";
import { AmsPayloadType } from "../flight/enums";
import { AmsWeekday, AmsWeekdays, AmsWeekdaysOpt } from "./enums";
import { filter } from "rxjs";

//#region AmsAlFlightNumberSchedule

export class AmsWeeklyFlightSchedule {
    wd: AmsWeekday;
    flpns: AmsAlFlightNumberSchedule[]
}

export class AmsWeeklyFlights {
    flpnId: number;
    alId:number;
    flpnNumber: number;
    flpnName: string;
    depApId: number;
    arrApId: number;
    distanceKm: number;
    flightH: number;
    priceE: number;
    priceB: number;
    priceF: number;
    priceCpKg: number;

    weekdays: AmsWeeklyFlightSchedule[];

    public static fromAmsAlFlightNumberSchedule(data: AmsAlFlightNumberSchedule[]): AmsWeeklyFlights[] {
        const rv = new Array<AmsWeeklyFlights>(); // Object.create(AmsWeeklyFlights.prototype);
        if (data && data.length > 0) {
            const uniqueFlpnIds = [...new Set(data.map(item => item.flpnId))];
            //console.log('fromAmsAlFlightNumberSchedule -> uniqueFlpnIds=', uniqueFlpnIds);
            if (uniqueFlpnIds && uniqueFlpnIds.length > 0) {
                uniqueFlpnIds.forEach((flpnId: number) => {
                    const flpms = data.filter(x => x.flpnId === flpnId);
                    if (flpms && flpms.length > 0) {
                        const json = flpms[0];
                        let vs = Object.create(AmsWeeklyFlights.prototype);

                        vs.flpnId = json && json.flpnId ? json.flpnId : undefined;
                        vs.alId = json && json.alId ? json.alId : undefined;
                        vs.flpnNumber = json && json.flpnNumber ? json.flpnNumber : undefined;
                        vs.flpnName = json && json.flpnName ? json.flpnName : undefined;
                        vs.alId = json && json.alId ? json.alId : undefined;
                        vs.depApId = json && json.depApId ? json.depApId : undefined;
                        vs.arrApId = json && json.arrApId ? json.arrApId : undefined;

                        vs.distanceKm = json && json.distanceKm ? json.distanceKm : undefined
                        vs.flightH = json && json.flightH ? json.flightH : undefined;
                        vs.priceE = json && json.priceE ? json.priceE : undefined;
                        vs.priceB = json && json.priceB ? json.priceB : undefined;
                        vs.priceF = json && json.priceF ? json.priceF : undefined;
                        vs.priceCpKg = json && json.priceCpKg ? json.priceCpKg : undefined;

                        vs.weekdays = new Array<AmsWeeklyFlightSchedule>();
                        AmsWeekdaysOpt.forEach((wd: AmsWeekday) => {
                            let weekday = new AmsWeeklyFlightSchedule();
                            weekday.wd = wd;
                            weekday.flpns = flpms.filter(x => x.wdId === wd.id);
                            vs.weekdays.push(weekday);
                        });
                        rv.push(vs);
                    }
                });
            }
        }
        return rv;
    }
}



export class AmsAlFlightNumberSchedule {
    flpnsId: number;
    flpnId: number;
    flpnNumber: number;
    flpnName: string;
    flpnsName: string;
    grpId: number;
    acId?:number;
    alId: number;
    wdId: number;
    wdAbbr: string;
    dtimeH: number;
    dtimeMin: number;
    atimeH: number;
    atimeMin: number;
    flightH: number;
    priceE: number;
    priceB: number;
    priceF: number;
    priceCpKg: number;
    adate: Date;
    udate: Date;
    depApId: number;
    arrApId: number;
    distanceKm: number;
    minRunwayM: number;
    minMtowKg: number;
    isActive: boolean;
    grpStDate: Date;
    grpIsActive: boolean;

    get depDate(): Date {
        const date = new Date();
        const currentDay = date.getDay();
        const distance = (this.wdId - 1) - currentDay;
        const dDate = new Date(new Date(date).setDate(date.getDate() + distance));

        const dep = new Date(dDate.getFullYear(), dDate.getMonth(), dDate.getDate(), this.dtimeH, this.dtimeMin, 0);
        return dep;
    }

    get arrDate(): Date {
        const dep = this.depDate;

        const rv = new Date(dep.getTime() + ((this.flightH * 60)) * 60000);
        const arr = new Date(rv.getFullYear(), rv.getMonth(), rv.getDate(), this.atimeH, this.atimeMin, 0);
        //console.log('arrDate -> arr: ', arr.toString());
        return arr;
    }

    nextFlightDepDate(opTimeMin: number = 5): Date {
        const arr = this.arrDate;
        const rv = new Date(arr.getTime() + (2 * opTimeMin) * 60000);
        return rv;
    }

    toEvent(): EventInput {
        let rv = {
            id: this.flpnsId?.toString(),
            groupId: this.grpId.toString(),
            start: this.depDate,
            end: this.arrDate,
            title: this.flpnsName,
            dow: (this.wdId - 1),
            allDay: false,
            display: 'block',

            flight: this,
        }
        return rv;
    }

    toEventBkg(opTimeMin: number = 5): EventInput {
        const dep = this.depDate;
        const ndep = new Date(dep.getTime() - (1 * opTimeMin) * 60000);
        const arr = this.arrDate;
        const narr = new Date(arr.getTime() + (1 * opTimeMin) * 60000);
        const next = this.nextFlightDepDate(opTimeMin);
        let rv: EventInput = {
            id: `${this.flpnsId?.toString()}-b`,
            groupId: this.grpId.toString(),
            start: ndep,
            end: narr,
            title: this.flpnsName,
            dow: (this.wdId - 1),
            allDay: false,
            display: 'background',
            nextflight: next,
        }
        return rv;
    }



    public static fromJSON(json: any): AmsAlFlightNumberSchedule {
        const vs = Object.create(AmsAlFlightNumberSchedule.prototype);
        return Object.assign(vs, json, {
            flpnsId: json && json.flpnsId ? json.flpnsId : undefined,
            flpnId: json && json.flpnId ? json.flpnId : undefined,
            flpnNumber: json && json.flpnNumber ? json.flpnNumber : undefined,
            alId: json && json.alId ? json.alId : undefined,
            depApId: json && json.depApId ? json.depApId : undefined,
            arrApId: json && json.arrApId ? json.arrApId : undefined,
            destId: json && json.destId ? json.destId : undefined,
            routeId: json && json.routeId ? json.routeId : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            isActive: json && json.isActive ? json.isActive > 0 ? true : false : false,
            grpStDate: (json && json.grpStDate) ? new Date(json.grpStDate) : undefined,
            boolean: json && json.boolean ? json.boolean > 0 ? true : false : false,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsAlFlightNumberSchedule.fromJSON(value) : value;
    }

}

export interface IAmsAlFlightNumberSchedule {
    flpnsId: number;
    flpnId: number;
    flpnNumber: number;
    flpnName: string;
    flpnsName: string;
    grpId: number;
    acId?:number;
    alId: number;
    wdId: number;
    wdAbbr: string;
    dtimeH: number;
    dtimeMin: number;
    atimeH: number;
    atimeMin: number;
    flightH: number;
    priceE: number;
    priceB: number;
    priceF: number;
    priceCpKg: number;
    adate: Date;
    udate: Date;
    depApId: number;
    arrApId: number;
    distanceKm: number;
    minRunwayM: number;
    minMtowKg: number;
    isActive: number;
    grpStDate: string;
    grpIsActive: number;
}


export class ResponseAmsAlFlightNumberScheduleGetData {
    flnschedule: IAmsAlFlightNumberSchedule;
}

export class ResponseAmsAlFlightNumberScheduleGet extends ResponseModel {
    data: ResponseAmsAlFlightNumberScheduleGetData;
}

export class ResponseAmsAlFlightNumberScheduleUpdateFromFlpData {
    affectedRows: number;
}
export class ResponseAmsAlFlightNumberScheduleUpdateFromFlp extends ResponseModel {
    data: ResponseAmsAlFlightNumberScheduleUpdateFromFlpData;
}





export class AmsAlFlightNumberScheduleTableCriteria extends TableCriteriaBase {
    alId?: number;
    grpId?: number;
    wdId?: number;
    flpnId?: number;
    depApId?: number;
    arrApId?: number;
    flpId?: number;
}

export class AmsAlFlightNumberScheduleCopy {
    grpId?: number;
    wdIdFrom?: number;
    wdIdTo?: number;
}


export class ResponceAmsAlFlightNumberScheduleCopyData {
    flnschedules: AmsAlFlightNumberSchedule[];
}

export class ResponceAmsAlFlightNumberScheduleCopy extends ResponseModel {
    data: ResponceAmsAlFlightNumberScheduleCopyData;
}

export class ResponceAmsAlFlightNumberScheduleTableData {
    flnschedules: AmsAlFlightNumberSchedule[];
    rowsCount: number;
}

export class ResponceAmsAlFlightNumberScheduleTable extends ResponseModel {
    data: ResponceAmsAlFlightNumberScheduleTableData;
}

export class AmsFlpPayloads {
    flpplId: number;
    flpId: number;
    pcpdId: number;
    pax: number;
    payloadKg: number;
    payloadType: number;
    ppdTypeId: number;
    description: string;
    price: number;
    adate: Date;

    public static fromJSON(json: any): AmsFlpPayloads {
        const vs = Object.create(AmsFlpPayloads.prototype);
        return Object.assign(vs, json, {
            flpplId: json && json.flpplId ? json.flpplId : undefined,
            flpId: json && json.flpId ? json.flpId : undefined,
            pcpdId: json && json.pcpdId ? json.pcpdId : undefined,
            pax: json && json.pax ? json.pax : undefined,
            payloadKg: json && json.payloadKg ? json.payloadKg : undefined,
            payloadType: json.payloadType ? (json.payloadType as AmsPayloadType) : undefined,
            ppdTypeId: json.ppdTypeId ? json.ppdTypeId : undefined,
            description: json.description ? json.description : undefined,
            price: json.price ? json.price : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsFlpPayloads.fromJSON(value) : value;
    }
}

export interface IAmsFlpPayloads {
    flpplId: number;
    flpId: number;
    pcpdId: number;
    pax: number;
    payloadKg: number;
    payloadType: number;
    ppdTypeId: number;
    description: string;
    price: number;
    adate: string;
}

export class AmsAlFlpPayloadsData {
    payloads: AmsFlpPayloads[];
    rowsCount: number;
}

export class ResponseAmsAlFlpPayloads extends ResponseModel {
    data: AmsAlFlpPayloadsData;
}

export class AmsAlFlpPayloadsCriteria extends TableCriteriaBase {
    flpId: number;
}

//#endregion

//#region AmsAlFlightNumber

export class AmsAlFlightNumber {
    flpnId: number;
    flpnNumber: number;
    flpnName: string;
    alId: number;
    depApId: number;
    arrApId: number;
    destId: number;
    routeId: number;
    flightH: number;
    priceE: number;
    priceB: number;
    priceF: number;
    priceCpKg: number;
    adate: Date;
    udate: Date;
    flpnSchedulesCount: number;
    maxPriceE: number;
    maxPriceB: number;
    maxPriceF: number;
    maxPriceCPerKg: number;

    distanceKm: number;
    minRunwayM: number;
    minMtow: number;

    get name(): string {
        let rv: string;
        if (this?.flpnNumber) {
            rv = String((this?.flpnNumber)).padStart(4, "0");
        }
        return rv;
    }


    public static fromJSON(json: any): AmsAlFlightNumber {
        const vs = Object.create(AmsAlFlightNumber.prototype);
        return Object.assign(vs, json, {
            flpnId: json && json.flpnId ? json.flpnId : undefined,
            flpnNumber: json && json.flpnNumber ? json.flpnNumber : undefined,
            alId: json && json.alId ? json.alId : undefined,
            depApId: json && json.depApId ? json.depApId : undefined,
            arrApId: json && json.arrApId ? json.arrApId : undefined,
            destId: json && json.destId ? json.destId : undefined,
            routeId: json && json.routeId ? json.routeId : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsAlFlightNumber.fromJSON(value) : value;
    }

}

export interface IAmsAlFlightNumber {
    flpnId: number;
    flpnNumber: number;
    flpnName: string;
    alId: number;
    depApId: number;
    arrApId: number;
    destId: number;
    routeId: number;
    flightH: number;
    priceE: number;
    priceB: number;
    priceF: number;
    priceCpKg: number;
    adate: Date;
    udate: Date;
    flpnSchedulesCount: number;
    maxPriceE: number;
    maxPriceB: number;
    maxPriceF: number;
    maxPriceCPerKg: number;
    distanceKm: number;
    minRunwayM: number;
    minMtow: number;
}


export class ResponseAmsAlFlightNumberGetData {
    flnr: IAmsAlFlightNumber;
}

export class ResponseAmsAlFlightNumberGet extends ResponseModel {
    data: ResponseAmsAlFlightNumberGetData;
}

export class AmsAlFlightNumberTableCriteria extends TableCriteriaBase {
    alId?: number;
    depApId?: number;
    arrApId?: number;
}

export class ResponceAmsAlFlightNumberTableData {
    flnrs: AmsAlFlightNumber[];
    rowsCount: number;
}

export class ResponceAmsAlFlightNumberTable extends ResponseModel {
    data: ResponceAmsAlFlightNumberTableData;
}


//#endregion

//#region AmsDestination

export class AmsDestination {
    destId: number;
    destName: string;
    depApId: number;
    arrApId: number;
    apTypeFactor: number;
    destTypeId: number;
    flightH: number;
    distanceKm: number;
    minRwLenghtM: number;
    minMtowKg: number;

    maxPriceE: number;
    maxPriceB: number;
    maxPriceF: number;

    priceE: number;
    priceB: number;
    priceF: number;
    priceCpKg: number;


    public static fromJSON(json: any): AmsDestination {
        const vs = Object.create(AmsDestination.prototype);
        return Object.assign(vs, json, {
            destId: json && json.destId ? json.destId : undefined,
            destName: json && json.destName ? json.destName : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsDestination.fromJSON(value) : value;
    }

}

export interface IAmsDestination {
    destId: number;
    destName: string;
    depApId: number;
    arrApId: number;
    apTypeFactor: number;
    destTypeId: number;
    flightH: number;
    distanceKm: number;
    minRwLenghtM: number;
    minMtowKg: number;

    maxPriceE: number;
    maxPriceB: number;
    maxPriceF: number;

    priceE: number;
    priceB: number;
    priceF: number;
    priceCpKg: number;
}


export class ResponseAmsDestinationGetData {
    dest: IAmsDestination;
}

export class ResponseAmsDestinationGet extends ResponseModel {
    data: ResponseAmsDestinationGetData;
}

//#endregion