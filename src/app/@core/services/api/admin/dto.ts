import { URL_COMMON_IMAGE_FLAGS } from "src/app/@core/const/app-storage.const";
import { EnumViewModel } from "src/app/@core/models/common/enum-view.model";
import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { AmsTaskType, AmsTaskTypeNameOpt, AmsTaskTypeOpt } from "./enums";

//#region AmsTask


export class AmsTask {
    taskId: number;
    taskType: string;
    taskTypeId:AmsTaskType;
    taskName: string;
    spName: string;
    periodMin: number;
    startTime: number;
    endTime: number;
    isRunning:boolean;
    execTimeMs:number;
    nextRun:Date;
    info:string;



    public static fromJSON(json: IAmsTask): AmsTask {
        const vs = Object.create(AmsTask.prototype);
        return Object.assign(vs, json, {
            taskId: json.taskId ? json.taskId : undefined,
            taskTypeId:(json && json.taskType) ? AmsTaskTypeOpt.find(x => x.name === json.taskType )?.id:AmsTaskType.minFive,
            startTime: (json && json.startTime) ? new Date(json.startTime) : undefined,
            isRunning: json.isRunning ? true : false,
            endTime: (json && json.endTime) ? new Date(json.endTime) : undefined,
            nextRun: (json && json.nextRun) ? new Date(json.nextRun) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {
        return key === '' ? AmsTask.fromJSON(value) : value;
    }

}

export interface IAmsTask {
    taskId: number;
    taskType: string;
    taskName: string;
    spName: string;
    periodMin: number;
    startTime: string;
    endTime: string;
    isRunning:boolean;
    execTimeMs:number;
    nextRun:Date;
    info:string;
}



export class AmsTaskData {
    task: IAmsTask;
}

export class ResponseAmsTaskData extends ResponseModel {
    data: AmsTaskData;
}

export class AmsTaskTableCriteria extends TableCriteriaBase {
    taskType: string;
}
export class RowsCountModel{
    totalRows: number;
}
export class AmsTaskTableData {
    tasks: AmsTask[];
    rowsCount: number;
}

export class IAmsTaskTableData {
    tasks: IAmsTask[];
    rowsCount: number;
}

export class ResponseAmsTaskTable extends ResponseModel {
    data: IAmsTaskTableData;
}

//#endregion
