import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { firstValueFrom, lastValueFrom, Observable, of, Subscription } from 'rxjs';
import { catchError, map, take, tap } from 'rxjs/operators';
// Services
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { environment } from 'src/environments/environment';
import { AmsTask, AmsTaskTableCriteria, AmsTaskTableData, IAmsTask, ResponseAmsTaskTable } from './dto';


// Models

@Injectable({ providedIn: 'root' })
export class AmsTaskClient extends ApiBaseClient {

  constructor(
    private client: HttpClient,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return environment.services.url.country;
  }

  //#region AmsTask

  async getTaskTable(req: AmsTaskTableCriteria): Promise<AmsTaskTableData> {

    //console.log('cityTableReq -> req:', req);
    const url = this.sertviceUrl + 'admin_task_table';
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //

    const source$ = this.client.post(url,
      {
        taskType: req.taskType,
        qry_offset: req.offset,
        qry_limit: req.limit,
        qry_orderCol: req.sortCol,
        qry_isDesc: req.sortDesc,
        qry_filter: req.filter,
      },
      { headers: hdrs })
      .pipe(
        take(1),
        map((response: ResponseAmsTaskTable) => {
          //console.log('cityTableReq -> response:', response);
          const resp = Object.assign(new ResponseAmsTaskTable(), response);
          let res = new AmsTaskTableData();
          res.tasks = new Array<AmsTask>();

          if (resp && resp.success) {
            res.rowsCount = resp.data.rowsCount;
            for (const dtoJ of resp.data.tasks) {
              let dto = AmsTask.fromJSON(dtoJ);
              res.tasks.push(dto);
            }
          }

          return res;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<AmsTaskTableData>(source$);

  }


  //#endregion


}

