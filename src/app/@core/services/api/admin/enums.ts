import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "src/app/@core/models/common/enum-view.model";

export enum AmsTaskType{
    MinOne = 1,
    minFive = 2
}
export const AmsTaskTypeOpt:EnumViewModel[] = [
    {id:1, name:'sp_ev_min_one'},
    {id:2, name:'sp_ev_min_five'}
];
export const AmsTaskTypeNameOpt:EnumViewModel[] = [
    {id:1, name:'One Min'},
    {id:2, name:'Five Min'}
];


@Pipe({ name: 'tasktype' })
export class AmsTaskTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = '';
        const data: EnumViewModel = AmsTaskTypeOpt.find(x => x.name === value);
        if (data) {
            rv = AmsTaskTypeNameOpt.find(x => x.id === data.id).name;
        }
        return rv;
    }
}
