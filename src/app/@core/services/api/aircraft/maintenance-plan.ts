//#region Cabin

import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { RowsCountModel } from "../country/dto";
import { AmsMaintenanceTypes } from "./enums";

export class AmsMaintenancePlan {
    acmpId: number;
    acId: number;

    amtId: AmsMaintenanceTypes;
    amtName:string;
    mtnTimeH:number;
    description: string;
    apId?: number;
    isHomeAp: boolean;
    price?: number;
    alId:number;
    startTime: Date;
    endTime?: Date;
    trId?: number;
    processed: boolean;
    adate: string;
    logAdate?: string;
    logDurationMin?: number;


    public static fromJSON(json: IAmsMaintenancePlan): AmsMaintenancePlan {
        const vs = Object.create(AmsMaintenancePlan.prototype);
        return Object.assign(vs, json, {
            amtId: json.amtId ? json.amtId as AmsMaintenanceTypes : undefined,
            startTime: (json && json.startTime) ? new Date(json.startTime) : undefined,
            endTime: (json && json.endTime) ? new Date(json.endTime) : undefined,
            logAdate: (json && json.logAdate) ? new Date(json.logAdate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsMaintenancePlan.fromJSON(value) : value;
    }

}

export interface IAmsMaintenancePlan {
    acmpId: number;
    acId: number;

    amtId: number;
    amtName:string;
    mtnTimeH:number;
    description: string;
    apId?: number;
    isHomeAp: boolean;
    price?: number;
    alId:number;
    startTime: string;
    endTime?: string;
    trId?: number;
    processed: boolean;
    adate: string;
    logAdate?: string;
    logDurationMin?: number;
}

export class AmsMaintenancePlanTableCriteria extends TableCriteriaBase {
    alId?: number;
    acId?: number;
    apId?: number;
}

export class AmsMaintenancePlanTableData {
    cabins: IAmsMaintenancePlan[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsMaintenancePlanTableData extends ResponseModel {
    data: AmsMaintenancePlanTableData;
}


export class AmsMaintenancePlanCreateData {
    acmpId?: number;
    acId: number;
    amtId: number;
    stTimeMs?: number;
}


export class AmsMaintenancePlanSaveData {
    maintenance: IAmsMaintenancePlan;
}

export class ResponseAmsMaintenancePlanCreateData extends ResponseModel {
    data: AmsMaintenancePlanSaveData;
}


export class AmsMaintenancePlanDeleteData {
    acmpId: number;
}

export class ResponseAmsMaintenancePlanDelete extends ResponseModel {
    data: AmsMaintenancePlanDeleteData;
}


//#endregion