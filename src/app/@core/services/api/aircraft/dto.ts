import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AIRCRAFT_ICON, URL_COMMON_IMAGE_AIRLINE, URL_COMMON_IMAGE_FLAGS, URL_NO_IMG } from "src/app/@core/const/app-storage.const";
import { EnumViewModel } from "src/app/@core/models/common/enum-view.model";
import { ResponseModel } from "src/app/@core/models/common/responce.model";
import { TableCriteriaBase } from "src/app/@core/models/common/table-criteria-base.model";
import { AircraftActionType, AircraftStatus } from "src/app/@core/models/pipes/aircraft-statis.pipe";
import { AmsStatus } from "src/app/@core/models/pipes/ams-status.enums";
import { RowsCountModel } from "../country/dto";
import { AmsFlightType } from "../flight/enums";
import { AmsMaintenanceType, AmsMaintenanceTypes, AmsMaintenanceTypesOpt } from "./enums";
import { chartColors } from "src/app/@share/charts/ams-chart-line/ams-chart-line.component";
import { TsDatePipe } from "src/app/@core/models/pipes/ts-date.pipe";

//#region Manufacturer

export class AmsManufacturer {
    mfrId: number;
    apId: number;
    iata: string;
    mfrName: string;
    headquartier: string;
    notes: string;
    logoImg: string;
    wikiUrl: string;
    homeUrl: string;
    amsStatus: AmsStatus;
    udate: Date;

    logoUrl: string;


    public static fromJSON(json: IAmsManufacturer): AmsManufacturer {
        const vs = Object.create(AmsManufacturer.prototype);
        return Object.assign(vs, json, {
            amsStatus: json.amsStatus ? json.amsStatus as AmsStatus : AmsStatus.DataCheckRequired,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            logoUrl: json.logoImg ? URL_COMMON_IMAGE_AIRCRAFT + json.logoImg : URL_NO_IMG,
            imgAirUrl: URL_COMMON_IMAGE_AIRCRAFT + (json.mfrId ? 'mfr_' + json.mfrId + '_headquater.png?op=' + new Date().getTime() : 'mfr_0_headquater.png'),
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsManufacturer.fromJSON(value) : value;
    }

}

export interface IAmsManufacturer {
    mfrId: number;
    apId: number;
    iata: string;
    mfrName: string;
    headquartier: string;
    notes: string;
    logoImg: string;
    wikiUrl: string;
    homeUrl: string;
    amsStatus: number;
    udate: string;
}

export class AmsManufacturerTableCriteria extends TableCriteriaBase {
    apId?: number;
    amsStatus?: number;
}

export class AmsManufacturerTableData {
    manufacturers: IAmsManufacturer[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsManufacturerTableData extends ResponseModel {
    data: AmsManufacturerTableData;
}


export class ManufacturerSaveData {
    manufacturer: IAmsManufacturer;
}

export class ResponsemanufacturerSave extends ResponseModel {
    data: ManufacturerSaveData;
}

export class ManufacturerDeleteData {
    mfrId: number;
}

export class ResponseManufacturerDelete extends ResponseModel {
    data: ManufacturerDeleteData;
}

//#endregion

//#region Mac

export class AmsMac {
    macId: number;
    mfrId: number;
    apId: number;
    apName: string;
    mfrName:string;
    acTypeId: number;
    model: string;
    price: number;
    pilots: number;
    cruiseSpeedKmph: number;
    cruiseAltitudeM: number;
    takeOffM: number;
    maxTakeOffKg: number;
    landingM: number;
    maxLandingKg: number;
    maxRangeKm: number;
    emptyWeightKg: number;
    fuelVolL: number;
    loadKg: number;
    cargoKg: number;
    wikiUrl: string;
    amsStatus: AmsStatus;
    notes: string;
    fuelConsumptionLp100km: number;
    costPerFh: number;
    operationTimeMin: number;
    popularity: number;
    maxSeating: number;
    asmiRate: number;
    powerplant: string;
    productionStart: Date;
    productionRateH: number;
    lastProducedOn: Date;
    numberBuild: number;
    udate: Date;

    imgTableUrl: string;
    imgAirUrl: string;
    onProduction: boolean;

    acMarketCount: number;
    // for market
    aircraftCount: number;

    public static fromJSON(json: IAmsMac): AmsMac {
        const vs = Object.create(AmsMac.prototype);
        return Object.assign(vs, json, {
            amsStatus: json.amsStatus ? json.amsStatus as AmsStatus : AmsStatus.DataCheckRequired,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            productionStart: (json && json.productionStart) ? new Date(json.productionStart) : undefined,
            lastProducedOn: (json && json.lastProducedOn) ? new Date(json.lastProducedOn) : new Date(),
            imageUrl: URL_COMMON_IMAGE_AIRCRAFT + (json.macId ? 'mac_' + json.macId + '_table.png?op=' + new Date().getTime() : 'aircraft_0_tableimg.png'),
            imgTableUrl: URL_COMMON_IMAGE_AIRCRAFT + (json.macId ? 'mac_' + json.macId + '_table.png?op=' + new Date().getTime() : 'aircraft_0_tableimg.png'),
            imgAirUrl: URL_COMMON_IMAGE_AIRCRAFT + (json.macId ? 'mac_' + json.macId + '_air.png?op=' + new Date().getTime() : 'aircraft_0_tableimg.png'),
            onProduction: (json.amsStatus && json.amsStatus === AmsStatus.Active && json.apId && json.productionRateH > 1),
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsMac.fromJSON(value) : value;
    }

    public get nextProducedOn(): Date {
        let producedOn = new Date();
        const productionRateMs = this.productionRateH ? this.productionRateH * 60 * 60 * 1000 : 0;
        if (this.lastProducedOn) {
            producedOn.setTime(new Date(this.lastProducedOn).getTime() + productionRateMs);
        } else {
            producedOn.setTime(producedOn.getTime() + productionRateMs);
        }
        return producedOn;
    }


}

export interface IAmsMac {
    macId: number;
    mfrId: number;
    apId: number;
    apName: string;
    mfrName:string;
    acTypeId: number;
    model: string;
    price: number;
    pilots: number;
    cruiseSpeedKmph: number;
    cruiseAltitudeM: number;
    takeOffM: number;
    maxTakeOffKg: number;
    landingM: number;
    maxLandingKg: number;
    maxRangeKm: number;
    emptyWeightKg: number;
    fuelVolL: number;
    loadKg: number;
    cargoKg: number;
    wikiUrl: string;
    amsStatus: number;
    notes: string;
    fuelConsumptionLp100km: number;
    costPerFh: number;
    operationTimeMin: number;
    popularity: number;
    maxSeating: number;
    asmiRate: number;
    powerplant: string;
    productionStart: string;
    productionRateH: number;
    lastProducedOn: string;
    numberBuild: number;
    udate: string;

    aircraftCount: number;
}

export class AmsMacTableCriteria extends TableCriteriaBase {
    mfrId?: number;
    amsStatus?: number;
}

export class AmsMacTableData {
    macs: IAmsMac[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsMacTableData extends ResponseModel {
    data: AmsMacTableData;
}


export class AmsMacSaveData {
    mac: IAmsMac;
}

export class ResponseAmsMacSave extends ResponseModel {
    data: AmsMacSaveData;
}

export class AmsMacDeleteData {
    macId: number;
}

export class ResponseAmsMacDelete extends ResponseModel {
    data: AmsMacDeleteData;
}

//#endregion

//#region Cabin

export class AmsMacCabin {
    cabinId: number;
    macId: number;

    cName: string;
    seatE: number;
    seatEtypeId?: number;
    seatB: number;
    seatBtypeId?: number;
    seatF: number;
    seatFtypeId?: number;
    seatCrew: number;
    confort: number;
    cargoKg:number;
    price: number;
    maxIFEId: number;
    isDefault: boolean;

    imgTableUrl: string;
    imgAirUrl: string;
    acCount: number;
    canDelete: boolean;


    public static fromJSON(json: IAmsMacCabin): AmsMacCabin {
        const vs = Object.create(AmsMacCabin.prototype);
        return Object.assign(vs, json, {
            seatE: json.seatE ? json.seatE : 0,
            seatEtypeId: json.seatEtypeId ? json.seatEtypeId as AmsStatus : undefined,
            seatB: json.seatB ? json.seatB : 0,
            seatBtypeId: json.seatBtypeId ? json.seatBtypeId as AmsStatus : undefined,
            seatF: json.seatF ? json.seatF : 0,
            seatFtypeId: json.seatFtypeId ? json.seatFtypeId as AmsStatus : undefined,
            seatCrew: json.seatCrew ? json.seatCrew : 0,
            isDefault: json.isDefault && json.isDefault === 1 ? true : false,
            canDelete: json && json.acCount === 0 ? true : false,
            imgTableUrl: URL_COMMON_IMAGE_AIRCRAFT + (json.cabinId ? 'cabin_' + json.cabinId + '_table.png' : 'aircraft_0_tableimg.png'),
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsMac.fromJSON(value) : value;
    }

}

export interface IAmsMacCabin {
    cabinId: number;
    macId: number;

    cName: string;
    seatE: number;
    seatEtypeId: number;
    seatB: number;
    seatBtypeId: number;
    seatF: number;
    seatFtypeId: number;
    seatCrew: number;
    confort: number;
    cargoKg:number;
    price: number;
    maxIFEId: number;
    isDefault: number;
    acCount: number;
}

export class AmsMacCabinTableCriteria extends TableCriteriaBase {
    macId?: number;
}

export class AmsMacCabinTableData {
    cabins: IAmsMacCabin[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsMacCabinTableData extends ResponseModel {
    data: AmsMacCabinTableData;
}


export class AmsMacCabinSaveData {
    cabin: IAmsMacCabin;
}

export class ResponseAmsMacCabinSave extends ResponseModel {
    data: AmsMacCabinSaveData;
}

export class AmsMacCabinDeleteData {
    cabinId: number;
}

export class ResponseAmsMacCabinDelete extends ResponseModel {
    data: AmsMacCabinDeleteData;
}

export class AmsJsonData {
    jsondata: any[];
}

export class ResponseAmsJsonData extends ResponseModel {
    data: AmsJsonData;
}

//#endregion


//#region Aircraft

export class AmsAircraft {

    aCheckFh: number;
    acId: number;
    acStatusId: AircraftStatus;
    adate: Date;
    bCheckFh: number;
    cCheckFh: number;
    cabinId: number;
    cargoKg: number;
    costPerFh: number;
    crew: number;
    currAp: string;
    currApId: number;
    currApCode: string;
    currUdate: Date;
    dCheckFh: number;
    distanceKm: number;
    flightHours: number;
    homeAp: string;
    homeApId: number;
    homeApCode: string;
    macId: number;
    opTimeMin: number;
    ownerAlId: number;
    paxB: number;
    paxE: number;
    paxF: number;
    pilots: number;
    points: number;
    price: number;
    registration: string
    stars: number;
    state: number;
    udate: Date;
    cabinName: string;
    confort: number;

    acTypeId: number;
    fuelConsumptionLp100km: number;
    maxRangeKm: number;
    minRunwayM: number;
    cruiseSpeedKmph: number;

    imgTableUrl: string;
    imgAirUrl: string;
    imgMapUrl: string;

    acActionTypeId: AircraftActionType;
    logFlId?: number;
    logFlightH?: number;
    logDistanceKm: number;
    logAdate: Date;
    logUdate: Date;
    logDurationMin: number;
    flInQueue: number;
    forSheduleFlights?: boolean;
    pendingMnt: boolean;
    canAssignMaintenance:boolean;

    grpId: number;
    grpName:string;
    lastFlqArrTime: Date;
    lastFlqArrApId: number;
    flqSumFlightH: number;
    nextFlqDepApId: number;
    nextFlqDepTime: Date;
    nextMntId: number;
    nextMntStart: Date;
    nextAmtIdFlightHTo:number;
    nextAmtId:AmsMaintenanceTypes;

    public get canFlight(): boolean {
        let rv: boolean = false;
        rv = (this.acId && this.acActionTypeId > AircraftActionType.CabinConfigurationChange &&
            this.acActionTypeId < AircraftActionType.AircratLeasing) ? true : false;
        return rv;
    }


    getPendingMntType(): AmsMaintenanceTypes {
        let rv: AmsMaintenanceTypes;
        const fh = (this.flightHours ? this.flightHours : 0);
        if ((this.aCheckFh - fh) < 10 && (this.bCheckFh - fh) > 10) {
            rv = AmsMaintenanceTypes.ACheck;
        }
        if ((this.bCheckFh - fh) < 10 && (this.cCheckFh - fh) > 10) {
            rv = AmsMaintenanceTypes.BCheck;
        }
        if ((this.cCheckFh - fh) < 10 && (this.dCheckFh - fh) > 10) {
            rv = AmsMaintenanceTypes.CCheck;
        }
        if ((this.dCheckFh - fh) < 10) {
            rv = AmsMaintenanceTypes.BCheck;
        }
        return rv;
    }

    getPendingMntOpt(): AmsMaintenanceType {
        let rv: AmsMaintenanceType;
        const mntType = this.getPendingMntType();
        if (mntType) {
            rv = AmsMaintenanceTypesOpt.find(x => x.amtId == mntType);
        }
        return rv;
    }

    getNextMntType(): AmsMaintenanceTypes {
        let rv: AmsMaintenanceTypes;

        const fh = (this.flightHours ? this.flightHours : 0);
        let nextMeintenance = 101;
        if ((this.aCheckFh - fh) < nextMeintenance && (this.bCheckFh - fh) > nextMeintenance) {
            nextMeintenance = this.aCheckFh - fh;
            rv = AmsMaintenanceTypes.ACheck;
        }
        if ((this.bCheckFh - fh) < nextMeintenance && (this.cCheckFh - fh) > nextMeintenance) {
            nextMeintenance = this.bCheckFh - fh;
            rv = AmsMaintenanceTypes.BCheck;
        }
        if ((this.cCheckFh - fh) < nextMeintenance && (this.dCheckFh - fh) > nextMeintenance) {
            nextMeintenance = this.cCheckFh - fh;
            rv = AmsMaintenanceTypes.CCheck;
        }
        if ((this.dCheckFh - fh) < nextMeintenance) {
            nextMeintenance = this.dCheckFh - fh;
            rv = AmsMaintenanceTypes.BCheck;
        }
        return rv;
    }

    nextMntTimeMin() {

        let rv = 100;
        const fh = (this.flightHours ? this.flightHours : 0);
        const nextMtnType: AmsMaintenanceTypes = this.getNextMntType();
        switch (nextMtnType) {
            case AmsMaintenanceTypes.ACheck:
                rv = this.aCheckFh - fh;
                break;
            case AmsMaintenanceTypes.BCheck:
                rv = this.bCheckFh - fh;
                break;
            case AmsMaintenanceTypes.CCheck:
                rv = this.cCheckFh - fh;
                break;
            case AmsMaintenanceTypes.DCheck:
                rv = this.dCheckFh - fh;
                break;
            default:
                rv = this.aCheckFh - fh;
                break;
        }

        return rv;
    }
    get logAdateLapsedMin(): number {
        let rv = 0;
        try {
            rv = this.logAdate ? (new Date().getTime() - this.logAdate.getTime()) / (60 * 1000) : 0;
        } catch (ex) {
        }
        return rv;
    }

    get mntRequired(): boolean {
        let rv: boolean = false;
        const nextMntTimeMin = this.nextMntTimeMin();
        if (nextMntTimeMin < 0) { rv = true; }
        return rv;
    }

    getNextMntOpt(): AmsMaintenanceType {
        let rv: AmsMaintenanceType;
        const mntType = this.getNextMntType();
        if (mntType) {
            rv = AmsMaintenanceTypesOpt.find(x => x.amtId == mntType);
        }
        return rv;
    }

    getCanAssignMaintenance():boolean{
        return (this.pendingMnt && this.acActionTypeId !== AircraftActionType.MaintenanceCheck && !this.nextMntId);
    }


    public static fromJSON(json: IAmsAircraft): AmsAircraft {
        const vs = Object.create(AmsAircraft.prototype);

        Object.assign(vs, json, {
            acId: (json && json.acId) ? json.acId : undefined,
            flightHours: (json && json.flightHours) ? json.flightHours : 0,
            acStatusId: json.acStatusId ? json.acStatusId as AircraftStatus : undefined,
            acActionTypeId: json.acActionTypeId ? json.acActionTypeId as AircraftActionType : undefined,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            currUdate: (json && json.currUdate) ? new Date(json.currUdate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
            ownerAlId: (json && json.ownerAlId) ? json.ownerAlId : undefined,
            imgTableUrl: URL_COMMON_IMAGE_AIRCRAFT + (json.macId ? 'mac_' + json.macId + '_table.png?up=' + new Date().getTime() : 'aircraft_0_tableimg.png'),
            imgAirUrl: URL_COMMON_IMAGE_AIRCRAFT + (json.macId ? 'mac_' + json.macId + '_air.png?up=' + new Date().getTime() : 'aircraft_0_tableimg.png'),
            logAdate: (json && json.logAdate) ? new Date(json.logAdate) : undefined,
            logUdate: (json && json.logUdate) ? new Date(json.logUdate) : undefined,
            lastFlqArrTime: (json && json.lastFlqArrTime) ? new Date(json.lastFlqArrTime) : undefined,
            lastFlqArrApId: (json && json.lastFlqArrApId) ? json.lastFlqArrApId : undefined,
            forSheduleFlights: (json && json.forSheduleFlights && json.forSheduleFlights > 0) ? true : false,
            pendingMnt: AmsAircraft.getPendingMnt(json),
            imgMapUrl: URL_COMMON_IMAGE_AIRCRAFT_ICON + (json.acTypeId ? 'ac-type-' + json.acTypeId + '-yellow.png' : 'ac-type-2-yellow.png'),
            nextFlqDepTime: (json && json.nextFlqDepTime) ? new Date(json.nextFlqDepTime) : undefined,
            nextMntStart: (json && json.nextMntStart) ? new Date(json.nextMntStart) : undefined,
        });

        vs.canAssignMaintenance = vs.getCanAssignMaintenance();
        return vs;
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsAircraft.fromJSON(value) : value;
    }

    public static getPendingMnt(json: IAmsAircraft): boolean {
        let rv: boolean = false;
        
            const fh = (json.flightHours ? json.flightHours : 0);
            if ((json.aCheckFh - fh) < 10) {
                rv = true;
            }
            if ((json.bCheckFh - fh) < 10) {
                rv = true;
            }
            if ((json.cCheckFh - fh) < 10) {
                rv = true;
            }
            if ((json.dCheckFh - fh) < 10) {
                rv = true;
            }

        return rv;
    }
}

export interface IAmsAircraft {

    aCheckFh: number;
    acId: number;
    acStatusId: number;
    adate: string;
    bCheckFh: number;
    cCheckFh: number;
    cabinId: number;
    cargoKg: number;
    costPerFh: number;
    crew: number;
    currAp: string;
    currApId: number;
    currApCode: string;
    currUdate: string;
    dCheckFh: number;
    distanceKm: number;
    flightHours: number;
    homeAp: string;
    homeApId: number;
    homeApCode: string;
    macId: number;
    opTimeMin: number;
    ownerAlId: number;
    paxB: number;
    paxE: number;
    paxF: number;
    pilots: number;
    points: number;
    price: number;
    registration: string
    stars: number;
    state: number;
    udate: string;
    cabinName: string;
    confort: number;
    acTypeId: number;
    fuelConsumptionLp100km: number;
    maxRangeKm: number;
    minRunwayM: number;
    cruiseSpeedKmph: number;

    acActionTypeId: number;
    logFlId?: number;
    logFlightH?: number;
    logDistanceKm: number;
    logAdate: string;
    logUdate: string;
    logDurationMin: number;
    flInQueue: number;
    forSheduleFlights?: number;
    grpId: number;
    grpName:string;
    lastFlqArrTime: string;
    lastFlqArrApId: number;
    flqSumFlightH: number;
    nextFlqDepApId: number;
    nextFlqDepTime: string;
    nextMntId: number;
    nextMntStart: string;
    nextAmtIdFlightHTo:number;
    nextAmtId:AmsMaintenanceTypes;

}

export class AmsAircraftTableCriteria extends TableCriteriaBase {
    mfrId?: number;
    macId?: number;
    homeApId?: number;
    currApId?: number;
    ownerAlId?: number;
    newAc?: boolean;
    amsStatus?: number;
    flInQueue?: string;
    forSheduleFlights?: boolean;
}

export class AmsAircraftTableData {
    aircrafts: IAmsAircraft[];
    rowsCount: RowsCountModel;
}

export class ResponseAmsAircraftTable extends ResponseModel {
    data: AmsAircraftTableData;
}


export class AmsAircraftSaveData {
    aircraft: IAmsAircraft;
}

export class ResponseAmsAircraftSave extends ResponseModel {
    data: AmsAircraftSaveData;
}


//#endregion


//#region AmsAircraftStatDay

export class AmsAircraftStatDay implements IAmsAircraftStatDay {

    id: number;
    acId: number;

    alId: number;
    day: Date;
    flightsCount?: number;
    chartersCount: number;
    distanceKm?: number;
    oilL?: number;
    pax?: number;
    payloadKg: number;
    expences: number;
    income: number;
    revenue: number;
    stdate: string;


    public static fromJSON(json: IAmsAircraftStatDay): AmsAircraftStatDay {
        const vs = Object.create(AmsAircraftStatDay.prototype);
        return Object.assign(vs, json, {
            id: json.id ? json.id : undefined,
            day: (json && json.stdate) ? new Date(json.stdate) : undefined,
        });
    }

    public static reviver(key: string, value: any): any {

        return key === '' ? AmsAircraftStatDay.fromJSON(value) : value;
    }

    public static getLineChartData(rows: AmsAircraftStatDay[]): any {
        let revenue = new Array<number>();
        let expences = new Array<number>();
        let income = new Array<number>();
        let lineChartData;
        if (rows && rows.length > 0) {
            const prevDto = rows[0];
            let prevDay = prevDto?.day;
            rows.slice().reverse().forEach(row => {
                if (row && row.day) {
                    var diff = Math.abs(prevDay.getTime() - row.day.getTime());
                    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
                    if (diffDays > 0 && false) {
                        for (let index = 0; index < diffDays; index++) {
                            expences.push(0);
                            income.push(0);
                            revenue.push(0);
                        }
                    }
                    expences.push(row && row.expences ? row.expences : 0);
                    income.push(row && row.income ? row.income : 0);
                    revenue.push(row && row.revenue ? row.revenue : 0);
                    prevDay = row.day;
                }
            });
        }
        lineChartData = [
            { data: revenue, label: 'End of day Revenue' },
        ];
        return lineChartData;

    }

    public static getLineChartLabels(rows: AmsAircraftStatDay[]): any[] {
        let lineChartLabels = new Array<string>();
        if (rows && rows.length >= 0) {
            const prevDto = rows[0];
            let prevDay = prevDto?.day;
            let lastDate = prevDto?.day;
            rows.slice().reverse().forEach(row => {
                if (row && row.day) {
                    var diff = Math.abs(prevDay.getTime() - row.day.getTime());
                    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
                    if (diffDays > 0 && false) {
                        for (let index = 0; index < diffDays; index++) {
                            lastDate.setDate(row.day.getDate() - index);
                            const label = TsDatePipe.padLeft((lastDate.getMonth() + 1).toString(), '0', 2) +
                                '-' + TsDatePipe.padLeft(lastDate.getDate().toString(), '0', 2) +
                                '-' + lastDate.getFullYear();
                            lineChartLabels.push(label);
                        }
                    }
                    //const label = row.day.toString().split(' ')[0];
                    const label = TsDatePipe.padLeft((row.day.getMonth() + 1).toString(), '0', 2) +
                        '-' + TsDatePipe.padLeft(row.day.getDate().toString(), '0', 2) +
                        '-' + row.day.getFullYear();
                    lineChartLabels.push(label);
                    prevDay = row.day;
                }
            });
        }
        return lineChartLabels;

    }

}

export interface IAmsAircraftStatDay {
    id: number;
    acId: number;

    alId: number;
    stdate: string;
    flightsCount?: number;
    chartersCount: number;
    distanceKm?: number;
    oilL?: number;
    pax?: number;
    payloadKg: number;
    expences: number;
    income: number;
    revenue: number;
}

export class AmsAircraftStatDayTableCriteria extends TableCriteriaBase {
    alId?: number;
    acId?: number;
}

export class AmsAircraftStatDayData {
    rows: AmsAircraftStatDay[];
    rowsCount: number;
    totalRevenue: number;
}

export class AmsAircraftStatDayTableData extends ResponseModel {
    data: AmsAircraftStatDayData;
}

//#endregion
