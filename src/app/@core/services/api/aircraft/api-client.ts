import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { lastValueFrom, Observable, of, Subscription } from 'rxjs';
import { catchError, elementAt, map, take, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
// Services
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../../auth/current-user.service';
// --- Models
import { environment } from 'src/environments/environment';
import {
  AmsAircraft, AmsAircraftStatDay, AmsAircraftStatDayData, AmsAircraftStatDayTableCriteria, AmsAircraftStatDayTableData, AmsAircraftTableCriteria, AmsMac, AmsMacCabin,
  AmsMacCabinTableCriteria,
  AmsMacTableCriteria, AmsManufacturer, AmsManufacturerTableCriteria,
  IAmsAircraft,
  IAmsAircraftStatDay,
  ResponseAmsAircraftSave, ResponseAmsAircraftTable, ResponseAmsJsonData,
  ResponseAmsMacCabinDelete, ResponseAmsMacCabinSave, ResponseAmsMacCabinTableData,
  ResponseAmsMacDelete, ResponseAmsMacSave, ResponseAmsMacTableData,
  ResponseAmsManufacturerTableData, ResponseManufacturerDelete, ResponsemanufacturerSave
} from './dto';
import { AmsMaintenancePlan, AmsMaintenancePlanCreateData, AmsMaintenancePlanSaveData, AmsMaintenancePlanTableCriteria, ResponseAmsMaintenancePlanCreateData, ResponseAmsMaintenancePlanDelete, ResponseAmsMaintenancePlanTableData } from './maintenance-plan';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class AmsAircraftClient extends ApiBaseClient {

  constructor(
    private toastr: ToastrService,
    private client: HttpClient,
    private cus: CurrentUserService) {
    super();
  }

  private get sertviceUrl(): string {
    return environment.services.url.aircraft;
  }


  //#region Manufacturer

  mfrTable(criteria: AmsManufacturerTableCriteria): Observable<ResponseAmsManufacturerTableData> {

    console.log('mfrTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mfr_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        apId: criteria.apId,
        amsStatus: criteria.amsStatus
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsManufacturerTableData) => {
          const res = Object.assign(new ResponseAmsManufacturerTableData(), resp);
          console.log('mfrTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap mfrTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  getManufacturer(value: number): Observable<AmsManufacturer> {
    console.log('getManufacturer-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mfr_get',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponsemanufacturerSave) => {
          console.log('getManufacturer-> response=', response);
          const res = Object.assign(new ResponsemanufacturerSave(), response);
          console.log('getManufacturer-> res=', res);
          let rv: AmsManufacturer;
          if (res && res.success) {
            if (res && res.data && res.data.manufacturer) {
              rv = AmsManufacturer.fromJSON(res.data.manufacturer);
            }
          }
          console.log('getManufacturer-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap getManufacturer get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }


  mfrSave(value: AmsManufacturer): Observable<AmsManufacturer> {
    //console.log('mfrSave-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mfr_save',
      { manufacturer: value },
      { headers: hdrs }).pipe(
        map((response: ResponsemanufacturerSave) => {
          console.log('mfrSave-> response=', response);
          const res = Object.assign(new ResponsemanufacturerSave(), response);
          console.log('airportSave-> res=', res);
          let rv: AmsManufacturer;
          if (res && res.success) {
            if (res && res.data && res.data.manufacturer) {
              rv = AmsManufacturer.fromJSON(res.data.manufacturer);
            }
          }
          console.log('mfrSave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap mfrSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  mfrDelete(value: number): Observable<number> {
    console.log('mfrDelete-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mfr_delete',
      { mfrId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseManufacturerDelete) => {
          console.log('mfrDelete-> response=', response);
          const res = Object.assign(new ResponseManufacturerDelete(), response);
          console.log('mfrDelete-> res=', res);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.mfrId) {
              rv = res.data.mfrId;
            }
          }
          console.log('mfrDelete-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap mfrDelete delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

  //#region Mac

  macTable(criteria: AmsMacTableCriteria): Observable<ResponseAmsMacTableData> {

    //console.log('macTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        mfrId: criteria.mfrId,
        amsStatus: criteria.amsStatus
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsMacTableData) => {
          const res = Object.assign(new ResponseAmsMacTableData(), resp);
          //console.log('macTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap macTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  async macListByAlIdP(criteria: number): Promise<AmsMac[]> {
    const source$ = this.macListByAlId(criteria);
    return await lastValueFrom<any>(source$);
  }

  macListByAlId(value: number): Observable<AmsMac[]> {

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_list_al',
      {
        alId: value,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsMacTableData) => {
          const res = Object.assign(new ResponseAmsMacTableData(), resp);
          let rv: AmsMac[] = new Array<AmsMac>;
          if (res && res.success) {
            if (res && res.data && res.data.macs) {
              res.data.macs.forEach(element => {
                rv.push(AmsMac.fromJSON(element));
              });
            }
          }
          return rv;
        }),
        catchError(this.handleError)
      );
  }

  getMac(value: number): Observable<AmsMac> {
    console.log('getMac-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_get',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsMacSave) => {
          console.log('getMac-> response=', response);
          const res = Object.assign(new ResponseAmsMacSave(), response);
          console.log('getMac-> res=', res);
          let rv: AmsMac;
          if (res && res.success) {
            if (res && res.data && res.data.mac) {
              rv = AmsMac.fromJSON(res.data.mac);
            }
          }
          console.log('getMac-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap getMac get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  getJsonData(value: string, limit: number = 1000): Observable<ResponseAmsJsonData> {
    console.log('getJsonData-> sql=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'get_json',
      { sql: value, limit: limit },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsJsonData) => {
          console.log('getJsonData-> response=', response);
          const res = Object.assign(new ResponseAmsJsonData(), response);
          console.log('getMac-> res=', res);
          return res;

        }),
        tap(event => {
          this.log(`tap getJsonData get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }


  macSave(value: AmsMac): Observable<AmsMac> {
    console.log('macSave-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_save',
      { mac: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsMacSave) => {
          //console.log('macSave-> response=', response);
          const res = Object.assign(new ResponseAmsMacSave(), response);
          console.log('macSave-> res=', res);
          let rv: AmsMac;
          if (res && res.success) {
            if (res && res.data && res.data.mac) {
              rv = AmsMac.fromJSON(res.data.mac);
            }
          }
          console.log('macSave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap mfrSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  macDelete(value: number): Observable<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_delete',
      { apId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsMacDelete) => {
          const res = Object.assign(new ResponseAmsMacDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.macId) {
              rv = res.data.macId;
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap macDelete delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );



  }

  //#endregion


  //#region MacCabin

  macCabinTable(criteria: AmsMacCabinTableCriteria): Observable<ResponseAmsMacCabinTableData> {

    console.log('macCabinTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_cabin_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        macId: criteria.macId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsMacCabinTableData) => {
          const res = Object.assign(new ResponseAmsMacCabinTableData(), resp);
          console.log('macCabinTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap macCabinTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  getMacCabin(value: number): Observable<AmsMacCabin> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_cabin_get',
      { cabinId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsMacCabinSave) => {
          const res = Object.assign(new ResponseAmsMacCabinSave(), response);
          let rv: AmsMacCabin;
          if (res && res.success) {
            if (res && res.data && res.data.cabin) {
              rv = AmsMacCabin.fromJSON(res.data.cabin);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap getMacCabin get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  macCabinSave(value: AmsMacCabin): Observable<AmsMacCabin> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_cabin_save',
      { cabin: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsMacCabinSave) => {
          const res = Object.assign(new ResponseAmsMacCabinSave(), response);
          let rv: AmsMacCabin;
          if (res && res.success) {
            if (res && res.data && res.data.cabin) {
              rv = AmsMacCabin.fromJSON(res.data.cabin);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap macCabinSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  async macCabinDelete(value: number): Promise<number> {

    //console.log('calcCityPayloadDemend ->');
    const url = this.sertviceUrl + 'mac_cabin_delete';
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //

    const source$ = this.client.post(url, { cabinId: value }, { headers: hdrs })
      .pipe(
        take(1),
        map((response: ResponseAmsMacCabinDelete) => {
          console.log('macCabinDelete -> response:', response);
          const res = Object.assign(new ResponseAmsMacCabinDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.cabinId) {
              rv = res.data.cabinId;
            }
          }
          return rv;
        }),
        catchError(this.handleError)
      );
    return await lastValueFrom<number>(source$);
  }

  macCabinDeleteObs(value: number): Observable<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_cabin_delete',
      { cabinId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsMacCabinDelete) => {
          const res = Object.assign(new ResponseAmsMacCabinDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.cabinId) {
              rv = res.data.cabinId;
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap macCabinDelete delete event: ` + JSON.stringify(event));
          // There may be other events besides the response.
        }),
        catchError(this.handleError)
      );
  }

  //#endregion


  //#region Aircraft

  async aircraftGetP(acId: number): Promise<AmsAircraft> {
    const source$ = this.aircraftGet(acId);
    return await lastValueFrom<any>(source$);
  }

  aircraftGet(value: number): Observable<AmsAircraft> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ac_get',
      { acId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsAircraftSave) => {
          const res = Object.assign(new ResponseAmsAircraftSave(), response);
          let rv: AmsAircraft;
          if (res && res.success) {
            if (res && res.data && res.data.aircraft) {
              rv = AmsAircraft.fromJSON(res.data.aircraft);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap airlineGet get event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  aircraftSaveOld(value: AmsAircraft): Observable<AmsAircraft> {
    //console.log('mfrSave-> value=', value);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ac_save',
      { aircraft: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsAircraftSave) => {
          console.log('aircraftSave-> response=', response);
          const res = Object.assign(new ResponseAmsAircraftSave(), response);
          let rv: AmsAircraft;
          if (res && res.success) {
            if (res && res.data && res.data.aircraft) {
              rv = AmsAircraft.fromJSON(res.data.aircraft);
            }
          }
          console.log('aircraftSave-> rv=', rv);
          return rv;
        }),
        tap(event => {
          this.log(`tap aircraftSave save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  aircraftSave(value: AmsAircraft): Promise<AmsAircraft> {

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    const headers = { headers: hdrs, observe: 'response' };
    const url = this.sertviceUrl + 'ac_save';
    const data = { aircraft: value };
    let promise = new Promise<AmsAircraft>((resolve, reject) => {

      this.client
        .post<ResponseAmsAircraftSave>(url, data, { headers: hdrs, observe: 'response' })
        .pipe(catchError(this.handleError))
        .subscribe((result: any) => {
          //console.log('characterSave -> resp: ', result);
          const rv: ResponseAmsAircraftSave = Object.assign(new ResponseAmsAircraftSave(), result.body);
          //console.log('characterSave rv:', rv);
          let dto: AmsAircraft;
          if (rv && rv.success) {
            this.toastr.success(rv.message, rv.status);
            if (rv.data && rv.data.aircraft) {
              dto = AmsAircraft.fromJSON((rv.data.aircraft as IAmsAircraft));
            }
            //console.log('chapterSave -> value:', value);
            resolve(dto);
            //
          } else {
            this.toastr.error(rv.message, rv.status);
            reject(rv.message);
          }

        },
          (err: any) => {
            console.log('aircraftSave -> err:', err);
            this.toastr.error(err.error, 'Save aircraft');
            reject(err.error);
          });
    });
    return promise;
  }


  async aircraftTableP(criteria: AmsAircraftTableCriteria): Promise<ResponseAmsAircraftTable> {
    const source$ = this.aircraftTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  aircraftTable(criteria: AmsAircraftTableCriteria): Observable<ResponseAmsAircraftTable> {

    //console.log('macTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'ac_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        mfrId: criteria.mfrId,
        macId: criteria.macId,
        homeApId: criteria.homeApId,
        currApId: criteria.currApId,
        newAc: criteria.newAc,
        ownerAlId: criteria.ownerAlId,
        amsStatus: criteria.amsStatus,
        forSheduleFlights: criteria.forSheduleFlights,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsAircraftTable) => {
          //console.log('aircraftTable -> resp=', resp);
          const res = Object.assign(new ResponseAmsAircraftTable(), resp);
          //console.log('aircraftTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap aircraftTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  async acMarketTableP(criteria: AmsAircraftTableCriteria): Promise<ResponseAmsMacTableData> {
    const source$ = this.acMarketTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  acMarketTable(criteria: AmsAircraftTableCriteria): Observable<ResponseAmsMacTableData> {

    //console.log('acMarketTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'mac_market_table',
      {

        qry_offset: criteria.offset,
        qry_limit: criteria.limit,
        qry_orderCol: criteria.sortCol,
        qry_isDesc: criteria.sortDesc,
        qry_filter: criteria.filter,
        mfrId: criteria.mfrId,
        macId: criteria.macId,
        homeApId: criteria.homeApId,
        currApId: criteria.currApId,
        newAc: criteria.newAc,
        ownerAlId: criteria.ownerAlId,
        amsStatus: criteria.amsStatus
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsMacTableData) => {
          //console.log('acMarketTable -> resp=', resp);
          const res = Object.assign(new ResponseAmsMacTableData(), resp);
          return res;
        }),
        tap(event => {
          this.log(`tap aircraftTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  acBuy(acId: number, alId: number): Observable<ResponseAmsAircraftSave> {

    //console.log('acMarketTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'acat_purchase',
      {
        acId: acId,
        alId: alId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsAircraftSave) => {
          console.log('acBuy -> resp=', resp);
          const res = Object.assign(new ResponseAmsAircraftSave(), resp);
          return res;
        }),
        tap(event => {
          this.log(`tap aircraftTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

  //#region AC Maintenance Check

  async acMaintenancePlanTableP(criteria: AmsMaintenancePlanTableCriteria): Promise<ResponseAmsMaintenancePlanTableData> {
    const source$ = this.acMaintenancePlanTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  acMaintenancePlanTable(criteria: AmsMaintenancePlanTableCriteria): Observable<ResponseAmsMaintenancePlanTableData> {

    //console.log('macTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'acmp_table',
      {

        qry_offset: criteria?.offset ?? 0,
        qry_limit: criteria?.limit ?? 12,
        qry_orderCol: criteria?.sortCol,
        qry_isDesc: criteria?.sortDesc,
        qry_filter: criteria?.filter,
        alId: criteria?.alId,
        acId: criteria?.acId,
        apId: criteria?.apId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsMaintenancePlanTableData) => {
          //console.log('aircraftTable -> resp=', resp);
          const res = Object.assign(new ResponseAmsMaintenancePlanTableData(), resp);
          //console.log('aircraftTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap aircraftTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  async maintenancePlanTableHP(criteria: AmsMaintenancePlanTableCriteria): Promise<ResponseAmsMaintenancePlanTableData> {
    const source$ = this.maintenancePlanTableH(criteria);
    return await lastValueFrom<any>(source$);
  }

  maintenancePlanTableH(criteria: AmsMaintenancePlanTableCriteria): Observable<ResponseAmsMaintenancePlanTableData> {

    //console.log('macTable-> criteria=', criteria);

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'acmp_table_h',
      {

        qry_offset: criteria?.offset ?? 0,
        qry_limit: criteria?.limit ?? 12,
        qry_orderCol: criteria?.sortCol,
        qry_isDesc: criteria?.sortDesc,
        qry_filter: criteria?.filter,
        alId: criteria?.alId,
        acId: criteria?.acId,
        apId: criteria?.apId,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsMaintenancePlanTableData) => {
          //console.log('aircraftTable -> resp=', resp);
          const res = Object.assign(new ResponseAmsMaintenancePlanTableData(), resp);
          //console.log('aircraftTable -> response=', res);
          return res;
        }),
        tap(event => {
          this.log(`tap aircraftTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  async maintenanceGetP(value: number): Promise<AmsMaintenancePlan> {
    const source$ = this.maintenanceGet(value);
    return await lastValueFrom<any>(source$);
  }

  maintenanceGet(value: number): Observable<AmsMaintenancePlan> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'acmp_get',
      { acmpId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsMaintenancePlanCreateData) => {
          const res = Object.assign(new ResponseAmsMaintenancePlanCreateData(), response);
          let rv: AmsMaintenancePlan;
          if (res && res.success) {
            if (res && res.data && res.data.maintenance) {
              rv = AmsMaintenancePlan.fromJSON(res.data.maintenance);
            }
          }
          return rv;
        }),
        catchError(this.handleError)
      );
  }


  acMaintenancePlanCreate(req: AmsMaintenancePlanCreateData): Observable<AmsMaintenancePlanSaveData> {
    console.log('acMaintenancePlanCreate-> req=', req);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'acmp_create',
      {
        acId: req.acId,
        amtId: req.amtId,
        stTime: req.stTimeMs ? req.stTimeMs : undefined,
      },
      { headers: hdrs }).pipe(
        map((resp: ResponseAmsMaintenancePlanCreateData) => {
          console.log('acMaintenancePlanCreate  -> resp=', resp);
          const res = Object.assign(new ResponseAmsMaintenancePlanCreateData(), resp);
          return res.data;
        }),
        tap(event => {
          this.log(`tap aircraftTable event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );
  }

  async maintenanceDeleteP(value: number): Promise<number> {
    const source$ = this.maintenanceDelete(value);
    return await lastValueFrom<any>(source$);
  }

  maintenanceDelete(value: number): Observable<number> {
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.sertviceUrl + 'acmp_delete',
      { acmpId: value },
      { headers: hdrs }).pipe(
        map((response: ResponseAmsMaintenancePlanDelete) => {
          const res = Object.assign(new ResponseAmsMaintenancePlanDelete(), response);
          let rv: number;
          if (res && res.success) {
            if (res && res.data && res.data.acmpId) {
              rv = res.data.acmpId;
            }
          }
          return rv;
        }),
        catchError(this.handleError)
      );



  }

  //#endregion

  //#region AmsAircraftStatDay

  async getAcStatDayTableP(criteria: AmsAircraftStatDayTableCriteria): Promise<AmsAircraftStatDayData> {
    const source$ = this.getAcStatDayTable(criteria);
    return await lastValueFrom<any>(source$);
  }

  getAcStatDayTable(req: AmsAircraftStatDayTableCriteria): Observable<AmsAircraftStatDayData> {
    //console.log('transStat8Weeks-> alId=', alId);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post(this.sertviceUrl + 'ac_al_stat_day_table',
      {
        qry_offset: req.offset,
        qry_limit: req.limit,
        alId: req.alId,
        acId: req.acId,
      },
      { headers: hdrs }).pipe(
        map((resp: AmsAircraftStatDayTableData) => {
          //console.log('getAcStatDayTable -> resp=', resp);
          const res = Object.assign(new AmsAircraftStatDayTableData(), resp);
          let data = new AmsAircraftStatDayData();
          data.rows = new Array<AmsAircraftStatDay>();
          data.rowsCount = 0;
          data.totalRevenue = 0;
          if (res && res.data) {
            data.rowsCount = res.data.rowsCount;
            data.totalRevenue = res.data.totalRevenue;
            if (res.data.rows && res.data.rows.length > 0) {
              res.data.rows.forEach(element => {
                data.rows.push(AmsAircraftStatDay.fromJSON((element)));
              });
            }
          }

          return data;
        }),
        catchError(this.handleError)
      );
  }

  //#endregion

}
