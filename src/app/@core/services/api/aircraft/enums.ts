import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "src/app/@core/models/common/enum-view.model";
import { AmsAircraft } from "./dto";
import { UndefinedResult } from "@es-joy/jsdoccomment";

//#region  MaintenanceTypes

export class AmsMaintenanceType {
    amtId: number;
    name: string;
    periodH: number;
    priceFacktor: number;
    timeMin: number;

    constructor() { }

    public static getPrice(aircraft: AmsAircraft, acmt: AmsMaintenanceType): number {
        let rv: number;
        if (aircraft && acmt.amtId) {
            rv = ((aircraft.price * acmt.priceFacktor) * (aircraft.currApId === aircraft.homeApId ? 0.5 : 1));
        }
        return rv;
    }
}

export enum AmsMaintenanceTypes {
    ACheck = 1,
    BCheck = 2,
    CCheck = 3,
    DCheck = 4
}

export const AmsMaintenanceTypesOpt: AmsMaintenanceType[] = [
    {
        amtId: 1,
        name: 'A check',
        periodH: 100,
        priceFacktor: 0.0001,
        timeMin: (6 * 60)
    },
    {
        amtId: 2,
        name: 'B check',
        periodH: 2_000,
        priceFacktor: 0.0008,
        timeMin: (24 * 60)
    },
    {
        amtId: 3,
        name: 'C check',
        periodH: 50_000,
        priceFacktor: 0.06,
        timeMin: (72 * 60)
    },
    {
        amtId: 4,
        name: 'D check',
        periodH: 100_000,
        priceFacktor: 0.24,
        timeMin: (144 * 60)
    },

];

@Pipe({ name: 'mnttype' })
export class AmsMaintenanceTypesDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsMaintenanceTypes[value];
        const data: AmsMaintenanceType = AmsMaintenanceTypesOpt.find(x => x.amtId === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}

//#endregion

export enum AmsFlightTypeOld {
    Schedule = 1,
    Charter = 2,
    Transfer = 3,
}

export const AmsFlightTypeOldOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Schedule'
    },
    {
        id: 2,
        name: 'Charter'
    },
    {
        id: 3,
        name: 'Transfer '
    },

];

/* 
@Pipe({ name: 'flighttype' })
export class AmsFlightTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsFlightType[value];
        const data: EnumViewModel = AmsFlightTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
} */
