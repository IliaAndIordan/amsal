import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { findIndex, map, shareReplay } from "rxjs/operators";
import { CurrentUserService } from "../../auth/current-user.service";
import { AmsAircraftClient } from "./api-client";
import { AmsAircraft, AmsAircraftTableCriteria, AmsAircraftTableData, ResponseAmsAircraftTable } from "./dto";
import { iconDefault } from "src/app/@share/maps/map-liflet-ap-marker.service";

@Injectable({ providedIn: 'root' })
export class AmsAirlineAircraftCacheService {

  acCache$: Observable<ResponseAmsAircraftTable>;

  constructor(
    private cus: CurrentUserService,
    private acClient: AmsAircraftClient) {
  }

  get aircraftTable(): Observable<ResponseAmsAircraftTable> {
    if (!this.acCache$) {

      this.acCache$ = this.getAircraftTable().pipe(shareReplay());
    }
    return this.acCache$;
  }

  clearCache(): void {
    this.acCache$ = undefined;
  }

  reloadCache(): Observable<AmsAircraft[]> {
    this.acCache$ = undefined;
    return this.aircrafts;
  }

  get aircrafts(): Observable<AmsAircraft[]> {
    return this.aircraftTable.pipe(
      map((acTable: ResponseAmsAircraftTable) => {
        let rv = new Array<AmsAircraft>();
        if (acTable && acTable.data && acTable.data.aircrafts) {
          for (const ac of acTable.data.aircrafts) {
            const aircraft = AmsAircraft.fromJSON(ac);
            rv.push(aircraft);
          }
        }
        //console.log('amsAirportsList -> rv:', rv);
        return rv;
      })
    );
  }

  getAircraft(id: number): Observable<AmsAircraft> {
    return this.aircrafts.pipe(
      map(aps => aps.find(x => x.acId == id))
    );
  }

  loadAircrafts(): Promise<AmsAircraft[]> {
    let promise = new Promise<AmsAircraft[]>((resolve, reject) => {
      const req$ = this.aircrafts;
      req$.subscribe((data: AmsAircraft[]) => {
        resolve(data);
      });
    });

    return promise;
  }

  getAircraftTable(): Observable<ResponseAmsAircraftTable> {
    const criteria = new AmsAircraftTableCriteria()
    criteria.limit = 20000;
    criteria.offset = 0;
    criteria.ownerAlId - this.cus.airline.alId;

    return this.acClient.aircraftTable(criteria)
  }

  getAlAircrafts(alId: number): Observable<AmsAircraft[]> {
    return this.aircrafts.pipe(
      map(aps => aps.filter(x => x.ownerAlId == alId))
    );
  }


}