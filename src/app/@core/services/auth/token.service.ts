import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AppStore, SessionStore } from '../../const/app-storage.const';
import { RefreshTokenModel } from './api/dto';
import { TokenFactory } from './token-factory';

@Injectable({ providedIn: 'root' })
export class TokenService extends TokenFactory {


  constructor(private jwtHelper: JwtHelperService) {
    super();
  }

  //#region JwtHelperService methods

  isTokenExpired(): boolean {
    const retVal = this.expIntervalMs < 0;
    return retVal
  }

  isAccessTokenExpired(): boolean {
    const retVal = this.expIntervalMs < 0;
    return retVal;
  }

  isRefreshTokenExpired(): boolean {
    let rv = true;
    if (this.refreshToken) {
      let refToken: RefreshTokenModel;
      try {
        refToken = Object.assign(new RefreshTokenModel(), this.decode(this.refreshToken));
      } catch (err) {
        this.refreshToken = undefined;
        console.log('isRefreshTokenExpired-> err:', err);
      }
      // console.log('isRefreshTokenExpired-> refToken:', refToken);
      if (refToken) {
        const curDate: Date = new Date();
        // console.log('isRefreshTokenExpired-> curDate ms:', curDate.getTime());
        const diff_ms = ((refToken.exp * 1000) - (curDate.getTime() + 500));
        // console.log('isRefreshTokenExpired-> diff_ms:', diff_ms.toFixed());
        rv = diff_ms > 0 ? false : true;
      }
    }


    return rv;
  }

  public getTokenExpirationDate(): Date {
    let jwtExp;
    const decodedToken = this.jwtHelper.decodeToken();

    if (decodedToken && decodedToken.exp) {
      jwtExp = new Date(0);
      jwtExp.setUTCSeconds(decodedToken.exp);
    }

    // console.log('ExpIntervalMs-> rv=', rv);
    return jwtExp;
  }

  decodeToken(): string {
    let rv: any;
    try {
      rv = this.jwtHelper.decodeToken();
    } catch (err) {
      this.clearToken();
    }
    return rv;
  }

  decode(token: string): string {
    return this.jwtHelper.decodeToken(token);
  }

  get expIntervalMs(): number {
    let rv = 0;
    const decodedToken = this.jwtHelper.decodeToken();

    if (decodedToken && decodedToken.exp) {
      const jwtExp = new Date(0);
      jwtExp.setUTCSeconds(decodedToken.exp);
      const curDate: Date = new Date();
      rv = jwtExp.getTime() - curDate.getTime();
    }

    // console.log('ExpIntervalMs-> rv=', rv);
    return rv;
  }

  //#endregion

  //#region Token



  public get barerToken(): string {
    return sessionStorage.getItem(SessionStore.BarerToken);
  }

  public set barerToken(value: string) {
    if (value) {
      sessionStorage.setItem(SessionStore.BarerToken, JSON.stringify(value));
    } else {
      sessionStorage.removeItem(SessionStore.BarerToken);
    }
  }

  public get refreshToken(): string {
    return sessionStorage.getItem(SessionStore.RefreshToken);
  }

  public set refreshToken(value: string) {
    if (value) {
      sessionStorage.setItem(SessionStore.RefreshToken, JSON.stringify(value));
    } else {
      sessionStorage.removeItem(SessionStore.RefreshToken);
    }
  }


  public clearToken() {
    sessionStorage.removeItem(SessionStore.RefreshToken);
    sessionStorage.removeItem(SessionStore.BarerToken);
  }


  //#endregion
}
