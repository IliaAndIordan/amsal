import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
// Services
import { CurrentUserService } from './current-user.service';
import { ApiAuthClient } from './api/api-auth.client';
import { ApiBaseClient } from '../api-base/api-base.client';
import { JwtModule, JWT_OPTIONS, JwtInterceptor } from '@auth0/angular-jwt';
import { TokenFactory } from './token-factory';
import { TokenInterceptor } from './token.interseptor';
import { GravatarModule } from 'ngx-gravatar';

@NgModule({

  imports: [
    CommonModule,
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useClass: TokenFactory
      }
    }),
    GravatarModule, 
  ],
  declarations: [],
  exports: [
    GravatarModule, 
  ],
  providers: [
    //ApiBaseClient,
    //ApiAuthClient,
    JwtInterceptor, // Providing JwtInterceptor allow to inject JwtInterceptor manually into RefreshTokenInterceptor
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    //CurrentUserService,
  ]
})
export class SxAuthModule {

}
