import { ICompanyModel, ICompanyProductModel } from 'src/app/@core/models/common/sx-company.model';
import { ILocalUserSettings } from 'src/app/@core/models/app/local-user-settings';
import { UserRole } from 'src/app/@core/models/pipes/sx-user-role.pipe';
import { ResponseModel } from 'src/app/@core/models/common/responce.model';
import { AmsAirline, IAmsAirline } from '../../api/airline/dto';
import { IAmsAirport } from '../../api/airport/dto';
import { TableCriteriaBase, TableResponseBase } from 'src/app/@core/models/common/table-criteria-base.model';



export class RefreshTokenModel {
    client_id: number;
    exp: number;
    iss: string;
    guid: string;
}

export class UserModel {

    public userId: number;
    public userName: string;
    public userEmail: string;
    public userPwd: string;
    public userRole: UserRole;
    public isReceiveEmails: boolean;
    public ipAddress: string;
    public udate: Date;
    public adate: Date;

    public static fromJSON(json: IUserModel): UserModel {
        const vs = Object.create(UserModel.prototype);
        return Object.assign(vs, json, {
            userRole: json.userRole ? json.userRole as UserRole : UserRole.User,
            isReceiveEmails: json.isReceiveEmails && json.isReceiveEmails > 0 ? true : false,
            adate: (json && json.adate) ? new Date(json.adate) : undefined,
            udate: (json && json.udate) ? new Date(json.udate) : undefined,
        });
    }

    // reviver can be passed as the second parameter to JSON.parse
    // to automatically call User.fromJSON on the resulting value.
    public static reviver(key: string, value: any): any {

        return key === '' ? UserModel.fromJSON(value) : value;
    }
    /*
    public toJSON(): IUserModel {
    
        return Object.assign({}, this, {
            is_receive_emails:this.is_receive_emails?1:0;
            adate: this.adate ? (this.adate instanceof Date ?
                this.adate.toJSON() : undefined) : undefined,
            udate: this.udate ?  (this.udate instanceof Date ?
                this.udate.toJSON() : undefined) : undefined,
        });

    }
    */
}

export interface IUserModel {
    userId: number;
    userName: string;
    userEmail: string;
    userPwd: string;
    userRole: number;
    isReceiveEmails: number;
    ipAddress: string;
    adate: string;
    udate: string;
}


export interface ResponseAuthenticateUser {
    user: IUserModel;
    settings: ILocalUserSettings;
    airline: IAmsAirline;
    hqAirport: IAmsAirport;
}

export class ResponseAuthenticate extends ResponseModel {
    data: ResponseAuthenticateUser;
}

export class LatLng {
    constructor(latitude: number, longitude: number, altitude?: number) {
        this.lat = latitude;
        this.lng = longitude;
        this.alt = altitude;
    }
    lat: number;
    lng: number;
    alt?: number;
    ipAddress?: string;
    action?: string;
    time?: string;
}


export class ResponseGetUserDatao {
    public user: IUserModel[];
}

interface ResponseGetUserData {
    user: IUserModel;
}

export class ResponseGetUser extends ResponseModel {
    data: ResponseGetUserData;
}


export class ResponseUserSaveData {
    user: IUserModel;

}

export class ResponseUserSave extends ResponseModel {
    data: ResponseUserSaveData;
}



export interface IUserTimestampModel {

    user_id: number;
    user_name: string;
    e_mail: string;
    u_password: string;
    company_id: number;
    user_role: number;
    is_receive_emails: number;
    ip_address: string;
    adate: number;
    udate: number;
    userName: string;

    company_name?: string;
    branch_code?: string;
    company_type_id?: number;
    country_id?: number;
}

export class RespCompanyUsersData {
    users: IUserTimestampModel[];
    totals: {
        total_rows: number;
    };
}

export class RespCompanyUsers extends ResponseModel {
    data: RespCompanyUsersData;
}


//#region CookiesConsentModel
export class CookieModel{
    name?:string;
    value?: string;
    expires?: any;
    path?: string;
    domain?: string;
    secure?: boolean;
    sameSite?: any;
  }
  
  export class CookiesConsentModel {
    id?: number;
    ipaddress?: string;
    userId?: number;
    consentUseLocalStore: boolean = false;
    consentUseStatistics: boolean = false;
    consentAdvertisement:boolean = false;
    adate?: Date;
    udate?: Date;
  
    userName?: string;
    email?: string;
  
    get hasConsent(): boolean {
      return (this.consentUseLocalStore && this.consentUseStatistics)??false;
    }
  
    public static fromJSON(json: any): CookiesConsentModel {
      const vs = Object.create(CookiesConsentModel.prototype);
      return Object.assign(vs, json, {
        id: json.id ? json.id : undefined,
        consentUseLocalStore: json.consentUseLocalStore ? json.consentUseLocalStore !== 0 : false,
        consentUseStatistics: json.consentUseStatistics ? json.consentUseStatistics !== 0 : false,
        consentAdvertisement: json.consentAdvertisement ? json.consentAdvertisement !== 0 : false,
        adate: (json && json.adate) ? new Date(json.adate) : undefined,
        udate: (json && json.udate) ? new Date(json.udate) : undefined,
      });
    }
  
    public static reviver(key: string, value: any): any {
      return key === '' ? UserModel.fromJSON(value) : value;
    }
  
    public toJSON(): ICookiesConsentModel {
  
      return Object.assign({}, this, {
        consentUseLocalStore: this.consentUseLocalStore && this.consentUseLocalStore === true ? 1 : 0,
        consentUseStatistics: this.consentUseStatistics && this.consentUseStatistics === true ? 1 : 0,
        consentAdvertisement: this.consentAdvertisement && this.consentAdvertisement === true ? 1 : 0,
      });
    }
  
  }
  
  export interface ICookiesConsentModel {
    id?: number;
    ipaddress?: string;
    userId?: number;
    consentUseLocalStore: number;
    consentUseStatistics: number;
    adate?: string;
    udate?: string;
  
    userName?: string;
    email?: string;
  }
  
  export class CookiesConsentData {
    public consent?: ICookiesConsentModel;
  }
  
  export class ResponseGetCookiesConsentModel extends ResponseModel {
    data?: CookiesConsentData;
  }
  
  export class GetCookiesConsentTableCriteria extends TableCriteriaBase {
  }
  
  export class GetChapterTableData extends TableResponseBase {
    consents?: ICookiesConsentModel[];
  }
  
  export class GetChapterTableResponce extends ResponseModel {
    data?: GetChapterTableData;
  }
  
  
  //#endregion

