import { HttpClient, HttpRequest, HttpParams, HttpResponse, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { Observable, of, Subscription, throwError } from 'rxjs';


// Services
import { ApiBaseClient } from '../../api-base/api-base.client';
import { CurrentUserService } from '../current-user.service';
import { TokenService } from '../token.service';
// Models
import { UserModel, ResponseAuthenticate, LatLng, ResponseUserSave } from './dto';
import { environment } from 'src/environments/environment';
import { catchError, map, take, tap } from 'rxjs/operators';
import { LocalUserSettings, ResponseLocalUserSettings } from 'src/app/@core/models/app/local-user-settings';
import { CompanyModel, CompanyProductModel } from 'src/app/@core/models/common/sx-company.model';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { LIVE_ANNOUNCER_ELEMENT_TOKEN_FACTORY } from '@angular/cdk/a11y';
import { AmsAirline } from '../../api/airline/dto';
import { AmsAirport } from '../../api/airport/dto';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class ApiAuthClient extends ApiBaseClient {

  constructor(
    private router: Router,
    private client: HttpClient,
    private tokenService: TokenService,
    private cus: CurrentUserService) {
    super();

  }

  private get baseUrl(): string {
    return environment.services.url.auth;
  }

  //#region JWT Tocken

  autenticate(name: string, pwd: string, latlon: LatLng = undefined): Observable<any> {
    // console.log('user: ' + name + ', ped: ' + pwd);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/login',
      { username: name, password: pwd, location: latlon },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((responce: HttpResponse<any>) => {
          const resp: ResponseAuthenticate = Object.assign(new ResponseAuthenticate(), responce.body);
          //console.log('resp:', resp);
          //console.log('success:', resp.success);
          if (resp.success) {
            return this.convertAutenticateToUser(responce);
          }
          else {
            return resp;
          }
        }),
        catchError(this.handleError)
      );
  }

  convertAutenticateToUser(response: HttpResponse<any>): ResponseAuthenticate {
    let userObj;
    let userSettings: LocalUserSettings;
    let airline: AmsAirline;
    let hqAirport: AmsAirport;
    let token;
    let rv: ResponseAuthenticate;
    let products = new Array<CompanyProductModel>();
    // console.log('response:', response);
    if (response) {

      if (response.headers) {
        token = response.headers.get('X-Authorization');
      }

      if (response.body) {
        rv = Object.assign(new ResponseAuthenticate(), response.body);
        if (rv && rv.success) {

          if (rv.data && rv.data.user) {
            userObj = UserModel.fromJSON(rv.data.user);
          }

          if (rv.data.settings) {
            userSettings = LocalUserSettings.fromJSON(rv.data.settings);
          }

          if (rv.data && rv.data.airline) {
            airline = AmsAirline.fromJSON(rv.data.airline);
          }

          if (rv.data && rv.data.hqAirport) {
            hqAirport = AmsAirport.fromJSON(rv.data.hqAirport);
          }

        }
      }


      if (token) {
        if (rv && rv.success) {
          this.tokenService.barerToken = token;
          this.cus.user = userObj;
          this.cus.localSettings = userSettings;
          this.cus.airline = airline;
          this.cus.hqAirport = hqAirport;
          console.log('decodeToken:', this.tokenService.decodeToken());
        }

      }

    }

    // console.dir(retvalue);
    return rv;
  }

  refreshToken(): Observable<any> {

    //console.log('refreshToken -> ');
    //console.log('refreshToken -> barerToken:', this.tokenService.barerToken);
    //console.log('refreshToken -> refreshToken:', this.tokenService.refreshToken);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + 'jwtrefresh',
      { refresh: this.tokenService.refreshToken },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          const ra: ResponseAuthenticate = this.convertAutenticateToUser(response);
          this.getUserSettingsP().then(res => {
            return this.tokenService.barerToken;
          });
          //console.log('refreshToken -> ResponseAuthenticate:', ra);
          //console.log('refreshToken -> barerToken:', this.tokenService.barerToken);

        }),
        catchError(this.handleError)
      );
  }

  logout(): Observable<any> {
    console.log('logout -> ');
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/authenticate',
      { provider: 'logout' },
      { headers: hdrs, observe: 'response' })
      .pipe(
        map((response: HttpResponse<any>) => {
          console.log('logout -> responce=', response);
          this.tokenService.clearToken();
          this.cus.user = undefined;
          this.cus.clearLocalStorage();
          this.cus.clearSession();
          this.router.navigate(['/', AppRoutes.public]);
          return of(response);
        }),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
  }

  register(mail: string, pwd: string, latlon: LatLng = undefined): Observable<any> {
    console.log('e-mail: ' + mail + ', psw: ' + pwd);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.client.post<HttpResponse<any>>(
      this.baseUrl + '/register',
      { email: mail, password: pwd, location: latlon },
      { headers: hdrs, observe: 'response' })
      .pipe(
        take(1),
        map((responce: HttpResponse<any>) => {
          if (responce) {
            const resp: ResponseAuthenticate = Object.assign(new ResponseAuthenticate(), responce.body);
            //console.log('resp:', resp);
            //console.log('success:', resp.success);
            if (resp.success) {
              return this.convertAutenticateToUser(responce);
            }
            else {
              return resp;
            }
          }

        }),
        /*
        tap((res: HttpResponse<any>) => {
          console.log(res.headers.keys);
        }),*/
        catchError(this.handleError)
      );
    /*
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
      */
  }

  userSave(value: UserModel, location: LatLng = undefined): Observable<UserModel> {

    if (!location) {
      location = new LatLng(-1, -1, -1);
    }
    location.action = "Updated from " + this.cus.user.userId.toString();

    value.ipAddress = JSON.stringify(location);
    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.baseUrl + 'user_save',
      { user: value },
      { headers: hdrs }).pipe(
        map((response: ResponseUserSave) => {
          const res = Object.assign(new ResponseUserSave(), response);
          let rv: UserModel;
          if (res && res.success) {
            if (res && res.data && res.data.user) {
              rv = UserModel.fromJSON(res.data.user);
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap Project save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }

  getUserSettings(value: number): Observable<LocalUserSettings> {

    const hdrs = new HttpHeaders({ 'Content-Type': 'application/json' }); //
    return this.client.post(this.baseUrl + 'user_settings',
      { id: value },
      { headers: hdrs }).pipe(
        map((response: ResponseLocalUserSettings) => {
          const res = Object.assign(new ResponseLocalUserSettings(), response);
          let rv: LocalUserSettings;
          if (res && res.success) {
            if (res && res.data && res.data.settings) {
              rv = LocalUserSettings.fromJSON(res.data.settings);
              if (value == this.cus.user.userId) {
                this.cus.localSettings = rv;
              }
            }
          }
          return rv;
        }),
        tap(event => {
          this.log(`tap Project save event: ` + JSON.stringify(event));
          // There may be other events besides the response.
          if (event instanceof HttpResponse) {
            // cache.put(req, event); // Update the cache.
          }
        }),
        catchError(this.handleError)
      );



  }


  getUserSettingsP(): Promise<LocalUserSettings> {
    let promise = new Promise<LocalUserSettings>((resolve, reject) => {
      const req$ = this.getUserSettings(this.cus.user?.userId);
      req$.subscribe((data: LocalUserSettings) => {
        resolve(data);
      });
    });

    return promise;
  }

  //#endregion


}
