import { Injectable, Inject } from '@angular/core';
import { AppStore, SessionStore } from '../../const/app-storage.const';

@Injectable({providedIn:'root'})
export class TokenFactory {

    // public headerName: string =  'Authorization';
    // public authScheme: string = 'Bearer ';
    // eslint-disable-next-line @typescript-eslint/no-inferrable-types
    public skipWhenExpired: boolean = false;
    public  throwNoTokenError = false;
    public allowedDomains : string[] = [
        'ams.ws.iordanov.info',
        'ams.iordanov.info',
        'sx.ws.iordanov.info',
        'sx.iordanov.info',
        'common.ams.iord',
        'localhost:4306',
        'maps.googleapis.com',
    ];

    // constructor(@Inject(localStorage) protected localStorage: any) { }
    constructor() { }
    public tokenGetter = () => {
        const token = sessionStorage.getItem(SessionStore.BarerToken);
        // console.log('TokenFactory.tokenGetter token:' + token);
        return token;
    }
}
