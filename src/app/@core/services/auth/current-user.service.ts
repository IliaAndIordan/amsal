import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, of, Subject, interval, Subscription } from 'rxjs';
// --- Services
// Models
import { UserModel, LatLng, CookiesConsentModel } from './api/dto';
import { TokenService } from './token.service';
import { AppStore, SessionStore, URL_NO_IMG_SQ } from '../../const/app-storage.const';
import { UserRole } from '../../models/pipes/sx-user-role.pipe';
import { CompanyType } from '../../models/pipes/sx-company-type.pipe';
import { CompanyModel, CompanyProductModel } from '../../models/common/sx-company.model';
import { LocalUserSettings } from '../../models/app/local-user-settings';
import { SxProjectModel } from '../api/project/dto';
import { AmsAirline } from '../api/airline/dto';
import { AmsAirport } from '../api/airport/dto';
import { MatDialog } from '@angular/material/dialog';
import { IInfoMessage, InfoMessageDialog } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { AmsAircraft, AmsMac } from '../api/aircraft/dto';
import { AmsAlFlightNumberSchedule } from '../api/airline/al-flp-number';
import { AmsFlightPlanPayload } from '../api/airline/al-flp-shedule';
import { NgxGravatarService } from 'ngx-gravatar';

@Injectable({ providedIn: 'root' })
export class CurrentUserService {

    currHubAirportSubject = new BehaviorSubject<any>(null);

    spmAircraftPanelOpen = new Subject<number>();
    spmMacPanelOpen = new Subject<AmsMac>();
    spmAirportPanelOpen = new Subject<number>();
    spmAirlinePanelOpen = new Subject<number>();
    spmFlightPanelOpen = new Subject<number>();
    spmFlightQueuePanelOpen = new Subject<number>();
    spmFlpPanelOpen = new Subject<AmsFlightPlanPayload>();

    constructor(
        private dialogService: MatDialog,
        private tockenService: TokenService,
        private gravatarService: NgxGravatarService) {
            this.initTimerOne();
    }

    //#region Timer
    timerMinOne:any;
    timerMinOneSubj = new Subject<number>();

    initTimerOne():void{
        if (this.timerMinOne) {
            clearInterval(this.timerMinOne);
        }
        this.timerMinOne = setInterval(() => {
            console.log('timerMinOne ->');
            new Date().getMinutes()
            this.timerMinOneSubj.next(new Date().getMinutes());
        }, 60_000);
    }
   
    stoTimetOne():void{
        if (this.timerMinOne) { clearInterval(this.timerMinOne); }
        this.timerMinOneSubj.complete();
    }

    //#endregion

    public showMessage(data: IInfoMessage): Promise<any> {
        let promise = new Promise<any>((resolve, reject) => {
            const dialogRef = this.dialogService.open(InfoMessageDialog, {
                width: '721px',
                //height: '320px',
                data: data
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('showMessage-> res:', res);
                if (res) {
                    resolve(res);
                }
            });
        });
        return promise;

    }

    //#region Location Geo

    getLocation(refresh = false): Observable<LatLng> {
        return new Observable(obs => {
            const str = sessionStorage.getItem(AppStore.location);
            if (!str || refresh) {
                navigator.geolocation.getCurrentPosition(
                    pos => {
                        const rv = new LatLng(pos.coords.latitude, pos.coords.longitude);
                        sessionStorage.setItem(AppStore.location, JSON.stringify(rv));
                        obs.next(rv);
                        obs.complete();
                    },
                    error => {
                        console.log('error', error);
                        const rv = new LatLng(42.704198, 23.2840454);
                        obs.next(rv);
                        obs.complete();
                    }
                );
            } else {
                const rv = JSON.parse(str) as LatLng;
                obs.next(rv);
                obs.complete();
            }

        });
    }

    clearLocation() {
        sessionStorage.removeItem(AppStore.location);
    }

    //#endregion

    //#region User

    // eslint-disable-next-line @typescript-eslint/member-ordering
    userChanged = new Subject<UserModel>();

    get user(): UserModel {
        const userStr = sessionStorage.getItem(SessionStore.user);
        let userObj: UserModel;
        // console.log('userStr: ', userStr);
        if (userStr) {
            userObj = Object.assign(new UserModel(), JSON.parse(userStr));
        }
        // console.log('userObj: ', userObj);
        return userObj;
    }

    set user(userObj: UserModel) {
        if (userObj) {
            sessionStorage.setItem(SessionStore.user, JSON.stringify(userObj));
        } else {
            //sessionStorage.removeItem(SessionStore.user);
            //this.clearLocalStorage();
            //this.clearLocation();
        }
        this.userChanged.next(userObj);
    }

    get isLogged(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        retValue = (userObj) ? true : false;
        //retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        if (retValue) {
            // console.log('isLogged user: ', userObj);
        }
        // console.log('isLogged: ' + retValue);
        return retValue;
    }

    get isAdmin(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        // retValue = (userObj && !this.tockenService.isTokenExpired()) ? true : false;
        retValue = (userObj && userObj.userRole === UserRole.Admin);
        // console.log('isAdmin: ' + retValue);
        return retValue;
    }

    get isEditor(): boolean {
        let retValue = false;
        const userObj: UserModel = this.user;
        if (userObj) {
            retValue = (userObj.userRole === UserRole.Editor);
        }
        // console.log('isCompanyAdmin: ' + retValue);
        return retValue;
    }

    get isAuthenticated(): boolean {
        // console.log('isAuthenticated: ');
        return this.isLogged;
    }

    get avatarUrl(): string {
        let retValue = URL_NO_IMG_SQ;
        const userObj: UserModel = this.user;
        if (userObj && userObj.userEmail) {
            const gravatarImgUrl = this.gravatarService.generateGravatarUrl(userObj?.userEmail!, undefined, 250);
            // console.log('gravatarImgUrl', gravatarImgUrl);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    getAvatarUrl(e_mail: string): string {
        let retValue = URL_NO_IMG_SQ;
        if (e_mail) {
            const gravatarImgUrl = this.gravatarService.generateGravatarUrl(e_mail, undefined, 250);
            if (gravatarImgUrl) {
                retValue = gravatarImgUrl;
            }
        }

        return retValue;
    }

    clearLocalStorage() {
        console.log('clearLocalStorage ->');
        Object.keys(AppStore).forEach((key: string) => {
            if (AppStore.hasOwnProperty(key)) {
                const val = (AppStore as any)[key];
                // console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                localStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }

    clearSession() {
        console.log('clearSession ->');
        Object.keys(SessionStore).forEach((key: string) => {
            if (SessionStore.hasOwnProperty(key)) {
                const val = (SessionStore as any)[key];
                // console.log('clearLocalStorage key= ' + key + ', val= ' + val);
                sessionStorage.removeItem(val);
            }
        });
        // console.log('clearLocalStorage <-');
    }

    logout(): Observable<boolean> {
        console.log('cus.logout ->');
        return new Observable<boolean>(subscriber => {
            this.tockenService.clearToken();
            this.clearLocalStorage();
            this.clearSession();
            this.user = undefined;
            subscriber.next(true);
        });

    }
    //#endregion

    //#region Local User Settings

    // eslint-disable-next-line @typescript-eslint/member-ordering
    public LocalSettingsChanged = new Subject<LocalUserSettings>();

    public get localSettings(): LocalUserSettings {
        const userStr = sessionStorage.getItem(AppStore.settings);
        let userObj = null;
        if (userStr) {
            userObj = Object.assign(new LocalUserSettings(), JSON.parse(userStr));
        } else {
            userObj = new LocalUserSettings();
            userObj.snavLeftWidth = 4;
            sessionStorage.setItem(AppStore.settings, JSON.stringify(userObj));
        }
        return userObj;
    }

    public set localSettings(userObj: LocalUserSettings) {
        if (userObj) {
            sessionStorage.setItem(AppStore.settings, JSON.stringify(userObj));
        } else {
            sessionStorage.removeItem(AppStore.settings);
        }
        this.LocalSettingsChanged.next(userObj);
    }

    //#endregion

    //#region Airline

    airlineChanged = new Subject<AmsAirline>();

    get airline(): AmsAirline {
        const onjStr = sessionStorage.getItem(SessionStore.airline);
        let obj: AmsAirline;
        if (onjStr) {
            obj = Object.assign(new AmsAirline(), JSON.parse(onjStr));
        }
        return obj;
    }

    set airline(value: AmsAirline) {
        const oldValue = this.airline;
        if (value) {
            sessionStorage.setItem(SessionStore.airline, JSON.stringify(value));
        } else {
            sessionStorage.removeItem(SessionStore.airline);
        }

        this.airlineChanged.next(value);
    }

    //#endregion

    //#region Airport HQ


    hqAirportChanged = new Subject<AmsAirport>();

    get hqAirport(): AmsAirport {
        const onjStr = sessionStorage.getItem(SessionStore.hqAirport);
        let obj: AmsAirport;
        if (onjStr) {
            obj = Object.assign(new AmsAirport(), JSON.parse(onjStr));
        }
        return obj;
    }

    set hqAirport(value: AmsAirport) {
        const oldValue = this.hqAirport;
        if (value) {
            sessionStorage.setItem(SessionStore.hqAirport, JSON.stringify(value));
        } else {
            sessionStorage.removeItem(SessionStore.hqAirport);
        }

        this.hqAirportChanged.next(value);
    }

    gotoAirportChanged = new Subject<AmsAirport>();

    gotoAirport(value: AmsAirport): void {
        if (value) {
            this.gotoAirportChanged.next(value);
        }
    }

    //#endregion

    //#region  Cookies Consent

    public cookiesConsentChanged = new Subject<CookiesConsentModel | undefined>();

    get cookiesConsent(): CookiesConsentModel | undefined {
        const str = localStorage.getItem(AppStore.CookiesConsent);
        let obj;
        //console.log('userStr: ', userStr);
        if (str !== null) {
            obj = CookiesConsentModel.fromJSON(JSON.parse(str));
        }
        if (!obj) {
            obj = new CookiesConsentModel();
            obj.consentUseLocalStore = true;
            obj.consentUseStatistics = true;
            obj.consentAdvertisement = true;
            obj.adate = new Date();
            this.cookiesConsent = obj;
        }
        //console.log('userObj: ', userObj);
        return obj;
    }

    set cookiesConsent(value: CookiesConsentModel | undefined) {
        if (value) {
            localStorage.setItem(AppStore.CookiesConsent, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.CookiesConsent);
        }
        this.cookiesConsentChanged.next(value);
    }

    //#endregion
}
