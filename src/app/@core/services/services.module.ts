import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerService } from './spinner.service';
import { SxAuthModule } from './auth/auth.module';
import { SxApiProjectClient } from './api/project/api-client';
import { SxApiCompanyClient } from './api/company/api-client';
import { SxAdminUserClient } from './api/admin-user/api-client';
import { AmsCountryClient } from './api/country/api-client';
import { AmsAirportClient } from './api/airport/api-client';
import { AmsAircraftClient } from './api/aircraft/api-client';
import { AmsAirlineClient } from './api/airline/api-client';
import { AmsFlightClient } from './api/flight/ams-flight.client';
import { AmsCommTransactionService } from './common/ams-comm-transaction.service';
import { AmsCountryCacheService } from './api/country/ams-country-cache.service';
import { AmsAirportCacheService } from './api/airport/ams-airport-cache.service';
import { AmsAirlineAircraftCacheService } from './api/aircraft/ams-airline-aircraft-cache.service';
import { AmsTaskClient } from './api/admin/api-client';
import { AmsAirlineHubsCacheService } from './api/airline/al-hub-cache.service';



@NgModule({
  imports: [
    CommonModule,
    SxAuthModule,
  ],
  declarations: [],
  exports: [
    SxAuthModule,
  ],
  providers: [
    SpinnerService,
    SxApiProjectClient,
    SxApiCompanyClient,
    // -Admin
    SxAdminUserClient,
    AmsAircraftClient,
    AmsTaskClient,
    // -WAD
    AmsCountryClient,
    AmsAirportClient,
    AmsAirlineClient,
    // -Flight
    AmsFlightClient,
    //Common
    AmsCommTransactionService,
    AmsCountryCacheService,
    AmsAirportCacheService,
    AmsAirlineAircraftCacheService,
    AmsAirlineHubsCacheService,
  ]
})

export class SxCoreServicesModule { }
