import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ResponseModel } from '../../models/common/responce.model';
import { ApiBaseModule } from './api-base.module';

@Injectable({providedIn:'root'})
export class ApiBaseClient {
    //public toastr:ToastrService;

   constructor() { 
        //this.toastr =  SxAuthModule.injector.get(ToastrService);
    }

    //#region Base

    public handleError(error: HttpErrorResponse) {
        console.log('handleError-> error:', error);
        const toastr = ApiBaseModule.injector.get(ToastrService);
        let msg =  `Backend returned code ${error.status} but error out during data processing. `;
        let title = 'Syntax Error';
        if (error.error instanceof ErrorEvent) {
            // Aclient-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            
           switch(error.status){
               case 200:
                   msg =  `Backend returned code ${error.status} but error out during data processing. `;
                   title = 'Syntax Error';
                   break;
                case 401:
                    const err:ResponseModel = Object.assign(new ResponseModel(),  error.error);
                    msg =  err?err.message: `Backend returned code ${error.status} but error out during data processing. `;
                    title = 'Unauthorized';
                    break;
           }
           console.log('handleError-> msg:', msg);
           console.log('handleError-> title:', title);
           toastr.error(msg, title, {enableHtml:true});
           /*
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error ? error.error.message : error.message}`);
            if (error.error && error.error.statusCode === 401) {
                console.error('handleError -> Go to Logout:');
                return throwError(error);
            }*/
        }

        // Let the app keep running by returning an empty result.
        // return of(result as T);
        return throwError(error);

    }

    /** Log a Service message  */
    protected log(message: string) {
        // console.log('Airport Runway Service', message);
        // this.toastr.info(message, 'WAD Service');
    }

    //#endregion
}
