import { ApplicationRef, Injector, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxAuthModule } from '../auth/auth.module';
import { ToastrService } from 'ngx-toastr';
import { ApiBaseClient } from './api-base.client';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  /*
  providers:[
    ToastrService,
  ]*/
})
export class ApiBaseModule {
  static injector: Injector;

  constructor(injector: Injector,
    applicationRef: ApplicationRef) {
    ApiBaseModule.injector = injector;
  }
}
