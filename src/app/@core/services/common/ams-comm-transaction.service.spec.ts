import { TestBed } from '@angular/core/testing';

import { AmsCommTransactionService } from './ams-comm-transaction.service';

describe('AmsCommTransactionService', () => {
  let service: AmsCommTransactionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AmsCommTransactionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
