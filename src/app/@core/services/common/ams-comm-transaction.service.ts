import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AmsAirlineClient } from '../api/airline/api-client';
import { ResponseBankHistory30Days, BankHistory30Days } from '../api/airline/dto';
import { AmsTransactionsSumLastWeekData, AmsTransactionsSum, AmsTransactionTypeSumData, AmsTransactionTypeSum } from '../api/airline/transaction';
import { AmsAirportClient } from '../api/airport/api-client';
import { AmsFlightClient } from '../api/flight/ams-flight.client';
import { CurrentUserService } from '../auth/current-user.service';

export const SR_SEL_BANK_HIS30 = 'ams_airline_bank_his_30_days';
export const SR_SEL_AL_TR_SUM_LAST_WEEK = 'ams_airline_tr_sum_last_week';
export const SR_SEL_AL_TR_TYPE_SUM_TODAY = 'ams_airline_tr_type_sum_today';

@Injectable({ providedIn: 'root' })
export class AmsCommTransactionService {

  constructor(
    private cus: CurrentUserService,
    private alClient: AmsAirlineClient,
    private apClient: AmsAirportClient,
    private flClient: AmsFlightClient) { 

    }

     //#region BankHistory30Days

     bankHistory30DaysChanged = new BehaviorSubject<ResponseBankHistory30Days>(undefined);
     //bankHistory30DaysData: ResponseBankHistory30Days;
     get bankHistory30DaysData(): ResponseBankHistory30Days {
         let rv: ResponseBankHistory30Days;
         const dataStr = localStorage.getItem(SR_SEL_BANK_HIS30);
         //console.log('selFolderId-> dataStr', dataStr);
         if (dataStr) {
             try {
                 rv = Object.assign(new ResponseBankHistory30Days(), JSON.parse(dataStr));
             }
             catch {
                 localStorage.removeItem(SR_SEL_BANK_HIS30);
                 rv = undefined;
             }
 
         }
         // console.log('selTabIdx-> rv', rv);
         return rv;
     }
 
     set bankHistory30DaysData(value: ResponseBankHistory30Days) {
         localStorage.setItem(SR_SEL_BANK_HIS30, JSON.stringify(value));
         this.bankHistory30DaysChanged.next(value);
     }
 
     loadBankHistoryChanrt(alId: number): Observable<BankHistory30Days[]> {
 
         return new Observable<BankHistory30Days[]>(subscriber => {
 
             this.alClient.airlineBankHistory30Days(alId)
                 .subscribe((resp: ResponseBankHistory30Days) => {
                     this.bankHistory30DaysData = resp;
                     this.bankHistory30DaysChanged.next(this.bankHistory30DaysData);
                     if (resp) {
                         const data = BankHistory30Days.dataToList(resp.data);
                         subscriber.next(data);
                     } else {
                         subscriber.next(undefined);
                     }
                 },
                     err => {
                         console.log('loadBankHistoryChanrt-> err:', err);
                         throw err;
                     });
         });
 
     }
 
     //#endregion
 
 
     //#region AmsTransactionsSumLastWeekData
     private loadingTrSumLastWeekSubject = new BehaviorSubject<boolean>(false);
     loadingTrSumLastWeek$ = this.loadingTrSumLastWeekSubject.asObservable();
     trSumLastWeekChanged = new BehaviorSubject<AmsTransactionsSumLastWeekData>(undefined);
 
     get trSumLastWeek(): AmsTransactionsSumLastWeekData {
         let rv: AmsTransactionsSumLastWeekData;
         const dataStr = localStorage.getItem(SR_SEL_AL_TR_SUM_LAST_WEEK);
         //console.log('selFolderId-> dataStr', dataStr);
         if (dataStr) {
             try {
                 rv = Object.assign(new AmsTransactionsSumLastWeekData(), JSON.parse(dataStr));
             }
             catch {
                 localStorage.removeItem(SR_SEL_AL_TR_SUM_LAST_WEEK);
                 rv = undefined;
             }
 
         }
         // console.log('selTabIdx-> rv', rv);
         return rv;
     }
 
     set trSumLastWeek(value: AmsTransactionsSumLastWeekData) {
         localStorage.setItem(SR_SEL_AL_TR_SUM_LAST_WEEK, JSON.stringify(value));
         this.trSumLastWeekChanged.next(value);
     }
 
     loadTrSumLastWeek(alId: number): Observable<AmsTransactionsSum[]> {
 
         return new Observable<AmsTransactionsSum[]>(subscriber => {
            this.loadingTrSumLastWeekSubject.next(true);
             this.alClient.transactionsSumLastWeek(alId)
                 .subscribe((resp: AmsTransactionsSumLastWeekData) => {
                    this.loadingTrSumLastWeekSubject.next(false);
                     this.trSumLastWeek = resp;
                     this.trSumLastWeekChanged.next(this.trSumLastWeek);
                     if (resp) {
                         const data = AmsTransactionsSum.dataToList(resp);
                         subscriber.next(data);
                     } else {
                         subscriber.next(undefined);
                     }
                 },
                     err => {
                        this.loadingTrSumLastWeekSubject.next(false);
                         console.log('loadBankHistoryChanrt-> err:', err);
                         throw err;
                     });
         });
 
     }
 
     //#endregion
 
     //#region AmsTransactionsSumLastWeekData
 
     trTypeSumTodayChanged = new BehaviorSubject<AmsTransactionTypeSumData>(undefined);
 
     get trTypeSumToday(): AmsTransactionTypeSumData {
         let rv: AmsTransactionTypeSumData;
         const dataStr = localStorage.getItem(SR_SEL_AL_TR_TYPE_SUM_TODAY);
         //console.log('selFolderId-> dataStr', dataStr);
         if (dataStr) {
             try {
                 rv = Object.assign(new AmsTransactionTypeSumData(), JSON.parse(dataStr));
             }
             catch {
                 localStorage.removeItem(SR_SEL_AL_TR_TYPE_SUM_TODAY);
                 rv = undefined;
             }
 
         }
         // console.log('selTabIdx-> rv', rv);
         return rv;
     }
 
     set trTypeSumToday(value: AmsTransactionTypeSumData) {
         localStorage.setItem(SR_SEL_AL_TR_TYPE_SUM_TODAY, JSON.stringify(value));
         this.trTypeSumTodayChanged.next(value);
     }
 
     loadTrTypeSumToday(alId: number): Observable<AmsTransactionTypeSum[]> {
 
         return new Observable<AmsTransactionTypeSum[]>(subscriber => {
 
             this.alClient.transactionsSumToday(alId)
                 .subscribe((resp: AmsTransactionTypeSumData) => {
                     this.trTypeSumToday = resp;
                     this.trTypeSumTodayChanged.next(this.trTypeSumToday);
                     if (resp) {
                         const data = AmsTransactionTypeSum.dataToList(resp);
                         console.log('loadTrTypeSumToday-> data:', data);
                         subscriber.next(data);
                     } else {
                         subscriber.next(undefined);
                     }
                 },
                     err => {
                         console.log('loadTrTypeSumToday-> err:', err);
                         throw err;
                     });
         });
 
     }
 
     //#endregion
 
}
