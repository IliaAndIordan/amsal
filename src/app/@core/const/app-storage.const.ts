import { environment } from 'src/environments/environment';

export const AppStore = {
    airline:'ams_current_airline',
    location: 'ams_current_user_location',
    settings: 'ams_current_user_settings',
    CookiesConsent:'ams_current_user_cookies_consent',
    // Market
    acMarketMac:'ams_ac_market_sel_mac',
    // -Admin
    adminSelUser: 'ams_admin_sel_user',
    adminMfrs: 'ams_admin_mfr_table',
    adminMfr: 'ams_admin_sel_mfr',
    adminMac: 'ams_admin_sel_mac',
    // -WAD
    wadRegion:'ams_wad_region',
    wadSubregion:'ams_wad_subregion',
    wadRegionList:'ams_wad_region_list',
    wadSubregionList:'ams_wad_subregion_list',
    wadCountry:'ams_wad_country',
    wadState:'ams_wad_state',
    wadCity:'ams_wad_city',
    wadAirport:'ams_wad_airport',
    mapAirports:'ams_map_airports',
    mapAirport:'ams_map_airport',
    mapAirportsSpm:'ams_map_airports_spm',
    mapHubsSpm:'ams_map_hubs_spm',
    mapAirportSpm:'ams_map_airport_spm',
    mapFlightsSpm:'ams_map_flights_spm',
    mapRoutesSpm:'ams_map_routes_spm',
    mapFlpnsSpm:'ams_map_flpns_spm',
    mapAlGroups:'ams_map_al_groups',
    mapAlGroup:'ams_map_al_group_selected',
    mapAlWeekday:'ams_map_al_group_selected_weekday',
    // TODO Delete following
};

export const SessionStore = {
    BarerToken: 'ams_barer_token_key',
    RefreshToken: 'ams_refresh_token_key',
    user: 'ams_current_user',
    airline: 'ams_current_airline',
    hqAirport:'ams_current_hq_airport',
}

export const AVATAR_IMG_URL = '/assets/images/common/noimage.png';
export const NO_IMG_URL = '/assets/images/common/noimage.png';
export const MOBILE_MEDIAQUERY = 'screen and (max-width: 599px)';
export const TABLET_MEDIAQUERY = 'screen and (min-width: 600px) and (max-width: 959px)';
export const MONITOR_MEDIAQUERY = 'screen and (min-width: 960px)';

export const URL_COMMON = 'https://common.iordanov.info/';
export const URL_COMMON_IMAGES = URL_COMMON + 'images/';
export const URL_COMMON_IMAGES_COMPANY = URL_COMMON_IMAGES + 'company/';
export const URL_COMMON_IMAGE_AMS = URL_COMMON_IMAGES + 'ams/';
export const URL_COMMON_IMAGE_AMS_COMMON = URL_COMMON_IMAGE_AMS + 'common/';
export const URL_COMMON_IMAGE_TILE = URL_COMMON_IMAGES + 'tile/';
export const URL_COMMON_IMAGE_SX = URL_COMMON_IMAGES + 'sx/';
export const URL_COMMON_IMAGE_FLAGS = URL_COMMON_IMAGES + 'flags/';
export const URL_COMMON_IMAGE_COMMODITY = URL_COMMON_IMAGES + 'commodity/';
export const URL_COMMON_IMAGE_AIRCRAFT= URL_COMMON_IMAGES + 'aircraft/';
export const URL_COMMON_IMAGE_AIRCRAFT_ICON = URL_COMMON_IMAGE_AIRCRAFT + 'icons/';
export const URL_COMMON_IMAGE_AIRLINE= URL_COMMON_IMAGES + 'airline/';

export const COMMON_IMG_LOGO_RED = URL_COMMON_IMAGE_AMS + 'logo_square.png';
export const COMMON_IMG_LOGO_CERCLE = URL_COMMON_IMAGE_SX + 'sx-logo-red-3d.png';
export const COMMON_IMG_AVATAR = URL_COMMON_IMAGES + 'iziordanov_sd_128_bw.png';
export const COMMON_IMG_LOGO_IZIORDAN = URL_COMMON_IMAGES + 'iziordanov_logo.png';

export const GMAP_API_KEY = 'AIzaSyDFFY0r9_fLApc2awmt-O1Q2URpkSFbQVc';

export const URL_NO_IMG = 'assets/images/common/noimage.png';
export const URL_NO_IMG_SQ = 'assets/images/common/noimage-sq.jpg';

export const HOUR_MS = (1 * 60 * 60 * 1000);
export const DAY_MS = (24 * HOUR_MS);

export const PAGE_SIZE_OPTIONS = [12, 25, 50, 100, 200];
export const PAX_WEIGHT_KG = 77;

export const Wide = 'image-wide';
export const Tall = 'image-tall';

export const URL_IMG_IATA = URL_COMMON_IMAGE_AMS_COMMON + 'iata.png';
export const URL_IMG_ICAO = URL_COMMON_IMAGE_AMS_COMMON + 'icao.png';
export const URL_IMG_WIKI = URL_COMMON_IMAGE_AMS_COMMON + 'wikipedia.ico';
export const URL_IMG_OUAP = URL_COMMON_IMAGE_AMS_COMMON + 'ourairports.png';
export const URL_IMG_AP = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';
export const URL_IMG_STORYLINE = URL_COMMON_IMAGES + 'bwt/storyline-white.png';


export const WEB_OURAP_BASE_URL = 'https://ourairports.com/';
export const WEB_OURAP_AP = WEB_OURAP_BASE_URL + 'airports/';
export const WEB_OURAP_REGION = WEB_OURAP_BASE_URL + 'countries/';

export const PayloadTypeFImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_f_2.png';
export const PayloadTypeBImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_b_4.png';
export const PayloadTypeEImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/person_e_3.png';
export const PayloadTypeCImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/cargo.png';
export const PayloadTypeGImgUrl = URL_COMMON_IMAGE_AIRCRAFT + 'cabin/pilot_m_1.png';
export const PayloadTypeCrewImgUrl = URL_COMMON_IMAGE_AMS_COMMON + '/crew_02.png';
