export const Animate = {
    'show': 'show',
    'hide': 'hide',
    'in': 'in',
    'out': 'out',
    'turned': 'turned',
    'normal': 'normal',
    'hover': 'hover',
    'noHover': 'noHover',
    'on': 'on',
    'off': 'off',
    'small': 'small',
    'large': 'large',
    'open': 'open',
    'close': 'close',
};

