
export const PageViewType = {
    Card: 'card',
    List: 'list',
    Table: 'table'
};

export const AppRoutes = {
    Root: '',
    public: 'public',
    Login: 'login',
    // -Baase
    
    // -- Admin
    admin:'admin',
    usersList: 'user-list',
    cfgCharter:'cfg-charter',
    charter:'charter',
    shedule:'shedule',
    booking:'booking',
    // -- Airline
    profile:'profile',
    airline:'airline',
    fleet:'fleet',
    acMarket:'ac-market',
    bank:'bank',
    hubs:'hubs',
    flightPlan:'flight-plan',

    manufacturers:'manufacturers',
    manufacturer:'manufacturer',
    mac:'mac',
    user:'user',
    aircafts:'aircafts',
    aircaft:'aircaft',
     // --WAD
     wad:'wad',
     region:'region',
     subregion:'subregion',
     country:'country',
     state:'state',
     airport:'airport',
     
    // -Common Subroots
    dashboard: 'dashboard',
    info:'info',
    create: 'create',
    
    Users: 'users',
    
    MyCompany: 'my-company',
    
    list:'list',
    ItemsImport: 'items-import',
    ItemsImportResult: 'items-import-result',
    ItemImportFiles: 'item-import-files',
    ItemImportFile: 'item-import-file',
    // ---Company
    Company: 'company',
    CompanyList: 'company-list',
    // -- Projects
    Projects: 'projects',
    ProjectList: 'projects-list',
    Project: 'project',
    // ---Commodity
    Commodity: 'commodity',
    // -- Manufacturer
    Manufacturer: 'manufacturer',
    
    // --Distributor
    Distributor: 'distributor',
    // --
    NotAuthorized: 'not-authorized',
    NoImageAvailable: '//images.tradeservice.com/ProductImages/DIR_TSL/TSL_PRODUCT_NOT_AVAILABLE.png',
    Maintenance: 'under-maintenance',
    SessionSuspended: 'session-suspended'
};
