import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxCoreServicesModule } from './services/services.module';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';



@NgModule({
  imports: [
    CommonModule,
    //
    SxCoreServicesModule,
  ],
  declarations: [],
  exports:[
    SxCoreServicesModule
  ],
  providers:[
    AuthGuard,
    AdminGuard,
  ]
})

export class SxCoreModule { }
