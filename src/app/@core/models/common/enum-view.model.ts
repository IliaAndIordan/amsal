export class EnumViewModel {
    id: number;
    name: string;
    color?:string;
    value?:number;
    timeMin?:number;
    group?:string;
}
