export class TableCriteriaBase {

    public limit: number;
    public offset: number;
    public filter?: string;
    public sortCol?: string;
    public sortDesc?: boolean;

    get pageIndex(): number {
        let rv = 0;
        rv = this.offset ? (Math.floor(this.offset / (this.limit ? this.limit : 25))) : 0;
        return rv;
    }

    set pageIndex(value: number) {
        this.offset = value ? (value) * (this.limit ? this.limit : 25) : 0;
    }

}

export class TableResponseBase {
    rowsCount?: number;
}