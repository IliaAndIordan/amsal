
export class HomePageSettings {
    public panelIn: boolean;

    public static default(): HomePageSettings {
        const rv = new HomePageSettings();
        rv.panelIn = true;
        return rv;
    }
}
