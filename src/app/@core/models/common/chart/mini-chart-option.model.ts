import { ChartOptions } from 'chart.js';

export class MiniChartOptionModel implements ChartOptions {
    responsive = true;
    canvasPadding= 0;
    labelFontSize=15;
    tooltipFormat: any;
    toolTipContent:any;
    tooltipValueLookups: any;
    plotToolText= "Store location: $label <br> Sales (YTD): $dataValue <br> $displayValue";
    width = 88;
    height = 88;
    sliceColors = ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)'];
    offset = 0;
    borderWidth = 0;
    
}
