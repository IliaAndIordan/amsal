import { ChartData, ChartDataset, ChartType } from 'chart.js';
import { AmsMac } from 'src/app/@core/services/api/aircraft/dto';
import { MiniChartOptionModel } from './mini-chart-option.model';

export class PieMiniChartViewModel {
    options: MiniChartOptionModel;
    data: number[];
    labels: Array<string>;
    colors: Array<any>;
    bgm = 'bgm-lightblue';

    public static fromAmsMac(mac: AmsMac): PieMiniChartViewModel {
        const rv = new PieMiniChartViewModel();
        rv.data =  new Array<number>();
        
        const now = new Date();
        const lastProduced = mac.lastProducedOn?new Date(mac.lastProducedOn):new Date();
        
        //console.log('lastProduced: ',  lastProduced);
        const timeMs = now.getTime() - lastProduced.getTime();
        
        const timeLeftH = parseFloat((timeMs/(1000 * 60 * 60)).toFixed());
        //console.log('timeLeftH: ',  timeLeftH);
        const timePassedH = parseFloat((mac.productionRateH - timeLeftH).toFixed());
        //console.log('timePassedH: ',  timePassedH);
        rv.data.push(timePassedH);
        rv.data.push(timeLeftH);
        rv.labels = ['From last', 'To next'];
        rv.colors = [
            { // 1st Year.
                borderColor: 'rgba(255,255,255,0.1)',
                backgroundColor: ['rgba(255,255,255,0.8)', 'rgba(255,255,255,0.2)'],
            },
        ];
        // eslint-disable-next-line no-use-before-define, @typescript-eslint/no-use-before-define
        const options = new MiniChartOptionModel();
        // const offset = ['Active Airports', 'Airports'];
        options.responsive = false;
        const offset = ['From last', 'To next'];
        options.tooltipValueLookups = { 'offset': offset };
        options.tooltipFormat =  '{{offset:offset}} {{value}} - {{percentValue}} %';

        rv.options = options;
        rv.bgm = 'bgm-lightblue';
       
        return rv;
    }

    public static ceratePieChart(data: number[], labels: string[]): PieMiniChartViewModel {
        const rv = new PieMiniChartViewModel();
        rv.data = data;
        rv.labels = labels;
        rv.colors = [
            {
                borderColor: 'rgba(255,255,255,0.1)',
                backgroundColor: ['rgba(255,255,255,0.8)', 'rgba(255,255,255,0.5)','rgba(255,255,255,0.2)'],
            }
        ];
        const options = new MiniChartOptionModel();
        const offset = labels;
        options.tooltipValueLookups = { 'offset': offset };
        options.tooltipFormat =  '{{offset:offset}}: {{value}}';
        rv.options = options;
        rv.bgm = 'bgm-lightblue';
        return rv;
    }


    

}

