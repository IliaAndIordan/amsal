import { Pipe, PipeTransform } from '@angular/core';

export enum SxProduct {
    SubmittalManager = 1,
    SupplierExchange = 2,
    ProjectStoryManager = 3,
}

export class SxProductViewModel {
    public id: SxProduct;
    public name: string;
    public abreviation: string;
    public isGranted: boolean;
}


export const ProductOpt: SxProductViewModel[] = [
    {
        id: SxProduct.SubmittalManager,
        name: 'Submittal Manager',
        abreviation: 'SM',
        isGranted: false,
    },
    {
        id: SxProduct.SupplierExchange,
        name: 'Supplier Exchange',
        abreviation: 'SX',
        isGranted: false,
    },
    {
        id: SxProduct.ProjectStoryManager,
        name: 'Project Story Manager',
        abreviation: 'PSM',
        isGranted: false,
    },
];


@Pipe({ name: 'sxproductname' })
export class SxProductNamePipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = SxProduct[value];
        const data: SxProductViewModel = ProductOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}



