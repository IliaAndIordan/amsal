import { Pipe, PipeTransform } from "@angular/core";
import { EnumViewModel } from "../common/enum-view.model";

export enum AmsBankTransactionType {
    AirlineCreate = 1,
    AircraftPurchase = 2,
    Boarding = 3,
    CargoLoad = 4,
    Disembarking = 5,
    CargoUnload = 6,
    InsuranceWeek = 7,
    LandingAndTaxyToGate = 8,
    Maintenance = 9,
    Refuling = 10,
    TaxAirportWeek = 11,
    TaxyAndTakeoff = 12,
    FlightCrew = 13,
    FlightIncome = 14,
    GovernmentBackedSupport = 15,

}


export const AmsBankTransactionTypeOpt: EnumViewModel[] = [
    {
        id: 1,
        name: 'Airline Create'
    },
    {
        id: 2,
        name: 'Aircraft Purchase'
    },
    {
        id: 3,
        name: 'Boarding'
    },
    {
        id: 4,
        name: 'Cargo Load'
    },
    {
        id: 5,
        name: 'Disembarking'
    },
    {
        id: 6,
        name: 'Cargo Unload'
    },
    {
        id: 7,
        name: 'Insurance Week'
    },
    {
        id: 8,
        name: 'Landing and Taxy to Gate'
    },
    {
        id: 9,
        name: 'Maintenance'
    },
    {
        id: 10,
        name: 'Refuling'
    },
    {
        id: 11,
        name: 'Tax Airport Week'
    },
    {
        id: 12,
        name: 'Taxy and Takeoff'
    },
    {
        id: 13,
        name: 'Flight Crew'
    },
    {
        id: 14,
        name: 'Flight Income'
    },
    {
        id: 15,
        name: 'Government-Backed Support'
    },

];


@Pipe({ name: 'transtype' })
export class AmsBankTransactionTypeDisplayPipe implements PipeTransform {
    transform(value, args?: string[]): string {
        let rv: string = AmsBankTransactionType[value];
        const data: EnumViewModel = AmsBankTransactionTypeOpt.find(x => x.id === value);
        if (data) {
            rv = data.name;
        }
        return rv;
    }
}
