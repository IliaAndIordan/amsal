import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AppStore } from '../../const/app-storage.const';
import { LocalUserSettings } from '../app/local-user-settings';

@Pipe({ name: 'periodmin' })
export class PeriodMinPipe implements PipeTransform {


    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {
        if (isNaN(value)) { return undefined; }
        if (value === null) { return undefined; }
        if (value === 0) { return undefined; }

        const sec_num = Math.abs(value) * 60;
        const hours = Math.floor(sec_num / 3600);
        const minutes = Math.floor(sec_num / 60) % 60;
        const seconds = sec_num % 60;
        /*
        const rv = [hours,minutes,seconds]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v,i) => v !== "00" || i > 0)
            .join(":");
            */
        let rv = [0, hours, minutes]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v, i) => v !== "00" || i > 0)
            .join(":");

        if (value < 0) {
            rv = '- ' + rv;
        }
        return rv;
    }
}

@Pipe({ name: 'periodms' })
export class PeriodMsPipe implements PipeTransform {


    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {
        if (isNaN(value)) { return undefined; }
        if (value == null) { return undefined; }
        if (value === 0) { return undefined; }

        const duration = Math.abs(value);

        const milliseconds = Math.floor((duration % 1000) / 100);
        const seconds = Math.floor((duration / 1000) % 60);
        const minutes = Math.floor((duration / (1000 * 60)) % 60);
        const hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        let rv: string = '';
        rv += hours && hours > 0 ? hours.toString().padStart(2, '0') + ":" : '';
        rv += minutes && minutes > 0 ? minutes.toString().padStart(2, '0') + ":" : '';
        rv += seconds.toString().padStart(2, '0') + "." + milliseconds.toString().padStart(3, '0');

        return rv;
    }
}

@Pipe({ name: 'nocoma' })
export class NoComaPipe implements PipeTransform {


    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: string): string {
        if (value !== undefined && value !== null) {
            return value.replace(/,/g, '');
          } else {
            return '';
          }
    }
}


