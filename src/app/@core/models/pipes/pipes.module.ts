import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyStatusDisplayPipe, CompanyTypeDisplayPipe } from './sx-company-type.pipe';
import { SxProductNamePipe } from './sx-product.pipe';
import { UserRoleDisplayPipe } from './sx-user-role.pipe';
import { SxProjectStatusDisplayPipe } from '../../services/api/project/enums';
import { TsDatePipe } from './ts-date.pipe';
import { BooleanPipe } from './boolean.pipe';
import { LatLonPipe } from './latlon.pipe';
import { DistanseNmiPipe, LengthKmPipe, LengthPipe, SpeedPipe } from './length.pipe';
import { RsRunwaySurfacePipe, RsRunwayTypePipe } from './rs-runway.pipe';
import { MtowPipe } from './mtow.pipe';
import { UtcPipe } from './utc.pipe';
import { AmsStatusDisplayPipe, WadStatusDisplayPipe } from './ams-status.enums';
import { ApTypeDisplayPipe, RwSerfaceDisplayPipe, RwTypeDisplayPipe } from './ap-type.pipe';
import { AcTypeDisplayPipe } from './ac-type.pipe';
import { AmsAcSeatTypeDisplayPipe, AmsAcSeatTypeImageDisplayPipe } from './ac-seat-type.pipe';
import { AmsAcIfeTypeDescrDisplayPipe, AmsAcIfeTypeDisplayPipe } from './ac-ife-type.pipe';
import { TruncatePipe } from './truncate.pipe';
import { AcStatusDisplayPipe, AircraftActionDisplayPipe } from './aircraft-statis.pipe';
import { NoComaPipe, PeriodMinPipe, PeriodMsPipe } from './interval.pipe';
import { LitersPipe, LpkmPipe } from './lpkm.pipe';
import { AmsFlightStatusDisplayPipe, AmsFlightStatusMonitorPipe, AmsFlightTypeDisplayPipe, AmsPayloadTypeDisplayPipe } from '../../services/api/flight/enums';
import { AmsMaintenanceTypesDisplayPipe } from '../../services/api/aircraft/enums';
import { AmsFlpStatusDisplayPipe, AmsHubTypeDisplayPipe, AmsTransactionTypeDisplayPipe, AmsWeekdaysAbbrDisplayPipe, AmsWeekdaysDisplayPipe } from '../../services/api/airline/enums';
import { AmsTaskTypeDisplayPipe } from '../../services/api/admin/enums';



@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SxProductNamePipe,
        UserRoleDisplayPipe,
        SxProjectStatusDisplayPipe,
        CompanyTypeDisplayPipe,
        SxProjectStatusDisplayPipe,
        TsDatePipe,
        BooleanPipe,
        LatLonPipe,
        LengthPipe,
        LengthKmPipe,
        DistanseNmiPipe,
        RsRunwaySurfacePipe,
        RsRunwayTypePipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        ApTypeDisplayPipe,
        RwTypeDisplayPipe,
        RwSerfaceDisplayPipe,
        AcTypeDisplayPipe,
        AmsAcSeatTypeImageDisplayPipe,
        AmsAcSeatTypeDisplayPipe,
        AmsAcIfeTypeDisplayPipe,
        AmsAcIfeTypeDescrDisplayPipe,
        TruncatePipe,
        AcStatusDisplayPipe,
        SpeedPipe,
        PeriodMinPipe,
        PeriodMsPipe,
        LpkmPipe,
        LitersPipe,
        AircraftActionDisplayPipe,
        AmsFlightTypeDisplayPipe,
        AmsPayloadTypeDisplayPipe,
        AmsFlightStatusDisplayPipe,
        AmsFlightStatusMonitorPipe,
        AmsMaintenanceTypesDisplayPipe,
        AmsHubTypeDisplayPipe,
        NoComaPipe,
        AmsWeekdaysAbbrDisplayPipe,
        AmsWeekdaysDisplayPipe,
        AmsFlpStatusDisplayPipe,
        AmsTransactionTypeDisplayPipe,
        AmsTaskTypeDisplayPipe,
    ],
    exports:[
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SxProductNamePipe,
        UserRoleDisplayPipe,
        SxProjectStatusDisplayPipe,
        SxProjectStatusDisplayPipe,
        TsDatePipe,
        BooleanPipe,
        LatLonPipe,
        LengthPipe,
        LengthKmPipe,
        DistanseNmiPipe,
        RsRunwaySurfacePipe,
        RsRunwayTypePipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        ApTypeDisplayPipe,
        RwTypeDisplayPipe,
        RwSerfaceDisplayPipe,
        AcTypeDisplayPipe,
        AmsAcSeatTypeImageDisplayPipe,
        AmsAcSeatTypeDisplayPipe,
        AmsAcIfeTypeDisplayPipe,
        AmsAcIfeTypeDescrDisplayPipe,
        TruncatePipe,
        AcStatusDisplayPipe,
        SpeedPipe,
        PeriodMinPipe,
        PeriodMsPipe,
        LpkmPipe,
        LitersPipe,
        AircraftActionDisplayPipe,
        AmsFlightTypeDisplayPipe,
        AmsPayloadTypeDisplayPipe,
        AmsFlightStatusDisplayPipe,
        AmsFlightStatusMonitorPipe,
        AmsMaintenanceTypesDisplayPipe,
        AmsHubTypeDisplayPipe,
        NoComaPipe,
        AmsWeekdaysAbbrDisplayPipe,
        AmsWeekdaysDisplayPipe,
        AmsFlpStatusDisplayPipe,
        AmsTransactionTypeDisplayPipe,
        AmsTaskTypeDisplayPipe,
    ],
    providers: [
        CompanyTypeDisplayPipe,
        CompanyStatusDisplayPipe,
        SxProductNamePipe,
        UserRoleDisplayPipe,
        SxProjectStatusDisplayPipe,
        SxProjectStatusDisplayPipe,
        TsDatePipe,
        BooleanPipe,
        LatLonPipe,
        LengthPipe,
        LengthKmPipe,
        DistanseNmiPipe,
        RsRunwaySurfacePipe,
        RsRunwayTypePipe,
        MtowPipe,
        UtcPipe,
        WadStatusDisplayPipe,
        AmsStatusDisplayPipe,
        ApTypeDisplayPipe,
        RwTypeDisplayPipe,
        RwSerfaceDisplayPipe,
        AcTypeDisplayPipe,
        AmsAcSeatTypeImageDisplayPipe,
        AmsAcSeatTypeDisplayPipe,
        AmsAcIfeTypeDisplayPipe,
        AmsAcIfeTypeDescrDisplayPipe,
        TruncatePipe,
        AcStatusDisplayPipe,
        SpeedPipe,
        PeriodMinPipe,
        PeriodMsPipe,
        LpkmPipe,
        LitersPipe,
        AircraftActionDisplayPipe,
        AmsFlightTypeDisplayPipe,
        AmsPayloadTypeDisplayPipe,
        AmsFlightStatusDisplayPipe,
        AmsFlightStatusMonitorPipe,
        AmsMaintenanceTypesDisplayPipe,
        AmsHubTypeDisplayPipe,
        AmsWeekdaysAbbrDisplayPipe,
        AmsWeekdaysDisplayPipe,
        AmsFlpStatusDisplayPipe,
        AmsTransactionTypeDisplayPipe,
        AmsTaskTypeDisplayPipe,
    ]
})
export class SxPipesModule { }
