import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';


@Pipe({
    name: 'utc'
})
export class UtcPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return null; }
        if (value === null) { return null; }
        if (value === 0) { return null; }

        const label = 'h';
        const num = value ;
        const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return rv;
    }
}
