import { PipeTransform, Pipe } from '@angular/core';
import { EnumViewModel } from '../common/enum-view.model';

export const RsRunwaySurfaceOptions: EnumViewModel[] = [

    {
        id: 1,
        name: 'Water Runway'
    },
    {
        id: 2,
        name: 'Snow or Ice'
    },
    {
        id: 3,
        name: 'Turf or Grass'
    },
    {
        id: 4,
        name: 'Dirt or Gravel'
    },
    {
        id: 5,
        name: 'Concrete'
    },
    {
        id: 6,
        name: 'Asphalt'
    },
];

@Pipe({ name: 'runwaysurface' })
export class RsRunwaySurfacePipe implements PipeTransform {
    transform(value, args: string[]): any {

        if (isNaN(value)) { return undefined; }
        if (value === null) { return undefined; }
        if (value === 0) { return undefined; }

        const label = RsRunwaySurfaceOptions.find(x => x.id === value).name;

        // const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return label;
    }
}


export const RsRunwayTypeOptions: EnumViewModel[] = [

    {
        id: 1,
        name: 'Helipad'
    },
    {
        id: 2,
        name: 'Runway'
    },
];

@Pipe({
    name: 'runwaytype'
})
export class RsRunwayTypePipe implements PipeTransform {
    transform(value, args: string[]): any {
        if (isNaN(value)) { return undefined; }
        if (value === null) { return undefined; }
        if (value === 0) { return undefined; }

        const label = RsRunwayTypeOptions.find(x => x.id === value).name;

        // const rv = new DecimalPipe(this.locale).transform(num, '1.0-0') + ' ' + label;
        return label;
    }
}

