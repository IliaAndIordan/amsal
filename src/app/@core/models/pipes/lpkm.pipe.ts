import { Pipe, PipeTransform, LOCALE_ID, Inject } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { AppStore } from '../../const/app-storage.const';
import { LocalUserSettings } from '../app/local-user-settings';

@Pipe({name: 'lpkm'})
export class LpkmPipe implements PipeTransform {


    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return undefined; }
        if (value === null) { return undefined; }
        if (value === 0) { return undefined; }

        const argStr = sessionStorage.getItem(AppStore.settings);
        let lpkmToMpg = 1;
        let label = 'l/100 km';
       
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            if (settings) {
                lpkmToMpg = settings.displayWeightLb ? (2.3521/100) : 1;
                label = settings.displayWeightLb ? 'mpg' : 'l/100 km';
            }
        }else{
            console.log('LpkmPipe -> AppStore.settings is missing:');
        }
        const num = value * lpkmToMpg;

        const rv = new DecimalPipe(this.locale).transform(num, '1.2-2') + ' ' + label;
        return rv;

    }
}

@Pipe({name: 'liters'})
export class LitersPipe implements PipeTransform {


    constructor(@Inject(LOCALE_ID) private locale: string) {
    }

    transform(value: number): string {

        if (isNaN(value)) { return undefined; }
        if (value === null) { return undefined; }
        if (value === 0) { return undefined; }

        const argStr = sessionStorage.getItem(AppStore.settings);
        let lToGal = 1;
        let label = 'L';
       
        if (argStr) {
            const settings = Object.assign(new LocalUserSettings(), JSON.parse(argStr));
            //console.log('LitersPipe -> settings:', settings);
            if (settings) {
                lToGal = settings.displayWeightLb ? (0.264172052) : 1;
                label = settings.displayWeightLb ? 'gal' : 'L';
            }
        } else{
            console.log('LitersPipe -> AppStore.settings is missing:');
        }
        const num = value * lToGal;

        const rv = new DecimalPipe(this.locale).transform(num, '1.2-2') + ' ' + label;
        return rv;

    }
}
