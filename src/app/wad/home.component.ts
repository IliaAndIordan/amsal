import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// --- Services
// --- Animations
import { Animate } from '../@core/const/animation.const';
// --- Models
import { AppRoutes } from '../@core/const/app-routes.const';
import { COMMON_IMG_LOGO_RED } from '../@core/const/app-storage.const';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from '../@core/const/animations-triggers';
import { MatDialog } from '@angular/material/dialog';
import { AmsWadService } from './wad.service';
import { Subscription } from 'rxjs';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from '../@core/services/api/country/dto';
import { AmsSidePanelModalComponent } from '../@share/components/common/side-panel-modal/side-panel-modal';
import { AmsAirport } from '../@core/services/api/airport/dto';
import { SxSidebarNavComponent } from '../@share/components/common/sidebar/nav/sx-sidebar-nav.component';
import { MatDrawerMode } from '@angular/material/sidenav';
import { MapLeafletSpmService } from '../@share/maps/map-liflet-spm.service';
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { AmsAirportCacheService } from '../@core/services/api/airport/ams-airport-cache.service';
import { SpinnerService } from '../@core/services/spinner.service';
// import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './home.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})

export class AmsWadHomeComponent implements OnInit, OnDestroy {

  @ViewChild('spmRegion') spmRegion: AmsSidePanelModalComponent;
  @ViewChild('spmSubregion') spmSubregion: AmsSidePanelModalComponent;
  @ViewChild('spmState') spmState: AmsSidePanelModalComponent;
  @ViewChild('spmCountry') spmCountry: AmsSidePanelModalComponent;
  @ViewChild('sxSide') sxSide: SxSidebarNavComponent;

  sideNavState: string = Animate.open;
  snavmode: MatDrawerMode = 'side';

  regionPanelOpen: Subscription;
  subregionPanelOpen: Subscription;
  statePanelOpen: Subscription;
  countryPanelOpen: Subscription;

  region: AmsRegion;
  subregion: AmsSubregion;
  country: AmsCountry;
  state: AmsState;
  stateChanged: Subscription;
  airport: AmsAirport;


  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';
  // --- Side Tab
  expandTabVar: string = Animate.in;
  showTabContentsVar: string = Animate.hide;
  total: number = 0;
  aggressiveTotal: number = 0;

  // public dialogService: MatDialog
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: SpinnerService,
    private cus: CurrentUserService,
    private apCache: AmsAirportCacheService,
    public dialogService: MatDialog,
    private wadService: AmsWadService) { }


  ngOnInit(): void {

    this.regionPanelOpen = this.wadService.regionPanelOpen.subscribe((panelIn: boolean) => {
      this.spmRegionOpen();
    });

    this.subregionPanelOpen = this.wadService.subregionPanelOpen.subscribe((panelIn: boolean) => {
      //console.log('HomeComponent-> subregionPanelOpen:', panelIn);
      this.spmSubregionOpen();
    });

    this.statePanelOpen = this.wadService.statePanelOpen.subscribe((panelIn: boolean) => {
      //console.log('HomeComponent-> statePanelOpen:', panelIn);
      this.spmStateOpen();
    });

    this.countryPanelOpen = this.wadService.countryPanelOpen.subscribe((country: AmsCountry) => {
      //console.log('HomeComponent-> statePanelOpen:', panelIn);
      this.spmCountryOpen(country);
    });
  }

  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.regionPanelOpen) { this.regionPanelOpen.unsubscribe(); }
    if (this.subregionPanelOpen) { this.subregionPanelOpen.unsubscribe(); }
    if (this.statePanelOpen) { this.statePanelOpen.unsubscribe(); }
    if (this.statePanelOpen) { this.statePanelOpen.unsubscribe(); }
  }

  initFields() {
    this.region = this.wadService.region;
    this.subregion = this.wadService.subregion;
    this.country = this.wadService.country;
    this.state = this.wadService.state;

    //console.log('initFields-> country:', this.country);
  }


  exapandTabClick() {
    this.expandTabVar = this.expandTabVar === Animate.in ? Animate.out : Animate.in;
    this.showTabContentsVar = this.showTabContentsVar === Animate.show ? Animate.hide : Animate.show;
  }

  public loginDialogShow(): void {
    /*
    const dialogRef = this.dialogService.open(LoginModal, {
      width: '500px',
      height: '340px',
      data: { email: null, password: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
      // this.password = result.password;
    });
    */
  }

  // sidebar-closed
  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }

  //#region SPM Region

  spmRegionClose(): void {
    this.spmRegion.closePanel();
  }

  spmRegionOpen() {
    this.region = this.wadService.region;
    if (this.region && this.spmRegion) {
      this.spmRegion.expandPanel();
    }

  }

  spmSubregionClose(): void {
    this.spmSubregion.closePanel();
  }

  spmSubregionOpen() {
    // console.log('spmSubregionOpen ->');
    this.subregion = this.wadService.subregion;
    // console.log('spmSubregionOpen -> spmSubregion', this.spmSubregion);
    if (this.subregion && this.spmSubregion) {
      this.spmSubregion.expandPanel();
    }
  }

  spmStateClose(): void {
    // console.log('spmStateClose ->');
    this.spmState.closePanel();
  }

  spmStateOpen() {
    //console.log('spmStateOpen ->');
    this.state = this.wadService.state;
    this.subregion = this.wadService.subregion;
    // console.log('spmStateOpen -> spmState', this.spmState);
    if (this.state && this.spmState) {
      this.spmState.expandPanel();
    }
  }

  spmCountryClose(): void {
    if (this.spmCountry) {
      this.spmCountry.closePanel();
    }
  }

  spmCountryOpen(value: AmsCountry) {
    this.initFields();
    this.country = value;
    // console.log('spmStateOpen -> spmState', this.spmState);
    if (this.country && this.spmCountry) {
      this.spmCountry.expandPanel();
    }
    else {
      this.spmCountryClose();
    }
  }
  
  //#endregion


  //#region  Expand/Close Menu


  snavToggleClick() {
    console.log('snavToggleClick-> sxSide', this.sxSide);
    if (this.sxSide) {
      this.sxSide.toggle();
    }
  }

  //#endregion


}
