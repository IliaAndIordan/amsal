import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from 'src/app/wad/wad.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { PageViewType } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsCountry, AmsRegion, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsWadCountryService } from '../country.service';

export const PAGE_VIEW_TYPE = 'sx_project_usecase_pageview';

@Component({
    selector: 'ams-wad-country-home-tab-state',
    templateUrl: './country-home-tab-state.component.html',
})
export class AmsWadCountryHomeTabStateComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    //@Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    //@Output() editUseCase: EventEmitter<SxUseCaseModel> = new EventEmitter<SxUseCaseModel>();
    @Input() tabIdx: number;
    /**
     * Fields
     */
    region: AmsRegion;
    subregion:AmsSubregion;
    subregionChanged: Subscription;
    country: AmsCountry;
    countryChanged: Subscription;

    selTabIdx: number;
    tabIdxChanged: Subscription;

    totalCount: number;
    pvtOpt = PageViewType;
    showCardVar: string = Animate.show;
    showListVar: string = Animate.hide;
    showTableVar: string = Animate.hide;



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService:AmsWadService,
        private cService: AmsWadCountryService) {

    }


    ngOnInit(): void {
       
        this.totalCount = 0;
        this.subregionChanged = this.wadService.subregionChanged.subscribe((subregion: AmsSubregion) => {
            this.initFields();
        });

        this.countryChanged = this.wadService.countryChanged.subscribe((country: AmsCountry) => {
            this.initFields();
        });


        this.tabIdxChanged = this.cService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.subregionChanged) { this.subregionChanged.unsubscribe(); }
        if (this.countryChanged) { this.countryChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.cService.tabIdx;
        this.region = this.cService.region;
        this.subregion = this.cService.subregion;
        this.country = this.cService.country;
    }

    

    //#region Action Methods

    createCountryClick(){
        console.log('createCountryClick -> ');
    }

    spmCountryOpenClick(value:any) {

        console.log('spmCountryOpenClick -> value=', value);
        if (value) {
            // this.spmProjectOpen.emit(this.project);
        }

    }

    //#endregion

}