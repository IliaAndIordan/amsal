import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { AmsCountry, AmsRegion, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsWadCountryService } from '../country.service';
import { URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_AMS_COMMON } from 'src/app/@core/const/app-storage.const';
import { MarkerService } from 'src/app/@share/maps/map-liflet-ap-marker.service';
import { AmsWadService } from '../../wad.service';
import { MapLifletApComponent } from 'src/app/@share/maps/map-liflet-ap/map-liflet-ap.component';


@Component({
    selector: 'ams-wad-country-home-tabs',
    templateUrl: './country-home-tabs.component.html',
})
export class AmsWadCountryHomeTabsComponent implements OnInit, OnDestroy {

    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();

   @ViewChild('leafletMap',{static:false}) map: MapLifletApComponent;

    amsImgUrl = URL_COMMON_IMAGE_AMS;
    airportsImgUrl = URL_COMMON_IMAGE_AMS_COMMON + 'airport.png';

    panelIChanged: Subscription;
    region: AmsRegion;
    subregion: AmsSubregion;
    subregionChanged: Subscription;
    country: AmsCountry;
    countryChanged: Subscription;

    selTabIdx: number;
    tabIdxChanged: Subscription;

    mapCriteriaChanged: Subscription;

    get panelIn(): boolean {
        return this.cService.panelIn;
    }

    set panelIn(value: boolean) {
        this.cService.panelIn = value;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService: AmsWadService,
        private cService: AmsWadCountryService,
        private markerService: MarkerService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.panelIChanged = this.cService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.cService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.countryChanged = this.wadService.countryChanged.subscribe(country => {
            if (this.country && false) {
                this.markerService.setCountryCriteria(this.country);
            }
        });
        this.country = this.cService.country;
        if (this.country) {
            this.markerService.setCountryCriteria(this.country);
        }
       
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.cService.tabIdx;
        this.region = this.cService.region;
        this.subregion = this.cService.subregion;
        this.country = this.cService.country;
        this.amsImgUrl = this.country && this.country.imageUrl ? this.country.imageUrl : URL_COMMON_IMAGE_AMS + 'country/subregion_' + this.subregion.srCode + '.png';
        if (this.selTabIdx === 0 && this.map) {
            this.map.makeMarkers();
        }
        this.preloader.hide();
    }

    //#region Mobile dialog methods

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> usecase:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.cService.tabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region SPM Methods
    panelInOpen() {
        this.cService.panelIn = true;
    }

    spmProjectOpenClick() {

        // console.log('spmProjectOpen -> spmProjectObj=', this.project);
        /*
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }*/

    }

    //#endregion

}
