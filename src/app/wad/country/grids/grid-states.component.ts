import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// ---
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadCountryService } from '../country.service';
import { AmsStateTableDataSource } from './grid-states.datasource';
// ---
import { AmsState, AmsStateTableCriteria, ResponseAmsStateTableData } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsStateEditDialog } from 'src/app/@share/components/dialogs/state-edit/state-edit.dialog';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { IInfoMessage, InfoMessageDialog } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { AmsWadService } from '../../wad.service';
import { WadStatus } from 'src/app/@core/models/pipes/ams-status.enums';


@Component({
    selector: 'ams-wad-grid-states',
    templateUrl: './grid-states.component.html',
})
export class AmsGridStatesComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsState>;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    selected: AmsState;
    criteria: AmsStateTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;
    done = WadStatus.Done;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'stateId', 'stName', 'iso', 'amsStatus', 'wadStatus', 'udate', 'cities', 'airports'];
    canEdit: boolean;



    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialog: MatDialog,
        private cus: CurrentUserService,
        private cService: AmsWadCountryService,
        private wadService: AmsWadService,
        public tableds: AmsStateTableDataSource,
        private cClient: AmsCountryClient) {

    }

    ngOnInit(): void {
        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((users: Array<AmsState>) => {
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsStateTableCriteria) => {
            this.criteria = this.tableds.criteria;
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;
        this.selected = this.cService.state;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
        const tq = '?m=' + new Date().getTime();
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    rowClicked(value: AmsState): void {
        this.cService.state = value;
        this.initFields();
    }


    editState(data: AmsState): void {
        //console.log('editState-> state:', data);
        this.cService.state = data;
        this.initFields();

        const dialogRef = this.dialog.open(AmsStateEditDialog, {
            width: '721px',
            /*height: '520px',*/
            data: {
                state: data,
                country: this.cService.country,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editCountry-> res:', res);
            if (res) {
                //this.table.renderRows();
                this.loadPage();
            }
            this.initFields();
        });



    }

    addState() {
        // console.log('inviteUser-> ');
        let dto = new AmsState();
        dto.countryId = this.cService.country.countryId;

        this.editState(dto);
    }

    deleteState(value: AmsState): void {
        console.log('deleteState-> value:', value);
        if(this.selected?.stateId === value.stateId){
            this.cService.state = undefined;
            this.initFields();
        }
        if (value && value.stateId) {
            let msg = '<div class="text-center w-100 size-20">Are you sure you want to delete state <br/><br/>';
            msg += ' <span class="blue">' + value.stName + ' ' + value.iso + ' (' + value.stateId + ') </span><br/><br/>';
            msg += 'Operation is not recoverable and date will be deleted from database.</div>';
            this.showMessage({ title: 'WAD State Delete', message: msg })
                .subscribe((res: boolean) => {
                    if (res) {
                        this.cClient.stateDelete(value.stateId)
                            .subscribe((stateId: number) => {
                                console.log('stateDelete-> resp:', stateId);
                                if (stateId === value.stateId) {
                                    msg = ' State ' + value.stName + ' ' + value.iso + ' (' + value.stateId + ') deleted.';
                                    this.toastr.success(msg, 'WAD State Delete', { enableHtml: true });
                                    this.loadPage();
                                }
                            });
                    } else {
                        this.initFields();
                    }

                });
        }
    }

    gotoState(data: AmsState) {
        console.log('gotoState-> state:', data);

        if (data) {
            this.cService.state = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.state]);
        }

    }

    showMessage(data: IInfoMessage): Observable<boolean> {
        console.log('showMessage-> data:', data);

        return new Observable<boolean>(subscriber => {
            const dialogRef = this.dialog.open(InfoMessageDialog, {
                width: '721px',
                height: '320px',
                data: data
            });

            dialogRef.afterClosed().subscribe(res => {
                console.log('showMessage-> res:', res);
                subscriber.next(res);
            });
        });
    }


    spmStateOpen(state: AmsState) {
        if (state) {
            this.wadService.state = state;
            this.wadService.statePanelOpen.next(true);
        }

    }

    //#endregion
}

