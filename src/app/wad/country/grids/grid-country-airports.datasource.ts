import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsAirport, AmsAirportTableCriteria, ResponseAmsAirportTableData } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { AmsWadCountryService } from '../country.service';

export const KEY_CRITERIA = 'ams_wad_grid_country_airports_criteria';
@Injectable()
export class AmsWadCountryAirportsTableDataSource extends DataSource<AmsAirport> {

    airport: AmsAirport;

    private _data: Array<AmsAirport>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsAirport[]> = new BehaviorSubject<AmsAirport[]>([]);
    listSubject = new BehaviorSubject<AmsAirport[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsAirport> {
        return this._data;
    }

    set data(value: Array<AmsAirport>) {
        this._data = value;
        this.listSubject.next(this._data as AmsAirport[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsAirportClient,
        private cService: AmsWadCountryService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsAirportTableCriteria>();

    public get criteria(): AmsAirportTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsAirportTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsAirportTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsAirportTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'typeId';
            obj.sortDesc = true;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsAirportTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsAirport[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsAirport>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<AmsAirport>>(subscriber => {

            this.client.airportTable(this.criteria)
                .subscribe((resp: ResponseAmsAirportTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsAirport>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.airports && resp.data.airports.length > 0) {
                            resp.data.airports.forEach(iu => {
                                const obj = AmsAirport.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsAirport>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
