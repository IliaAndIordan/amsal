import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// ---
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---
import { AmsCity, AmsCountry, AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirport, AmsAirportTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { AmsWadService } from '../../wad.service';
import { AmsAirportEditDialog } from 'src/app/@share/components/dialogs/airport-edit/airport-edit.dialog';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { AmsWadCountryAirportsTableDataSource } from './grid-country-airports.datasource';
import { AmsWadCountryService } from '../country.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { AmsWadStateService } from '../../state/state.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { WadStatus } from 'src/app/@core/models/pipes/ams-status.enums';




@Component({
    selector: 'ams-wad-grid-country-airports',
    templateUrl: './grid-country-airports.component.html',
})
export class AmsGridWadCountryAirportsComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsState>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    done = WadStatus.Done;
    filter: string;

    
    selected: AmsAirport;
    airport:AmsAirport;
    country: AmsCountry;
    countryChanged: Subscription;
    criteria: AmsAirportTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'apId', 'iata', 'icao', 'apName', 'ctName', 'typeId', 'maxRwLenght', 'active', 'paxDemandDay', 'amsStatus', 'wadStatus', 'udate',];
    canEdit: boolean;


    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private client: AmsAirportClient,
        private countryClient: AmsCountryClient,
        private cService: AmsWadCountryService,
        private wadService: AmsWadService,
        private spmMapService: MapLeafletSpmService,
        private stService: AmsWadStateService,
        public tableds: AmsWadCountryAirportsTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {


        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((users: Array<AmsAirport>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsAirportTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });


        this.countryChanged = this.wadService.countryChanged.subscribe((country: AmsCountry) => {
            //console.log(' countryChanged -> = country', country);
            this.criteria = this.tableds.criteria;
            this.country = country;
            this.criteria.countryId = country.countryId;
            this.tableds.criteria = this.criteria;
        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.countryChanged) { this.countryChanged.unsubscribe(); }
    }

    initFields() {
        this.canEdit = this.cus.isAdmin;
        this.airport = this.wadService.airport;
        this.selected = this.wadService.airport;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        if (this.criteria.countryId !== this.cService.country.countryId) {
            this.criteria.countryId = this.cService.country.countryId;
            this.tableds.criteria = this.criteria;
        }

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
          if (event.keyCode != 13) {
            $this.setFilter( (event.target as HTMLInputElement).value);
          }
        }, 800);
    }

    setFilter(value:string){
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = undefined;
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }
    

    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    rowClicked(value: AmsAirport): void {
        this.wadService.airport = value;
        this.initFields();
    }

    spmMapOpen() {
        this.spmMapService.flights = undefined;
        this.spmMapService.airports = this.tableds.data;
        this.spmMapService.spmMapPanelOpen.next(true);
    }

    async ediAirport(data: AmsAirport) {
        console.log('ediAirport-> airport:', data);
        this.preloader.show();
        if (data && data.stateId && this.wadService.state?.stateId != data.stateId) {
            this.wadService.state = await this.wadService.loadState(data.stateId);
            console.log('ediAirport-> state:', this.cService.state);
        }
        let city = this.wadService.city;

        if (data && data.cityId && this.wadService.city?.cityId != data.cityId) {
            let city = await this.wadService.loadCity(data.cityId);
            console.log('ediAirport-> city:', this.wadService.city);
            this.wadService.city = city;
        }

        if (city) {
            this.wadService.airport = data;
            this.ediAirportCity(data, city);
        }
        else {
            this.toastr.info('Please select city first.', 'Select City');
        }
    }

    ediAirportCity(ap: AmsAirport, city: AmsCity) {
        const dialogRef = this.dialogService.open(AmsAirportEditDialog, {
            //width: '721px',
            /*height: '620px',*/
            data: {
                airport: ap,
                city: city,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('ediAirport-> res:', res);
            if (res) {
                //this.table.renderRows();
                this.loadPage();
            }
            this.initFields();
        });
    }

    async gotoAirport(data: AmsAirport) {
        //console.log('gotoAirport-> airport:', data);
        this.preloader.show();
        if (data && data.stateId && this.cService.state?.stateId != data.stateId) {
            this.wadService.state = await this.wadService.loadState(data.stateId);
        }

        let city = this.wadService.city;

        if (data && data.cityId && this.wadService.city?.cityId != data.cityId) {
            let city = await this.wadService.loadCity(data.cityId);
            this.wadService.city = city;
        }

        if (city) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        } else {
            this.toastr.info('No city found. Go to state first.', 'Select City');
        }

    }

    togleAirportActive(airport: AmsAirport, activate: boolean) {
        console.log('togleAirportActive -> activate:', activate);
        console.log('togleAirportActive -> ap:', airport);
        if (airport && airport.active !== activate) {
            airport.active = activate;
            this.airportSave(airport);
        }
    }

    airportSave(airport: AmsAirport) {
        if (airport) {

            this.preloader.display(true);

            this.client.airportSave(airport)
                .subscribe((res: AmsAirport) => {
                    this.preloader.display(false);
                    if (res) {
                        //this.table.renderRows();
                        this.loadPage();
                    }
                    this.initFields();
                },
                    err => {
                        this.preloader.display(false);
                        console.error('Observer got an error: ' + err);
                        this.toastr.error('Compnay', 'Operation Failed: Compnay ' + err);
                    },
                    () => console.log('Observer got a complete notification'));

        }

    }

    async loadCity(cityId: number): Promise<AmsCity> {
        let city = await this.wadService.loadCity(cityId);
        return city;

    }

    //#endregion

    //#region Side Panel

    spmAirportOpen(airport: AmsAirport) {
        console.log('spmAirportOpen -> airport:', airport);
        if (airport) {
            //this.cus.spmAirportPanelOpen.next(airport.apId);
            this.wadService.airportPanelOpen(airport);
        }

    }

    //#endregion
}

