

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadService } from '../../wad/wad.service';
import { AmsWadCountryService } from './country.service';
// -Models
import { environment } from 'src/environments/environment';
import { ExpandTab, PageTransition, ShowHideTriggerBlock, 
  ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev } from '../../@core/const/animations-triggers';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsCountry, AmsRegion, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { Animate } from 'src/app/@core/const/animation.const';
import { Subscription } from 'rxjs';
import { AmsSidePanelModalComponent } from 'src/app/@share/components/common/side-panel-modal/side-panel-modal';
import { SxSidebarNavComponent } from 'src/app/@share/components/common/sidebar/nav/sx-sidebar-nav.component';
import { MatDrawerMode } from '@angular/material/sidenav';


@Component({
  templateUrl: './country.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AmsWadCountryHomeComponent implements OnInit, OnDestroy {


  @ViewChild('spmRegion') spmRegion: AmsSidePanelModalComponent;
  @ViewChild('spmSubregion') spmSubregion: AmsSidePanelModalComponent;
  @ViewChild('sxSide') sxSide: SxSidebarNavComponent;
  
  in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;
  
  sideNavState: string = Animate.open;
  snavmode: MatDrawerMode = 'side';
  
  

  logo = COMMON_IMG_LOGO_RED;
  amsImgUrl = URL_COMMON_IMAGE_AMS;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  region: AmsRegion;
  subregion: AmsSubregion;
  country: AmsCountry;
  countryChanged: Subscription;

  regionPanelOpen: Subscription;
  subregionPanelOpen: Subscription;


  get panelIn(): boolean {
    let rv = this.subregService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.subregService.panelIn;
  }

  set panelIn(value: boolean) {
    this.subregService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private wadService: AmsWadService,
    private subregService: AmsWadCountryService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    this.panelInChanged = this.subregService.panelInChanged.subscribe((panelIn: boolean) => {
      this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    });

    this.regionPanelOpen = this.wadService.regionPanelOpen.subscribe((panelIn: boolean) => {
      this.spmRegionOpen();
    });

    this.subregionPanelOpen = this.wadService.subregionPanelOpen.subscribe((panelIn: boolean) => {
      // console.log('HomeComponent-> subregionPanelOpen:', panelIn);
      this.spmSubregionOpen();
    });

    this.countryChanged = this.wadService.countryChanged.subscribe((country: AmsCountry) => {
      this.initFields();
    });

    if (!this.wadService.subregions || this.wadService.subregions.length === 0) {
      this.loadRegions();
    } else {
      this.initFields();
    }

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
    if (this.regionPanelOpen) { this.regionPanelOpen.unsubscribe(); }
    if (this.subregionPanelOpen) { this.subregionPanelOpen.unsubscribe(); }
  }

  initFields() {
    this.region = this.wadService.region;
    this.subregion = this.wadService.subregion;
    this.country = this.wadService.country;

    //console.log('initFields-> country:', this.country);
  }

  //#region  data

  public loadRegions(): void {
    this.spinerService.show();
    this.wadService.loadRegions()
      .subscribe((resp: ResponseAmsRegionsData) => {
        //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
        this.spinerService.hide();
        this.initFields();
      },
        err => {
          this.spinerService.hide();
          console.log('loadRegions-> err: ', err);
        });

  }
  //#endregion

  //#region  Actions

  // sidebar-closed
  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoSubregion(value: AmsSubregion) {
    //console.log('gotoSubregion -> value:', value);
    if (value) {
      this.wadService.subregion = value;
      this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }

  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmRegionClose(): void {
    this.spmRegion.closePanel();
  }

  spmRegionOpen() {
    this.region = this.wadService.region;
    if (this.region && this.spmRegion) {
      this.spmRegion.expandPanel();
    }

  }

  spmSubregionClose(): void {
    this.spmSubregion.closePanel();
  }

  spmSubregionOpen() {
    //console.log('spmSubregionOpen ->');
    this.subregion = this.wadService.subregion;
    //console.log('spmSubregionOpen -> spmSubregion', this.spmSubregion);
    if (this.subregion && this.spmSubregion) {
      this.spmSubregion.expandPanel();
    }
  }

  //#endregion

  
  //#region  Expand/Close Menu


  snavToggleClick() {
    console.log('snavToggleClick-> sxSide', this.sxSide);
    if(this.sxSide){
      this.sxSide.toggle();
    }
  }

  //#endregion
}
