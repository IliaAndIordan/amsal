import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from '../../../wad/wad.service';
import { AmsWadCountryService } from '../country.service';
// ---Models
import { AmsCountry, AmsRegion, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsCountryEditDialog } from 'src/app/@share/components/dialogs/country-edit/country-edit.dialog';


@Component({
    selector: 'ams-wad-country-filter-panel-body',
    templateUrl: './wad-country-filter-pane.component.html',
})
export class AmsWadCountryFilterPanelBodyComponent implements OnInit, OnDestroy {

    roots = AppRoutes;
    region: AmsRegion;
    subregion: AmsSubregion;
    country:AmsCountry;
    countryChanged: Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService: AmsWadService,
        private cService: AmsWadCountryService) {

    }


    ngOnInit(): void {

        this.countryChanged = this.wadService.countryChanged.subscribe((country: AmsCountry) => {
            this.initFields();
        });

        this.tabIdxChanged = this.cService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.countryChanged) { this.countryChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.tabIdx = this.cService.tabIdx;
        this.region = this.wadService.region;
        this.subregion = this.wadService.subregion;
        this.country = this.wadService.country;
    }


    //#region Action Methods

    editCountryClick(){
        if(this.country){
            this.editCountry(this.country);
        }
    }

    editCountry(data: AmsCountry) {
        console.log('editCountry-> user:', data);
        
        const dialogRef = this.dialogService.open(AmsCountryEditDialog, {
            width: '721px',
            //height: '520px',
            data: {
                country: data,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editCountry-> res:', res);
            if (res) {
                this.wadService.country = res;
            }
            this.initFields();
        });
        
    }


    spmSubregionOpen() {
        // console.log('spmSubregionOpen -> subregion:', this.subregion);
        this.wadService.subregionPanelOpen.next(true);
    }

    gotoSubregion(){
        this.router.navigate([AppRoutes.wad, AppRoutes.subregion]);
    }

    spmCountryOpen(){
        this.wadService.countryPanelOpen.next(this.country);
    }
    

    //#endregion

}
