import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { AppRoutes } from "src/app/@core/const/app-routes.const";
import { COMMON_IMG_LOGO_RED } from "src/app/@core/const/app-storage.const";
import { AmsCountry, AmsRegion, AmsState, AmsSubregion, ResponseAmsRegionsData } from "src/app/@core/services/api/country/dto";
import { UserModel } from "src/app/@core/services/auth/api/dto";
import { CurrentUserService } from "src/app/@core/services/auth/current-user.service";
import { SpinnerService } from "src/app/@core/services/spinner.service";
import { environment } from "src/environments/environment";
import { AmsWadService } from "../wad.service";

@Component({
    selector: 'ams-wad-menu-main',
    templateUrl: './menu-main.component.html',
    // animations: [PageTransition]
})
export class AmsWadMenuMainComponent implements OnInit, OnDestroy {

    @Output() sidebarToggle: EventEmitter<any> = new EventEmitter<any>();

    logoImgUrl = COMMON_IMG_LOGO_RED; // COMMON_IMG_LOGO_IZIORDAN;
    roots = AppRoutes;
    user: UserModel;
    userName: string;
    userChanged: Subscription;
    region: AmsRegion;
    regionChanged: Subscription;
    subregion: AmsSubregion;
    subregionChanged: Subscription;
    country: AmsCountry;
    countryChanged: Subscription;
    state: AmsState;

    avatarUrl: string;
    env: string;
    isAdmin: boolean;

    constructor(private router: Router,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService: AmsWadService) { }

    ngOnInit(): void {
        this.isAdmin = this.cus.isAdmin;
        this.userChanged = this.cus.userChanged.subscribe(user => {
            this.initFields();
        });
        this.regionChanged = this.wadService.regionChanged.subscribe(region => {
            this.initFields();
        });
        this.subregionChanged = this.wadService.subregionChanged.subscribe(subregion => {
            this.initFields();
        });
        this.countryChanged = this.wadService.countryChanged.subscribe(country => {
            this.initFields();
        });
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.userChanged) { this.userChanged.unsubscribe(); }
        if (this.regionChanged) { this.regionChanged.unsubscribe(); }
        if (this.countryChanged) { this.countryChanged.unsubscribe(); }
    }

    initFields() {
        this.user = this.cus.user;
        this.isAdmin = this.cus.isAdmin;
        this.env = environment.abreviation;
        this.avatarUrl = this.cus.avatarUrl;
        this.region = this.wadService.region;
        this.subregion = this.wadService.subregion;
        this.country = this.wadService.country;
        this.state = this.wadService.state;
        let name = 'Guest';
        if (this.cus.user) {
            name = this.cus.user.userName ? this.cus.user.userName : this.cus.user.userEmail;
        }
        this.userName = name;
        // console.log('initFields -> cus.user:', this.cus.user);
    }

    spmRegionOpen() {
        this.wadService.regionPanelOpen.next(true);
    }
    spmSubegionOpen() {
        this.wadService.subregionPanelOpen.next(true);
    }

    //#region Data

    public loadRegions(): void {
        this.spinerService.show();
        this.wadService.loadRegions()
            .subscribe((resp: ResponseAmsRegionsData) => {
                this.spinerService.hide();
                console.log('loadRegions-> regions:', this.wadService.regions);
                this.initFields();
            },
                err => {
                    this.spinerService.hide();
                    console.log('loadRegions-> err: ', err);
                });

    }

    //#endregion
}
