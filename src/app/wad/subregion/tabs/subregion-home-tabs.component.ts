import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsSubregionService } from '../subregion.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { AmsRegion, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { URL_COMMON_IMAGE_AMS } from 'src/app/@core/const/app-storage.const';


@Component({
    selector: 'ams-wad-subregion-home-tabs',
    templateUrl: './subregion-home-tabs.component.html',
})
export class AmsWadSubregionHomeTabsComponent implements OnInit, OnDestroy {
   
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();

    amsImgUrl = URL_COMMON_IMAGE_AMS;
    panelIChanged: Subscription;
    region: AmsRegion;
    regionChanged: Subscription;
    subregion: AmsSubregion;
    subregionChanged: Subscription;

    selTabIdx: number;
    tabIdxChanged: Subscription;

    get panelIn(): boolean {
        return this.subregService.panelIn;
    }

    set panelIn(value: boolean) {
        this.subregService.panelIn = value;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private subregService: AmsSubregionService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.panelIChanged = this.subregService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.subregService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.subregService.tabIdx;
        this.region = this.subregService.region;
        this.subregion = this.subregService.subregion;
        this.amsImgUrl = this.subregion && this.subregion.srImageUrl?this.subregion.srImageUrl:URL_COMMON_IMAGE_AMS + 'country/subregion_' +this.subregion.srCode+'.png';


        this.preloader.hide();
    }

    //#region Mobile dialog methods

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> usecase:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.subregService.tabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region SPM Methods
    panelInOpen(){
        this.subregService.panelIn = true;
    }

    spmProjectOpenClick() {

        // console.log('spmProjectOpen -> spmProjectObj=', this.project);
        /*
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }*/

    }

    //#endregion

}
