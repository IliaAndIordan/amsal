

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// -Models
import { environment } from 'src/environments/environment';
import {
  ExpandTab, PageTransition, ShowHideTriggerBlock, ShowHideTriggerFlex,
  SpinExpandIconTrigger, TogleBtnTopRev
} from '../../@core/const/animations-triggers';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsWadService } from '../../wad/wad.service';
import { AmsRegion, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsSubregionService } from './subregion.service';


@Component({
  templateUrl: './subregion.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AmsWadSubregionHomeComponent implements OnInit, OnDestroy {

  in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  logo = COMMON_IMG_LOGO_RED;
  amsImgUrl = URL_COMMON_IMAGE_AMS;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  region: AmsRegion;
  subregion: AmsSubregion;

  regionPanelOpen: Subscription;
  subregionPanelOpen: Subscription;


  get panelIn(): boolean {
    let rv = this.subregService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.subregService.panelIn;
  }

  set panelIn(value: boolean) {
    this.subregService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private wadService: AmsWadService,
    private subregService: AmsSubregionService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    this.panelInChanged = this.subregService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });

    if (!this.wadService.subregions || this.wadService.subregions.length === 0) {
      this.loadRegions();
    } else {
      this.initFields();
    }

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
    if (this.regionPanelOpen) { this.regionPanelOpen.unsubscribe(); }
    if (this.subregionPanelOpen) { this.subregionPanelOpen.unsubscribe(); }
  }

  initFields() {
    this.region = this.wadService.region;
    this.subregion = this.wadService.subregion;

    //console.log('initFields-> region:', this.region);
    //console.log('initFields-> subregions:', this.subregion);
  }

  //#region  data

  public loadRegions(): void {
    this.spinerService.show();
    this.wadService.loadRegions()
      .subscribe((resp: ResponseAmsRegionsData) => {
        //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
        this.spinerService.hide();
        this.initFields();
      },
        err => {
          this.spinerService.hide();
          console.log('loadRegions-> err: ', err);
        });

  }
  //#endregion

  //#region  Actions

  // sidebar-closed
  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoSubregion(value: AmsSubregion) {
    console.log('gotoSubregion -> value:', value);
    if (value) {
      this.wadService.subregion = value;
      this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }

  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmRegionClose(): void {
    console.log('spmRegionClose ->');
    //this.spmRegion.closePanel();
  }

  spmRegionOpen() {
    this.region = this.wadService.region;
    //if (this.region && this.spmRegion) {
    //this.spmRegion.expandPanel();
    //}

  }

  spmSubregionClose(): void {
    this.wadService.subregionPanelOpen.next(false);
  }

  spmSubregionOpen() {
    if (this.wadService.subregion) {
      this.wadService.subregionPanelOpen.next(true);
    }
  }

  //#endregion
}
