import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxAdminUserClient } from 'src/app/@core/services/api/admin-user/api-client';
import { AmsUserTableCriteria, ResponseAmsUserTableData } from 'src/app/@core/services/api/admin-user/dto';
import { AmsCountry, AmsCountryTableCriteria, ResponseAmsCountryTableData } from 'src/app/@core/services/api/country/dto';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { AmsSubregionService } from '../subregion.service';

export const KEY_CRITERIA = 'ams_wad_grid_country_criteria';
@Injectable()
export class AmsCountryTableDataSource extends DataSource<AmsCountry> {

    selcontry: AmsCountry;

    private _data: Array<AmsCountry>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsCountry[]> = new BehaviorSubject<AmsCountry[]>([]);
    listSubject = new BehaviorSubject<AmsCountry[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsCountry> {
        return this._data;
    }

    set data(value: Array<AmsCountry>) {
        this._data = value;
        this.listSubject.next(this._data as AmsCountry[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsCountryClient,
        private subregService: AmsSubregionService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsCountryTableCriteria>();

    public get criteria(): AmsCountryTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsCountryTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsCountryTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsCountryTableCriteria();
            obj.limit = 25;
            obj.offset = 0;
            obj.sortCol = 'cName';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsCountryTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(collectionViewer: CollectionViewer): Observable<AmsCountry[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsCountry>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        const limit = this.criteria.limit;
        const offset = this.criteria.offset;
        const orderCol = this.criteria.sortCol;
        const isDesc = this.criteria.sortDesc; // sortDirection && sortDirection === 'asc' ? false : true;
        const subregionId = this.criteria.subregionId?this.criteria.subregionId:this.subregService.subregion.subregionId; 
        return new Observable<Array<AmsCountry>>(subscriber => {

            this.client.countryTable(limit, offset, orderCol, isDesc, subregionId, this.criteria.filter)
                .subscribe((resp: ResponseAmsCountryTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    this.data = new Array<AmsCountry>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.countries && resp.data.countries.length > 0) {
                            resp.data.countries.forEach(iu => {
                                const obj = AmsCountry.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsCountry>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
