import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
// ---
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// ---
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsUserEditDialog } from 'src/app/@share/components/dialogs/user-edit/user-edit.dialog';
import { AmsCountryTableDataSource } from './grid-country.datasource';
import { AmsSubregionService } from '../subregion.service';
import { AmsCountry, AmsCountryTableCriteria, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsCountryEditDialog } from 'src/app/@share/components/dialogs/country-edit/country-edit.dialog';
import { WadStatus } from 'src/app/@core/models/pipes/ams-status.enums';

@Component({
    selector: 'ams-wad-grid-country',
    templateUrl: './grid-country.component.html',
})
export class AmsGridCountryComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsCountry>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    done = WadStatus.Done;
    filter: string;

    selected: AmsCountry;
    criteria: AmsCountryTableCriteria;
    criteriaChanged: Subscription;
    subregion: AmsSubregion;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'countryId', 'cCode', 'cName', 'iso2', 'iso3', 'icao', 'amsStatus', 'wadStatus', 'udate', 'statesCount'];
    sortColumns = ['country_name', 'country_id', 'country_code', 'country_name',
        'country_iso2', 'country_iso3', 'country_icao',
        'ams_status_id', 'wad_status_id', 'udate', 'statesCount']
    canEdit: boolean;



    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private subregService: AmsSubregionService,
        public tableds: AmsCountryTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {


        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((users: Array<AmsCountry>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            //this.dataCount = this.tableds.itemsCount;
            //this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsCountryTableCriteria) => {
            //this.criteria = this.tableds.criteria;
            //console.log('criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });

        this.initFields();
        this.criteria.subregionId = this.subregion?.subregionId;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
    }

    initFields() {
        this.canEdit = this.cus.isAdmin;
        this.subregion = this.subregService.subregion;
        this.selected = this.subregService.country;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria?.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 800);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = undefined;
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }


    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    rowClicked(value: AmsCountry): void {
        this.subregService.country = value;
        this.initFields();
    }

    editCountry(data: AmsCountry) {
        console.log('editCountry-> user:', data);

        const dialogRef = this.dialogService.open(AmsCountryEditDialog, {
            width: '721px',
            //height: '520px',
            data: {
                country: data,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editCountry-> res:', res);
            if (res) {
                //this.table.renderRows();
                this.loadPage();
            }
            this.initFields();
        });

    }

    addCountry() {
        // console.log('inviteUser-> ');
        let dto = new AmsCountry();
        dto.subregionId = this.subregService.subregion.subregionId;
        dto.regionId = this.subregService.subregion.regionId;
        this.editCountry(dto);
    }

    gotoCountry(data: AmsCountry) {
        console.log('gotoCountry-> country:', data);

        if (data) {
            this.subregService.country = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.country]);
        }

    }

    //#endregion
}

