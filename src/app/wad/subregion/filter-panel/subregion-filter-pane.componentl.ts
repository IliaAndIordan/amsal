import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from '../../../wad/wad.service';
// ---Models
import { AmsRegion, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsSubregionService } from '../subregion.service';


@Component({
    selector: 'ams-wad-subregion-filter-panel-body',
    templateUrl: './subregion-filter-pane.componentl.html',
})
export class AmsWadSubregionFilterPanelBodyComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */

    /**
     * Fields
     */
    region: AmsRegion;
    regionChanged: Subscription;
    subregion: AmsSubregion;
    subregionChanged: Subscription;
    canEdit:boolean;
    tabIdx: number;
    tabIdxChanged: Subscription;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService: AmsWadService,
        private subregService: AmsSubregionService) {

    }


    ngOnInit(): void {
       
        this.regionChanged = this.wadService.regionChanged.subscribe((region: AmsRegion) => {
            this.initFields();
        });

        this.tabIdxChanged = this.subregService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.regionChanged) { this.regionChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.canEdit = this.cus.isAdmin;
        this.tabIdx = this.subregService.tabIdx;
        this.region = this.wadService.region;
        this.subregion = this.wadService.subregion;
    }


    //#region Action Methods

    spmSubregionOpen() {
        // console.log('spmSubregionOpen -> subregion:', this.subregion);
        this.wadService.subregionPanelOpen.next(true);
    }
    

    //#endregion

}
