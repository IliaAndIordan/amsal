import { Component, EventEmitter, Inject, LOCALE_ID, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from '../../../wad/wad.service';
// ---Models
import { AmsCity, AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsWadStateService } from '../state.service';
import { WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsAirportCacheService } from 'src/app/@core/services/api/airport/ams-airport-cache.service';
import { DecimalPipe } from '@angular/common';


@Component({
    selector: 'ams-wad-state-filter-panel-body',
    templateUrl: './wad-state-filter-pane.component.html',
})
export class AmsWadStateFilterPanelBodyComponent implements OnInit, OnDestroy {
    
    roots = AppRoutes;
    region: AmsRegion;
    subregion: AmsSubregion;
    country:AmsCountry;
    state:AmsState;
    stateChanged: Subscription;
    city:AmsCity;
    cityChanged: Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;
    ourApUrl:string;

    constructor(
        @Inject(LOCALE_ID) private locale: string,
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinner: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private apCache: AmsAirportCacheService,
        private wadService: AmsWadService,
        private stService: AmsWadStateService) {

    }


    ngOnInit(): void {

        this.stateChanged = this.wadService.stateChanged.subscribe((state: AmsState) => {
            this.initFields();
        });

        
        this.cityChanged = this.wadService.cityChanged.subscribe((city: AmsCity) => {
            this.initFields();
        });

        this.tabIdxChanged = this.stService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.stateChanged) { this.stateChanged.unsubscribe(); }
        if (this.cityChanged) { this.cityChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.tabIdx = this.stService.tabIdx;
        this.region = this.stService.region;
        this.subregion = this.stService.subregion;
        this.country = this.stService.country;
        this.state = this.stService.state;
        this.city = this.stService.city;
        if(this.city && this.state && this.city.stateId !== this.state.stateId){
            this.stService.city = undefined;
        }
        this.ourApUrl = WEB_OURAP_REGION + (this.state && this.state.iso?this.state.iso.split('-').join('/') + '/':'');
    }

    //#region Action Metods

    refreshAirportCache():void{
        this.apCache.clearCache();
        this.spinner.show();
        this.spinner.setMessage('Airport Cache loading...');
        const ap$ = this.apCache.airports;
        ap$.subscribe(airports => {
            this.spinner.hide();
            const counter = new DecimalPipe(this.locale).transform(airports?.length??0, '1.0-0');
            this.toastr.info(`<b>${counter}</b> airports loaded into cache. <br/>`, 'Airports', { enableHtml: true })
        });
    }
    //#endregion

    //#region SPM

    spmRegionOpen() {
        this.wadService.regionPanelOpen.next(true);
    }

    gotoRegion(){
        this.router.navigate([AppRoutes.wad, AppRoutes.region]);
    }


    spmSubregionOpen() {
        // console.log('spmSubregionOpen -> subregion:', this.subregion);
        this.wadService.subregionPanelOpen.next(true);
    }

    gotoSubregion(){
        this.router.navigate([AppRoutes.wad, AppRoutes.subregion]);
    }

    spmCountryOpen() {
        // console.log('spmSubregionOpen -> subregion:', this.subregion);
        this.wadService.countryPanelOpen.next(this.country);
    }

    gotoCountry(){
        this.router.navigate([AppRoutes.wad, AppRoutes.country]);
    }

    spmStateOpen(){
        this.wadService.statePanelOpen.next(true);
    }

    //#endregion

}
