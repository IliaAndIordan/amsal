import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// -Services
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadService } from '../wad.service';
// -Nodels
import { AmsCity, AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';


export const SR_LEFT_PANEL_IN_KEY = 'ams_wad_state_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_wad_state_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class AmsWadStateService {

    /**
     *  Fields
     */

    constructor(
        private cus: CurrentUserService,
        private client: AmsCountryClient,
        private wadService: AmsWadService) {
    }

    get region(): AmsRegion {
        return this.wadService.region;
    }

    get subregion(): AmsSubregion {
        return this.wadService.subregion;
    }

    set subregion(value: AmsSubregion)  {
        this.wadService.subregion = value;
    }


    get country(): AmsCountry {
        return this.wadService.country;
    }

    set country(value: AmsCountry) {
        this.wadService.country = value;
    }

    get state(): AmsState {
        return this.wadService.state;
    }
    set state(value: AmsState) {
        this.wadService.state = value;
    }


    cityChanged = new BehaviorSubject<AmsCity>(undefined);
    get city(): AmsCity {
        return this.wadService.city;
    }

    set city(value: AmsCity) {
        this.wadService.city = value;
        this.cityChanged.next(value);
    }

    //#region subregion tree panel

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Data

    public loadCity(cityId: number): Observable<AmsCity> {

        return new Observable<AmsCity>(subscriber => {

            this.client.getCity(cityId)
                .subscribe((resp: AmsCity) => {
                    //console.log('loadCity-> resp', resp);
                    subscriber.next(resp);
                },
                err => {

                    throw err;
                });
        });

    }

    //#endregion


}
