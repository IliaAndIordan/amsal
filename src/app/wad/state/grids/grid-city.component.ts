import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// ---
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---
import { AmsCity, AmsCityTableCriteria, AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AmsWadStateService } from '../state.service';
import { AmsCityTableDataSource } from './grid-city.datasource';
import { AmsCityEditDialog } from 'src/app/@share/components/dialogs/city-edit/city-edit.dialog';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
import { WadStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { state } from '@angular/animations';




@Component({
    selector: 'ams-wad-grid-cities',
    templateUrl: './grid-city.component.html',
})
export class AmsGridCitiesComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild(MatTable, { static: true }) table: MatTable<AmsCity>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    done = WadStatus.Done;
    filter: string;

    selected: AmsCity;
    cityChanged: Subscription
    celCityId: number;

    criteria: AmsCityTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'cityId', 'ctName', 'population', 'stateCapital', 'countryCapital', 'airports', 'paxDemandDay', 'amsStatus', 'wadStatus', 'udate',];
    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private stService: AmsWadStateService,
        public tableds: AmsCityTableDataSource,
        private cClient: AmsCountryClient,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {

        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((users: Array<AmsCity>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsCityTableCriteria) => {
            this.initFields();
            this.loadPage();
        });
        this.cityChanged = this.stService.cityChanged.subscribe((city: AmsCity) => {
            this.initFields();
        })

        this.initFields();
        this.criteria.offset = 0;
        this.criteria.stateId = this.stService?.state?.stateId;
        this.tableds.criteria = this.criteria;
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.cityChanged) { this.cityChanged.unsubscribe(); }
    }

    initFields() {
        this.canEdit = this.cus.isAdmin;
        this.criteria = this.tableds.criteria;
        this.selected = this.stService.city;
        this.filter = this.criteria?.filter;
        this.celCityId = this.selected ? this.selected.cityId : undefined;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 800);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = undefined;
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    rowClicked(value: AmsCity): void {
        this.stService.city = value;
        this.initFields();
    }

    ediCity(data: AmsCity) {
        console.log('ediCity-> city:', data);
        if (data && data.cityId !== 0) {
            this.stService.city = data;
        }
        const dialogRef = this.dialogService.open(AmsCityEditDialog, {
            width: '721px',
            /*height: '520px',*/
            data: {
                city: data,
                state: this.stService.state,
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editCountry-> res:', res);
            if (res) {
                //this.table.renderRows();
                this.loadPage();
            }
            this.initFields();
        });


    }

    addCity() {
        // console.log('inviteUser-> ');
        let dto = new AmsCity();
        dto.stateId = this.stService.state.stateId;
        dto.countryId = this.stService.country.countryId;

        this.ediCity(dto);
    }

    deleteCity(value: AmsCity) {
        console.log('deleteCity-> value:', value);
        if (value && value.cityId && this.canEdit) {
            const cityName = value.ctName + ',  ' + this.stService.state.stName + ' (' + value.cityId + ')';
            let msg = '<div class="text-center w-100 size-20">Are you sure you want to delete city <br/><br/>';
            msg += ' <span class="blue">' + cityName + ' </span><br/><br/>';
            msg += 'Operation is not recoverable and date will be deleted from database.</div>';
            this.cus.showMessage({ title: 'WAD State Delete', message: msg })
                .then((res: boolean) => {
                    if (res) {
                        if (this.stService?.city?.cityId === value.cityId) {
                            this.stService.city = undefined;
                        }
                        this.cClient.cityDelete(value.cityId)
                            .subscribe((cityId: number) => {
                                console.log('cityDelete-> resp:', cityId);
                                msg = ' State ' + cityName + ' deleted.';
                                this.toastr.success(msg, 'WAD City Delete', { enableHtml: true });
                                this.initFields();
                                this.loadPage();
                            });
                    } else {
                        this.initFields();
                    }

                });
        }
    }

    gotoCity(data: AmsCity) {
        console.log('gotoCity-> city:', data);

        if (data) {
            this.stService.city = data;
            //this.router.navigate([AppRoutes.wad, AppRoutes.city]);
        }

    }

    //#endregion
}

