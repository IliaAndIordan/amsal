import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription, lastValueFrom } from 'rxjs';
import { catchError, map, take, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// ---
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---
import { AmsCity, AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsWadStateService } from '../state.service';
import { AmsStateAirportsTableDataSource } from './grid-state-airports.datasource';
import { AmsAirport, AmsAirportTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { AmsWadService } from '../../wad.service';
import { AmsAirportEditDialog } from 'src/app/@share/components/dialogs/airport-edit/airport-edit.dialog';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { WadStatus } from 'src/app/@core/models/pipes/ams-status.enums';




@Component({
    selector: 'ams-wad-grid-state-airports',
    templateUrl: './grid-state-airports.component.html',
})
export class AmsGridStateAirportsComponent implements OnInit, OnDestroy, AfterViewInit {
    
    @ViewChild(MatTable, { static: true }) table: MatTable<AmsState>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    done = WadStatus.Done;
    filter: string;

    selected: AmsAirport;
    
    state: AmsState;
    stateChanged: Subscription;
    criteria: AmsAirportTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    //'amsStatus',
    displayedColumns = ['action', 'apId', 'iata', 'icao', 'apName', 'ctName', 'typeId', 'maxRwLenght', 'active', 'paxDemandDay', 'wadStatus', 'udate',];
    canEdit: boolean;


    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private client: AmsAirportClient,
        private stService: AmsWadStateService,
        private wadService: AmsWadService,
        private spmMapService: MapLeafletSpmService,
        public tableds: AmsStateAirportsTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {


        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((users: Array<AmsAirport>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            //this.dataCount = this.tableds.itemsCount;
            //this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsAirportTableCriteria) => {
            this.initFields();
            this.loadPage();
        });


        this.stateChanged = this.wadService.stateChanged.subscribe((state: AmsState) => {
            //console.log(' stateChanged -> = state', state);
            this.initFields();
            this.criteria.stateId = state.stateId;
            this.tableds.criteria = this.criteria;

        });

        this.initFields();
        this.criteria.offset = 0;
        this.criteria.stateId = this.stService?.state?.stateId;
        this.tableds.criteria = this.criteria;
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.stateChanged) { this.stateChanged.unsubscribe(); }
    }

    initFields() {
        this.canEdit = this.cus.isAdmin;
        this.selected = this.wadService.airport;
        this.criteria = this.tableds.criteria;
        this.filter = this.criteria?.filter;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';
    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    timeout: any = null;
    applyFilter(event: any) {
        clearTimeout(this.timeout);
        var $this = this;
        this.timeout = setTimeout(function () {
            if (event.keyCode != 13) {
                $this.setFilter((event.target as HTMLInputElement).value);
            }
        }, 800);
    }

    setFilter(value: string) {
        this.criteria.filter = value?.trim();
        this.criteria.sortCol = undefined;
        this.criteria.sortDesc = false;
        this.criteria.offset = 0;
        this.tableds.criteria = this.criteria;
    }


    clearFilter() {
        this.criteria.filter = undefined;
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.dataCount = this.tableds.itemsCount;
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    rowClicked(value: AmsAirport): void {
        this.wadService.airport = value;
        this.initFields();
    }

    spmAirportOpen(airport: AmsAirport) {
        this.cus.spmAirportPanelOpen.next(airport.apId);
    }

    spmMapOpen() {
        this.spmMapService.flights = undefined;
        this.spmMapService.airports = this.tableds.data;
        console.log('spmMapOpen -> airports:', this.spmMapService.airports)
        this.spmMapService.spmMapPanelOpen.next(true);
    }


    async ediAirport(data: AmsAirport) {
        //console.log('ediAirport-> airport:', data);
        //console.log('ediAirport-> city:', this.stService.city);
        if ((!this.stService.city && data.cityId) || 
            this.stService.city?.cityId !== data.cityId) {
            const city = await this.loadCity(data.cityId);
            //console.log('gotoAirport-> city:', city);
            this.stService.city = city;
        }

        if (this.stService.city) {


            const dialogRef = this.dialogService.open(AmsAirportEditDialog, {
                //width: '721px',
                /*height: '620px',*/
                data: {
                    airport: data,
                    city: this.stService.city,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    //this.table.renderRows();
                    this.loadPage();
                }
                this.initFields();
            });
        }
        else {
            this.toastr.info('Please select city first.', 'Select City');
        }

    }

    addAirport() {
        // console.log('inviteUser-> ');
        let dto = new AmsAirport();
        dto.stateId = this.stService.state.stateId;
        dto.countryId = this.stService.country.countryId;
        dto.cityId = this.stService.city ? this.stService.city.cityId : undefined;
        this.ediAirport(dto);
    }

    async gotoAirport(data: AmsAirport) {
        if (data) {
            this.wadService.airport = data;
            if (data.cityId) {
                const city = await this.loadCity(data.cityId);
                //console.log('gotoAirport-> city:', city);
                this.stService.city = city;
            }

            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }

    }

    togleAirportActive(airport: AmsAirport, activate: boolean) {
        console.log('togleAirportActive -> activate:', activate);
        console.log('togleAirportActive -> ap:', airport);
        if (airport && airport.active !== activate) {
            airport.active = activate;
            this.airportSave(airport);
        }
    }

    airportSave(airport: AmsAirport) {
        if (airport) {

            this.preloader.display(true);

            this.client.airportSave(airport)
                .subscribe((res: AmsAirport) => {
                    this.preloader.display(false);
                    if (res) {
                        //this.table.renderRows();
                        this.loadPage();
                    }
                    this.initFields();
                },
                    err => {
                        this.preloader.display(false);
                        console.error('Observer got an error: ' + err);
                        this.toastr.error('Compnay', 'Operation Failed: Compnay ' + err);
                    },
                    () => console.log('Observer got a complete notification'));

        }

    }

    async loadCity(cityId: number): Promise<AmsCity> {
        let promise = new Promise<AmsCity>(async (resolve, reject) => {
            const source$ = this.stService.loadCity(cityId).subscribe((res:AmsCity)=>{
                resolve(res);
            });
        });
        return promise
    }

    //#endregion
}

