import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsCity, AmsCityTableCriteria, AmsCityTableData, AmsCountry, AmsState, AmsStateTableCriteria, ResponseAmsCityTableData, ResponseAmsStateTableData } from 'src/app/@core/services/api/country/dto';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsWadStateService } from '../state.service';

export const KEY_CRITERIA = 'ams_wad_grid_city_criteria';
@Injectable()
export class AmsCityTableDataSource extends DataSource<AmsCity> {

    selcity: AmsCity;

    private _data: Array<AmsCity>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsCity[]> = new BehaviorSubject<AmsCity[]>([]);
    listSubject = new BehaviorSubject<AmsCity[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsCity> {
        return this._data;
    }

    set data(value: Array<AmsCity>) {
        this._data = value;
        this.listSubject.next(this._data as AmsCity[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsCountryClient,
        private stService: AmsWadStateService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsCityTableCriteria>();

    public get criteria(): AmsCityTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsCityTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsCityTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsCityTableCriteria();
            obj.limit = 25;
            obj.offset = 0;
            obj.sortCol = 'ctName';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsCityTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(collectionViewer: CollectionViewer): Observable<AmsCity[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsCity>> {
        this.isLoading = true;
        return new Observable<Array<AmsCity>>(subscriber => {
            this.client.cityTableReq(this.criteria)
                .then((resp: ResponseAmsCityTableData) => {
                    //console.log('loadData -> resp:', resp);
                    this.data = new Array<AmsCity>();
                    this.data = resp?.data?.cities;
                    this.itemsCount = resp.data.rowsCount ? resp.data.rowsCount.totalRows : 0;
                    this.listSubject.next(this.data);
                    subscriber.next(this.data);
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> msg:', msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsCity>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
