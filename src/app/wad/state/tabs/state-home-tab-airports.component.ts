import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from 'src/app/wad/wad.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { PageViewType } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsWadStateService } from '../state.service';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';

export const PAGE_VIEW_TYPE = 'sx_project_usecase_pageview';

@Component({
    selector: 'ams-wad-state-home-tab-airports',
    templateUrl: './state-home-tab-airports.component.html',
})
export class AmsWadStateHomeTabAirportsComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    @Input() tabIdx: number;
    /**
     * Fields
     */
    region: AmsRegion;
    subregion:AmsSubregion;
    country: AmsCountry;
    state:AmsState;
    stateChanged: Subscription;
    
    selTabIdx: number;
    tabIdxChanged: Subscription;

    totalCount: number;
    pvtOpt = PageViewType;
    showCardVar: string = Animate.show;
    showListVar: string = Animate.hide;
    showTableVar: string = Animate.hide;



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService:AmsWadService,
        private stService: AmsWadStateService) {

    }


    ngOnInit(): void {
       
        this.totalCount = 0;

        this.stateChanged = this.wadService.stateChanged.subscribe((state: AmsState) => {
            this.initFields();
        });


        this.tabIdxChanged = this.stService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.stateChanged) { this.stateChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.stService.tabIdx;
        this.region = this.stService.region;
        this.subregion = this.stService.subregion;
        this.country = this.stService.country;
    }

    

    //#region Action Methods

    createCountryClick(){
        console.log('createCountryClick -> ');
    }

    spmCountryOpenClick(value:any) {

        console.log('spmCountryOpenClick -> value=', value);
        if (value) {
            // this.spmProjectOpen.emit(this.project);
        }

    }

    //#endregion

}