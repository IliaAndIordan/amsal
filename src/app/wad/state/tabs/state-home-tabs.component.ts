import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsWadStateService } from '../state.service';
import { AmsWadService } from '../../wad.service';
import { MarkerService } from 'src/app/@share/maps/map-liflet-ap-marker.service';
import { MapLifletApComponent } from 'src/app/@share/maps/map-liflet-ap/map-liflet-ap.component';


@Component({
    selector: 'ams-wad-state-home-tabs',
    templateUrl: './state-home-tabs.component.html',
})
export class AmsWadStateHomeTabsComponent implements OnInit, OnDestroy {
    
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();

    @ViewChild('leafletMap') map: MapLifletApComponent;
    
    panelIChanged: Subscription;
    region: AmsRegion;
    subregion: AmsSubregion;
    subregionChanged: Subscription;
    country: AmsCountry;
    state:AmsState;
    stateChanged: Subscription;

    selTabIdx: number;
    tabIdxChanged: Subscription;

    get panelIn(): boolean {
        return this.stService.panelIn;
    }

    set panelIn(value: boolean) {
        this.stService.panelIn = value;
    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService: AmsWadService,
        private stService: AmsWadStateService,
        private markerService: MarkerService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.panelIChanged = this.stService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.stService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.stateChanged = this.wadService.stateChanged.subscribe((state: AmsState) => {
            this.initFields();
          });

          this.state = this.stService.state;
          if (this.state) {
              this.markerService.setStateCriteria(this.state);
          }
        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.stService.tabIdx;
        this.region = this.stService.region;
        this.subregion = this.stService.subregion;
        this.country = this.stService.country;
        this.state = this.stService.state;

        if (this.selTabIdx === 0 && this.map) {
            this.map.makeMarkers();
        }

        this.preloader.hide();
    }

    //#region Mobile dialog methods

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> usecase:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.stService.tabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }


    //#endregion

    //#region SPM Methods
    panelInOpen() {
        this.stService.panelIn = true;
    }

    spmProjectOpenClick() {

        // console.log('spmProjectOpen -> spmProjectObj=', this.project);
        /*
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }*/

    }

    //#endregion

}
