

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadService } from '../../wad/wad.service';
// -Models
import { environment } from 'src/environments/environment';
import {
  ExpandTab, PageTransition, ShowHideTriggerBlock,
  ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev
} from '../../@core/const/animations-triggers';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { Animate } from 'src/app/@core/const/animation.const';
import { Subscription } from 'rxjs';
import { AmsSidePanelModalComponent } from 'src/app/@share/components/common/side-panel-modal/side-panel-modal';
import { AmsWadStateService } from './state.service';


@Component({
  templateUrl: './state.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AmsWadStateHomeComponent implements OnInit, OnDestroy {


  in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  logo = COMMON_IMG_LOGO_RED;
  amsImgUrl = URL_COMMON_IMAGE_AMS;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  region: AmsRegion;
  subregion: AmsSubregion;
  country: AmsCountry;
  state: AmsState;
  stateChanged: Subscription;

  get panelIn(): boolean {
    let rv = this.stService.panelIn;
    this.panelInVar = rv ? Animate.out : Animate.in;
    return this.stService.panelIn;
  }

  set panelIn(value: boolean) {
    this.stService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private wadService: AmsWadService,
    private stService: AmsWadStateService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    this.panelInChanged = this.stService.panelInChanged.subscribe((panelIn: boolean) => {
      const tmp = this.panelIn;
    });


    this.stateChanged = this.wadService.stateChanged.subscribe((state: AmsState) => {
      this.initFields();
    });

    if (!this.wadService.subregions || this.wadService.subregions.length === 0) {
      this.loadRegions();
    } else {
      this.initFields();
    }

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.stateChanged) { this.stateChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields() {
    this.region = this.wadService.region;
    this.subregion = this.wadService.subregion;
    this.country = this.wadService.country;
    this.state = this.wadService.state;

    //console.log('initFields-> country:', this.country);
  }

  //#region  data

  public loadRegions(): void {
    this.spinerService.show();
    this.wadService.loadRegions()
      .subscribe((resp: ResponseAmsRegionsData) => {
        //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
        this.spinerService.hide();
        this.initFields();
      },
        err => {
          this.spinerService.hide();
          console.log('loadRegions-> err: ', err);
        });

  }
  //#endregion

  //#region  Actions

  // sidebar-closed
  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoSubregion(value: AmsSubregion) {
    if (value) {
      this.wadService.subregion = value;
      this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }

  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmRegionClose(): void {
    this.wadService.regionPanelOpen.next(false);
  }

  spmRegionOpen() {
    this.region = this.wadService.region;
    if (this.region ) {
      this.wadService.regionPanelOpen.next(true);
    }

  }

  spmSubregionClose(): void {
    this.wadService.subregionPanelOpen.next(false);
  }

  spmSubregionOpen() {
    this.subregion = this.wadService.subregion;
   
    if (this.subregion) {
      this.wadService.subregionPanelOpen.next(true);
    }
  }

  //#endregion
}
