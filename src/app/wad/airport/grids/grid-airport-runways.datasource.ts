import { DataSource } from '@angular/cdk/table';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';
// ---Models
import { CollectionViewer } from '@angular/cdk/collections';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsAirport, AmsAirportTableCriteria, AmsRunway, AmsRunwayTableCriteria, ResponseAmsAirportTableData, ResponseAmsRunwayTableData } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { AmsWadAirportService } from '../airport.service';

export const KEY_CRITERIA = 'ams_wad_grid_airport_runways_criteria';
@Injectable()
export class AmsAirportRunwaysTableDataSource extends DataSource<AmsRunway> {

    selrw: AmsRunway;

    private _data: Array<AmsRunway>;
    private _isLoading = false;

    numberOfPages = 0;
    itemsCount = 0;

    dataChange: BehaviorSubject<AmsRunway[]> = new BehaviorSubject<AmsRunway[]>([]);
    listSubject = new BehaviorSubject<AmsRunway[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    loading$ = this.loadingSubject.asObservable();

    get isLoading(): boolean {
        return this._isLoading;
    }

    set isLoading(value: boolean) {
        this._isLoading = value;
        this.loadingSubject.next(this._isLoading);
    }

    get data(): Array<AmsRunway> {
        return this._data;
    }

    set data(value: Array<AmsRunway>) {
        this._data = value;
        this.listSubject.next(this._data as AmsRunway[]);
    }

    constructor(
        private cus: CurrentUserService,
        private client: AmsAirportClient,
        private aService: AmsWadAirportService) {
        super();
    }

    //#region criteria

    public criteriaChanged = new Subject<AmsRunwayTableCriteria>();

    public get criteria(): AmsRunwayTableCriteria {
        const onjStr = localStorage.getItem(KEY_CRITERIA);
        let obj: AmsRunwayTableCriteria;
        if (onjStr) {
            obj = Object.assign(new AmsRunwayTableCriteria(), JSON.parse(onjStr));
        } else {
            obj = new AmsRunwayTableCriteria();
            obj.limit = 12;
            obj.offset = 0;
            obj.sortCol = 'rwOrder';
            obj.sortDesc = false;
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        }
        return obj;
    }

    public set criteria(obj: AmsRunwayTableCriteria) {
        if (obj) {
            localStorage.setItem(KEY_CRITERIA, JSON.stringify(obj));
        } else {
            localStorage.removeItem(KEY_CRITERIA);
        }
        this.criteriaChanged.next(obj);
    }

    //#endregion

    //#region DataSource Interface Methods

    connect(collectionViewer: CollectionViewer): Observable<AmsRunway[]> {
        // return this.userListSubject.asObservable();
        return this.listSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        // this.userListSubject.complete();
        // this.loadingSubject.complete();
    }


    loadData(): Observable<Array<AmsRunway>> {
        //console.log('loadData->');
        //console.log('loadData-> criteria=', this.criteria);

        this.isLoading = true;
        
        return new Observable<Array<AmsRunway>>(subscriber => {

            this.client.runwayTable(this.criteria)
                .subscribe((resp: ResponseAmsRunwayTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    this.data = new Array<AmsRunway>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.runways && resp.data.runways.length > 0) {
                            resp.data.runways.forEach(iu => {
                                const obj = AmsRunway.fromJSON(iu);
                                this.data.push(obj);
                            });
                        }

                        this.itemsCount = resp.data.rowsCount?resp.data.rowsCount.totalRows:0;
                        this.listSubject.next(this.data);
                        subscriber.next(this.data);
                    }
                    this.isLoading = false;
                }, msg => {
                    console.log('loadData -> ' + msg);

                    this.itemsCount = 0;
                    this.data = new Array<AmsRunway>();
                    this.isLoading = false;

                    // component.errorMessage = msg;
                });

        });

    }
    //#endregion
}
