import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BehaviorSubject, Observable, merge, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
// ---
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
// ---
import { AmsState } from 'src/app/@core/services/api/country/dto';
import { COMMON_IMG_AVATAR, PAGE_SIZE_OPTIONS } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsAirport, AmsRunway, AmsRunwayTableCriteria } from 'src/app/@core/services/api/airport/dto';
import { AmsWadService } from '../../wad.service';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { AmsAirportRunwaysTableDataSource } from './grid-airport-runways.datasource';
import { AmsWadAirportService } from '../airport.service';
import { RwTypeOpt } from 'src/app/@core/models/pipes/ap-type.pipe';
import { AmsRunwayEditDialog } from 'src/app/@share/components/dialogs/runway-edit/runway-edit.dialog';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';




@Component({
    selector: 'ams-wad-grid-airport-runways',
    templateUrl: './grid-airport-runways.component.html',
})
export class AmsGridAirportRunwaysComponent implements OnInit, OnDestroy, AfterViewInit {
    /**
     * Binding
     */
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatTable, { static: true }) table: MatTable<AmsState>; // initialize
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    /**
     * Fields
     */
    typeOpt = RwTypeOpt;
    selRunway: AmsRunway;
    runway: AmsState;
    airport: AmsAirport;
    airportChanged: Subscription;
    criteria: AmsRunwayTableCriteria;
    criteriaChanged: Subscription;
    imageAvatarUrl = COMMON_IMG_AVATAR;

    pageSizeOpt = PAGE_SIZE_OPTIONS;
    pageIndex = 0;
    pageSize = 12;
    sortActive: string;
    sortDirection: SortDirection;

    dataCount = 0;
    dataChanged: Subscription;
    displayedColumns = ['action', 'rwOrder', 'rwType', 'rwDirection', 'serfaceId', 'lenght', 'width', 'mtow', 'udate', 'rotation', 'radius', 'rwId', 'apId'];
    canEdit: boolean;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aService: AmsWadAirportService,
        private wadService: AmsWadService,
        private spmMapService: MapLeafletSpmService,
        public tableds: AmsAirportRunwaysTableDataSource,
        private cdref: ChangeDetectorRef) {

    }


    ngOnInit(): void {


        this.canEdit = this.cus.isAdmin;
        this.dataChanged = this.tableds.listSubject.subscribe((users: Array<AmsRunway>) => {
            // console.log('MyCompanyUsersComponent:userListSubject()->', users);
            this.dataCount = this.tableds.itemsCount;
            this.initFields();
        });
        this.criteriaChanged = this.tableds.criteriaChanged.subscribe((criteria: AmsRunwayTableCriteria) => {
            this.criteria = this.tableds.criteria;
            // console.log('MyCompanyUsersComponent:criteriaChanged()->', criteria);
            // this.paginator._changePageSize(this.paginator.pageSize);
            this.initFields();
            this.loadPage();
        });


        this.airportChanged = this.wadService.airportChanged.subscribe((ap: AmsAirport) => {
            console.log(' airportChanged -> = ap', ap);
            this.criteria = this.tableds.criteria;
            this.criteria.apId = ap.apId;
            this.tableds.criteria = this.criteria;

        });

        this.initFields();
        this.loadPage();
    }

    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
        if (this.dataChanged) { this.dataChanged.unsubscribe(); }
        if (this.criteriaChanged) { this.criteriaChanged.unsubscribe(); }
        if (this.airportChanged) { this.airportChanged.unsubscribe(); }
    }

    initFields() {
        this.canEdit = this.cus.isAdmin;
        this.criteria = this.tableds.criteria;
        this.pageIndex = this.criteria.pageIndex;
        this.pageSize = this.criteria.limit;

        this.sortActive = this.criteria.sortCol;
        this.sortDirection = this.criteria.sortDesc ? 'desc' : 'asc';

        if (this.criteria.apId !== this.aService.airport.apId) {
            this.criteria.apId = this.aService.airport.apId;
            this.tableds.criteria = this.criteria;
        }

        // console.log('initFields-> criteria:', this.criteria);
        const tq = '?m=' + new Date().getTime();


    }


    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => {
            this.criteria.pageIndex = 0;
            this.criteria.sortCol = this.sort.active;
            this.criteria.sortDesc = this.sort.direction !== 'asc';
            this.tableds.criteria = this.criteria;
        });
        this.paginator.page
            .pipe(
                tap(() => {
                    this.criteria.pageIndex = this.paginator.pageIndex;
                    this.criteria.limit = this.paginator.pageSize;
                    this.tableds.criteria = this.criteria;
                })
            )
            .subscribe();
    }

    //#region Table events

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.criteria.filter = filterValue.trim();
        this.tableds.criteria = this.criteria;
    }

    loadPage() {
        this.tableds.loadData()
            .subscribe(res => {
                this.initFields();
            });
    }

    //#endregion

    //#region Actions

    refreshClick() {
        this.loadPage();
    }

    spmAirportOpen(airport: AmsAirport) {
        this.wadService.airportPanelOpen(airport);
    }


    ediRunway(data: AmsRunway) {
        console.log('ediRunway-> AmsRunway:', data);
        //console.log('ediAirport-> city:', this.stService.city);

        if (data) {
            const dialogRef = this.dialogService.open(AmsRunwayEditDialog, {
                width: '780px',
                /*height: '380px',*/
                data: {
                    runway: data,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    //this.table.renderRows();
                    //this.loadPage();
                    this.loadAirport(this.aService.airport.apId)
                    .subscribe((res: AmsAirport) => {
                        if (res) {
                            this.aService.airport = res;
                            this.initFields();
                        }
                    });
                }
                else{
                    this.initFields();
                }
                
                
            });
        }
        else {
            this.toastr.info('Please select airport first.', 'Select Airport');
        }

    }

    addRunway() {
        // console.log('inviteUser-> ');
        let dto = new AmsRunway();
        dto.apId = this.aService.airport.apId;
        this.ediRunway(dto);
    }

    
    //#endregion

    //#region Data

    public loadAirport(apId: number): Observable<AmsAirport> {
        this.preloader.show();
        return new Observable<AmsAirport>(subscriber => {

            this.aService.loadAirport(apId)
                .subscribe((resp: AmsAirport) => {
                    //console.log('loadAirport-> resp', resp);
                    subscriber.next(resp);
                    this.preloader.hide();
                },
                    err => {
                        this.preloader.hide();
                        throw err;
                    });
        });

    }

    //#endregion


    /*
    gotoAirport(data: AmsAirport) {
        
        if (data) {
            this.wadService.airport = data;
            this.router.navigate([AppRoutes.wad, AppRoutes.airport]);
        }

    }*/

    //#endregion
}

