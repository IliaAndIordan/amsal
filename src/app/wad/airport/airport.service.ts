import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
// -Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadService } from '../wad.service';
// -Nodels
import { AmsCity, AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsAirport, AmsRunway } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';


export const SR_LEFT_PANEL_IN_KEY = 'ams_wad_airport_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_wad_airport_tab_idx';

@Injectable({
    providedIn: 'root',
})
export class AmsWadAirportService {

    /**
     *  Fields
     */

    constructor(
        private cus: CurrentUserService,
        private client: AmsAirportClient,
        private wadService: AmsWadService) {
    }

    get region(): AmsRegion {
        return this.wadService.region;
    }

    get subregion(): AmsSubregion {
        return this.wadService.subregion;
    }

    set subregion(value: AmsSubregion) {
        this.wadService.subregion = value;
    }


    get country(): AmsCountry {
        return this.wadService.country;
    }

    set country(value: AmsCountry) {
        this.wadService.country = value;
    }

    get state(): AmsState {
        return this.wadService.state;
    }
    set state(value: AmsState) {
        this.wadService.state = value;
    }



    get city(): AmsCity {
        return this.wadService.city;
    }

    set city(value: AmsCity) {
        this.wadService.city = value;
    }

    get airport(): AmsAirport {
        return this.wadService.airport;
    }

    set airport(value: AmsAirport) {
        this.wadService.airport = value;
    }

    //#region subregion tree panel

    panelInChanged = new BehaviorSubject<boolean>(true);

    get panelIn(): boolean {
        let rv = true;
        const valStr = localStorage.getItem(SR_LEFT_PANEL_IN_KEY);
        if (valStr) {
            rv = JSON.parse(valStr) as boolean;
        }
        return rv;
    }

    set panelIn(value: boolean) {
        localStorage.setItem(SR_LEFT_PANEL_IN_KEY, JSON.stringify(value));
        this.panelInChanged.next(value);
    }

    tabIdxChanged = new BehaviorSubject<number>(undefined);

    get tabIdx(): number {
        let rv = 0;
        const dataStr = localStorage.getItem(SR_SEL_TABIDX_KEY);
        //console.log('selFolderId-> dataStr', dataStr);
        if (dataStr) {
            try {
                rv = parseInt(dataStr, 10);
            }
            catch {
                localStorage.removeItem(SR_SEL_TABIDX_KEY);
                rv = 1;
            }

        }
        // console.log('selTabIdx-> rv', rv);
        return rv;
    }

    set tabIdx(value: number) {
        // console.log('selTabIdx->', value);
        const oldValue = this.tabIdx;

        localStorage.setItem(SR_SEL_TABIDX_KEY, JSON.stringify(value));
        if (oldValue !== value) {
            this.tabIdxChanged.next(value);
        }
    }

    //#endregion

    //#region Data

    public loadAirport(apId: number): Observable<AmsAirport> {

        return new Observable<AmsAirport>(subscriber => {

            this.client.getAirport(apId)
                .subscribe((resp: AmsAirport) => {
                    console.log('loadAirport-> resp', resp);
                    subscriber.next(resp);
                },
                err => {

                    throw err;
                });
    });

    }

    public loadRunway(rwId: number): Observable<AmsRunway> {

        return new Observable<AmsRunway>(subscriber => {

            this.client.getRunway(rwId)
                .subscribe((resp: AmsRunway) => {
                    //console.log('loadRunway-> resp', resp);
                    subscriber.next(resp);
                },
                err => {

                    throw err;
                });
    });

    }

    //#endregion


}
