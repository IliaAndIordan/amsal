import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from '../../../wad/wad.service';
// ---Models
import { AmsCity, AmsCountCityNoDemandResponce, AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { WEB_OURAP_REGION } from 'src/app/@core/const/app-storage.const';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';
import { AmsWadAirportService } from '../airport.service';
import { AmsAirportEditDialog } from 'src/app/@share/components/dialogs/airport-edit/airport-edit.dialog';
import { AmsWadStateService } from '../../state/state.service';
import { MapLeafletSpmService } from 'src/app/@share/maps/map-liflet-spm.service';
import { IInfoMessage, InfoMessageDialog } from 'src/app/@share/components/dialogs/info-message/info-message.dialog';
import { AmsCountryClient } from 'src/app/@core/services/api/country/api-client';


@Component({
    selector: 'ams-wad-airport-filter-panel-body',
    templateUrl: './wad-airport-filter-pane.component.html',
})
export class AmsWaAirportFilterPanelBodyComponent implements OnInit, OnDestroy {

    roots = AppRoutes;
    region: AmsRegion;
    subregion: AmsSubregion;
    country: AmsCountry;
    state: AmsState;
    city: AmsCity;
    airport: AmsAirport
    airportChanged: Subscription;

    tabIdx: number;
    tabIdxChanged: Subscription;
    ourApUrl: string;
    canEdit: boolean;
    calcCityPpdColor:string = 'primary';

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinner: SpinnerService,
        public dialog: MatDialog,
        private cus: CurrentUserService,
        private wadService: AmsWadService,
        private aService: AmsWadAirportService,
        private stService: AmsWadStateService,
        private countryClient: AmsCountryClient,
        private spmMapService: MapLeafletSpmService,) {

    }


    ngOnInit(): void {

        this.airportChanged = this.wadService.airportChanged.subscribe((ap: AmsAirport) => {
            this.initFields();
        });

        this.tabIdxChanged = this.aService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.airportChanged) { this.airportChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.canEdit = this.cus.isAdmin;
        this.tabIdx = this.aService.tabIdx;
        this.region = this.aService.region;
        this.subregion = this.aService.subregion;
        this.country = this.aService.country;
        this.state = this.aService.state;
        this.airport = this.aService.airport;
        this.city = this.aService.city;
        if (this.city && this.airport && this.city.cityId !== this.airport.cityId) {
            this.loadCity();
        }
        this.ourApUrl = WEB_OURAP_REGION + (this.state && this.state.iso ? this.state.iso.split('-').join('/') + '/' : '');
        this.calcCityPpdColor = this.city && this.city.paxDemandPeriod?'primary':'warn';
    }

    //#region Dialogs

    ediAirport() {
        //console.log('ediAirport-> airport:', data);
        //console.log('ediAirport-> city:', this.stService.city);
        if (this.airport) {


            const dialogRef = this.dialog.open(AmsAirportEditDialog, {
                //width: '821px',
                //height: '600px',
                data: {
                    airport: this.airport,
                    city: this.aService.city,
                }
            });

            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.aService.airport = res;
                }
                this.initFields();
            });
        }
        else {
            this.toastr.info('Please select airport first.', 'Select City');
        }

    }

    calcCityPayloadDemendClick() {
        console.log('calcCityPayloadDemendClick-> city:', this.city);
        if (!this.city && !this.city.cityId) {
            return;
        }
        const data: IInfoMessage = {
            message: '<div>Callculation of city and airports PAX and cargo demand is a long process.<be/><br/>Are your shire that you ould like to start it?</div>',
            title: 'City Payload Demand Callculation'
        }
        const dialogRef = this.dialog.open(InfoMessageDialog, {
            /*width: '721px',
            height: '320px',*/
            data: data
        });

        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.spinner.display(true);
                this.spinner.setMessage('Callculate city payload demans ...')
                this.countryClient.calcCityPayloadDemendCityId(this.city.cityId)
                    .then((res: AmsCountCityNoDemandResponce) => {
                        //console.log('loadCityNoDemandCount -> res:', res);
                        this.toastr.success(`Success: Paylad deman for ${res?.data?.rowsCount} cities culated.`, 'City Payload Demand Callculation')
                        this.stService.loadCity(this.airport.cityId)
                            .subscribe((city: AmsCity) => {
                                this.spinner.hide();
                                this.stService.city = city;
                                this.aService.city = city;
                                this.initFields();
                            });
                    },
                        msg => {
                            console.log('loadCityNoDemandCount -> msg:', msg);
                            this.spinner.display(false);
                            this.toastr.error(`Error: ${msg?.name}`, 'City Payload Demand Callculation')
                        }).catch(ex => {
                            console.log('loadCityNoDemandCount -> ex:', ex);
                            this.spinner.display(false);
                            this.toastr.error(`Error: ${ex}`, 'City Payload Demand Callculation')
                        });
            }
        });

    }
    //#endregion

    //#region SPM

    spmMapOpen() {
        this.spmMapService.flights = undefined;
        this.spmMapService.airports = [this.airport];
        this.spmMapService.spmMapPanelOpen.next(true);
    }

    spmRegionOpen() {
        this.wadService.regionPanelOpen.next(true);
    }

    gotoRegion() {
        this.router.navigate([AppRoutes.wad, AppRoutes.region]);
    }

    gotoState() {
        this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.state]);
    }

    spmSubregionOpen() {
        // console.log('spmSubregionOpen -> subregion:', this.subregion);
        this.wadService.subregionPanelOpen.next(true);
    }

    gotoSubregion() {
        this.router.navigate([AppRoutes.wad, AppRoutes.subregion]);
    }

    spmCountryOpen() {
        // console.log('spmSubregionOpen -> subregion:', this.subregion);
        this.wadService.subregionPanelOpen.next(true);
    }

    gotoCountry() {
        this.router.navigate([AppRoutes.wad, AppRoutes.country]);
    }

    spmStateOpen() {
        this.wadService.statePanelOpen.next(true);
    }

    spmAirportOpen() {
        this.cus.spmAirportPanelOpen.next(this.airport.apId);
    }

    //#endregion

    //#region Date

    loadCity(): void {
        this.spinner.show();
        this.stService.loadCity(this.airport.cityId)
            .subscribe((city: AmsCity) => {
                this.spinner.hide();
                this.stService.city = city;
                this.aService.city = city;
                this.initFields();
            });
    }

    //#endregion

}
