import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsWadService } from '../../wad.service';
import { AmsWadAirportService } from '../airport.service';
import { AmsAirport, AmsPayloadDemandByDay } from 'src/app/@core/services/api/airport/dto';
import { AmsAirportClient } from 'src/app/@core/services/api/airport/api-client';
import { URL_COMMON_IMAGE_AIRCRAFT, URL_COMMON_IMAGE_AMS_COMMON, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { chartOptPaxCargo } from 'src/app/@share/charts/ams-chart-line/ams-chart-line-2y.component';


@Component({
    selector: 'ams-wad-airport-home-tabs',
    templateUrl: './airport-home-tabs.component.html',
})
export class AmsWadAirportHomeTabsComponent implements OnInit, OnDestroy {
   
    @Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();

    tileImgUrl = URL_COMMON_IMAGE_TILE;
    acCharterImg = URL_COMMON_IMAGE_AMS_COMMON + 'charter_03.png';
    acCargoImg = URL_COMMON_IMAGE_AMS_COMMON + 'ac-cargo-03.png';
    imgAircraftPath = URL_COMMON_IMAGE_AIRCRAFT;
    imgCabinPath = URL_COMMON_IMAGE_AIRCRAFT+ 'cabin/';
   
    panelIChanged: Subscription;
    region: AmsRegion;
    subregion: AmsSubregion;
    subregionChanged: Subscription;
    country: AmsCountry;
    state:AmsState;
    airport: AmsAirport
    airportChanged: Subscription;

    payloads: AmsPayloadDemandByDay[];
    payload:AmsPayloadDemandByDay;

    selTabIdx: number;
    tabIdxChanged: Subscription;

    get panelIn(): boolean {
        return this.aService.panelIn;
    }

    set panelIn(value: boolean) {
        this.aService.panelIn = value;
    }
    payloadHisOptions = chartOptPaxCargo;
    payloadHisChartLabels = ['January', 'February', 'Mars', 'April'];
    payloadHisChartkData = [
        { data: [0, 1, 2, 10, 2, 2, 2, 5, 2, 2, 4, 4, 4, 4], label: 'PAX' },
        { data: [120, 455, 100, 340, 120, 455, 100, 340, 120, 455, 100, 340, 120, 455], label: 'Cargo' },
    ];


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private aClient:AmsAirportClient,
        private wadService: AmsWadService,
        private aService: AmsWadAirportService) {

    }


    ngOnInit(): void {
        this.preloader.show();

        this.panelIChanged = this.aService.panelInChanged.subscribe((panelIn: boolean) => {
            const tmp = this.panelIn;
        });

        this.tabIdxChanged = this.aService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.airportChanged = this.wadService.airportChanged.subscribe((ap: AmsAirport) => {
            this.initFields();
            this.loadPayloadDemand();
          });


        this.initFields();
        this.loadPayloadDemand();
    }

    ngOnDestroy(): void {
        if (this.panelIChanged) { this.panelIChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }
        if (this.airportChanged) { this.airportChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.aService.tabIdx;
        this.region = this.aService.region;
        this.subregion = this.aService.subregion;
        this.country = this.aService.country;
        this.state = this.aService.state;
        this.airport = this.aService.airport;


        this.preloader.hide();
    }

    //#region Mobile dialog methods

    editUseCase(data: SxUseCaseModel) {
        console.log('editUseCase-> usecase:', data);
        /*
        const dialogRef = this.dialogService.open(SxUseCaseEditDialog, {
            width: '721px',
            height: '520px',
            data: {
                project: this.pService.selProject,
                usecase: data
            }
        });

        dialogRef.afterClosed().subscribe(res => {
            console.log('editUseCase-> res:', res);
            if (res) {
                const criteria = this.usecaseDs.criteria;
                this.usecaseDs.criteria = criteria;
            }
        });*/
    }

    //#endregion    

    //#region  Tab Panel Actions

    selectedTabChanged(tabIdx: number) {
        // console.log('selectedTabChanged -> tabIdx=', tabIdx);
        const oldTab = this.selTabIdx;
        this.selTabIdx = tabIdx;
        this.aService.tabIdx = this.selTabIdx;
        // console.log('ngOnInit -> rootFolder ', this.rootFolder);
    }

    async loadPayloadDemand():Promise<void>{
        if(this.airport){
            this.preloader.show();
            this.payloads = await this.aClient.airportPayloadDemand14Days(this.airport?.apId);
            this.payload = this.payloads && this.payloads.length>0?this.payloads[0]:undefined;
            this.payloadHisChartkData = AmsPayloadDemandByDay.getLineChartData(this.payloads);
            this.payloadHisChartLabels = AmsPayloadDemandByDay.getLineChartLabels(this.payloads);
            this.initFields();
            this.preloader.hide();
        }
       
    }
    //#endregion

    //#region SPM Methods
    panelInOpen() {
        this.aService.panelIn = true;
    }

    spmProjectOpenClick() {

        // console.log('spmProjectOpen -> spmProjectObj=', this.project);
        /*
        if (this.project) {
            this.spmProjectOpen.emit(this.project);
        }*/

    }

    //#endregion

}
