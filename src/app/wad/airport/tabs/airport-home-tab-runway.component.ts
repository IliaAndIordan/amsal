import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
// ---Services
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SpinnerService } from 'src/app/@core/services/spinner.service';
import { AmsWadService } from 'src/app/wad/wad.service';
// ---Models
import { SxProjectModel, SxUseCaseModel } from 'src/app/@core/services/api/project/dto';
import { PageViewType } from 'src/app/@core/const/app-routes.const';
import { Animate } from 'src/app/@core/const/animation.const';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion } from 'src/app/@core/services/api/country/dto';
import { AmsStatus } from 'src/app/@core/models/pipes/ams-status.enums';
import { AmsWadAirportService } from '../airport.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';

export const PAGE_VIEW_TYPE = 'sx_project_usecase_pageview';

@Component({
    selector: 'ams-wad-airport-home-tab-runway',
    templateUrl: './airport-home-tab-runway.component.html',
})
export class AmsWadAirportHomeTabRunwayComponent implements OnInit, OnDestroy {
    /**
     * Binding
     */
    //@Output() spmProjectOpen: EventEmitter<SxProjectModel> = new EventEmitter<SxProjectModel>();
    //@Output() editUseCase: EventEmitter<SxUseCaseModel> = new EventEmitter<SxUseCaseModel>();
    @Input() tabIdx: number;
    /**
     * Fields
     */
    region: AmsRegion;
    subregion:AmsSubregion;
    country: AmsCountry;
    state:AmsState;
    airport: AmsAirport
    airportChanged: Subscription;
    
    selTabIdx: number;
    tabIdxChanged: Subscription;

    totalCount: number;
    pvtOpt = PageViewType;
    showCardVar: string = Animate.show;
    showListVar: string = Animate.hide;
    showTableVar: string = Animate.hide;



    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private preloader: SpinnerService,
        public dialogService: MatDialog,
        private cus: CurrentUserService,
        private wadService:AmsWadService,
        private aService: AmsWadAirportService) {

    }


    ngOnInit(): void {
       
        this.totalCount = 0;

        this.airportChanged = this.wadService.airportChanged.subscribe((ap: AmsAirport) => {
            this.initFields();
        });


        this.tabIdxChanged = this.aService.tabIdxChanged.subscribe(tabIdx => {
            // console.log('packageCahnged -> pkg', pkg);
            this.initFields();
        });

        this.initFields();
    }

    ngOnDestroy(): void {
        if (this.airportChanged) { this.airportChanged.unsubscribe(); }
        if (this.tabIdxChanged) { this.tabIdxChanged.unsubscribe(); }

    }

    initFields() {
        this.selTabIdx = this.aService.tabIdx;
        this.region = this.aService.region;
        this.subregion = this.aService.subregion;
        this.country = this.aService.country;
        this.state = this.aService.state;
        this.airport = this.aService.airport;
    }

    

    //#region Action Methods

    createCountryClick(){
        console.log('createCountryClick -> ');
    }

    spmCountryOpenClick(value:any) {

        console.log('spmCountryOpenClick -> value=', value);
        if (value) {
            // this.spmProjectOpen.emit(this.project);
        }

    }

    //#endregion

}