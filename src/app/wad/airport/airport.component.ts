

import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { AmsWadService } from '../../wad/wad.service';
// -Models
import { environment } from 'src/environments/environment';
import {
  ExpandTab, PageTransition, ShowHideTriggerBlock,
  ShowHideTriggerFlex, SpinExpandIconTrigger, TogleBtnTopRev
} from '../../@core/const/animations-triggers';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsCountry, AmsRegion, AmsState, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';
import { Animate } from 'src/app/@core/const/animation.const';
import { Subscription } from 'rxjs';
import { AmsSidePanelModalComponent } from 'src/app/@share/components/common/side-panel-modal/side-panel-modal';
import { AmsWadAirportService } from './airport.service';
import { AmsAirport } from 'src/app/@core/services/api/airport/dto';


@Component({
  templateUrl: './airport.component.html',
  animations: [PageTransition,
    ShowHideTriggerBlock,
    ShowHideTriggerFlex,
    SpinExpandIconTrigger,
    ExpandTab, TogleBtnTopRev]
})
export class AmsWadAirportHomeComponent implements OnInit, OnDestroy {


  in = Animate.in;
  panelInVar = Animate.in;
  panelInChanged: Subscription;

  logo = COMMON_IMG_LOGO_RED;
  amsImgUrl = URL_COMMON_IMAGE_AMS;
  env: string;

  sxLogo = COMMON_IMG_LOGO_RED;
  sidebarClass = 'sidebar-closed';

  region: AmsRegion;
  subregion: AmsSubregion;
  country: AmsCountry;
  state: AmsState;
  airport: AmsAirport
  airportChanged: Subscription;

  get panelIn(): boolean {
    return this.aService.panelIn;
  }

  set panelIn(value: boolean) {
    this.aService.panelIn = value;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinerService: SpinnerService,
    private cus: CurrentUserService,
    private wadService: AmsWadService,
    private aService: AmsWadAirportService) { }


  ngOnInit(): void {
    this.env = environment.abreviation;
    this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    this.panelInChanged = this.aService.panelInChanged.subscribe((panelIn: boolean) => {
      this.panelInVar = this.panelIn ? Animate.out : Animate.in;
    });


    this.airportChanged = this.wadService.airportChanged.subscribe((airport: AmsAirport) => {
      this.initFields();
    });

    
    this.initFields();
    

  }



  ngOnDestroy(): void {
    // console.log('HomeComponent:ngOnDestroy()->');
    if (this.airportChanged) { this.airportChanged.unsubscribe(); }
    if (this.panelInChanged) { this.panelInChanged.unsubscribe(); }
  }

  initFields() {
    this.region = this.wadService.region;
    this.subregion = this.wadService.subregion;
    this.country = this.wadService.country;
    this.state = this.wadService.state;
    this.airport = this.wadService.airport;
  }

  //#region  data

  public loadAirport(): void {
    this.spinerService.show();
    this.aService.loadAirport(this.airport.apId)
      .subscribe((resp: AmsAirport) => {
        //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
        this.wadService.airport = resp;
        this.spinerService.hide();
        this.initFields();
      },
        err => {
          this.spinerService.hide();
          console.log('loadRegions-> err: ', err);
        });

  }
  //#endregion

  //#region  Actions

  // sidebar-closed
  sidebarToggle() {
    this.sidebarClass = this.sidebarClass === 'sidebar-closed' ? 'sidebar-open' : 'sidebar-closed';
  }


  gotoSubregion(value: AmsSubregion) {
    console.log('gotoSubregion -> value:', value);
    if (value) {
      this.wadService.subregion = value;
      this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
    }

  }

  //#endregion

  //#region Tree Panel

  public closePanelLeftClick(): void {
    this.panelIn = false;
  }

  public expandPanelLeftClick(): void {
    // this.treeComponent.openPanelClick();
    this.panelIn = true;
  }

  //#endregion

  //#region SPM Region

  spmRegionClose(): void {
    console.log('spmRegionClose ->');
    this.wadService.regionPanelOpen.next(false);
  }

  spmRegionOpen() {
    console.log('spmRegionOpen ->');
    this.region = this.wadService.region;
    if (this.region ) {
      this.wadService.regionPanelOpen.next(true);
    }

  }

  spmSubregionClose(): void {
    //console.log('spmSubregionClose ->');
    this.wadService.subregionPanelOpen.next(false);
  }

  spmSubregionOpen() {
    //console.log('spmSubregionOpen ->');
    this.subregion = this.wadService.subregion;
   
    if (this.subregion) {
      this.wadService.subregionPanelOpen.next(true);
    }
  }

  spmAirportClose(): void {
    console.log('spmAirportClose ->');
    this.wadService.airportPanelOpen(undefined);
  }

  spmAirportOpen() {
    console.log('spmAirportOpen ->');
    this.airport = this.wadService.airport;
    if (this.airport ) {
      this.wadService.airportPanelOpen(this.airport);
    }

  }


  //#endregion
}
