import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
// Component
import { AppRoutes } from '../@core/const/app-routes.const';
import { AmsWadHomeComponent } from './home.component';
import { AmsWadComponent } from './wad.component';
import { AmsWadDashboardComponent } from './dashboard/dashboard.component';
import { AmsWadRegionComponent } from './region/region.component';
import { AuthGuard } from '../@core/guards/auth.guard';
import { AmsWadRegionsComponent } from './regions/regions.component';
import { AmsWadSubregionHomeComponent } from './subregion/subregion.component';
import { AmsWadSubregionHomeTabsComponent } from './subregion/tabs/subregion-home-tabs.component';
import { AmsWadCountryHomeComponent } from './country/country.component';
import { AmsWadCountryHomeTabsComponent } from './country/tabs/country-home-tabs.component';
import { AmsWadStateHomeComponent } from './state/state.component';
import { AmsWadStateHomeTabsComponent } from './state/tabs/state-home-tabs.component';
import { AmsWadAirportHomeComponent } from './airport/airport.component';
import { AmsWadAirportHomeTabsComponent } from './airport/tabs/airport-home-tabs.component';


const routes: Routes = [
  {
    path: AppRoutes.Root, component: AmsWadComponent,
    children: [
      { path: AppRoutes.Root, component: AmsWadHomeComponent, children: [
          { path: AppRoutes.Root, component: AmsWadDashboardComponent, children: [ 
              {path: AppRoutes.Root,   pathMatch: 'full', component: AmsWadRegionsComponent},
              { path: AppRoutes.region, component: AmsWadRegionComponent },
            ]
          },
          { path: AppRoutes.subregion, component: AmsWadSubregionHomeComponent, children: [ 
              {path: AppRoutes.Root,   pathMatch: 'full', component: AmsWadSubregionHomeTabsComponent},
            ]
          },
          { path: AppRoutes.country, component: AmsWadCountryHomeComponent, children: [ 
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsWadCountryHomeTabsComponent},
          ],
          
          },
          { path: AppRoutes.state, component: AmsWadStateHomeComponent, children: [ 
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsWadStateHomeTabsComponent},
          ],
          
          },
          { path: AppRoutes.airport, component: AmsWadAirportHomeComponent, children: [ 
            {path: AppRoutes.Root,   pathMatch: 'full', component: AmsWadAirportHomeTabsComponent},
          ],
          
          },
        ]
      },
      // { path: 'copper-pricing', component: CopperPricingComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AmsWadRoutingModule { }

export const routedComponents = [
  AmsWadComponent, 
  AmsWadHomeComponent,
  AmsWadDashboardComponent,
  AmsWadRegionsComponent,
  AmsWadRegionComponent,
  AmsWadSubregionHomeComponent,
  AmsWadSubregionHomeTabsComponent,
  AmsWadCountryHomeComponent,
  AmsWadCountryHomeTabsComponent,
  AmsWadStateHomeComponent,
  AmsWadStateHomeTabsComponent,
  AmsWadAirportHomeComponent,
  AmsWadAirportHomeTabsComponent,
];
