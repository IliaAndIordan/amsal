import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscriber, Subscription } from 'rxjs';
import { AppStore } from '../@core/const/app-storage.const';
import { AmsAirportClient } from '../@core/services/api/airport/api-client';
import { AmsAirport, AmsAirportTableCriteria, ResponseAmsAirportTableData } from '../@core/services/api/airport/dto';
import { AmsCountryClient } from '../@core/services/api/country/api-client';
import { AmsCity, AmsCountry, AmsRegion, AmsState, AmsSubregion, IAmsRegion, IAmsSubregion, ResponseAmsCountryTableData, ResponseAmsRegionsData } from '../@core/services/api/country/dto';
// -Services
import { CurrentUserService } from '../@core/services/auth/current-user.service';
import { SpinnerService } from '../@core/services/spinner.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from '../@core/const/app-routes.const';

export const SR_LEFT_PANEL_IN_KEY = 'ams_wad_subreg_tree_panel_in';
export const SR_SEL_TABIDX_KEY = 'ams_wad_subreg_tab_idx';
@Injectable({
    providedIn: 'root',
})
export class AmsWadService {

    /**
     *  Fields
     */


    constructor(
        private router: Router,
        private toastr: ToastrService,
        private cus: CurrentUserService,
        private preloader:SpinnerService,
        private apClient: AmsAirportClient,
        private wadClient: AmsCountryClient) {
            this.gotoAirport = this.cus.gotoAirportChanged.subscribe(value =>{
                this.gotoAirportEv(value);
            })
    }

    //#region region

    regionChanged = new Subject<AmsRegion>();

    get region(): AmsRegion {
        const onjStr = localStorage.getItem(AppStore.wadRegion);
        let obj: AmsRegion;
        if (onjStr) {
            obj = Object.assign(new AmsRegion(), JSON.parse(onjStr));
        }
        return obj;
    }

    set region(value: AmsRegion) {
        const oldValue = this.region;
        if (value) {
            localStorage.setItem(AppStore.wadRegion, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.wadRegion);
        }

        this.regionChanged.next(value);
    }

    //#endregion

    //#region subregion

    subregionChanged = new Subject<AmsSubregion>();

    get subregion(): AmsSubregion {
        const onjStr = localStorage.getItem(AppStore.wadSubregion);
        let obj: AmsSubregion;
        if (onjStr) {
            obj = Object.assign(new AmsSubregion(), JSON.parse(onjStr));
        }
        return obj;
    }

    set subregion(value: AmsSubregion) {
        const oldValue = this.subregion;
        if (value) {
            localStorage.setItem(AppStore.wadSubregion, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.wadSubregion);
        }

        this.subregionChanged.next(value);
    }

    //#endregion

    //#region country

    countryChanged = new Subject<AmsCountry>();

    get country(): AmsCountry {
        const onjStr = localStorage.getItem(AppStore.wadCountry);
        let obj: AmsCountry;
        if (onjStr) {
            obj = Object.assign(new AmsCountry(), JSON.parse(onjStr));
        }
        return obj;
    }

    set country(value: AmsCountry) {
        const oldValue = this.country;
        if (value) {
            localStorage.setItem(AppStore.wadCountry, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.wadCountry);
        }

        this.countryChanged.next(value);
    }

    //#endregion

    //#region state

    stateChanged = new Subject<AmsState>();

    get state(): AmsState {
        const onjStr = localStorage.getItem(AppStore.wadState);
        let obj: AmsState;
        if (onjStr) {
            obj = Object.assign(new AmsState(), JSON.parse(onjStr));
        }
        return obj;
    }

    set state(value: AmsState) {
        const oldValue = this.state;
        if (value) {
            localStorage.setItem(AppStore.wadState, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.wadState);
        }

        this.stateChanged.next(value);
    }

    //#endregion

    //#region city

    cityChanged = new Subject<AmsCity>();

    get city(): AmsCity {
        const onjStr = localStorage.getItem(AppStore.wadCity);
        let obj: AmsCity;
        if (onjStr) {
            obj = Object.assign(new AmsCity(), JSON.parse(onjStr));
        }
        return obj;
    }

    set city(value: AmsCity) {
        const oldValue = this.country;
        if (value) {
            localStorage.setItem(AppStore.wadCity, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.wadCity);
        }

        this.cityChanged.next(value);
    }

    //#endregion


    //#region airport

    airportChanged = new Subject<AmsAirport>();

    get airport(): AmsAirport {
        const onjStr = localStorage.getItem(AppStore.wadAirport);
        let obj: AmsAirport;
        if (onjStr) {
            obj = Object.assign(new AmsAirport(), JSON.parse(onjStr));
        }
        return obj;
    }

    set airport(value: AmsAirport) {
        const oldValue = this.airport;
        if (value) {
            localStorage.setItem(AppStore.wadAirport, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.wadAirport);
        }

        this.airportChanged.next(value);
    }

    gotoAirport:Subscription;

    async gotoAirportEv(data: AmsAirport) {
        //console.log('WADS gotoAirport-> airport:', data);
        if(data){
            this.preloader.show();
            if (data && data.stateId && this.state?.stateId != data.stateId) {
                this.state = await this.loadState(data.stateId);
            }

            if (data && data.cityId && this.city?.cityId != data.cityId) {
                let city = await this.loadCity(data.cityId);
                this.city = city;
            }

            this.airport = data;
            this.router.navigate([AppRoutes.Root,AppRoutes.wad, AppRoutes.airport]);
        } else {
            this.toastr.info('No airport found. Go to state first.', 'Go To Airport');
        }
    }

    //#endregion

    //#region regions

    get regions(): AmsRegion[] {
        const onjStr = localStorage.getItem(AppStore.wadRegionList);
        let obj: AmsRegion[];
        if (onjStr) {
            obj = JSON.parse(onjStr);
        }
        return obj;
    }

    set regions(value: AmsRegion[]) {
        const oldValue = this.regions;
        if (value) {
            localStorage.setItem(AppStore.wadRegionList, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.wadRegionList);
        }
        //this.subregionChanged.next(value);
    }

    //#endregion



    //#region regions

    get subregions(): AmsSubregion[] {
        const onjStr = localStorage.getItem(AppStore.wadSubregionList);
        let obj: AmsSubregion[];
        if (onjStr) {
            obj = JSON.parse(onjStr);
        }
        return obj;
    }

    set subregions(value: AmsSubregion[]) {
        const oldValue = this.subregions;
        if (value) {
            localStorage.setItem(AppStore.wadSubregionList, JSON.stringify(value));
        } else {
            localStorage.removeItem(AppStore.wadSubregionList);
        }
        //this.subregionChanged.next(value);
    }

    //#endregion

    //#region subregion tree panel

    regionPanelOpen = new BehaviorSubject<boolean>(true);
    subregionPanelOpen = new BehaviorSubject<boolean>(true);
    statePanelOpen = new BehaviorSubject<boolean>(true);
    countryPanelOpen = new BehaviorSubject<AmsCountry>(undefined);
    airportPanelOpen(airport:AmsAirport){
        if(airport){
            this.cus.spmAirportPanelOpen.next(airport.apId);
        }
    };
    //#endregion

    //#region Admin User Charts

    public loadRegions(): Observable<ResponseAmsRegionsData> {

        return new Observable<ResponseAmsRegionsData>(subscriber => {

            this.wadClient.Regions()
                .subscribe((resp: ResponseAmsRegionsData) => {
                    if (resp) {
                        if (resp.data) {
                            let regions = new Array<AmsRegion>();
                            resp.data.regions.forEach((reg: IAmsRegion) => {
                                const val = AmsRegion.fromJSON(reg);
                                regions.push(val);
                            });
                            this.regions = regions;
                            let subregions = new Array<AmsSubregion>();
                            resp.data.subregions.forEach((subreg: IAmsSubregion) => {
                                const val = AmsSubregion.fromJSON(subreg);
                                subregions.push(val);
                            });
                            this.subregions = subregions;
                        }


                        subscriber.next(resp);
                    } else {
                        subscriber.next(undefined);
                    }
                },
                    err => {

                        throw err;
                    });
        });

    }

    loadCountryBySubregion(subregionId: number): Observable<Array<AmsCountry>> {
        const limit = 1000;
        const offset = 0;
        const orderCol = 'cName';
        const isDesc = false;
        return new Observable<Array<AmsCountry>>(subscriber => {

            this.wadClient.countryTable(limit, offset, orderCol, isDesc, subregionId, undefined)
                .subscribe((resp: ResponseAmsCountryTableData) => {
                    //const resp = Object.assign(new ResponseAmsUserTableData(), res);
                    //console.log('loadData-> resp=', resp);
                    let data = new Array<AmsCountry>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.countries && resp.data.countries.length > 0) {
                            resp.data.countries.forEach(iu => {
                                const obj = AmsCountry.fromJSON(iu);
                                data.push(obj);
                            });
                        }

                        subscriber.next(data);
                    }

                }, msg => {
                    console.log('loadData -> ' + msg);

                    let data = new Array<AmsCountry>();
                    subscriber.next(data);
                });

        });

    }

    loadAirportsByCountry(countryId: number): Observable<Array<AmsAirport>> {

        return new Observable<Array<AmsAirport>>(subscriber => {
            let obj = new AmsAirportTableCriteria();
            obj.limit = 10000;
            obj.offset = 0;
            obj.sortCol = 'apName';
            obj.sortDesc = false;
            obj.countryId = countryId;

            this.apClient.airportTable(obj)
                .subscribe((resp: ResponseAmsAirportTableData) => {

                    let data = new Array<AmsAirport>();

                    if (resp && resp.success) {
                        if (resp && resp.data &&
                            resp.data.airports && resp.data.airports.length > 0) {
                            resp.data.airports.forEach(iu => {
                                const obj = AmsAirport.fromJSON(iu);
                                data.push(obj);
                            });
                        }

                        subscriber.next(data);
                    }
                }, msg => {
                    console.log('loadAirportsByCountry -> ' + msg);
                    const data = new Array<AmsAirport>();
                    subscriber.next(data);
                });

        });

    }

    //#endregion

    //#region Data

    loadState(stateId: number): Promise<AmsState> {

        let promise = new Promise<AmsState>(async (resolve, reject) => {
             this.wadClient.stateGet(stateId).subscribe((res:AmsState)=>{
                resolve(res);
            });
        });
        return promise

    }

    loadCity(cityId: number): Promise<AmsCity> {

        let promise = new Promise<AmsCity>( (resolve, reject) => {
             this.wadClient.getCity(cityId).subscribe((res:AmsCity)=>{
                resolve(res);
            });
        });
        return promise

    }
    //#endregion


}
