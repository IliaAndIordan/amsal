

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { LoginsCountModel } from 'src/app/@core/services/api/admin-user/charts-dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsWadService } from '../wad.service';
import { AmsRegion, AmsSubregion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';


@Component({
    selector: 'ams-wad-region',
    templateUrl: './region.component.html',
    // animations: [PageTransition]
})
export class AmsWadRegionComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    logo = COMMON_IMG_LOGO_RED;
    amsImgUrl = URL_COMMON_IMAGE_AMS;
    env: string;

    region: AmsRegion;
    subregions: AmsSubregion[];

    usersCount: number;
    loginsLis: LoginsCountModel[];
    chartDataUsers = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUsers = ['January', 'February', 'Mars', 'April'];

    chartDataUserList;
    chartLabelsUserList;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private wadService: AmsWadService) { }


    ngOnInit(): void {
        this.env = environment.abreviation;
        if(!this.wadService.subregions || this.wadService.subregions.length === 0){
            this.loadRegions();
        } else{
           this.initFields();
        }
        
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    initFields(){
        this.region = this.wadService.region;
        if(this.region && this.wadService.subregions && this.wadService.subregions.length > 0){
            const srAll = this.wadService.subregions;
            this.subregions = srAll.filter(x=>x.regionId === this.region.regionId);
        } else{
            this.subregions = new Array<AmsSubregion>();
        }
        
        //console.log('initFields-> subregions:', this.subregions);
    }

    //#region  data

    public loadRegions(): void {
        this.spinerService.show();
        this.wadService.loadRegions()
            .subscribe((resp: ResponseAmsRegionsData) => {
                //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
                this.spinerService.hide();
               this.initFields();
            },
            err => {
                this.spinerService.hide();
                console.log('loadRegions-> err: ', err);
            });

    }
    //#endregion

    //#region  Actions

    gotoSubregion(value: AmsSubregion) {
        console.log('gotoSubregion -> value:', value);
        if(value){
            this.wadService.subregion = value;
            this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.subregion]);
        }
        
    }

    //#endregion
}
