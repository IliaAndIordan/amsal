

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Services
import { SpinnerService } from '../../@core/services/spinner.service';
import { CurrentUserService } from 'src/app/@core/services/auth/current-user.service';
import { SxApiProjectClient } from 'src/app/@core/services/api/project/api-client';
// -Models
import { environment } from 'src/environments/environment';
import { COMMON_IMG_LOGO_RED, URL_COMMON_IMAGE_AMS, URL_COMMON_IMAGE_TILE } from 'src/app/@core/const/app-storage.const';
import { LoginsCountModel } from 'src/app/@core/services/api/admin-user/charts-dto';
import { AppRoutes } from 'src/app/@core/const/app-routes.const';
import { AmsWadService } from '../wad.service';
import { AmsRegion, ResponseAmsRegionsData } from 'src/app/@core/services/api/country/dto';


@Component({
    templateUrl: './dashboard.component.html',
    // animations: [PageTransition]
})
export class AmsWadDashboardComponent implements OnInit, OnDestroy {

    /**
     * Fields
     */
    logo = COMMON_IMG_LOGO_RED;
    amsImgUrl = URL_COMMON_IMAGE_AMS;
    env: string;

    regions: AmsRegion[];

    usersCount: number;
    loginsLis: LoginsCountModel[];
    chartDataUsers = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
    ];

    chartLabelsUsers = ['January', 'February', 'Mars', 'April'];

    chartDataUserList;
    chartLabelsUserList;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private toastr: ToastrService,
        private spinerService: SpinnerService,
        private cus: CurrentUserService,
        private wadService: AmsWadService) { }


    ngOnInit(): void {
        this.env = environment.abreviation;
        if(!this.wadService.regions || this.wadService.regions.length === 0){
            this.loadRegions();
        } else{
            this.regions = this.wadService.regions;
            //console.log('loadRegions-> regions:', this.regions);
        }
        
    }



    ngOnDestroy(): void {
        // console.log('HomeComponent:ngOnDestroy()->');
    }

    //#region  data

    public loadRegions(): void {
        this.spinerService.show();
        this.wadService.loadRegions()
            .subscribe((resp: ResponseAmsRegionsData) => {
                //console.log('gatGvdtTsoSearchHits-> loadLoginsCount', list);
                this.spinerService.hide();
                this.regions = this.wadService.regions;
                //console.log('loadRegions-> regions:', this.regions);
            },
            err => {
                this.spinerService.hide();
                console.log('loadRegions-> err: ', err);
            });

    }
    //#endregion

    //#region  Actions

    gotoRegion(value: AmsRegion) {
        console.log('gotoRegion -> value:', value);
        if(value){
            this.wadService.region = value;
            this.router.navigate([AppRoutes.Root, AppRoutes.wad, AppRoutes.region]);
        }
        
    }

    //#endregion
}
