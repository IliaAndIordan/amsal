import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SxShareModule } from '../@share/@share.module';
import { SxPipesModule } from '../@core/models/pipes/pipes.module';
import { SxMaterialModule } from '../@share/components/material.module';
import { AmsWadRoutingModule, routedComponents } from './wad-routing.module';
import { AmsWadService } from './wad.service';
import { AmsWadMenuMainComponent } from './menu-main/menu-main.component';
import { AmsSubregionService } from './subregion/subregion.service';
import { AmsWadSubregionFilterPanelBodyComponent } from './subregion/filter-panel/subregion-filter-pane.componentl';
import { AmsWadSubregionHomeTabCountryComponent } from './subregion/tabs/subregion-home-tab-country.component';
import { AmsCountryTableDataSource } from './subregion/grids/grid-country.datasource';
import { AmsGridCountryComponent } from './subregion/grids/grid-country.component';
import { AmsWadCountryFilterPanelBodyComponent } from './country/filter-panel/wad-country-filter-pane.component';
import { AmsWadCountryService } from './country/country.service';
import { AmsWadCountryHomeTabStateComponent } from './country/tabs/country-home-tab-state.component';
import { AmsGridStatesComponent } from './country/grids/grid-states.component';
import { AmsStateTableDataSource } from './country/grids/grid-states.datasource';
import { AmsWadStateService } from './state/state.service';
import { AmsWadStateFilterPanelBodyComponent } from './state/filter-panel/wad-state-filter-pane.component';
import { AmsWadStateHomeTabStateComponent } from './state/tabs/state-home-tab-city.component';
import { AmsCityTableDataSource } from './state/grids/grid-city.datasource';
import { AmsGridCitiesComponent } from './state/grids/grid-city.component';
import { AmsStateAirportsTableDataSource } from './state/grids/grid-state-airports.datasource';
import { AmsWadStateHomeTabAirportsComponent } from './state/tabs/state-home-tab-airports.component';
import { AmsWadAirportService } from './airport/airport.service';
import { AmsWaAirportFilterPanelBodyComponent } from './airport/filter-panel/wad-airport-filter-pane.component';
import { AmsWadAirportHomeTabRunwayComponent } from './airport/tabs/airport-home-tab-runway.component';
import { AmsAirportRunwaysTableDataSource } from './airport/grids/grid-airport-runways.datasource';
import { AmsGridAirportRunwaysComponent } from './airport/grids/grid-airport-runways.component';
import { AmsWadCountryHomeTabAirportsComponent } from './country/tabs/country-home-tab-airports.component';
import { AmsWadCountryAirportsTableDataSource } from './country/grids/grid-country-airports.datasource';
import { AmsGridStateAirportsComponent } from './state/grids/grid-state-airports.component';
import { AmsGridWadCountryAirportsComponent } from './country/grids/grid-country-airports.component';


@NgModule({
    imports: [
        CommonModule,
        SxShareModule,
        SxMaterialModule,
        SxPipesModule,
        //
        AmsWadRoutingModule,
    ],
    declarations: [
        routedComponents,
        AmsWadMenuMainComponent,
        //-Dialogs
        // -Filter Panels
        AmsWadSubregionFilterPanelBodyComponent,
        AmsWadCountryFilterPanelBodyComponent,
        AmsWadStateFilterPanelBodyComponent,
        AmsWaAirportFilterPanelBodyComponent,
        // -Tabs
        AmsWadSubregionHomeTabCountryComponent,
        AmsWadCountryHomeTabStateComponent,
        AmsWadStateHomeTabStateComponent,
        AmsWadStateHomeTabAirportsComponent,
        AmsWadCountryHomeTabAirportsComponent,
        AmsWadAirportHomeTabRunwayComponent,
        // -Grids
        AmsGridCountryComponent,
        AmsGridStatesComponent,
        AmsGridCitiesComponent,
        AmsGridStateAirportsComponent,
        AmsGridAirportRunwaysComponent,
        AmsGridWadCountryAirportsComponent,
    ],
    exports: [],
    providers: [
        AmsWadService,
        AmsSubregionService,
        AmsCountryTableDataSource,
        AmsWadCountryService,
        AmsStateTableDataSource,
        AmsWadStateService,
        AmsCityTableDataSource,
        AmsStateAirportsTableDataSource,
        AmsWadAirportService,
        AmsAirportRunwaysTableDataSource,
        AmsWadCountryAirportsTableDataSource,
    ]
})
export class AmsWadModule { }
