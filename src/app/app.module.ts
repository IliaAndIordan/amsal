import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SxCoreModule } from './@core/@core.module';
import { SxShareModule } from './@share/@share.module';
import { ApiBaseModule } from './@core/services/api-base/api-base.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MapLeafletSpmService } from './@share/maps/map-liflet-spm.service';
import { MapLeafletDashboardService } from './@share/maps/map-liflet-dashboard.service';
import { MapLeafletApRoutesService } from './@share/maps/map-liflet-ap-routes.service';

@NgModule({

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    //
    AppRoutingModule,
    SxShareModule,
    ApiBaseModule,
    SxCoreModule,
  ],
  declarations: [
    AppComponent,
  ],
  exports:[ 
    SxCoreModule,
    SxShareModule,
  ],
  providers: [
    MapLeafletSpmService,
    MapLeafletDashboardService,
    MapLeafletApRoutesService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
 
}
